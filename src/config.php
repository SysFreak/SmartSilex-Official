<?php

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;

use App\Providers\AppServiceProvider;
use App\Providers\FPDFServiceProvider;
use App\Providers\ParisServiceProvider;
use App\Providers\AuraSessionServiceProvider;
use App\Providers\PagosLoggerServiceProvider;

use josegonzalez\Dotenv\Loader as EnvLoader;


$APP_DEBUG						=	true;
$TWIG_CACHE						=	false;

$CYPHER_KEY						=	25c6c7ff35b9979b151f2136cd13b0ff;

#DB_DRIVER=mysql
#DB_HOST=192.168.2.22
#DB_NAME=silex
#DB_USER=luis.924
#DB_PASSWORD=123456

$DB_DRIVER						=	'mysql';
$DB_HOST						=	'localhost';
$DB_NAME						=	'silexdb';
$DB_USER						=	'softdev';
$DB_PASSWORD					=	'T3mpP4ss836';

$DB_SESSION_SEGMENT				=	'some_ramdom_string';

$UBER_URL						=	'http://billing.boomnetpr.com/';
$UBER_USER						=	'apiuser';
$BER_TOKEN						=	'B00mN3t1234';
