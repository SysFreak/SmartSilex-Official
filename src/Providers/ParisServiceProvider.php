<?php
namespace App\Providers;

use ORM;
use Model;
use Silex\Application;
use App\Lib\Config;
use Silex\ServiceProviderInterface;

/**
* Proveedor de servicio de session usando (Aura/Session)[https://packagist.org/packages/aura/session]
*/
class ParisServiceProvider implements ServiceProviderInterface
{
 

    public function register(Application $app)
    {
        $datos      = New Config();
        $dataBase   = $datos->DataBase();

        Model::$short_table_names = true;
        ORM::configure('username', $dataBase['DB_USER']);
        ORM::configure('password', $dataBase['DB_PASSWORD']);
        ORM::configure(
            $dataBase['DB_DRIVER']
            .':host='  .$dataBase['DB_HOST']
            .';dbname='.$dataBase['DB_NAME']
        );
    }

    // public function register(Application $app)
    // {
    //     Model::$short_table_names = true;
    //     ORM::configure('username', $app['env']['DB_USER']);
    //     ORM::configure('password', $app['env']['DB_PASSWORD']);
    //     ORM::configure(
    //         $app['env']['DB_DRIVER']
    //         .':host='  .$app['env']['DB_HOST']
    //         .';dbname='.$app['env']['DB_NAME']
    //     );
    // }

    public function boot(Application $app)
    {
    }
}