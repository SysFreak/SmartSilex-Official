<?php
namespace App\Providers;

require '../vendor/autoload.php';

use Silex\Application;
use Silex\ServiceProviderInterface;
use josegonzalez\Dotenv\Loader;
use App\Lib\Config;
use App\Lib\DataCypher;
use App\Lib\DataLogger;
use App\Lib\SendMail;
use App\Lib\MultiLogger;
use App\Lib\Curl;
use App\Lib\Conexion;
use App\Lib\DataBase;

use App\PHPExcel\ExcelReport;
use App\Models\Logger;
use Carbon\Carbon;

/**
* clase para agregar servicios que sean necesarios para el funcionamiento de la aplicacion
*/
class AppServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    {
        $datos      = New Config();
        
        $cypherKey  = $datos->CypherKey();

        $app['cypher'] = $app->share(function() use ($app)
        {
            $datos      = New Config();
            return new DataCypher($datos->Ramdom(32));
        });

        $app['upload'] = $app->share(function() use ($app)
        {
            return UPLOADPATH;
        });

        $app['date'] = $app->share(function() use ($app){
            return date('Y-m-d h:i:s', time());
        });

        $app['year'] = $app->share(function() use ($app){
            return date("Y");
        });

        $app['datalogger'] = $app->share(function() use ($app){
            return new DataLogger($info);
        });

        $app['mail'] = $app->share(function() use ($app){
            return new SendMail();
        });

        $app['curl'] = $app->share(function() use ($app){
            return new Curl();
        });

        $app['PDO'] = $app->share(function() use ($app){
           
            try {
                $connection = Database::instance();
                $sql    =   $query;
                $query  =   $connection->prepare($sql);
                $query->execute();

                $res    =   $query->fetchAll(PDO::FETCH_ASSOC);

                return ($res) ? $res : false;

            } catch (Exception $e) {

                return "Error!: " . $e->getMessage();

            }
        });

        $app['replacestr'] = $app->share(function() use ($app){
            return New Config();

        });
    }

    public function boot(Application $app)
    {
        // desactivar variables estrictas en twig, esto permite que cuando se use una variable
        // no definida en una plantilla twig esta devuelva null, por ejemplo, en este proyecto
        // hay una variable llamada 'sidebar' en la plantilla 'layouts/main.html.twig' que 
        // controla si el layouy usa una sidebar, cuando esto sea necesario es solo
        // cuestion de definirla con el valor true y el layout incluira una sidebar
        // definida en la plantilla 'layouts/sidebar.html.twig'
        $app['twig']->disableStrictVariables();

        // si la configuracion define una cache para twig, usarla en el servicio twig
        // para desactivar la cache, asignar 'TWIG_CACHE' el valor false en el
        // archivo .env
        if (isset($app['env']['TWIG_CACHE'])) {
            $app['twig']->setCache($app['env']['TWIG_CACHE']);
        }  
        
        // $cache = array_key_exists('TWIG_CACHE', $app['env']) ? $app['env']['TWIG_CACHE'] : true ;
        // $app['twig']->setCache($cache);
    }
}