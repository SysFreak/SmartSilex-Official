<?php
namespace App\Providers;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Aura\Session\SessionFactory;

/**
* Proveedor de servicio de session usando (Aura/Session)[https://packagist.org/packages/aura/session]
*/
class AuraSessionServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    {
    }

    public function boot(Application $app)
    {
        $app['session_factory'] = $app->share(function() use ($app) {
            return new SessionFactory();
        });

        $app['session_manager'] = $app->share(function() use ($app) {
            return $app['session_factory']->newInstance($_COOKIE);
        });

        $app['session'] = $app->share(function() use ($app) {
            return $app['session_manager'] ->getSegment($app['env']['DB_SESSION_SEGMENT']); //Creates a Segment Object
        });
    }
}