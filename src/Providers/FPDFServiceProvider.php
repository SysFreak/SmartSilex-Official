<?php
namespace App\Providers;

use FPDF;
use Silex\Application;
use Silex\ServiceProviderInterface;

/**
* Proveedor de servicio de session usando (Aura/Session)[https://packagist.org/packages/aura/session]
*/
class FPDFServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    {
        $app['fpdf'] = $app->share(function() use ($app) {
            return new FPDF($app['fpdf.orientation'], $app['fpdf.unit'], $app['fpdf.size']);
        });
    }

    public function boot(Application $app)
    {
        $app['fpdf.unit'] = 'mm';
        $app['fpdf.size'] = 'letter';
        $app['fpdf.orientation'] = 'P'; 
    }
}
