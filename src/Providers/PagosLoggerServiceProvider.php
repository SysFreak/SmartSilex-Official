<?php
namespace App\Providers;

use Silex\Application;
use Silex\ServiceProviderInterface;

use ORM;
use Monolog\Logger;
use MySQLHandler\MySQLHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\HtmlFormatter;

/**
* Proveedor de servicio que agrega un logger donde escribir los eventos de cargos a tarjetas
*/
class PagosLoggerServiceProvider implements ServiceProviderInterface
{

    public function register(Application $app)
    { 
        $app['logger.pagos.web'] = $app->share(function() use ($app){
            
            // definir un nuevo logger
            $monolog = new Logger('pagos.web');

            // definir el archivo de log a usar cuando se registre
            // un cargo a una tarjeta de credito
            $logDir = realpath(__DIR__.'/../../var/logs');

            // definir dos handlers que guarden sus logs en el directorio /var/logs
            // un log sera en texto plano y el otro en tablas HTML, para las tablas
            // se usara la clase HtmlFormatter que viene por defecto en monolog
            
            $handlerHtml  = new StreamHandler($logDir.'/Pagos_log.html',  Logger::INFO);
            $handlerPlain = new StreamHandler($logDir.'/Pagos_log.log', Logger::INFO);
            $handlerMysql = new MySQLHandler(ORM::get_db(),'credit_pagos',array('user','gateway','request','exito','gatewayResponse'),Logger::INFO);

            // hacer que este handler use el HtmlFormatter, para que los logs
            // puedan visualizarse de manera mas facil
            $handlerHtml->setFormatter(new HtmlFormatter('D d-m-Y H:i:s'));
            
            // por ultimo, usar ese handler en el logger de los cargos a 
            // tarjetas de credito
            $monolog->pushHandler($handlerHtml);
            $monolog->pushHandler($handlerMysql);
            $monolog->pushHandler($handlerPlain);
            
            // ddd($monolog);
            return $monolog;
        }); 

        $app['logger.pagos.cli'] = $app->share(function() use ($app){
            return $app['logger.pagos.web']->withName('pagos.cli');
        });

    }

    public function boot(Application $app)
    {
    }
}