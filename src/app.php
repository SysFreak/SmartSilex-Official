<?php

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;

use App\Providers\AppServiceProvider;
use App\Providers\FPDFServiceProvider;
use App\Providers\ParisServiceProvider;
use App\Providers\AuraSessionServiceProvider;
use App\Providers\PagosLoggerServiceProvider;

use josegonzalez\Dotenv\Loader as EnvLoader;

$envLoader = new EnvLoader(__DIR__.'/../.env');
$envUpload = new EnvLoader(__DIR__.'/../upload/');

$app = new Application();

$app['env'] = array_merge(
    $envLoader->parse()->toArray(),
    $_ENV
);

$app->register(new TwigServiceProvider);
$app->register(new ValidatorServiceProvider);
$app->register(new UrlGeneratorServiceProvider);
$app->register(new HttpFragmentServiceProvider);
$app->register(new ServiceControllerServiceProvider);

$app->register(new AppServiceProvider);
$app->register(new FPDFServiceProvider);
$app->register(new ParisServiceProvider);
$app->register(new AuraSessionServiceProvider);
$app->register(new PagosLoggerServiceProvider);

$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {

    $twig->addFunction(new Twig_SimpleFunction('s', 's'));
    $twig->addFunction(new Twig_SimpleFunction('d', 'd'));
    $twig->addFunction(new Twig_SimpleFunction('asset', function($file) use ($app) {
        return $app['request']->getBaseUrl().'/'.$file;
    }));
    
    return $twig;
}));

return $app;
