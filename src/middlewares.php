<?php

$app['middleware.redirectIfNotAuthenticated'] = $app->protect(function($request, $app) {

	// return $app->redirect($app["url_generator"]->generate('Suspended'));

	$curl = $app['curl']->execute(array('command' => 'license'));

	if($curl == "expired")

	{ return $app->redirect($app["url_generator"]->generate('ErrorLicense')); }

	elseif(!$app['session']->get('id')) 
	{     
        return $app->redirect($app["url_generator"]->generate('login'));
    }

});


$app['middleware.check'] = $app->protect(function($request, $app) {

	$curl = $app['curl']->execute(array('command' => 'license'));

	if($curl == "expired")

	{ return $app->json(array('false' => 'ErrorLicense')); }

});

$app['middleware.UserAuthorized'] = $app->protect(function($request, $app) 
{
	switch ($app['session']->get('username')) 
	{
		case 'EUKARIS.SL304':	break;
		case 'ARAMIS.SL376':	break;
		case 'ERICK.SL341':		break;
		case 'JORGEB.SL303':	break;
		case 'LCAMPOS':			break;
		case 'YOYBER.DE516':	break;
		case 'LINDA.DE504':		break;
		case 'MARIAH.DE506':	break;
		case 'JESUS.SP901':		break;
		case 'ANDRY.CL604':		break;
		case 'JANNYBETH.DE503':	break;
		case 'STEPHANIE':		break;			
		case 'EIBEL.LEAD':		break;		
		case 'AUDIT.CALLS': 	break;		
		case 'ANTONIO.LEAD':	break;		
		default:
			return $app->redirect($app["url_generator"]->generate('Suspended'));
		break;
	}
	
	// $curl = $app['curl']->execute(array('command' => 'license'));

	// if($curl == "expired")

	// { return $app->json(array('false' => 'ErrorLicense')); }

});

$app['middleware.CustomerServices'] = $app->protect(function($request, $app) 
{
	// ddd($app['session']->get('username'));

	switch ($app['session']->get('username')) 
	{
		case 'JUAN.CS907':		break;
		case 'LUISD.CS922':		break;
		case 'ANDRY.CS908':		break;
		case 'WUILIANA.CS926':	break;
		case 'THAIS.CS920':		break;
		case 'ELIEZMAR.CS213':	break;
		case 'NANCY.CS918':		break;
		case 'BRAIDY.CS923':	break;
		case 'YOSELINE.CS934':	break;
		case 'MIGUEL.CS928':	break;
		case 'JAVIER.CS935':	break;
		case 'ANTONIO.CS919':	break;
		case 'RAMON.CS933':		break;
		case 'ADELIS.CS914':	break;
		case 'ROSANGELA.CS929':	break;
		case 'MARIA.CS921':		break;
		case 'JOSE.CS903':		break;
		case 'HECTOR':			break;
		case 'JANNYBETH.CS906':	break;
		case 'ANDRY.CS908':		break;
		case 'JOSEPH.CS912':	break;
		case 'JORGE.CS902':		break;
		case 'JESUS.CS916':		break;	
		case 'LCAMPOS':			break;
		case 'JESUS.CS901':		break;	
		case 'EIBEL.LEAD':		break;		
		case 'AUDIT.CALLS':		break;	
		case 'LUIS.CS933':		break;	
		case 'YONER.CS905':		break;
		case 'MARIAH.DE506':	break;
		case 'LINDA.DE504':		break;	
		case 'JOSER.CS909':		break;	
		case 'YIBRAND.CS910':	break;	
		case 'LEOPOLDO.CS936':	break;	
		case 'YUANNI.CS937':	break;	
		case 'PAOLAP.CS938':	break;	
		case 'JUDITH.CS939':	break;
		case 'MARIAB.OP':		break;	
		case 'JORGE.CS929':		break;	
		case 'JULIO.CS919':		break;	
		case 'MIRIALBER.CS905':	break;	
		case 'JOSEA.CS917':		break;	
		case 'WILLIAMS.CS933': 	break;	
		case 'ANTONIO.LEAD': 	break;	
		case 'JORGEL.CS904': 	break;	
		case 'JONATHAN.CS934': 	break;	
		case 'JOSEA.CS917': 	break;	
		case 'JONATHAN.CS934': 	break;	
		case 'JORGEL.CS904': 	break;	
		case 'LUISP.CS905': 	break;	
		case 'MIGUELC.CS906': 	break;	
		case 'MARLON.CS914': 	break;	
		case 'JOSEC.CS908': 	break;	
		case 'LUISD.CS905': 	break;
		case 'LUISD.CL606': 	break;
		default:
			return $app->redirect($app["url_generator"]->generate('Suspended'));
		break;
	}
	
	// $curl = $app['curl']->execute(array('command' => 'license'));

	// if($curl == "expired")

	// { return $app->json(array('false' => 'ErrorLicense')); }

});

$app['middleware.ISPServices'] = $app->protect(function($request, $app) 
{

	switch ($app['session']->get('username')) 
	{
		case 'JESUS.CS901': 	break;	
		case 'JORGE.CS902': 	break;
		case 'JOSEPH.CS912': 	break;
		case 'LCAMPOS': 		break;
		case 'EIBEL.LEAD': 		break;		
		case 'AUDIT.CALLS': 	break;	
		case 'MARIAB.OP': 		break;	
		case 'ALBAN.LEAD': 		break;
		case 'CARLOS.CEO': 		break;
		default:	return $app->redirect($app["url_generator"]->generate('Suspended'));	break;
	}

});


$app['middleware.Admins'] = $app->protect(function($request, $app) 
{
	switch ($app['session']->get('username')) 
	{
		case 'LCAMPOS':
			break;
		default:
			return $app->redirect($app["url_generator"]->generate('Suspended'));
		break;
	}
	
	// $curl = $app['curl']->execute(array('command' => 'license'));

	// if($curl == "expired")

	// { return $app->json(array('false' => 'ErrorLicense')); }

});