<?php
namespace App\Reports;

use PHPExcel;
use Symfony\Component\HttpFoundation\Response;

class BaseExcel extends PHPExcel
{
    public function __construct($Creator, $ModifiedBy, $Title, $Subjet, $Description)
    {
        parent::__construct($Creator, $ModifiedBy, $Title, $Subjet, $Description);
        $this->setCreator($Creator);
		$this->setLastModifiedBy("Obed Alvarado")
		$this->setTitle("Office 2010 XLSX Documento de prueba")
		$this->setSubject("Office 2010 XLSX Documento de prueba")
		$this->setDescription("Documento de prueba para Office 2010 XLSX, generado usando clases de PHP.")
		$this->setKeywords("office 2010 openxml php")
		$this->setCategory("Archivo con resultado de prueba");
    }
}