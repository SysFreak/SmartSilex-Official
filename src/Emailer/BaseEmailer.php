<?php
namespace App\Emailer;

use Swift_Mailer;
use Swift_Message;
use RuntimeException;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Symfony\Component\HttpFoundation\Response;

class BaseEmailer  {

    protected $swiftmailer;

    protected $username;
    protected $password;

    protected $to;
    protected $subject;
    protected $attachments;
    protected $messageType;
    protected $messageBody;
    protected $messageObject;

    protected $twigLoader;
    protected $twigObject;

    protected $properties;

    const TRANSPORT_MAIL = 0;
    const TRANSPORT_SMTP = 1;
    const TRANSPORT_SENDMAIL = 2;

    // function __construct($transport=self::TRANSPORT_MAIL, $username='intranet@boomnetpr.com', $password='') {
    function __construct($transport=self::TRANSPORT_MAIL, $username='boomnetphp@gmail.com', $password='b00mN3tPHP') {
        // $this->username = $username;
        // $this->password = $password;
        $this->username = "boomnetphp@gmail.com";
        $this->password = "b00mN3tPHP";

        $this->attachments = array();

        $this->transport     = self::newTransportInstance($transport, $username, $password);
        $this->swiftmailer   = Swift_Mailer::newInstance($this->transport);
        $this->messageObject = Swift_Message::newInstance();
    }

    /**
     * Retorna una instancia al transport de swiftmailer especidicado en sus parametros
     *
     * @param int $transport     tipo de transport del que se quiere una instancia
     * @param string $user       usuario para ser usando en caso de que $transport sea
     *                           una instancia de \Swift_SmtpTransport
     * @param string $pass       contrasena para ser usanda en caso de que $transport
     *                           sea una instancia de \Swift_SmtpTransport
     * @param string $smtpServer direccion del servidor SMTP a usar
     * @param string $port       puerto a usar para conectarse a $smtpServer
     *
     * @return \Swift_Transport
     */
    // protected static function newTransportInstance($transport=self::TRANSPORT_SMTP, $user='', $pass='', $smtpServer='smtp.gmail.com', $port = 465 )
    protected static function newTransportInstance($transport=self::TRANSPORT_SMTP, $user='', $pass='', $smtpServer='smtp.gmail.com', $port = 465 )
    {
        if ($transport == self::TRANSPORT_SMTP) {

            return \Swift_SmtpTransport::newInstance($smtpServer, $port, 'ssl')
                        ->setUsername($user)
                        ->setPassword($pass);

        } elseif ($transport == self::TRANSPORT_SENDMAIL) {          
            
            return \Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');

        } elseif ($transport == self::TRANSPORT_MAIL) {
            
            return \Swift_MailTransport::newInstance();

        }
    }
    
}