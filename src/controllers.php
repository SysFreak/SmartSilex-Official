<?php
namespace App\Controllers;

$app['Auth.controller'] = $app->share(function() use ($app) {
    return new AuthController($app);
});

$app['Home.controller'] = $app->share(function() use ($app) {
    return new HomeController($app);
});

$app['Config.controller'] = $app->share(function() use ($app) {
    return new ConfigController($app);
});

$app['Calendar.controller'] = $app->share(function() use ($app) {
    return new CalendarController($app);
});

$app['Departament.controller'] = $app->share(function() use ($app) {
    return new DepartamentController($app);
});

$app['Teams.controller'] = $app->share(function() use ($app) {
    return new TeamsController($app);
});

$app['Did.controller'] = $app->share(function() use ($app) {
    return new DidController($app);
});

$app['Users.controller'] = $app->share(function() use ($app) {
    return new UsersController($app);
});

$app['Role.controller'] = $app->share(function() use ($app) {
    return new RoleController($app);
});

$app['Leads.controller'] = $app->share(function() use ($app) {
    return new LeadsController($app);
});

$app['Leaders.controller'] = $app->share(function() use ($app) {
    return new LeadersController($app);
});

$app['Error.controller'] = $app->share(function() use ($app) {
    return new ErrorController($app);
});

$app['Test.controller'] = $app->share(function() use ($app) {
    return new TestController($app);
});

$app['Country.controller'] = $app->share(function() use ($app) {
    return new CountryController($app);
});

$app['Town.controller'] = $app->share(function() use ($app) {
    return new TownController($app);
});

$app['Ceiling.controller'] = $app->share(function() use ($app) {
    return new CeilingController($app);
});

$app['House.controller'] = $app->share(function() use ($app) {
    return new HouseController($app);
});

$app['Level.controller'] = $app->share(function() use ($app) {
    return new LevelController($app);
});

$app['Bank.controller'] = $app->share(function() use ($app) {
    return new BankController($app);
});

$app['Payment.controller'] = $app->share(function() use ($app) {
    return new PaymentController($app);
});

$app['Provider.controller'] = $app->share(function() use ($app) {
    return new ProviderController($app);
});

$app['Package.controller'] = $app->share(function() use ($app) {
    return new PackageController($app);
});

$app['Cameras.controller'] = $app->share(function() use ($app) {
    return new CamerasController($app);
});

$app['Dvr.controller'] = $app->share(function() use ($app) {
    return new DvrController($app);
});

$app['Phone.controller'] = $app->share(function() use ($app) {
    return new PhoneController($app);
});

$app['Internet.controller'] = $app->share(function() use ($app) {
    return new InternetController($app);
});

$app['InternetType.controller'] = $app->share(function() use ($app) {
    return new InternetTypeController($app);
});

$app['Creative.controller'] = $app->share(function() use ($app) {
    return new CreativeController($app);
});

$app['Zip.controller'] = $app->share(function() use ($app) {
    return new ZipController($app);
});

$app['Objection.controller'] = $app->share(function() use ($app) {
    return new ObjectionController($app);
});

$app['Medium.controller'] = $app->share(function() use ($app) {
    return new MediumController($app);
});

$app['Objetive.controller'] = $app->share(function() use ($app) {
    return new ObjetiveController($app);
});

$app['Post.controller'] = $app->share(function() use ($app) {
    return new PostController($app);
});

$app['Origen.controller'] = $app->share(function() use ($app) {
    return new OrigenController($app);
});

$app['Forms.controller'] = $app->share(function() use ($app) {
    return new FormsController($app);
});

$app['ServiceM.controller'] = $app->share(function() use ($app) {
    return new ServiceMController($app);
});

$app['Campaign.controller'] = $app->share(function() use ($app) {
    return new CampaignController($app);
});

$app['Service.controller'] = $app->share(function() use ($app) {
    return new ServiceController($app);
});

$app['Referred.controller'] = $app->share(function() use ($app) {
    return new ReferredController($app);
});

$app['Reporting.controller'] = $app->share(function() use ($app) {
    return new ReportingController($app);
});

$app['Search.controller'] = $app->share(function() use ($app) {
    return new SearchController($app);
});

$app['Approvals.controller'] = $app->share(function() use ($app) {
    return new ApprovalsController($app);
});

$app['Coordinations.controller'] = $app->share(function() use ($app) {
    return new CoordinationsController($app);
});

$app['Operator.controller'] = $app->share(function() use ($app) {
    return new OperatorController($app);
});

$app['Marketing.controller'] = $app->share(function() use ($app) {
    return new MarketingController($app);
});

$app['Ticket.controller'] = $app->share(function() use ($app) {
    return new TicketController($app);
});

$app['Support.controller'] = $app->share(function() use ($app) {
    return new SupportController($app);
});

$app['Rrhh.controller'] = $app->share(function() use ($app) {
    return new RrhhController($app);
});

$app['Maintenance.controller'] = $app->share(function() use ($app) {
    return new MaintenanceController($app);
});

$app['Collection.controller'] = $app->share(function() use ($app) {
    return new CollectionController($app);
});

$app['CollectionVE.controller'] = $app->share(function() use ($app) {
    return new CollectionVEController($app);
});

$app['Contract.controller'] = $app->share(function() use ($app) {
    return new ContractController($app);
});

$app['Statistics.controller'] = $app->share(function() use ($app) {
    return new StatisticsController($app);
});

$app['Development.controller'] = $app->share(function() use ($app) {
    return new DevelopmentController($app);
});

$app['ISP.controller'] = $app->share(function() use ($app) {
    return new ISPController($app);
});

$app['B24.controller'] = $app->share(function() use ($app) {
    return new B24Controller($app);
});

$app['Transactions.controller'] = $app->share(function() use ($app) {
    return new TransactionsController($app);
});