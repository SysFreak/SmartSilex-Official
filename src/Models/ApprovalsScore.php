<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Bank;
use App\Models\CypherData;
use App\Models\Country;
use App\Models\Town;
use App\Models\Approvals;
use App\Models\ApprovalsScore;
use App\Models\ApprovalsScoreTmp;
use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\Leads;
use App\Models\User;
use App\Models\Departament;
use App\Models\Service;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;


use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class ApprovalsScore extends Model 
{

	static $_table 		= 'cp_appro_score';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'appro_id', 'service_id', 'score', 'transactions', 'value', 'fico', 'operator_id', 'created_at');

//////////////////////////////////// GetServiceID ////////////////////////////////////


	public static function GetScoreById($id)
	{
		$query 	=	'SELECT ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at FROM cp_appro_score WHERE id = "'.$id.'"';

		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'id'			=>	$serv['id'],
				'ticket'		=> 	$serv['ticket'],
				'client'		=>	$serv['client_id'],
				'service'		=>	$serv['service_id'],
				'approval'		=>	$serv['appro_id'],
				'score'			=>	$serv['score'],
				'transaction'	=>	$serv['transactions'],
				'value'			=>	$serv['value'],
				'fico'			=>	$serv['fico'],
				'operator'		=>	$serv['operator_id'],
				'created'		=>	$serv['created_at']
			];

		}else{	return false;	}
	}

//////////////////////////////////// GetServiceID ////////////////////////////////////

	public static function GetLastScore($info)
	{

		$query 	=	'SELECT id, ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at FROM cp_appro_score WHERE client_id = "'.$info['client'].'" AND service_id = "'.$info['service'].'"';

		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'id'			=>	$serv['id'],
				'ticket'		=> 	$serv['ticket'],
				'client'		=>	$serv['client_id'],
				'approval'		=>	$serv['appro_id'],
				'service'		=>	$serv['service_id'],
				'score'			=>	$serv['score'],
				'transaction'	=>	$serv['transactions'],
				'value'			=>	$serv['value'],
				'fico'			=>	$serv['fico'],
				'operator'		=>	$serv['operator_id'],
				'created'		=>	$serv['created_at']
			];

		}else{	return false;	}
	}


//////////////////////////////////// GetServiceID ////////////////////////////////////

	public static function GetScoreByTicket($ticket)
	{
		$query 	=	'SELECT ticket, client_id, score AS score_id, (SELECT name FROM data_score WHERE id = score) AS score, transactions, fico FROM cp_appro_score WHERE ticket = "'.$ticket.'"';
		$serv 	=	DBSmart::DBQuery($query);
		return [
			'ticket'		=> 	$serv['ticket'],
			'client'		=>	$serv['client_id'],
			'score_id'		=>	$serv['score_id'],
			'score'			=>	$serv['score'],
			'transaction'	=>	$serv['transactions'],
			'fico'			=>	$serv['fico']
		];
	}


//////////////////////////////////// GetServiceID ////////////////////////////////////

	public static function GetScoreType($type, $info)
	{
		switch ($type) 
		{
			case 'ticket':
				$query 	=	'SELECT ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at FROM cp_appro_score WHERE ticket = "'.$info.'"';
				$score 	=	DBSmart::DBQuery($query);
				break;
			
			case 'client':
				$query 	=	'SELECT ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at FROM cp_appro_score WHERE client_id = "'.$info.'"';
				$score 	=	DBSmart::DBQuery($query);
				break;

			case 'id':
				$query 	=	'SELECT ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at FROM cp_appro_score WHERE id = "'.$info.'"';
				$score 	=	DBSmart::DBQuery($query);
				break;
		}

		if($serv <> false)
		{
			return [
				'ticket'		=> 	$serv['ticket'],
				'client'		=>	$serv['client_id'],
				'service'		=>	$serv['service_id'],
				'approval'		=>	$serv['appro_id'],
				'score'			=>	$serv['score'],
				'transaction'	=>	$serv['transaction'],
				'value'			=>	$serv['value'],
				'fico'			=>	$serv['fico'],
				'operator'		=>	$serv['operator_id'],
				'created'		=>	$serv['created_at']
			];

		}else{	return false;	}

	}

//////////////////////////////////// Save Score ////////////////////////////////////

	public static function SaveScore($info)
	{

		$date	=	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_appro_score(ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at) VALUES ( "'.$info['ticket'].'", "'.$info['client'].'", "'.$info['appro'].'", "'.$info['service'].'", "'.$info['score'].'", "'.$info['transaction'].'", "'.$info['value'].'", "'.$info['fico'].'", "'.$info['ope_a'].'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}

///////////////////////////////// Save Score Copy ///////////////////////////////////

	public static function SaveScoreCopy($info)
	{

		$date	=	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_appro_score(ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at) VALUES ( "'.$info['ticket'].'", "'.$info['client'].'", "'.$info['appro'].'", "'.$info['service'].'", "'.$info['score'].'", "'.$info['transaction'].'", "'.$info['value'].'", "'.$info['fico'].'", "'.$info['ope_a'].'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;

	}


}