<?php
namespace App\Models;

use Model;

use App\Models\ServiceMRouterPrTmp;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\Referred;
use App\Models\User;
use App\Models\Payment;
use App\Models\StatusService;
use App\Models\LeadsProvider;

use App\Lib\Config;
use App\Lib\DBSmart;
use PDO;

class ServiceMRouterPrTmp extends Model 
{

	static $_table = 'cp_service_pr_mr_tmp';
	
	Public $_fillable = array('client_id', 'cant', 'ssid', 'red_id', 'password', 'payment_id', 'service_id', 'type_order_id', 'origen_id', 'ref_id', 'ref_list_id', 'sup_id', 'sup_ope_id', 'owen_id', 'assit_id', 'created_at');

	public static function SaveMRouter($info)
	{
		$date 	=	date('Y-m-d H:i:s', time());

		if($info['sup'] == 0)
		{	$user1 = $info['operator']; $user2 = $info['operator']; }
		else
		{	$user1 = $info['sup_list']; $user2 = $info['operator']; }

		$user 	=	($info['sup'] == 0) ? $info['operator'] : $info['sup_list'];

		$query 	=	'INSERT INTO cp_service_pr_mr_tmp(client_id, cant, ssid, red_id, password, payment_id, service_id, type_order_id, origen_id, ref_id, ref_list_id, sup_id, sup_ope_id, owen_id, assit_id, created_at) VALUES ("'.$info['client'].'", "'.$info['cant'].'", "'.$info['ssid'].'", "'.$info['red'].'", "'.$info['password'].'", "'.$info['payment'].'", "'.$info['service'].'", "'.$info['order_type'].'", "'.$info['origen'].'", "'.$info['ref'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$user1.'", "'.$user2.'", "'.$date.'") ';

		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	}

	public static function ServiceMRouter($client)
	{
			
		$query 	=	'SELECT client_id, cant, red_id, ssid, password, payment_id, service_id, type_order_id, origen_id, ref_id, ref_list_id, sup_id, sup_ope_id, owen_id, created_at FROM cp_service_pr_mr_tmp WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'status'		=>	true,
			 	'client' 		=>	($serv['client_id'] <> '') 		?	$serv['client_id']		:	$serv['client_id'],
			 	'cant' 			=>	($serv['cant'] <> '') 			?	$serv['cant']			:	'0',
			 	'red' 			=>	($serv['red_id'] <> '') 		?	$serv['red_id']			:	'0',
			 	'ssid' 			=>	($serv['ssid'] <> '') 			?	$serv['ssid']			:	'',
			 	'password' 		=>	($serv['password'] <> '') 		?	$serv['password']		:	'',
			 	'payment'		=>	($serv['payment_id'] <> '') 	?	$serv['payment_id']		:	'1',
			 	'service' 		=>	($serv['service_id'] <> '') 	?	$serv['service_id']		:	'1',
			 	'order_type' 	=>	($serv['type_order_id'] <> '')	?	$serv['type_order_id']	:	'1',
			 	'origen' 		=>	($serv['origen_id'] <> '')		?	$serv['origen_id']		:	'1',
				'ref' 			=>	($serv['ref_id'] <> '') 		?	$serv['ref_id']			:	'0',
			 	'ref_list' 		=>	($serv['ref_list_id'] <> '') 	?	$serv['ref_list_id']	:	'0',
			 	'sup' 			=>	($serv['sup_id'] <> '') 		?	$serv['sup_id']			:	'0',
			 	'sup_list' 		=>	($serv['sup_ope_id'] <> '') 	?	$serv['sup_ope_id']		:	'1',
			 	'operator'		=>	($serv['owen_id'] <> '') 		?	$serv['owen_id']		:	'',
			 	'created' 		=>	($serv['created_at'] <> '') 	?	$serv['created_at']		:	'',
			];

		}else{

		 	return ['status' => false, 'client' => $client, 'cant' => "0", 'ssid' => "", 'payment' => "1", 'red' => "0", 'password' => "", 'service' => "1", 'order_type' => "1", 'origen' => "1", 'ref' => "0", 'ref_list' => "0", 'sup'	=> "0", 'sup_list' => "1"];				
		}
	}

}