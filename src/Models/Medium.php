<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Medium;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Medium extends Model {

	static $_table 		= 'data_mkt_medium';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id');

	public static function GetMedium()
	{
		
		$query 	= 	'SELECT * FROM data_mkt_medium ORDER BY id ASC';
		$med 	=	DBSmart::DBQueryAll($query);
		
		if($med <> false)
		{
			foreach ($med as $k => $val) 
			{
				$medium[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=>	$val['name'],
					'service'  		=>	Service::ServiceById($val['service_id'])['name'],
					'service_di'  	=>	$val['service_id'],
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=>	$val['country_id'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id']
				);
			}
	        
	        return $medium;

		}else{ return false; }
	}

	public static function GetMediumById($id)
	{
		
		$query 	= 	'SELECT * FROM data_mkt_medium WHERE id = "'.$id.'" ORDER BY id ASC';
		$med 	=	DBSmart::DBQuery($query);
		
		if($med <> false)
		{
			return array(
			 	'id'           => $med['id'],
			 	'name'         => $med['name'],
			 	'service'      => $med['service_id'],
			 	'country'      => $med['country_id'],
			 	'status'       => $med['status_id']
			);

		}else{ return false; }

	}

	public static function GetMediumByCountryID($id)
	{

		$query 	= 	'SELECT id, name, status_id FROM data_mkt_medium WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$med 	=	DBSmart::DBQueryAll($query);
		
		if($med <> false)
		{
			foreach ($med as $k => $val) 
			{
				$medio[$k] = [
					'id'		=>	$val['id'],
					'name'		=>	$val['name'],
					'status'	=>	$val['status_id']
				];
			}
			return $medio;
		}else{ return false; }
	}

	public static function GetMediumByCountry($id)
	{
		$query 	= 	'SELECT id, name FROM data_mkt_medium WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$med 	=	DBSmart::DBQueryAll($query);

		$html 	= 	"";

		if($med <> false)
		{
			foreach ($med as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			return $html;

		}else{ return $html; }
	}

	public static function GetMediumByName($info)
	{

		$query 	= 	'SELECT * FROM data_mkt_medium WHERE name = "'.$info['name_me'].'" AND service_id = "'.$info['service_id_me'].'" ORDER BY id ASC';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false )
		{
			return array('id' => $serv['id'], 'name' => $serv['name'], 'service_id' => $serv['service_id'], 'country_id' => $serv['country_id'],'status' => $serv['status_id']);
			
		}else{	return false; 	}
	}

	public static function SaveMedium($info)
	{
		$date 		= date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_me']));

		if($info['type_me'] == 'new')
		{
			$query 	= 	'INSERT INTO data_mkt_medium(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_me'].'", "'.$info['country_id_me'].'" "'.$info['status_id_me'].'", "'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? $dep['last_id'] : false;
		}
		elseif ($info['type_me'] == 'edit') 
		{
			$query 	=	'UPDATE data_mkt_medium SET name="'.$name.'", service_id="'.$info['service_id_me'].'", country_id="'.$info['country_id_me'].'", status_id="'.$info['status_id_me'].'" WHERE id = "'.$info['id_me'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}