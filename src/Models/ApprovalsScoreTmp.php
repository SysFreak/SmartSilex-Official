<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Bank;
use App\Models\CypherData;
use App\Models\Country;
use App\Models\Town;
use App\Models\Approvals;
use App\Models\ApprovalsScore;
use App\Models\ApprovalsScoreTmp;
use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\Leads;
use App\Models\User;
use App\Models\Departament;
use App\Models\Service;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;


use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class ApprovalsScoreTmp extends Model 
{

	static $_table 		= 'cp_appro_score_tmp';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'appro_id', 'service_id', 'score', 'transactions', 'value', 'fico', 'operator_id', 'created_at');

	public static function SaveScore($info)
	{
		
		$date	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_appro_score_tmp(ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at) VALUES ( "'.$info['ticket'].'", "'.$info['client'].'", "'.$info['appro'].'", "'.$info['service'].'", "'.$info['score'].'", "'.$info['transactions'].'", "'.$info['value'].'", "'.$info['fico'].'", "'.$info['operator'].'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecute($query);

		return 	($dep <> false ) ? true : false;
	}

	public static function SaveScoreCopy($info)
	{
		
		$date	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_appro_score_tmp(ticket, client_id, appro_id, service_id, score, transactions, value, fico, operator_id, created_at) VALUES ( "'.$info['ticket'].'", "'.$info['client'].'", "'.$info['appro'].'", "'.$info['service'].'", "'.$info['score'].'", "'.$info['transaction'].'", "'.$info['value'].'", "'.$info['fico'].'", "'.$info['operator'].'", "'.$info['created'].'")';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return 	($dep <> false ) ? $dep : false;
	}

	public static function GetLastScoreTicket($info)
	{
		
		$query 	=	'SELECT ticket, client_id, service_id, score, transactions, value, fico, operator_id, created_at FROM cp_appro_score_tmp WHERE ticket = "'.$info.'" ORDER BY id DESC';
		$sco 	=	DBSmart::DBQuery($query);

		if($sco <> false)
		{
			return [
				'status'		=>	true,
				'ticket'		=>	$sco['ticket'],
				'client'		=>	$sco['client_id'],
				'service'		=>	$sco['service_id'],
				'score'			=>	$sco['score'],
				'transactions'	=>	$sco['transactions'],
				'value'			=>	$sco['value'],
				'fico'			=>	$sco['fico'],
				'operator'		=>	$sco['operator_id'],
				'created'		=>	$sco['created_at']
			];

		}else{	

			return [
				'status'		=>	true,
				'ticket'		=>	"",
				'client'		=>	"",
				'service'		=>	"",
				'score'			=>	"",
				'transactions'	=>	"",
				'value'			=>	"",
				'fico'			=>	"",
				'operator'		=>	"",
				'created'		=>	""
			];
		}
	}

}