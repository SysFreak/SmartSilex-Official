<?php
namespace App\Models;

use Model;

use App\Models\ServiceSecPr;
use App\Models\Camera;
use App\Models\Dvr;
use App\Models\User;
use App\Models\OrderType;
use App\Models\LeadsProvider;

use App\Lib\Config;
use App\Lib\DBSmart;
use PDO;

class ServiceSecPr extends Model 
{

	static $_table = 'cp_service_pr_sec';
	
	Public $_fillable = array('ticket', 'client_id', 'previously_id', 'equipment', 'cameras_id', 'comp_equipment', 'dvr_id', 'add_equipment', 'password_alarm', 'payment_id', 'ref_id', 'ref_list_id', 'sup_id', 'sup_ope_id', 'service_id', 'type_order_id', 'origen_id', 'contact_1', 'contact_1_desc', 'contact_2', 'contact_2_desc', 'contact_3', 'contact_3_desc', 'operator_id', 'created_at');

	public static function GetTicket($info)
	{
		$query 	= 	'SELECT ticket FROM cp_service_pr_sec ORDER BY ticket DESC';
		$tic 	=	DBSmart::DBQuery($query);

		if($tic <> false)
		{
			$tick = (substr($tic['ticket'], 3, strlen($tic['ticket'])) + 1);
			return "SEC".$tick;
		}else{
			return "SEC10001";
		}
	}

	public static function GetServiceByTicket($ticket)
	{
		$query 	= 	'SELECT * FROM cp_service_pr_sec WHERE ticket = "'.$ticket.'"';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{	
			return [
				'ticket'		=>	$serv['ticket'],
				'client'		=>	$serv['client_id'],
				'previously'	=>	$serv['previously_id'],
				'equipment'		=>	$serv['equipment'],
				'cameras'		=>	$serv['cameras_id'],
				'comp_equ'		=>	$serv['comp_equipment'],
				'dvr'			=>	$serv['dvr_id'],
				'add_equip'		=>	$serv['add_equipment'],
				'pass_alarm'	=>	$serv['password_alarm'],
				'payment'		=>	$serv['payment_id'],
				'ref'			=>	$serv['ref_id'],
				'ref_name'		=>	$serv['ref_id'],
				'ref_client'	=>	$serv['ref_id'],
				'sup'			=> 	$serv['sup_id'],
				'sup_pro'		=>	$serv['sup_id'],	
				'sup_apo'		=>	$serv['sup_ope_id'],
				'service'		=>	$serv['service_id'],
				'cont_1'		=>	$serv['contact_1'],
				'cont_1_desc'	=>	$serv['contact_1_desc'],
				'cont_2'		=>	$serv['contact_2'],
				'cont_2_desc'	=>	$serv['contact_2_desc'],
				'cont_3'		=>	$serv['contact_3'],
				'cont_3_desc'	=>	$serv['contact_3_desc'],
				'operator_id'	=>	$serv['operator_id'],
				'created'		=>	$serv['created_at']

			];

		}else{	return false;	}
	}

	public static function GetService($ticket)
	{

        $query 		=	'SELECT t1.id, t1.ticket, t1.created_at, t1.client_id, t3.name, t3.phone_main as phone, t1.service_id, t5.name as service, (SELECT username FROM users WHERE id = t2.operator_id) as sale, t6.name as departament, t1.status_id, (SELECT name FROM data_score WHERE id = t1.score) AS score, (SELECT IF(t1.status_id = 1, "PENDIENTE", "PROCESADO")) AS status, (SELECT username FROM users WHERE id = t1.operator_id) AS operator, t2.created_at AS date_process FROM cp_appro_serv AS t1 INNER JOIN cp_service_pr_sec AS t2 ON (t1.ticket = t2.ticket) INNER JOIN cp_leads as t3 ON (t1.client_id = t3.client_id) INNER JOIN users as t4 ON (t2.operator_id = t4.id) INNER JOIN data_services AS t5 ON (t1.service_id = t5.id) INNER JOIN data_departament as t6 ON (t4.departament_id = t6.id) INNER JOIN cp_appro_score AS t7 ON (t1.score_id = t7.id) AND t1.ticket = "'.$ticket.'"';

		$result 	=	DBSmart::DBQuery($query);

        if($result <> false)
        {
        	$staHtml = ($result['status_id'] == 1) 	? 
        		'<span class="label label-warning">'.$result['status'].'</span>': 
        		'<span class="label label-success">'.$result['status'].'</span>';

			return	[
				'id'			=>	$result['id'],
	    		'ticket'		=>	$result['ticket'],
	    		'created'		=>	$result['created_at'],
	    		'client'		=>	$result['client_id'],
	    		'name'			=>	$result['name'],
	    		'phone'			=>	$result['phone'],
	    		'serv'			=>	$result['service_id'],
	    		'service'		=>	$result['service'],
	    		'servHtml'		=>	'<span class="label label-danger">'.$result['service'].'</span>',
	    		'sale'			=>	$result['sale'],
	    		'departament'	=>	$result['departament'],
	    		'status'		=>	$result['status'],
	    		'statusHtml'	=>	$staHtml,
	    		'score'			=>	$result['score'],
	    		'operator'		=>	$result['operator'],
	    		'date_process'	=>	$result['date_process']
	    	];

        }else{ 	return false;	}
	}

	public static function GetSubmit($info, $user)
	{
		$query 	=	'SELECT * FROM cp_service_pr_sec WHERE ticket = "'.$info.'"';
		$sub 	=	DBSmart::DBQuery($query);

		if($sub <> false)
		{
			if($sub['sup_id'] == 0)
			{$pro 	=	$user; $apo 	=	$user; }
			else
			{ $pro 	=	User::GetUserById($sub['sup_ope_id'])['username']; $apo 	=	$user;}

			$data =	[
				'ticket'		=>	$sub['ticket'],
				'client'		=>	$sub['client_id'],
				'previosly'		=>	($sub['previously_id'] <> 0) 	? "SI" : 	"NO",
				'equipment'		=>	$sub['equipment'],
				'cameras'		=>	($sub['cameras_id'] <> 0) 		? Camera::GetCameraById($sub['cameras_id'])['name'] 			: "NO",
				'com_equip'		=>	$sub['comp_equipment'],
				'dvr'			=>	($sub['dvr_id'] <> 0) 			? Dvr::GetDvrById($sub['dvr_id'])['name'] 						: "NO",
				'add_equip'		=>	$sub['add_equipment'],
				'pass_alarm'	=>	$sub['password_alarm'],
				'payment'		=>	($sub['payment_id'] <> 0)		? Payment::GetPaymentById($sub['payment_id'])['name']			: "NO",
				'ref'			=>	($sub['ref_id'] == 1) 			? "SI" :	"NO",
				'ref_name'		=>	($sub['ref_id'] == 1) 			? Referred::GetRefId($sub['ref_list_id'])['name']  				: "NO",
				'ref_client'	=>	($sub['ref_id'] == 1) 			? Referred::GetRefId($sub['ref_list_id'])['client']				: "NO",
				'sup'			=> 	($sub['sup_id'] == 1) 			? "SI" : 	"NO",
				'sup_pro'		=>	$pro,	
				'sup_apo'		=>	$apo,
				'cont1'			=>	$sub['contact_1'],
				'cont1des'		=>	$sub['contact_1_desc'],
				'cont2'			=>	$sub['contact_2'],
				'cont2des'		=>	$sub['contact_2_desc'],
				'cont3'			=>	$sub['contact_3'],
				'cont3des'		=>	$sub['contact_3_desc'],
				'service'		=>	StatusService::GetStatusById($sub['service_id'])['name'],
				'order_type'	=>	($sub['type_order_id'])			? $sub['type_order_id'] 											: 	"",
				'o_type'		=>	($sub['type_order_id'])			? OrderType::GetOrderTypeById($sub['type_order_id'])['name'] 		: 	"",
				'origen'		=>	($sub['origen_id'])				? $sub['origen_id'] 												: 	"",
				't_origen'		=>	($sub['origen_id'])				? LeadsProvider::GetLeadsProviderById($sub['origen_id'])['name'] 	: 	"",
				'created'		=>	$sub['created_at']
			];
			return $data;
		}else{
			return false;
		}

	}

	public static function SaveService($ticket, $info)
	{
		$date = date('Y-m-d h:i:s', time());

		$user =	($info['sup'] == 0) ? $info['operator'] : $info['sup_list'];

		$query =	'INSERT INTO cp_service_pr_sec(ticket, client_id, previously_id, equipment, cameras_id, comp_equipment, dvr_id, add_equipment, password_alarm, payment_id, ref_id, ref_list_id, sup_id, sup_ope_id, service_id, type_order_id, origen_id, contact_1, contact_1_desc, contact_2, contact_2_desc, contact_3, contact_3_desc, operator_id, created_at) VALUES ("'.$ticket.'", "'.$info['client'].'", "'.$info['previosly'].'", "'.$info['equipment'].'", "'.$info['cameras'].'", "'.$info['comp_equip'].'", "'.$info['dvr'].'", "'.$info['add_equip'].'", "'.$info['pass_alarm'].'", "'.$info['payment'].'", "'.$info['ref_id'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$info['service'].'", "'.$info['order_type'].'", "'.$info['origen'].'", "'.$info['cont1'].'", "'.$info['cont1desc'].'", "'.$info['cont2'].'", "'.$info['cont2desc'].'", "'.$info['cont3'].'", "'.$info['cont3desc'].'", "'.$user.'", "'.$date.'") ';

		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	}

	public static function UpdateServiceCoord($iData)
	{
		$query 	=	'UPDATE cp_service_pr_sec SET previously_id="'.$iData['previosly'].'", equipment="'.$iData['equipment'].'", cameras_id="'.$iData['cameras'].'", comp_equipment="'.$iData['comp_equip'].'", dvr_id="'.$iData['dvr'].'", add_equipment="'.$iData['add_equip'].'", password_alarm="'.$iData['pass_alarm'].'", payment_id="'.$iData['payment'].'", contact_1="'.$iData['cont1'].'", contact_1_desc="'.$iData['cont1desc'].'", contact_2="'.$iData['cont2'].'", contact_2_desc="'.$iData['cont2desc'].'", contact_3="'.$iData['cont3'].'", contact_3_desc="'.$iData['cont3desc'].'"  WHERE ticket = "'.$iData['ticket'].'"';

		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	}
}