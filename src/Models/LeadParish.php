<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Town;
use App\Models\LeadStates;
use App\Models\LeadTownShip;
use App\Models\LeadParish;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadParish extends Model 
{

	static $_table 		= 'cp_leads_parish';

	Public $_fillable 	= array('name', 'township_id', 'status_id', 'created_at');

	public static function GetParish()
	{
		$query 	= 	'SELECT id, name, township_id, status_id FROM cp_leads_parish ORDER BY id ASC';
		$cit 	=	DBSmart::DBQueryAll($query);
		
		if($cit <> false)
		{
			foreach ($cit as $k => $val) 
			{
				$city[$k] = array(
					'id' 	 	=> 	$val['id'], 
					'name' 	 	=>	$val['name'],
					'states'	=>	LeadTownShip::GetTownShipById($val['township_id'])['name'],
					'status' 	=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'	=>	$val['status_id']
				);
			}
	        
	        return $city;

		}else{ return false; }
	}

	public static function GetParishById($id)
	{
		$query 		= 	'SELECT id, name, township_id, status_id FROM cp_leads_parish WHERE id = "'.$id.'" ORDER BY id DESC';
		$country 	=	DBSmart::DBQuery($query);

		if($country <> false)
		{
			return [
			 'id'           => $country['id'],
			 'name'         => $country['name'],
			 'states'		=> $country['township_id'],
			 'status'       => $country['status_id']
			];

		}else{ return false; }
	}

	public static function GetListByParish($id)
	{
		$query 	= 	'SELECT id, name, township_id, status_id FROM cp_leads_parish WHERE status_id = "1" AND township_id = "'.$id.'" ORDER BY id DESC';
		$tship 	=	DBSmart::DBQueryAll($query);

		$html 	=	'';
		$html.='<select class="form-control" name="c_parish" id="c_parish">';
		if($tship <> false)
		{
      		foreach ($tship as $t => $tsh) 
      		{
      			$html.='<option value="'.$tsh['id'].'"> '.$tsh['name'].'</option>';
      		}

      	}else{ 
      		$html.='<option value=""> SELECCIONE</option>';
      	}

      	$html.='</select>';
      	return $html;
	
	}

	public static function GetListByParishGen($township, $id)
	{
		$query 	= 	'SELECT id, name, township_id, status_id FROM cp_leads_parish WHERE status_id = "1" AND township_id = "'.$township.'" ORDER BY id DESC';

		$tship 	=	DBSmart::DBQueryAll($query);

		$html 	=	'';
		$html.='<select class="form-control" name="c_parish" id="c_parish">';
		if($tship <> false)
		{
      		foreach ($tship as $t => $tsh) 
      		{
      			if($tsh['id'] == $id)
      			{
	      			$html.='<option value="'.$tsh['id'].'" selected> '.$tsh['name'].'</option>';
      			}else{
      				$html.='<option value="'.$tsh['id'].'"> '.$tsh['name'].'</option>';
      			}
      		}

      	}else{ 
      		$html.='<option value=""> SELECCIONE</option>';
      	}

      	$html.='</select>';
      	return $html;
	
	}

	public static function GetListByParishSelect()
	{
		$html 	=	'';
		$html.='<select class="form-control" name="c_parish" id="c_parish">';
  		$html.='<option value=""> SELECCIONE</option>';
		$html.='</select>';
      	return $html;
	
	}

	public static function InsertParish($iData)
	{
		$query 	= 	'INSERT INTO cp_leads_parish(name, township_id, status_id, created_at) VALUES ("'.strtoupper($iData['name']).'", "'.$iData['township_id'].'", "'.$iData['status_id'].'",  NOW())';
		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}

	public static function UpdateParish($iData, $id)
	{
		$query 	=	'UPDATE cp_leads_parish SET name="'.strtoupper($iData['name']).'", township_id = "'.$iData['township_id'].'", status_id="'.$iData['status_id'].'" WHERE id = "'.$id.'"';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false) ? true : false;
	}

	public static function GetParishHtml($iData)
	{
		$html 	=	'';

		$html.='<div class="widget-body no-padding">';
		$html.='<table id="dt_parish" class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<thead>                     ';
		$html.='<tr>';
		$html.='<th data-hide="user">ID</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-check text-muted hidden-md hidden-sm hidden-xs"></i> Nombre</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-check text-muted hidden-md hidden-sm hidden-xs"></i> Municipio</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-check text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-edit text-muted hidden-md hidden-sm hidden-xs"></i> Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		if($iData <> false)
		{
			$html.='<tbody>';
			foreach ($iData as $k => $dat) 
			{
				$html.='<tr>';
				$html.='<td>'.$dat['id'].'</td>';
				$html.='<td>'.$dat['name'].'</td>';
				$html.='<td>'.$dat['states'].'</td>';
				$html.='<td>'.$dat['status'].'</td>';
				$html.='<td>';
				$html.='<div class="btn-group">';
				$html.='<a class="btn btn-primary btn-xs" onclick="ParishEdit('.$dat['id'].');"><i class="fa fa-edit"></i> Editar </a>';
				$html.='</div>';
				$html.='</td>';
				$html.='</tr>';
			}
			$html.='</tbody>';
			$html.='</table>';
		}else{
			$html.='</tbody>';
			$html.='</table>';
		}
		$html.='</div>';

		return $html;

	}

	public static function GetShowParishHtml($country, $status)
	{
		$html = "";

		$html.='<form class="FormParish" method="post" id="FormParish" data-parsley-validate="">';
		$html.='<div class="modal-body" style="background: aliceblue;">';
		$html.='<div class="row">';
		$html.='<div id="alertStates"></div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-12">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Nombre</label>';
		$html.='<input type="text" id="name" name="name" class="form-control" placeholder="Nombre"required />';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Municipio</label>';
		$html.='<select class="form-control" id="township_id" name="township_id" required style="font-size: 10px;">';
		foreach ($country as $c => $cou) 
		{
			if($cou['status_id'] == "1")
			{	$html.='<option value="'.$cou['id'].'"> '.$cou['name'].' </option>'; }else{
				$html.='<option value="" selected > SELECCIONE </option>';
			}

		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Status</label>';
		$html.='<select class="form-control" id="status_id" name="status_id" required style="font-size: 10px;">';
		foreach ($status as $s => $sta) 
		{
			$html.='<option value="'.$sta['id'].'"> '.$sta['name'].'</option>';
		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="modal-footer">';
		$html.='<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
		$html.='<a class="btn btn-primary" onclick="ParishCreated()">Registrar</a>';
		$html.='</div>';
		$html.='</form>';

		return $html;

	}

	public static function GetParishEditHtml($iData, $country, $status)
	{
		$html = "";

		$html.='<form class="FormParish" method="post" id="FormParish" data-parsley-validate="">';
		$html.='<div class="modal-body" style="background: aliceblue;">';
		$html.='<div class="row">';
		$html.='<div id="alertStates"></div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-12">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Nombre</label>';
		$html.='<input type="text" id="name" name="name" class="form-control" placeholder="Nombre" value="'.$iData['name'].'" required />';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Municipio</label>';
		$html.='<select class="form-control" id="township_id" name="township_id" required style="font-size: 10px;">';
		foreach ($country as $c => $cou) 
		{
			if($iData['states'] == $cou['id'])
			{	$html.='<option value="'.$cou['id'].'" selected > '.$cou['name'].''; }
			else
			{ 	$html.='<option value="'.$cou['id'].'"> '.$cou['name'].''; }
		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Status</label>';
		$html.='<select class="form-control" id="status_id" name="status_id" required style="font-size: 10px;">';
		foreach ($status as $s => $sta) 
		{
			if($iData['status'] == $sta['id'])
			{	$html.='<option value="'.$sta['id'].'" selected > '.$sta['name'].''; }
			else
			{ 	$html.='<option value="'.$sta['id'].'"> '.$sta['name'].''; }
		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="modal-footer">';
		$html.='<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
		$html.='<a class="btn btn-primary" onclick="ParishUpdate('.$iData['id'].')">Actualizar</a>';
		$html.='</div>';
		$html.='</form>';

		return $html;
	
	}


}