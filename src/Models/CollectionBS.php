<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\DBMikroVE;
use App\Lib\UnCypher;

class CollectionBS extends Model 
{

	static $_table 		= 'cp_collection_dolar';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'appro_id', 'service_id', 'score', 'transactions', 'value', 'fico', 'operator_id', 'created_at');

	public static function InsertDollar($iData)
	{
		$query 		=	'INSERT INTO cp_collection_dolar (base, increment, total, type, created_by, created_at) VALUES ("'.$iData['base'].'", "'.$iData['agregado'].'", "'.$iData['total'].'", "'.$iData['type'].'", "'.$iData['operator'].'", "'.$iData['date'].'")';

		$insert 	=	DBSmart::DataExecute($query);

		return ($insert <> false ) ? true : false;

	}

	public static function SearchtDollar()
	{
		$query 		=	'SELECT t1.base, t1.increment, t1.total, t1.type, t1.created_at AS date, t2.username AS operador FROM cp_collection_dolar AS t1 INNER JOIN users AS t2 ON (t1.created_by = t2.id) ORDER BY t1.created_at DESC';

		$insert 	=	DBSmart::DBQueryAll($query);

		return ($insert <> false ) ? $insert : false;

	}

    public static function SearchtDollarDate($iDate)
    {
        $query      =   'SELECT * FROM cp_collection_dolar WHERE created_at LIKE "'.$iDate.'%" ';

        $insert     =   DBSmart::DBQueryAll($query);

        return ($insert <> false ) ? $insert : false;

    }

	public static function DolarTable($iData)
	{
		$html = '';

		$html.='<table id="TableDataDollar" class="table table-bordered" border="1" style="font-size: 10px; text-align: center;">';
			$html.='<thead>';
				$html.='<tr>';
					$html.='<th style="text-align: center;"><strong>#</strong></th>';
					$html.='<th style="text-align: center;"><strong>Fecha</strong></th>';
					$html.='<th style="text-align: center;"><strong>Base</strong></th>';
					$html.='<th style="text-align: center;"><strong>Agregado</strong></th>';
					$html.='<th style="text-align: center;"><strong>Calculado</strong></th>';
					$html.='<th style="text-align: center;"><strong>Tipo</strong></th>';
					$html.='<th style="text-align: center;"><strong>Operdor</strong></th>';
				$html.='</tr>';
			$html.='</thead>';
			$html.='<tbody>';
			if($iData <> false)
			{
				foreach ($iData as $k => $val) 
				{
					$html.='<tr>';
						$html.='<td>'.$k.'</td>';
						$html.='<td>'.$val['date'].'</td>';
						$html.='<td>'.$val['base'].'</td>';
						$html.='<td>'.$val['increment'].'</td>';
						$html.='<td>'.$val['total'].'</td>';
						$html.='<td>'.$val['type'].'</td>';
						$html.='<td>'.$val['operador'].'</td>';
					$html.='</tr>';
				}

			}else{
				$html.='<tr>';
				$html.='<td colspan="7"> SIN INFORMACION PARA MOSTRAR </td>';
				$html.='</tr>';
			}
			$html.='</tbody>';
		$html.='</table>';

		return $html;
	
	}

    public static function SaverUploadFiles($file, $id)
    {
        // $query      = "TRUNCATE TABLE cp_mw_payment_bs_tmp";
        // $truncate   = DBSmart::DataExecute($query);

        $csv    =   [];
        $iData  =   [];

        if($file['csv']['type'] == 'application/vnd.ms-excel')
        {
            $name       =   $file['csv']['name'];
            $ext        =   'csv';
            $type       =   $file['csv']['type'];
            $tmpName    =   $file['csv']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                set_time_limit(0);
                $row = 0;

                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $col_count = count($data); 

                    if($row <> 0)
                    {

                    	if( ($data[0] <> '') AND ($data[1] <> '') AND ($data[3] <> '') )
                    	{
                            $amount     =   ($data[3] <> '')    ?   str_replace(',', '', $data[3])          :   '';

                            $cod        =   ($data[1] <> '')    ?   str_replace("'","", $data[1])            :   '';


                            if( (strlen($cod) <= 20) AND (strlen($cod) >= 16)) 
                            {
                                $code   =   substr($cod,-16);

                            }elseif( strlen($cod) < 16){

                                $code   =   (strlen($cod) < 16) ?   str_pad($cod, 16, "0", STR_PAD_LEFT)    :  str_pad($cod, (16-strlen($cod)), "0", STR_PAD_LEFT);  

                            }else{

                                $code   =   (strlen($cod) < 16) ?   str_pad($cod, 16, "0", STR_PAD_LEFT)    :  str_pad($cod, (16-strlen($cod)), "0", STR_PAD_LEFT);

                            }

	                        $csv[$row]['date']     =   ($data[0] <> '')    ? (date("Y-m-d", strtotime($data[0]))) : '';
	                        $csv[$row]['code']     =   $code;
	                        $csv[$row]['amount']   =   ($data[3] <> '')    ? str_replace(',', '.', $amount) : '';

                            $dup    =   CollectionBS::SearchPaymentCodes(substr($csv[$row]['code'], -9));

                            if($dup == false)
                            {
                                $query      =   'INSERT INTO cp_mw_payment_bs(fecha, code, amount, status_id, created_by, created_at, consolide_id, asigned_by, asigned_at) VALUES ("'.$csv[$row]['date'].'", "'.$csv[$row]['code'].'", "'.$csv[$row]['amount'].'", "1", "'.$id.'", NOW(), "0", "1", "")';

                                $insert     =   DBSmart::DataExecute($query);
                                
                                $csv[$row]['status']   =   'AGREGADO';

                            }else{
                                $csv[$row]['status']   =   'DUPLICADO';

                            }

                    	}

                    }
                    $row++;

                }

                return $csv;
            
            }else{
                return false;

            }
        
        }else{
            return false;
        }
    
    }

    public static function UploadFilesHtml($iData)
    {

        $html = '';
        $html.='<table id="bs_codes" class="table table-bordered table-striped" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Fecha de Pago</th>';
        $html.='<th style="text-align: center;">C&oacute;digo</th>';
        $html.='<th style="text-align: center;">Monto</th>';
        $html.='<th style="text-align: center;">Status</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody style="text-align: center;">';

        foreach ($iData as $k => $val) 
        {
            $html.='<tr>';
            $html.='<td>'.$val['date'].'</td>';
            $html.='<td>'.$val['code'].'</td>';
            $html.='<td><input style="text-align: center; class="input-sm" type="number" value="'.$val['amount'].'"></td>';
            $html.='<td>'.$val['status'].'</td>';
            $html.='</tr>';
        }
                          
        $html.='</tbody>';
        $html.='</table>';
        return $html;
    
    }

    public static function  SearchPaymentCodes($code)
    {
        $query  =   'SELECT id, fecha, code, amount FROM cp_mw_payment_bs WHERE code LIKE "%'.$code.'%"';
        return  DBSmart::DBQuery($query);
    
    }

    public static function  SearchPaymentDate($date)
    {
        $query  =   'SELECT id, fecha, code, amount, created_at FROM cp_mw_payment_bs WHERE created_at LIKE "'.$date.'%"';
        return  DBSmart::DBQueryAll($query);
    
    }

    public static function  SearchCodeStatus($status)
    {
        $query  =   'SELECT id, fecha, code, amount, created_at FROM cp_mw_payment_bs WHERE status_id = "'.$status.'" ORDER BY id DESC';
        return  DBSmart::DBQueryAll($query);
    
    }

    public static function UpdateCodeLotStatus($id, $iOpe, $status)
    {
        $query  =   'UPDATE cp_mw_payment_bs SET asigned_by = "'.$iOpe.'", asigned_at = NOW(), status_id = "'.$status.'" WHERE id="'.$id.'"';
        $upd    =   DBSmart::DataExecute($query);

    }



    public static function SelectCodeLotStatusId($id)
    {
        $query  =   'SELECT * FROM cp_mw_payment_bs WHERE id = "'.$id.'"';
        $iData  =   DBSmart::DBQuery($query);  

        return  ($iData <> false) ? $iData : false;
    
    }

    public static function InsertCodeManual($iData)
    {
        $query      =   'INSERT INTO cp_mw_payment_bs(fecha, code, amount, status_id, created_by, created_at, consolide_id, asigned_by, asigned_at) VALUES ("'.$iData['date'].'", "'.$iData['code'].'", "'.$iData['amount'].'", "1", "'.$iData['ope'].'", "'.$iData['date'].'", "0", "1", "")';

        return      DBSmart::DataExecute($query);
    }

    public static function PendingCodeHtml($iData)
    {

        $html = '';
        $html.='<table id="TablePending" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;"></th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">C&oacute;digo</th>';
        $html.='<th style="text-align: center;">BS</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody style="text-align: center; font-size: 10px;">';
            foreach ($iData as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;"><label class="checkbox"><input type="checkbox" name="LotPeng'.$val['id'].'" id="LotPeng'.$val['id'].'"><i></i></label></td>';
                $html.='<td>'.$val['created_at'].'</td>';
                $html.='<td>'.$val['code'].'</td>';
                $html.='<td>'.$val['amount'].'</td>';
                $html.='</tr>';
            
            }

            $html.='</tbody>';
            $html.='</table>';
        }else{
            $html.='</tbody>';
            $html.='</table>';
        }

        return $html;
    
    }

    public static function AnexCodeHtml($iData)
    {

        $html = '';
        $html.='<table id="TableConsolidateAnex" class="table table-bordered table-striped">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Acci&oacute;n</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">C&oacute;digo</th>';
        $html.='<th style="text-align: center;">BS</th>';
        $html.='<th style="text-align: center;">Status</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody style="text-align: center;">';
            foreach ($iData as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td><button type="button" class="btn btn-default btn-xs btn-block" id="AddCodeAsig'.(int)$val['id'].'" onclick="BtnPenCodeAsig('.(int)$val['id'].')"> Asignar </button></td>';
                $html.='<td>'.$val['created_at'].'</td>';
                $html.='<td>'.$val['code'].'</td>';
                $html.='<td>'.$val['amount'].'</td>';
                $html.='<td><div id="status'.(int)$val['id'].'"></div></td>';
                $html.='</tr>';
            
            }

            $html.='</tbody>';
            $html.='</table>';
        }else{
            $html.='</tbody>';
            $html.='</table>';
        }

        return $html;
    
    }

    public static function ClientsReportsPay()
    {
        $query  =   'SELECT id, idcliente, (SELECT nombre FROM usuarios WHERE id = idcliente) AS nombre, nfactura, (SELECT total FROM facturas WHERE id = nfactura) AS total, fecha, asunto, transaccion, total AS tTrans, fecha2 AS fReporte, (CASE WHEN estado = "1" THEN "PENDIENTE" WHEN estado = "0" THEN "PROCESADA" ELSE "OTRO" END) AS estado FROM reporte WHERE estado = "1"  ORDER BY fecha2 DESC';
        return  DBMikroVE::DBQueryAll($query);
    
    }

    public static function ClientsPay($cl)
    {
        $query  =   'SELECT id, idcliente, (SELECT nombre FROM usuarios WHERE id = idcliente) AS nombre, nfactura, (SELECT total FROM facturas WHERE id = nfactura) AS total, fecha, asunto, transaccion, total AS tTrans, fecha2 AS fReporte, (CASE WHEN estado = "1" THEN "PENDIENTE" WHEN estado = "0" THEN "PROCESADA" ELSE "OTRO" END) AS estado FROM reporte WHERE estado = "1" AND idcliente="'.$cl.'"';

        return  DBMikroVE::DBQuery($query);
    
    }


    public static function UpdateReportMW($bill)
    {
        $query  =   'UPDATE reporte SET estado="0" WHERE nfactura = "'.$bill.'"';

        return  DBMikroVE::DataExecute($query);
   
    }

    public static function NoteMikrowisp($iData, $status)
    {
        $in      =  '&lt;p&gt;';
        $fn      =  '&lt;/p&gt;';

        switch ($status) 
        {
            case 0:
                $asunto  =  'Asignacion Codigo - Codigo de Transferencia - BS/US';
                $mensaje =  ''.$in.'Se asigna codigo de Transferencia Bajo los siguientes parametros - Codigo: '.$iData['code'].' - Monto: '.$iData['total'].' - Fecha: '.$iData['fecha'].' - Factura: '.$iData['factura'].' - Dolar: '.$iData['dolar'].' - Operador - '.$iData['user'].' '.$fn.'';
                break;
            
            case 1:
                $asunto  =  'Asignacion Manual - Codigo de Transferencia - BS/US';
                $mensaje =  ''.$in.'Se asigna codigo de Transferencia Bajo los siguientes parametros - Codigo: '.$iData['code'].' - Monto: '.$iData['total'].' - Fecha: '.$iData['fecha'].' - Factura: '.$iData['factura'].' - Dolar: '.$iData['dolar'].' - Operador - '.$iData['user'].' '.$fn.'';
                break;

            case 2:
                $asunto  =  'Pago de Factura - BS/US';
                $mensaje =  ''.$in.'Se realiza Pago de factura bajo los siguientes Parametros - Codigo: '.$iData['code'].' - Monto: '.$iData['total'].' - Factura: '.$iData['factura'].' - Dolar: '.$iData['dolar'].' - Operador - '.$iData['user'].' '.$fn.'';
                break;
        }

        $query  =   'INSERT INTO notas(idcliente, asunto, mensaje, fecha, adjuntos, idoperador, imagen, tipo, portal) VALUES ("'.$iData['client'].'", "'.$asunto.'", "'.$mensaje.'", NOW(), "", "44", "", "0", "")';

        return  DBMikroVE::DataExecute($query);

    }

    public static function ClientsReporttHtml($iData)
    {
        $_replace   =   new Config();
        $html       =   "";

        $html.='<div class="table-responsive">';
        $html.='<table id="TableReporting" class="table table-bordered" border="1" style="font-size: 12px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Factura</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Codigo</th>';
        $html.='<th style="text-align: center;">Total Trans.</th>';
        $html.='<th style="text-align: center;">Fecha Trans.</th>';
        $html.='<th style="text-align: center;">Accion</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;">'.$inf['idcliente'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nombre'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nfactura'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['total'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['fecha'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['transaccion'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['tTrans'].'</td>';
                $html.='<td style="text-align: center;">'.$_replace->ShowDate($inf['fReporte']).'</td>';
                $html.='<td style="text-align: center;"><a class="btn btn-success btn-xs btn-block" onclick="AsigRepClient('.(int)$inf['idcliente'].');">Asignar</a></td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }
        $html.='</div>';
        
        return  $html;
    
    }

    public static function GetClientsData($id)
    {
        $query    =   'SELECT id, nombre, estado, telefono, cedula FROM usuarios WHERE id = "'.$id.'" ORDER BY id ASC';

        return      DBMikroVE::DBQuery($query);
    
    }

    public static function ConsolideReporting($iCode, $iClient)
    {
        $tTotal     =   false;

        foreach ($iCode as $i => $cod) 
        {            
            foreach ($iClient as $c => $cli) 
            {

                if( (strlen($cli['transaccion']) >= 5) )
                {

                   if(strpos(substr(strtoupper($cod['code']), -7), substr(strtoupper($cli['transaccion']), -7) ) !== false)
                    {

                        $iTime  =   ['date' =>  substr($cli['fReporte'],0, 10), 'type'   =>  CollectionBS::CheckTime(substr($cli['fReporte'], -8)) ];
                        $dol    =   CollectionBS::SearchDollarDateTime($iTime);

                        // $dolar['total']  =   ($dol <> false) ? $dol : 1;
                        
                        $dolar  =   [
                            'total'     =>  ( ($dol <> false) ? $dol['total']   :  1),  
                            'type'      =>  ( ($dol <> false) ? $dol['type']    : $iTime['type']) 
                        ];

                        if($dolar['total'] <> 1)
                        {
                            $tTotal[$i] =   [
                                'id'        =>  $cli['id'], 
                                'client'    =>  $cli['idcliente'],
                                'asunto'    =>  $cli['asunto'],
                                'name'      =>  CollectionBS::GetClientsData($cli['idcliente'])['nombre'],
                                'fdate'     =>  $cli['fReporte'],
                                'dtran'     =>  $cod['code'],
                                'ctran'     =>  $cli['transaccion'],
                                'factura'   =>  $cli['nfactura'],
                                'total'     =>  $cli['total'],
                                'amount'    =>  $cod['amount'],
                                'status'    =>  "1",
                                'dolar'     =>  $dolar['total'],
                                'dolartype' =>  $dolar['type'],
                                'tpay'      =>  number_format(round(($cod['amount'] / $dolar['total']),2), 2)
                            ];
                        }

                    }

                }

            }

        }

        return $tTotal;
    
    }

    public static function CheckTime($iTime)
    {

        $time   =   strtotime($iTime);
        
        if( ($time >= strtotime("00:00:00")) AND ($time <= strtotime("13:29:59")) )
        {
            return "AM";
        }
        elseif ( ($time >= strtotime("13:30:00")) AND ($time <= strtotime("23:59:59")) ) 
        {
            return "PM";
        }
    
    }

    public static function SearchDollarDateTime($iTime)
    {

        $query      =   'SELECT * FROM cp_collection_dolar WHERE created_at = "'.$iTime['date'].'" AND type = "'.$iTime['type'].'"';
        $insert     =   DBSmart::DBQuery($query);

        return ($insert <> false ) ? $insert : false;

    }

    public static function InsertConsolidate($iData, $iOpe)
    {
        foreach ($iData as $i => $dat) 
        {

            $query  =   'SELECT * FROM cp_mw_consolidate_bs WHERE idcliente = "'.$dat['client'].'" AND ftransaccion = "'.$dat['ctran'].'"';
            $ok     =   DBSmart::DBQuery($query);

            if($ok == false)
            {
                $query  =   'INSERT INTO cp_mw_consolidate_bs (idcliente, asunto, name, nfactura, ftotal, fdate, ftransaccion, code, amount, dolar, dolartime, dtotal, cdate, status_id, img, type, size, created_by, created_at, process_by, process_at) VALUES ("'.$dat['client'].'", "'.$dat['asunto'].'", "'.$dat['name'].'", "'.$dat['factura'].'", "'.$dat['total'].'", "'.$dat['fdate'].'", "'.$dat['ctran'].'", "'.$dat['dtran'].'", "'.$dat['amount'].'", "'.$dat['dolar'].'", "'.$dat['dolartype'].'", "'.$dat['tpay'].'", NOW(), "1", "", "", "", "'.$iOpe.'", NOW(), "1", "")';

                $insert =   DBSmart::DataExecuteLastID($query);

                $iData  =   [
                    'code'              =>  $dat['dtran'],
                    'amount'            =>  $dat['amount'],
                    'cdate'             =>  $dat['fdate'],
                    'dolar'             =>  $dat['dolar'],
                    'dolartime'         =>  $dat['dolartype'],
                    'dtotal'            =>  number_format(round(($dat['amount'] / $dat['dolar']),2), 2),
                    'consolidate_id'    =>  $insert,
                    'created_by'        =>  $iOpe,
                ];

                $insert     =   CollectionBS::InsertAddCode($iData);

                $query  =   'UPDATE cp_mw_payment_bs SET asigned_by = "'.$iOpe.'", asigned_at = NOW(), status_id = "2" WHERE code LIKE "%'.substr($dat['ctran'], -6).'%"';
                $upd    =   DBSmart::DataExecute($query);
            }


        }

        return $insert;
    
    }

    public static function InsertConsolidateSingle($dat, $iOpe)
    {
        $query  =   'SELECT * FROM cp_mw_consolidate_bs WHERE idcliente = "'.$dat['client'].'" AND ftransaccion = "'.$dat['ctran'].'"';
        $ok     =   DBSmart::DBQuery($query);

        if($ok == false)
        {
            $query  =   'INSERT INTO cp_mw_consolidate_bs (idcliente, asunto, name, nfactura, ftotal, fdate, ftransaccion, code, amount, dolar, dolartime, dtotal, cdate, status_id, img, type, size, created_by, created_at, process_by, process_at) VALUES ("'.$dat['client'].'", "'.$dat['asunto'].'", "'.$dat['name'].'", "'.$dat['factura'].'", "'.$dat['total'].'", "'.$dat['fdate'].'", "'.$dat['ctran'].'", "'.$dat['dtran'].'", "'.$dat['amount'].'", "'.$dat['dolar'].'", "'.$dat['dolartype'].'", "'.$dat['tpay'].'", NOW(), "1", "", "", "", "'.$iOpe.'", NOW(), "1", "")';

            $insert =   DBSmart::DataExecuteLastID($query);

            $iData  =   [
                'code'              =>  $dat['dtran'],
                'amount'            =>  $dat['amount'],
                'cdate'             =>  $dat['fdate'],
                'dolar'             =>  $dat['dolar'],
                'dolartime'         =>  $dat['dolartype'],
                'dtotal'            =>  number_format(round(($dat['amount'] / $dat['dolar']),2), 2),
                'consolidate_id'    =>  $insert,
                'created_by'        =>  $iOpe,
            ];

            $insert     =   CollectionBS::InsertAddCode($iData);

            $query  =   'UPDATE cp_mw_payment_bs SET asigned_by = "'.$iOpe.'", asigned_at = NOW(), status_id = "2" WHERE code LIKE "%'.substr($dat['ctran'], -6).'%"';
            $upd    =   DBSmart::DataExecute($query);

        }

        return ($insert <> false) ? true : false;
    }

    public static function SearchConsolidateStatus($status)
    {
        $query      =   'SELECT * FROM cp_mw_consolidate_bs WHERE status_id = "'.$status.'" ORDER BY id DESC';
        $search     =   DBSmart::DBQueryAll($query);
        
        return  ( $search <> false ) ? $search : false;
    
    }

    public static function SearchConsolidateId($id)
    {
        $query  =   'SELECT * FROM cp_mw_consolidate_bs WHERE id="'.$id.'"';
        return  DBSmart::DBQuery($query);

    }

    public static function SearchConsolidateClient($client)
    {
        $query  =   'SELECT * FROM cp_mw_consolidate_bs WHERE idcliente LIKE "%'.$client.'%" AND status_id = "1"';
        $search     =   DBSmart::DBQuery($query);
        
        return  ( $search <> false ) ? $search : false;

    }

    public static function UpdateConsolidate($iData)
    {
        $query  =   'UPDATE cp_mw_consolidate_bs SET status_id = "2", process_by = "'.$iData['ope'].'", process_at = NOW() WHERE id = "'.$iData['id'].'"';

        return  DBSmart::DataExecute($query);

    }

    public static function UpdConsolidateSuspend($iData)
    {
        $query  =   'UPDATE cp_mw_consolidate_bs SET status_id = "3", process_by = "'.$iData['ope'].'", process_at = NOW() WHERE id = "'.$iData['id'].'"';
        
        return  DBSmart::DataExecute($query);        

    }

    public static function UpdConsolidateTotal($iData)
    {
        $query  =   'UPDATE cp_mw_consolidate_bs SET dtotal = "'.$iData['total'].'" WHERE id = "'.$iData['id'].'"';
        
        return  DBSmart::DataExecute($query);        

    }

    public static function ConsolidatePendingHtml($iData)
    {
        $html = '';
        $html.='<table id="TableConsolidatePending" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;"></th>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Factura</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Codigo</th>';
        $html.='<th style="text-align: center;">Monto</th>';
        $html.='<th style="text-align: center;">Dolar.</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;"><i class="icon-append fa fa-lg fa-wrench"></i></th>';
        $html.='<th style="text-align: center;">Acci&oacute;n</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;"><label class="checkbox"><input type="checkbox" name="ConPending'.$inf['id'].'" id="ConPending'.$inf['id'].'"><i></i></label></td>';
                $html.='<td style="text-align: center;">'.$inf['idcliente'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['name'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nfactura'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['ftotal'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['fdate'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['ftransaccion'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['amount'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['dolar'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['dtotal'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['cdate'].'</td>';
                $html.='<td style="text-align: center;"><a onclick="ViewCodes('.(int)$inf['id'].')"><i class="icon-append fa fa-lg fa-fw fa-eye"></i></a></td>';
                $html.='<td style="text-align: center;">
                        <div class="btn-group">
                            <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                Acci&oacute;n <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a onclick="EditRepClient('.(int)$inf['id'].');">Editar</a></li>
                                <li><a onclick="ProcConsProc('.(int)$inf['id'].');">Procesar</a></li>
                                <li><a onclick="AnexConsProc('.(int)$inf['id'].');">Anexar</a></li>
                            </ul>
                        </div>
                </td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }

        return $html;

    }

    public static function ConsolidateProccessHtml($iData)
    {
        $html = '';
        $html.='<table id="TableConsolidateProccess" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;"><strong>Fecha</strong></th>';
        $html.='<th style="text-align: center;"><strong>Id</strong></th>';
        $html.='<th style="text-align: center;"><strong>Factura</strong></th>';
        $html.='<th style="text-align: center;"><strong>C&oacute;digo</strong></th>';
        $html.='<th style="text-align: center;"><strong>Dolar</strong></th>';
        $html.='<th style="text-align: center;"><strong>Monto</strong></th>';
        $html.='<th style="text-align: center;"><strong><i class="icon-append fa fa-lg fa-wrench"></i></strong></th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;">'.$inf['process_at'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['idcliente'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nfactura'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['code'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['dolar'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['dtotal'].'</td>';
                $html.='<td style="text-align: center;"><a onclick="ViewCodes('.(int)$inf['id'].')"><i class="icon-append fa fa-lg fa-fw fa-eye"></i></a></td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }

        return $html;

    }

    public static function InsertAddCode($iData)
    {
        $query  =   'INSERT INTO cp_mw_payment_codes_bs (code, amount, cdate, dolar, dolartime, dtotal, consolidate_id, created_by, created_at) VALUES("'.$iData['code'].'", "'.$iData['amount'].'", "'.$iData['cdate'].'", "'.$iData['dolar'].'", "'.$iData['dolartime'].'", "'.$iData['dtotal'].'", "'.$iData['consolidate_id'].'", "'.$iData['created_by'].'", NOW())';
        return DBSmart::DataExecute($query);
    
    }

    public static function SearchAddCode($iData)
    {
        $query  =   'SELECT dtotal FROM cp_mw_payment_codes_bs WHERE consolidate_id = "'.$iData.'"';
        return  DBSmart::DBQueryAll($query);
    
    }

    public static function SearchAllCode($iData)
    {
        $query  =   'SELECT * FROM cp_mw_payment_codes_bs WHERE consolidate_id = "'.$iData.'"';
        return  DBSmart::DBQueryAll($query);
    
    }

    public static function SearchAllCodeView()
    {
        $query  =   'SELECT * FROM cp_mw_payment_codes_bs ORDER BY id DESC';
        return  DBSmart::DBQueryAll($query);
    
    }

    public static function ViewCodeAid($id)
    {
        $query  =   'SELECT * FROM cp_mw_payment_codes_bs WHERE id="'.$id.'"';
        return  DBSmart::DBQuery($query);
    
    }
    
    public static function UpdateCodeAsig($iData)
    {
        $query  =   'UPDATE cp_mw_payment_codes_bs SET dolar = "'.$iData['dolar'].'", dtotal = "'.$iData['dtotal'].'" WHERE id="'.$iData['id'].'"';
        return  DBSmart::DataExecute($query);
    
    }


    public static function HtmlCodeView($iData)
    {
        $html = "";
        $html.='<table id="TableCodeUtl" class="table table-bordered" border="1" style="font-size: 10px; text-align: center;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;"><strong>#</strong></th>';
        $html.='<th style="text-align: center;"><strong>Code</strong></th>';
        $html.='<th style="text-align: center;"><strong>Monto BS</strong></th>';
        $html.='<th style="text-align: center;"><strong>Dolar</strong></th>';
        $html.='<th style="text-align: center;"><strong>Total</strong></th>';
        $html.='<th style="text-align: center;"><strong>Accion</strong></th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody style="text-align: center;">';
            foreach ($iData as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td>'.$k.'</td>';
                $html.='<td>'.$val['code'].'</td>';
                $html.='<td>'.$val['amount'].'</td>';
                $html.='<td>'.$val['dolar'].'</td>';
                $html.='<td>'.$val['dtotal'].'</td>';
                $html.='<td><a class="btn btn-default btn-xs btn-block" onclick="EditCodeAsig('.(int)$val['id'].')">Editar <a/></td>';
                $html.='</tr>';
            
            }

            $html.='</tbody>';
            $html.='</table>';
        }else{
            $html.='</tbody>';
            $html.='</table>';
        }

        return $html;
    }

    public static function ViewCodeHtml($iData)
    {
        $html = "";


        $html.='<table class="table table-bordered">';
        $html.='<tbody>';
        $html.='<tr style="text-align: center;">';
        $html.='<td>Codigo</td>';
        $html.='<td>Monto</td>';
        $html.='<td>Dolar</td>';
        $html.='<td>Total</td>';
        $html.='<td>Operador</td>';
        $html.='</tr>';
        foreach ($iData as $i => $dat) 
        {
            $html.='<tr style="text-align: center;">';
            $html.='<td>'.$dat['code'].'</td>';
            $html.='<td>'.$dat['amount'].'</td>';
            $html.='<td>'.$dat['dolar'].'</td>';
            $html.='<td>'.$dat['dtotal'].'</td>';
            $html.='<td>'.User::GetUserById($dat['created_by'])['username'].'</td>';
            $html.='</tr>';
        }

        $html.='</tbody>';
        $html.='</table>';

        return $html;
    
    }

    public static function AnexCodeClientHtml($iData)
    {

        $html = '';
        $html.='<table id="TableConsolidateClient" class="table table-bordered table-striped">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Acci&oacute;n</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">C&oacute;digo</th>';
        $html.='<th style="text-align: center;">BS</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody style="text-align: center;">';
            foreach ($iData as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td><button type="button" class="btn btn-default btn-xs btn-block" id="AddCodeClient'.(int)$val['id'].'" onclick="BtnClientCodeAsig('.(int)$val['id'].')"> Asignar </button></td>';
                $html.='<td>'.$val['created_at'].'</td>';
                $html.='<td>'.$val['code'].'</td>';
                $html.='<td>'.$val['amount'].'</td>';
                $html.='</tr>';
            
            }

            $html.='</tbody>';
            $html.='</table>';
        }else{
            $html.='</tbody>';
            $html.='</table>';
        }

        return $html;
    
    }

}