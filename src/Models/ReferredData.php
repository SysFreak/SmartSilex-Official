<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\ReferredData;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class ReferredData extends Model {

	static $_table 		= 'data_referred';

	Public $_fillable 	= array('id', 'name', 'country_id', 'status_id');

	public static function GetReferred()
	{
		
		$query 	= 	'SELECT * FROM data_referred ORDER BY id ASC';
		$gen 	=	DBSmart::DBQueryAll($query);

		$general = array();
		
		if($gen <> false)
		{
			foreach ($gen as $k => $val) 
			{
				$general[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=> 	$val['name'], 
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id'],
					'country_id'	=>	$val['country_id'],
				);
			}

	        return $general;

		}else{ return false; }

	}

	public static function GetReferredById($id)
	{
		
		$query 	= 	'SELECT * FROM data_referred WHERE id = "'.$id.'" ORDER BY id ASC';
		$gene 	=	DBSmart::DBQuery($query);

		if($gene <> false)
		{
			return array(
				'id'           => $gene['id'],
				'name'         => $gene['name'],
				'country'      => $gene['country_id'],
				'status'       => $gene['status_id']
			);

		}else{ return false; }

	}

	public static function GetReferredByName($info)
	{

		$query 	= 	'SELECT * FROM data_referred WHERE name = "'.$info.'" ORDER BY id ASC';
		$gen 	=	DBSmart::DBQuery($query);

		if($gen <> false)
		{
			return array(
				'id'           => $gene['id'],
				'name'         => $gene['name'],
				'country'      => $gene['country_id'],
				'status'       => $gene['status_id']
			);

		}else{ return false; }
	}

	public static function SaveReferredData($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_ref']));

		if($info['type_ref'] == 'new')
		{
			$query 	= 	'INSERT INTO data_referred(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_ref'].'", "'.$info['status_id_ref'].'", "'.$date.'")';
			$serv 	=	DBSmart::DataExecute($query);

			return ($serv <> false ) ? true : false;
		}
		elseif ($info['type_ref'] == 'edit') 
		{
			$query 	=	'UPDATE data_referred SET name="'.$name.'", country_id="'.$info['country_id_ref'].'", status_id="'.$info['status_id_ref'].'" WHERE id = "'.$info['id_ref'].'"';
			$serv 	=	DBSmart::DataExecute($query);

			return ($serv <> false) ? true : false;
		}
	}
}

