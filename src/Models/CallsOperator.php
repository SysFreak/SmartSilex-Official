<?php
namespace App\Models;

use Model;

use App\Models\CallsOperator;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CallsOperator extends Model 
{

   	static $_table 			= 'calls_operator';

	protected $_fillable 	= array('calldate', 'src', 'did', 'dst', 'operator', 'ext', 'departament', 'team', 'disposition', 'time', 'online', 'type', 'created_at');


	public static function SaveCalls($info)
	{

		$calls 	=	CallsOperator::create();

		$calls->calldate	=	$info['calldate'];
		$calls->src			=	$info['src'];
		$calls->did			=	$info['did'];
		$calls->dst			=	$info['dst'];
		$calls->operator	=	$info['operator'];
		$calls->ext			=	$info['ext'];
		$calls->departament	=	$info['departament'];
		$calls->team		=	$info['team'];
		$calls->disposition	=	$info['disposition'];
		$calls->time		=	$info['time'];
		$calls->online		=	$info['online'];
		$calls->type		=	$info['type'];
		$calls->created_at	=	$info['created_at'];

		return ($calls->save()) ? true : false;
	}

 }