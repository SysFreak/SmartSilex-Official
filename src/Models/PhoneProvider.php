<?php
namespace App\Models;

use Model;

use App\Models\PhoneProvider;
use App\Models\Status;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class PhoneProvider extends Model 
{
	static $_table 		= 'data_phone_provider';

	Public $_fillable 	= array('name', 'country_id', 'status_id', 'created_at');

	public static function GetProvider()
	{
		$query 	= 	'SELECT id, name, country_id, status_id FROM data_phone_provider ORDER BY id ASC';
		$pro 	=	DBSmart::DBQueryAll($query);

		$provider = array();

		if($pro <> false)
		{
			foreach ($pro as $k => $val) 
			{
				$provider[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'], 
					'country_id'	=> $val['country_id'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name']
				);
			}
	        
	        return $provider;

		}else{ 	return false;	}
	}

	public static function GetProviderByStatus()
	{
		$query 	= 	'SELECT id, name, country_id, status_id FROM data_phone_provider WHERE status_id = "1" ORDER BY id ASC';
		$pro 	=	DBSmart::DBQueryAll($query);

		$provider = array();

		if($pro <> false)
		{
			foreach ($pro as $k => $val) 
			{
				$provider[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'], 
					'country_id'	=> $val['country_id'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name']
				);
			}
	        
	        return $provider;

		}else{ 	return false;	}
	}

	public static function GetProviderById($id)
	{
		$query 	= 	'SELECT id, name, country_id, status_id FROM data_phone_provider WHERE id = "'.$id.'"';
		$pro 	=	DBSmart::DBQuery($query);

		if($pro <> false)
		{
			return 	array(
				'id' 	 		=> $pro['id'], 
				'name' 	 		=> $pro['name'], 
				'country_id'	=> $pro['country_id'],
				'status_id'		=> $pro['status_id'],
				'country' 		=> Country::GetCountryById($pro['country_id'])['name'],
				'status' 		=> Status::GetStatusById($pro['status_id'])['name']
			);

		}else{ 	return false;	}
	}

	public static function SaveProvider($info)
	{
		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$name 		= 	strtoupper($_replace->deleteTilde($info['name_prov_t']));

		if($info['type_prov_t'] == 'new')
		{
			$query 	= 	'INSERT INTO data_phone_provider(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_prov_t'].'", "'.$info['status_id_prov_t'].'", "'.$date.'")';
			$serv  	=	DBSmart::DataExecute($query);
			
			return ($serv <> false) ? true : false;

		}
		elseif ($info['type_prov_t'] == 'edit') 
		{
			$query 	=	'UPDATE data_phone_provider SET name="'.$name.'", country_id="'.$info['country_id_prov_t'].'", status_id="'.$info['status_id_prov_t'].'" WHERE id = "'.$info['id_prov_t'].'"';

			$serv  	=	DBSmart::DataExecute($query);
			
			return ($serv <> false) ? true : false;
		}		
	}
}

