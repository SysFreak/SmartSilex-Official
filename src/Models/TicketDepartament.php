<?php
namespace App\Models;

use Model;

use App\Models\TicketDepartament;
use App\Models\TicketService;
use App\Models\Departament;
use App\Models\Status;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class TicketDepartament extends Model
{

	static $_table 		= 'data_ticket_dep';

	Public $_fillable 	= array('name', 'departament_id', 'dep_id', 'status_id', 'created_at');

	public static function GetDepartament()
	{
		
		$query	=	'SELECT * FROM data_ticket_dep ORDER BY id DESC';
		$dep 	=	DBSmart::DBQueryAll($query);

		if($dep <> false)
		{
			foreach ($dep as $d => $dal) 
			{
				$tDep[$d]	=	[
					'id'			=>	$dal['id'],
					'name'			=>	$dal['name'],
					'ticket_dep'	=>	$dal['departament_id'],
					'ticket_depart'	=>	Departament::GetDepById($dal['departament_id'])['name'],
					'dep_id'		=>	$dal['dep_id'],
					'depart'		=>	Departament::GetDepById($dal['dep_id'])['name'],
					'status_id'		=>	$dal['status_id'],
					'status'		=>	Status::GetStatusById($dal['status_id'])['name']
				];
			}
			return $tDep;

		}else{ return false; }
	
	}

	public static function FindDepartament()
	{
		$query 		=	'SELECT id, name, status_id FROM data_departament WHERE name NOT LIKE "%TICKET%" ORDER BY id ASC';
		$info 		=	DBSmart::DBQueryAll($query);

		if($info <> false)
		{ 
			foreach ($info as $ke => $value) 
			{
				$dept1[$ke] =	[
					'id'			=> $value['id'],
					'name'			=> $value['name'],
					'status_id'		=> $value['status_id']
				];
			}
		}else{	$dept1 	=	false; }

        $query 		=	'SELECT id, name, status_id FROM data_departament WHERE name LIKE "TICKET%" ORDER BY id ASC';
        $info2 		=	DBSmart::DBQueryAll($query);

		if($info2 <> false)
		{ 
			foreach ($info2 as $k => $val) 
			{
				$dept2[$k] =	[
					'id'			=> $val['id'],
					'name'			=> $val['name'],
					'status_id'		=> $val['status_id']
				];
			}

		}else{	$dept2 	=	false; }

		return ['dept1'	=>	$dept1, 'dept2' => $dept2];

	}

	public static function GetDepartamentHtml()
	{
		$query 		=	'SELECT id, name, departament_id FROM data_ticket_dep WHERE status_id = "1" ORDER BY id ASC';
        $dep 		=	DBSmart::DBQueryAll($query);

		$html = "";

		if($dep <> false)
		{	
			$html.='<select class="form-control" id="tic_dep" name="tic_dep" required>';
			$html.='<option value="0" selected="" disabled="">SELECTED</option>';

			foreach ($dep as $k => $val) 
			{  $html.='<option value="'.$val['departament_id'].'">'.$val['name'].'</option>'; }

			$html.='</select>';

			return $html;

		}else{ return $html; }
	
	}

	public static function GetDeptByTicDep($id)
	{
		$query 		=	'SELECT * FROM data_ticket_dep WHERE dep_id = "'.$id.'" ORDER BY id DESC';
        $dep 		=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return [
				'id'			=>	$dep['id'],
				'name'			=>	$dep['name'],
				'ticket_dep'	=>	$dep['departament_id'],
				'ticket_depart'	=>	Departament::GetDepById($dep['departament_id'])['name'],
				'dep_id'		=>	$dep['dep_id'],
				'depart'		=>	Departament::GetDepById($dep['dep_id'])['name'],
				'status_id'		=>	$dep['status_id'],
				'status'		=>	Status::GetStatusById($dep['status_id'])['name']
			];

		}else{ return false; }
	
	}

	public static function GetDepartamentById($id)
	{
		$query 		=	'SELECT * FROM data_ticket_dep WHERE id = "'.$id.'" ORDER BY id DESC';
        $dep 		=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return [
				'id'			=>	$dep['id'],
				'name'			=>	$dep['name'],
				'ticket_dep'	=>	$dep['departament_id'],
				'ticket_depart'	=>	Departament::GetDepById($dep['departament_id'])['name'],
				'dep_id'		=>	$dep['dep_id'],
				'depart'		=>	Departament::GetDepById($dep['dep_id'])['name'],
				'status_id'		=>	$dep['status_id'],
				'status'		=>	Status::GetStatusById($dep['status_id'])['name']
			];

		}else{ return false; }
	
	}

	public static function GetDepartamentByTicDep($id)
	{
		$query 		=	'SELECT * FROM data_ticket_dep WHERE departament_id = "'.$id.'" ORDER BY id DESC';
        $dep 		=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return [
				'id'			=>	$dep['id'],
				'name'			=>	$dep['name'],
				'ticket_dep'	=>	$dep['departament_id'],
				'ticket_depart'	=>	Departament::GetDepById($dep['departament_id'])['name'],
				'dep_id'		=>	$dep['dep_id'],
				'depart'		=>	Departament::GetDepById($dep['dep_id'])['name'],
				'status_id'		=>	$dep['status_id'],
				'status'		=>	Status::GetStatusById($dep['status_id'])['name']
			];

		}else{ return false; }
	
	}
	
	public static function DepartamentDep($id)
	{
		$query 		=	'SELECT * FROM data_ticket_dep WHERE departament_id = "'.$id.'" AND status_id = "1" ORDER BY id DESC';
        $dep 		=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return [
				'id'			=>	$dep['id'],
				'name'			=>	$dep['name'],
				'ticket_dep'	=>	$dep['departament_id'],
				'ticket_depart'	=>	Departament::GetID($dep['departament_id'])['name'],
				'dep_id'		=>	$dep['dep_id'],
				'depart'		=>	Departament::GetID($dep['dep_id'])['name'],
				'status_id'		=>	$dep['status_id'],
				'status'		=>	Status::GetID($dep['status_id'])['name']
			];

		}else{ return false; }
	
	}

	public static function SaveDepartament($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['ticket_name']));

		if($info['ticket_type_id'] == 'new')
		{
			$query 	=	'INSERT INTO data_ticket_dep(name, departament_id, dep_id, status_id, created_at) VALUES ("'.$name.'","'.$info['ticket_dept1_id'].'","'.$info['ticket_dept2_id'].'","'.$info['ticket_status_id'].'","'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['ticket_type_id'] == 'edit') 
		{
			$query 	=	'UPDATE data_ticket_dep SET name="'.$name.'",departament_id="'.$info['ticket_dept1_id'].'",dep_id="'.$info['ticket_dept2_id'].'",status_id="'.$info['ticket_status_id'].'" WHERE id = "'.$info['ticket_dep_id'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	
	}
}