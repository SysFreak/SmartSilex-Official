<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Bank;
use App\Models\CypherData;
use App\Models\Country;
use App\Models\Town;
use App\Models\Approvals;
use App\Models\ApprovalsCancel;
use App\Models\ApprovalsRespCancel;
use App\Models\ApprovalsScoreTmp;
use App\Models\Service;
use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\ServiceTvPrTmp;
use App\Models\ServiceSecPrTmp;
use App\Models\ServiceIntPrTmp;
use App\Models\Leads;
use App\Models\User;
use App\Models\Departament;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\PhoneProvider;
use App\Models\Phone;
use App\Models\Score;
use App\Models\Notes;
use App\Models\Package;
use App\Models\IDType;
use App\Models\SSType;


use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class Approvals extends Model 
{

	static $_table 		= 'cp_appro_serv';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'service_id', 'score_id', 'score', 'transactions', 'value', 'fico', 'status_id', 'view', 'cancel', 'created_by', 'created_dep_id', 'created_at', 'finish_by', 'finish_dep_id', 'finish_at');

//////////////////////////////////// Info Load  /////////////////////////////////////

	public static function Load()
	{
		$date 	= 	date("Y-m-d");

		$type 	=	['0', '1', '2', '3'];

		$row 	=	"";
		$row2 	=	"";
		$row3 	=	"";
		$row4 	=	"";

		foreach ($type as $k => $val) 
		{
			if($val == 0)
			{
				$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "0" AND cancel = "0") AND created_at LIKE "'.$date.'%" ORDER BY created_at DESC';

				$info 	=	DBSmart::DBQueryAll($query);

				if($info <> false)
				{ 
					foreach ($info as $ke => $value) {	$row3.=''.Approvals::RowHtmlCancel($value).''; }
				}
			}
			elseif($val == 1)
			{
				$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "1" AND cancel = "1") ORDER BY created_at DESC';

				$info 	=	DBSmart::DBQueryAll($query);

				if($info <> false)
				{ 
					foreach ($info as $ke => $value) {	$row.=''.Approvals::RowHtmlPending($value).''; }
				}

			}
			elseif($val == 2)
			{
				$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "2" AND cancel = "1") AND created_at LIKE "'.$date.'%" ORDER BY created_at DESC';

				$info 	=	DBSmart::DBQueryAll($query);
        			
				if($info <> false)
				{ 
					foreach ($info as $ke => $valu) 
					{ $row2.=''.Approvals::RowHtmlProcessed($valu).''; }
				}
			}
			elseif($val == 3)
			{
				$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "1" AND cancel = "1") ORDER BY created_at DESC';

				$info 	=	DBSmart::DBQueryAll($query);	
        			
				if($info <> false)
				{ 
					foreach ($info as $ke => $value) { $row4.=''.Approvals::RowHtmlErrorTV($value).''; }
				}
			}

		}

		return [
			'pending'		=>	Approvals::TableHtmlPending($row),
			'processed'		=>	Approvals::TableHtmlProcessed($row2),	
			'cancel'		=>	Approvals::TableHtmlCancel($row3),
			'errortv'		=>	Approvals::TableHtmlErrorTV($row4)
		];
	
	}

////////////////////////////////// Load By Date //////////////////////////////////

	public static function LoadByDate($info, $dates)
	{

		$type 	=	['0', '1', '2', '3'];

		$row 	=	"";
		$row2 	=	"";
		$row3 	=	"";
		$row4 	=	"";

		if($info == "0")
		{
			foreach ($type as $k => $val) 
			{
				if($val == 0)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "0" AND cancel = "0") AND created_at LIKE "'.$dates['dateEnd'].'%" ORDER BY created_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);

					if($info <> false)
					{ 
						foreach ($info as $ke => $value) {	$row3.=''.Approvals::RowHtmlCancel($value).''; }
					}
				}
				elseif($val == 1)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "1" AND cancel = "1") ORDER BY created_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);

					if($info <> false)
					{ 
						foreach ($info as $ke => $value) {	$row.=''.Approvals::RowHtmlPending($value).'';}
					}
				}
				elseif($val == 2)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "2" AND cancel = "1") AND created_at LIKE "'.$dates['dateEnd'].'%" ORDER BY created_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);
	        			
					if($info <> false)
					{ 
						foreach ($info as $ke => $valu) 
						{ $row2.=''.Approvals::RowHtmlProcessed($valu).''; }
					}
				}
				elseif($val == 3)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "1" AND cancel = "1") ORDER BY created_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);
	        			
					if($info <> false)
					{ 
						foreach ($info as $ke => $valu) 
						{ $row4.=''.Approvals::RowHtmlErrorTV($valu).'';  }
					}
				}
			}

		}else{

			foreach ($type as $k => $val) 
			{
				if($val == 0)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "0" AND cancel = "0") AND created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY finish_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);

					if($info <> false)
					{ 
						foreach ($info as $ke => $value) {	$row3.=''.Approvals::RowHtmlCancel($value).''; }
					}
				}
				elseif($val == 1)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "1" AND cancel = "1") ORDER BY created_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);

					if($info <> false)
					{ 
						foreach ($info as $ke => $value) {	$row.=''.Approvals::RowHtmlPending($value).'';  }
					}
				}
				elseif($val == 2)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "2" AND cancel = "1") AND created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY finish_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);
	        			
					if($info <> false)
					{ 
						foreach ($info as $ke => $valu) 
						{ $row2.=''.Approvals::RowHtmlProcessed($valu).''; }
					}
				}
				elseif($val == 3)
				{
					$query 	= 	'SELECT ticket FROM cp_appro_serv WHERE (status_id = "1" AND cancel = "1") ORDER BY created_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);
	        			
					if($info <> false)
					{ 
						foreach ($info as $ke => $valu) 
						{ $row4.=''.Approvals::RowHtmlErrorTV($valu).'';  }
					}
				}
			}

		}

		return [
			'pending'		=>	Approvals::TableHtmlPending($row),
			'processed'		=>	Approvals::TableHtmlProcessed($row2),	
			'cancel'		=>	Approvals::TableHtmlCancel($row3),
			'errortv'		=>	Approvals::TableHtmlErrorTV($row4)
		];

	}

////////////////////////////////// Process Cancel //////////////////////////////////

	public static function ApprovalsCancel($info, $data)
	{

		foreach ($info['cancel_sel'] as $k => $val) { $can 	=	ApprovalsCancel::SaveCancel($val, $data); }

		if($can <> false)
		{
			$note 	=	Notes::SaveNote($data);

			if($note <> false)
			{

				$query  	= 	'UPDATE cp_appro_serv SET status_id = "0", cancel="0", finish_by = "'.$data['operator'].'", finish_dep_id = "'.$data['depart'].'", finish_at = "'.$data['date'].'" WHERE id = "'.$data['id'].'"';

				$change 	=	DBSmart::DataExecute($query);

				return ($change <> false ) ? true : false;

			}else{	return false;	}

		}else{	return false; }

	}

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function DetecTicket($in, $ticket)
	{

		switch ($in) {
			case 'TV':
				return (ServiceTvPr::GetServiceByTicket($ticket) <> false) ? 'TVPR' : false;
				break;
			case 'SE':			
				return (ServiceSecPr::GetServiceByTicket($ticket) <> false) ? 'SECPR'  : false;
				break;
			case 'IN':
				return (ServiceIntPr::GetServiceByTicket($ticket) <> false) ? 'INTPR' : false;;
				break;
			case 'IV':
				return (ServiceIntVZ::GetServiceByTicket($ticket) <> false) ? 'IVE' : false;;
				break;
		}
	
	}	

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function GetInfo($type, $ticket)
	{

		$cypher		=	New UnCypher();

        $table 		=	$referred 	=		'';

        if($type == 'TVPR')
        {
        	$query 	=	'SELECT ticket, client_id, tv, (SELECT name FROM data_package WHERE id = package_id) AS package, (SELECT name FROM data_provider WHERE id = provider_id) AS provider, decoder_id, dvr_id, (SELECT name FROM data_payment WHERE id = payment_id) AS payment,  IF(ch_hd = "on", "SI", "NO") AS ch_hd, IF(ch_dvr = "on", "SI", "NO") AS ch_dvr, IF(ch_hbo = "on", "SI", "NO") AS ch_hbo, IF(ch_cinemax = "on", "SI", "NO") AS ch_cinemax, IF(ch_starz = "on", "SI", "NO") AS ch_starz, IF(ch_showtime = "on", "SI", "NO") AS ch_showtime, IF(gif_ent = "on", "SI", "NO") AS gif_ent, IF(gif_xtra = "on", "SI", "NO") AS gif_xtra, IF(gif_choice = "on", "SI", "NO") AS gif_choice, IF(advertising_id = "1", "SI", "NO") AS advertising FROM cp_service_pr_tv WHERE ticket = "'.$ticket.'"';

        	$table = 'cp_service_pr_tv';
        }
        elseif ($type == 'SECPR') 
        {
        	$query 	= 	'SELECT ticket, client_id, if( previously_id = "1", "SI", "NO") AS sist_previo, equipment, (SELECT name FROM data_camera WHERE id = cameras_id) AS camaras,  comp_equipment AS compania, (SELECT name FROM data_camera WHERE id = dvr_id ) AS dvr, add_equipment AS adicional, password_alarm, (SELECT name FROM data_payment WHERE id = payment_id) AS payment, contact_1, contact_1_desc, contact_2, contact_2_desc, contact_3, contact_3_desc FROM cp_service_pr_sec WHERE ticket = "'.$ticket.'"';
        	
        	$table = 'cp_service_pr_sec';

        }
        elseif ($type == 'INTPR') 
        {
        	$query = 	'SELECT ticket, client_id, (SELECT name FROM data_internet 	WHERE id = int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = pho_res_id) AS pho_res, (SELECT name FROM data_internet 	WHERE id = int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = payment_id) AS payment FROM cp_service_pr_int WHERE ticket = "'.$ticket.'"';
        	
        	$table = 'cp_service_pr_int';
        }
        elseif($type == 'IVE')
        {
        	$query = 'SELECT ticket, client_id, (SELECT name FROM data_internet WHERE id = int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = pho_res_id) AS phone_res, (SELECT name FROM data_internet WHERE id = int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = pho_com_id) AS phone_com, (SELECT name FROM data_payment WHERE id = payment_id) AS payment, (SELECT name FROM data_bank WHERE id = bank_id) AS bank, amount, trans_date, trans_date, email, additional FROM cp_service_vzla_int WHERE ticket = "'.$ticket.'"';

        	$table = 'cp_service_vzla_int';

        }else{
        	var_dump($type, $ticket);exit;
        }

        $service    =   DBSmart::DBQuery($query);

		$query 		=	'SELECT t2.client_id, t2.name, t2.birthday, t2.coor_lati AS coord_la, t2.coor_long AS coord_lo, t2.referred_id, t2.phone_main AS phone,  (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS p_provider, t2.phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = t2.phone_alt_provider) AS a_provider, t2.phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = t2.phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = t2.town_id) AS town, (SELECT code FROM data_zips WHERE id = t2.zip_id) AS zip, (SELECT name FROM data_country WHERE id = t2.country_id) AS country, (SELECT name FROM data_ceiling WHERE id = t2.h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = t2.h_level_id) AS level, t2.ident_id, t2.ident_exp, t2.ss_id, t2.ss_exp, t2.add_main, t2.add_postal, t2.email_main, t2.additional FROM '.$table.' AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

        $client     =   DBSmart::DBQuery($query);

        if($client['referred_id'] <> "0")
        {
	        $query 		= 	'SELECT client_id, name, birthday, coor_lati AS coord_la, coor_long AS coord_lo, referred_id, phone_main AS phone, (SELECT name FROM data_phone_provider WHERE id = phone_main_provider) AS p_provider, phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = phone_alt_provider) AS a_provider, phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT code FROM data_zips WHERE id = zip_id) AS zip, (SELECT name FROM data_country WHERE id = country_id) AS country, (SELECT name FROM data_ceiling WHERE id = h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = h_level_id) AS level, ident_id, ident_exp, ss_id, ss_exp, add_main, add_postal, email_main, additional FROM cp_leads WHERE client_id = "'.$client['referred_id'].'"';

	        $referred   =   DBSmart::DBQuery($query);

        }else{ $referred == false; }


        return ['client' => $client, 'referred' => $referred, 'service' => $service];

   	}

	public static function GetInfoSingleVE($client)
	{

		$cypher		=	New UnCypher();

        $table 		=	$referred 	=		'';

    	$query = 'SELECT client_id, (SELECT name FROM data_internet WHERE id = int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = pho_res_id) AS phone_res, (SELECT name FROM data_internet WHERE id = int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = pho_com_id) AS phone_com, (SELECT name FROM data_payment WHERE id = payment_id) AS payment, (SELECT name FROM data_bank WHERE id = bank_id) AS bank, amount, trans_date, trans_date, email, additional FROM cp_service_vzla_int_tmp WHERE client_id = "'.$client.'" ORDER BY id DESC';

        $service    =   DBSmart::DBQuery($query);


        return ['service' => $service];

   	}

//////////////////////////////////// Row Single ////////////////////////////////////

   	public static function ObtData($client, $ticket)
   	{
		$cypher		=	New UnCypher();

        if($client)
        {
        	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'ID');
			$ide 	=	$info['infoCard'];

	    	if($ide <> false) 
	    		{ $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = IDType::GetIdTypeById($info['info']['type_d'])['name'];}
	    		// { $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = "";}
	    	else 
	    		{ $ident = ""; $ident_exp = ""; $ident_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'SS');
	    	$sss 	=	$info['infoCard'];

	    	if($sss <> false) 
	    		{ $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = SSType::GetSSTypeById($info['info']['type_d'])['name'];}
	    		// { $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = "";}
	    	else 
	    		{ $ss = ""; $ss_exp = ""; $ss_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'CC');
	    	$cards 	=	$info['infoCard'];

	    	if($cards <> false) 
	    		{ $card = $cards['card']; $card_exp = $cards['month']."/".$cards['year']; }
	    	else 
	    		{ $card = ""; $card_exp = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'AB');

			$ABS 	=	$info['infoCard'];

	    	if($ABS <> false) 
	    		{ 
					$ab_nam = $ABS['name']; $ab_num = $ABS['account']; $ab_ban = Bank::GetBankById($ABS['bank'])['name']; $ab_typ = $ABS['type'];
	    		}
	    	else 
	    		{ $ab_nam = ""; $ab_num = ""; $ab_ban = ""; $ab_typ = ""; }

	    	$cData = 	[
	    		'client'		=>	($client['client_id'] <> '')	? $client['client_id'] 	: '',
	    		'ticket'		=>	($ticket <> '')					? $ticket 				: '',
	    		'name'			=>	($client['name'] <> '')			? $client['name'] 		: '',
	    		'birthday'		=>	($client['birthday'] <> '')		? $client['birthday'] 	: '',
	    		'coord_la'		=>	($client['coord_la'] <> '')		? $client['coord_la'] 	: '',
	    		'coord_lo'		=>	($client['coord_lo'] <> '')		? $client['coord_lo'] 	: '',
	    		'phone'			=>	($client['phone'] <> '')		? $client['phone'] 		: '',
	    		'p_provider'	=>	($client['p_provider'] <> '')	? $client['p_provider'] : '',
	    		'p_alter'		=>	($client['p_alter'] <> '')		? $client['p_alter'] 	: '',
	    		'a_provider'	=>	($client['a_provider'] <> '')	? $client['a_provider'] : '',
	    		'p_other'		=>	($client['p_other'] <> '')		? $client['p_other'] 	: '',
	    		'o_provider'	=>	($client['o_provider'] <> '')	? $client['o_provider'] : '',
	    		'town'			=>	($client['town'] <> '')			? $client['town'] 		: '',
	    		'country'		=>	($client['country'] <> '')		? $client['country'] 	: '',
	    		'zip'			=>	($client['zip'] <> '')			? $client['zip'] 		: '',
	    		'ss'			=>	($ss <> '') 		?	$ss 		: '',
	    		'ss_exp'		=>	($ss_exp <> '') 	? 	$ss_exp 	: '',
				'ss_type'		=>	($ss_type <> '') 				? 	$ss_type 			: '',
	    		'id'			=>	($ident <> '') 		? 	$ident 		: '',
	    		'id_exp'		=>	($ident_exp <> '') 	? 	$ident_exp 	: '',
				'id_type'		=>	($ident_type <> '')				? 	$ident_type 		: '',
	    		'cc'			=>	($card <> '') 		? 	$card 		: '',
	    		'cc_exp'		=>	($card_exp <> '')	?   $card_exp 	: '',
				'cc_exp'		=>	($card_exp <> '')				?   $card_exp 			: '',
	    		'ab'			=>	($ab_nam <> '') 	? 	$ab_nam 	: '',
	    		'ab_num'		=>	($ab_num <> '') 	? 	$ab_num		: '',
	    		'ab_bank'		=>	($ab_ban <> '') 	? 	$ab_ban 	: '',
	    		'ab_type'		=>	($ab_typ <> '') 	? 	$ab_typ 	: '',
	    		'add'			=>	($client['add_main'] <> '')		? $client['add_main']	: '',
	    		'add_postal'	=>	($client['add_postal'] <> '')	? $client['add_postal']	: '',
	    		'email'			=>	($client['email_main'] <> '')	? $client['email_main']	: '',
	    		'additional'	=>	($client['additional'] <> '')	? $client['additional']	: ''
	    	];


        }else{

		    $cData = 	[
	    		'client' => '', 'ticket' => '', 'name' => '', 'phone' => '', 'p_provider' => '', 'p_alter' => '', 'a_provider' => '', 'p_other' => '', 'o_provider' => '', 'town' => '', 'country'=> '', 'zip' => '', 'ss' => '', 'ss_exp' => '', 'id' => '', 'id_exp' => '', 'id_type' => '', 'cc' => '', 'cc_exp' => '', 'cc_exp' => '', 'ab' => '', 'ab_num' => '', 'ab_bank' => '', 'ab_type' => '', 'add' => '', 'add_postal' => '', 'email'=> ''];        	
        }
        
    	return ['status'=> ($client) ? true : false, 'data' => $cData];
   
   	}

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function GetServById($id)
	{

		$query	= 	'SELECT id, ticket, client_id, service_id, score_id, created_by FROM cp_appro_serv WHERE id = "'.$id.'"';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'id'		=>	$serv['id'],
				'ticket'	=> 	$serv['ticket'],
				'client'	=>	$serv['client_id'],
				'service'	=>	$serv['service_id'],
				'score_id'	=>	$serv['score_id'],
				'created'	=>	$serv['created_by'] 
			];

		}else{	return false;	}
	
	}

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function GetServByClient($client)
	{
		$query	= 	'SELECT id, ticket, client_id, service_id, score_id, created_by FROM cp_appro_serv WHERE client_id = "'.$client.'"';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'id'		=>	$serv['id'],
				'ticket'	=> 	$serv['ticket'],
				'client'	=>	$serv['client_id'],
				'service'	=>	$serv['service_id'],
				'score_id'	=>	$serv['score_id']
			];

		}else{	return false;	}
	
	}

////////////////////////////////// Info Television //////////////////////////////////

	public static function GetInfoService($id, $ser)
	{
		$query	= 	'SELECT id, ticket, score, transactions, finish_by, created_by, finish_at, fico, value FROM cp_appro_serv WHERE client_id = "'.$id.'" AND service_id = "'.$ser.'" AND status_id = "2" ORDER BY id DESC';
		$serv 	=	DBSmart::DBQuery($query);

		$status = 	"";

		switch ($ser) {
			case '2':
				$status 	=	StatusService::GetStatusById(ServiceTvPrTmp::ServiceTv($id)['service'])['name'];
				break;
			case '3':
				$status 	=	StatusService::GetStatusById(ServiceSecPrTmp::ServiceSec($id)['service'])['name'];
				break;
			case '4':
				$status 	=	StatusService::GetStatusById(ServiceIntPrTmp::ServiceInt($id)['service'])['name'];
				break;
			case '6':
				$status 	=	StatusService::GetStatusById(ServiceIntVZTmp::ServiceInt($id)['service'])['name'];
				break;
		}

		return [
			'id'		=>	(isset($serv['id'])) 			?	$serv['id'] : "",
			'ticket'	=>	(isset($serv['ticket'])) 		?	$serv['ticket'] : "",
			'score'		=>	(isset($serv['score'])) 		? 	Score::GetScoreId($serv['score'])['name'] : "",
			'trans'		=>	(isset($serv['transactions']))	?	$serv['transactions'] : "",
			'f_ope'		=>	(isset($serv['finish_by'])) 	? 	User::GetUserById($serv['finish_by'])['username'] : "",
			'sale'		=>	(isset($serv['created_by'])) 	? 	User::GetUserById($serv['created_by'])['username'] : "",
			'f_fin'		=>	(isset($serv['finish_at'])) 	?	$serv['finish_at'] : "",
			'fico'		=>	(isset($serv['fico'])) 			?	$serv['fico'] : "",
			'value'		=>	(isset($serv['value'])) 		?	$serv['value'] : "",
			'status'	=>	$status,
			'package'	=>	Package::GetPackageByTicket($serv['ticket']),
		];
		
	}	

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function GetInfoOLD($info, $type, $service)
	{
		$cypher		=	New UnCypher();

        if($service == 'tv')
        {      	

	        if($type == 'ref') 
	        {
		        $query 		=	'SELECT t3.client_id, t3.name, t3.birthday, t3.phone_main, t3.phone_main_provider, t3.phone_alt, t3.phone_alt_provider, t3.phone_other, t3.phone_other_provider, t3.town_id, t3.country_id, t3.ident_id, t3.ident_exp, t3.ss_id, t3.ss_exp, t3.add_main, t3.add_postal, t3.email_main, t3.coor_lati, t3.coor_long, t3.additional, t1.ref_id, t1.tv, t1.package_id, t1.provider_id, t1.decoder_id, t1.dvr_id, t1.payment_id, t1.ch_hd, t1.ch_dvr, t1.ch_hbo, t1.ch_cinemax, t1.ch_starz, t1.ch_showtime, t1.gif_ent, t1.gif_xtra, t1.gif_choice, t1.advertising_id FROM cp_service_pr_tv AS t1 INNER JOIN cp_referred AS t2 ON (t1.ref_list_id = t2.id) INNER JOIN cp_leads AS t3 ON (t2.client_id = t3.client_id) AND t1.ticket = "'.$info['ticket'].'"';
	        }else{

	        	$query 		=	'SELECT t2.client_id, t2.name, t2.birthday, t2.phone_main, t2.phone_main_provider, t2.phone_alt, t2.phone_alt_provider, t2.phone_other, t2.phone_other_provider,t2.town_id, t2.country_id, t2.ident_id, t2.ident_exp, t2.ss_id, t2.ss_exp, t2.add_main, t2.add_postal, t2.email_main, t2.coor_lati, t2.coor_long, t2.additional, t1.ref_id, t1.tv, t1.package_id, t1.provider_id, t1.decoder_id, t1.dvr_id, t1.payment_id, t1.ch_hd, t1.ch_dvr, t1.ch_hbo, t1.ch_cinemax, t1.ch_starz, t1.ch_showtime, t1.gif_ent, t1.gif_xtra, t1.gif_choice, t1.advertising_id FROM cp_service_pr_tv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';
	        }
        }

        if($service == 'sec')
        {
        	if($type == 'ref')
        	{
        		$query = 'SELECT t3.client_id, t3.name, t3.birthday, t3.phone_main, t3.phone_main_provider, t3.phone_alt, t3.phone_alt_provider, t3.phone_other, t3.phone_other_provider, t3.town_id, t3.country_id, t3.ident_id, t3.ident_exp, t3.ss_id, t3.ss_exp, t3.add_main, t3.add_postal, t3.email_main, t3.coor_lati, t3.coor_long, t3.additional, t1.ref_id, t1.previously_id, t1.equipment, t1.cameras_id, t1.comp_equipment, t1.dvr_id, t1.add_equipment, t1.password_alarm, t1.payment_id, t1.contact_1, t1.contact_1_desc, t1.contact_2, t1.contact_2_desc, t1.contact_3, t1.contact_3_desc FROM cp_service_pr_sec AS t1 INNER JOIN cp_referred AS t2 ON (t1.ref_list_id = t2.id) INNER JOIN cp_leads AS t3 ON (t2.client_id = t3.client_id) AND t1.ticket = "'.$info['ticket'].'"';
        	}else{

        		$query = 'SELECT t2.client_id, t2.name, t2.birthday, t2.phone_main, t2.phone_main_provider, t2.phone_alt, t2.phone_alt_provider, t2.phone_other, t2.phone_other_provider, t2.town_id, t2.country_id, t2.ident_id, t2.ident_exp, t2.ss_id, t2.ss_exp, t2.add_main, t2.add_postal, t2.email_main, t2.coor_lati, t2.coor_long, t2.additional, t1.ref_id, t1.previously_id, t1.equipment, t1.cameras_id, t1.comp_equipment, t1.dvr_id, t1.add_equipment, t1.password_alarm, t1.payment_id, t1.contact_1, t1.contact_1_desc, t1.contact_2, t1.contact_2_desc, t1.contact_3, t1.contact_3_desc FROM cp_service_pr_sec AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';
        	}
        }

        if($service == 'int')
        {
        	if($type == 'ref')
        	{

        		$query = 'SELECT t3.client_id, t3.name, t3.birthday, t3.phone_main, t3.phone_main_provider, t3.phone_alt, t3.phone_alt_provider, t3.phone_other, t3.phone_other_provider, t3.town_id, t3.country_id, t3.ident_id, t3.ident_exp, t3.ss_id, t3.ss_exp, t3.add_main, t3.add_postal, t3.email_main, t3.coor_lati, t3.coor_long, t3.additional, t1.ref_id, t1.int_res_id, t1.pho_res_id, t1.int_com_id, t1.pho_com_id, t1.payment_id FROM cp_service_pr_int AS t1 INNER JOIN cp_referred AS t2 ON (t1.ref_list_id = t2.id) INNER JOIN cp_leads AS t3 ON (t2.client_id = t3.client_id) AND t1.ticket = "'.$info['ticket'].'"';	
        	}else{

        		$query = 'SELECT t2.client_id, t2.name, t2.birthday, t2.phone_main, t2.phone_main_provider, t2.phone_alt, t2.phone_alt_provider, t2.phone_other, t2.phone_other_provider, t2.country_id, t2.town_id, t2.ident_id, t2.ident_exp, t2.ss_id, t2.ss_exp, t2.add_main, t2.add_postal, t2.email_main, t2.coor_lati, t2.coor_long, t2.additional, t1.ref_id, t1.int_res_id, t1.pho_res_id, t1.int_com_id, t1.pho_com_id, t1.payment_id FROM cp_service_pr_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';
        	}
        }

        if($service == 'ive')
        {
        	if($type == 'ref')
        	{

        		$query = 'SELECT t3.client_id, t3.name, t3.birthday, t3.phone_main, t3.phone_main_provider, t3.phone_alt, t3.phone_alt_provider, t3.phone_other, t3.phone_other_provider, t3.town_id, t3.country_id, t3.ident_id, t3.ident_exp, t3.ss_id, t3.ss_exp, t3.add_main, t3.add_postal, t3.email_main, t3.coor_lati, t3.coor_long, t3.additional, t1.ref_id, t1.int_res_id, t1.pho_res_id, t1.int_com_id, t1.pho_com_id, t1.payment_id, t1.bank_id, t1.email, t1.amount, t1.transference, t1.trans_date FROM cp_service_vzla_int AS t1 INNER JOIN cp_referred AS t2 ON (t1.ref_list_id = t2.id) INNER JOIN cp_leads AS t3 ON (t2.client_id = t3.client_id) AND t1.ticket = "'.$info['ticket'].'"';	

        	}else{

        		$query = 'SELECT t2.client_id, t2.name, t2.birthday, t2.phone_main, t2.phone_main_provider, t2.phone_alt, t2.phone_alt_provider, t2.phone_other, t2.phone_other_provider, t2.country_id, t2.town_id, t2.ident_id, t2.ident_exp, t2.ss_id, t2.ss_exp, t2.add_main, t2.add_postal, t2.email_main, t2.coor_lati, t2.coor_long, t2.additional, t1.ref_id, t1.int_res_id, t1.pho_res_id, t1.int_com_id, t1.pho_com_id, t1.payment_id, t1.bank_id, t1.email, t1.amount, t1.transference, t1.trans_date FROM cp_service_vzla_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';
        	}
        }

        $result     =   DBSmart::DBQuery($query);

        if($result <> false)
        {
        	$ide =	$cypher->GetInfoCypher($result['client_id'], 'ID');

        	if($ide <> false) 
        		{ $ident = $ide['id']; $ident_exp = $ide['exp']; }
        	else 
        		{ $ident = ""; $ident_exp = ""; }

        	$sss =	$cypher->GetInfoCypher($result['client_id'], 'SS');

        	if($sss <> false) 
        		{ $ss = $sss['ss']; $ss_exp = $sss['exp']; }
        	else 
        		{ $ss = ""; $ss_exp = ""; }

        	$cards =	$cypher->GetInfoCypher($result['client_id'], 'CC');

        	if($cards <> false) 
        		{ $card = $cards['card']; $card_exp = $cards['month']."/".$cards['year']; }
        	else 
        		{ $card = ""; $card_exp = ""; }

			$ABS =	$cypher->GetInfoCypher($result['client_id'], 'AB');

        	if($ABS <> false) 
        		{ 
					$ab_nam = $ABS['name']; $ab_num = $ABS['account']; $ab_ban = Bank::GetBankById($ABS['bank'])['name']; $ab_typ = $ABS['type'];
        		}
        	else 
        		{ $ab_nam = ""; $ab_num = ""; $ab_ban = ""; $ab_typ = ""; }

        	$client	= 	[
        		'ref'		=> 	$result['ref_id'],
        		'name'		=>	$result['name'],
        		'birthday'	=>	$result['birthday'],
        		'phone'		=>	$result['phone_main'],
        		'phone_pro'	=>	($result['phone_main_provider'] <> '') 	? PhoneProvider::GetProviderById($result['phone_main_provider'])['name'] : "",
        		'alter'		=>	$result['phone_alt'],
        		'alter_pro'	=>	($result['phone_alt_provider'] <> '') 	? PhoneProvider::GetProviderById($result['phone_alt_provider'])['name'] : "",
        		'other'		=>	$result['phone_alt'],
        		'other_pro'	=>	($result['phone_other_provider'] <> '') ? PhoneProvider::GetProviderById($result['phone_other_provider'])['name'] : "",
        		'town'		=>	($result['town_id'] <> "") 		? Town::GetTownById($result['town_id'])['name'] : "",
        		'country'	=>	($result['country_id'] <> "") 	? Country::GetCountryById($result['country_id'])['name'] : "",
        		'lati'		=>	(isset($result['coor_lati']))	? $result['coor_lati']	:	"",
        		'long'		=>	(isset($result['coor_long']))	? $result['coor_long']	:	"",
        		'additional'=>  (isset($result['additional']))	? $result['additional']	:	"",
        		'ss'		=>	$ss,
        		'ss_exp'	=>	$ss_exp,
        		'id'		=>	$ident,
        		'id_exp'	=>	$ident_exp,
        		'cc'		=>	$card,
        		'cc_exp'	=>	$card_exp,
        		'ab'		=>	$ab_nam,
        		'ab_num'	=>	$ab_num,
        		'ab_bank'	=>	$ab_ban,
        		'ab_type'	=>	$ab_typ,
        		'add'		=>	$result['add_main'],
        		'add_postal'=>	$result['add_postal'],
        		'email'		=>	$result['email_main']
        	];

       		if($service == 'tv')
        	{

				$serv 	= 	[
	        		'tv'		=>	$result['tv'],
	        		'decoders'	=>	$result['decoder_id'],
	        		'package'	=>	($result['package_id'] <> 0) 	? Package::GetPackageById($result['package_id'])['name'] : "0",
	        		'dvr'		=>	$result['dvr_id'],
	        		'provider'	=>	($result['provider_id'] <> 0)	? Provider::GetProviderById($result['provider_id'])['name'] : "0",
	        		'payment'	=>	Payment::GetPaymentById($result['payment_id'])['name'],
	        		'adv'		=>	($result['advertising_id'] == '1')	? 'SI' : "NO",
	        		'hd'		=>	($result['ch_hd'] == 'on')		? 'SI' : "NO",
	        		'hbo'		=>	($result['ch_hbo'] == 'on')		? 'SI' : "NO",
	        		'cinemax'	=>	($result['ch_cinemax'] == 'on')	? 'SI' : "NO",
	        		'startz'	=>	($result['ch_starz'] == 'on')	? 'SI' : "NO",
	        		'showtime'	=>	($result['ch_showtime'] == 'on')? 'SI' : "NO",
	        		'gift_ent'	=>	($result['gif_ent'] == 'on')	? 'SI' : "NO",
	        		'gift_cho'	=>	($result['gif_choice'] == 'on')	? 'SI' : "NO",
	        		'gift_ext'	=>	($result['gif_xtra'] == 'on')	? 'SI' : "NO",        	
	        	];


	        	return [
	        		'client'	=>	$client,
	        		'service'	=>	$serv
	        	];
        	}

        	if($service == 'sec')
        	{
        		$serv 	= 	[
	        		'previously'	=>	($result['previously_id'] <> 0)	?	"SI" : "NO",
	        		'equipment'		=>	$result['equipment'],
	        		'cameras'		=>	($result['cameras_id'] <> 0) 	? Camera::GetCameraById($result['cameras_id'])['name'] 	: "0",
	        		'comp_equip'	=>	$result['comp_equipment'],
	        		'dvr'			=>	($result['dvr_id'] <> 0) 		? Dvr::GetDvrById($result['dvr_id'])['name'] 		: "0",
	        		'add_equip'		=>	$result['add_equipment'],
	        		'password'		=>	$result['password_alarm'],
	        		'payment'		=>	Payment::GetPaymentById($result['payment_id'])['name'],
	        		'cont1'			=>	$result['contact_1'],
	        		'cont1desc'		=>	$result['contact_1_desc'],
	        		'cont2'			=>	$result['contact_2'],
	        		'cont2desc'		=>	$result['contact_2_desc'],
	        		'cont3'			=>	$result['contact_3'],
	        		'cont3desc'		=>	$result['contact_3_desc']
	        	];

	        	return [
	        		'client'	=>	$client,
	        		'service'	=>	$serv
	        	];	
        	}

        	if($service == 'int')
        	{
        		$serv 	= 	[
	        		'int_res'	=>	($result['int_res_id'] <> 0 )	? Internet::GetInternetById($result['int_res_id'])['name'] 	: "0",
	        		'pho_res'	=>	($result['pho_res_id'] <> 0 )	? Phone::GetPhoneById($result['pho_res_id'])['name'] 	 	: "0",
	        		'int_com'	=>	($result['int_com_id'] <> 0) 	? Internet::GetInternetById($result['int_com_id'])['name'] 	: "0",
	        		'pho_com'	=>	($result['pho_com_id'] <> 0 )	? Phone::GetPhoneById($result['pho_com_id'])['name'] 	 	: "0",
	        		'payment'	=>	Payment::GetPaymentById($result['payment_id'])['name'],
	        		];

	        	return [
	        		'client'	=>	$client,
	        		'service'	=>	$serv
	        	];	
        	}

        	if($service == 'ive')
        	{
        		$serv 	= 	[
	        		'int_res'		=>	($result['int_res_id'] <> 0 )	? Internet::GetInternetById($result['int_res_id'])['name'] 	: 	"0",
	        		'pho_res'		=>	($result['pho_res_id'] <> 0 )	? Phone::GetPhoneById($result['pho_res_id'])['name'] 	 	: 	"0",
	        		'int_com'		=>	($result['int_com_id'] <> 0) 	? Internet::GetInternetById($result['int_com_id'])['name'] 	: 	"0",
	        		'pho_com'		=>	($result['pho_com_id'] <> 0 )	? Phone::GetPhoneById($result['pho_com_id'])['name'] 	 	: 	"0",
	        		'payment'		=>	Payment::GetPaymentById($result['payment_id'])['name'],
	        		'bank'			=>	($result['bank_id'] <> '')		? Bank::GetBankById($result['bank_id'])['name']				:	"",
	        		'amount'		=>	($result['amount'] <> '')		? $result['amount']				:	"0.00",
	        		'trans'			=>	($result['transference'] <> '')	? $result['transference']		:	"",
	        		'trans_date'	=>	($result['trans_date'] <> '')	? $result['trans_date']			:	"",
	        		'email'			=>	($result['email'] <> '')		? $result['email']				:	"",
	        		];

	        	return [
	        		'client'	=>	$client,
	        		'service'	=>	$serv
	        	];	
        	}


        }else{
        	return false;
        }

	}	

/////////////////////////////// Save Qualification Temp /////////////////////////////

	public static function SaveQualTmp($info, $serv, $user)
	{
		$data 	=	[
			'ticket'		=> 	$serv['ticket'],
			'client'		=>	$serv['client'],
			'appro' 		=>	$info['qual_id'],
			'service'		=>	$serv['service'],
			'score'			=>	(isset($info['qual_score'])) ? $info['qual_score'] : "",
			'transactions'	=>	(isset($info['qual_trans'])) ? $info['qual_trans'] : "",
			'value'			=>	(isset($info['qual_value'])) ? $info['qual_value'] : "",
			'fico'			=>	(isset($info['qual_fico']))  ? $info['qual_fico']  : "",
			'operator'		=>	$user
		];

		return 	ApprovalsScoreTmp::SaveScore($data);	

	}

///////////////////////////////// Save Qualification ///////////////////////////////

	public static function SaveQualification($info)
	{
		$date 	= 	date('Y-m-d H:i:s', time());

		$score 	=	ApprovalsScore::SaveScoreCopy($info);
		
		if($score <> false)
		{
			$depart =	Departament::GetDepById(User::GetUserById($info['ope_a'])['departament'])['id'];

			$query 	=	'UPDATE cp_appro_serv SET score_id="'.$score.'", score="'.$info['score'].'", transactions="'.$info['transaction'].'", value="'.$info['value'].'", fico="'.$info['fico'].'", status_id="2", finish_by="'.$info['ope_a'].'", finish_dep_id="'.$depart.'", finish_at="'.$date.'" WHERE ticket = "'.$info['ticket'].'" AND client_id = "'.$info['client'].'"';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;

		}else{ return false; }
	
	}

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function QualHtml($info, $serv, $id)
	{	

		$sc 	=	Score::GetScoreId($info['score']);

		$data 	=	[
			'id'			=>	($info 	<> false)	?	$info['id'] : "",
			'status'		=>	($info 	<> false) 	? 	true : false,
			'score'			=>	($sc 	<> false)	? 	$sc['name'] : "",
			'transaction'	=>	($info 	<> false) 	? 	$info['transaction'] : "",
			'value'			=>	($info 	<> false) 	? 	$info['value'] : "",
			'fico'			=>	($info 	<> false) 	? 	$info['fico'] : "",
			'process'		=>	($sc 	<> false)	? 	User::GetUserById($info['operator'])['username'] : "",
			'date'			=>	($sc 	<> false)	? 	$info['created'] : ""
		];

		$score 	=	Score::GetScoreByType('service', $serv);

		switch ($serv) 
		{
			case '2':
				return 		Approvals::QualTvHtml($data, $score, $id);
				break;

			case '3':
				return 		Approvals::QualSecHtml($data, $score, $id);
				break;

			case '4':
				return 		Approvals::QualIntHtml($data, $score, $id);
				break;

			case '6':
				return 		Approvals::QualIntHtml($data, $score, $id);
				break;
		}

	}

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function RowHtmlPending($info)
	{
		$_replace 	= 	new Config();

		$html 		=	$servHtml	= 	$staHtml		= 	'';
		
		$query 		= 	'SELECT t1.id, t1.ticket, t1.created_at, t1.client_id, t2.name, t2.phone_main, (SELECT IF(t2.referred_id <> "0", "SI", "NO")) AS ref, t2.referred_id AS client, t1.service_id, (SELECT name FROM data_services WHERE id = t1.service_id) AS service, (SELECT username FROM users WHERE id = t1.created_by) AS sale, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departament, t1.status_id, (CASE WHEN t1.status_id = "0" THEN "CANCELADO" WHEN t1.status_id = "1" THEN "PENDIENTE" WHEN t1.status_id = "2" THEN "PROCESADO" ELSE "NINGUNO" END) AS status, t1.score_id, (SELECT name FROM data_score WHERE id = t1.score) AS score, (SELECT username FROM users WHERE id = finish_by) AS operator, t1.finish_by, t1.finish_at FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON(t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';

		$iData 		=	DBSmart::DBQuery($query);

        $query 		=	'SELECT id, score AS score_id, (SELECT name FROM data_score WHERE id = score) AS score, (SELECT username FROM users WHERE id = operator_id) AS operator FROM cp_appro_score_tmp WHERE ticket = "'.$info['ticket'].'" order by id DESC';
        
        $prev 	=	DBSmart::DBQuery($query);

		$score 		=	(isset($prev['score'])) 	? $prev['score'] 	: "";
		
		$operator	=	(isset($prev['operator'])) 	? $prev['operator'] : "";

		$refHtml 	=	($iData['ref'] == "SI") 	? '<span class="label label-danger">SI</span>': '';

		$refClient 	=	($iData['client'] <> "0") 	? $iData['client'] : "";

        switch ($iData['service_id']) 
        {
        	case 2:
        		$servHtml 	= 	'<span class="label label-primary">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_pr_tv WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;
        	
        	case 3:
        		$servHtml = '<span class="label label-danger">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_pr_sec WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;

        	case 4:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_pr_int WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;
        	case 6:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_vzla_int WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;
        }

 		switch ($iData['status_id']) 
 		{
        	case 0:
        		$staHtml = '<span class="label label-danger">'.$iData['status'].'</span>';
        		break;
        	case 1:
        		$staHtml = '<span class="label label-warning">'.$iData['status'].'</span>';
        		break;
        	case 2:
        		$staHtml = '<span class="label label-success">'.$iData['status'].'</span>';
        		break;
        }

    	if(($prev['score_id'] <> "4") AND ($prev['score_id'] <> "5") AND ($prev['score_id'] <> "6") AND ($prev['score_id'] <> "42"))
    	{	


	    	$html.='<tr>';
			$html.='<td>'.$iData['ticket'].'</td>';
			$html.='<td>'.$iData['client_id'].'</td>';
			$html.='<td>'.$_replace->ShowDateAll($iData['created_at']).'</td>';
			$html.='<td>'.$iData['name'].'</td>';
			$html.='<td>'.$iData['phone_main'].'</td>';
			$html.='<td>'.$refHtml.'</td>';
			$html.='<td>'.$servHtml.'</td>';
			$html.='<td>'.$typeOrd.'</td>';
			$html.='<td>'.$iData['sale'].'</td>';
			$html.='<td>'.$iData['departament'].'</td>';
			$html.='<td>'.$staHtml.'</td>';
			$html.='<td>'.$score.'</td>';
			$html.='<td>'.$operator.'</td>';
			$html.='<td><ul class="demo-btns text-center">';
			$html.='<table>';
			$html.='<tbody>';
			$html.='<tr>';
			$html.='<td><a onclick="ViewAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Client"><i class="fa fa-eye"></i></a></td>';
			$html.='<td><a onclick="QualAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Qualification"><i class="fa fa-check"></i></a></td>';
			$html.='<td><a onclick="NotesAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Notes"><i class="fa fa-comments"></i></a></td>';
			$html.='</tr>';
			$html.='<tr>';
			$html.='<td><a onclick="QualAproProc('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Procces Qualification"><i class="fa fa-spinner"></i></a></td>';
			$html.='<td><a onclick="CancAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Cancel"><i class="fa fa-times"></i></a></td>';
			$html.='<td></td>';
			$html.='</tr>';
			$html.='</tbody>';
			$html.='</table>';
			$html.='</ul></td>';
			$html.='</tr>';
    	}

		return $html;
	
	}

	public static function RowHtmlErrorTV($info)
	{

		$_replace 	= 	new Config();

		$html 		=	$servHtml	= 	$staHtml		= 	'';
		
		$query 		= 	'SELECT t1.id, t1.ticket, t1.created_at, t1.client_id, t2.name, t2.phone_main, (SELECT IF(t2.referred_id <> "0", "SI", "NO")) AS ref, t2.referred_id AS client, t1.service_id, (SELECT name FROM data_services WHERE id = t1.service_id) AS service, (SELECT username FROM users WHERE id = t1.created_by) AS sale, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departament, t1.status_id, (CASE WHEN t1.status_id = "0" THEN "CANCELADO" WHEN t1.status_id = "1" THEN "PENDIENTE" WHEN t1.status_id = "2" THEN "PROCESADO" ELSE "NINGUNO" END) AS status, t1.score_id, (SELECT name FROM data_score WHERE id = t1.score) AS score, (SELECT username FROM users WHERE id = finish_by) AS operator, t1.finish_by, t1.finish_at FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON(t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';

		$iData 		=	DBSmart::DBQuery($query);

        $query 		=	'SELECT id, score AS score_id, (SELECT name FROM data_score WHERE id = score) AS score, (SELECT username FROM users WHERE id = operator_id) AS operator FROM cp_appro_score_tmp WHERE ticket = "'.$info['ticket'].'" order by id DESC LIMIT 1';
        
        $prev 		=	DBSmart::DBQuery($query);

        $score 		=	(isset($prev['score'])) 	? $prev['score'] 	: "";

        $operator	=	(isset($prev['operator'])) 	? $prev['operator'] : "";

		$refHtml 	=	($iData['ref'] == "SI") 	? '<span class="label label-danger">SI</span>': '';

		$refClient 	=	($iData['client'] <> "0") 	? $iData['client'] : "";

        switch ($iData['service_id']) 
        {
        	case 2:
        		$servHtml = '<span class="label label-primary">'.$iData['service'].'</span>';
        		break;
        	
        	case 3:
        		$servHtml = '<span class="label label-danger">'.$iData['service'].'</span>';
        		break;

        	case 4:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		break;
        	case 6:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		break;
        }

 		switch ($iData['status_id']) 
 		{
        	case 0:
        		$staHtml = '<span class="label label-danger">'.$iData['status'].'</span>';
        		break;
        	case 1:
        		$staHtml = '<span class="label label-warning">'.$iData['status'].'</span>';
        		break;
        	case 2:
        		$staHtml = '<span class="label label-success">'.$iData['status'].'</span>';
        		break;
        }

    	if(($prev['score_id'] == "4") OR ($prev['score_id'] == "5") OR ($prev['score_id'] == "6") OR ($prev['score_id'] == "42"))
    	{
	    	$html.='<tr>';
			$html.='<td>'.$iData['ticket'].'</td>';
			$html.='<td>'.$iData['client_id'].'</td>';
			$html.='<td>'.$_replace->ShowDateAll($iData['created_at']).'</td>';
			$html.='<td>'.$iData['name'].'</td>';
			$html.='<td>'.$iData['phone_main'].'</td>';
			$html.='<td>'.$refHtml.'</td>';
			$html.='<td>'.$refClient.'</td>';
			$html.='<td>'.$servHtml.'</td>';
			$html.='<td>'.$iData['sale'].'</td>';
			$html.='<td>'.$iData['departament'].'</td>';
			$html.='<td>'.$staHtml.'</td>';
			$html.='<td>'.$score.'</td>';
			$html.='<td>'.$operator.'</td>';
			$html.='<td><ul class="demo-btns text-center">';
			$html.='<table>';
			$html.='<tbody>';
			$html.='<tr>';
			$html.='<td><a onclick="ViewAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Client"><i class="fa fa-eye"></i></a></td>';
			$html.='<td><a onclick="QualAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Qualification"><i class="fa fa-check"></i></a></td>';
			$html.='<td><a onclick="NotesAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Notes"><i class="fa fa-comments"></i></a></td>';
			$html.='</tr>';
			$html.='<tr>';
			$html.='<td><a onclick="QualAproProc('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Procces Qualification"><i class="fa fa-spinner"></i></a></td>';
			$html.='<td><a onclick="CancAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Cancel"><i class="fa fa-times"></i></a></td>';
			$html.='<td></td>';
			$html.='</tr>';
			$html.='</tbody>';
			$html.='</table>';
			$html.='</ul></td>';
			$html.='</tr>';
    	}

		return $html;
	
	}

//////////////////////////////////// Row Single ////////////////////////////////////

	public static function Closures($dates)
	{
		$_replace 	= 	new Config();

		$dateC 		= 	date("Y-m-d");

		if($dates['dateIni'] == "" AND $dates['dateEnd'] == "")
		{
			// $query 		=	'SELECT t1.id, t1.view, t1.client_id, t1.ticket, t2.name, t1.created_at, t1.service_id, (CASE WHEN t1.service_id = "2" THEN "TELEVISION PR" WHEN t1.service_id = "3" THEN "SEGURIDAD PR" WHEN t1.service_id = "4" THEN "INTERNET PR" WHEN t1.service_id = "6" THEN "INTERNET VZLA" 	ELSE "ERROR" END) AS servicio, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departamento, t1.status_id, (CASE WHEN t1.status_id = "0" AND t1.cancel = "0" THEN "CANCELADO" WHEN t1.status_id = "1" AND t1.cancel = "1" THEN "PENDIENTE"	WHEN t1.status_id = "2" AND t1.cancel = "1" THEN "PROCESADO" ELSE "ERROR" END) AS status, (SELECT name FROM data_score WHERE id = (SELECT score FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS procesado, (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_by, (SELECT created_at FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_at FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.created_at LIKE "'.$dateC.'%" ORDER BY t1.id DESC';

			$query 		=	'SELECT t1.id, t1.view, t1.client_id, t1.ticket, t2.name, t1.created_at, t1.service_id, (CASE WHEN t1.service_id = "2" THEN "TELEVISION PR" WHEN t1.service_id = "3" THEN "SEGURIDAD PR" WHEN t1.service_id = "4" THEN "INTERNET PR" WHEN t1.service_id = "6" THEN "INTERNET VZLA" 	ELSE "ERROR" END) AS servicio, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departamento, t1.status_id, (CASE WHEN t1.status_id = "0" AND t1.cancel = "0" THEN "CANCELADO" WHEN t1.status_id = "1" AND t1.cancel = "1" THEN "PENDIENTE"	WHEN t1.status_id = "2" AND t1.cancel = "1" THEN "PROCESADO" ELSE "ERROR" END) AS status, (SELECT name FROM data_score WHERE id = (SELECT score FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS procesado, (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_by, (SELECT created_at FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_at,(SELECT username FROM users WHERE id = finish_by) AS procesado_c, t1.finish_at AS finish_at_c FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.created_at LIKE "'.$dateC.'%" ORDER BY t1.id DESC';

			$info     =   DBSmart::DBQueryAll($query);

			if($info <> false)
			{
				foreach ($info as $k => $val) 
				{
					switch ($val['status_id']) 
					{
						case '0':
							$status = '<span class="label label-danger"> '.$val['status'].' </span>';
							break;
						case '1':
							$status = '<span class="label label-warning"> '.$val['status'].' </span>';
							break;
						case '2':
							$status = '<span class="label label-success"> '.$val['status'].' </span>';
							break;
					} 

					switch ($val['service_id']) 
					{
						case '2':
							$servHtml 	= '<span class="label label-primary">'.$val['servicio'].'</span>';
							break;
						case '3':
							$servHtml 	= '<span class="label label-danger">'.$val['servicio'].'</span>';
							break;
						case '4':
							$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
							break;
						case '6':
							$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
							break;
					}

					if($val['status_id'] <> "0")
					{ 
						$operator 	= 	($val['finish_by'] <> null)	?	$val['procesado'] 	: '';
						$finish 	=	($val['finish_at'] <> '') 	?	$_replace->ShowDateAll($val['finish_at']) : '';
						$score 		=	($val['score'] <> '' ) 		?	$val['score'] : '';

					}else{
						$operator 	= 	($val['finish_by'] <> null)	?	$val['procesado_c'] : '';
						$finish 	=	($val['finish_at_c'] <> '')	?	$_replace->ShowDateAll($val['finish_at_c']) : '';
						$score 		=	'';
					}

					$iData[$k]	=	[
						'id'			=>	$val['id'],
						'idClient'		=>	$val['client_id'],
						'ticket'		=>	$val['ticket'],
						'client'		=>	$val['name'],
						'date'			=> 	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
						'service'		=>	$servHtml,
						'status'		=>	$status,
						'score'			=>	$score,
						'sale'			=>	$val['vendedor'],
						'depart'		=>	$val['departamento'],
						'created'		=>	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
						'process'		=> 	$operator,
						'finish'		=>	$finish,
						'view'			=>	$val['view']
					];
				
				}

				return $iData;

			}else{ return false; }

		}else{

			$query 	=	'SELECT t1.id, t1.view, t1.client_id, t1.ticket, t2.name, t1.created_at, t1.service_id, (CASE WHEN t1.service_id = "2" THEN "TELEVISION PR" WHEN t1.service_id = "3" THEN "SEGURIDAD PR" WHEN t1.service_id = "4" THEN "INTERNET PR" WHEN t1.service_id = "6" THEN "INTERNET VZLA" 	ELSE "ERROR" END) AS servicio, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departamento, t1.status_id, (CASE WHEN t1.status_id = "0" AND t1.cancel = "0" THEN "CANCELADO" WHEN t1.status_id = "1" AND t1.cancel = "1" THEN "PENDIENTE"	WHEN t1.status_id = "2" AND t1.cancel = "1" THEN "PROCESADO" ELSE "ERROR" END) AS status, (SELECT name FROM data_score WHERE id = (SELECT score FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS procesado, (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_by, (SELECT created_at FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_at,(SELECT username FROM users WHERE id = finish_by) AS procesado_c, t1.finish_at AS finish_at_c FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY t1.id ASC';

			$info     =   DBSmart::DBQueryAll($query);

			if($info <> false)
			{
				foreach ($info as $k => $val) 
				{
					switch ($val['status_id']) 
					{
						case '0':
							$status = '<span class="label label-danger"> '.$val['status'].' </span>';
							break;
						case '1':
							$status = '<span class="label label-warning"> '.$val['status'].' </span>';
							break;
						case '2':
							$status = '<span class="label label-success"> '.$val['status'].' </span>';
							break;
					} 

					switch ($val['service_id']) 
					{
						case '2':
							$servHtml 	= '<span class="label label-primary">'.$val['servicio'].'</span>';
							break;
						case '3':
							$servHtml 	= '<span class="label label-danger">'.$val['servicio'].'</span>';
							break;
						case '4':
							$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
							break;
						case '6':
							$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
							break;
					}

					if($val['status_id'] <> "0")
					{ 
						$operator 	= 	($val['finish_by'] <> '1')	?	$val['procesado'] : '';
						$finish 	=	($val['finish_at'] <> '') 	?	$_replace->ShowDateAll($val['finish_at']) : '';
						$score 		=	($val['score'] <> '' ) 		?	$val['score'] : '';

					}else{
						$operator 	= 	($val['finish_by'] <> '1')	?	$val['procesado_c'] : '';
						$finish 	=	($val['finish_at_c'] <> '')	?	$_replace->ShowDateAll($val['finish_at_c']) : '';
						$score 		=	'';
					}

					$iData[$k]	=	[
						'id'			=>	$val['id'],
						'idClient'		=>	$val['client_id'],
						'ticket'		=>	$val['ticket'],
						'client'		=>	$val['name'],
						'date'			=> 	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
						'service'		=>	$servHtml,
						'status'		=>	$status,
						'score'			=>	$score,
						'sale'			=>	$val['vendedor'],
						'depart'		=>	$val['departamento'],
						'created'		=>	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
						'process'		=> 	$operator,
						'finish'		=>	$finish,
						'view'			=>	$val['view']
					];
				
				}

				return $iData;

			}

		
		}
	
	}

	public static function ViewClosures()
	{
        $query 		=	'SELECT t1.client_id, t1.ticket, t2.name, t3.name AS score, t1.score as score_id, t3.service_id AS service, (SELECT username FROM users WHERE id=t1.created_by) AS sale, (SELECT username FROM users WHERE id=t1.finish_by) AS appro FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_score AS t3 ON (t1.score = t3.id) AND t1.view = "0" AND (t1.score = "1" OR t1.score = "2" OR t1.score = "3" OR t1.score = "11" OR t1.score = "22") ORDER BY t1.id ASC';
     
        $result     =   DBSmart::DBQuery($query);

        $html = $image = '';

        if($result)
        {
        	switch ($result['score_id']) 
        	{
        		case '1':
        			$image 	= '/assets/img/approvals/SC1.png';
        			break;
        		case '2':
        			$image 	= '/assets/img/approvals/SC2.png';
        			break;
        		case '3':
        			$image 	= '/assets/img/approvals/SC3.png';
        			break;
        		case '11':
        			$image 	= '/assets/img/approvals/SC4.png';
        			break;
        		case '22':
        			$image 	= '/assets/img/approvals/SC5.png';
        			break;
        	}

			$html.='<div class="row">';
			$html.='<div class="col-md-6" style="text-align: center;">';
			$html.='<img src="'.$image.'" alt="SmartAdmin" style="width: 250px;">';
			$html.='</div>';
			$html.='<div class="col-md-6">';
			$html.='<div id="tableModal"><table class="table table-bordered " border="1" style="border-collapse: collapse;"></table><table class="table table-bordered" style="text-align: center; margin-top: 10px; font-size: 11px;"><tbody><tr><td>ID</td><td>'.$result['client_id'].'</td><td>CLIENTE</td><td>'.$result['name'].'</td></tr><tr><td>TICKET</td><td>'.$result['ticket'].'</td><td>CALIFICADO POR</td><td>'.$result['appro'].'</td></tr></tbody></table></div>';
			$html.='<div id="showScore"><h1 style="text-align: center; font-size: 40px; font-family: serif;">'.$result['sale'].'</h1></div>';
			$html.='</div>';
			$html.='</div>';

			$query  =	'UPDATE cp_appro_serv SET view="1" WHERE ticket = "'.$result['ticket'].'"';

			$dep 	=	DBSmart::DataExecute($query);

			return $html;

        }else{	return $html;	}

	}

	public static function ViewClosuresSearch($dates)
	{
		$_replace 	= 	new Config();
	
		$query 		=	'SELECT t1.id, t1.view, t1.client_id, t1.ticket, t2.name, t1.created_at, t1.service_id, (CASE WHEN t1.service_id = "2" THEN "TELEVISION PR" WHEN t1.service_id = "3" THEN "SEGURIDAD PR" WHEN t1.service_id = "4" THEN "INTERNET PR" WHEN t1.service_id = "6" THEN "INTERNET VZLA" 	ELSE "ERROR" END) AS servicio, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departamento, t1.status_id, (CASE WHEN t1.status_id = "0" AND t1.cancel = "0" THEN "CANCELADO" WHEN t1.status_id = "1" AND t1.cancel = "1" THEN "PENDIENTE"	WHEN t1.status_id = "2" AND t1.cancel = "1" THEN "PROCESADO" ELSE "ERROR" END) AS status, (SELECT name FROM data_score WHERE id = (SELECT score FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS procesado, (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_by, (SELECT created_at FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_at,(SELECT username FROM users WHERE id = finish_by) AS procesado_c, t1.finish_at AS finish_at_c FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY t1.finish_at ASC';

			$info     =   DBSmart::DBQueryAll($query);

			if($info <> false)
			{
				foreach ($info as $k => $val) 
				{
					switch ($val['status_id']) 
					{
						case '0':
							$status = '<span class="label label-danger"> '.$val['status'].' </span>';
							break;
						case '1':
							$status = '<span class="label label-warning"> '.$val['status'].' </span>';
							break;
						case '2':
							$status = '<span class="label label-success"> '.$val['status'].' </span>';
							break;
					} 

					switch ($val['service_id']) 
					{
						case '2':
							$servHtml 	= '<span class="label label-primary">'.$val['servicio'].'</span>';
							break;
						case '3':
							$servHtml 	= '<span class="label label-danger">'.$val['servicio'].'</span>';
							break;
						case '4':
							$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
							break;
						case '6':
							$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
							break;
					}

					if($val['status_id'] <> "0")
					{ 
						$operator 	= 	($val['finish_by'] <> '1')	?	$val['procesado'] : '';
						$finish 	=	($val['finish_at'] <> '') 	?	$_replace->ShowDateAll($val['finish_at']) : '';
						$score 		=	($val['score'] <> '' ) 		?	$val['score'] : '';

					}else{
						$operator 	= 	($val['finish_by'] <> '1')	?	$val['procesado_c'] : '';
						$finish 	=	($val['finish_at_c'] <> '')	?	$_replace->ShowDateAll($val['finish_at_c']) : '';
						$score 		=	'';
					}


					if(($status == "PROCESADO") && ($score == "SCORE 3"))
					{
						$status2 	=	"NO PROCEDE";
					}else{
						$status2 	=	$status;
					}

					$iData[$k]	=	[
						'id'			=>	$val['id'],
						'idClient'		=>	$val['client_id'],
						'ticket'		=>	$val['ticket'],
						'client'		=>	$val['name'],
						'date'			=> 	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
						'service'		=>	$servHtml,
						'status'		=>	$status2,
						'score'			=>	$score,
						'sale'			=>	$val['vendedor'],
						'depart'		=>	$val['departamento'],
						'created'		=>	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
						'process'		=> 	$operator,
						'finish'		=>	$finish,
						'view'			=>	$val['view']
					];
				
				}

				return $iData;

			}else{	return false;	}


	}


	public static function LoadClosures($serv, $date)
	{
		$_replace 	= 	new Config();
		$week       =   $_replace->CurrentMonthDays();

		if($date == false)
		{
			$betw		=	'created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$betC		=	'BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$dateC 		=	't1.created_at LIKE "'.date("Y-m-d").'%"';

		}else{
			$betw		=	'created_at BETWEEN'.' "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';
			$betC		=	'BETWEEN'.' "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';
			$dateC 		=	't1.created_at BETWEEN'.' "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';

		}

		if($serv == "2")
		{
			$query 		=	'SELECT id FROM data_score WHERE service_id = "2" AND (id <> "1" AND id <> "2" AND id <> "3") ORDER BY id ASC';
			$scTV		=	DBSmart::DBQueryAll($query);
			$nscore  	=   "";
		    $nscore  	=   "(";
		    foreach ($scTV as $k => $ka) 
		    {
		        $nscore.='score = "'.$ka['id'].'" OR ';
		    }
		    $nscore 	= substr($nscore, 0, -4);
		    $nscore.=')';

			$betwA5		=	't1.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$betwC5		=	't2.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$score 		=	'(score = "1" OR score = "2" OR score = "3")';
			$scoreT5A 	=	'(t1.score = "1" OR t1.score = "2" OR t1.score = "3")';
			$scoreT5C 	=	'(t2.score = "1" OR t2.score = "2" OR t2.score = "3")';

			$img 		=	'TVPR';

		}
		elseif($serv == "4")
		{
			$query 		=	'SELECT id FROM data_score WHERE service_id = "4" AND id <> "22" ORDER BY id ASC';
			$scTV		=	DBSmart::DBQueryAll($query);
			$nscore  	=   "";
		    $nscore  	=   "(";
		    foreach ($scTV as $k => $ka) 
		    {
		        $nscore.='score = "'.$ka['id'].'" OR ';
		    }
		    $nscore 	= substr($nscore, 0, -4);
		    $nscore.=')';

			$betwA5		=	't1.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$betwC5		=	't2.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$score 		=	'(score = "22")';
			$scoreT5A 	=	'(t1.score = "22")';
			$scoreT5C 	=	'(t2.score = "22")';

			$img 		=	'INTPR';

		}
		elseif($serv == "6")
		{
			$query 		=	'SELECT id FROM data_score WHERE service_id = "6" AND id <> "29" ORDER BY id ASC';
			$scTV		=	DBSmart::DBQueryAll($query);
			$nscore  	=   "";
		    $nscore  	=   "(";
		    foreach ($scTV as $k => $ka) 
		    {
		        $nscore.='score = "'.$ka['id'].'" OR ';
		    }
		    $nscore 	= substr($nscore, 0, -4);
		    $nscore.=')';

			$betwA5		=	't1.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$betwC5		=	't2.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
			$score 		=	'(score = "29")';
			$scoreT5A 	=	'(t1.score = "29")';
			$scoreT5C 	=	'(t2.score = "29")';

			$img 		=	'INTVE';
		
		}

		$query 	=	'SELECT COUNT(*) AS T, t1.created_by, t3.id, t3.username FROM cp_appro_serv AS t1 INNER JOIN cp_coord_serv AS t2 ON (t1.ticket = t2.ticket) INNER JOIN users AS t3 ON (t1.created_by = t3.id) AND t1.service_id = "'.$serv.'" AND t2.coord_id <> "0" AND t1.finish_at '.$betC.' GROUP BY t1.created_by ORDER BY T DESC LIMIT 5';


		$top5	=	DBSmart::DBQueryAll($query);

		if($top5 <> false)
		{
	        foreach ($top5 as $t => $top) 
	        {

	            $query  = 'SELECT
	            (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$top['created_by'].'" AND service_id = "'.$serv.'" AND finish_at '.$betC.') AS T,
	            (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$top['created_by'].'" AND service_id = "'.$serv.'" AND '.$score.' AND finish_at '.$betC.') AS TApro';

				// var_dump($query);exit;

	            $sta    =   DBSmart::DBQuery($query);

	            $imgLink 	=	PROJECTPATH.'/assets/img/operator/IMG'.$top['id'].'.jpg';
	            $imgLink 	=	PROJECTPATH.'/assets/img/operator/IMG'.$top['id'].'.jpg';

				if (file_exists($imgLink)) 
				{
				    $img = '/assets/img/operator/IMG'.$top['id'].'.jpg';
				} else {
				    $img = '/assets/img/operator/user.jpg';
				}		

	            $img 	=	($t == 0) 
	            				? '<img src="'.$img.'" class="img-circle" style="width: 100px;"> <h3 style="color: black;">'.$top['username'].'</h3>' 
	            				: '<img src="'.$img.'" class="img-circle" style="width: 100px;"> <h3 style="color: black;">OPERADOR</h3>';

	            $statics[$t]    =   [
	                'username'      =>  $top['username'],
	                'img'			=>	$img,
	                'sometidas'     =>  $sta['T'],
	                'aprobadas'     =>  $sta['TApro'],
	                'coordinadas'   =>  $top['T'],
	            ];

	        }

		}else{
			$statics	=	false;
		}


		return [
			'top5'	=>	$top5,
			'sta'	=>	$statics
		];

	}

	public static function LoadClosuresData($date)
	{
        $_replace 	= 	new Config();

		if($date == false)
		{
			$dateC 		=	't1.created_at LIKE "'.date("Y-m-d").'%"';

		}else{
			$dateC 		=	't1.created_at BETWEEN'.' "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';

		}

    	$query 		=	'SELECT t1.id, t1.view, t1.client_id, t1.ticket, t2.name, t1.created_at, t1.service_id, (CASE WHEN t1.service_id = "2" THEN "TELEVISION PR" WHEN t1.service_id = "3" THEN "SEGURIDAD PR" WHEN t1.service_id = "4" THEN "INTERNET PR" WHEN t1.service_id = "6" THEN "INTERNET VZLA" 	ELSE "ERROR" END) AS servicio, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departamento, t1.status_id, (CASE WHEN t1.status_id = "0" AND t1.cancel = "0" THEN "CANCELADO" WHEN t1.status_id = "1" AND t1.cancel = "1" THEN "PENDIENTE"	WHEN t1.status_id = "2" AND t1.cancel = "1" THEN "PROCESADO" ELSE "ERROR" END) AS status, (SELECT name FROM data_score WHERE id = (SELECT score FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS score, t1.score AS score_id, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS procesado, (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_by, (SELECT created_at FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS finish_at,(SELECT username FROM users WHERE id = finish_by) AS procesado_c, t1.finish_at AS finish_at_c FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND '.$dateC.' ORDER BY t1.id DESC';

		$info     =   DBSmart::DBQueryAll($query);

		if($info <> false)
		{
			foreach ($info as $k => $val) 
			{
				switch ($val['status_id']) 
				{
					case '0':
						$status = '<span class="label label-danger"> '.$val['status'].' </span>';
						break;
					case '1':
						$status = '<span class="label label-warning"> '.$val['status'].' </span>';
						break;
					case '2':
						$status = '<span class="label label-success"> '.$val['status'].' </span>';
						break;
				} 

				switch ($val['service_id']) 
				{
					case '2':
						$servHtml 	= '<span class="label label-primary">'.$val['servicio'].'</span>';
						break;
					case '3':
						$servHtml 	= '<span class="label label-danger">'.$val['servicio'].'</span>';
						break;
					case '4':
						$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
						break;
					case '6':
						$servHtml 	= '<span class="label label-success">'.$val['servicio'].'</span>';
						break;
				}

				if($val['status_id'] <> "0")
				{ 
					$operator 	= 	($val['finish_by'] <> null)	?	$val['procesado'] 	: '';
					$finish 	=	($val['finish_at'] <> '') 	?	$_replace->ShowDateAll($val['finish_at']) : '';
					$score 		=	($val['score'] <> '' ) 		?	$val['score'] : '';

				}else{
					$operator 	= 	($val['finish_by'] <> null)	?	$val['procesado_c'] : '';
					$finish 	=	($val['finish_at_c'] <> '')	?	$_replace->ShowDateAll($val['finish_at_c']) : '';
					$score 		=	'';
				}

				if(($val['status'] == "2") AND ($val['score_id'] == "3"))
				{
					$status2 	=	"NO PROCEDE";
					var_dump($status2);
					exit;
				}else{
					$status2 	=	$status;
				}


				$iData[$k]	=	[
					'id'			=>	$val['id'],
					'idClient'		=>	$val['client_id'],
					'ticket'		=>	$val['ticket'],
					'client'		=>	$val['name'],
					'date'			=> 	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
					'service'		=>	$servHtml,
					'status'		=>	$status2,
					'score'			=>	$score,
					'sale'			=>	$val['vendedor'],
					'depart'		=>	$val['departamento'],
					'created'		=>	($val['created_at'] <> '') 	?	$_replace->ShowDateAll($val['created_at']) : '',
					'process'		=> 	$operator,
					'finish'		=>	$finish,
					'view'			=>	$val['view']
				];

			
			}

			return $iData;

		
		}
		else
		{ 
			return false; 
		}
	}

//////////////////////////////////// Row Finish ////////////////////////////////////

	public static function RowHtmlProcessed($info)
	{
		$_replace 	= 	new Config();

		$html 		=	'';

		$query 		=	'SELECT t1.id, t1.ticket, t1.created_at AS created, t1.client_id, t2.name, t2.phone_main AS phone, (SELECT IF(t2.referred_id <> "0", "SI", "NO")) AS ref, t2.referred_id AS client, t1.service_id, (SELECT name FROM data_services WHERE id = t1.service_id) AS service, (SELECT username FROM users WHERE id = t1.created_by) AS sale, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departament, t1.status_id, (SELECT IF(t1.status_id = 1, "PENDIENTE", "PROCESADO")) AS status, (SELECT name FROM data_score WHERE id = t1.score) AS score, (SELECT username FROM users WHERE id = finish_by) AS operator, t1.finish_by, t1.finish_at FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON(t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';

		$iData 		=	DBSmart::DBQuery($query);

		$servHtml 	= 	'<span class="label label-warning">OTHER</span>';
		$typeOrd 	=	"OTHER"; 
		
        switch ($iData['service_id']) 
        {
        	case 2:
        		$servHtml 	= 	'<span class="label label-primary">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_pr_tv WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;
        	
        	case 3:
        		$servHtml = '<span class="label label-danger">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_pr_sec WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;

        	case 4:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_pr_int WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;
        	case 6:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		$query 		=	'SELECT (SELECT name FROM data_type_order WHERE id = type_order_id) AS type FROM cp_service_vzla_int WHERE ticket = "'.$info['ticket'].'"';
        		$typeOrd 	=  	DBSmart::DBQuery($query)['type'];
        		break;
        }

    	$staHtml 	= 	($iData['status_id'] == 0) 	? '<span class="label label-danger">'.$iData['status'].'</span>' : '<span class="label label-success">'.$iData['status'].'</span>';

		$refHtml 	=	($iData['ref'] == "SI") 	? '<span class="label label-danger">SI</span>': '';
		
		$refClient 	=	($iData['client'] <> "0") 	? $iData['client'] : "";

		$operator 	=	($iData['finish_by'] <> "1") ? $iData['operator'] : "";

    	$html.='<tr>';
		$html.='<td>'.$iData['ticket'].'</td>';
		$html.='<td>'.$iData['client_id'].'</td>';
		$html.='<td>'.$_replace->ShowDateAll($iData['created']).'</td>';
		$html.='<td>'.$iData['name'].'</td>';
		$html.='<td>'.$iData['phone'].'</td>';
		$html.='<td>'.$refHtml.'</td>';
		$html.='<td>'.$servHtml.'</td>';
		$html.='<td>'.$typeOrd.'</td>';
		$html.='<td>'.$iData['sale'].'</td>';
		$html.='<td>'.$iData['departament'].'</td>';
		$html.='<td>'.$staHtml.'</td>';
		$html.='<td>'.$iData['score'].'</td>';
		$html.='<td>'.$operator.'</td>';
		$html.='<td>'.$_replace->ShowDateAll($iData['finish_at']).'</td>';

		$html.='<td><ul class="demo-btns text-center">';
		$html.='<table>';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><a onclick="ViewAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Clien"><i class="fa fa-eye"></i></a></td>';
		$html.='<td><a onclick="NotesAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Notes"><i class="fa fa-edit"></i></a></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
        $html.='</ul> </td>';
		$html.='</tr>';
		return $html;
	
	}

///////////////////////////////// Table HTML Pending ////////////////////////////////

	public static function TableHtmlPending($info)
	{
		$html = '';
		$html.='<table id="TablePendingAprovHtml" class="table table-bordered" border="1" style="font-size: 10px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>Ticket</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Fecha</th>';
		$html.='<th>Nombre</th>';
		$html.='<th>Telefono</th>';
		$html.='<th>Ref</th>';
		$html.='<th>Servicio</th>';
		$html.='<th>Orden</th>';
		$html.='<th>Vendedor</th>';
		$html.='<th>Depart.</th>';
		$html.='<th>Status</th>';
		$html.='<th>Score</th>';
		$html.='<th>Procesado</th>';
		$html.='<th>Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		$html.=''.$info.'';

		$html.='</tbody>';
		$html.='</table>';
		return $html;

	}

	public static function TableHtmlErrorTV($info)
	{
		$html = '';
		$html.='<table id="TableErrorTVAprovHtml" class="table table-bordered" border="1" style="font-size: 10px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>Ticket</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Fecha</th>';
		$html.='<th>Nombre</th>';
		$html.='<th>Telefono</th>';
		$html.='<th>Ref</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Servicio</th>';
		$html.='<th>Vendedor</th>';
		$html.='<th>Depart.</th>';
		$html.='<th>Status</th>';
		$html.='<th>Score</th>';
		$html.='<th>Procesado</th>';
		$html.='<th>Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		$html.=''.$info.'';

		$html.='</tbody>';
		$html.='</table>';
		return $html;
	}

///////////////////////////////// Table HTML Cancel ////////////////////////////////

	public static function RowHtmlCancel($info)
	{
		$_replace 	= 	new Config();

		$html 		=	$servHtml	= 	$staHtml		= 	'';
		
		$query 		= 	'SELECT t1.id, t1.ticket, t1.created_at, t1.client_id, t2.name, t2.phone_main, (SELECT IF(t2.referred_id <> "0", "SI", "NO")) AS ref, t2.referred_id AS client, t1.service_id, (SELECT name FROM data_services WHERE id = t1.service_id) AS service, (SELECT username FROM users WHERE id = t1.created_by) AS sale, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departament, t1.status_id, (CASE WHEN t1.status_id = "0" THEN "CANCELADO" WHEN t1.status_id = "1" THEN "PENDIENTE" WHEN t1.status_id = "2" THEN "PROCESADO" ELSE "NINGUNO" END) AS status, t1.score_id, (SELECT name FROM data_score WHERE id = t1.score) AS score, (SELECT username FROM users WHERE id = finish_by) AS operator, t1.finish_by, t1.finish_at FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON(t1.client_id = t2.client_id) AND t1.ticket = "'.$info['ticket'].'"';

		$iData 		=	DBSmart::DBQuery($query);

		$refHtml 	=	($iData['ref'] == "SI") 	? '<span class="label label-danger">SI</span>': '';

		$refClient 	=	($iData['client'] <> "0") 	? $iData['client'] : "";

        switch ($iData['service_id']) 
        {
        	case 2:
        		$servHtml = '<span class="label label-primary">'.$iData['service'].'</span>';
        		break;
        	
        	case 3:
        		$servHtml = '<span class="label label-danger">'.$iData['service'].'</span>';
        		break;

        	case 4:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		break;
        	case 6:
        		$servHtml = '<span class="label label-success">'.$iData['service'].'</span>';
        		break;
        }

 		switch ($iData['status_id']) 
 		{
        	case 0:
        		$staHtml = '<span class="label label-danger">'.$iData['status'].'</span>';
        		break;
        	case 1:
        		$staHtml = '<span class="label label-warning">'.$iData['status'].'</span>';
        		break;
        	case 2:
        		$staHtml = '<span class="label label-success">'.$iData['status'].'</span>';
        		break;
        }

		$operator 	=	($iData['finish_by'] <> "1") ? $iData['operator'] : "";

    	$html.='<tr>';
		$html.='<td>'.$iData['ticket'].'</td>';
		$html.='<td>'.$iData['client_id'].'</td>';
		$html.='<td>'.$_replace->ShowDateAll($iData['created_at']).'</td>';
		$html.='<td>'.$iData['name'].'</td>';
		$html.='<td>'.$iData['phone_main'].'</td>';
		$html.='<td>'.$refHtml.'</td>';
		$html.='<td>'.$refClient.'</td>';
		$html.='<td>'.$servHtml.'</td>';
		$html.='<td>'.$iData['sale'].'</td>';
		$html.='<td>'.$iData['departament'].'</td>';
		$html.='<td>'.$staHtml.'</td>';
		$html.='<td>'.$operator.'</td>';
		$html.='<td>'.$_replace->ShowDateAll($iData['finish_at']).'</td>';

		$html.='<td><ul class="demo-btns text-center">';
		$html.='<table>';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><a onclick="ViewAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Clien"><i class="fa fa-eye"></i></a></td>';
		$html.='<td><a onclick="NotesAproPend('.(int)$iData['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Notes"><i class="fa fa-edit"></i></a></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
        $html.='</ul> </td>';
		$html.='</tr>';
		return $html;
	
	}

///////////////////////////////// Table HTML Processed ///////////////////////////////

	public static function TableHtmlProcessed($info)
	{

		$html = '';
		$html.='<table id="TableProcessAprovHtml" class="table table-bordered" border="1" style="font-size: 10px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>Ticket</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Fecha</th>';
		$html.='<th>Nombre</th>';
		$html.='<th>Telefono</th>';
		$html.='<th>Ref</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Servicio</th>';
		$html.='<th>Vendedor</th>';
		$html.='<th>Depart.</th>';
		$html.='<th>Status</th>';
		$html.='<th>Score</th>';
		$html.='<th>Procesado</th>';
		$html.='<th>Date</th>';
		$html.='<th>Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		$html.=''.$info.'';

		$html.='</tbody>';
		$html.='</table>';
		return $html;

	}

	public static function TableHtmlCancel($info)
	{

		$html = '';
		$html.='<table id="TableCancelprovHtml" class="table table-bordered" border="1" style="font-size: 10px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>Ticket</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Fecha</th>';
		$html.='<th>Nombre</th>';
		$html.='<th>Telefono</th>';
		$html.='<th>Ref</th>';
		$html.='<th>Servicio</th>';
		$html.='<th>Orden</th>';
		$html.='<th>Vendedor</th>';
		$html.='<th>Depart.</th>';
		$html.='<th>Status</th>';
		$html.='<th>Procesado</th>';
		$html.='<th>Date</th>';
		$html.='<th>Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		$html.=''.$info.'';

		$html.='</tbody>';
		$html.='</table>';
		return $html;

	}

	public static function InfoViewClient($info, $type, $score)
	{

		$html = '';
   		$_replace 	= 	new Config();


		if($type == 'referred')
		{
			$html.='<div class="row">';
			$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
			$html.='<div class="alert alert-danger fade in"><i class="fa-fw fa fa-times"></i><strong>Datos de Cliente Principal!</strong> Esta Solicitud muestra los Datos del Cliente Principal.</div>';
			$html.='</div">';
			$html.='</div">';
		}


		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
	
		if($type == 'client')
		{
			$html.='<header style="text-align: center;">INFORMACION DE CLIENTE</header>';
		}else{
			$html.='<header style="text-align: center;">INFORMACION DEL REFERIDO</header>';
		}

		$html.='<table class="table table-bordered" style="background: #ecf8ff; font-size: 11px;" >';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>NOMBRE</strong></td>';
		$html.='<td>'.$info['name'].'</td>';
		$html.='<td><strong>CUMPLEA&Ntilde;OS</strong></td>';
		$html.='<td>'.$_replace->ShowDate($info['birthday']).'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TELEFONO</strong></td>';
		$html.='<td>'.$info['phone'].'</td>';
		$html.='<td><strong>PROVEEDOR</strong></td>';
		$html.='<td>'.$info['p_provider'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>ALTERNATIVO</strong></td>';
		$html.='<td>'.$info['p_alter'].'</td>';
		$html.='<td><strong>PROVEEDOR</strong></td>';
		$html.='<td>'.$info['a_provider'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>OTRO</strong></td>';
		$html.='<td>'.$info['p_other'].'</td>';
		$html.='<td><strong>PROVEEDOR</strong></td>';
		$html.='<td>'.$info['o_provider'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CIUDAD</strong></td>';
		$html.='<td>'.$info['town'].'</td>';
		$html.='<td><strong>PAIS</strong></td>';
		$html.='<td>'.$info['country'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DIRECCI&Oacute;N</strong></td>';
		$html.='<td colspan="5">'.$info['add'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>POSTAL</strong></td>';
		$html.='<td colspan="5">'.$info['add_postal'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$info['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$info['coord_lo'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>SEGURO SOCIAL</strong></td>';
		$html.='<td>'.$info['ss'].'</td>';
		$html.='<td><strong>FECHA EXPIRACI&Oacute;N</strong></td>';
		$html.='<td>'.$info['ss_exp'].'</td>';
		$html.='<td><strong>TIPO</strong></td>';
		$html.='<td>'.$info['ss_type'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>ID</strong></td>';
		$html.='<td>'.$info['id'].'</td>';
		$html.='<td><strong>FECHA EXPIRACI&Oacute;N</strong></td>';
		$html.='<td>'.$info['id_exp'].'</td>';
		$html.='<td><strong>TIPO</strong></td>';
		$html.='<td>'.$info['id_type'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TARJETA DE CREDITO</strong></td>';
		$html.='<td>'.$info['cc'].'</td>';
		$html.='<td><strong>FECHA EXPIRACI&Oacute;N</strong></td>';
		$html.='<td>'.$info['cc_exp'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>NOMBRE DE LA CUENTA</strong></td>';
		$html.='<td>'.$info['ab'].'</td>';
		$html.='<td><strong>NUMERO DE CUENTA</strong></td>';
		$html.='<td>'.$info['ab_num'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>NOMBRE DEL BANCO</strong></td>';
		$html.='<td>'.$info['ab_bank'].'</td>';
		$html.='<td><strong>TIPO DE CUENTA</strong></td>';
		$html.='<td>'.$info['ab_type'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>SCORE</strong></td>';
		$html.='<td>'.$score['score'].'</td>';
		$html.='<td><strong>TRANSACTIONS</strong></td>';
		$html.='<td>'.$score['transaction'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EMAIL</strong></td>';
		$html.='<td>'.$info['email'].'</td>';
		$html.='<td><strong>ZIP</strong></td>';
		$html.='<td>'.$info['zip'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INFORMACI&Oacute;N ADICIONAL</strong></td>';
		$html.='<td colspan="5">'.$info['additional'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

   		return $html;
	
	}
///////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServTvPR($info)
   	{
   		$html = "";  		
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>TV</strong></td>';
		$html.='<td>'.$info['tv'].'</td>';
		$html.='<td><strong>DECODIFICADOR</strong></td>';
		$html.='<td>'.$info['decoder_id'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PAQUETE</strong></td>';
		$html.='<td>'.$info['package'].'</td>';
		$html.='<td><strong>DECODIFICADOR DVR</strong></td>';
		$html.='<td>'.$info['dvr_id'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PROVEEDOR ANTERIOR</strong></td>';
		$html.='<td>'.$info['provider'].'</td>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].' PAYMENT</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>HD</strong></td>';
		$html.='<td>'.$info['ch_hd'].'</td>';
		$html.='<td><strong>DVR</strong></td>';
		$html.='<td>'.$info['ch_dvr'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>HBO</strong></td>';
		$html.='<td>'.$info['ch_hbo'].'</td>';
		$html.='<td><strong>CINEMAX</strong></td>';
		$html.='<td>'.$info['ch_cinemax'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>STARTZ</strong></td>';
		$html.='<td>'.$info['ch_starz'].'</td>';
		$html.='<td><strong>SHOWTIME</strong></td>';
		$html.='<td>'.$info['ch_showtime'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>GIFT ENTERTAINMENT</strong></td>';
		$html.='<td>'.$info['gif_ent'].'</td>';
		$html.='<td><strong>GIFT CHOICE</strong></td>';
		$html.='<td>'.$info['gif_choice'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>GIFT XTRA</strong></td>';
		$html.='<td>'.$info['gif_xtra'].'</td>';
		$html.='<td><strong>PUBLICIDAD AT&T</strong></td>';
		$html.='<td>'.$info['advertising'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;
   
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServSecPR($info)
   	{
   	   	$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>TIENE SISTEMA DE SEGURIDAD PREVIAMENTE</strong></td>';
		$html.='<td>'.$info['sist_previo'].'</td>';
		$html.='<td><strong>EMPRESA DE MONITOREO</strong></td>';
		$html.='<td>'.$info['compania'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CAMARAS</strong></td>';
		$html.='<td>'.$info['camaras'].'</td>';
		$html.='<td><strong>DVR</strong></td>';
		$html.='<td>'.$info['dvr'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PASSWORD DE ALARMA</strong></td>';
		$html.='<td>'.$info['password_alarm'].'</td>';
		$html.='<td><strong>METODO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DESCRIPCI&Oacute;N DE EQUIPOS</strong></td>';
		$html.='<td colspan="4">'.$info['equipment'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EQUIPOS ADICIONALES</strong></td>';
		$html.='<td colspan="4">'.$info['adicional'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACT 1</strong></td>';
		$html.='<td>'.$info['contact_1'].'</td>';
		$html.='<td><strong>DESCRIPCI&Oacute;N</strong></td>';
		$html.='<td>'.$info['contact_1_desc'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACT 2</strong></td>';
		$html.='<td>'.$info['contact_2'].'</td>';
		$html.='<td><strong>DESCRIPCI&Oacute;N</strong></td>';
		$html.='<td>'.$info['contact_2_desc'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACT 3</strong></td>';
		$html.='<td>'.$info['contact_2_desc'].'</td>';
		$html.='<td><strong>DESCRIPCI&Oacute;N</strong></td>';
		$html.='<td>'.$info['contact_3_desc'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;	
   	
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServIntPR($info, $data)
   	{
   		$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['int_res'].'</td>';
		$html.='<td><strong>TELEFONO RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['pho_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET COMERCIAL</strong></td>';
		$html.='<td>'.$info['int_com'].'</td>';
		$html.='<td><strong>TELEFONO COMERCIAL</strong></td>';
		$html.='<td>'.$info['pho_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$data['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$data['coord_lo'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='<td><strong></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServIntVZ($info, $data)
   	{

  		$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['int_res'].'</td>';
		$html.='<td><strong>TELEFONO RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['phone_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET COMERCIAL</strong></td>';
		$html.='<td>'.$info['int_com'].'</td>';
		$html.='<td><strong>TELEFONO COMERCIAL</strong></td>';
		$html.='<td>'.$info['phone_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$data['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$data['coord_lo'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='<td><strong>BANCO</strong></td>';
		$html.='<td>'.$info['bank'].'</td>';
		$html.='</tr>';
		$html.='<td><strong>MONTO</strong></td>';
		$html.='<td>'.$info['amount'].'</td>';
		$html.='<td><strong>TRANSACCION</strong></td>';
		$html.='<td>'.$info['trans_date'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EMAIL</strong></td>';
		$html.='<td colspan="4">'.$info['email'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CAMPO ADICIONAL</strong></td>';
		$html.='<td colspan="4">'.$info['additional'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;
   	
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////// Qualifications HTML ////////////////////////////////

	public static function QualTvHtml($info, $score, $id)
	{

		$html = "";

		$html.='<div class="widget-body">';
		$html.='<input type="text" name="qual_id" id="qual_id" value="'.$id.'" hidden readonly>';
		$html.='<fieldset>';
		$html.='<div class="form-group">';

		if($info['status'] == true)
		{
			$html.='<div class="row">';
			$html.='<header style="text-align: center;">UTLIMO SCORE</header>';
			$html.='<table class="table table-bordered" style="background: #ecf8ff; margin-top: 11px;">';
			$html.='<tbody style="text-align: center;">';
			$html.='<tr>';
			$html.='<td><strong>SCORE</strong></td>';
			$html.='<td>'.$info['score'].'</td>';
			$html.='<td><strong>TRANSACCI&Oacute;N</strong></td>';
			$html.='<td>'.$info['transaction'].'</td>';
			$html.='<td rowspan="2">';
			$html.='<a style="margin-top: 15px;" onclick="CopyScore('.(int)$info['id'].');" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Copy Score"><i class="fa fa-copy"></i></a>';
			$html.='</td>';
			$html.='</tr>';
			$html.='<tr>';
			$html.='<td><strong>PROCESADO POR</strong></td>';
			$html.='<td>'.$info['process'].'</td>';
			$html.='<td><strong>FECHA</strong></td>';
			$html.='<td>'.$info['date'].'</td>';
			$html.='</tr>';
			$html.='</tbody>';
			$html.='</table>';
			$html.='<br/>';
			$html.='</div>';
		}
		$html.='<div class="row" style="text-align: center;">';
		$html.='<div class="col-xs-6" >';
		$html.='<label>SCORE</label>';
		$html.='<select class="form-control" id="qual_score" name="qual_score" required>';

		foreach ($score as $k => $val) 
		{
			$html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';
		}

		$html.='</select>';
		$html.='</div>';
		$html.='<div class="col-xs-6">';
		$html.='<label>TRANSACCION</label>';
		$html.='<input class="form-control"  id="qual_trans" name="qual_trans" maxlength="255" type="text" placeholder="TRANSACCION" required>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</fieldset>';
		$html.='</div>';


		return $html;

	}

	public static function QualSecHtml($info, $score, $id)
	{

		$html = "";

		$html.='<div class="widget-body">';
		$html.='<input type="text" name="qual_id" id="qual_id" value="'.$id.'" hidden readonly>';
		if($info['status'] == true)
		{
		$html.='<fieldset>';
		$html.='<div class="form-group">';

		$html.='<div class="row">';
		$html.='<table class="table table-bordered" style="background: #ecf8ff; margin-top: 10px; font-size: 12px;">';
		$html.='<tbody style="text-align: center;">';

		$html.='<tr>';
		$html.='<td><strong>SCORE</strong></td>';
		$html.='<td>'.$info['score'].'</td>';
		$html.='<td><strong>VALOR</strong></td>';
		$html.='<td>'.$info['value'].'</td>';
		$html.='<td><strong>FICO</strong></td>';
		$html.='<td>'.$info['fico'].'</td>';
		$html.='<td rowspan="2">';
		$html.='<a style="margin-top: 15px;" onclick="CopyScore('.(int)$info['id'].');" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Copy Score"><i class="fa fa-copy"></i></a>';
		$html.='</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PROCESADO POR</strong></td>';
		$html.='<td colspan="2">'.$info['process'].'</td>';
		$html.='<td><strong>FECHA</strong></td>';
		$html.='<td colspan="2">'.$info['date'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';
		$html.='</fieldset>';
		}

		$html.='<fieldset>';
		$html.='<div class="form-group">';
		$html.='<div class="row" style="text-align: center;">';
		$html.='<div class="col-xs-4" >';
		$html.='<label>SCORE</label>';
		$html.='<select class="form-control" id="qual_score" name="qual_score" required>';

		foreach ($score as $k => $val) 
		{
			$html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';
		}
		$html.='</select>';
		$html.='</div>';
		$html.='<div class="col-xs-4">';
		$html.='<label>VALOR</label>';
		$html.='<input class="form-control"  id="qual_value" name="qual_value" maxlength="255" type="text" placeholder="VALOR DE SCORE" required>';
		$html.='</div>';
		$html.='<div class="col-xs-4">';
		$html.='<label>FICO</label>';
		$html.='<input class="form-control"  id="qual_fico" name="qual_fico" maxlength="255" type="text" placeholder="SCORE FICO" required>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</fieldset>';
		$html.='</div>';

		return $html;

	}

	public static function QualIntHtml($info, $score, $id)
	{

		$html = "";

		$html.='<div class="widget-body">';
		$html.='<input type="text" name="qual_id" id="qual_id" value="'.$id.'" hidden readonly>';
		if($info['status'] == true)
		{
		$html.='<fieldset>';
		$html.='<div class="form-group">';
		$html.='<div class="row">';
		$html.='<table class="table table-bordered" style="background: #ecf8ff; margin-top: 10px; font-size: 12px;">';
		$html.='<tbody style="text-align: center;">';
		$html.='<tr>';
		$html.='<td><strong>SCORE</strong></td>';
		$html.='<td>'.$info['score'].'</td>';
		$html.='<td><strong>VALOR</strong></td>';
		$html.='<td>'.$info['value'].'</td>';
		$html.='<td rowspan="2">';
		$html.='<a style="margin-top: 15px;" onclick="CopyScore('.(int)$info['id'].');" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Copy Score"><i class="fa fa-copy"></i></a>';
		$html.='</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PROCESADO POR</strong></td>';
		$html.='<td>'.$info['process'].'</td>';
		$html.='<td><strong>FECHA</strong></td>';
		$html.='<td>'.$info['date'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';
		$html.='</fieldset>';
		}

		$html.='<fieldset>';
		$html.='<div class="form-group">';
		$html.='<div class="row">';
		$html.='<div class="col-xs-6" >';
		$html.='<label>SCORE</label>';
		$html.='<select class="form-control" id="qual_score" name="qual_score" required>';
		foreach ($score as $k => $val) 
		{
		$html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';
		}
		$html.='</select>';
		$html.='</div>';
		$html.='<div class="col-xs-6">';
		$html.='<label>VALOR</label>';
		$html.='<input class="form-control"  id="qual_value" name="qual_value" maxlength="255" type="text" placeholder="VALOR DE SCORE" readonly>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</fieldset>';
		$html.='</div>';


		return $html;
	
	}

	public static function ViewCancel($service, $id)
	{
		$serv = ApprovalsRespCancel::GetObjectionsByService($service);
		
		$html = '';
		$html.='<div class="widget-body" >';
		$html.='<input type="text" name="cancel_id" id="cancel_id" value="'.$id.'" hidden readonly>';
		if($serv <> false)
		{
			$html.='<fieldset style="background: aliceblue;">';
			$html.='<section>';
			$html.='<label class="label"><h1 style="text-align: center;">RAZON DE CANCELACION</h1></label>';
			$html.='<label class="select select-multiple">';
			$html.='<select multiple="" name="cancel_sel[]" id="cancel_sel" style="height: 250px;">';

			foreach ($serv as $k => $val) 
			{
				$html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';
			}
			$html.='</select> </label>';
			$html.='<div class="note">';
			$html.='<strong>Nota:</strong> Selecciona una o mas Opciones..';
			$html.='</div>';
			$html.='</section>';
			$html.='</fieldset>';

		}				

		$html.='<fieldset style="background: aliceblue;">';
		$html.='<section>';
		$html.='<label class="label">Nota</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="3" name="cancel_note" id="cancel_note" class="custom-scroll"></textarea> ';
		$html.='</label>';
		$html.='</section>';
		$html.='</fieldset>	';
		$html.='</div>';

		return $html;

	}

/////////////////////////////////// Approvals Save ///////////////////////////////////

	public static function SaveAprovals($ticket, $info, $date)
	{
		$service 	=	substr($ticket, 0,2);
        switch ($service) 
        {
        	case 'TV':
        		$serv = 2;
        		break;       	
        	case 'SE':
        		$serv = 3;
        		break;
        	case 'IN':
        		$serv = 4;
        		break;
        	case 'IV':
        		$serv = 6;
        		break;
        }

		if($info['sup'] == 0)
		{
			$user =	[
				'ope'	=>	$info['operator'],
				'dep'	=>	Departament::GetDepById(User::GetUserById($info['operator'])['departament'])['id']
			];
		}else{
			$user =	[
				'ope'	=>	$info['sup_list'],
				'dep'	=>	Departament::GetDepById(User::GetUserById($info['sup_list'])['departament'])['id']
			];
		}

		$query 	=	'INSERT INTO cp_appro_serv(ticket, client_id, service_id, score_id, score, transactions, value, fico, status_id, view, cancel, created_by, created_dep_id, created_at, finish_by, finish_dep_id, finish_at) VALUES ("'.$ticket.'", "'.$info['client'].'", "'.$serv.'", "", "", "", "", "", "1" ,"0", "1", "'.$user['ope'].'", "'.$user['dep'].'", "'.$date.'", "1", "1", "")';
	
		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	
	}


	public static function GetLeadHtml($info)
	{
		$html = "";

		$html.='<table class="head-wrap" style="background: #ecf8ff">';							
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>TICKET</strong></td>';
		$html.='<td>'.strtoupper($info['ticket']).'</td>';
		$html.='<td><strong>SCORE</strong></td>';
		$html.='<td>'.strtoupper($info['score']).'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>APROBADO POR</strong></td>';
		$html.='<td>'.strtoupper($info['operator']).'</td>';
		$html.='<td><strong>FECHA</strong></td>';
		$html.='<td>'.strtoupper($info['date']).'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';

		return $html;
	}

	public static function ClosuresTable($info)
	{
		$html = "";
		$html.='<p style="text-align: center;"> <span>TOP 5 APROBACIONES</span></p>';
		$html.='<div class="widget-body">';
		$html.='<div class="table-responsive">';
		$html.='<table class="table table-bordered table-striped" border="1" style="border-collapse: collapse;">';
		$html.='<tbody>';
		$html.='<td>#</td>';
		$html.='<td></td>';
		$html.='<td>OPERADOR</td>';
		$html.='<td>CANT</td>';
		$html.='</tr>';
		foreach ($info as $i => $inf) 
		{
			$html.='<tr>';
			$html.='<td>'.($i+1).'</td>';
			if($i == 0)
			{
			$html.='<td><img src="/assets/img/approvals/SC'.($i+1).'.jpg" style="width: 10px;"></td>';
			}elseif ($i == 1) {
			$html.='<td><img src="/assets/img/approvals/SC'.($i+1).'.jpg" style="width: 10px;"></td>';
			}elseif ($i == 2) {
			$html.='<td><img src="/assets/img/approvals/SC'.($i+1).'.jpg" style="width: 10px;"></td>';
			}else{
			$html.='<td></td>';
			}
			$html.='<td>'.$inf['username'].'</td>';
			$html.='<td>'.$inf['T'].'</td>';
		}
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';
      return $html;

	}

	public static function ClosuresNewTable($info)
	{
		$html="";
        $html.='<div class="table-responsive">';
          $html.='<table class="table table-bordered" border="1" style="border-collapse: collapse; font-size: 10px; text-align: center; height: 0px">';
            $html.='<tbody>';
              $html.='<tr>';
                $html.='<td>OPERADOR</td>';
                $html.='<td>SOMETIDAS</td>';
                $html.='<td>APROBADAS</td>';
                $html.='<td>COORDINADAS</td>';
              $html.='</tr>';

              if($info <> false)
              {
              	foreach ($info as $k => $val) 
              	{
	              	$html.='<tr>';
		                $html.='<td>'.$val['username'].'</td>';
		                $html.='<td>'.$val['sometidas'].'</td>';
		                $html.='<td>'.$val['aprobadas'].'</td>';
		                $html.='<td>'.$val['coordinadas'].'</td>';
		            $html.='</tr>';
              	}
              }else{
              	$html.='<tr>';
	            	$html.='<td colspan="4">SIN INFORMACION PARA MOSTRAR</td>';
	            $html.='</tr>';
              }

            $html.='</tbody>';
          $html.='</table>';
        $html.='</div>';

        return $html;
	
	}

	public static function ClosuresItemes($info)
	{
		$html = '';
		$html.='<table id="TableClosuresItems" class="table table-bordered" border="1" style="font-size: 10px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th><strong>#</strong></th>';
		$html.='<th><strong>ID</strong></th>';
		$html.='<th><strong>Ticket</strong></th>';
		$html.='<th><strong>Cliente</strong></th>';
		$html.='<th><strong>Fecha</strong></th>';
		$html.='<th><strong>Servicio</strong></th>';
		$html.='<th><strong>Vendedor</strong></th>';
		$html.='<th><strong>Departamento</strong></th>';
		$html.='<th><strong>Status</strong></th>';
		$html.='<th><strong>Score</strong></th>';
		$html.='<th><strong>Procesado</strong></th>';
		$html.='<th><strong>Fecha</strong></th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		$html.=''.$info.'';

		$html.='</tbody>';
		$html.='</table>';
		return $html;
	}

/////////////////////////////////// Approvals Save ///////////////////////////////////

}