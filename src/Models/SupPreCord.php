<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;

class SupPreCord extends Model 
{


	static $_table 		= 'cp_coord_pre_sup';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'name', 'status_id', 'created_by', 'created_at');

	public static function GetSupPreCord()
	{
		$_replace 	=   new Config();

		$cWeek		=	$_replace->CurrentWeek();

		$query 	= 	'SELECT id, ticket, client_id, name, status_id, created_by, created_at FROM cp_coord_pre_sup WHERE created_at BETWEEN "'.$cWeek['yI'].'-'.$cWeek['mI'].'-'.$cWeek['dI'].' 00:00:00" AND "'.$cWeek['yF'].'-'.$cWeek['mF'].'-'.$cWeek['dF'].' 23:59:59" ORDER BY id ASC';

		$cit 	=	DBSmart::DBQueryAll($query);
		
		if($cit <> false)
		{
			foreach ($cit as $k => $val) 
			{
				$city[$k] = array(
					'id' 	 	=> 	$val['id'], 
					'ticket'	=>	$val['ticket'],
					'client_id'	=>	$val['client_id'],
					'name' 	 	=>	$val['name'],
					'status' 	=> 	($val['status_id'] == "1") ? "PENDIENTE" : "PROCESADO",
					'operator'	=>	User::GetUserById($val['created_by'])['username'],
					'created'	=>	$_replace->ShowDate($val['created_at'])
				);
			}
	        
	        return $city;

		}else{ return false; }
	
	}

	public static function GetSupPreCordById($id)
	{
		$_replace 	=   new Config();

		$query 	= 	'SELECT id, ticket, client_id, name, status_id, created_by, created_at FROM cp_coord_pre_sup WHERE id = "'.$id.'" ORDER BY id ASC';
		$cit 	=	DBSmart::DBQuery($query);
		
		if($cit <> false)
		{
			$city = [
				'id' 	 	=> 	$cit['id'], 
				'ticket'	=>	$cit['ticket'],
				'client_id'	=>	$cit['client_id'],
				'name' 	 	=>	$cit['name'],
				'status' 	=> 	($cit['status_id'] == "1") ? "PENDIENTE" : "PROCESADO",
				'operator'	=>	User::GetUserById($cit['created_by'])['username'],
				'created'	=>	$_replace->ShowDate($cit['created_at'])
			];

	        return $city;

		}else{ return false; }
	
	}

	public static function GetSupPreCordByTicket($ticket)
	{
		$_replace 	=   new Config();

		$query 	= 	'SELECT id, ticket, client_id, name, status_id, created_by, created_at FROM cp_coord_pre_sup WHERE ticket = "'.$ticket.'" ORDER BY id ASC';
		$cit 	=	DBSmart::DBQuery($query);
		
		if($cit <> false)
		{
			$city = [
				'id' 	 	=> 	$cit['id'], 
				'ticket'	=>	$cit['ticket'],
				'client_id'	=>	$cit['client_id'],
				'name' 	 	=>	$cit['name'],
				'status' 	=> 	($cit['status_id'] == "1") ? "PENDIENTE" : "PROCESADO",
				'operator'	=>	User::GetUserById($cit['created_by'])['username'],
				'created'	=>	$_replace->ShowDate($cit['created_at'])
			];

	        return $city;

		}else{ return false; }
	
	}

	public static function DetecTicket($in, $ticket)
	{
		switch ($in) {
			case 'TV':
				return (ServiceTvPr::GetServiceByTicket($ticket) <> false) ? 'TVPR' : false;
				break;
			case 'SE':			
				return (ServiceSecPr::GetServiceByTicket($ticket) <> false) ? 'SECPR'  : false;
				break;
			case 'IN':
				return (ServiceIntPr::GetServiceByTicket($ticket) <> false) ? 'INTPR' : false;;
				break;
			case 'IV':
				return (ServiceIntVZ::GetServiceByTicket($ticket) <> false) ? 'IVE' : false;;
				break;
		}
	
	}

	public static function GetInfo($type, $ticket)
	{

		$cypher		=	New UnCypher();

        $table 		=	$referred 	=		'';
		
		if ($type == 'INTPR') 
        {
        	$query = 	'SELECT ticket, client_id, (SELECT name FROM data_internet 	WHERE id = int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = pho_res_id) AS pho_res, (SELECT name FROM data_internet 	WHERE id = int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = payment_id) AS payment FROM cp_service_pr_int WHERE ticket = "'.$ticket.'"';
        	
        	$table = 'cp_service_pr_int';
        }
        elseif($type == 'IVE')
        {
        	$query = 'SELECT ticket, client_id, (SELECT name FROM data_internet WHERE id = int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = pho_res_id) AS phone_res, (SELECT name FROM data_internet WHERE id = int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = pho_com_id) AS phone_com, (SELECT name FROM data_payment WHERE id = payment_id) AS payment, (SELECT name FROM data_bank WHERE id = bank_id) AS bank, amount, trans_date, email, additional FROM cp_service_vzla_int WHERE ticket = "'.$ticket.'"';

        	$table = 'cp_service_vzla_int';

        }

        $service    =   DBSmart::DBQuery($query);

        $query 		=	'SELECT t2.client_id, t2.name, t2.birthday, t2.coor_lati AS coord_la, t2.coor_long AS coord_lo, t2.referred_id, t2.phone_main AS phone,  (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS p_provider, t2.phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = t2.phone_alt_provider) AS a_provider, t2.phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = t2.phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = t2.town_id) AS town, (SELECT code FROM data_zips WHERE id = t2.zip_id) AS zip, (SELECT name FROM data_country WHERE id = t2.country_id) AS country, (SELECT name FROM data_ceiling WHERE id = t2.h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = t2.h_level_id) AS level, t2.ident_id, t2.ident_exp, t2.ss_id, t2.ss_exp, t2.add_main, t2.add_postal, t2.email_main, t2.additional FROM '.$table.' AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

        $client     =   DBSmart::DBQuery($query);

        if($client['referred_id'] <> "0")
        {
	        $query 		= 	'SELECT client_id, name, birthday, coor_lati AS coord_la, coor_long AS coord_lo, referred_id, phone_main AS phone, (SELECT name FROM data_phone_provider WHERE id = phone_main_provider) AS p_provider, phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = phone_alt_provider) AS a_provider, phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT code FROM data_zips WHERE id = zip_id) AS zip, (SELECT name FROM data_country WHERE id = country_id) AS country, (SELECT name FROM data_ceiling WHERE id = h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = h_level_id) AS level, ident_id, ident_exp, ss_id, ss_exp, add_main, add_postal, email_main, additional FROM cp_leads WHERE client_id = "'.$client['referred_id'].'"';

	        $referred   =   DBSmart::DBQuery($query);

        }else{ $referred == false; }


        return ['client' => $client, 'referred' => $referred, 'service' => $service];

   	}

   	public static function ObtData($client, $ticket)
   	{
		$cypher		=	New UnCypher();

        if($client)
        {

        	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'ID');
			$ide 	=	$info['infoCard'];

	    	if($ide <> false) 
	    		{ $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = IDType::GetIdTypeById($info['info']['type_d'])['name'];}
	    		// { $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = "";}
	    	else 
	    		{ $ident = ""; $ident_exp = ""; $ident_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'SS');
	    	$sss 	=	$info['infoCard'];

	    	if($sss <> false) 
	    		{ $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = SSType::GetSSTypeById($info['info']['type_d'])['name'];}
	    		// { $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = "";}
	    	else 
	    		{ $ss = ""; $ss_exp = ""; $ss_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'CC');
	    	$cards 	=	$info['infoCard'];

	    	if($cards <> false) 
	    		{ $card = $cards['card']; $card_exp = $cards['month']."/".$cards['year']; }
	    	else 
	    		{ $card = ""; $card_exp = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'AB');

			$ABS 	=	$info['infoCard'];

	    	if($ABS <> false) 
	    		{ 
					$ab_nam = $ABS['name']; $ab_num = $ABS['account']; $ab_ban = Bank::GetBankById($ABS['bank'])['name']; $ab_typ = $ABS['type'];
	    		}
	    	else 
	    		{ $ab_nam = ""; $ab_num = ""; $ab_ban = ""; $ab_typ = ""; }

	    	$cData = 	[
	    		'client'		=>	($client['client_id'] <> '')	? $client['client_id'] 	: '',
	    		'ticket'		=>	($ticket <> '')					? $ticket 				: '',
	    		'name'			=>	($client['name'] <> '')			? $client['name'] 		: '',
	    		'birthday'		=>	($client['birthday'] <> '')		? $client['birthday'] 	: '',
	    		'coord_la'		=>	($client['coord_la'] <> '')		? $client['coord_la'] 	: '',
	    		'coord_lo'		=>	($client['coord_lo'] <> '')		? $client['coord_lo'] 	: '',
	    		'phone'			=>	($client['phone'] <> '')		? $client['phone'] 		: '',
	    		'p_provider'	=>	($client['p_provider'] <> '')	? $client['p_provider'] : '',
	    		'p_alter'		=>	($client['p_alter'] <> '')		? $client['p_alter'] 	: '',
	    		'a_provider'	=>	($client['a_provider'] <> '')	? $client['a_provider'] : '',
	    		'p_other'		=>	($client['p_other'] <> '')		? $client['p_other'] 	: '',
	    		'o_provider'	=>	($client['o_provider'] <> '')	? $client['o_provider'] : '',
	    		'town'			=>	($client['town'] <> '')			? $client['town'] 		: '',
	    		'country'		=>	($client['country'] <> '')		? $client['country'] 	: '',
				'ceiling'		=>	($client['ceiling'] <> '')		? $client['ceiling'] 	: '',
	    		'level'			=>	($client['level'] <> '')		? $client['level'] 		: '',
	    		'ss'			=>	($ss <> '') 		?	$ss 		: '',
	    		'ss_exp'		=>	($ss_exp <> '') 	? 	$ss_exp 	: '',
	    		'ss_type'		=>	($ss_type <> '') 				? 	$ss_type 			: '',
	    		'id'			=>	($ident <> '') 		? 	$ident 		: '',
	    		'id_exp'		=>	($ident_exp <> '')	? 	$ident_exp 	: '',
				'id_type'		=>	($ident_type <> '')				? 	$ident_type 		: '',
	    		'cc'			=>	($card <> '') 		? 	$card 		: '',
	    		'cc_exp'		=>	($card_exp <> '')	?   $card_exp 	: '',
	    		'ab'			=>	($ab_nam <> '') 	? 	$ab_nam 	: '',
	    		'ab_num'		=>	($ab_num <> '') 	? 	$ab_num		: '',
	    		'ab_bank'		=>	($ab_ban <> '') 	? 	$ab_ban 	: '',
	    		'ab_type'		=>	($ab_typ <> '') 	? 	$ab_typ 	: '',
	    		'add'			=>	($client['add_main'] <> '')		? $client['add_main']	: '',
	    		'add_postal'	=>	($client['add_postal'] <> '')	? $client['add_postal']	: '',
	    		'email'			=>	($client['email_main'] <> '')	? $client['email_main']	: '',
	    		'additional'	=>	($client['additional'] <> '')	? $client['additional']	: ''
	    	];

        }else{

		    $cData = 	[
	    		'client' => '', 'ticket' => '', 'name' => '', 'phone' => '', 'p_provider' => '', 'p_alter' => '', 'a_provider' => '', 'p_other' => '', 'o_provider' => '', 'town' => '', 'country'=> '', 'ss' => '', 'ss_exp' => '', 'id' => '', 'id_exp' => '', 'cc' => '', 'cc_exp' => '', 'ab' => '', 'ab_num' => '', 'ab_bank' => '', 'ab_type' => '', 'add' => '', 'add_postal' => '', 'email'=> ''];        	
        }
        
    	return ['status'=> ($client) ? true : false, 'data' => $cData];
   	
   	}

   	public static function LoadByDate($info, $dates)
   	{
   		$_replace   =   new Config();
   		
		if($info == "0")
		{
			$query 	= 	'SELECT id, ticket, client_id, name, status_id, created_by, created_at FROM cp_coord_pre_sup WHERE created_at LIKE "%'.$dates['dateEnd'].'%" ORDER BY id ASC';
			$cit 	=	DBSmart::DBQueryAll($query);
			
			if($cit <> false)
			{
				foreach ($cit as $k => $val) 
				{
					$city[$k] = array(
						'id' 	 	=> 	$val['id'], 
						'ticket'	=>	$val['ticket'],
						'client_id'	=>	$val['client_id'],
						'name' 	 	=>	$val['name'],
						'status' 	=> 	($val['status_id'] == "1") ? "PENDIENTE" : "PROCESADO",
						'operator'	=>	User::GetUserById($val['created_by'])['username'],
						'created'	=>	$_replace->ShowDate($val['created_at'])
					);
				}
		        
		        return $city;

			}else{ return false; }

		}else{
			
			$query 	= 	'SELECT id, ticket, client_id, name, status_id, created_by, created_at FROM cp_coord_pre_sup WHERE created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY id ASC';
			$cit 	=	DBSmart::DBQueryAll($query);
			
			if($cit <> false)
			{
				foreach ($cit as $k => $val) 
				{
					$city[$k] = array(
						'id' 	 	=> 	$val['id'], 
						'ticket'	=>	$val['ticket'],
						'client_id'	=>	$val['client_id'],
						'name' 	 	=>	$val['name'],
						'status' 	=> 	($val['status_id'] == "1") ? "PENDIENTE" : "PROCESADO",
						'operator'	=>	User::GetUserById($val['created_by'])['username'],
						'created'	=>	$_replace->ShowDate($val['created_at'])
					);
				}
		        
		        return $city;

			}else{ return false; }
		}
   	}

   	public static  function UpdatePreCord($id)
   	{
   		$query 	=	'UPDATE cp_coord_pre_sup SET status_id = "2" WHERE id = "'.$id.'"';
   		return 	DBSmart::DataExecute($query);
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function GetPreCoordHTML($iData)
	{
		$html = '';		

		$html.='<table id="dt_precord" class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th style="text-align: center;">ID</th>';
		$html.='<th style="text-align: center;">Ticket</th>';
		$html.='<th style="text-align: center;">Cliente</th>';
		$html.='<th style="text-align: center;">Nombre</th>';
		$html.='<th style="text-align: center;">Status</th>';
		$html.='<th style="text-align: center;">Fecha</th>';
		$html.='<th style="text-align: center;">Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		if($iData <> false)
		{
			foreach ($iData as $k => $val) 
			{
				$status 	=	($val['status'] == "PENDIENTE") ? '<span class="label label-warning">PENDIENTE</span>' : '<span class="label label-success">PROCESADO</span>';

				$html.='<tr>';
				$html.='<td style="text-align: center;">'.$val['id'].'</td>';
				$html.='<td style="text-align: center;">'.$val['ticket'].'</td>';
				$html.='<td style="text-align: center;">'.$val['client_id'].'</td>';
				$html.='<td style="text-align: center;">'.$val['name'].'</td>';
				$html.='<td style="text-align: center;">'.$status.'</td>';
				$html.='<td style="text-align: center;">'.$val['created'].'</td>';
				$html.='<td style="text-align: center;">';
				$html.='<div class="btn-group">';
				$html.='<button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
				$html.='<ul class="dropdown-menu">';
				$html.='<li>';
				$html.='<a onclick="PreCordView('.$val['id'].');">Ver</a>';
				$html.='</li>';
				$html.='<li>';
				$html.='<a onclick="PreCordProc('.$val['id'].');">Procesar</a>';
				$html.='</li>';
				$html.='</ul>';
				$html.='</div>';
				$html.='</td>';
				$html.='</tr>';
			}
		}else{

		}
		
		$html.='</tbody>';
		$html.='</table>';

		return $html;
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServIntPR($info, $data)
   	{
   		$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['int_res'].'</td>';
		$html.='<td><strong>TELEFONO RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['pho_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET COMERCIAL</strong></td>';
		$html.='<td>'.$info['int_com'].'</td>';
		$html.='<td><strong>TELEFONO COMERCIAL</strong></td>';
		$html.='<td>'.$info['pho_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$data['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$data['coord_lo'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='<td><strong></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServIntVZ($info, $data)
   	{

  		$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['int_res'].'</td>';
		$html.='<td><strong>TELEFONO RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['phone_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET COMERCIAL</strong></td>';
		$html.='<td>'.$info['int_com'].'</td>';
		$html.='<td><strong>TELEFONO COMERCIAL</strong></td>';
		$html.='<td>'.$info['phone_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$data['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$data['coord_lo'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='<td><strong>BANCO</strong></td>';
		$html.='<td>'.$info['bank'].'</td>';
		$html.='</tr>';
		$html.='<td><strong>MONTO</strong></td>';
		$html.='<td>'.$info['amount'].'</td>';
		$html.='<td><strong>TRANSACCION</strong></td>';
		$html.='<td>'.$info['trans_date'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EMAIL</strong></td>';
		$html.='<td colspan="4">'.$info['email'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CAMPO ADICIONAL</strong></td>';
		$html.='<td colspan="4">'.$info['additional'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;
   	
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

}