<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Creative;
use App\Models\User;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Creative extends Model {

	static $_table 		= 'mkt_img_art';

	Public $_fillable 	= array('name', 'img', 'type', 'country_id', 'status_id', 'user_id', 'update_id', 'created_at');

	public static function GetImgs()
	{
		$_replace 	= 	new Config();

		$query	=	'SELECT * FROM mkt_img_art ORDER BY id DESC';
		$cre 	=	DBSmart::DBQueryAll($query);
		
		$creati = array();

		if($cre == true){
			foreach ($cre as $k => $val) 
			{
				$creati[$k] = array(
					'id' 	 		=> $val['id'],
					'type'	 		=> $val['type'],
					'base'	 		=> $val['img'],
					'name' 	 		=> $val['name'], 
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id' 	=> $val['country_id'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id' 	=> $val['status_id'],
					'users'  		=> User::GetUserById($val['user_id'])['username'],
					'create' 		=> $_replace->ShowDateAll($val['created_at'])
				);
			}

	        return $creati;
		}else{	return false;	}
	}

	public static function GetImgId($id)
	{
		
		$query 	= 	'SELECT img FROM mkt_img_art WHERE id = "'.$id.'"';
		$cre 	=	DBSmart::DBQuery($query);

		if($cre <> false)
		{
			return array('base'	 => $cre['img']);

		}else{	return false; }
	}

	public static function SaveImg($info, $file)
	{
		$_replace  = new Config();
		$date 	= 	date('Y-m-d H:i:s', time());
		$name 	=	strtoupper($_replace->deleteTilde($info['name']));

		$query 	=	'INSERT INTO mkt_img_art(name, img, type, size, country_id, status_id, user_id, update_id, created_at) VALUES ("'.$name.'", "'.$file['dir'].'", "'.$file['type'].'", "'.$file['size'].'", "'.$info['country'].'", "'.$info['status'].'", "'.$info['user'].'", "'.$info['user'].'", "'.$date.'")';

		$camp 	=	DBSmart::DataExecute($query);

		return ($camp <> false ) ? true : false;
	}

	public static function EditSave($info)
	{
		$_replace  = new Config();

		$date 	= 	date('Y-m-d H:i:s', time());
		$name 	=	strtoupper($_replace->deleteTilde($info['name']));

		$query 	=	'UPDATE mkt_img_art SET name="'.$name.'", country_id="'.$info['country'].'", status_id="'.$info['status'].'", update_id="'.$info['update'].'" WHERE id = "'.$info['id'].'"';

		$camp 	=	DBSmart::DataExecute($query);

		return ($camp <> false ) ? true : false;
	}

	public static function GetImgIds($id)
	{
	
		$query 	= 	'SELECT * FROM mkt_img_art WHERE id = "'.$id.'"';
		$cre 	=	DBSmart::DBQuery($query);
		
		if($cre <> false)
		{
			return array(
				'id'           => $cre['id'],
				'name'         => $cre['name'],
				'country'      => $cre['country_id'],
				'status'       => $cre['status_id']
			);

		}else{	return false; }
	}

}



