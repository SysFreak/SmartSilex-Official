<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Level;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Level extends Model {

	static $_table 		= 'data_levels';

	Public $_fillable 	= array('name', 'status_id', 'country_id');

	public static function GetLevel()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_levels ORDER BY id ASC';
		$lev 	=	DBSmart::DBQueryAll($query);

		$Level = array();

		if($lev <> false)
		{
			foreach ($lev as $k => $val) 
			{
				$Level[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $Level;
		}else{ 	return false;	}
	}

	public static function GetLevelById($id)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_levels WHERE id = "'.$id.'" ORDER BY id ASC';
		$lev 	=	DBSmart::DBQuery($query);

		if($lev <> false)
		{
			return array(
				'id' => $lev['id'], 'name' => $lev['name'], 'status' => $lev['status_id'], 'country' => $lev['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetLevelByStatus()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_levels WHERE status_id = "1" ORDER BY id ASC';
		$lev 	=	DBSmart::DBQuery($query);

		if($lev <> false)
		{
			return array(
				'id' => $lev['id'], 'name' => $lev['name'], 'status' => $lev['status_id'], 'country' => $lev['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetLevelByName($info)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_levels WHERE name = "'.$info.'"';
		$lev 	=	DBSmart::DBQuery($query);

		if($lev <> false)
		{
			return array(
				'id' => $lev['id'], 'name' => $lev['name'], 'status' => $lev['status_id'], 'country' => $lev['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetLevelByCountryID($id)
	{

		$query 	= 	'SELECT id, name FROM data_levels WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$to 	=	DBSmart::DBQueryAll($query);
		return ($to <> false) ? $to : false;
	}

	public static function GetLevelByCountry($id)
	{

		$query 	= 	'SELECT id, name FROM data_levels WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$lev 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($lev)
		{
			foreach ($lev as $k => $val) 
			{ $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';}

			return $html;

		}else{ return $html; }
	}

	public static function SaveLevel($info)
	{

		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_lev']));

		if($info['type_lev'] == 'new')
		{
			$query 	= 	'INSERT INTO data_levels(name, status_id, country_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_lev'].'", "'.$info['country_id_lev'].'", "'.$date.'")';

			$lev 	=	DBSmart::DataExecute($query);

			return ($lev <> false ) ? true : false;

		}
		elseif ($info['type_lev'] == 'edit') 
		{
			$query 	=	'UPDATE data_levels SET name="'.$name.'", status_id="'.$info['status_id_lev'].'", country_id="'.$info['country_id_lev'].'" WHERE id = "'.$info['id_lev'].'"';

			$lev 	=	DBSmart::DataExecute($query);

			return ($lev <> false) ? true : false;
		}
	}

}