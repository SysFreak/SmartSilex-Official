<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Referred;
use App\Models\ReferredTmp;
use App\Models\ReferredData;
use App\Models\Country;
use App\Models\User;
use App\Models\CypherData;

use App\Lib\Config;
use App\Lib\DBSmart;

class ReferredTmp extends Model {

	static $_table 		= 'cp_referred_tmp';

	Public $_fillable 	= array('id', 'name', 'birthday', 'phone', 'client_id', 'referred_id', 'type_id', 'type', 'user_id', 'created_at');

	public static function SaveReferred($info)
	{
		$date 		= 	date('Y-m-d h:i:s', time());

		$_replace  	= 	new Config();

		$name 		=	strtoupper($_replace->deleteTilde($info['name']));

		$query 		=	'INSERT INTO cp_referred_tmp(name, birthday, phone, client_id, referred_id, type_id, type, user_id, created_at) VALUES ("'.$name.'", "'.$info['birthday'].'", "'.$info['phone'].'", "'.$info['client'].'", "'.$info['referred'].'", "'.$info['type'].'", "new", "'.$info['user'].'", "'.$date.'") ';

		$serv  		=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	}

	public static function UpdateReferred($info, $user)
	{
		$date 	= date('Y-m-d H:i:s', time());

		$ref 	=	Referred::GetRefId($info['id2_ref']);

		if($ref <> false)
		{
			$query 	=	'INSERT INTO cp_referred_tmp(name, birthday, phone, client_id, referred_id, type_id, type, user_id, created_at) VALUES ("'.$ref['name'].'", "'.$ref['birthday'].'", "'.$ref['phone'].'", "'.$ref['client'].'", "'.$ref['ref_id'].'", "'.$info['type_id_ref2'].'", "update", "'.$user.'", "'.$date.'")';

			$serv  	=	DBSmart::DataExecute($query);
			
			return ($serv <> false) ? true : false;

		}else{	return false;	}
	}
}