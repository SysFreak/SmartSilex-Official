<?php
namespace App\Models;

use Model;

use App\Lib\DBMikroVE;
use App\Lib\DBSmart;
use App\Lib\ApiMWVz;
use App\Lib\Config;
use PDO;

class Transactions extends Model 
{
    public static function Load()
    {
        return [
            'type'      =>  DBSmart::DBQueryAll('SELECT id, name    FROM data_payments_type     WHERE status_id = "1" ORDER BY id ASC'),
            'form'      =>  DBSmart::DBQueryAll('SELECT id, name    FROM data_payments_form     WHERE status_id = "1" ORDER BY id ASC'),
            'office'    =>  DBSmart::DBQueryAll('SELECT id, name    FROM data_payments_office   WHERE status_id = "1" ORDER BY id ASC'),
            'box'       =>  DBSmart::DBQueryAll('SELECT id, name    FROM data_payments_box      WHERE status_id = "1" ORDER BY id ASC'),
            'bank'      =>  DBSmart::DBQueryAll('SELECT id, name    FROM data_payments_banks    WHERE status_id = "1" ORDER BY id ASC'),
            'items'     =>  DBSmart::DBQueryAll('SELECT id, content FROM data_mw_fact_items     WHERE status_id = "1" ORDER BY id ASC'),
        ];
    
    }

	public static function HtmlSearch($data)
    {
        $html 	=	'';
        $stado  =	'';

		$html = '';
		$html.='<table id="TableTransacctionHtml" class="table table-bordered" border="1" style="font-size: 12px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>Id</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Estado</th>';
		$html.='<th>Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

        if($data)
        {
            foreach ($data as $d => $dat) 
            {
                switch ( strtoupper($dat['estado']) ) 
                {
                    case 'ACTIVO':      $stado  =  '<span class="label label-success">'.$dat['estado'].'</span>';break;
                    case 'SUSPENDIDO':  $stado  =  '<span class="label label-warning">'.$dat['estado'].'</span>';break;
                    case 'RETIRADO':    $stado  =  '<span class="label label-danger">'.$dat['estado'].'</span>';break;
                    default:            $stado  =  '<span class="label label-success">'.$dat['estado'].'</span>';break;
                }
                $html.='<tr>';
                $html.='<td style="text-align: center; width:10%;">'.$dat['mw'].'</td>';
                $html.='<td style="text-align: center;">'.strtoupper($dat['name']).'</td>';
                $html.='<td style="text-align: center; width:10%;">'.$stado.'</td>';
                $html.='<td style="text-align: center; width:10%;"><a class="btn btn-info btn-xs" onclick="CliSel('.(int)$dat['mw'].');">+</a></td>';
                $html.='</tr>';
            }
        
        }else{
	        $html.='<tr>';
	        $html.='</tr>';

        }

		$html.='</tbody>';
		$html.='</table>';
		return $html;

    }

    public static function HtmlStatics($data)
    {
        $html   =   '';
        $stado  =   '';

        $html = '';
        $html.='<table id="TableStaticsHtml" class="table table-bordered" border="1" style="font-size: 12px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th>Cliente</th>';
        $html.='<th>Factura</th>';
        $html.='<th>Tipo</th>';
        $html.='<th>Forma</th>';
        $html.='<th>Status</th>';
        $html.='<th>Total</th>';
        $html.='<th>Fecha</th>';
        $html.='<th>Acci&oacute;n</th>';

        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';

        if($data)
        {
            foreach ($data as $d => $dat) 
            {
                $html.='<tr>';
                    $html.='<td style="text-align: center; font-size:10px;">'.$dat['name'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['bills'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['type'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['form'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['status'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['total_fp'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['created_at'].'</td>';
                    $html.='<td style="text-align: center;"><a class="btn btn-info btn-xs" onclick="StaSel('.(int)$dat['id'].');">+</a></td>';
                $html.='</tr>';
            }
        
        }else{
            $html.='<tr>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
            $html.='</tr>';

        }

        $html.='</tbody>';
        $html.='</table>';
        return $html;

    }

    public static function GetBills($bil)
    {
        
        $bill   =   DBSmart::DBQueryAll('SELECT id, pay_amount, total_fp, (SELECT username FROM users WHERE id = operator_id) AS operator, created_at  FROM cp_mw_payments WHERE bills = "'.$bil.'" AND status_id = "0" ORDER BY id ASC');
        
        $total  =   0;

        if($bill <> false)
        {
            foreach ($bill as $b => $bi) 
            {
                $iBill[$b]  =   [
                    'id'        =>  $bi['id'],
                    'bill'      =>  $bil,
                    'amount'    =>  $bi['pay_amount'],
                    'dr'        =>  $bi['total_fp'],
                    'operator'  =>  $bi['operator'],
                    'created'   =>  $bi['created_at']
                ];
                $total  =   $total + $bi['total_fp'];
            }
            $pending    =   ($bill[0]['pay_amount'] - $total);

            return ['status' => true, 'info' => $iBill, 'pending' => $pending];
        }

        return ['status' => false, 'info' => '', 'pending' => 0];
    
    }

    public static function GetListBills($mw)
    {
        $facturas   =   'SELECT f_id, total FROM cp_mw_invoices WHERE mw = "'.$mw.'" AND estado = "No pagado" ORDER BY id ASC';
        $fac    =   DBSmart::DBQueryAll($facturas);

        $fact   =   false;
        $c      =   $total  =   0;

        if($fac <> false) 
        { 
            foreach ($fac as $f => $fa)  
            { 
                $fact[$f]   =   ['id' => $fa['f_id'], 'total' => $fa['f_id']." - Total: $".$fa['total'] ]; 
            } 
        }

        return $fact;

    }


    public static function GetBill($bill)
    {
        $facturas   =   'SELECT id FROM facturas WHERE idcliente = "'.$bill.'" AND estado = "No pagado" ORDER BY id ASC';
        $fac        =   DBMikroVE::DBQueryAll($facturas);

        if($fac <> false)
        {
            foreach ($fac as $f => $fa) 
            {
                $iFac[$f]   =   $fa['id'];
            }
        }else{
            $iFac   =   false;
        }

        return $iFac;
    
    }

    public static function HtmlBills($bill)
    {
        $alert = ''; 
        $total = 0;

        $alert.='<div class="alert alert-info fade in">';
        foreach ($bill['info'] as $b => $bil) 
        {

            $alert.='<i class="fa-fw fa fa-info"></i><strong> Pago: $ '.$bil['dr'].' </strong> - Procesado por: '.$bil['operator'].' - Fecha: '.$bil['created'].'<br>';
            $total  =   $total + $bil['dr'];
        }
        $alert.='<i class="fa-fw fa fa-info"></i><strong> Saldo Pendiente: $ '.$bill['pending'].' </strong><br>';
        $alert.='</div>';

        return $alert;
    
    }

    public static function ProcessBill($bill)
    {
        $fact   =   Transactions::GetBills($bill['rp004']);

        if($fact['status'] == true)
        {

            $iTotal =   DBSmart::DBQuery('SELECT SUM(total_fp) T FROM cp_mw_payments WHERE bills = "'.$bill['rp004'].'" AND status_id = "0"');
            
            $mkw    =   Config::ApiMWTrans();

            $url    =   $mkw['url'].'/PaidInvoice';

            $fields     =   [
                'token'         =>  $mkw['token'],
                'idfactura'     =>  $fact['info'][0]['bill'],
                'pasarela'      =>  "Pasarela de Pago",
                'cantidad'      =>  $iTotal['T'],
                'comision'      =>  "0",
                'idtransaccion' =>  date("Ymdhms"),
                'fecha'         =>  $fact['info'][0]['created']
            ];

            $payment    =   ApiMWVz::Curl($fields, $url);

            if($payment['estado'] == "exito")
            {
                $sta        =   DBSmart::DataExecute('UPDATE cp_mw_payments SET status_id = "1" WHERE bills = "'.$bill['rp004'].'" and status_id = "0"');
                $sta        =   DBSmart::DataExecute('UPDATE cp_mw_invoices SET estado = "pagado" WHERE f_id = "'.$bill['rp004'].'"');
                
                if ($sta <> false)
                {
                    $sta    =   DBSmart::DataExecute('INSERT INTO cp_mw_invoices_log (f_id, operator, created_at) VALUE(
                        "'.$bill['rp004'].'",
                        "'.$bill['rp003'].'",
                        "'.date("Y-m-d H:m:s").'"
                        )');

                    return ['status' => true,   'message' => 'Datos Procesados Correctamente', 'bill' => $bill['rp004']];

                }else{
                    return ['status' => false,  'message' => "Error al intentar actualizar el registro, intente nuevamente"];

                }

            }else{

                return ['status' => false, 'message' => strtoupper($payment['mensaje'])];
            }

        }else{
            return ['status' => false, 'message' => "No existen registros asociado a la factura seleccionada."];

        }
    
    }

    public static function PaymentForm($gen, $info, $status)
    {
        $tTran  =   $tPM    =   [];

        foreach ($gen['2'] as $i => $in) 
        {   
            $k          =   substr($i, 3, strlen($i)); 
            $tGen[$k]   = ($gen['1']['rp001'] == 1) ? $gen[2]['com'.$k.''] : $gen[3]['par'.$k.'']; 
        }
        if($status <> 1)
        {
            if(isset($info) )
            {
                foreach ($info as $i => $in) 
                {
                    $in         =   substr($i, 0, (strlen($i)-3) );
                    $k          =   substr($i, ($in-3), strlen($i));
                    $tTran[$k]  =   ($info[$in.''.$k.''] <> "") ? $info[$in.''.$k.''] : "";
                }
            }
        }

        return  [ 'gen'   =>  $tGen, 'res'   =>  $tTran];
    
    }

    public static function CheckFiels($t1, $t2, $i, $i2)
    {
        $infoo  =   $genee   =   true;
        $inf    =   '';

        if( ( ($t1 == 1) || ($t1 == 2) ) AND ($t2 == 1) )
        {
            $gen    =  $i;     $inf    =   '1';
        }else{
            $gen    =  $i;     $inf    =    $i2;
        }

        /**
         * Determinar Si los campos estan completos de Tipo de Pago
        **/
        foreach ($gen as $g => $ge) { if($gen[$g] == "")        {   $genee = false; $infoo = true; } }
        if( ($inf <> '') AND ($inf <> '1') )
        {
            foreach ($inf as $i => $in) { if($inf[$i] == "")    {   $infoo = false; } }
        }

        return ( ( ($genee == true) && ($infoo == true) ) ) ? true : false;

    }

    public static function GetSQL($t1, $t2, $i, $i2, $bill, $ope)
    {
        $genValue    =   $fiels     =   '';

        $tpGFiels['001'] =  'bills';
        $tpGFiels['002'] =  'pay_type';
        $tpGFiels['003'] =  'pay_form';
        $tpGFiels['004'] =  'pay_office';
        $tpGFiels['005'] =  'pay_box';
        $tpGFiels['006'] =  'pay_amount';

        $fFiels          =   Transactions::GenerateFiels($tpGFiels, $bill, 0);

        $tpFiels1['001'] =   'total_dr';
        $tpFiels1['002'] =   'total_vc';
        $tpFiels1['003'] =   'total_fp';

        $fiel1           =   Transactions::GenerateFiels($tpFiels1, $i, 1);
        $fiel2           =   ['fiels' => '', 'values' => ''];

        $billBD          =   Transactions::GetBills($bill['001']);
        
        $total           =   ($billBD['pending'] == 0) ? round(($bill['006'] - $i['com003']),2) : round(($billBD['pending'] - $i['com003']), 2);

        $fTotal          =   ['fiels' => 'total_df', 'values' =>  $total];

        switch ($bill['004']) 
        {
            case '1': $typeMW  =  'Pago Parcial - Efectivo - Sucursal BQTO'; break;
            case '2': $typeMW  =  'Pago Parcial - Efectivo - Sucursal SFPE'; break;
            default:  $typeMW  =  'Pago Parcial - Efectivo - Sucursal BQTO'; break;
        }

        if( $t2 == 2 )
        {
            $tpFiels2['001'] =   'trans_title';
            $tpFiels2['002'] =   'trans_account';
            $tpFiels2['003'] =   'trans_amount';
            $tpFiels2['004'] =   'trans_bank';
            $tpFiels2['005'] =   'trans_ref';
            $tpFiels2['006'] =   'trans_date';
            
            $fiel2           =   Transactions::GenerateFiels($tpFiels2, $i2, 0); 
            $fTotal          =   ['fiels' => 'total_df', 'values' =>  $total];
            $typeMW          =   'Pago Parcial - Transferencia - Bancaria';
        }

        if( $t2 == 3 )
        {
            $tpFiels3['001'] =   'pm_ced';
            $tpFiels3['002'] =   'pm_phone';
            $tpFiels3['003'] =   'pm_amount';
            $tpFiels3['004'] =   'pm_bank';
            $tpFiels3['005'] =   'pm_ref';
            $tpFiels3['006'] =   'pm_date';

            $fiel2           =   Transactions::GenerateFiels($tpFiels3, $i2, 0);
            $fTotal          =   ['fiels' => 'total_df', 'values' =>  $total]; 
            $typeMW          =   'Pago Parcial - Transferencia - Pago Movil';
        }

        if( $t2 == 4 )
        {
            $tpFiels4['001'] =   'pp_title';
            $tpFiels4['002'] =   'pp_amount';
            $tpFiels4['003'] =   'pp_ref';
            $tpFiels4['004'] =   'pp_email';
            $tpFiels4['005'] =   'pp_date';
            $fiel2           =   Transactions::GenerateFiels($tpFiels4, $i2, 0);
            $fTotal          =   ['fiels' => 'total_df', 'values' =>  $total];
            $typeMW          =   'Pago Parcial - Transferencia Electronica - PayPal';
        }

        if( $t2 == 5 )
        {
            $tpFiels5['001'] =   'zl_title';
            $tpFiels5['002'] =   'zl_ref';
            $tpFiels5['003'] =   'zl_amount';
            $tpFiels5['004'] =   'zl_date';
            $fiel2           =   Transactions::GenerateFiels($tpFiels5, $i2, 0);
            $fTotal          =   ['fiels' => 'total_df', 'values' =>  $total]; 
            $typeMW          =   'Pago Parcial - Transferencia Electronica - Zelle';
        }

        $coma   =   ($fiel2['fiels'] <> '') ? ', ' : '';

        $query  =   'INSERT INTO cp_mw_payments ('.$fFiels['fiels'].', '.$fiel1['fiels'].', '.$fTotal['fiels'].' '.$coma.' '.$fiel2['fiels'].', status_id, operator_id, created_at) VALUES('.$fFiels['values'].', '.$fiel1['values'].', "'.$fTotal['values'].'"'.$coma.''.$fiel2['values'].', "0", "'.$ope.'", "'.date("Y-m-d H:m:s").'")';

        return  ['query' => $query, 'typeMW' => $typeMW];
    
    }

    public static function GenerateFiels($tpFiels, $i, $tt)
    {
        $genValue   =   $genFiels   =   '';

        foreach ($tpFiels as $t => $tp) 
        {
            $genFiels.=''.$tp.', ';

            foreach ($i as $g => $iG) 
            {
                if($t == substr($g, (strlen($g)-3))) 
                {
                    $genValue.='"'.$iG.'", ';
                }
            }
        }

        $genFiels = substr($genFiels, 0, (strlen($genFiels)-2) );
        $genValue = substr($genValue, 0, (strlen($genValue)-2) );

        return ['fiels'  =>  $genFiels, 'values'   =>    $genValue];
    
    }

    public static function OpeStatics($iData)
    {
        
        foreach ($iData['data']['type'] as $t => $pt) 
        {
            $cGen   =   $tGen   =   0;
            foreach ($iData['data']['form'] as $p => $pf) 
            {
                $info = DBSmart::DBQuery('SELECT  
                        COUNT(pay_form) CANT, 
                        SUM(total_fp) TOTAL 
                        FROM cp_mw_payments WHERE pay_type = "'.$pt['id'].'" AND pay_form = "'.$pf['id'].'"
                        AND '.$iData['status'].' AND '.$iData['operator'].' AND '.$iData['created'].'');
                $cGen = ( (is_null($info['CANT']))      ? ($cGen + 0)      :   ($cGen + $info['CANT']));
                $tGen = ( (is_null($info['TOTAL']))     ? ($tGen + 0)      :   ($tGen + $info['TOTAL']));


                $iTotal[$pt['id']][$pf['id']]     =   [
                    'TYPE'  =>  $pf['name'],
                    'CANT'  =>  ( (is_null($info['CANT']))   ? "0"      :   $info['CANT']),
                    'TOTAL' =>  ( (is_null($info['TOTAL']))  ? "00.00"  :   $info['TOTAL']),
                    'CGEN'  =>  $cGen,
                    'TGEN'  =>  number_format($tGen,2)
                ];
            }
        }

        $iStatics   =   DBSmart::DBQueryAll('SELECT id, (SELECT name FROM cp_mw_clients WHERE mw =  (SELECT mw FROM cp_mw_invoices WHERE f_id = bills)) name, bills, (CASE WHEN pay_type = 1 THEN "COMPLETO" WHEN pay_type = 2 THEN "PARCIAL" END) type, (CASE WHEN pay_form = 1 THEN "EFECTIVO" WHEN pay_form = 2 THEN "TRANSFERENCIA" WHEN pay_form = 3 THEN "PAGO MOVIL" WHEN pay_form = 4 THEN "PAYPAL" WHEN pay_form = 5 THEN "ZELLE" END) form, (CASE WHEN status_id = 0 THEN "PENDIENTE" WHEN status_id = 1 THEN "PROCESADO" WHEN status_id = 2 THEN "SUSPENDIDO" END) status, total_fp, created_at FROM cp_mw_payments WHERE '.$iData['operator'].' AND '.$iData['created'].' ORDER BY id ASC');

        return ['info' => $iTotal, 'statics' => $iStatics];
    
    }

    public function OpeStaticsReport($iD)
    {
        $gen    =   DBSmart::DBQueryAll('SELECT id, (SELECT name FROM cp_mw_clients WHERE mw =  (SELECT mw FROM cp_mw_invoices WHERE f_id = bills)) name, bills, (CASE WHEN pay_type = 1 THEN "COMPLETO" WHEN pay_type = 2 THEN "PARCIAL" END) type, (CASE WHEN pay_form = 1 THEN "EFECTIVO" WHEN pay_form = 2 THEN "TRANSFERENCIA" WHEN pay_form = 3 THEN "PAGO MOVIL" WHEN pay_form = 4 THEN "PAYPAL" WHEN pay_form = 5 THEN "ZELLE" END) form, (CASE WHEN pay_office = 1 THEN "BARQUISIMETO" WHEN pay_office = 2 THEN "SAN FELIPE" WHEN pay_office = 3 THEN "CALL CENTER" END) office, (CASE WHEN pay_box = 1 THEN "CAJA 1" WHEN pay_box = 2 THEN "CAJA 2" WHEN pay_box = 3 THEN "CAJA 3" WHEN pay_box = 4 THEN "EJECUTIVO"  END) box, (CASE WHEN status_id = 0 THEN "PENDIENTE" WHEN status_id = 1 THEN "PROCESADO" WHEN status_id = 2 THEN "SUSPENDIDO" END) status, total_dr, total_vc, total_fp, created_at FROM cp_mw_payments WHERE '.$iD['ope'].' AND '.$iD['dat'].' ORDER BY id ASC');

        for ($i=1; $i <= 5 ; $i++) 
        { 
            // ddd('SELECT COUNT(*) C, SUM(total_fp) T FROM cp_mw_payments WHERE pay_form = "'.$i.'" AND '.$iD['ope'].' AND '.$iD['sta'].' AND '.$iD['dat'].'');
            $form[$i]   =   DBSmart::DBQuery('SELECT COUNT(*) C, SUM(total_fp) T FROM cp_mw_payments WHERE pay_form = "'.$i.'" AND '.$iD['ope'].' AND '.$iD['sta'].' AND '.$iD['dat'].'');
        }

        return ['sta' => $gen, 'form' => $form];

    }

    public static function RegLog($iData)
    {
        $sql    =   'INSERT INTO cp_mw_payment_log (f_id, message, operator, created_at) VALUES ("'.$iData['f_id'].'", "'.$iData['message'].'", "'.$iData['operator'].'", "'.date("Y-m-d").'")';
        return  DBSmart::DataExecute($sql);
    
    }

    public static function InsFielsInvo($iFact, $type)
    {

        $sql    =   'INSERT INTO facturaitems (idfactura, descripcion, cantidad, idalmacen, unidades, impuesto, block, impuesto911, clave_invoice, tipoitem, montodescuento) VALUES ("'.$iFact.'", "'.$type['type'].'", "'.$type['amount'].'", "0", "0", "0", "0", "0", "", "0","0")';

        return  DBMikroVE::DataExecute($sql);
    
    }

    public static function ViewReport($id)
    {
        $html   =   '';
        $data   =   DBSmart::DBQuery('SELECT id, bills, (CASE WHEN pay_type = 1 THEN "COMPLETO" WHEN pay_type = 2 THEN "PARCIAL" END) type, (CASE WHEN pay_form = 1 THEN "EFECTIVO" WHEN pay_form = 2 THEN "TRANSFERENCIA"  WHEN pay_form = 3 THEN "PAGO MOVIL" WHEN pay_form = 4 THEN "PAYPAL" WHEN pay_form = 5 THEN "ZELLE"  END) form, (CASE WHEN status_id = 0 THEN "PENDIENTE" WHEN status_id = 1 THEN "PROCESADO" WHEN status_id = 2 THEN "SUSPENDIDO" END) status, total_fp,  created_at FROM cp_mw_payments WHERE id = "'.$id.'"');

        $html.='<tbody>';
            $html.='<tr style="text-align:center;">';
                $html.='<td colspan="6">INFORMACI&Oacute;N DE TRANSACCI&Oacute;N</td>';
            $html.='</tr>';
            $html.='<tr style="text-align:center;">';
                $html.='<td>FACTURA</td>';
                $html.='<td>TIPO</td>';
                $html.='<td>FORMA</td>';
                $html.='<td>STATUS</td>';
                $html.='<td>FECHA</td>';
            $html.='</tr>';
            $html.='<tr style="text-align:center;">';
                $html.='<td>'.$data['bills'].'</td>';
                $html.='<td>'.$data['type'].'</td>';
                $html.='<td>'.$data['form'].'</td>';
                $html.='<td>'.$data['status'].'</td>';
                $html.='<td>'.$data['created_at'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td style="text-align:center;">MONTO</td>';
                $html.='<td colspan="4">$ '.$data['total_fp'].'</td>';
            $html.='</tr>';

            if($data['form'] == 'EFECTIVO')
            {
                $iD     =   DBSmart::DBQuery('SELECT total_dr, total_vc FROM cp_mw_payments WHERE id = "'.$id.'"');
                $html.='<tr>';
                    $html.='<td style="text-align:center;">DINERO RECIBIDO</td>';
                    $html.='<td colspan="4">$ '.$iD['total_dr'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">DINERO DE CAMBIO</td>';
                    $html.='<td colspan="4">$ '.$iD['total_vc'].'</td>';
                $html.='</tr>';
            
            }

            if($data['form'] == 'TRANSFERENCIA')
            {
                $iD     =   DBSmart::DBQuery('SELECT trans_title, trans_account, trans_amount, (SELECT name FROM data_payments_banks WHERE id = trans_bank) trans_bank, trans_ref, trans_date FROM cp_mw_payments WHERE id = "'.$id.'"');
                $html.='<tr>';
                    $html.='<td style="text-align:center;">TITULAR DE LA CUENTA</td>';
                    $html.='<td colspan="4">'.$iD['trans_title'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">N&Uacute;MERO DE CUENTA</td>';
                    $html.='<td colspan="4">'.$iD['trans_account'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">MONTO TRANSFERIDO</td>';
                    $html.='<td colspan="4">$ '.$iD['trans_amount'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">BANCO</td>';
                    $html.='<td colspan="4">'.$iD['trans_bank'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">N&Uacute;MERO DE REFERENCIA</td>';
                    $html.='<td colspan="4">$ '.$iD['trans_ref'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">FECHA DE TRANSFERENCIA</td>';
                    $html.='<td colspan="4">$ '.$iD['trans_date'].'</td>';
                $html.='</tr>';
            
            }

            if($data['form'] == 'PAGO MOVIL')
            {
                $iD     =   DBSmart::DBQuery('SELECT pm_ced, pm_phone, pm_amount, (SELECT name FROM data_payments_banks WHERE id = pm_bank) pm_bank, pm_ref, pm_date FROM cp_mw_payments WHERE id = "'.$id.'"');
                $html.='<tr>';
                    $html.='<td style="text-align:center;">C&Eacute;DULA O RIF</td>';
                    $html.='<td colspan="4">'.$iD['pm_ced'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">TEL&Eacute;FONO</td>';
                    $html.='<td colspan="4">'.$iD['pm_phone'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">MONTO TRANSFERIDO</td>';
                    $html.='<td colspan="4">$ '.$iD['pm_amount'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">BANCO</td>';
                    $html.='<td colspan="4">'.$iD['pm_bank'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">N&Uacute;MERO DE REFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['pm_ref'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">FECHA DE TRANSFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['pm_date'].'</td>';
                $html.='</tr>';
            
            }

            if($data['form'] == 'PAYPAL')
            {
                $iD     =   DBSmart::DBQuery('SELECT pp_title, pp_amount, pp_ref, pp_email, pp_date FROM cp_mw_payments WHERE id = "'.$id.'"');
                $html.='<tr>';
                    $html.='<td style="text-align:center;">TITULAR DE LA TRANSFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['pp_title'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">MONTO TRANSFERIDO</td>';
                    $html.='<td colspan="4">$ '.$iD['pp_amount'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">N&Uacute;MERO DE REFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['pp_ref'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">CORREO</td>';
                    $html.='<td colspan="4">'.$iD['pp_email'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">FECHA DE TRANSFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['pp_date'].'</td>';
                $html.='</tr>';
            
            }
            
            if($data['form'] == 'ZELLE')
            {
                $iD     =   DBSmart::DBQuery('SELECT zl_title, zl_ref, zl_amount, zl_date FROM cp_mw_payments WHERE id = "'.$id.'"');
                $html.='<tr>';
                    $html.='<td style="text-align:center;">TITULAR DE LA TRANSFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['zl_title'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">MONTO TRANSFERIDO</td>';
                    $html.='<td colspan="4">$ '.$iD['zl_amount'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">N&Uacute;MERO DE REFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['zl_ref'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td style="text-align:center;">FECHA DE TRANSFERENCIA</td>';
                    $html.='<td colspan="4">'.$iD['zl_date'].'</td>';
                $html.='</tr>';
            
            }

        return ['html' => $html, 'status' => $data['status'] ];
    
    }

    public static function SuspendedReport($id)
    {
        return  DBSmart::DataExecute('UPDATE cp_mw_payments SET status_id = "2" WHERE id = "'.$id.'"');
    }

    public static function ReceiptPayment($id)
    {
        $data    =   DBSmart::DBQueryAll('SELECT id, (SELECT name FROM cp_mw_clients WHERE mw =  (SELECT mw FROM cp_mw_invoices WHERE f_id = bills)) name, bills, (CASE WHEN pay_type = 1 THEN "COMPLETO" WHEN pay_type = 2 THEN "PARCIAL" END) type, (CASE WHEN pay_form = 1 THEN "EFECTIVO" WHEN pay_form = 2 THEN "TRANSFERENCIA" WHEN pay_form = 3 THEN "PAGO MOVIL" WHEN pay_form = 4 THEN "PAYPAL" WHEN pay_form = 5 THEN "ZELLE" END) form, (CASE WHEN status_id = 0 THEN "PENDIENTE" WHEN status_id = 1 THEN "PROCESADO" WHEN status_id = 2 THEN "SUSPENDIDO" END) status, total_fp, created_at FROM cp_mw_payments WHERE operator_id = "'.$id.'" AND (status_id = 0 OR status_id = 1) AND created_at BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59" ORDER BY id ASC');

        $html   =   '';
        $stado  =   '';

        $html = '';
        $html.='<table id="TablePrintHtml" class="table table-bordered" border="1" style="font-size: 12px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="width: 300px; text-align:center;">Cliente</th>';
        $html.='<th style="text-align:center;">Factura</th>';
        $html.='<th style="text-align:center;">Tipo</th>';
        $html.='<th style="text-align:center;">Forma</th>';
        $html.='<th style="text-align:center;">Status</th>';
        $html.='<th style="text-align:center;">Total</th>';
        $html.='<th style="width: 10%;">Acci&oacute;n</th>';

        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';

        if($data)
        {
            foreach ($data as $d => $dat) 
            {
                $html.='<tr>';
                    $html.='<td style="text-align: center; font-size:10px;">'.$dat['name'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['bills'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['type'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['form'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['status'].'</td>';
                    $html.='<td style="text-align: center;">'.$dat['total_fp'].'</td>';
                    $html.='<td style="text-align: center; width: 10px;"><a class="btn btn-info btn-xs" onclick="PrintTicket('.(int)$dat['id'].');">+</a></td>';
                $html.='</tr>';
            }
        
        }else{
            $html.='<tr>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
                $html.='<td style="text-align: center;"></td>';
            $html.='</tr>';

        }

        $html.='</tbody>';
        $html.='</table>';

        return $html;

    }

}
