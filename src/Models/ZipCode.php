<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\ZipCode;
use App\Models\Town;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class ZipCode extends Model 
{

	static $_table 		= 'data_zips';

	Public $_fillable 	= array('code', 'status_id', 'town_id', 'country_id', 'created_at');

	public static function GetZipCode()
	{
		$query 	= 	'SELECT id, code, status_id, town_id, country_id FROM data_zips ORDER BY id ASC';
		$zip 	=	DBSmart::DBQueryAll($query);

		$ZipCode = array();

		if($zip <> false)
		{
			foreach ($zip as $k => $val) 
			{
				$ZipCode[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'code' 	 		=> 	$val['code'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'town'			=> 	Town::GetTownById($val['town_id'])['name'],
					'country'		=> 	Country::GetCountryById($val['country_id'])['name'],
					'status_id'		=>	$val['status_id'],
					'town_id'		=>	$val['town_id'],
					'country_id'	=>	$val['country_id'],
				);
			}
	        
	        return $ZipCode;
		}else{ 	return false;	}
	}

	public static function GetZipById($id)
	{
		$query 		= 	'SELECT id, code, status_id, town_id, country_id FROM data_zips WHERE id = "'.$id.'"';
		$ZipCode 	=	DBSmart::DBQuery($query);

		if($ZipCode <> false)
		{
			return array(
				'id'        => $ZipCode['id'],
				'code'      => $ZipCode['code'],
				'status'    => $ZipCode['status_id'],
				'town'      => $ZipCode['town_id'],
				'country'   => $ZipCode['country_id'],
			);

		}else{ 	return false;	}
	}

	public static function GetZipByStatus()
	{
		$query 		= 	'SELECT id, code, status_id, town_id, country_id FROM data_zips WHERE id = "'.$id.'"';
		$ZipCode 	=	DBSmart::DBQuery($query);

		if($ZipCode <> false)
		{
			return array(
				'id'        => $ZipCode['id'],
				'code'      => $ZipCode['code'],
				'status'    => $ZipCode['status_id'],
				'town'      => $ZipCode['town_id'],
				'country'   => $ZipCode['country_id'],
			);

		}else{ 	return false;	}
	}

	public static function GetZipByCountry($id)
	{

		$query 	= 	'SELECT id, code FROM data_zips WHERE country_id ="'.$id.'" AND status_id = "1" ORDER BY id ASC';

		$zip 	=	DBSmart::DBQueryAll($query);

		$html = "";

		if($zip <> false)
		{
			foreach ($zip as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['code'].'</option>'; }

			return $html;

		}else{ return $html; }

	}

	public static function GetZipByTownID($id)
	{
		$query 	= 	'SELECT id, code FROM data_zips WHERE town_id ="'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$to 	=	DBSmart::DBQueryAll($query);

		return ($to <> false) ? $to : false;

	}

	public static function GetZipByTown($id)
	{
		$query 	= 	'SELECT id, code FROM data_zips WHERE town_id ="'.$id.'" AND status_id = "1" ORDER BY id ASC';

		$zip 	=	DBSmart::DBQueryAll($query);

		$html = "";

		if($zip)
		{
			$html.='<option value="0" selected="" disabled="">SELECTED</option>';

			foreach ($zip as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['code'].'</option>'; }

			return $html;

		}else{ return $html; }

	}

	public static function SaveZip($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['code_z']));

		if($info['type_z'] == 'new')
		{
			$query 	= 	'INSERT INTO data_zips(code, status_id, town_id, country_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_z'].'", "'.$info['town_id_z'].'", "'.$info['country_id_z'].'", "'.$date.'")';
			$zip 	=	DBSmart::DataExecute($query);

			return ($zip <> false ) ? true : false;
		}
		elseif ($info['type_z'] == 'edit') 
		{
			$query 	=	'UPDATE data_zips SET code="'.$name.'", status_id="'.$info['status_id_z'].'", town_id="'.$info['town_id_z'].'", country_id="'.$info['country_id_z'].'" WHERE id = "'.$info['id_z'].'"';
			$zip 	=	DBSmart::DataExecute($query);

			return ($zip <> false) ? true : false;
		}
	}

}


