<?php
namespace App\Models;

use Model;

use App\Models\StatusClient;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class StatusClient extends Model 
{

	static $_table 		= 'data_status_client';

	Public $_fillable 	= array('id', 'name', 'status_id', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetStatusClient()
	{

		$query 	= 	'SELECT id, name, status_id FROM data_status_client ORDER BY id ASC';
		$lev 	=	DBSmart::DBQueryAll($query);

		$Level = array();

		if($lev <> false)
		{
			foreach ($lev as $k => $val) 
			{
				$Level[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id']
				);
			}
	        
	        return $Level;
		}else{ 	return false;	}
	}

	public static function GetStatusClientById($id)
	{

		$query 	= 	'SELECT id, name, status_id FROM data_status_client WHERE id = "'.$id.'" ORDER BY id ASC';
		$lev 	=	DBSmart::DBQuery($query);

		if($lev <> false)
		{
			return array('id' => $lev['id'], 'name' => $lev['name'], 'status' => $lev['status_id']);

		}else{ 	return false;	}
	}

	public static function GetStatusclientByStatus()
	{

		$query 	= 	'SELECT id, name, status_id FROM data_status_client WHERE status_id = "1" ORDER BY id ASC';
		$lev 	=	DBSmart::DBQueryAll($query);

		if($lev <> false)
		{
			foreach ($lev as $k => $val) 
			{
				$status[$k] 	=	 ['id' => $val['id'], 'name' => $val['name'], 'status' => $val['status_id']];
			}
			return $status;

		}else{ 	return false;	}
	}

//////////////////////////////////////////////////////////////////////////////////////////

}