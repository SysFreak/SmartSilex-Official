<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Country;
use App\Models\Payment;
use App\Models\CypherData;

use App\Lib\Config;
use App\Lib\DBSmart;


class CypherData extends Model 
{

	static $_table 		= 'cp_cypherdata';

	Public $_fillable 	= array('client_id', 'cypher', 'key_id', 'type', 'last', 'exp', 'c_type', 'country_id', 'type_d', 'status_id', 'user_id', 'created_at');

	public static function GetCypherId($id)
	{
		$query 	=	'SELECT * FROM cp_cypherdata WHERE id = "'.$id.'"';
		$cypher =	DBSmart::DBQuery($query);

		if($cypher <> false)
		{

			return array(
				'id'    	=> $cypher['id'],
				'client'	=> $cypher['client_id'],
				'cypher'	=> $cypher['cypher'],
				'key'       => $cypher['key_id'],
				'type'      => $cypher['type'],
				'last'      => $cypher['last'],
				'exp'       => $cypher['exp'],
				'c_type'    => $cypher['type'],
				'country'   => $cypher['country_id'],
				'status'    => $cypher['status_id'],
				'status_id' => $cypher['status_id'],
				'user'      => $cypher['user_id'],
				'created'	=> $cypher['created_at']
			);

		}else{ return false; }
	}

	public static function GetCypherClientType($client, $type)
	{
		$_replace 	= new Config();

		$query 		=	'SELECT id, cypher, key_id, type, c_type, last, exp, country_id, type_d FROM cp_cypherdata WHERE client_id = "'.$client.'" AND c_type = "'.$type.'" ORDER BY id DESC';
		$cypher 	=	DBSmart::DBQuery($query);

		if($cypher <> false)
		{
			return [
				'id'			=>	$cypher['id'],
				'key'			=>	$cypher['key_id'],
				'cypher'		=>	$cypher['cypher'],
				$type			=> 	"XXXX".$cypher['last'],
				$type.'_exp'	=>	$_replace->ShowDate($cypher['exp']),
				'c_type'		=>	$cypher['c_type'],
				'type'			=>	$cypher['type'],
				'country'		=>	$cypher['country_id'],
				'type_d'		=>	$cypher['type_d']
			];
		}else{ 			
			return [
				'id'			=>	"",
				'key'			=>	"",
				'cypher'		=>	"",
				$type			=> 	"",
				$type.'_exp'	=>	"",
				'c_type'		=>	"",
				'type_d'		=>	"",
				'country'		=>	""
			]; 
		}
	}


	public static function GetCypherClient($client)
	{
		$_replace 	= 	new Config();

		$query 		=	'SELECT * FROM cp_cypherdata WHERE client_id = "'.$client.'"';
		$cypher 	=	DBSmart::DBQuery($query);

		if($cypher <> false)
		{
			return array(
				'id'       	=> $cypher['id'],
				'client'   	=> $cypher['client_id'],
				'cypher'   	=> $cypher['cypher'],
				'key'      	=> $cypher['key_id'],
				'type'     	=> $cypher['type'],
				'last'     	=> $cypher['last'],
				'exp'      	=> $_replace->ShowDate($cypher['exp']),
				'c_type'   	=> $cypher['type'],
				'country'  	=> $cypher['country_id'],
				'status'   	=> $cypher['status_id'],
				'user'     	=> $cypher['user_id'],
				'created' 	=> $cypher['created_at']
			);
		}else{ 	return false;	}

	}

	public static function SaveCypher($info, $user)
	{
		$date 	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_cypherdata(client_id, cypher, key_id, type, last, exp, c_type, country_id, status_id, type_d, user_id, created_at) VALUES ("'.$info['client'].'", "'.$info['cypher'].'", "'.$info['key'].'", "'.$info['type'].'", "'.$info['last'].'", "'.$info['exp'].'", "'.$info['c_type'].'", "'.$info['country'].'", "1", "'.$info['type_d'].'", "'.$user.'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;
	}
}