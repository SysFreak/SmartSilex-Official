<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Objection;
use App\Models\Country;
use App\Models\Service;

use App\Lib\Config;
use App\Lib\DBSmart;

class Objection extends Model 
{

	static $_table 		= 'data_objections';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id', 'created_at');

	public static function GetObjection()
	{

		$query 	= 	'SELECT id, name, service_id, country_id, status_id, created_at FROM data_objections ORDER BY id DESC';
		$obj 	=	DBSmart::DBQueryAll($query);

		$objection = array();

		if($obj <> false)
		{
			foreach ($obj as $k => $val) 
			{
				$objection[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id'],
					'service' 		=> Service::ServiceById($val['service_id'])['name'],
					'service_id'	=> $val['service_id']
				);
			}
	        
	        return $objection;

		}else{ 	return false;	}
	}

	public static function GetObjectionAll()
	{

		$query 	= 	'SELECT id, name, service_id, country_id, status_id, created_at FROM data_objections ORDER BY id DESC';
		$obj 	=	DBSmart::DBQueryAll($query);

		$objection = array();

		if($obj <> false)
		{
			foreach ($obj as $k => $val) 
			{
				$objection[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status_id'		=> $val['status_id'],
					'country_id'	=> $val['country_id'],
					'service_id'	=> $val['service_id']
				);
			}
	        
	        return $objection;

		}else{ 	return false;	}
	}

	public static function GetObjectionById($id)
	{
		$query 	= 	'SELECT id, name, service_id, country_id, status_id, created_at FROM data_objections WHERE id = "'.$id.'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack <> false)
		{
			return array(
				'id' => $pack['id'], 'name' => $pack['name'], 'service' => $pack['service_id'], 'status' => $pack['status_id'], 'country' => $pack['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetObjectionByName($info)
	{

		$query 	= 	'SELECT id, name, service_id, country_id, status_id, created_at FROM data_objections WHERE name = "'.$info['name_obj'].'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack)
		{
			return array('id' => $serv['id'], 'name' => $serv['name'], 'service' => $serv['service_id'], 'country' => $serv['country_id'], 'status' => $serv['status_id']);
		}else{
			return false;
		}
	}

	public static function GetObjectionByService($info)
	{
		$query 	= 	'SELECT id, name, service_id FROM data_objections WHERE status_id = "'.$info['status'].'" AND service_id = "'.$info['service'].'" ORDER BY id ASC';

		$serv 	=	DBSmart::DBQueryAll($query);

		if($serv <> false)
		{
			foreach ($serv as $s => $ser) 
			{
				$inf[$s] 	=	['id'		=>	$ser['id'], 'name' => $ser['name'],	'service' => $ser['service_id'] ];
			}
			return $inf;

		}else{	return false;	}
	}

	public static function SaveObjection($info)
	{
		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$name 		= 	strtoupper($_replace->deleteTilde($info['name_obj']));

		if($info['type_obj'] == 'new')
		{
			$query 	= 	'INSERT INTO data_objections(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_obj'].'", "'.$info['country_id_obj'].'", "'.$info['status_id_obj'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? $hou['last_id'] : false;

		}
		elseif ($info['type_obj'] == 'edit') 
		{
			$query 	=	'UPDATE data_objections SET name="'.$name.'", service_id="'.$info['service_id_obj'].'", country_id="'.$info['country_id_obj'].'", status_id="'.$info['status_id_obj'].'" WHERE id = "'.$info['id_obj'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}

}


		