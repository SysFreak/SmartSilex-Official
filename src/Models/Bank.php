<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Bank;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Bank extends Model {

	static $_table 		= 'data_bank';

	Public $_fillable 	= array('name', 'routs', 'country_id', 'status_id');

	public static function GetBank()
	{

		$query 	= 	'SELECT * FROM data_bank ORDER BY id ASC';
		$bank 	=	DBSmart::DBQueryAll($query);

		$banks = array();

		if($bank <> false)
		{
			foreach ($bank as $k => $val) 
			{
				$banks[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'rout' 	 		=> $val['routs'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $banks;
		}else{ 	return false;	}
	}

	public static function GetBankById($id)
	{
		$query 	= 	'SELECT * FROM data_bank WHERE id = "'.$id.'"';
		$bank 	=	DBSmart::DBQuery($query);

		if($bank <> false)
		{
			return array(
				'id' => $bank['id'], 'name' => $bank['name'], 'rout' => $bank['routs'], 'status' => $bank['status_id'], 'country' => $bank['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetBankByName($info)
	{

		$query 	= 	'SELECT * FROM data_bank WHERE name = "'.$info.'"';
		$bank 	=	DBSmart::DBQuery($query);

		if($bank <> false)
		{
			return array(
				'id' => $bank['id'], 'name' => $bank['name'], 'rout' => $bank['routs'], 'status' => $bank['status_id'], 'country' => $bank['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetBankByCountry($id)
	{
		
		$query 	= 	'SELECT id, name FROM data_bank WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$bank 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($bank)
		{
			foreach ($bank as $k => $val) 
			{ $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';}

			return $html;

		}else{ return $html; }
	}

	public static function SaveBank($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_ba']));

		if($info['type_ba'] == 'new')
		{
			$query 	= 	'INSERT INTO data_bank(name, routs, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['rout_ba'].'", "'.$info['status_id_ba'].'", "'.$info['country_id_ba'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_ba'] == 'edit') 
		{
			$query 	=	'UPDATE data_bank SET name="'.$name.'", routs="'.$info['rout_ba'].'", country_id="'.$info['country_id_ba'].'", status_id="'.$info['status_id_ba'].'" WHERE id = "'.$info['id_ba'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}

}

		