<?php
namespace App\Models;

use Model;

use App\Models\Status;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;

class ContractVE extends Model 
{

    static $_table      = 'data_contracts';

    Public $_fillable   = array('ticket', 'client_id', 'name', 'add_main', 'add_postal', 'phone_main', 'phone_alt', 'email', 'nivel', 'techo', 'house', 'coor_lati', 'coor_long', 'package', 'type', 'pre_date', 'date_res', 'price', 'prorateo', 'mes', 'instalation', 'cc_last', 'cc_exp', 'ab_last', 'ab_exp', 'ab_bank', 'id_last', 'id_exp', 'ss_last', 'ss_exp', 'contract', 'country', 'sale', 'tp', 'created_by', 'created_at');

    public static function LoadVE()
    {
        $_replace   =   new Config();
        $cWeek      =   $_replace->CurrentWeek();

        $query      =   'SELECT client_id, ticket, name, phone_main, sale, created_by, type, tp, created_at, contract FROM cp_contracts WHERE country = "VENEZUELA" AND created_at BETWEEN "'.$cWeek['yI'].'-'.$cWeek['mI'].'-'.$cWeek['dI'].' 00:00:00" AND "'.$cWeek['yF'].'-'.$cWeek['mF'].'-'.$cWeek['dF'].' 23:59:59"';

        $res        =   DBSmart::DBQueryAll($query);

        $htmlRes    =   ContractVE::ContractHtmlRes($res);
        $htmlCom    =   ContractVE::ContractHtmlCom($res);

        return ['res'   =>  $htmlRes, 'com'     =>  $htmlCom];
    }

    public static function LoadVEFib()
    {
        $_replace   =   new Config();
        $cWeek      =   $_replace->CurrentWeek();

        $query      =   'SELECT client_id, ticket, name, phone_main, sale, created_by, type, tp, created_at, contract FROM cp_contracts_fibra WHERE country = "VENEZUELA" AND created_at BETWEEN "'.$cWeek['yI'].'-'.$cWeek['mI'].'-'.$cWeek['dI'].' 00:00:00" AND "'.$cWeek['yF'].'-'.$cWeek['mF'].'-'.$cWeek['dF'].' 23:59:59"';

        $res        =   DBSmart::DBQueryAll($query);

        $htmlRes    =   ContractVE::ContractHtmlFibRes($res);

        return ['fib'   =>  $htmlRes];
    }

    public static function SearchSingleLoadVE($iDate)
    {
        $_replace   =   new Config();
        $cWeek      =   $_replace->CurrentWeek();

        $query      =   'SELECT * FROM cp_contracts WHERE country = "VENEZUELA" AND created_at LIKE "'.$iDate.'%"';
        $res        =   DBSmart::DBQueryAll($query);


        $query      =   'SELECT * FROM cp_contracts_fibra WHERE country = "VENEZUELA" AND created_at LIKE "'.$iDate.'%"';
        $fib        =   DBSmart::DBQueryAll($query);

        $htmlRes        =   ContractVE::ContractHtmlRes($res);
        $htmlCom        =   ContractVE::ContractHtmlCom($res);
        $htmlFibRes     =   ContractVE::ContractHtmlFibRes($fib);

        return ['res'   =>  $htmlRes, 'com'     =>  $htmlCom, 'fib'     =>  $htmlFibRes];
    
    }

    public static function SearchBetweenLoadVE($iDate)
    {
        $_replace   =   new Config();
        $cWeek      =   $_replace->CurrentWeek();

        $query      =   'SELECT * FROM cp_contracts WHERE country = "VENEZUELA" AND created_at BETWEEN "'.$iDate['dateIni'].' 00:00:00" AND "'.$iDate['dateEnd'].' 23:59:59"';

        $res        =   DBSmart::DBQueryAll($query);

        $query      =   'SELECT * FROM cp_contracts_fibra WHERE country = "VENEZUELA" AND created_at BETWEEN "'.$iDate['dateIni'].' 00:00:00" AND "'.$iDate['dateEnd'].' 23:59:59"';

        $fib        =   DBSmart::DBQueryAll($query);

        $htmlRes        =   ContractVE::ContractHtmlRes($res);
        $htmlCom        =   ContractVE::ContractHtmlCom($res);
        $htmlFibRes     =   ContractVE::ContractHtmlFibRes($fib);

        return ['res'   =>  $htmlRes, 'com'     =>  $htmlCom, 'fib'     =>  $htmlFibRes];
    
    }

    public static function GetInfoCoordClient($client)
    {
        $query      =   'SELECT t1.service_id, t2.ticket, t2.client_id, t2.coord_id, (SELECT pre_date FROM cp_coord_pre_tmp WHERE id = t2.pre_id) AS pre_date, (SELECT username FROM users WHERE id = t1.created_by) AS sale, t1.created_at AS sale_date FROM cp_appro_serv AS t1 INNER JOIN cp_coord_serv AS t2 ON (t1.ticket =  t2.ticket) AND t2.client_id = "'.$client.'" AND t2.coord_id <> 0 AND t1.service_id = "6" ORDER BY t1.id DESC LIMIT 1';

        $res    =   DBSmart::DBQuery($query);

        return ($res <> false ) ? $res : false;
    
    }

    public static function GetInfoPack($ticket)
    {

        $query  =   'SELECT (SELECT IF(int_com_id <> "20", (SELECT name FROM data_internet WHERE id = int_com_id), (SELECT name FROM data_internet WHERE id =int_res_id))) AS pack, (SELECT IF(int_com_id <> "20", "COMERCIAL", "RESIDENCIAL")) AS type, (SELECT IF(int_com_id <> "20", (SELECT price FROM data_internet WHERE id = int_com_id), (SELECT price FROM data_internet WHERE id = int_res_id))) AS price, (SELECT IF(pho_com_id <> "13", (SELECT name FROM data_phone WHERE id = pho_com_id), (SELECT name FROM data_phone WHERE id = pho_res_id))) AS telf, (SELECT IF(pho_com_id <> "13", "COMERCIAL", "RESIDENCIAL")) AS teltype, (SELECT IF(pho_com_id <> "13", (SELECT price FROM data_phone WHERE id = pho_com_id), (SELECT price FROM data_phone WHERE id = pho_res_id))) AS telprice FROM cp_service_vzla_int WHERE ticket = "'.$ticket.'"';

        $res    =   DBSmart::DBQuery($query);

        return ($res <> false ) ? $res : false;
    
    }

    public static function GetInfoClient($client)
    {
        $query      =   'SELECT client_id, name, add_main, add_postal, phone_main, phone_alt, phone_other, email_main, repre_legal, (SELECT name FROM data_levels WHERE id = h_level_id) AS nivel, (SELECT name FROM data_ceiling WHERE id = h_roof_id) AS techo, (SELECT name FROM data_house WHERE id = h_type_id) AS house, coor_lati, coor_long FROM cp_leads WHERE client_id = "'.$client.'"';

        $res    =   DBSmart::DBQuery($query);

        return ($res <> false ) ? $res : false;
    
    }

    public static function GetCypherClient($client)
    {
        $cy     =   ['CC', 'AB', 'ID', 'SS'];

        $cc = $ab = $id = $ss = '';

        foreach ($cy as $k => $val) 
        {
            $query      =   'SELECT last, exp, c_type FROM cp_cypherdata WHERE client_id = "'.$client.'" AND c_type = "'.$val.'" ORDER BY id DESC LIMIT 1';

            $cyp        =   DBSmart::DBQuery($query);

            switch ($val) 
            {
                case 'CC':
                    $cc     =   ($cyp <> false ) ? ['last' => 'xxx-xxxx-xxxx-'.$cyp['last'], 'exp' => $cyp['exp'] ] : ['last' => '', 'exp' => ''];
                    break;
                
                case 'AB':
                    $cData  =   UnCypher::GetInfoCypher($client, 'AB');

                    $query  =   'SELECT name FROM data_bank WHERE id = "'.$cData['infoCard']['bank'].'"';
                    $bank   =    DBSmart::DBQuery($query)['name'];

                    $ab     =   ($cyp <> false ) ? ['last' => 'xxx-xx-'.$cyp['last'], 'exp' => $cyp['exp'], 'bank' => $bank ] : ['last' => '', 'exp' => '', 'bank' => ''];
                    break;

                case 'ID':
                    $cID  = UnCypher::GetInfoCypher($client, 'ID');
                    
                    $id     =   ($cyp <> false ) ? ['last' => $cID['infoCard']['id'], 'exp' => $cyp['exp'] ] : ['last' => '', 'exp' => ''];
                    break;

                case 'SS':
                    $cSS  = UnCypher::GetInfoCypher($client, 'SS');
                    $ss     =   ($cyp <> false ) ? ['last' => $cSS['infoCard']['ss'], 'exp' => $cyp['exp'] ] : ['last' => '', 'exp' => ''];
                    break;
            }
        
        }

        $iCypher    =   [
            'cc'    =>  $cc,
            'ab'    =>  $ab,
            'id'    =>  $id,
            'ss'    =>  $ss
        ];

        return $iCypher;
    
    }

    public static function GetProrateo($info)
    {

        $dT         =   "30";
        $dC         =   substr($info['pre_date'],-2);
        $dR         =   $dT- $dC;

        if($dR <= 0) { $total = "0.00"; }else{ $total  =   ( ($info['price'] / $dT) * $dR); }

        return $total;

    }

    public static function GetProrateo2($info)
    {

        $dT         =   "30";
        $dC         =   substr($info['cli_ve_date'],3,2);
        $dR         =   $dT- $dC;

        if($dR <= 0) { $total = "0.00"; $dR = "0"; }else{ $total  =   round(( ($info['cli_ve_price'] / $dT) * $dR), 2); }

        return ['dc' => $dC, 'dRes' => $dR, 'total' => $total ];

    }

    public static function GetDateEnd($f, $y)
    {
        $_replace   =   New Config();

        $fecha  =   $_replace->ChangeDate($f);

        if($y == "24"){ $year = 2; }elseif($y == "36") { $year = "3"; }

        $nFecha = strtotime('+'.$year.' year',strtotime($fecha));

        return $_replace->ShowDate(date('Y-m-d',$nFecha));
    }

    public static function InsertInfoContract($info)
    {
        $query  =   'INSERT INTO cp_contracts(ticket, client_id, name, add_main, add_postal, phone_main, phone_alt, phone_other, email, nivel, techo, house, coor_lati, coor_long, package, type, telephone, tel_type, tel_price, pre_date, last_date, date_res, price, prorateo, mes, instalation, cc_last, cc_exp, ab_last, ab_exp, ab_bank, id_last, id_exp, ss_last, ss_exp, contract, country, sale, repre_legal, tp, created_by, created_at) VALUES ("'.$info['ticket'].'","'.$info['client'].'","'.$info['name'].'","'.$info['add_main'].'","'.$info['add_postal'].'","'.$info['phone_main'].'","'.$info['phone_alt'].'","'.$info['phone_other'].'","'.$info['email'].'","'.$info['nivel'].'","'.$info['techo'].'","'.$info['house'].'","'.$info['coor_lati'].'","'.$info['coor_long'].'","'.$info['package'].'", "'.$info['type'].'", "'.$info['telephone'].'", "'.$info['tel_type'].'", "'.$info['tel_price'].'", "'.$info['pre_date'].'","'.$info['last_date'].'", "'.$info['date_res'].'","'.$info['price'].'","'.$info['prorateo'].'","'.$info['mes'].'","'.$info['instalation'].'","'.$info['cc_last'].'","'.$info['cc_exp'].'","'.$info['ab_last'].'", "'.$info['ab_exp'].'","'.$info['ab_bank'].'","'.$info['id_last'].'","'.$info['id_exp'].'","'.$info['ss_last'].'","'.$info['ss_exp'].'","'.$info['contract'].'","'.$info['country'].'","'.$info['sale'].'", "'.$info['repre'].'", "'.$info['tp'].'", "'.$info['created_by'].'", NOW())';
        
        $res    =   DBSmart::DataExecute($query);

        return ($res <> false ) ? $res : false;


    }


    public static function SaveContractFib($iData)
    {
        $query  =   'INSERT INTO cp_contracts_fibra(ticket, client_id, name, add_main, add_postal, phone_main, phone_alt, phone_other, email, nivel, techo, house, coor_lati, coor_long, package, type, telephone, tel_type, tel_price, pre_date, last_date, date_res, price, prorateo, mes, instalation, cc_last, cc_exp, ab_last, ab_exp, ab_bank, id_last, id_exp, ss_last, ss_exp, contract, country, sale, repre_legal, tp, created_by, created_at) VALUES ("'.$iData['ticket'].'","'.$iData['client'].'","'.$iData['name'].'","'.$iData['add_main'].'","'.$iData['add_postal'].'","'.$iData['phone_main'].'","'.$iData['phone_alt'].'","'.$iData['phone_other'].'", "'.$iData['email'].'","'.$iData['nivel'].'","'.$iData['techo'].'","'.$iData['house'].'","'.$iData['coor_lati'].'","'.$iData['coor_long'].'","'.$iData['package'].'","'.$iData['type'].'","'.$iData['telephone'].'","'.$iData['tel_type'].'","'.$iData['tel_price'].'","'.$iData['pre_date'].'","'.$iData['last_date'].'","'.$iData['date_res'].'","'.$iData['price'].'","'.$iData['prorateo'].'","'.$iData['mes'].'","'.$iData['instalation'].'","'.$iData['cc_last'].'","'.$iData['cc_exp'].'","'.$iData['ab_last'].'","'.$iData['ab_exp'].'","'.$iData['ab_bank'].'","'.$iData['id_last'].'","'.$iData['id_exp'].'","'.$iData['ss_last'].'","'.$iData['ss_exp'].'","'.$iData['contract'].'","'.$iData['country'].'","'.$iData['sale'].'","'.$iData['repre'].'","'.$iData['tp'].'","'.$iData['created_by'].'", NOW())';

        $res    =   DBSmart::DataExecute($query);

        return ($res <>  false) ? true : false;

    }

    public static function ContractHtmlRes($iData)
    {
        $_replace   =   new Config();
        $html       =   "";

        $html.='<div class="table-responsive">';
        $html.='<table id="ve_contract_res" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Ticket</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Tel&eacute;fono</th>';
        $html.='<th style="text-align: center;">Vendedor</th>';
        $html.='<th style="text-align: center;">Tipo</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Formato</th>';
        $html.='<th style="text-align: center;">Creado Por</th>';
        $html.='<th style="text-align: center;">Accion</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {

                // var_dump($inf);
                // exit;
                if($inf['type'] == "RESIDENCIAL")
                {
                    $con    =   ($inf['tp'] == 1) ? "ContractVE" : "ContractVEN";
                    $html.='<tr>';
                    $html.='<td style="text-align: center;">'.$inf['client_id'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['ticket'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['name'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['phone_main'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['sale'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['type'].'</td>';
                    $html.='<td style="text-align: center;">'.$_replace->ShowDateAll($inf['created_at']).'</td>';
                    $html.='<td style="text-align: center;">'.(($inf['tp'] == "1") ? "VIEJO" : "NUEVO" ).'</td>';
                    $html.='<td style="text-align: center;">'.$inf['created_by'].'</td>';
                    $html.='<td style="text-align: center;"><a class="btn btn-xs btn-default" href="'.$con.'.php?type='.$inf['type'].'&contract='.$inf['contract'].'"><i class="fa fa-eye"></i></a></td>';                      
                    $html.='</tr>';
                }
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }
        $html.='</div>';
        
        return  $html;
    
    }

    public static function ContractHtmlCom($iData)
    {
        $_replace   =   new Config();
        $html       =   "";

        $html.='<div class="table-responsive">';
        $html.='<table id="ve_contract_com" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Ticket</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Tel&eacute;fono</th>';
        $html.='<th style="text-align: center;">Vendedor</th>';
        $html.='<th style="text-align: center;">Tipo</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Formato</th>';
        $html.='<th style="text-align: center;">Creado Por</th>';
        $html.='<th style="text-align: center;">Accion</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                if($inf['type'] == "COMERCIAL")
                {
                    $con    =   ($inf['tp'] == 1) ? "ContractVE" : "ContractVEN";
                    $html.='<tr>';
                    $html.='<td style="text-align: center;">'.$inf['client_id'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['ticket'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['name'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['phone_main'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['sale'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['type'].'</td>';
                    $html.='<td style="text-align: center;">'.$_replace->ShowDateAll($inf['created_at']).'</td>';
                    $html.='<td style="text-align: center;">'.$inf['created_by'].'</td>';
                    $html.='<td style="text-align: center;">'.(($inf['tp'] == "1") ? "VIEJO" : "NUEVO" ).'</td>';
                    $html.='<td style="text-align: center;"><a class="btn btn-xs btn-default" href="'.$con.'.php?type='.$inf['type'].'&contract='.$inf['contract'].'"><i class="fa fa-eye"></i></a></td>';                      
                    $html.='</tr>';
                }
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }
        $html.='</div>';
        
        return  $html;
    
    }

    public static function ContractHtmlFibRes($iData)
    {
        $_replace   =   new Config();
        $html       =   "";

        $html.='<div class="table-responsive">';
        $html.='<table id="ve_contract_fib_res" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Ticket</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Tel&eacute;fono</th>';
        $html.='<th style="text-align: center;">Vendedor</th>';
        $html.='<th style="text-align: center;">Tipo</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Formato</th>';
        $html.='<th style="text-align: center;">Creado Por</th>';
        $html.='<th style="text-align: center;">Accion</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                if($inf['type'] == "RESIDENCIAL")
                {
                    $con    =   "ContractVEFib";
                    $html.='<tr>';
                    $html.='<td style="text-align: center;">'.$inf['client_id'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['ticket'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['name'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['phone_main'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['sale'].'</td>';
                    $html.='<td style="text-align: center;">'.$inf['type'].'</td>';
                    $html.='<td style="text-align: center;">'.$_replace->ShowDateAll($inf['created_at']).'</td>';
                    $html.='<td style="text-align: center;">'.(($inf['tp'] == "1") ? "VIEJO" : "NUEVO" ).'</td>';
                    $html.='<td style="text-align: center;">'.$inf['created_by'].'</td>';
                    $html.='<td style="text-align: center;"><a class="btn btn-xs btn-default" href="'.$con.'.php?type='.$inf['type'].'&contract='.$inf['contract'].'"><i class="fa fa-eye"></i></a></td>';                      
                    $html.='</tr>';
                }
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }
        $html.='</div>';
        
        return  $html;
    
    }
}   