<?php
namespace App\Models;

use Model;

use App\Models\Campaign;
use App\Models\Status;
use App\Models\Service;
use App\Models\Country;
use App\Models\Creative;

use App\Lib\Config;
use App\Lib\DBSmart;

class Campaign extends Model {

	static $_table 		= 'mkt_campaign';

	Public $_fillable 	= array('name', 'img_id', 'country_id', 'service_id', 'status_id');

	public static function GetCampaign()
	{

		$query 	= 	'SELECT * FROM mkt_campaign ORDER BY id ASC';
		$camp 	=	DBSmart::DBQueryAll($query);
		
		if($camp <> false)
		{
			foreach ($camp as $k => $val) 
			{
				$campaign[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=>	$val['name'],
					'base'	 		=> 	Creative::GetImgId($val['img_id'])['base'],
					'base_id' 		=> 	$val['img_id'],
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=>	$val['country_id'],
					'service'  		=>	Service::ServiceById($val['service_id'])['name'],
					'service_di'  	=>	$val['service_id'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id']
				);
			}
	        
	        return $campaign;

		}else{ return false; }

	}

	public static function HtmlCampaign($iData, $country)
	{

		$html = '';
		
		if($iData <> false)
		{
	        $html.='<div class="well">';
	        $html.='<h3 style="text-align: center;">'.$country['name'].'</h3>';
	        $html.='<div id="'.$country['sig'].'Carousel" class="carousel slide">';
	            $html.='<ol class="carousel-indicators">';
	            for ($i=1; $i <= $iData['total'] ; $i++) 
	            {   
	                $class = ( $i == 1) ? "active" : ""; $html.='<li data-target="#'.$country['sig'].'Carousel" data-slide-to="'.$i.'" class="'.$class.'"></li>'; 
	            }
	            $html.='</ol>';
	            $html.='<div class="carousel-inner">';
	            foreach ($iData['campaign'] as $k => $val) 
	            {
	                $c = $k +1;
	                $act    =   ($c == 1) ? "active" : "";
	                $html.='<div class="item '.$act.'" style="text-align: -webkit-center;">';
	                    $html.='<img src="'.$val['base'].'" alt="'.$val['name'].'" title="'.$val['name'].'" style="display: inherit;">';
	                    $html.='<div class="carousel-caption caption-right">';
	                        $html.='<h4 style="font-size: 15px;">'.$val['name'].'</h4>';
	                        $html.='<br>';
	                    $html.='</div>';
	                  $html.='<div style="margin-top: 10px; text-align: center;">';
	                    $html.=''.$val['copy'].'';
	                  $html.='</div>';
	                $html.='</div>';
	            }
	            $html.='</div>';
				$html.='<a class="left carousel-control" href="#'.$country['sig'].'Carousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span></a>';
				$html.='<a class="right carousel-control" href="#'.$country['sig'].'Carousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span></a>';
	        $html.='</div>';
	        $html.='</div>';
		}


		return $html;
	}

	public static function GetCampaignActive($country)
	{
		$query 		= 	'SELECT * FROM mkt_campaign WHERE status_id = "1" AND country_id = "'.$country.'" ORDER BY country_id ASC';
		$camp 		=	DBSmart::DBQueryAll($query);
		$query 		=	'SELECT COUNT(*) AS count FROM mkt_campaign WHERE status_id = "1" AND country_id = "'.$country.'"';
		$cont 		=	DBSmart::DBQuery($query)['count'];

		if($camp <> false)
		{
			foreach ($camp as $k => $val) 
			{
				$campaign[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=>	$val['name'],
					'base'	 		=> 	Creative::GetImgId($val['img_id'])['base'],
					'base_id' 		=> 	$val['img_id'],
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=>	$val['country_id'],
					'service'  		=>	Service::ServiceById($val['service_id'])['name'],
					'service_di'  	=>	$val['service_id'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id'],
					'copy'			=>	$val['copyright']
				);
			}
	        
	        return 	[
	        			'campaign'	=>	$campaign,
	        			'total'		=>	$cont
	        		];

		}else{ return false; }	
	}


	public static function GetCampaignById($id)
	{
		$query 	= 	'SELECT * FROM mkt_campaign WHERE id = "'.$id.'" ORDER BY id ASC';
		$camp 	=	DBSmart::DBQuery($query);

		if($camp <> false)
		{
			return array(
			 	'id'        => $camp['id'],
			 	'name'      => $camp['name'],
			 	'img'      	=> $camp['img_id'],
			 	'imgC'      => Creative::GetImgId($camp['img_id'])['base'],
			 	'imgH'		=> '<img src="'.Creative::GetImgId($camp['img_id'])['base'].'" alt="SmartAdmin" style="width: 250px;">',
			 	'country'   => $camp['country_id'],
			 	'service'   => $camp['service_id'],
			 	'status'    => $camp['status_id'],
			 	'copy'		=> $camp['copyright']
			);

		}else{ return false; }
	}


	public static function GetCampaignByCountryID($id)
	{
		$query 	= 	'SELECT id, name, status_id FROM mkt_campaign WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$camp 	=	DBSmart::DBQueryAll($query);

		$html = "";

		if($camp <> false)
		{

			foreach ($camp as $k => $val) 
			{
				$campaign[$k] = [
					'id'		=>	$val['id'],
					'name'		=>	$val['name'],
					'status'	=>	$val['status_id']
				];
			}
			return $campaign; 
		}else{ return false; }
		
	}

	public static function GetCampaignByCountry($id)
	{

		$query 	= 	'SELECT * FROM mkt_campaign WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$camp 	=	DBSmart::DBQueryAll($query);

		$html = "";

		if($camp <> false)
		{
			foreach ($camp as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			return $html;

		}else{ return $html; }
	}

	public static function GetCampaignByName($info)
	{
		$query 	= 	'SELECT * FROM mkt_campaign WHERE name = "'.$info['name_camp'].'" AND service_id = "'.$info['service_id_camp'].'" ORDER BY id ASC';
		$camp 	=	DBSmart::DBQueryAll($query);

		if($camp <> false)
		{
			return array('id' => $camp['id'], 'name' => $camp['name'], 'img_id' => $camp['img_id'], 'country_id' => $camp['country_id'], 'service_id' => $camp['service_id'], 'status' => $camp['status_id']);
		}else{
			return false;
		}
	}

	public static function SaveCampaign($info)
	{
		$date 		= date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_camp']));
		$copy 		= strtoupper($_replace->deleteTilde($info['copy_camp']));

		if($info['type_camp'] == 'new')
		{
			$query 	= 	'INSERT INTO mkt_campaign(name, img_id, country_id, service_id, status_id, copyright, created_at) VALUES ("'.$name.'", "'.$info['img_id_camp'].'", "'.$info['country_id_camp'].'", "'.$info['service_id_camp'].'", "'.$info['status_id_camp'].'", "'.$copy.'", "'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['type_camp'] == 'edit') 
		{
			$query 	=	'UPDATE mkt_campaign SET name="'.$name.'", img_id="'.$info['img_id_camp'].'", country_id="'.$info['country_id_camp'].'", service_id="'.$info['service_id_camp'].'", status_id="'.$info['status_id_camp'].'", copyright="'.$copy.'" WHERE id = "'.$info['id_camp'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}


