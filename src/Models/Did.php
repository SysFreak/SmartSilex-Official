<?php
namespace App\Models;

use Model;

use App\Models\Did;
use App\Models\Status;
use App\Models\Departament;
use App\Models\Service;

use App\Lib\DBSmart;
use App\Lib\Config;

class Did extends Model {

	static $_table 		= 'data_did';

	Public $_fillable 	= array('name', 'inbound', 'outbound', 'queue', 'departament_id', 'services_id', 'status_id', 'tv', 'sec', 'net', 'ene', 'created_at');

	public static function GetDid()
	{

		$query 	= 	'SELECT * FROM data_did ORDER BY id ASC';
		$did 	=	DBSmart::DBQueryAll($query);

		$dids 	= array();

		if($did <> false)
		{
			foreach ($did as $k => $val) 
			{
				$dids[$k] = array(
				'id' 	 		=> $val['id'], 
				'name' 	 		=> $val['name'], 
				'inbound' 		=> $val['inbound'], 
				'outbound' 		=> $val['outbound'], 
				'queue' 		=> $val['queue'], 
      			'departament'	=> Departament::GetDepById($val['departament_id'])['name'],
      			'dep'			=> $val['departament_id'],
      			'service'		=> Service::ServiceById($val['services_id'])['name'],
      			'serv'			=> $val['services_id'],
            	'status'       	=> Status::GetStatusById($val['status_id'])['name'],
            	'status_id' 	=> $val['status_id'],	            
	            'tv'           	=> $val['tv_pr'],
	            'sec'          	=> $val['sec_pr'],
	            'net'          	=> $val['net_pr'],
	            'ene'          	=> $val['ene_pr'],
	            'tv_usa'        => $val['tv_usa'],
	            'wir_usa'       => $val['wir_usa'],
	            'net_usa'       => $val['net_usa'],	            
	            'tv_col'      	=> $val['tv_col'],
	            'sec_col'       => $val['sec_col'],
	            'net_col'       => $val['net_col']
			);
			}
	        
	        return $dids;
		}else{ 	return false;	}
	}

	public static function GetDidById($id)
	{
		$query 	= 	'SELECT * FROM data_did WHERE id="'.$id.'"';
		$did 	=	DBSmart::DBQuery($query);

		if($did <>  false)
		{

			return array(
				'id' 	 		=> $did['id'], 
				'name' 	 		=> $did['name'], 
				'queue' 		=> $did['queue'], 
				'inbound' 		=> $did['inbound'], 
				'outbound' 		=> $did['outbound'], 
	  			'departament'	=> $did['departament_id'],
	  			'service'		=> $did['services_id'],
	        	'status'       	=> $did['status_id'],
	            'tv'           	=> $did['tv_pr'],
	            'sec'          	=> $did['sec_pr'],
	            'net'          	=> $did['net_pr'],
	            'ene'          	=> $did['ene_pr'],
	            'tv_usa'      	=> $did['tv_usa'],
	            'wir_usa'      	=> $did['wir_usa'],
	            'net_usa'      	=> $did['net_usa'],
	            'tv_col'       	=> $did['tv_col'],
	            'sec_col'      	=> $did['sec_col'],
	            'net_col'      	=> $did['net_col'],
			);

		}else{ return false; }

	}

	public static function GetDidByName($info)
	{

		$query 	= 	'SELECT id, name, status_id FROM data_did WHERE name="'.$info.'"';
		$did 	=	DBSmart::DBQuery($query);

		if($did)
		{
			return array('id' => $did['id'], 'name' => $did['name'], 'status' => $did['status_id']);

		}else{ return false; }
	}

	public static function SaveDid($info)
	{

		$date = date('Y-m-d H:i:s', time());

		$_replace  = new Config();

		$iData 	=	[
			'name' 				=> 	strtoupper($_replace->deleteTilde($info['name_di'])),
			'inbound' 			=>	$info['inbound'],
			'outbound' 			=> 	$info['outbound'],
			'queue' 			=> 	$info['queue'],
			'departament_id' 	=> 	$info['departament_id_di'],
			'services_id' 		=> 	$info['services_id_di'],
			'status_id' 		=> 	$info['status_id_di'],
		 	'tv' 				=>	(isset($info['tv_pr_di']))  	? $info['tv_pr_di']  	: 'off',
	      	'sec' 				=>	(isset($info['sec_pr_di'])) 	? $info['sec_pr_di'] 	: 'off',
	      	'net' 				=>	(isset($info['net_pr_di'])) 	? $info['net_pr_di'] 	: 'off',
	      	'ene' 				=>	(isset($info['ene_pr_di'])) 	? $info['ene_pr_di'] 	: 'off',
		 	'tvU' 				=>	(isset($info['tv_usa_di']))  	? $info['tv_usa_di']  	: 'off',
	      	'wirU' 				=>	(isset($info['wir_usa_di'])) 	? $info['wir_usa_di'] 	: 'off',
	      	'netU' 				=>	(isset($info['net_usa_di'])) 	? $info['net_usa_di'] 	: 'off',
		 	'tvC' 				=>	(isset($info['tv_col_di']))  	? $info['tv_col_di']  	: 'off',
	      	'netC' 				=>	(isset($info['net_col_di'])) 	? $info['net_col_di'] 	: 'off',
	      	'secC' 				=>	(isset($info['sec_col_di'])) 	? $info['sec_col_di'] 	: 'off'
		];

		if($info['type_di'] == 'new')
		{
			$query 	= 	'INSERT INTO data_did(name, inbound, outbound, queue, departament_id, services_id, status_id, tv_pr, sec_pr, net_pr, ene_pr, tv_usa, wir_usa, net_usa, tv_col, net_col, sec_col, created_at) VALUES ("'.$iData['name'].'","'.$iData['inbound'].'","'.$iData['outbound'].'","'.$iData['queue'].'","'.$iData['departament_id'].'","'.$iData['services_id'].'","'.$iData['status_id'].'","'.$iData['tv'].'","'.$iData['sec'].'","'.$iData['net'].'","'.$iData['ene'].'","'.$iData['tvU'].'","'.$iData['wirU'].'","'.$iData['netU'].'","'.$iData['tvC'].'","'.$iData['netC'].'","'.$iData['secC'].'", "'.$date.'")';

			$did 	=	DBSmart::DataExecute($query);

			return ($did <> false) ? true : false;

		}
		elseif ($info['type_di'] == 'edit') 
		{

			$query = 'UPDATE data_did SET name="'.$iData['name'].'",inbound="'.$iData['inbound'].'",outbound="'.$iData['outbound'].'", queue="'.$iData['queue'].'", departament_id="'.$iData['departament_id'].'",services_id="'.$iData['services_id'].'",status_id="'.$iData['status_id'].'",tv_pr="'.$iData['tv'].'",sec_pr="'.$iData['sec'].'",net_pr="'.$iData['net'].'",ene_pr="'.$iData['ene'].'",tv_usa="'.$iData['tvU'].'",wir_usa="'.$iData['wirU'].'",net_usa="'.$iData['netU'].'",tv_col="'.$iData['tvC'].'",net_col="'.$iData['netC'].'",sec_col="'.$iData['secC'].'" WHERE id = "'.$info['id_di'].'"';

			$did 	=	DBSmart::DataExecute($query);

			return ($did <> false) ? true : false;
		}
	}

}