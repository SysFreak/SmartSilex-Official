<?php
namespace App\Models;

use Model;

use App\Models\LeadSuite;
use App\Lib\Config;

use App\Lib\DBSugar;
use App\Lib\DBSugarTest;

use PDO;

class LeadSuite extends Model 
{

	static $_table 		= 'lead_suite';

	Public $_fillable 	= array('id_c', 'client_id', 'name', 'birthday', 'phone_main', 'phone_main_owner', 'phone_alt', 'phone_alt_owner', 'phone_other', 'phone_other_owner', 'email_main', 'email_other', 'add_main', 'add_postal', 'coor_lati', 'coor_long', 'country_id', 'town_id', 'zip_id', 'h_type_id', 'h_roof_id', 'h_level_id', 'ident_id', 'ident_exp', 'ss_id', 'ss_exp', 'mkt_medium_id', 'mkt_objective_id', 'mkt_post_id', 'mkt_service_id', 'mkt_campaign_id', 'creatged_at');


	public static function SaveLead($info)
	{
        $con        =   New DBSugarTest;
        $connect    =   $con->Conexion();
		$date 		=	date('Y-m-d H:i:s', time());

		$idClient 	=	"";

		foreach ($info as $key => $value) 
		{

	        $query      =   'SELECT email_address_id FROM email_addr_bean_rel WHERE bean_id = "'.$value['id'].'"';
	        $statement  =   $connect->prepare($query);
	        $statement->execute();
	        $dEmail     =   $statement->fetch(PDO::FETCH_ASSOC);

	        if($dEmail)
	        {
				$query      =   'SELECT email_address FROM email_addresses WHERE id = "'.$dEmail['email_address_id'].'"';
		        $statement  =   $connect->prepare($query);
		        $statement->execute();
		        $eEmail     =   $statement->fetch(PDO::FETCH_ASSOC);
		        $Email 		=	($eEmail) ? strtoupper($eEmail['email_address']) : "";
	        }else{
	        	$Email 		=	'';
	        }


			$lead 		=	LeadSuite::create();

			$idClient 	=	($idClient 	== '') ? '10001' : ($idClient + 1);
			$name 		=	$value['first_name'].' '.$value['last_name'];
			$oBirth 	= 	($value['birthdate'] <> '') ? $value['birthdate'] : '';
			$fBirth 	=	($oBirth <> '')	? date("m-d-Y", strtotime($oBirth)) : '';
			$phoneM		=	($value['phone_home'] <> '') ? trim(preg_replace('/[^0-9]+/', '', $value['phone_home'])) : '';
			$phoneA		=	($value['phone_other'] <> '') ? trim(preg_replace('/[^0-9]+/', '', $value['phone_other'])) : '';
			
			$lead->id_c 				=	$value['id'];
			$lead->client_id 			=	$idClient;
			$lead->name 				=	strtoupper($name);
			$lead->birthday 			=	$fBirth;
			$lead->phone_main 			=	$phoneM;
			$lead->phone_main_owner 	=	'';
			$lead->phone_alt 			=	$phoneA;
			$lead->phone_alt_owner 		=	'';
			$lead->phone_other 			=	'';
			$lead->phone_other_owner 	=	'';
			$lead->email_main 			=	$Email;
			$lead->email_other 			=	'';
			$lead->add_main 			=	($value['addprimary'] <> '') ? strtoupper($value['addprimary']) : '';
			$lead->add_postal 			=	($value['addpostal'] <> '') ? strtoupper($value['addpostal']) : '';
			$lead->coor_lati 			=	'';
			$lead->coor_long 			=	'';
			$lead->country_id 			=	'1';
			$lead->town_id 				=	($value['town_list_c'] <> '') ? strtoupper($value['town_list_c']) : '';
			$lead->zip_id 				=	'';
			$lead->h_type_id 			=	($value['housing_tenure_c'] <> '') ? strtoupper($value['housing_tenure_c']) : '';
			$lead->h_roof_id 			=	($value['techo_internet_c'] <> '') ? strtoupper($value['techo_internet_c']) : '';
			$lead->h_level_id 			=	($value['niveles_internet_c'] <> '') ? strtoupper($value['niveles_internet_c']) : '';
			$lead->ident_id 			=	($value['licencia_de_conduccion_c'] <> '') ? trim(preg_replace('/[^0-9]+/', '', $value['licencia_de_conduccion_c'])) : '';
			$lead->ident_exp 			=	'12-31-2019';
			$lead->ss_id 				=	($value['seguro_social_c'] <> '') ? trim(preg_replace('/[^0-9]+/', '', $value['seguro_social_c'])) : '';
			$lead->ss_exp 				=	'12-31-2019';
			$lead->mkt_medium_id 		=	'4';
			$lead->mkt_objective_id 	=	'1';
			$lead->mkt_post_id 			=	'2';
			$lead->mkt_service_id 		=	'12';
			$lead->mkt_campaign_id 		=	'1';
			$lead->creatged_at 			= 	$date;
			$lead->save();

		}

		return true;
	}




 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


}