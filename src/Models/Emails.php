<?php
namespace App\Models;

use Model;

use App\Models\User;
use App\Models\Departament;
use App\Models\Status;
use App\Models\Country;
use App\Models\Emails;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class Emails extends Model 
{

	static $_table 		= 'data_emails';

	Public $_fillable 	= array('id', 'email', 'name', 'departament_id', 'country_id', 'status_id', 'created_at');


	public static function GetEmail()
	{

		$query 	= 	'SELECT * FROM data_emails ORDER BY id DESC';
		$email 	=	DBSmart::DBQueryAll($query);

		$emails = array();

		if($email <> false)
		{
			foreach ($email as $k => $val) 
			{
				$emails[$k] = array(
					'id' 	 			=> 	$val['id'], 
					'email' 	 		=> 	$val['email'],
					'name' 	 			=> 	$val['name'],
					'status' 			=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'			=> 	$val['status_id'],
					'country'			=>	Country::GetCountryById($val['country_id'])['name'],
					'country_id'		=> 	$val['country_id'],
					'departament'		=>	Departament::GetDepById($val['departament_id'])['name'],
					'departament_id'	=> 	$val['departament_id'],
				);
			}
	        
	        return $emails;
		}else{ 	return false;	}

	}

	public static function GetEmailByDep($id)
	{
		
		$query 	= 	'SELECT * FROM data_emails WHERE departament_id = "'.$id.'" AND status_id = "1" ORDER BY id DESC';
		$email 	=	DBSmart::DBQueryAll($query);

		$emails = array();

		if($email <> false)
		{
			foreach ($email as $k => $val) 
			{
				$emails[$k] = array(
					'id' 	 			=> 	$val['id'], 
					'email' 	 		=> 	$val['email'],
					'name' 	 			=> 	$val['name'],
					'status' 			=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'			=> 	$val['status_id'],
					'country'			=>	Country::GetCountryById($val['country_id'])['name'],
					'country_id'		=> 	$val['country_id'],
					'departament'		=>	Departament::GetDepById($val['departament_id'])['name'],
					'departament_id'	=> 	$val['departament_id'],
				);
			}
	        
	        return $emails;
		}else{ 	return false;	}
	}

	public static function GetEmailById($id)
	{
		$query 	= 	'SELECT * FROM data_emails WHERE id="'.$id.'" ORDER BY id DESC';
		$email 	=	DBSmart::DBQuery($query);

		return array(
			'id'           => 	$email['id'],
			'email'        => 	$email['email'],
			'name'        	=> 	$email['name'],
			'status'       => 	$email['status_id'],
			'departament'	=>	$email['departament_id'],
			'country'		=>	$email['country_id']
		);
	}

	public static function SaveEmail($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_e']));

		if($info['type_e'] == 'new')
		{

			$query 	=	'INSERT INTO data_emails(email, name, departament_id, country_id, status_id, created_at) VALUES ("'.$info['email_e'].'", "'.$name.'", "'.$info['depart_id_e'].'", "'.$info['country_id_e'].'", "'.$info['status_id_e'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_e'] == 'edit') 
		{

			$query 	=	'UPDATE data_emails SET email="'.$info['email_e'].'", name="'.$name.'", departament_id="'.$info['depart_id_e'].'", country_id="'.$info['country_id_e'].'", status_id="'.$info['status_id_e'].'" WHERE id = "'.$info['id_e'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}
}
