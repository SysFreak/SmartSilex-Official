<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Departament;

use App\Lib\Config;
use App\Lib\DBSmart;
use \PDO;

class Departament extends Model {

	static $_table 		= 'data_departament';

	Public $_fillable 	= array('name', 'status_id');

	public static function GetDepartament()
	{
		$query 	= 	'SELECT id, name, status_id FROM data_departament ORDER BY id DESC';
		$dep 	=	DBSmart::DBQueryAll($query);

		$depart = array();

		if($dep <> false)
		{
			foreach ($dep as $k => $val) 
			{
				$depart[$k] = array(
					'id' 	 	=> $val['id'], 
					'name' 	 	=> $val['name'], 
					'status' 	=> Status::GetStatusById($val['status_id'])['name'],
					'status_id' => $val['status_id']
				);
			}
	        
	        return $depart;
		}else{ 	return false;	}

	}

   public static function GetID($id)
   {
      $query   =  'SELECT * FROM data_departament WHERE id = "'.$id.'"';
      $val    	=   DBSmart::DBQuery($query);

      return   [
         'id'       => $val['id'],
         'name'     => $val['name'],
         'status'	  => $val['status_id']
      ];  
   
   }
   
	public static function GetDepartamentStatus()
	{
		$query 	= 	'SELECT id, name, status_id FROM data_departament WHERE status_id = "1" ORDER BY id DESC';
		$dep 	=	DBSmart::DBQueryAll($query);
		
		$depart = array();

		if($dep <> false)
		{
			foreach ($dep as $k => $val) 
			{
				$depart[$k] = array(
					'id' 	 	=> $val['id'], 
					'name' 	 	=> $val['name'], 
					'status' 	=>  Status::GetStatusById($val['status_id'])['name'],
					'status_id' => $val['status_id']
				);
			}
	        
	        return $depart;

		}else{ 	return false;	}
	}

	public static function GetDepById($id)
	{
		$query 	= 	'SELECT id, name, status_id FROM data_departament WHERE id = "'.$id.'"';
		$dep 	=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return array(
				'id' => $dep['id'], 'name' => $dep['name'], 'status' => $dep['status_id']
			);

		}else{ 	return false;	}
	}

	public static function GetDepByName($info)
	{
		$query 	= 	'SELECT id, name, status_id FROM data_departament WHERE name = "'.$info.'"';
		$dep 	=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return array('id' => $dep['id'], 'name' => $dep['name'], 'status' => $dep['status_id']);

		}else{
			return false;
		}
	}

	public static function SaveDepartament($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_d']));

		if($info['type_d'] == 'new')
		{
			$query 	= 	'INSERT INTO data_departament(name, status_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_d'].'", "'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['type_d'] == 'edit') 
		{
			$query 	=	'UPDATE data_departament SET name="'.$name.'", status_id="'.$info['status_id_d'].'", created_at="'.$date.'" WHERE id = "'.$info['id_d'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}

}


