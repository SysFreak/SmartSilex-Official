<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Country;
use App\Models\Dvr;

use App\Lib\Config;
use App\Lib\DBSmart;

class Dvr extends Model {

	static $_table 		= 'data_dvr';

	Public $_fillable 	= array('name', 'country_id', 'status_id');

	public static function GetDvr()
	{

		$query 	= 	'SELECT * FROM data_dvr ORDER BY id ASC';
		$dvr 	=	DBSmart::DBQueryAll($query);

		$dvrs 	= 	array();

		if($dvr <> false)
		{
			foreach ($dvr as $k => $val) 
			{
				$dvrs[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $dvrs;
		}else{ 	return false;	}

	}

	public static function GetDvrById($id)
	{

		$query 	= 	'SELECT * FROM data_dvr WHERE id = "'.$id.'"';
		$dvr 	=	DBSmart::DBQuery($query);

		if($dvr <> false)
		{
			return array( 'id' => $dvr['id'], 'name' => $dvr['name'], 'status' => $dvr['status_id'], 'country' => $dvr['country_id']);

		}else{ 	return false;	}

	}

	public static function GetDvrByName($info)
	{

		$query 	= 	'SELECT * FROM data_dvr WHERE name = "'.$info.'"';
		$dvr 	=	DBSmart::DBQuery($query);

		if($dvr <> false)
		{
			return array('id' => $dvr['id'], 'name' => $dvr['name'], 'status' => $dvr['status_id'], 'country' => $dvr['country_id'] );

		}else{ 	return false;	}
	}

	public static function SaveDvr($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_dvr']));

		if($info['type_dvr'] == 'new')
		{
			$query 	= 	'INSERT INTO data_dvr(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_dvr'].'", "'.$info['status_id_dvr'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_dvr'] == 'edit') 
		{
			$query 	=	'UPDATE data_dvr SET name="'.$name.'", country_id="'.$info['country_id_dvr'].'", status_id="'.$info['status_id_dvr'].'" WHERE id = "'.$info['id_dvr'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}

}



