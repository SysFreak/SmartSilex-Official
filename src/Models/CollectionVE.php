<?php
namespace App\Models;

use Model;

use App\Models\Status;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\DBMikroVE;
use App\Lib\UnCypher;

class CollectionVE extends Model 
{

    public static function GetBillsMW()
    {
        $query  =   'SELECT id, idcliente, (SELECT nombre FROM usuarios WHERE id = idcliente) AS nombre, id AS factura, emitido, estado, total FROM facturas WHERE estado = "No pagado" ORDER BY emitido ASC';

        return  DBMikroVE::DBQueryAll($query);
    
    }

    public static function BIllsMWtHtmlRes($iData)
    {
        $_replace   =   new Config();
        $html       =   "";

        $html.='<div class="table-responsive">';
        $html.='<table id="ve_bills" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Factura</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Estado</th>';
        $html.='<th style="text-align: center;">Accion</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;">'.$inf['idcliente'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nombre'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['emitido'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['factura'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['total'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['estado'].'</td>';
                $html.='<td style="text-align: center;">
                            <div class="btn-group">
                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                    Acci&oacute;n <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a onclick="AsiBillPen('.(int)$inf['factura'].');" >Asignar</a></li>
                                    <li><a onclick="ManBillPen('.(int)$inf['factura'].');">Manual</a></li>
                                </ul>
                            </div>
                        </td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';


            
        }
        $html.='</div>';
        
        return  $html;
    
    }

    public static function GetClientsMW()
    {
        $qActive    =   'SELECT id, nombre, estado, telefono, cedula FROM usuarios WHERE estado = "ACTIVO" ORDER BY id ASC';
        $qSuspen    =   'SELECT id, nombre, estado, telefono, cedula FROM usuarios WHERE estado = "SUSPENDIDO" ORDER BY id ASC';

        $iData      =   ['active'   =>  DBMikroVE::DBQueryAll($qActive), 'suspendidos'     =>  DBMikroVE::DBQueryAll($qSuspen)];

        return  $iData;
    
    }

    public static function GetClientsData($id)
    {
        $query    =   'SELECT id, nombre, estado, telefono, cedula FROM usuarios WHERE id = "'.$id.'" ORDER BY id ASC';

        return      DBMikroVE::DBQuery($query);
    
    }

    public static function ClientsMWtHtml($iData)
    {
        $_replace   =   new Config();
        $html       =   "";

        $dt     =   ($iData[0]['estado'] == "ACTIVO") ? "cl_active" : "cl_suspendidos";

        $html.='<div class="table-responsive">';
        $html.='<table id="'.$dt.'" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Telefono</th>';
        $html.='<th style="text-align: center;">C&eacute;dula</th>';
        $html.='<th style="text-align: center;">Accion</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;">'.$inf['id'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nombre'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['telefono'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['cedula'].'</td>';
                $html.='<td style="text-align: center;"><a onclick="ShowInfoClient('.(int)$inf['id'].');" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a></td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }
        $html.='</div>';
        
        return  $html;
    
    }

    public static function ClientsReportsPay()
    {        
        $query  =   'SELECT id, idcliente, (SELECT nombre FROM usuarios WHERE id = idcliente) AS nombre, nfactura, (SELECT total FROM facturas WHERE id = nfactura) AS total, fecha, asunto, transaccion, total AS tTrans, fecha2 AS fReporte, (CASE WHEN estado = "1" THEN "PENDIENTE" WHEN estado = "0" THEN "PROCESADA" ELSE "OTRO" END) AS estado FROM reporte WHERE estado = "1" ORDER BY fecha2 DESC';

        return  DBMikroVE::DBQueryAll($query);
    
    }

    public static function ClientReportId($id)
    {
        $query  =   'SELECT id, idcliente, (SELECT nombre FROM usuarios WHERE id = idcliente) AS nombre, (SELECT total FROM facturas WHERE id = nfactura) AS total, nfactura FROM reporte WHERE id = "'.$id.'"';
        return  DBMikroVE::DBQuery($query);
   
    }

    public static function ClientReportClient($id)
    {
        $query  =   'SELECT id, idcliente, (SELECT nombre FROM usuarios WHERE id = idcliente) AS nombre, (SELECT total FROM facturas WHERE id = nfactura) AS total, nfactura FROM reporte WHERE idcliente = "'.$id.'"';
        return  DBMikroVE::DBQuery($query);
    
    }

    public static function UpdateReportMW($bill)
    {
        $query  =   'UPDATE reporte SET estado="0" WHERE nfactura = "'.$bill.'"';

        return  DBMikroVE::DataExecute($query);
   
    }

    public static function ClientsReporttHtml($iData)
    {
        $_replace   =   new Config();
        $html       =   "";

        $html.='<div class="table-responsive">';
        $html.='<table id="cl_reports_pay" class="table table-bordered" border="1" style="font-size: 10px;">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Id</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Factura</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Codigo</th>';
        $html.='<th style="text-align: center;">Total Trans.</th>';
        $html.='<th style="text-align: center;">Fecha Trans.</th>';
        $html.='<th style="text-align: center;">Accion</th>';
        $html.='</tr>';
        $html.='</thead>';

        if($iData <> false)
        {
            $html.='<tbody >';

            foreach ($iData as $in => $inf) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;">'.$inf['idcliente'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nombre'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['nfactura'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['total'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['fecha'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['transaccion'].'</td>';
                $html.='<td style="text-align: center;">'.$inf['tTrans'].'</td>';
                $html.='<td style="text-align: center;">'.$_replace->ShowDate($inf['fReporte']).'</td>';
                $html.='<td style="text-align: center;">
                        <div class="btn-group">
                            <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                Acci&oacute;n <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a onclick="AsigRepClient('.(int)$inf['id'].');">Asignar</a></li>
                                <li><a onclick="ManRepClient('.(int)$inf['id'].');">Manual</a></li>
                            </ul>
                        </div>
                </td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';

        }else{
            $html.='</tbody>';
            $html.='</table>';
            
        }
        $html.='</div>';
        
        return  $html;
    
    }

    public static function SaverUploadFiles($file)
    {
        $query      = "TRUNCATE TABLE cp_mw_payment_tmp";
        $truncate   = DBSmart::DataExecute($query);

        $csv    =   [];
        $iData  =   [];
        $st     =   0;

        if($file['csv']['type'] == 'application/vnd.ms-excel')
        {
            $name       =   $file['csv']['name'];
            $ext        =   'csv';
            $type       =   $file['csv']['type'];
            $tmpName    =   $file['csv']['tmp_name'];



            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                set_time_limit(0);
                $row = 0;

                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $info   =   $data[0];

                    if( $info == "Date")
                    {
                        $st = 1;
                    }elseif( $info == "Details" ){
                        $st = 2;  
                    }elseif( $info == "Zelle"){
                        $st = 3;  
                    }

                    if($st == 1)
                    {
                        if($row <> 0)
                        {

                            $csv[$row]['Date']          =   ($data[0] <> '')    ? (date("Y-m-d", strtotime($data[0]))) : '';
                            $csv[$row]['Description']   =   ($data[1] <> '')    ? $data[1] : '';
                            $csv[$row]['Amount']        =   ($data[2] <> '')    ? $data[2] : '';

                            $iData[$row]    =   [
                                'date'          =>  ($data[0] <> '')    ? (date("Y-m-d", strtotime($data[0]))) : '',
                                'description'   =>  trim(substr($data[1], (strpos($data[1], '#')+1), (strpos($data[1], ';') - (strpos($data[1], '#')+1)))),
                                'amount'        =>  ($data[2] <> '')    ? $data[2] : ''
                            ];

                            $query      =   'INSERT INTO cp_mw_payment_tmp(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';
                            $insert     =   DBSmart::DataExecute($query);
                        }

                        $row++;

                    }
                    elseif($st == 2)
                    {
                        if($row <> 0)
                        {
                            $csv[$row]['Details']           =   ($data[0] <> '')    ? $data[0] : '';
                            $csv[$row]['Posting Date']      =   ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '';
                            $csv[$row]['Description']       =   ($data[2] <> '')    ? $data[2] : '';
                            $csv[$row]['Amount']            =   ($data[3] <> '')    ? $data[3] : '';
                            $csv[$row]['Type']              =   ($data[4] <> '')    ? $data[4] : '';

                            // $csv[$row]['Check or Slip #']   =   ($data[5] <> '')    ? $data[5] : '';

                            if($csv[$row]['Type'] == "PARTNERFI_TO_CHASE")
                            {
                                $iData[$row]    =   [
                                    'date'          =>  ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '',
                                    'description'   =>  (substr($data[2],-9)),
                                    'amount'        =>  ($data[3] <> '')    ? $data[3] : ''
                                ];

                                $query  =   'INSERT INTO cp_mw_payment_tmp(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';

                                $insert     =   DBSmart::DataExecute($query);
                            }

                            if($csv[$row]['Type'] == "QUICKPAY_CREDIT")
                            {
                                $iData[$row]    =   [
                                    'date'          =>  ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '',
                                    'description'   =>  (substr($data[2],-10)),
                                    'amount'        =>  ($data[3] <> '')    ? $data[3] : ''
                                ];

                                $query  =   'INSERT INTO cp_mw_payment_tmp(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';

                                $insert     =   DBSmart::DataExecute($query);
                            
                            }

                            if($csv[$row]['Type'] == "ACH_CREDIT")
                            {
                                $iData[$row]    =   [
                                    'date'          =>  ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '',
                                    'description'   =>  (substr($data[2],-9)),
                                    'amount'        =>  ($data[3] <> '')    ? $data[3] : ''
                                ];

                                $query  =   'INSERT INTO cp_mw_payment_tmp(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';

                                $insert     =   DBSmart::DataExecute($query);
                            }

                        }

                        $row++;

                    }
                    elseif($st == 3)
                    {

                        if($row <> 0)
                        {

                            $csv[$row]['Zelle']         =   ($data[0] <> '')    ? (date("Y-m-d", strtotime($data[0]))) : '';
                            $csv[$row]['Description']   =   ($data[1] <> '')    ? $data[1] : '';
                            $csv[$row]['Amount']        =   ($data[2] <> '')    ? $data[2] : '';



                            $iData[$row]    =   [
                                'date'          =>  ($data[0] <> '')    ? (date("Y-m-d", strtotime($data[0]))) : '',
                                'description'   =>  trim(substr($data[1], (strpos($data[1], '#')+1), (strpos($data[1], ';') - (strpos($data[1], '#')+1)))),
                                'amount'        =>  ($data[2] <> '')    ? $data[2] : ''
                            ];

                            $query      =   'INSERT INTO cp_mw_payment_tmp(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';
                            $insert     =   DBSmart::DataExecute($query);
                        }

                        $row++;

                    }elseif($st == 0){
                        return false;
                    }

                }
                
                return true;
            
            }else{

                return false;

            }
        
        }else{
            
            return false;
        }
    
    }

    public static function EditAmountCode($iData)
    {
        $query  =   'UPDATE cp_mw_payment_tmp SET amount = "'.$iData['amount'].'" WHERE id = "'.$iData['id'].'"';
        return      DBSmart::DataExecute($query);
    
    }

    public static function InsertTmpPayment($iData)
    {
        $query  =   'INSERT INTO cp_mw_payment_tmp2(fecha, code, amount, type) VALUES ("'.$iData['fecha'].'", "'.$iData['Transaccion'].'", "'.$iData['amount'].'", "'.$iData['status'].'")';

        return  DBSmart::DataExecute($query);
   
    }

    public static function SearchTmpPayment()
    {
        $query  =   'SELECT fecha, code, amount, type FROM cp_mw_payment_tmp2 ORDER BY id DESC';

        return  DBSmart::DBQueryAll($query);
    
    }

    public static function PaymentGeneral($iData)
    {
        $query  =   'UPDATE cp_mw_payment SET amount = "'.$iData['amount'].'" WHERE id = "'.$iData['id'].'"';
        return      DBSmart::DataExecute($query);
    
    }

    public static function  SearchCodesTmp()
    {
        $query  =   'SELECT id, fecha, code, amount FROM cp_mw_payment_tmp ORDER BY id DESC';
        return DBSmart::DBQueryAll($query);
    
    }

    public static function SearchIdConsolidate($id)
    {
        $query  =   'SELECT * FROM cp_mw_consolidate WHERE id = "'.$id.'"';
        return  DBSmart::DBQuery($query);
    
    }

    public static function SearchCodeCons($code)
    {
        $query  =   'SELECT * FROM cp_mw_consolidate WHERE code = "'.$code.'"';
        return  DBSmart::DBQuery($query);
    
    }

    public static function SearchBillCons($code)
    {
        $query  =   'SELECT * FROM cp_mw_consolidate WHERE nfactura = "'.$code.'" AND status_id = "0"';
        return  DBSmart::DBQuery($query);
    
    }

    public static function SearchCodeConsolidate($code)
    {
        $query  =   'SELECT id FROM cp_mw_consolidate WHERE code = "'.$code.'"';
        return  DBSmart::DBQuery($query);
    
    }

    public static function SearchAllConsolidate()
    {
        $query  =   'SELECT * FROM cp_mw_consolidate WHERE status_id = "0" ORDER BY id DESC';
        return  DBSmart::DBQueryAll($query);
    
    }

    public static function SearchAllConsolidateStatus($status)
    {
        $query  =   'SELECT * FROM cp_mw_consolidate WHERE status_id = "'.$status.'" ORDER BY id DESC';
        return  DBSmart::DBQueryAll($query);
    
    }
    
    public static function InsertCodeConsolidate($data, $user)
    {
        $query  =   'INSERT INTO cp_mw_consolidate(idcliente, name, asunto, nfactura, total, fdate, transaccion, code, amount, cdate, status_id, img, type, size, created_by, created_at) VALUES ("0", "", "", "0", "0", "'.date("Y-m-d H:m:s").'", "", "'.$data['code'].'", "'.$data['amount'].'", "'.$data['fecha'].'", "0", "", "", "", "'.$user.'", "'.date("Y-m-d H:m:s").'")';
        return  DBSmart::DataExecute($query);
    
    }

    public static function InsertCodeConsolidateGen($data, $file, $user)
    {
        $query  =   'INSERT INTO cp_mw_consolidate(idcliente, name, asunto, nfactura, total, fdate, transaccion, code, amount, cdate, status_id, img, type, size, created_by, created_at) VALUES ("'.$data['cliente'].'", "'.$data['nombre'].'", "'.$data['asunto'].'", "'.$data['factura'].'", "'.$data['total'].'", "'.$data['emitido'].'", "'.$data['transaccion'].'", "'.$data['code'].'", "'.$data['amount'].'", "'.$data['fecha'].'", "'.$data['status'].'", "'.$file['dir'].'", "'.$file['type'].'", "'.$file['size'].'", "'.$user.'", "'.date("Y-m-d H:m:s").'")';

        return  DBSmart::DataExecute($query);
    
    }

    public static function UpdateCodeConsolidateGen($data)
    {

        $query  =   'UPDATE cp_mw_consolidate SET idcliente="'.$data['client'].'", name="'.$data['name'].'", asunto="'.$data['asunto'].'", nfactura="'.$data['factura'].'", total="'.$data['total'].'", fdate="'.$data['fdate'].'", transaccion="'.$data['code'].'", status_id = "'.$data['status'].'", created_at = NOW() WHERE id = "'.$data['id'].'"';


        return  DBSmart::DataExecute($query);
   
    }

    public static function UpdateCodeConsolidate($data, $file)
    {

        $query  =   'UPDATE cp_mw_consolidate SET idcliente="'.$data['client'].'", name="'.$data['name'].'", asunto="'.$data['asunto'].'", nfactura="'.$data['factura'].'", total="'.$data['total'].'", fdate="'.$data['fdate'].'", transaccion="'.$data['code'].'", status_id = "'.$data['status'].'", img = "'.$file['dir'].'", type = "'.$file['type'].'", size = "'.$file['size'].'" WHERE id = "'.$data['id'].'"';


        return  DBSmart::DataExecute($query);
   
    }

    public static function UpdateConsolidateIdPayment($id)
    {
        $query  =   'UPDATE cp_mw_consolidate SET status_id="2" WHERE id = "'.$id.'"';

        return  DBSmart::DataExecute($query);
   
    }

    public static function UpdateConsolidateStatus($data)
    {
        $query  =   'UPDATE cp_mw_consolidate SET amount="'.$data['amount'].'" WHERE id = "'.$data['id'].'"';

        return  DBSmart::DataExecute($query);
   
    }

    public static function NoteMikrowisp($iData, $status)
    {
        $in      =  '&lt;p&gt;';
        $fn      =  '&lt;/p&gt;';

        switch ($status) 
        {
            case 0:
                $asunto  =  'Asignacion Codigo - Codigo de Transferencia';
                $mensaje =  ''.$in.'Se asigna codigo de Transferencia Bajo los siguientes parametros - Codigo: '.$iData['code'].' - Monto: '.$iData['total'].' - Fecha: '.$iData['fecha'].' - Factura: '.$iData['factura'].' - Operador - '.$iData['user'].' '.$fn.'';
                break;
            
            case 1:
                $asunto  =  'Asignacion Manual - Codigo de Transferencia';
                $mensaje =  ''.$in.'Se asigna codigo de Transferencia Bajo los siguientes parametros - Codigo: '.$iData['code'].' - Monto: '.$iData['total'].' - Fecha: '.$iData['fecha'].' - Factura: '.$iData['factura'].' - Operador - '.$iData['user'].' '.$fn.'';
                break;

            case 2:
                $asunto  =  'Pago de Factura';
                $mensaje =  ''.$in.'Se realiza Pago de factura bajo los siguientes Parametros - Codigo: '.$iData['code'].' - Monto: '.$iData['total'].' - Factura: '.$iData['factura'].' - Operador - '.$iData['user'].' '.$fn.'';
                break;
        }

        $query  =   'INSERT INTO notas(idcliente, asunto, mensaje, fecha, adjuntos, idoperador, imagen, tipo, portal) VALUES ("'.$iData['client'].'", "'.$asunto.'", "'.$mensaje.'", NOW(), "", "44", "", "0", "")';

        return  DBMikroVE::DataExecute($query);

    }

    public static function UploadFilesHtml()
    {

        $query  =   'SELECT id, fecha, code, amount FROM cp_mw_payment_tmp ORDER BY id DESC';
        $iData  =   DBSmart::DBQueryAll($query);

        $html = '';
        $html.='<table id="ve_codes" class="table table-bordered table-striped">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Fecha de Pago</th>';
        $html.='<th style="text-align: center;">C&oacute;digo</th>';
        $html.='<th style="text-align: center;">Monto</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody style="text-align: center;">';

        foreach ($iData as $k => $val) 
        {
            $html.='<tr>';
            $html.='<td>'.$val['fecha'].'</td>';
            $html.='<td>'.$val['code'].'</td>';
            $html.='<td><input style="text-align: center; class="input-sm" type="number" name="amount'.$val['id'].'" id="amount'.$val['id'].'" value="'.$val['amount'].'"></td>';
            $html.='</tr>';
        }
                          
        $html.='</tbody>';
        $html.='</table>';
        return $html;
    
    }

    public static function ConsolidateHtml($iData)
    {

        $info   =   ($iData <> false) ? 'id="ve_consolidate"' : '';

        $html = '';
        $html = '<form id="PendingLotCode">';
        $html.='<table '.$info.' class="table table-bordered table-striped">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;"></th>';
        $html.='<th style="text-align: center;">ID</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Factura</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Transacci&oacute;n</th>';
        $html.='<th style="text-align: center;">Monto</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Status</th>';
        $html.='<th style="text-align: center;">File</th>';
        $html.='<th style="text-align: center;">Acci&oacute;n</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody style="text-align: center;">';

        if($iData <> false)
        {
            foreach ($iData as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;"><label class="checkbox"><input type="checkbox" name="CheckConso'.$val['id'].'" id="CheckConso'.$val['id'].'"><i></i></label></td>';
                $html.='<td>'.$val['idcliente'].'</td>';
                $html.='<td>'.$val['name'].'</td>';
                $html.='<td>'.$val['nfactura'].'</td>';
                $html.='<td>'.$val['total'].'</td>';
                $html.='<td>'.$val['fdate'].'</td>';
                $html.='<td>'.$val['code'].'</td>';
                $html.='<td>'.$val['amount'].'</td>';
                $html.='<td>'.$val['cdate'].'</td>';
                $html.='<td><span class="label label-info">Por Procesar</span></td>';

                if($val['img'] <> "")
                {
                    $html.='<td><a target="_blank" href="../../'.$val['img'].'" class="btn btn-xs btn-xs btn-default"><i class="fa fa-eye"></i></a></td>';
                }else{ $html.='<td></td>';   }

                $html.='<td style="text-align: center;">
                            <div class="btn-group">
                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                    Acci&oacute;n <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a onclick="EditConsProc('.(int)$val['id'].');">Editar</a></li>
                                    <li><a onclick="ProcConsProc('.(int)$val['id'].');">Procesar</a></li>
                                </ul>
                            </div>
                        </td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';
            $html.='</form>';
        }else{
            $html.='<tr>';
            $html.='<td colspan="12">Sin Informaci&oacute;n para Mostar</td>';
            $html.='</tr>';
            $html.='</tbody>';
            $html.='</table>';
            $html.='</form>';
        }
        return $html;
    
    }

    public static function ConsolidateHtmlPend($iData)
    {

        $info   =   ($iData <> false) ? 'id="ve_cons_pend"' : '';
        
        $html = '';
        $html.='<table '.$info.' class="table table-bordered table-striped">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Transacci&oacute;n</th>';
        $html.='<th style="text-align: center;">Monto</th>';
        $html.='<th style="text-align: center;">Status</th>';
        $html.='<th style="text-align: center;">Acci&oacute;n</th>';
        $html.='<th style="text-align: center;">Cargado</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody style="text-align: center;">';

        if($iData <> false)
        {
            foreach ($iData as $k => $val) 
            {

                $html.='<tr>';
                $html.='<td>'.$val['cdate'].'</td>';
                $html.='<td>'.$val['code'].'</td>';
                $html.='<td>'.$val['amount'].'</td>';
                $html.='<td><span class="label label-warning">Pendiente</span></td>';
                $html.='<td style="text-align: center;">
                            <div class="btn-group">
                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                    Acci&oacute;n <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a onclick="AsigCode('.(int)$val['id'].');">Asignar</a></li>
                                </ul>
                            </div>
                        </td>';
                $html.='<td>'.$val['created_at'].'</td>';
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';
            $html.='</form>';
        }else{
            $html.='<tr>';
            $html.='<td colspan="5">Sin Informaci&oacute;n para Mostar</td>';
            $html.='</tr>';
            $html.='</tbody>';
            $html.='</table>';
        }
        return $html;
    
    }

    public static function ConsolidateHtmlPendAsig($iData)
    {
        $html = '';
        $html.='<div class="modal-body" style="background: aliceblue;">';
        $html.='<form id="ConPendAsig">';
        $html.='<div class="row">';
        $html.='<div class="form-group">';
        $html.='<div class="input-group">';
        $html.='<input class="form-control" type="number" name="iSearchAsig" id="iSearchAsig" placeholder="Id de Cliente">';
        $html.='<span class="input-group-addon"><a onclick="SearchMWClient()"; class="btn btn-primary btn-xs">Buscar</a></span>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
        $html.='<div class="form-group">';
        $html.='<div class="col-md-6" style="margin-top: 10px;">';
        $html.='<label><strong> Nombre del Cliente</strong></label>';
        $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_name" name="mw_name" placeholder="Nombre de Cliente" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-3" style="margin-top: 10px;">';
        $html.='<label><strong> Id</strong></label>';
        $html.='<input style="text-align: center;" class="form-control" type="number" id="mw_id" name="mw_id" placeholder="ID Mikrowisp" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-3" style="margin-top: 10px;">';
        $html.='<div id="mw_bills_option">';
        $html.='<label><strong> Facturas</strong></label>';
        $html.='<select class="form-control" id="mw_bills" name="mw_bills" required="" readonly>';
        $html.='<option value="">FACTURAS</option>';
        $html.='</select>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="form-group">';
        $html.='<div class="col-md-6" style="margin-top: 10px;">';
        $html.='<label><strong> Code</strong></label>';
        $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_code" name="mw_code" value="'.$iData['code'].'" placeholder="C&oacute;digo" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-3" style="margin-top: 10px;">';
        $html.='<label><strong> Total</strong></label>';
        $html.='<input style="text-align: center;" class="form-control" type="number" id="mw_total" name="mw_total" value="'.$iData['amount'].'" placeholder="Total" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-3" style="margin-top: 10px;">';
        $html.='<label><strong> Fecha</strong></label>';
        $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_date" name="mw_date" value="'.$iData['cdate'].'" placeholder="Fecha" readonly>';
        $html.='</div>';
        $html.='</div>';

        $html.='<div class="form-group">';
        $html.='<div class="col-md-12" style="margin-top: 10px;">';
          $html.='<label><strong> Adjunto</strong></label>';
          $html.='<input type="file" class="form-control" id="file" name="file" />';
        $html.='</div>';
        $html.='</div>';

        $html.='</div>';
        $html.='</form>';
        $html.='</div>';
        $html.='<div class="modal-footer">';
        $html.='<a onclick="UpdateAsigned();" class="btn btn-success">Actualizar</a>';
        $html.='<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        $html.='</div>';

        return $html;    
    
    }

    public static function ProcessHtml($iData)
    {
        $info   =   ($iData <> false) ? 'id="ve_process"' : '';

        $html = '';
        $html.='<table '.$info.' class="table table-bordered table-striped">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">ID</th>';
        $html.='<th style="text-align: center;">Cliente</th>';
        $html.='<th style="text-align: center;">Factura</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Transacci&oacute;n</th>';
        $html.='<th style="text-align: center;">Monto</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Status</th>';
        $html.='<th style="text-align: center;">File</th>';
        $html.='<th style="text-align: center;">Procesados</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody style="text-align: center;">';

        if($iData <> false)
        {
            foreach ($iData as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td>'.$val['idcliente'].'</td>';
                $html.='<td>'.$val['name'].'</td>';
                $html.='<td>'.$val['nfactura'].'</td>';
                $html.='<td>'.$val['total'].'</td>';
                $html.='<td>'.$val['fdate'].'</td>';
                $html.='<td>'.$val['code'].'</td>';
                $html.='<td>'.$val['amount'].'</td>';
                $html.='<td>'.$val['cdate'].'</td>';
                $html.='<td><span class="label label-success">Procesados</span></td>';
                if($val['img'] <> "")
                {
                    $html.='<td><a target="_blank" href="../../'.$val['img'].'" class="btn btn-xs btn-xs btn-default"><i class="fa fa-eye"></i></a></td>';
                }else{ $html.='<td></td>';   }
                $html.='<td>'.$val['created_at'].'</td>';
                
                $html.='</tr>';
            }

            $html.='</tbody>';
            $html.='</table>';
        }else{
            $html.='<tr>';
            $html.='<td colspan="10">Sin Informaci&oacute;n para Mostar</td>';
            $html.='</tr>';
            $html.='</tbody>';
            $html.='</table>';
        }
        return $html;
    
    }

    public static function EditConsolidateHTML($iData)
    {
        $html   =   '';

        $html.='<div class="modal-body" style="background: aliceblue;">';
        $html.='<form>';
        $html.='<div id="AlertEditConso" style="text-align: center; background: aliceblue; margin-top: 10px;"></div>';
        $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
        $html.='<div class="form-group">';
        $html.='<div class="col-md-6" style="margin-top: 10px;">';
        $html.='<label><strong> Nombre del Cliente</strong></label>';
        $html.='<input value="'.$iData['name'].'" style="text-align: center;" class="form-control" type="text" id="co_name" name="co_name" placeholder="Nombre de Cliente" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-6" style="margin-top: 10px;">';
        $html.='<label><strong> Id</strong></label>';
        $html.='<input value="'.$iData['idcliente'].'" style="text-align: center;" class="form-control" type="number" id="co_id" name="co_id" placeholder="Identificador Mikrowisp" readonly>';
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="form-group" >';
        $html.='<div class="col-md-4" style="margin-top: 10px;">';
        $html.='<label><strong> Factura</strong></label>';
        $html.='<input value="'.$iData['nfactura'].'" style="text-align: center;" class="form-control" type="number" id="co_bill" name="co_bill" placeholder="N&uacute;mero de Factura" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-4" style="margin-top: 10px;">';
        $html.='<label><strong> Total</strong></label>';
        $html.='<input value="'.$iData['total'].'" style="text-align: center;" class="form-control" type="number" id="co_total" name="co_total" placeholder="Monto de Factura" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-4" style="margin-top: 10px;">';
        $html.='<label><strong> Fecha</strong></label>';
        $html.='<input value="'.$iData['fdate'].'" style="text-align: center;" class="form-control" type="text" id="cd_fecha" name="cd_fecha" placeholder="Fecha de Emisi&oacute;n" readonly>';
        $html.='</div>';
        $html.='</div>';
        $html.='<div class="form-group">';
        $html.='<div class="col-md-4" style="margin-top: 10px;">';
        $html.='<label><strong> Transacci&oacute;n</strong></label>';
        $html.='<input value="'.$iData['code'].'" style="text-align: center;" class="form-control" type="text" id="co_code" name="co_code" placeholder="C&oacute;digo de Transacci&oacute;n" readonly>';
        $html.='</div>';
        $html.='<div class="col-md-4" style="margin-top: 10px;">';
        $html.='<label><strong> Monto</strong></label>';
        $html.='<input value="'.$iData['amount'].'" style="text-align: center;" class="form-control" type="number" id="co_amount" name="co_amount" placeholder="Monto Transacci&oacute;n">';
        $html.='</div>';
        $html.='<div class="col-md-4" style="margin-top: 10px;">';
        $html.='<label><strong> Fecha</strong></label>';
        $html.='<input value="'.$iData['cdate'].'" style="text-align: center;" class="form-control" type="text" id="co_date" name="co_date" placeholder="Fecha de Transacci&oacute;n" readonly>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</form>';
        $html.='</div>';
        $html.='<div class="modal-footer">';
        $html.='<a onclick="UpdateCons('.$iData['id'].');" class="btn btn-success">Actualizar</a>';
        $html.='<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        $html.='</div>';

        return $html;

    }

    public static function AsigRepClientHtml($client, $iData)
    {
        $html = '';
        $html.='<div class="modal-body" style="background: aliceblue;">';
          $html.='<form id="FormAsigRepClient">';
            $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
                $html.='<div id="AlertAsigRepClient" style="text-align: center; background: aliceblue; margin-top: 10px;"></div>';
            $html.='</div>';
            $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
              $html.='<div class="form-group">';
                $html.='<div class="col-md-6" style="margin-top: 10px;">';
                  $html.='<label><strong> Nombre del Cliente</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_name" name="mw_name" value="'.$client['nombre'].'" placeholder="Nombre de Cliente" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-3" style="margin-top: 10px;">';
                  $html.='<label><strong> Id</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="number" id="mw_id" name="mw_id" value="'.$client['idcliente'].'" placeholder="ID Mikrowisp" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-3" style="margin-top: 10px;">';
                  $html.='<label><strong> Factura</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="number" id="mw_bill" name="mw_bill" value="'.$client['nfactura'].'" placeholder="ID Factura" readonly>';
                $html.='</div>';
              $html.='</div>';
            $html.='</div>';
            $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
              $html.='<table id="SearchRepClient" class="table table-bordered table-striped">';
                $html.='<thead>';
                  $html.='<tr>';
                    $html.='<th style="text-align: center;"></th>';
                    $html.='<th style="text-align: center;">Fecha</th>';
                    $html.='<th style="text-align: center;">Transacci&oacute;n</th>';
                    $html.='<th style="text-align: center;">Monto</th>';
                  $html.='</tr>';
                $html.='</thead>';
                $html.='<tbody style="text-align: center;">';
                foreach ($iData as $k => $val) 
                {
                    $html.='<tr>';
                    $html.='<td style="text-align: center;"><label class="checkbox"><input type="checkbox" name="CheckAsigRep'.$val['id'].'" id="CheckAsigRep'.$val['id'].'"><i></i></label></td>';
                    $html.='<td>'.$val['cdate'].'</td>';
                    $html.='<td>'.$val['code'].'</td>';
                    $html.='<td>'.$val['amount'].'</td>';
                  $html.='</tr>';   
                }
                $html.='</tbody>';
              $html.='</table>';
            $html.='</div>';

            $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
                $html.='<div class="form-group">';
                    $html.='<div class="col-md-12" style="margin-top: 10px;">';
                      $html.='<label><strong> Adjunto</strong></label>';
                      $html.='<input type="file" class="form-control" id="file" name="file" />';
                    $html.='</div>';
                $html.='</div>';
            $html.='</div>';

          $html.='</form>';
        $html.='</div>';
        $html.='<div class="modal-footer">';
          $html.='<a onclick="UpdateAsigRepClient();" class="btn btn-success">Actualizar</a>';
          $html.='<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        $html.='</div>';
        return $html;
    
    }

    public static function ShowManRepClientHtml($iData)
    {
        $html='';

        $html.='<div class="modal-body" style="background: aliceblue;">';
          $html.='<form id="FormPendAsig" enctype="multipart/form-data" method="post">';
          $html.='<div id="AlertPendAsig" style="text-align: center;"></div>';
            $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
              $html.='<div class="form-group">';
                $html.='<div class="col-md-6" style="margin-top: 10px;">';
                  $html.='<label><strong> Nombre del Cliente</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_name" name="mw_name" value="'.$iData['nombre'].'" placeholder="Nombre de Cliente" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-3" style="margin-top: 10px;">';
                  $html.='<label><strong> Id</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="number" id="mw_id" name="mw_id" value="'.$iData['idcliente'].'" placeholder="ID Mikrowisp" readonly>';
                $html.='</div>';
                $html.='<div class="col-md-3" style="margin-top: 10px;">';
                  $html.='<label><strong> Factura</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_bills" name="mw_bills" value="'.$iData['nfactura'].'" placeholder="Factura" readonly>';
                $html.='</div>';
              $html.='</div>';
            $html.='</div>';
            $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
              $html.='<div class="form-group">';
                $html.='<div class="col-md-6" style="margin-top: 10px;">';
                  $html.='<label><strong> C&oacute;digo de Transferencia</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_code" name="mw_code" placeholder="C&oacute;digo de Transferencia">';
                $html.='</div>';
                $html.='<div class="col-md-3" style="margin-top: 10px;">';
                  $html.='<label><strong> Monto</strong></label>';
                  $html.='<input style="text-align: center;" class="form-control" type="number" id="mw_amount" name="mw_amount" placeholder="Monto">';
                $html.='</div>';
                $html.='<div class="col-md-3" style="margin-top: 10px;">';
                  $html.='<div id="mw_bills_option">';
                    $html.='<label><strong> Fecha</strong></label>';
                    $html.='<input style="text-align: center;" class="form-control" type="text" id="mw_date" name="mw_date" placeholder="Fecha ">';
                  $html.='</div>';
                $html.='</div>';
              $html.='</div>';
            $html.='</div>';

            $html.='<div class="row" style="text-align: center; background: aliceblue; margin-top: 10px;">';
              $html.='<div class="form-group">';
                $html.='<div class="col-md-12" style="margin-top: 10px;">';
                  $html.='<label><strong> Adjunto</strong></label>';
                  $html.='<input type="file" class="form-control" id="file" name="file" />';
                $html.='</div>';
              $html.='</div>';
            $html.='</div>';
        $html.='</div>';
        $html.='<div class="modal-footer">';
        $html.='<a onclick="ProcessPendAsigned();" class="btn btn-success">Actualizar</a>';
        $html.='<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        $html.='</form>';
        $html.='</div>';

        return $html;
    
    }

    public static function ShowInfoClientHml($info, $serv, $bills)
    {
        $html = '';

        $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        $html.='<div class="row" style="margin-top: 10px;">';
        $html.='<article class="col-sm-12">';
        $html.='<div class="jarviswidget" id="wid-id-0"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">';
        $html.='<header>';
        $html.='<ul class="nav nav-tabs pull-right in" id="myTab">';
        $html.='<li class="active"><a data-toggle="tab" href="#mcl1"><i class="fa fa-lg fa-warning"></i> <span class="hidden-mobile hidden-tablet"> Resumen </span></a></li>';
        $html.='<li><a data-toggle="tab" href="#mcl2"><i class="fa fa-lg fa-history"></i> <span class="hidden-mobile hidden-tablet"> Servicios </span></a></li>';
        $html.='<li><a data-toggle="tab" href="#mcl3"><i class="fa fa-lg fa-history"></i> <span class="hidden-mobile hidden-tablet"> Facturas </span></a></li>';
        $html.='</ul>';
        $html.='</header>';
        $html.='<div class="no-padding">';
        $html.='<div class="widget-body">';
        $html.='<div class="tab-content ">';
        $html.='<div class="tab-pane active" id="mcl1">';
        if($info <> false)
        {
            $html.='<table class="table table-bordered" border="1" style="margin-top: 20px;">';
            $html.='<tbody>';
            $html.='<tr>';
            $html.='<td>Id</td>';
            $html.='<td>'.$info['id'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
            $html.='<td>Nombre</td>';
            $html.='<td>'.$info['nombre'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
            $html.='<td>Direccion</td>';
            $html.='<td>'.$info['direccion_principal'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
            $html.='<td>Telefono</td>';
            $html.='<td>'.$info['telefono'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
            $html.='<td>Telefono</td>';
            $html.='<td>'.$info['movil'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
            $html.='<td>Email</td>';
            $html.='<td>'.$info['correo'].'</td>';
            $html.='</tr>';
            $html.='</tbody>';
            $html.='</table>';
        }else{
            $html.='table class="table table-bordered" border="1" style="margin-top: 20px;">';
            $html.='tbody>';
            $html.='tr>';
            $html.='td>Sin Informacion de Cliente Para Mostrar</td>';
            $html.='/tr>';
            $html.='/tbody>';
            $html.='/table>';
        }
        $html.='</div>';

        $html.='<div class="tab-pane" id="mcl2">';
        if($serv <> false)
        {
        $html.='<table class="table table-bordered" border="1" style="margin-top: 20px;">';
        $html.='<tbody>';
        $html.='<tr>';
        $html.='<td>Id</td>';
        $html.='<td>Plan</td>';
        $html.='<td>Costo</td>';
        $html.='<td>IP</td>';
        $html.='</tr>';
          foreach ($serv as $k => $val) 
          {
              $html.='<tr>';
              $html.='<td>'.$val['id'].'</td>';
              $html.='<td>'.$val['plan'].'</td>';
              $html.='<td>'.$val['costo'].'</td>';
              $html.='<td>'.$val['ip'].'</td>';
              $html.='</tr>';
          }
        $html.='</tbody>';
        $html.='</table>';
        }else{
            $html.='<table class="table table-bordered" border="1" style="margin-top: 20px;">';
            $html.='<tbody>';
            $html.='<tr>';
            $html.='<td>Sin Informacion de Servicio Para Mostrar</td>';
            $html.='</tr>';
            $html.='</tbody>';
            $html.='</table>';
        }
        $html.='</div>';

        $html.='<div class="tab-pane" id="mcl3">';
        if($bills <> false)
        {
        $html.='<table id="BillsClients" class="table table-bordered table-striped">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th style="text-align: center;">Factura</th>';
        $html.='<th style="text-align: center;">Fiscal</th>';
        $html.='<th style="text-align: center;">Fecha</th>';
        $html.='<th style="text-align: center;">Estado</th>';
        $html.='<th style="text-align: center;">Total</th>';
        $html.='<th style="text-align: center;">Pagado</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody style="text-align: center;">';
        foreach ($bills as $k => $val) 
        {
            $html.='<tr>';
            $html.='<td>'.$val['factura'].'</td>';
            $html.='<td>'.$val['fiscal'].'</td>';
            $html.='<td>'.$val['emitido'].'</td>';
            $html.='<td>'.$val['estado'].'</td>';
            $html.='<td>'.$val['total'].'</td>';
            $html.='<td>'.$val['pago'].'</td>';
            $html.='</tr>';
        }

        $html.='</tbody>';
        $html.='</table>';
        }else{
            $html.='<table class="table table-bordered" border="1" style="margin-top: 20px;">';
            $html.='<tbody>';
            $html.='<tr>';
            $html.='<td>Sin Informacion de Facturas Para Mostrar</td>';
            $html.='</tr>';
            $html.='</tbody>';
            $html.='</table>';                            
        }

        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</article>';
        $html.='</div>';
        $html.='</div>';
        return $html;
    
    }

    public static function NoteMassive($iData)
    {
        $_replace   =   new Config;

        $in      =  '&lt;p&gt;';
        $fn      =  '&lt;/p&gt;';

        $asunto     =   $iData['title'];
        $mensaje    =   ''.$in.' '.$iData['note'].' '.$fn.'';

        $query  =   'INSERT INTO notas(idcliente, asunto, mensaje, fecha, adjuntos, idoperador, imagen, tipo, portal) VALUES ("'.$iData['client'].'", "'.$asunto.'", "'.$mensaje.'", NOW(), "", "1", "", "0", "")';

        return  DBMikroVE::DataExecute($query);

    }


}