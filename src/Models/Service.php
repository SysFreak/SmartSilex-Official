<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Service extends Model {

	static $_table = 'data_services';
	
	Public $_fillable = array('name', 'country_id', 'status_id');

	public static function GetService()
	{
		$query 	= 	'SELECT id, name, status_id, country_id FROM data_services ORDER BY id ASC';
		$serv 	=	DBSmart::DBQueryAll($query);

		$service = array();

		if($serv <> false)
		{
			foreach ($serv as $k => $val) 
			{
				$service[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $service;
		}else{ 	return false;	}

	}

	public static function ServiceById($id)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_services WHERE id = "'.$id.'"';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return array(
				'id' => $serv['id'], 'name' => $serv['name'], 'status' => $serv['status_id'], 'country' => $serv['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetServiceByCountry($id)
	{
		$query 	= 	'SELECT id, name, country_id, status_id FROM data_services WHERE country_id = "'.$id.'" AND id <> "1" ORDER BY id ASC';
		$ser 	=	DBSmart::DBQueryAll($query);

		if($ser <> false)
		{
			foreach ($ser as $k => $val) 
			{
				$service[$k] = [
					'id' 		=> $val['id'],
					'name' 		=> $val['name'],
					'status' 	=> Status::GetStatusById($val['status_id'])['name'],
					'country' 	=> Country::GetCountryById($val['country_id'])['name']
				];
			}
	        
	        return $service;

		}else{ return false; }
	}

	public static function ServiceByName($info)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_services WHERE name = "'.$info.'"';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return array(
				'id' => $serv['id'], 'name' => $serv['name'], 'status' => $serv['status_id'], 'country' => $serv['country_id']
			);

		}else{ 	return false;	}
	}

	public static function Service($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name']));

		if($info['type'] == 'new')
		{
			$query 	= 	'INSERT INTO data_services(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id'].'", "'.$info['status_id'].'", NOW())';
			$cou 	=	DBSmart::DataExecute($query);

			return ($cou <> false ) ? true : false;
		}
		elseif ($info['type'] == 'edit') 
		{
			$query 	=	'UPDATE data_services SET name="'.$name.'", country_id = "'.$info['country_id'].'", status_id="'.$info['status_id'].'" WHERE id = "'.$info['id'].'"';
			$cou 	=	DBSmart::DataExecute($query);

			return ($cou <> false) ? true : false;
		}
	}

	public static function ServiceHTML($info)
	{
		$html 	=	"";

		switch ($info) 
		{
			case $info == "2":
				$html 	= '<span class="label label-primary">Television</span>';
				break;
			
			case $info == "3":
				$html 	= '<span class="label label-danger">Security</span>';
				break;

			case $info == "4":
				$html 	= '<span class="label label-success">Internet</span>';
				break;

			case $info == "5":
				$html 	= '<span class="label label-warning">Wireless</span>';
				break;
		}

		return $html;

	}
}