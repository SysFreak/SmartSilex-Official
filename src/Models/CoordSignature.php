<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CoordSignature extends Model
{

	static $_table 		= 'data_coord_sig';

	Public $_fillable 	= array('id', 'name', 'status_id', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SaveSignature($info)
	{
		
		$date	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO data_coord_sig(name, status_id, created_at) VALUES ("'.$info['name'].'", "'.$info['status'].'", "'.$date.'") ';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? $dep : false;
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetSignature()
	{

		$query 	=	'SELECT id, name FROM data_coord_sig WHERE status_id = "1" ORDER BY ID ASC';

		$sig 	=	DBSmart::DBQueryAll($query);

		if($sig <> false)
		{
			foreach ($sig as $s => $sal) 
			{
				$sign[$s]	=	[
					'id'	=>	$sal['id'],
					'name'	=>	$sal['name']
				];
			}

			return $sign;

		}else{
			return false;
		}

		return ($dep <> false ) ? $dep : false;
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

}