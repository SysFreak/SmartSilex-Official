<?php
namespace App\Models;

use Model;

use App\Models\ServiceMRouterPr;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\Referred;
use App\Models\User;
use App\Models\Payment;
use App\Models\StatusService;
use App\Models\WirelessPlan;
use App\Models\LeadsProvider;

use App\Lib\Config;
use App\Lib\DBSmart;
use PDO;

class ServiceMRouterPr extends Model 
{

	static $_table = 'cp_service_pr_mr';
	
	Public $_fillable = array('ticket', 'client_id', 'cant', 'ssid', 'red_id', 'password', 'payment_id', 'service_id', 'type_order_id', 'origen_id', 'operator_id', 'created_at');

	public static function GetTicket()
	{
		$query 	= 	'SELECT ticket FROM cp_service_pr_mr ORDER BY ticket DESC';
		$tic 	=	DBSmart::DBQuery($query);

		if($tic <> false)
		{
			$tick = (substr($tic['ticket'], 2, strlen($tic['ticket'])) + 1);
			return "MR".$tick;
		}else{
			return "MR10001";
		}
	
	}

	public static function GetServiceByTicket($ticket)
	{
		$query 	= 	'SELECT * FROM cp_service_pr_mr  WHERE ticket = "'.$ticket.'" ORDER BY id DESC';
		$serv 	=	DBSmart::DBQuery($query);		

		if($serv <> false)
		{	
			return [
				'ticket'		=>	$serv['ticket'],
				'client'		=>	$serv['client_id'],
				'cant'			=>	$serv['cant'],
				'ssid'			=>	$serv['ssid'],
				'red'			=>	$serv['red_id'],
				'password'		=>	$serv['password'],
				'payment'		=>	$serv['payment_id'],
				'service'		=>	$serv['service_id'],
				'order_type'	=>	$serv['type_order_id'],
				'origen'		=>	$serv['origen_id'],
				'owen'			=>	$serv['owen_id'],
				'assit'			=>	$serv['assit_id'],
				'created'		=>	$serv['created_at']
			];

		}else{ 	return false; 	}

	}

	public static function GetService($ticket)
	{

        $query 		=	'SELECT t1.id, t1.ticket, t1.created_at, t1.client_id, t3.name, t3.phone_main as phone, t1.service_id, t5.name as service, (SELECT username FROM users WHERE id = t2.operator_id) as sale, t6.name as departament, t1.status_id, (SELECT name FROM data_score WHERE id = t1.score) AS score, (SELECT IF(t1.status_id = 1, "PENDIENTE", "PROCESADO")) AS status, (SELECT username FROM users WHERE id = t1.operator_id) AS operator, t2.created_at AS date_process FROM cp_appro_serv AS t1 INNER JOIN cp_service_pr_mr AS t2 ON (t1.ticket = t2.ticket) INNER JOIN cp_leads as t3 ON (t1.client_id = t3.client_id) INNER JOIN users as t4 ON (t2.operator_id = t4.id) INNER JOIN data_services AS t5 ON (t1.service_id = t5.id) INNER JOIN data_departament as t6 ON (t4.departament_id = t6.id) INNER JOIN cp_appro_score AS t7 ON (t1.score_id = t7.id) AND t1.ticket = "'.$ticket.'"';

		$result 	=	DBSmart::DBQuery($query);

        if($result <> false)
        {
        	$staHtml = ($result['status_id'] == 1) 	? 
        		'<span class="label label-warning">'.$result['status'].'</span>': 
        		'<span class="label label-success">'.$result['status'].'</span>';

			return	[
				'id'			=>	$result['id'],
	    		'ticket'		=>	$result['ticket'],
	    		'created'		=>	$result['created_at'],
	    		'client'		=>	$result['client_id'],
	    		'name'			=>	$result['name'],
	    		'phone'			=>	$result['phone'],
	    		'serv'			=>	$result['service_id'],
	    		'service'		=>	$result['service'],
	    		'servHtml'		=>	'<span class="label label-success">'.$result['service'].'</span>',
	    		'sale'			=>	$result['sale'],
	    		'departament'	=>	$result['departament'],
	    		'status'		=>	$result['status'],
	    		'statusHtml'	=>	$staHtml,
	    		'score'			=>	$result['score'],
	    		'operator'		=>	$result['operator'],
	    		'date_process'	=>	$result['date_process']
	    	];
        }else{
        	return false;
        }
	
	}

	public static function GetSubmit($info, $user)
	{
		$query 	=	'SELECT * FROM cp_service_pr_mr WHERE ticket = "'.$info.'"';
		$sub 	=	DBSmart::DBQuery($query);

		if($sub <> false)
		{
			if($sub['sup_id'] == 0)
			{$pro 	=	$user; $apo 	=	$user; }
			else
			{ $pro 	=	User::GetUserById($sub['sup_ope_id'])['username']; $apo 	=	$user;}

			$data =	[
				'ticket'		=>	$sub['ticket'],
				'client'		=>	$sub['client_id'],
				'cant'			=>	($sub['cant'] <> 0) 		? $sub['cant'] : "0",
				'red'			=>	($sub['red_id']<> 0) 		? WirelessPlan::GetWirelessById($sub['red_id'])['name'] : "NO",
				'ssid'			=>	($sub['ssid'] <> "")  		? $sub['ssid'] : "",
				'password'		=>	($sub['password'] <> 0) 	? $sub['password'] : "",
				'ref'			=>	($sub['ref_id'] == 1) 		? "SI" : "NO",
				'ref_name'		=>	($sub['ref_id'] == 1) 		? Referred::GetRefId($sub['ref_list_id'])['name'] : "NO",
				'ref_client'	=>	($sub['ref_id'] == 1) 		? Referred::GetRefId($sub['ref_list_id'])['ref_id'] : "NO",
				'sup'			=> 	($sub['sup_id'] == 1) 		? "SI" : "NO",
				'sup_pro'		=>	$pro,	
				'sup_apo'		=>	$apo,
				'payment'		=>	Payment::GetPaymentById($sub['payment_id'])['name'],
				'service'		=>	StatusService::GetStatusById($sub['service_id'])['name'],
				'order_type'	=>	($sub['type_order_id'])		? $sub['type_order_id'] 											: 	"",
				'o_type'		=>	($sub['type_order_id'])		? OrderType::GetOrderTypeById($sub['type_order_id'])['name'] 		: 	"",
				'origen'		=>	($sub['origen_id'])			? $sub['origen_id'] 												: 	"",
				't_origen'		=>	($sub['origen_id'])			? LeadsProvider::GetLeadsProviderById($sub['origen_id'])['name'] 	: 	"",
				'created'		=>	$sub['created_at']
			];
			return $data;
		}else{
			return false;
		}

	}

	public static function SaveMRouter($ticket, $info, $date)
	{

		$user =	($info['sup'] == 0) ? $info['operator'] : $info['sup_list'];

		$query =	'INSERT INTO cp_service_pr_mr(ticket, client_id, cant, ssid, red_id, password, payment_id, service_id, type_order_id, origen_id, ref_id, ref_list_id, sup_id, sup_ope_id, operator_id, created_at) VALUES ("'.$ticket.'", "'.$info['client'].'", "'.$info['cant'].'", "'.$info['ssid'].'", "'.$info['red'].'", "'.$info['password'].'", "'.$info['payment'].'", "'.$info['service'].'", "'.$info['order_type'].'", "'.$info['origen'].'", "'.$info['ref'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$user.'", "'.$date.'")';

		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;	
	}
}