<?php

namespace App\Models;

use Model;

class Logger extends Model {

	static $_table = 'log';

	Public $_fillable = array('channel', 'client', 'message', 'time', 'ip', 'username');
}