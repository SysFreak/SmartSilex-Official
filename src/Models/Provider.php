<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Provider;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Provider extends Model {

	static $_table 		= 'data_provider';

	Public $_fillable 	= array('id', 'name', 'country_id', 'status_id');

	public static function GetProvider()
	{
		$query 	= 	'SELECT * FROM data_provider ORDER BY id ASC';
		$prov 	=	DBSmart::DBQueryAll($query);

		$provider = array();

		if($prov <> false)
		{
			foreach ($prov as $k => $val) 
			{
				$provider[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $provider;

		}else{ 	return false;	}
	}

	public static function GetProviderById($id)
	{

		$query 	= 	'SELECT * FROM data_provider WHERE id = "'.$id.'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack <> false)
		{
			return array('id' => $pack['id'], 'name' => $pack['name'], 'status' => $pack['status_id'], 'country' => $pack['country_id']);

		}else{ 	return false;	}

	}

	public static function GetProviderByName($info)
	{

		$query 	= 	'SELECT * FROM data_provider WHERE name = "'.$info.'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack <> false)
		{
			return array('id' => $pack['id'], 'name' => $pack['name'], 'status' => $pack['status_id'], 'country' => $pack['country_id']);

		}else{ 	return false;	}
	}

	public static function SaveProvider($info)
	{
		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$name 		= 	strtoupper($_replace->deleteTilde($info['name_pr']));

		if($info['type_pr'] == 'new')
		{
			$query 	= 	'INSERT INTO data_provider(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_pr'].'", "'.$info['status_id_pr'].'", "'.$date.'")';

			$serv  	=	DBSmart::DataExecute($query);
			
			return ($serv <> false) ? true : false;

		}
		elseif ($info['type_pr'] == 'edit') 
		{
			$query 	=	'UPDATE data_provider SET name="'.$name.'", country_id="'.$info['country_id_pr'].'", status_id="'.$info['status_id_pr'].'" WHERE id = "'.$info['id_pr'].'"';

			$serv  	=	DBSmart::DataExecute($query);
			
			return ($serv <> false) ? true : false;
		}
	}
}