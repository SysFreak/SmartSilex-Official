<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Package;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Package extends Model {

	static $_table 		= 'data_package';

	Public $_fillable 	= array('name', 'country_id' ,'status_id');

	public static function GetPackage()
	{
		$query 	= 	'SELECT * FROM data_package ORDER BY id ASC';
		$pack 	=	DBSmart::DBQueryAll($query);

		$package = array();

		if($pack <> false)
		{
			foreach ($pack as $k => $val) 
			{
				$package[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $package;

		}else{ 	return false;	}
	}

	public static function GetPackageById($id)
	{
		$query 	= 	'SELECT * FROM data_package WHERE id = "'.$id.'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack <> false)
		{
			return array('id' => $pack['id'], 'name' => $pack['name'], 'status' => $pack['status_id'], 'country' => $pack['country_id']);

		}else{ 	return false;	}

	}

	public static function GetPackageByName($info)
	{

		$query 	= 	'SELECT * FROM data_package WHERE name = "'.$info.'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack <> false)
		{
			return array('id' => $pack['id'], 'name' => $pack['name'], 'status' => $pack['status_id'], 'country' => $pack['country_id']);

		}else{ 	return false;	}
	}

	public static function SavePackage($info)
	{
		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$name 		= 	strtoupper($_replace->deleteTilde($info['name_pac']));

		if($info['type_pac'] == 'new')
		{
			$query 	= 	'INSERT INTO data_package(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_pac'].'", "'.$info['status_id_pac'].'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;

		}
		elseif ($info['type_pac'] == 'edit') 
		{
			$query 	=	'UPDATE data_package SET name="'.$name.'", country_id="'.$info['country_id_pac'].'", status_id="'.$info['status_id_pac'].'" WHERE id = "'.$info['id_pac'].'"';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
	}

	public static function GetPackageByTicket($ticket)
	{
		$query 	= 	'SELECT name FROM data_package WHERE id = (SELECT package_id FROM cp_service_pr_tv WHERE ticket = "'.$ticket.'")';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <>  false)
		{
			return $serv['name'];
			
		}else{ return ''; }
	}

}


