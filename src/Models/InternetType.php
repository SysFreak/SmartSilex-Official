<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Internet;
use App\Models\InternetType;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class InternetType extends Model {

	static $_table 		= 'data_internet_type';

	Public $_fillable 	= array('name', 'country_id', 'status_id');

	public static function GetInternetType()
	{

		$query 	= 	'SELECT * FROM data_internet_type ORDER BY id DESC';
		$int 	=	DBSmart::DBQueryAll($query);

		$interType = array();

		if($int <> false)
		{
			foreach ($int as $k => $val) 
			{
				$interType[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $interType;
		}else{ 	return false;	}
	}

	public static function GetInternetTypeById($id)
	{

		$query 	= 	'SELECT * FROM data_internet_type WHERE id = "'.$id.'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack <> false)
		{
			return array('id' => $pack['id'], 'name' => $pack['name'], 'status' => $pack['status_id'], 'country' => $pack['country_id']);

		}else{ 	return false;	}

	}

	public static function GetInternetTypeByName($info)
	{
		$query 	= 	'SELECT * FROM data_internet_type WHERE name = "'.$info.'"';
		$pack 	=	DBSmart::DBQuery($query);

		if($pack <> false)
		{
			return array('id' => $pack['id'], 'name' => $pack['name'], 'status' => $pack['status_id'], 'country' => $pack['country_id']);

		}else{ 	return false;	}
	}

	public static function SaveInternetType($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_int_t']));

		if($info['type_int_t'] == 'new')
		{
			$query 	= 	'INSERT INTO data_internet_type(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_int_t'].'", "'.$info['status_id_int_t'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_int_t'] == 'edit') 
		{
			$query 	=	'UPDATE data_internet_type SET name="'.$name.'", country_id="'.$info['country_id_int_t'].'", status_id="'.$info['status_id_int_t'].'" WHERE id = "'.$info['id_int_t'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}
}


