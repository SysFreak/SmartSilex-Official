<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\LeadsCompSEC;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadsCompSEC extends Model 
{

	static $_table 		= 'cp_coord_comp_data_sec';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'installer_date', 'installer', 'company', 'n_order', 'client', 'operator_id', 'created_at');


	public static function GetInfoClient($client)
	{
		$query 	=	'SELECT installer_date, installer, company, n_order, client FROM cp_coord_comp_data_sec WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		$result =	DBSmart::DBQuery($query);

		if($result <> false)
		{
			return [
				'inst_d'	=>	strtoupper($result['installer_date']),
				'instal'	=>	strtoupper($result['installer']),
				'company'	=>	strtoupper($result['company']),
				'order'		=>	strtoupper($result['n_order']),
				'client'	=>	strtoupper($result['client'])
			];
		}else{

			return ['inst_d' => '', 'instal' => '', 'company' => '', 'order' => '', 'client' => ''];
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function InsertInfoClient($params)
	{
		$query 	=	'INSERT INTO cp_coord_comp_data_sec(ticket, client_id, installer_date, installer, company, n_order, client, operator_id, created_at) VALUES ("'.$params['ticket'].'","'.$params['client'].'", "'.$params['instal_d'].'","'.$params['instal'].'","'.$params['compa'].'", "'.$params['order'].'", "'.$params['cliente'].'", "'.$params['operat'].'","'.date('Y-m-d H:m:s').'")';

		$leads 	=	DBSmart::DataExecute($query);

		return ($leads <> false ) ? true : false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

}