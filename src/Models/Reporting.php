<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Referred;
use App\Models\ReferredData;
use App\Models\Country;
use App\Models\User;
use App\Models\CypherData;

use App\Lib\Config;
use App\Lib\DBSmart;

use PDO;

class Reporting extends Model 
{
	public static function ReportingDID($info)
	{
        $query      =   'SELECT calldate, src, dst as queue, did FROM cdr WHERE did = "'.$info['did'].'" AND calldate BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'" GROUP BY src ORDER BY calldate DESC';
        $result     =   DBSmart::DBQueryAll($query);

        foreach ($result as $key => $val) 
        {
            $rows[$key]  =   [
                'calldate'  =>  $val['calldate'],
                'src'       =>  $val['src'],
                'queue'     =>  $val['queue'],
                'did'       =>  $val['did']
            ];
        }

        $columnNames = array();
        if(!empty($rows)){
            $firstRow = $rows[0];
            foreach($firstRow as $colName => $val){
                $columnNames[] = $colName;
            }
        }

        $fileName = 'Reporting-DID-'.date("Y-d-m").'.csv';

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
         
        $fp = fopen('php://output', 'w');
         
        fputcsv($fp, $columnNames);

        foreach ($rows as $row) { fputcsv($fp, $row);   }
         
        fclose($fp);

        exit;
        return true;

	}

}