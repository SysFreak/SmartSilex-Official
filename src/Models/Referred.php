<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Referred;
use App\Models\ReferredData;
use App\Models\Country;
use App\Models\User;
use App\Models\CypherData;

use App\Lib\Config;
use App\Lib\DBSmart;

class Referred extends Model {

	static $_table 		= 'cp_referred';

	Public $_fillable 	= array('id', 'name', 'birthday', 'client_id', 'type_id', 'user_id');

	public static function GetReferred()
	{
		$query 	= 	'SELECT * FROM cp_referred ORDER BY id ASC';
		$gen 	=	DBSmart::DBQueryAll($query);

		$general = array();

		if($gen <> false)
		{
			foreach ($gen as $k => $val) 
			{
				$general[$k] = array(
					'id' 	 	=> $val['id'],
					'name' 	 	=> $val['name'],
					'birthday' 	=> $val['birthday'],
					'client' 	=> $val['client_id'],
					'type' 		=> ReferredData::GetReferredById($val['type_id'])['name'],
					'user' 		=> User::GetUserById($val['user_id'])['username']
				);
			}
	        return $general;

		}else{ return false; }
	}

	public static function GetRefId($id)
	{

		$query 	= 	'SELECT * FROM cp_referred WHERE id = "'.$id.'"';
		$val 	=	DBSmart::DBQuery($query);

		if($val <> false)
		{
			$info = [
				'id' 	 	=> 	$val['id'],
				'name' 	 	=> 	$val['name'],
				'birthday' 	=> 	$val['birthday'],
				'phone' 	=> 	$val['phone'],
				'client' 	=> 	$val['client_id'],
				'type'		=>	$val['type_id'],
				'user' 		=> 	User::GetUserById($val['user_id'])['username'],
				'ref_id'	=>	$val['referred_id'],
				'created'	=>	$val['created_at']
			];
			return $info;

		}else{	return false;	}
	}

	public static function GetReferredById($id)
	{
		
		$query 	= 	'SELECT * FROM cp_referred WHERE id = "'.$id.'"';
		$val 	=	DBSmart::DBQuery($query);

		$info = [
			'id' 	 	=> 	$val['id'],
			'name' 	 	=> 	$val['name'],
			'birthday' 	=> 	$val['birthday'],
			'phone' 	=> 	$val['phone'],
			'client' 	=> 	$val['client_id'],
			'type'		=>	$val['type_id'],
			'user' 		=> 	User::GetUserById($val['user_id'])['username'],
			'created'	=>	$val['created_at']
		];
		
		$html = "";
		$html.='<dl class="dl-horizontal">';
		$html.='<dt>Client</dt>';
		$html.='<dd>'.$info['client'].'</dd>';
		$html.='<dt>NAME</dt>';
		$html.='<dd>'.$info['name'].'</dd>';
		$html.='<dt>BIRTHDAY</dt>';
		$html.='<dd>'.$info['birthday'].'</dd>';
		$html.='<dt>PHONE</dt>';
		$html.='<dd>'.$info['phone'].'</dd>';
		$html.='<dt>CREATED BY</dt>';
		$html.='<dd>'.$info['user'].'</dd>';
		$html.='</dl>';
					
		return array('info'	=>	$info,	'html'	=>	$html);
	}

	public static function GetReferredByClient($client)
	{
		$query 	= 	'SELECT * FROM cp_referred WHERE referred_id = "'.$client.'"';

		$ref 	=	DBSmart::DBQueryAll($query);

		if($ref <> false)
		{
			foreach ($ref as $key => $val) 
			{
				$referred[$key]		=	[
					'id' 	 	=> $val['id'],
					'name' 	 	=> $val['name'],
					'birthday' 	=> $val['birthday'],
					'phone' 	=> $val['phone'],
					'client' 	=> $val['client_id'],
					'type' 		=> ReferredData::GetReferredById($val['type_id'])['name'],
					'user' 		=> User::GetUserById($val['user_id'])['username']
				];
			}

			return $referred;

		}else{ return false; }

	}

	public static function GetReferredClientHtml($client, $type, $serv, $code)
	{
		$query 	= 	'SELECT * FROM cp_referred WHERE referred_id = "'.$client.'" AND type_id = "'.$type.'" ORDER BY id ASC';
		$val 	=	DBSmart::DBQueryAll($query);

		$html 	= 	"";

		if($val <> false)
		{
			$html='<select class="form-control" name="pr_'.$serv.'_ref_list" id="pr_'.$serv.'_'.$code.'">';

			foreach ($val as $k => $ref) 
			{ $html.='<option value="'.$ref['id'].'" selected="">'.$ref['name'].'</option>'; }
			$html.='</select>';

		}else{
			$html = '<select class="form-control" name="pr_'.$serv.'_ref_list" id="pr_'.$serv.'_'.$code.'" disabled><option value="0" selected="">NINGUNO</option></select>';
		}

		return [
			'status'	=>	(isset($ref['id'])) ? true : false,
			'html'		=>	$html
		];
	}

	public static function GetReferredClientVZHtml($client, $type, $serv, $code)
	{
		$query 	= 	'SELECT * FROM cp_referred WHERE referred_id = "'.$client.'" AND type_id = "'.$type.'" ORDER BY id ASC';

		$val 	=	DBSmart::DBQueryAll($query);

		$html 	= 	"";
		if($val <> false)
		{
			$html='<select class="form-control" name="vzla_'.$serv.'_ref_list" id="vzla_'.$serv.'_'.$code.'">';

			foreach ($val as $k => $ref) 
			{
				$html.='<option value="'.$ref['id'].'" selected="">'.$ref['name'].'</option>';
			}
			$html.='</select>';

		}else{
			$html = '<select class="form-control" name="vzla_'.$serv.'_ref_list" id="vzla_'.$serv.'_'.$code.'" disabled><option value="0" selected="">NINGUNO</option></select>';
		}

		return [
			'status'	=>	(isset($ref['id'])) ? true : false,
			'html'		=>	$html
		];
	}

	public static function SaveReferred($info)
	{
		$_replace  	= 	new Config();

		$date 		= 	date('Y-m-d H:i:s', time());

		$name 		=	strtoupper($_replace->deleteTilde($info['name']));
		
		$query 		=	'INSERT INTO cp_referred(name, birthday, phone, client_id, referred_id, type_id, user_id, created_at) VALUES ("'.$name.'","'.$info['birthday'].'","'.$info['phone'].'","'.$info['client'].'","'.$info['referred'].'","'.$info['type'].'","'.$info['user'].'","'.$date.'")';

		$serv  		=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	}

	public static function SaveReferredLead($cl)
	{


		$_replace  	= 	new Config();
		
		$query 		=	'INSERT INTO cp_referred(name, birthday, phone, client_id, referred_id, type_id, user_id, created_at) VALUES ("'.$cl['name'].'","'.$cl['birth'].'","'.$cl['phone'].'","'.$cl['client'].'","'.$cl['orig'].'","'.$cl['type'].'","'.$cl['user'].'", NOW())';

		$serv  		=	DBSmart::DataExecute($query);

		if($serv <> false)
		{
			$query	=	'UPDATE cp_leads set referred_id = "'.$cl['orig'].'" WHERE client_id = "'.$cl['client'].'"';
			$ref  	=	DBSmart::DataExecute($query);

			return ($ref <> false) ? true : false;

		}else{
			return ($serv <> false) ? true : false;

		}
	}

	public static function UpdateReferred($info, $user)
	{

		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$query  	=	'UPDATE cp_referred SET type_id = "'.$info['type_id_ref2'].'", user_id = "'.$user.'", created_at = "'.$date.'" WHERE id = "'.$info['id2_ref'].'" ';

		$ref 		=	DBSmart::DataExecute($query);

		if($ref <> false) 
		{	
			$query 	=	'SELECT client_id FROM cp_referred WHERE id = "'.$info['id2_ref'].'"';
			$infoR 	=	DBSmart::DBQuery($query);
			$client =	($infoR <> false) 	? 	$infoR 	: 	false;

		}else{	$client  = false; 	}

		return ($ref <> false) ? ['status'=> true, 'client' => $client] : ['status' => false];
	}
} 