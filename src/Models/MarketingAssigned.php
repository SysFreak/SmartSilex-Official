<?php
namespace App\Models;

use Model;

use App\Models\StatusLead;
use App\Models\CypherData;
use App\Models\Leads;
use App\Models\LeadsTmp;
use App\Models\Town;
use App\Models\Country;
use App\Models\ZipCode;
use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\Notes;
use App\Models\MarketingTmp;
use App\Models\OperatorAssigned;
use App\Models\MarketingAssigned;

use App\Lib\Config;
use App\Lib\DBSmart;

use PDO;

class MarketingAssigned extends Model 
{
	static $_table = 'cp_mkt_assig_leads';
	
	Public $_fillable = array('id', 'client_id', 'service_i_id', 'origen_id', 'medium_id', 'objective_id', 'post_id', 'form_id', 'conv_id', 'service_id', 'campaign_id', 'operator_id', 'user_id', 'created_at');

	public static function SaveMktAssig($info, $users)
	{
		$query 	=	'INSERT INTO cp_mkt_assig_leads(client_id, service_i_id, origen_id, medium_id, objective_id, post_id, form_id, conv_id, service_id, campaign_id, operator_id, user_id, created_at) VALUES ("'.$info['client'].'", "'.$info['serviceI'].'", "'.$info['origen'].'", "'.$info['medium'].'", "'.$info['objective'].'", "'.$info['post'].'", "'.$info['form'].'", "'.$info['conv'].'", "'.$info['service'].'", "'.$info['campaign'].'", "'.$users['operator'].'", "'.$users['user'].'", "'.$info['date'].'")';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;
		
	}
}
