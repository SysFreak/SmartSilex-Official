<?php
namespace App\Models;

use Model;

use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\ServiceIntVZ;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class Coordination extends Model
{

	static $_table 		= 'cp_coord_serv';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'contact_id', 'view_id', 'edit_id', 'pre_id', 'status_id', 'coord_id', 'coord_ref', 'cancel', 'created_at');

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function Load()
	{
		$_replace	=	new Config();
		$dWeek 		=	$_replace->dayWeek();
		$cWeek		=	$_replace->CurrentWeek();

		$Y 	=	date("Y");
		$M	=	date("m");

		$serv =	['2', '3', '4', '6'];

		$date 	=	date("Y-m-d");

		foreach ($serv as $k => $val) 
		{
			switch ($val) 
			{
				case '2':
					$servic =	'cp_service_pr_tv';
					break;
				case '3':
					$servic =	'cp_service_pr_sec';
					break;
				case '4':
					$servic =	'cp_service_pr_int';
					break;
				case '6':
					$servic =	'cp_service_vzla_int';
					break;
			}
			$query 	=	'SELECT t3.created_at AS created, t1.id, t1.ticket, t1.client_id, t2.name, t2.referred_id, t2.phone_main, (CASE WHEN t2.type_acc = "1" THEN "PRIMERA" WHEN t2.type_acc = "2" THEN "SEGUNDA" WHEN t2.type_acc = "3" THEN "TERCERA" WHEN t2.type_acc = "4" THEN "CUARTA" WHEN t2.type_acc = "5" THEN "QUINTA" ELSE ""	END) AS type_acc, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT name FROM data_score WHERE id = t5.score) AS score, (SUBSTRING(t1.ticket, "1", "2")) AS service, t1.status_id, (IF(t1.status_id <> "0", "AGENDADO", "NO CONTESTA")) AS status, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (IF(cont_id = "1","AGENDADO","NO CONTESTA")) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (IF(resp_id = "1", "EN PROCESO","EN PROCESO")) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN "EN PROCESO" WHEN t1.status_id = "4" THEN (SELECT (IF(coord_id = "1","EN PROCESO", "EN PROCESO")) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) WHEN t1.status_id = "99" THEN "CANCELADO" ELSE ""	END) AS reason, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN (SELECT (SELECT username FROM users WHERE id = user_id) FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS username, (CASE WHEN t1.status_id =	"0"	THEN ""	WHEN t1.status_id = "1" THEN (SELECT created_at FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT created_at FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN	t1.status_id = "3" THEN (SELECT created_at FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT created_at FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN	t1.status_id = "5" THEN (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT created_at FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS proc_date, t1.created_at FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) AND t1.contact_id = "0" AND t1.view_id = "0" AND t1.edit_id = "0" AND t1.pre_id = "0" AND t1.status_id = "1" AND t1.coord_id = "0" AND t1.coord_ref = "0" AND t1.cancel = "0" ORDER BY t3.created_at DESC';

			$pend 		=	DBSmart::DBQueryAll($query);

			$query 	=	'SELECT t1.id, t1.ticket, t1.client_id, t3.created_at AS created, t2.name, t2.referred_id, t2.phone_main, (CASE WHEN t2.type_acc = "1" THEN "PRIMERA" WHEN t2.type_acc = "2" THEN "SEGUNDA" WHEN t2.type_acc = "3" THEN "TERCERA" WHEN t2.type_acc = "4" THEN "CUARTA" WHEN t2.type_acc = "5" THEN "QUINTA" ELSE ""	END) AS type_acc, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SUBSTRING(t1.ticket, "1","2")) AS service, t1.status_id, (IF(t1.status_id <> "0", "AGENDADO", "NO CONTESTA")) AS status, (SELECT name FROM data_score WHERE id = t5.score) AS score, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1"	THEN (SELECT (IF(cont_id = "1", "AGENDADO", "NO CONTESTA")) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2"	THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN "EN PROCESO" WHEN t1.status_id = "4" THEN (SELECT (IF(coord_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6"	THEN (SELECT (IF(resp_id = "1","EN PROCESO", "EN PROCESO")) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) WHEN t1.status_id = "99" THEN "CANCELADO" ELSE "" END) AS reason, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN (SELECT (SELECT username FROM users WHERE id = user_id) FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS username, (CASE WHEN t1.status_id =	"0"	THEN ""	WHEN t1.status_id = "1" THEN (SELECT created_at FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT created_at FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN	t1.status_id = "3" THEN (SELECT created_at FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT created_at FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN	t1.status_id = "5" THEN (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT created_at FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) ELSE ""	END) AS proc_date, t1.created_at FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) AND t1.coord_id = "0" AND t1.coord_ref = "0" AND t1.cancel = "0" AND t1.contact_id <> "0"';

			$seg 		=	DBSmart::DBQueryAll($query);

			$query 		=	'SELECT t3.created_at, t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t2.name, t2.phone_main, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT name FROM data_score WHERE id = t4.score) AS score, (SELECT username FROM users WHERE id = t5.created_by) AS CoordUser, t5.pre_date AS preDate, t5.pre_disp AS preDisp FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_appro_serv AS t4 ON (t1.ticket = t4.ticket) INNER JOIN cp_coord_pre_tmp AS t5 ON (t1.pre_id = t5.id) INNER JOIN cp_coord_proc_tmp AS t6 ON (t1.coord_id = t6.id) AND t1.coord_id <> 0 AND t6.created_at BETWEEN "'.$cWeek['yI'].'-'.$cWeek['mI'].'-'.$cWeek['dI'].' 00:00:00" AND "'.$cWeek['yF'].'-'.$cWeek['mF'].'-'.$cWeek['dF'].' 23:59:59"';
		
			$coord 		=	DBSmart::DBQueryAll($query);

			$query 	=	'SELECT t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t3.created_at AS tCreated, t2.name, t2.phone_main, t6.name AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT id FROM users WHERE id = t7.operator_id) AS cancel_id, (SELECT username FROM users WHERE id = cancel_id) AS CancelUser, (SELECT reason_id FROM cp_coord_cancel_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS reason_id, (SELECT name FROM data_cancel_coord_objection WHERE id = reason_id) AS reason, t1.created_at AS cCancel FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id)INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket)	INNER JOIN data_score AS t6 ON (t5.score = t6.id) INNER JOIN cp_coord_cancel_tmp AS t7 ON (t1.cancel = t7.id) AND t1.cancel <> 0 AND t1.created_at BETWEEN "'.$dWeek['y'].'-'.$dWeek['m'].'-'.$dWeek['di'].' 00:00:00" AND "'.$dWeek['y'].'-'.$dWeek['m'].'-'.$dWeek['df'].' 23:59:59"';

			$cancel 	=	DBSmart::DBQueryAll($query);

			$query 		=	'SELECT t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t3.created_at AS tCreated, t2.name, t2.phone_main, t6.name AS score, (SELECT id FROM users WHERE id = t3.operator_id) AS sale_id, (SELECT username FROM users WHERE id = t3.operator_id) AS sale, (SELECT departament_id FROM users WHERE id = t3.operator_id) AS sale_dep_id, (SELECT name FROM data_departament WHERE id = sale_dep_id) AS sale_dep, (SELECT id FROM users WHERE id = t7.created_by) AS c_ref_id, (SELECT username FROM users WHERE id = c_ref_id) AS c_ref_user, t1.created_at AS rCancel FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) INNER JOIN data_score AS t6 ON (t5.score = t6.id) INNER JOIN cp_coord_ref_tmp AS t7 ON (t1.coord_ref = t7.id) AND t1.coord_ref <> 0 AND t1.created_at BETWEEN "'.$dWeek['y'].'-'.$dWeek['m'].'-'.$dWeek['di'].' 00:00:00" AND "'.$dWeek['y'].'-'.$dWeek['m'].'-'.$dWeek['df'].' 23:59:59"';

			$ref 		=	DBSmart::DBQueryAll($query);

			$iPend[$servic]		=	($pend <> false) 	?	$pend 	: 	'';
			$iSeg[$servic]		=	($seg <> false) 	?	$seg 	: 	'';
			$iCoord[$servic] 	=	($coord <> false) 	? 	$coord 	: 	'';
			$iCancel[$servic] 	=	($cancel <> false) 	? 	$cancel : 	'';
			$iRef[$servic] 		=	($ref <> false) 	? 	$ref 	: 	'';
		}

		return 	[
			'pend' 		=>	Coordination::CoordPendClient($iPend),
			'seg' 		=>	Coordination::CoordSegClient($iSeg),
			'coord'		=>	Coordination::CoordinationProcClient($iCoord),
			'cancel'	=>	Coordination::CoordCancelClient($iCancel),
			'ref'		=>	Coordination::CoordRefClient($iRef)
		];

		return Coordination::CoordPendClient($iCoord);
	
	}


////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function LoadByDate($info, $dates)
	{

		$type 	=	['0', '1', '2', '3'];

		$row 	=	"";
		$row2 	=	"";
		$row3 	=	"";
		$row4 	=	"";
		
		$serv =	['2', '3', '4', '6'];

		if($info == "0")
		{

			foreach ($serv as $k => $val) 
			{
				switch ($val) 
				{
					case '2':
						$servic =	'cp_service_pr_tv';
						break;
					case '3':
						$servic =	'cp_service_pr_sec';
						break;
					case '4':
						$servic =	'cp_service_pr_int';
						break;
					case '6':
						$servic =	'cp_service_vzla_int';
						break;
				}

				$query 	=	'SELECT t3.created_at AS created, t1.id, t1.ticket, t1.client_id, t2.name, t2.referred_id, t2.phone_main, (CASE WHEN t2.type_acc = "1" THEN "PRIMERA" WHEN t2.type_acc = "2" THEN "SEGUNDA" WHEN t2.type_acc = "3" THEN "TERCERA" WHEN t2.type_acc = "4" THEN "CUARTA" WHEN t2.type_acc = "5" THEN "QUINTA" ELSE ""	END) AS type_acc, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT name FROM data_score WHERE id = t5.score) AS score, (SUBSTRING(t1.ticket, "1", "2")) AS service, t1.status_id, (IF(t1.status_id <> "0", "AGENDADO", "NO CONTESTA")) AS status, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (IF(cont_id = "1","AGENDADO","NO CONTESTA")) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (IF(resp_id = "1", "EN PROCESO","EN PROCESO")) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN "EN PROCESO" WHEN t1.status_id = "4" THEN (SELECT (IF(coord_id = "1","EN PROCESO", "EN PROCESO")) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) WHEN t1.status_id = "99" THEN "CANCELADO" ELSE ""	END) AS reason, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN (SELECT (SELECT username FROM users WHERE id = user_id) FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS username, (CASE WHEN t1.status_id =	"0"	THEN ""	WHEN t1.status_id = "1" THEN (SELECT created_at FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT created_at FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN	t1.status_id = "3" THEN (SELECT created_at FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT created_at FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN	t1.status_id = "5" THEN (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT created_at FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS proc_date, t1.created_at FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) AND t1.contact_id = "0" AND t1.view_id = "0" AND t1.edit_id = "0" AND t1.pre_id = "0" AND t1.status_id = "1" AND t1.coord_id = "0" AND t1.coord_ref = "0" AND t1.cancel = "0" ORDER BY t3.created_at DESC';

				$pend 		=	DBSmart::DBQueryAll($query);

				$query 	=	'SELECT t1.id, t1.ticket, t1.client_id, t3.created_at AS created, t2.name, t2.referred_id, t2.phone_main, (CASE WHEN t2.type_acc = "1" THEN "PRIMERA" WHEN t2.type_acc = "2" THEN "SEGUNDA" WHEN t2.type_acc = "3" THEN "TERCERA" WHEN t2.type_acc = "4" THEN "CUARTA" WHEN t2.type_acc = "5" THEN "QUINTA" ELSE ""	END) AS type_acc, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SUBSTRING(t1.ticket, "1","2")) AS service, t1.status_id, (IF(t1.status_id <> "0", "AGENDADO", "NO CONTESTA")) AS status, (SELECT name FROM data_score WHERE id = t5.score) AS score, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1"	THEN (SELECT (IF(cont_id = "1", "AGENDADO", "NO CONTESTA")) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2"	THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN "EN PROCESO" WHEN t1.status_id = "4" THEN (SELECT (IF(coord_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6"	THEN (SELECT (IF(resp_id = "1","EN PROCESO", "EN PROCESO")) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) WHEN t1.status_id = "99" THEN "CANCELADO" ELSE "" END) AS reason, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN (SELECT (SELECT username FROM users WHERE id = user_id) FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS username, (CASE WHEN t1.status_id =	"0"	THEN ""	WHEN t1.status_id = "1" THEN (SELECT created_at FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT created_at FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN	t1.status_id = "3" THEN (SELECT created_at FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT created_at FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN	t1.status_id = "5" THEN (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT created_at FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) ELSE ""	END) AS proc_date, t1.created_at FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) AND t1.coord_id = "0" AND t1.coord_ref = "0" AND t1.cancel = "0" AND t1.contact_id <> "0" AND t1.created_at LIKE "'.$dates['dateEnd'].'%" ORDER BY t1.created_at DESC';

				$seg 		=	DBSmart::DBQueryAll($query);

				$query 		=	'SELECT t3.created_at, t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t2.name, t2.phone_main, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT name FROM data_score WHERE id = t4.score) AS score, (SELECT username FROM users WHERE id = t5.created_by) AS CoordUser, t5.pre_date AS preDate, t5.pre_disp AS preDisp FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_appro_serv AS t4 ON (t1.ticket = t4.ticket) INNER JOIN cp_coord_pre_tmp AS t5 ON (t1.pre_id = t5.id) INNER JOIN cp_coord_proc_tmp AS t6 ON (t1.coord_id = t6.id) AND t1.coord_id <> 0 AND t6.created_at LIKE "'.$dates['dateEnd'].'%" ORDER BY t1.created_at DESC';
			
				$coord 		=	DBSmart::DBQueryAll($query);

				$query 	=	'SELECT t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t3.created_at AS tCreated, t2.name, t2.phone_main, t6.name AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT id FROM users WHERE id = t7.operator_id) AS cancel_id, (SELECT username FROM users WHERE id = cancel_id) AS CancelUser, (SELECT reason_id FROM cp_coord_cancel_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS reason_id, (SELECT name FROM data_cancel_coord_objection WHERE id = reason_id) AS reason, t1.created_at AS cCancel FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id)INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket)	INNER JOIN data_score AS t6 ON (t5.score = t6.id) INNER JOIN cp_coord_cancel_tmp AS t7 ON (t1.cancel = t7.id) AND t1.cancel <> 0 AND t1.created_at LIKE "'.$dates['dateEnd'].'%" ORDER BY t1.created_at DESC';

				$cancel 	=	DBSmart::DBQueryAll($query);

				$query 		=	'SELECT t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t3.created_at AS tCreated, t2.name, t2.phone_main, t6.name AS score, (SELECT id FROM users WHERE id = t4.owen_id) AS sale_id, (SELECT username FROM users WHERE id = t4.owen_id) AS sale, (SELECT departament_id FROM users WHERE id = t4.owen_id) AS sale_dep_id, (SELECT name FROM data_departament WHERE id = sale_dep_id) AS sale_dep, (SELECT id FROM users WHERE id = t7.created_by) AS c_ref_id, (SELECT username FROM users WHERE id = c_ref_id) AS c_ref_user, t1.created_at AS rCancel FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) INNER JOIN data_score AS t6 ON (t5.score = t6.id) INNER JOIN cp_coord_ref_tmp AS t7 ON (t1.coord_ref = t7.id) AND t1.coord_ref <> 0 AND t1.created_at LIKE "'.$dates['dateEnd'].'%" ORDER BY t1.created_at DESC';

				$ref 		=	DBSmart::DBQueryAll($query);

				$iPend[$servic]		=	($pend <> false) 	?	$pend 	: 	'';
				$iSeg[$servic]		=	($seg <> false) 	?	$seg 	: 	'';
				$iCoord[$servic] 	=	($coord <> false) 	? 	$coord 	: 	'';
				$iCancel[$servic] 	=	($cancel <> false) 	? 	$cancel : 	'';
				$iRef[$servic] 		=	($ref <> false) 	? 	$ref 	: 	'';
			
			}

		}else{

			foreach ($serv as $k => $val) 
			{
				switch ($val) 
				{
					case '2':
						$servic =	'cp_service_pr_tv';
						break;
					case '3':
						$servic =	'cp_service_pr_sec';
						break;
					case '4':
						$servic =	'cp_service_pr_int';
						break;
					case '6':
						$servic =	'cp_service_vzla_int';
						break;
				}

				$query 	=	'SELECT t3.created_at AS created, t1.id, t1.ticket, t1.client_id, t2.name, t2.referred_id, t2.phone_main, (CASE WHEN t2.type_acc = "1" THEN "PRIMERA" WHEN t2.type_acc = "2" THEN "SEGUNDA" WHEN t2.type_acc = "3" THEN "TERCERA" WHEN t2.type_acc = "4" THEN "CUARTA" WHEN t2.type_acc = "5" THEN "QUINTA" ELSE ""	END) AS type_acc, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT name FROM data_score WHERE id = t5.score) AS score, (SUBSTRING(t1.ticket, "1", "2")) AS service, t1.status_id, (IF(t1.status_id <> "0", "AGENDADO", "NO CONTESTA")) AS status, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (IF(cont_id = "1","AGENDADO","NO CONTESTA")) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (IF(resp_id = "1", "EN PROCESO","EN PROCESO")) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN "EN PROCESO" WHEN t1.status_id = "4" THEN (SELECT (IF(coord_id = "1","EN PROCESO", "EN PROCESO")) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) WHEN t1.status_id = "99" THEN "CANCELADO" ELSE ""	END) AS reason, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN (SELECT (SELECT username FROM users WHERE id = user_id) FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS username, (CASE WHEN t1.status_id =	"0"	THEN ""	WHEN t1.status_id = "1" THEN (SELECT created_at FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT created_at FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN	t1.status_id = "3" THEN (SELECT created_at FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT created_at FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN	t1.status_id = "5" THEN (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT created_at FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS proc_date, t1.created_at FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) AND t1.contact_id = "0" AND t1.view_id = "0" AND t1.edit_id = "0" AND t1.pre_id = "0" AND t1.status_id = "1" AND t1.coord_id = "0" AND t1.coord_ref = "0" AND t1.cancel = "0" ORDER BY t3.created_at DESC';

				$pend 		=	DBSmart::DBQueryAll($query);

				$query 	=	'SELECT t1.id, t1.ticket, t1.client_id, t3.created_at AS created, t2.name, t2.referred_id, t2.phone_main, (CASE WHEN t2.type_acc = "1" THEN "PRIMERA" WHEN t2.type_acc = "2" THEN "SEGUNDA" WHEN t2.type_acc = "3" THEN "TERCERA" WHEN t2.type_acc = "4" THEN "CUARTA" WHEN t2.type_acc = "5" THEN "QUINTA" ELSE ""	END) AS type_acc, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SUBSTRING(t1.ticket, "1","2")) AS service, t1.status_id, (IF(t1.status_id <> "0", "AGENDADO", "NO CONTESTA")) AS status, (SELECT name FROM data_score WHERE id = t5.score) AS score, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1"	THEN (SELECT (IF(cont_id = "1", "AGENDADO", "NO CONTESTA")) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2"	THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN "EN PROCESO" WHEN t1.status_id = "4" THEN (SELECT (IF(coord_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (IF(resp_id = "1", "EN PROCESO", "EN PROCESO")) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6"	THEN (SELECT (IF(resp_id = "1","EN PROCESO", "EN PROCESO")) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) WHEN t1.status_id = "99" THEN "CANCELADO" ELSE "" END) AS reason, (CASE WHEN t1.status_id = "0" THEN "" WHEN t1.status_id = "1" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN t1.status_id = "3" THEN (SELECT (SELECT username FROM users WHERE id = user_id) FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN t1.status_id = "5" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT (SELECT username FROM users WHERE id = created_by) FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)	ELSE ""	END) AS username, (CASE WHEN t1.status_id =	"0"	THEN ""	WHEN t1.status_id = "1" THEN (SELECT created_at FROM cp_coord_contact_tmp WHERE id = t1.contact_id) WHEN t1.status_id = "2" THEN (SELECT created_at FROM cp_coord_view_tmp WHERE id = t1.view_id) WHEN	t1.status_id = "3" THEN (SELECT created_at FROM cp_coord_serv_edit WHERE id = t1.edit_id) WHEN t1.status_id = "4" THEN (SELECT created_at FROM cp_coord_pre_tmp WHERE id = t1.pre_id) WHEN	t1.status_id = "5" THEN (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.coord_id) WHEN t1.status_id = "6" THEN (SELECT created_at FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) ELSE ""	END) AS proc_date, t1.created_at FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) AND t1.coord_id = "0" AND t1.coord_ref = "0" AND t1.cancel = "0" AND t1.contact_id <> "0" AND t1.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY t1.created_at DESC';

				$seg 		=	DBSmart::DBQueryAll($query);

				// $query 		=	'SELECT t3.created_at, t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t2.name, t2.phone_main, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT name FROM data_score WHERE id = t4.score) AS score, (SELECT username FROM users WHERE id = t5.created_by) AS CoordUser, t6.pre_date AS preDate, t6.pre_disp AS preDisp FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_appro_serv AS t4 ON (t1.ticket = t4.ticket) INNER JOIN cp_coord_pre_tmp AS t5 ON (t1.pre_id = t5.id) INNER JOIN cp_coord_pre_tmp AS t6 ON (t1.pre_id = t6.id) AND t1.coord_id <> 0 AND t1.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY t1.created_at DESC';

				$query 		=	'SELECT t3.created_at, t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t2.name, t2.phone_main, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT name FROM data_score WHERE id = t4.score) AS score, (SELECT username FROM users WHERE id = t5.created_by) AS CoordUser, t5.pre_date AS preDate, t5.pre_disp AS preDisp FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_appro_serv AS t4 ON (t1.ticket = t4.ticket) INNER JOIN cp_coord_pre_tmp AS t5 ON (t1.pre_id = t5.id) INNER JOIN cp_coord_proc_tmp AS t6 ON (t1.coord_id = t6.id) AND t1.coord_id <> 0 AND t6.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY t1.created_at DESC';


			
				$coord 		=	DBSmart::DBQueryAll($query);

				$query 	=	'SELECT t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t3.created_at AS tCreated, t2.name, t2.phone_main, t6.name AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket )) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM '.$servic.' WHERE ticket = t1.ticket ))) AS sale_dep, (SELECT id FROM users WHERE id = t7.operator_id) AS cancel_id, (SELECT username FROM users WHERE id = cancel_id) AS CancelUser, (SELECT reason_id FROM cp_coord_cancel_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS reason_id, (SELECT name FROM data_cancel_coord_objection WHERE id = reason_id) AS reason, t1.created_at AS cCancel FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id)INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket)	INNER JOIN data_score AS t6 ON (t5.score = t6.id) INNER JOIN cp_coord_cancel_tmp AS t7 ON (t1.cancel = t7.id) AND t1.cancel <> 0 AND t1.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY t1.created_at DESC';


				$cancel 	=	DBSmart::DBQueryAll($query);

				$query 		=	'SELECT t1.id, t1.ticket, (SUBSTRING(t1.ticket,1,2)) AS service, t1.client_id, t3.created_at AS tCreated, t2.name, t2.phone_main, t6.name AS score, (SELECT id FROM users WHERE id = t4.owen_id) AS sale_id, (SELECT username FROM users WHERE id = t4.owen_id) AS sale, (SELECT departament_id FROM users WHERE id = t4.owen_id) AS sale_dep_id, (SELECT name FROM data_departament WHERE id = sale_dep_id) AS sale_dep, (SELECT id FROM users WHERE id = t7.created_by) AS c_ref_id, (SELECT username FROM users WHERE id = c_ref_id) AS c_ref_user, t1.created_at AS rCancel FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN '.$servic.' AS t3 ON (t1.ticket = t3.ticket) INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id) INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket) INNER JOIN data_score AS t6 ON (t5.score = t6.id) INNER JOIN cp_coord_ref_tmp AS t7 ON (t1.coord_ref = t7.id) AND t1.coord_ref <> 0 AND t1.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ORDER BY t1.created_at DESC';

				$ref 		=	DBSmart::DBQueryAll($query);

				$iPend[$servic]		=	($pend <> false) 	?	$pend 	: 	'';
				$iSeg[$servic]		=	($seg <> false) 	?	$seg 	: 	'';
				$iCoord[$servic] 	=	($coord <> false) 	? 	$coord 	: 	'';
				$iCancel[$servic] 	=	($cancel <> false) 	? 	$cancel : 	'';
				$iRef[$servic] 		=	($ref <> false) 	? 	$ref 	: 	'';
			
			}

		}

			return 	[
				'pend' 		=>	Coordination::CoordPendClient($iPend),
				'seg' 		=>	Coordination::CoordSegClient($iSeg),
				'coord'		=>	Coordination::CoordinationProcClient($iCoord),
				'cancel'	=>	Coordination::CoordCancelClient($iCancel),
				'ref'		=>	Coordination::CoordRefClient($iRef)
			];

			return Coordination::CoordPendClient($iCoord);

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function GetCoordInfoById($id)
	{
		$query 	=	'SELECT ticket, client_id, (SELECT (IF(cont_id  = "1", "SI", "NO")) FROM cp_coord_contact_tmp WHERE id = contact_id) AS contact, (SELECT (IF(resp_id  = "1", "SI", "NO")) FROM cp_coord_view_tmp WHERE id = view_id) AS view, (SELECT (IF(coord_id = "1", "SI", "NO")) FROM cp_coord_pre_tmp WHERE id = pre_id) AS pre FROM cp_coord_serv WHERE id = "'.$id.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function ViewCoord($id)
	{
		$query 	= 	'SELECT ticket, client_id FROM cp_coord_serv WHERE client_id = "'.$id.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function ViewCoordPend($id)
	{
		$query 	= 	'SELECT ticket, client_id FROM cp_coord_serv WHERE id = "'.$id.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function DetecTicket($in, $ticket)
	{

		switch ($in) {
			case 'TV':
				return (ServiceTvPr::GetServiceByTicket($ticket) <> false) ? 'TVPR' : false;
				break;
			case 'SE':			
				return (ServiceSecPr::GetServiceByTicket($ticket) <> false) ? 'SECPR'  : false;
				break;
			case 'IN':
				return (ServiceIntPr::GetServiceByTicket($ticket) <> false) ? 'INTPR' : false;;
				break;
			case 'IV':
				return (ServiceIntVZ::GetServiceByTicket($ticket) <> false) ? 'IVE' : false;;
				break;
		}
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function UpdateTicket($data, $info, $body)
	{
		switch ($body) 
		{
			case 'CONTACT':
				$query 	=	'UPDATE cp_coord_serv SET contact_id = "'.$info.'", status_id = "1" WHERE ticket = "'.$data['ticket'].'"';
				break;
			case 'VIEW':
				$query 	=	'UPDATE cp_coord_serv SET view_id = "'.$info.'", 	status_id = "2" WHERE ticket = "'.$data['ticket'].'"';
				break;
			case 'EDIT':
				$query 	=	'UPDATE cp_coord_serv SET edit_id = "'.$info.'",	status_id = "3" WHERE ticket = "'.$data['ticket'].'"';
				break;
			case 'PRE':
				$query 	=	'UPDATE cp_coord_serv SET pre_id = "'.$info.'", 	status_id = "4" WHERE ticket = "'.$data['ticket'].'"';
				break;
			case 'C_ID':
				$query 	=	'UPDATE cp_coord_serv SET coord_id = "'.$info.'", 	status_id = "5" WHERE ticket = "'.$data['ticket'].'"';
				break;
			case 'C_RE':
				$query 	=	'UPDATE cp_coord_serv SET coord_ref = "'.$info.'", 	status_id = "6" WHERE ticket = "'.$data['ticket'].'"';
				break;
		}

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveCoord($info)
	{
		$date 	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_coord_serv(ticket, client_id, contact_id, view_id, edit_id, pre_id, status_id, coord_id, coord_ref, cancel, created_by, created_at) VALUES ("'.$info['ticket'].'", "'.$info['client'].'", "0", "0", "0", "0", "1", "0", "0", "0", "'.$info['ope_a'].'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function GetInfo($type, $ticket)
	{

		$cypher		=	New UnCypher();

        $table 		=	$referred 	=		'';

        if($type == 'TVPR')
        {
        	$query 	=	'SELECT ticket, client_id, tv, (SELECT name FROM data_package WHERE id = package_id) AS package, (SELECT name FROM data_provider WHERE id = provider_id) AS provider, decoder_id, dvr_id, (SELECT name FROM data_payment WHERE id = payment_id) AS payment,  IF(ch_hd = "on", "SI", "NO") AS ch_hd, IF(ch_dvr = "on", "SI", "NO") AS ch_dvr, IF(ch_hbo = "on", "SI", "NO") AS ch_hbo, IF(ch_cinemax = "on", "SI", "NO") AS ch_cinemax, IF(ch_starz = "on", "SI", "NO") AS ch_starz, IF(ch_showtime = "on", "SI", "NO") AS ch_showtime, IF(gif_ent = "on", "SI", "NO") AS gif_ent, IF(gif_xtra = "on", "SI", "NO") AS gif_xtra, IF(gif_choice = "on", "SI", "NO") AS gif_choice, IF(advertising_id = "1", "SI", "NO") AS advertising FROM cp_service_pr_tv WHERE ticket = "'.$ticket.'"';

        	$table = 'cp_service_pr_tv';
        }
        elseif ($type == 'SECPR') 
        {
        	$query 	= 	'SELECT ticket, client_id, if( previously_id = "1", "SI", "NO") AS sist_previo, equipment, (SELECT name FROM data_camera WHERE id = cameras_id) AS camaras,  comp_equipment AS compania, (SELECT name FROM data_camera WHERE id = dvr_id ) AS dvr, add_equipment AS adicional, password_alarm, (SELECT name FROM data_payment WHERE id = payment_id) AS payment, contact_1, contact_1_desc, contact_2, contact_2_desc, contact_3, contact_3_desc FROM cp_service_pr_sec WHERE ticket = "'.$ticket.'"';
        	
        	$table = 'cp_service_pr_sec';

        }
        elseif ($type == 'INTPR') 
        {
        	$query = 	'SELECT ticket, client_id, (SELECT name FROM data_internet 	WHERE id = int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = pho_res_id) AS pho_res, (SELECT name FROM data_internet 	WHERE id = int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = payment_id) AS payment FROM cp_service_pr_int WHERE ticket = "'.$ticket.'"';
        	
        	$table = 'cp_service_pr_int';
        }
        elseif($type == 'IVE')
        {
        	$query = 'SELECT ticket, client_id, (SELECT name FROM data_internet WHERE id = int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = pho_res_id) AS phone_res, (SELECT name FROM data_internet WHERE id = int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = pho_com_id) AS phone_com, (SELECT name FROM data_payment WHERE id = payment_id) AS payment, (SELECT name FROM data_bank WHERE id = bank_id) AS bank, amount, trans_date, email, additional FROM cp_service_vzla_int WHERE ticket = "'.$ticket.'"';

        	$table = 'cp_service_vzla_int';

        }

        $service    =   DBSmart::DBQuery($query);

        $query 		=	'SELECT t2.client_id, t2.name, t2.birthday, t2.coor_lati AS coord_la, t2.coor_long AS coord_lo, t2.referred_id, t2.phone_main AS phone,  (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS p_provider, t2.phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = t2.phone_alt_provider) AS a_provider, t2.phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = t2.phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = t2.town_id) AS town, (SELECT code FROM data_zips WHERE id = t2.zip_id) AS zip, (SELECT name FROM data_country WHERE id = t2.country_id) AS country, (SELECT name FROM data_ceiling WHERE id = t2.h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = t2.h_level_id) AS level, t2.ident_id, t2.ident_exp, t2.ss_id, t2.ss_exp, t2.add_main, t2.add_postal, t2.email_main, t2.additional FROM '.$table.' AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

        $client     =   DBSmart::DBQuery($query);

        if($client['referred_id'] <> "0")
        {
	        $query 		= 	'SELECT client_id, name, birthday, coor_lati AS coord_la, coor_long AS coord_lo, referred_id, phone_main AS phone, (SELECT name FROM data_phone_provider WHERE id = phone_main_provider) AS p_provider, phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = phone_alt_provider) AS a_provider, phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT code FROM data_zips WHERE id = zip_id) AS zip, (SELECT name FROM data_country WHERE id = country_id) AS country, (SELECT name FROM data_ceiling WHERE id = h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = h_level_id) AS level, ident_id, ident_exp, ss_id, ss_exp, add_main, add_postal, email_main, additional FROM cp_leads WHERE client_id = "'.$client['referred_id'].'"';

	        $referred   =   DBSmart::DBQuery($query);

        }else{ $referred == false; }


        return ['client' => $client, 'referred' => $referred, 'service' => $service];

   	}

   	public static function GetInfoGen($client)
   	{
		$cypher		=	New UnCypher();

        $table 		=	$referred 	=		'';
        
   		$query 	=	'SELECT client_id, name, birthday, coor_lati AS coord_la, coor_long AS coord_lo, referred_id, phone_main AS phone,  (SELECT name FROM data_phone_provider WHERE id = phone_main_provider) AS p_provider, phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = phone_alt_provider) AS a_provider, phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT code FROM data_zips WHERE id = zip_id) AS zip, (SELECT name FROM data_country WHERE id = country_id) AS country, (SELECT name FROM data_ceiling WHERE id = h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = h_level_id) AS level, ident_id, ident_exp, ss_id, ss_exp, add_main, add_postal, email_main, additional FROM cp_leads WHERE client_id = "'.$client.'"';

   		$client     =   DBSmart::DBQuery($query);

        if($client['referred_id'] <> "0")
        {
	        $query 		= 	'SELECT client_id, name, birthday, coor_lati AS coord_la, coor_long AS coord_lo, referred_id, phone_main AS phone, (SELECT name FROM data_phone_provider WHERE id = phone_main_provider) AS p_provider, phone_alt AS p_alter, (SELECT name FROM data_phone_provider WHERE id = phone_alt_provider) AS a_provider, phone_other AS p_other, (SELECT name FROM data_phone_provider WHERE id = phone_other_provider) AS o_provider, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT code FROM data_zips WHERE id = zip_id) AS zip, (SELECT name FROM data_country WHERE id = country_id) AS country, (SELECT name FROM data_ceiling WHERE id = h_type_id) AS ceiling, (SELECT name FROM data_levels WHERE id = h_level_id) AS level, ident_id, ident_exp, ss_id, ss_exp, add_main, add_postal, email_main, additional FROM cp_leads WHERE client_id = "'.$client['referred_id'].'"';

	        $referred   =   DBSmart::DBQuery($query);

        }else{ $referred == false; }

   		return ['client' => $client, 'referred' => $referred];

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function ObtData($client, $ticket)
   	{
		$cypher		=	New UnCypher();

        if($client)
        {

        	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'ID');
			$ide 	=	$info['infoCard'];

	    	if($ide <> false) 
	    		{ $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = IDType::GetIdTypeById($info['info']['type_d'])['name'];}
	    		// { $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = "";}
	    	else 
	    		{ $ident = ""; $ident_exp = ""; $ident_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'SS');
	    	$sss 	=	$info['infoCard'];

	    	if($sss <> false) 
	    		{ $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = SSType::GetSSTypeById($info['info']['type_d'])['name'];}
	    		// { $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = "";}
	    	else 
	    		{ $ss = ""; $ss_exp = ""; $ss_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'CC');
	    	$cards 	=	$info['infoCard'];

	    	if($cards <> false) 
	    		{ $card = $cards['card']; $card_exp = $cards['month']."/".$cards['year']; }
	    	else 
	    		{ $card = ""; $card_exp = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'AB');

			$ABS 	=	$info['infoCard'];

	    	if($ABS <> false) 
	    		{ 
					$ab_nam = $ABS['name']; $ab_num = $ABS['account']; $ab_ban = Bank::GetBankById($ABS['bank'])['name']; $ab_typ = $ABS['type'];
	    		}
	    	else 
	    		{ $ab_nam = ""; $ab_num = ""; $ab_ban = ""; $ab_typ = ""; }

	    	$cData = 	[
	    		'client'		=>	($client['client_id'] <> '')	? $client['client_id'] 	: '',
	    		'ticket'		=>	($ticket <> '')					? $ticket 				: '',
	    		'name'			=>	($client['name'] <> '')			? $client['name'] 		: '',
	    		'birthday'		=>	($client['birthday'] <> '')		? $client['birthday'] 	: '',
	    		'coord_la'		=>	($client['coord_la'] <> '')		? $client['coord_la'] 	: '',
	    		'coord_lo'		=>	($client['coord_lo'] <> '')		? $client['coord_lo'] 	: '',
	    		'phone'			=>	($client['phone'] <> '')		? $client['phone'] 		: '',
	    		'p_provider'	=>	($client['p_provider'] <> '')	? $client['p_provider'] : '',
	    		'p_alter'		=>	($client['p_alter'] <> '')		? $client['p_alter'] 	: '',
	    		'a_provider'	=>	($client['a_provider'] <> '')	? $client['a_provider'] : '',
	    		'p_other'		=>	($client['p_other'] <> '')		? $client['p_other'] 	: '',
	    		'o_provider'	=>	($client['o_provider'] <> '')	? $client['o_provider'] : '',
	    		'town'			=>	($client['town'] <> '')			? $client['town'] 		: '',
	    		'country'		=>	($client['country'] <> '')		? $client['country'] 	: '',
				'ceiling'		=>	($client['ceiling'] <> '')		? $client['ceiling'] 	: '',
	    		'level'			=>	($client['level'] <> '')		? $client['level'] 		: '',
	    		'ss'			=>	($ss <> '') 		?	$ss 		: '',
	    		'ss_exp'		=>	($ss_exp <> '') 	? 	$ss_exp 	: '',
	    		'ss_type'		=>	($ss_type <> '') 				? 	$ss_type 			: '',
	    		'id'			=>	($ident <> '') 		? 	$ident 		: '',
	    		'id_exp'		=>	($ident_exp <> '')	? 	$ident_exp 	: '',
				'id_type'		=>	($ident_type <> '')				? 	$ident_type 		: '',
	    		'cc'			=>	($card <> '') 		? 	$card 		: '',
	    		'cc_exp'		=>	($card_exp <> '')	?   $card_exp 	: '',
	    		'ab'			=>	($ab_nam <> '') 	? 	$ab_nam 	: '',
	    		'ab_num'		=>	($ab_num <> '') 	? 	$ab_num		: '',
	    		'ab_bank'		=>	($ab_ban <> '') 	? 	$ab_ban 	: '',
	    		'ab_type'		=>	($ab_typ <> '') 	? 	$ab_typ 	: '',
	    		'add'			=>	($client['add_main'] <> '')		? $client['add_main']	: '',
	    		'add_postal'	=>	($client['add_postal'] <> '')	? $client['add_postal']	: '',
	    		'email'			=>	($client['email_main'] <> '')	? $client['email_main']	: '',
	    		'additional'	=>	($client['additional'] <> '')	? $client['additional']	: ''
	    	];

        }else{

		    $cData = 	[
	    		'client' => '', 'ticket' => '', 'name' => '', 'phone' => '', 'p_provider' => '', 'p_alter' => '', 'a_provider' => '', 'p_other' => '', 'o_provider' => '', 'town' => '', 'country'=> '', 'ss' => '', 'ss_exp' => '', 'id' => '', 'id_exp' => '', 'cc' => '', 'cc_exp' => '', 'ab' => '', 'ab_num' => '', 'ab_bank' => '', 'ab_type' => '', 'add' => '', 'add_postal' => '', 'email'=> ''];        	
        }
        
    	return ['status'=> ($client) ? true : false, 'data' => $cData];
   	
   	}


   	public static function ObtDataGen($client)
   	{
		$cypher		=	New UnCypher();

        if($client)
        {

        	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'ID');
			$ide 	=	$info['infoCard'];

	    	if($ide <> false) 
	    		{ $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = IDType::GetIdTypeById($info['info']['type_d'])['name'];}
	    		// { $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = "";}
	    	else 
	    		{ $ident = ""; $ident_exp = ""; $ident_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'SS');
	    	$sss 	=	$info['infoCard'];

	    	if($sss <> false) 
	    		{ $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = SSType::GetSSTypeById($info['info']['type_d'])['name'];}
	    		// { $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = "";}
	    	else 
	    		{ $ss = ""; $ss_exp = ""; $ss_type = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'CC');
	    	$cards 	=	$info['infoCard'];

	    	if($cards <> false) 
	    		{ $card = $cards['card']; $card_exp = $cards['month']."/".$cards['year']; }
	    	else 
	    		{ $card = ""; $card_exp = ""; }

	    	$info 	=	$cypher->GetInfoCypher($client['client_id'], 'AB');

			$ABS 	=	$info['infoCard'];

	    	if($ABS <> false) 
	    		{ 
					$ab_nam = $ABS['name']; $ab_num = $ABS['account']; $ab_ban = Bank::GetBankById($ABS['bank'])['name']; $ab_typ = $ABS['type'];
	    		}
	    	else 
	    		{ $ab_nam = ""; $ab_num = ""; $ab_ban = ""; $ab_typ = ""; }

	    	$cData = 	[
	    		'client'		=>	($client['client_id'] <> '')	? $client['client_id'] 	: '',
	    		'name'			=>	($client['name'] <> '')			? $client['name'] 		: '',
	    		'birthday'		=>	($client['birthday'] <> '')		? $client['birthday'] 	: '',
	    		'coord_la'		=>	($client['coord_la'] <> '')		? $client['coord_la'] 	: '',
	    		'coord_lo'		=>	($client['coord_lo'] <> '')		? $client['coord_lo'] 	: '',
	    		'phone'			=>	($client['phone'] <> '')		? $client['phone'] 		: '',
	    		'p_provider'	=>	($client['p_provider'] <> '')	? $client['p_provider'] : '',
	    		'p_alter'		=>	($client['p_alter'] <> '')		? $client['p_alter'] 	: '',
	    		'a_provider'	=>	($client['a_provider'] <> '')	? $client['a_provider'] : '',
	    		'p_other'		=>	($client['p_other'] <> '')		? $client['p_other'] 	: '',
	    		'o_provider'	=>	($client['o_provider'] <> '')	? $client['o_provider'] : '',
	    		'town'			=>	($client['town'] <> '')			? $client['town'] 		: '',
	    		'country'		=>	($client['country'] <> '')		? $client['country'] 	: '',
				'ceiling'		=>	($client['ceiling'] <> '')		? $client['ceiling'] 	: '',
	    		'level'			=>	($client['level'] <> '')		? $client['level'] 		: '',
	    		'ss'			=>	($ss <> '') 		?	$ss 		: '',
	    		'ss_exp'		=>	($ss_exp <> '') 	? 	$ss_exp 	: '',
	    		'ss_type'		=>	($ss_type <> '') 				? 	$ss_type 			: '',
	    		'id'			=>	($ident <> '') 		? 	$ident 		: '',
	    		'id_exp'		=>	($ident_exp <> '')	? 	$ident_exp 	: '',
				'id_type'		=>	($ident_type <> '')				? 	$ident_type 		: '',
	    		'cc'			=>	($card <> '') 		? 	$card 		: '',
	    		'cc_exp'		=>	($card_exp <> '')	?   $card_exp 	: '',
	    		'ab'			=>	($ab_nam <> '') 	? 	$ab_nam 	: '',
	    		'ab_num'		=>	($ab_num <> '') 	? 	$ab_num		: '',
	    		'ab_bank'		=>	($ab_ban <> '') 	? 	$ab_ban 	: '',
	    		'ab_type'		=>	($ab_typ <> '') 	? 	$ab_typ 	: '',
	    		'add'			=>	($client['add_main'] <> '')		? $client['add_main']	: '',
	    		'add_postal'	=>	($client['add_postal'] <> '')	? $client['add_postal']	: '',
	    		'email'			=>	($client['email_main'] <> '')	? $client['email_main']	: '',
	    		'additional'	=>	($client['additional'] <> '')	? $client['additional']	: ''
	    	];

        }else{

		    $cData = 	[
	    		'client' => '', 'ticket' => '', 'name' => '', 'phone' => '', 'p_provider' => '', 'p_alter' => '', 'a_provider' => '', 'p_other' => '', 'o_provider' => '', 'town' => '', 'country'=> '', 'ss' => '', 'ss_exp' => '', 'id' => '', 'id_exp' => '', 'cc' => '', 'cc_exp' => '', 'ab' => '', 'ab_num' => '', 'ab_bank' => '', 'ab_type' => '', 'add' => '', 'add_postal' => '', 'email'=> ''];        	
        }
        
    	return ['status'=> ($client) ? true : false, 'data' => $cData];
   	
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function CoordCancel($cData, $params)
	{

		foreach ($params['cancel_sel_c'] as $k => $val) { $can 	=	CoordCancelTmp::SaveCancelCoord($val, $cData); }

		if($can <> false)
		{
			$note 	=	Notes::SaveNote($cData);

			if($note <> false)
			{
				$query  	= 	'UPDATE cp_coord_serv SET cancel="'.$can.'" WHERE id = "'.$cData['id'].'"';

				$change 	=	DBSmart::DataExecute($query);

				return ($change <> false ) ? true : false;

			}else{	return false;	}

		}else{	return false; }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function LogCoord($iData)
	{
		$_replace 	= 	new Config();

		$query		=	'SELECT (IF(cont_id = "1", "SI", "NO")) AS contacto, (SELECT name FROM data_coord_contact WHERE id = reason_id) AS razon, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_contact_tmp WHERE ticket = "'.$iData['ticket'].'"';

		$contact 	=	DBSmart::DBQueryAll($query);

		$query 		=	'SELECT (IF(resp_id = "1", "SI", "NO")) AS contacto, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_view_tmp WHERE ticket = "'.$iData['ticket'].'"';

		$view 		=	DBSmart::DBQueryAll($query);

		$query 	=	'SELECT (IF(coord_id = "1", "SI", "NO")) AS contacto, (SELECT name FROM data_coord_pre_reason WHERE id = reason_id) AS razon, pre_date, pre_disp, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_pre_tmp WHERE ticket = "'.$iData['ticket'].'"';

		$pre 	=	DBSmart::DBQueryAll($query);

		$query 	=	'SELECT (IF(resp_id = "1", "SI", "NO")) AS contacto, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_proc_tmp WHERE ticket = "'.$iData['ticket'].'"';

		$coord 	=	DBSmart::DBQueryAll($query);

		$query 	=	'SELECT (IF(resp_id = "1", "SI", "NO")) AS contacto, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_ref_tmp WHERE ticket = "'.$iData['ticket'].'"';

		$ref 	=	DBSmart::DBQueryAll($query);

		$query 	=	'SELECT (IF(reason_id = "1", "SI", "NO")) AS contacto, (SELECT name FROM data_cancel_coord_objection WHERE id = reason_id) AS razon, (SELECT username FROM users WHERE id = operator_id) AS usuario, created_at AS fecha FROM cp_coord_cancel_tmp WHERE ticket = "'.$iData['ticket'].'"';

		$cancel =	DBSmart::DBQueryAll($query);

		return Coordination::LogCoordHtml($contact, $view, $pre, $coord, $ref, $cancel);

	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function DriveCoord($iData)
	{
		$_replace 	= 	new Config();

		$query 		=	'SELECT t1.ticket, t1.client_id, t3.created_at AS created, t2.name, t2.phone_main, (CASE	WHEN SUBSTRING(t1.ticket,1,2) = "TV" THEN "TELEVISION PR"	WHEN SUBSTRING(t1.ticket,1,2) = "SE" THEN "SEGURIDAD PR" WHEN SUBSTRING(t1.ticket,1,2) = "IN" THEN "INTERNET PR" WHEN SUBSTRING(t1.ticket,1,2) = "IV" THEN "INTERNET VZLA" ELSE ""	END) AS servicio, (SELECT id FROM users WHERE id = t4.owen_id) AS sale_id, (SELECT username FROM users WHERE id = t4.owen_id) AS sale, (SELECT name FROM data_departament WHERE id = sale_id) AS sale_dep, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, t6.name AS score, t1.created_at FROM cp_coord_serv AS t1	INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN cp_service_pr_tv AS t3 ON (t1.ticket = t3.ticket)	INNER JOIN cp_serv_prop AS t4 ON (t1.ticket = t4.ticket_id)	INNER JOIN cp_appro_serv AS t5 ON (t1.ticket = t5.ticket)	INNER JOIN data_score AS t6 ON (t5.score =  t6.id) AND t1.ticket = "TV10001"';

		$general 	=	DBSmart::DBQuery($query);

		$query 		=	'SELECT (IF(cont_id = "1", "SI", "NO")) AS contacto,  (SELECT name FROM data_coord_contact WHERE id = reason_id) AS razon, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_contact_tmp WHERE id = "'.$iData['contact'].'"';

		$contact 	=	DBSmart::DBQuery($query);

		$query 		=	'SELECT (IF(resp_id = "1", "SI", "NO")) AS contacto, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_view_tmp WHERE id = "'.$iData['view'].'"';

		$view 		=	DBSmart::DBQuery($query);

		$query 	=	'SELECT (IF(coord_id = "1", "SI", "NO")) AS contacto, (SELECT name FROM data_coord_pre_reason WHERE id = reason_id) AS razon, pre_date, pre_disp, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_pre_tmp WHERE id = "'.$iData['pre'].'"';

		$pre 	=	DBSmart::DBQuery($query);

		$query 	=	'SELECT (IF(resp_id = "1", "SI", "NO")) AS contacto, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_proc_tmp WHERE id = "'.$iData['coord'].'"';

		$coord 	=	DBSmart::DBQuery($query);

		$query 	=	'SELECT (IF(resp_id = "1", "SI", "NO")) AS contacto, (SELECT username FROM users WHERE id = created_by) AS usuario, created_at AS fecha FROM cp_coord_ref_tmp WHERE id = "'.$iData['ref'].'"';

		$ref 	=	DBSmart::DBQuery($query);

		$query 	=	'SELECT (IF(reason_id = "1", "SI", "NO")) AS contacto, (SELECT name FROM data_cancel_coord_objection WHERE id = reason_id) AS razon, (SELECT username FROM users WHERE id = operator_id) AS usuario, created_at AS fecha FROM cp_coord_cancel_tmp WHERE id = "'.$iData['cancel'].'"';

		$cancel =	DBSmart::DBQuery($query);

		return [
			'general'	=>	$general,
			'contact'	=>	$contact,
			'view'		=>	$view,
			'pre' 		=>	$pre,
			'coord'		=>	$coord,
			'ref'		=>	$ref,
			'cancel'	=>	$cancel
		];

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function GetInfoService($ticket)
	{
		$query 	=	'SELECT (SELECT pre_date FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_fecha, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_pre_tmp WHERE id = pre_id)) AS c_pre_operador, (SELECT created_at FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_fecha_coord FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	}


////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function CoordPendClient($iData)
   	{
   		$_replace 	= 	new Config();
   		$html 		= 	"";


   		if($iData <> false)
   		{
			$html.='<div class="table-responsive">';
			$html.='<table id="CoordPendTable" class="table table-bordered" border="1" style="font-size: 10px;">';
			$html.='<thead>';
			$html.='<tr>';
			$html.='<th style="text-align: center;">Ticket</th>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Cliente</th>';
			$html.='<th style="text-align: center;">Tel&eacute;fono</th>';
			$html.='<th style="text-align: center;">Ref</th>';
			$html.='<th style="text-align: center;">Tipo</th>';
			$html.='<th style="text-align: center;">Servicio</th>';
			$html.='<th style="text-align: center;">Vendedor</th>';
			$html.='<th style="text-align: center;">Depart</th>';
			$html.='<th style="text-align: center;">Score</th>';
			$html.='<th style="text-align: center;">Status</th>';
			$html.='<th style="text-align: center;">Procesado</th>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Proceso</th>';
			$html.='</tr>';
			$html.='</thead>';

            $html.='<tbody >';

	   		foreach ($iData as $in => $inf) 
	   		{
	   			if($inf <> '')
	   			{
					foreach ($inf as $k => $val)  
		            {
				   		switch ($val['service']) 
				        {
				        	case 'TV':
				        		$servHtml =	'<span class="label label-primary">TELEVISION PR</span>';
				        		break;
				        	case 'SE':
				        		$servHtml = '<span class="label label-danger">SEGURIDAD PR</span>';
				        		break;
				        	case 'IN':
				        		$servHtml = '<span class="label label-success">INTERNET PR</span>';
				        		break;
				        	case 'IV':
				        		$servHtml = '<span class="label label-success">INTERNET VZLA</span>';
				        		break;
				        }

				        $refClient 	=	($val['referred_id'] <> "0") 	? 	$val['referred_id'] : '';
				        $refHtml 	=	($val['referred_id'] <> "0") 	?	'<span class="label label-danger">SI</span>' 	:	'';

				        $date = ($val['proc_date'] <> "" ) ? $_replace->ShowDateAll($val['proc_date']) : '';

						$html.='<tr>';
						$html.='<td>'.$val['ticket'].'</td>';
						$html.='<td>'.$_replace->ShowDateAll($val['created']).'</td>';
						$html.='<td>'.$val['name'].'</td>';
						$html.='<td>'.$val['phone_main'].'</td>';
						$html.='<td>'.$refHtml.'</td>';
						$html.='<td>'.$val['type_acc'].'</td>';
						$html.='<td>'.$servHtml.'</td>';
						$html.='<td>'.$val['sale'].'</td>';
						$html.='<td>'.$val['sale_dep'].'</td>';
						$html.='<td>'.$val['score'].'</td>';
						$html.='<td><span class="label label-success">'.$val['reason'].'</span></td>';
						$html.='<td>'.$val['username'].'</td>';
						$html.='<td>'.$date.'</td>';
						$html.='<td>';
						$html.='<table>';
						$html.='<tbody>';
						$html.='<tr>';
						$html.='<td><a onclick="ViewCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-phone"></i></a></td>';
						$html.='<td><a onclick="InfoCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-info-circle"></i></a></td>';
						$html.='<td><a onclick="PackageCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a></td>';
						$html.='</tr>';
						$html.='<tr>';
						$html.='<td><a onclick="NotesCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-comments"></i></a></td>';
						$html.='<td><a class="btn btn-xs btn-default" href="/leads/view/'.(int)$val['client_id'].'"><i class="fa fa-eye"></i></a></td>';
						$html.='<td><a onclick="PreCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-calendar"></i></a></td>';
						$html.='</tr>';
						$html.='</tbody>';
						$html.='</table>';
						$html.='</td>';
						$html.='</tr>';
		            }
	   			}
	   		}

			$html.='</tbody>';
			$html.='</table>';
			$html.='</div>';
			
			return 	$html;

   		}else{	return $html;	}

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function CoordSegClient($iData)
   	{
   		$_replace 	= 	new Config();
   		$html 		= 	"";


   		if($iData <> false)
   		{
			$html.='<div class="table-responsive">';
			$html.='<table id="CoordSegTable" class="table table-bordered" border="1" style="font-size: 10px;">';
			$html.='<thead>';
			$html.='<tr>';
			$html.='<th style="text-align: center;">Ticket</th>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Cliente</th>';
			$html.='<th style="text-align: center;">Tel&eacute;fono</th>';
			$html.='<th style="text-align: center;">Ref</th>';
			$html.='<th style="text-align: center;">Tipo</th>';
			$html.='<th style="text-align: center;">Servicio</th>';
			$html.='<th style="text-align: center;">Vendedor</th>';
			$html.='<th style="text-align: center;">Depart</th>';
			$html.='<th style="text-align: center;">Score</th>';
			$html.='<th style="text-align: center;">Status</th>';
			$html.='<th style="text-align: center;">Procesado</th>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Edici&oacute;n</th>';
			$html.='<th style="text-align: center;">Proceso</th>';
			$html.='</tr>';
			$html.='</thead>';

            $html.='<tbody >';

	   		foreach ($iData as $in => $inf) 
	   		{
	   			if($inf <> '')
	   			{
					foreach ($inf as $k => $val)  
		            {
				   		switch ($val['service']) 
				        {
				        	case 'TV':
				        		$servHtml =	'<span class="label label-primary">TELEVISION PR</span>';
				        		break;
				        	case 'SE':
				        		$servHtml = '<span class="label label-danger">SEGURIDAD PR</span>';
				        		break;
				        	case 'IN':
				        		$servHtml = '<span class="label label-success">INTERNET PR</span>';
				        		break;
				        	case 'IV':
				        		$servHtml = '<span class="label label-success">INTERNET VZLA</span>';
				        		break;
				        }

				        $refClient 	=	($val['referred_id'] <> "0") 	? 	$val['referred_id'] : '';
				        $refHtml 	=	($val['referred_id'] <> "0") 	?	'<span class="label label-danger">SI</span>' 	:	'';

				        $date = ($val['proc_date'] <> "" ) ? $_replace->ShowDateAll($val['proc_date']) : '';

						$html.='<tr>';
						$html.='<td>'.$val['ticket'].'</td>';
						$html.='<td>'.$_replace->ShowDateAll($val['created']).'</td>';
						$html.='<td>'.$val['name'].'</td>';
						$html.='<td>'.$val['phone_main'].'</td>';
						$html.='<td>'.$refHtml.'</td>';
						$html.='<td>'.$val['type_acc'].'</td>';
						$html.='<td>'.$servHtml.'</td>';
						$html.='<td>'.$val['sale'].'</td>';
						$html.='<td>'.$val['sale_dep'].'</td>';
						$html.='<td>'.$val['score'].'</td>';
						$html.='<td><span class="label label-success">'.$val['reason'].'</span></td>';
						$html.='<td>'.$val['username'].'</td>';
						$html.='<td>'.$date.'</td>';

						$html.='<td>';
						$html.='<table>';
						$html.='<tbody>';
						$html.='<tr>';
						$html.='<td><a onclick="ViewCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-phone"></i></a></td>';
						$html.='<td><a onclick="InfoCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-info-circle"></i></a></td>';
						$html.='<td><a onclick="PackageCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a></td>';
						$html.='</tr>';
						$html.='<tr>';
						$html.='<td><a onclick="NotesCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-comments"></i></a></td>';
						$html.='<td><a class="btn btn-xs btn-default" href="/leads/view/'.(int)$val['client_id'].'"><i class="fa fa-eye"></i></a></td>';
						$html.='<td><a onclick="PreCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-calendar"></i></a></td>';
						$html.='</tr>';
						$html.='</tbody>';
						$html.='</table>';
						$html.='</td>';

						$html.='<td>';
						$html.='<table>';
						$html.='<tbody>';
						$html.='<tr>';
						$html.='<td><a onclick="ProcessCoord('.(int)$val['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-check"></i></a></td>';
						$html.='<td><a onclick="RefProcessCoord('.(int)$val['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-database"></i></a></td>';
						$html.='</tr>';
						$html.='<tr>';
						$html.='<td><a onclick="LogCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-file-archive-o"></i></a></td>';
						$html.='<td><a onclick="CancelCoord('.(int)$val['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-times"></i></a></td>';
						$html.='</tr>';
						$html.='</tbody>';
						$html.='</table>';
						$html.='</td>';

						$html.='</tr>';
		            }
	   			}
	   		}

			$html.='</tbody>';
			$html.='</table>';
			$html.='</div>';
			
			return 	$html;

   		}else{	return $html;	}

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function CoordProcClient($iData)
   	{
   		$_replace 	= 	new Config();
   		$html 		= 	"";

   		if($iData <> false)
   		{
			$html.='<div class="table-responsive">';
			$html.='<table id="CoordProcTable" class="table table-bordered" border="1" style="font-size: 10px;">';
			$html.='<thead>';
			$html.='<tr>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Ticket</th>';
			$html.='<th style="text-align: center;">Cliente</th>';
			$html.='<th style="text-align: center;">Tel&eacute;fono</th>';
			$html.='<th style="text-align: center;">Servicio</th>';
			$html.='<th style="text-align: center;">Vendedor</th>';
			$html.='<th style="text-align: center;">Depart</th>';
			$html.='<th style="text-align: center;">Score</th>';
			$html.='<th style="text-align: center;">Procesado</th>';
			$html.='<th style="text-align: center;">Coordinaci&oacute;n</th>';
			$html.='<th style="text-align: center;">Disponibilidad</th>';
			$html.='<th style="text-align: center;">Accion</th>';
			$html.='</tr>';
			$html.='</thead>';

            $html.='<tbody >';

	   		foreach ($iData as $in => $inf) 
	   		{
	   			if($inf <> '')
	   			{
					foreach ($inf as $k => $val)  
		            {
				   		switch ($val['service']) 
				        {
				        	case 'TV':
				        		$servHtml =	'<span class="label label-primary">TELEVISION PR</span>';
				        		break;
				        	case 'SE':
				        		$servHtml = '<span class="label label-danger">SEGURIDAD PR</span>';
				        		break;
				        	case 'IN':
				        		$servHtml = '<span class="label label-success">INTERNET PR</span>';
				        		break;
				        	case 'IV':
				        		$servHtml = '<span class="label label-success">INTERNET VZLA</span>';
				        		break;
				        }

						$html.='<tr>';
						$html.='<td>'.$_replace->ShowDateAll($val['created_at']).'</td>';
						$html.='<td>'.$val['ticket'].'</td>';
						$html.='<td>'.$val['name'].'</td>';
						$html.='<td>'.$val['phone_main'].'</td>';
						$html.='<td>'.$servHtml.'</td>';
						$html.='<td>'.$val['sale'].'</td>';
						$html.='<td>'.$val['sale_dep'].'</td>';
						$html.='<td>'.$val['score'].'</td>';
						$html.='<td>'.$val['CoordUser'].'</td>';
						$html.='<td>'.$_replace->ShowDate($val['preDate']).'</td>';
						$html.='<td>'.$val['preDisp'].'</td>';
						
						$html.='<td>';
						$html.='<table>';
						$html.='<tbody>';
						$html.='<tr>';
						$html.='<td><a class="btn btn-xs btn-default" href="/leads/view/'.$val['client_id'].'"><i class="fa fa-eye"></i></a></td>';
						$html.='<td><a onclick="NotesCoord('.(int)$val['client_id'].')" class="btn btn-xs btn-default"><i class="fa fa-comments"></i></a></td>';
						$html.='<td><a onclick="LogCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-file-archive-o"></i></a></td>';
						$html.='<td><a onclick="InfoCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-info-circle"></i></a></td>';
						$html.='<td><a onclick="PackageCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a></td>';
						$html.='</tr>';
						$html.='</tbody>';
						$html.='</table>';
						$html.='</td>';

						$html.='</tr>';
		            }
	   			}
	   		}

			$html.='</tbody>';
			$html.='</table>';
			$html.='</div>';
			
			return 	$html;

   		}else{	return $html;	}

   	}

   	public static function CoordinationProcClient($iData)
   	{
   		$_replace 	= 	new Config();
   		$html 		= 	"";

   		if($iData <> false)
   		{
			$html.='<div class="table-responsive">';
			$html.='<table id="CoordProcTable" class="table table-bordered" border="1" style="font-size: 10px;">';
			$html.='<thead>';
			$html.='<tr>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Ticket</th>';
			$html.='<th style="text-align: center;">ID</th>';
			$html.='<th style="text-align: center;">Cliente</th>';
			$html.='<th style="text-align: center;">Tel&eacute;fono</th>';
			$html.='<th style="text-align: center;">Servicio</th>';
			$html.='<th style="text-align: center;">Vendedor</th>';
			$html.='<th style="text-align: center;">Depart</th>';
			$html.='<th style="text-align: center;">Score</th>';
			$html.='<th style="text-align: center;">Procesado</th>';
			$html.='<th style="text-align: center;">Fecha Coord.</th>';
			$html.='<th style="text-align: center;">Disponibilidad</th>';
			$html.='<th style="text-align: center;">Accion</th>';
			$html.='</tr>';
			$html.='</thead>';

            $html.='<tbody >';

	   		foreach ($iData as $in => $inf) 
	   		{
	   			if($inf <> '')
	   			{
					foreach ($inf as $k => $val)  
		            {
				   		switch ($val['service']) 
				        {
				        	case 'TV':
				        		$servHtml =	'<span class="label label-primary">TELEVISION PR</span>';
				        		break;
				        	case 'SE':
				        		$servHtml = '<span class="label label-danger">SEGURIDAD PR</span>';
				        		break;
				        	case 'IN':
				        		$servHtml = '<span class="label label-success">INTERNET PR</span>';
				        		break;
				        	case 'IV':
				        		$servHtml = '<span class="label label-success">INTERNET VZLA</span>';
				        		break;
				        }

						$html.='<tr>';
						$html.='<td>'.$_replace->ShowDateAll($val['created_at']).'</td>';
						$html.='<td>'.$val['ticket'].'</td>';
						$html.='<td>'.$val['client_id'].'</td>';
						$html.='<td>'.$val['name'].'</td>';
						$html.='<td>'.$val['phone_main'].'</td>';
						$html.='<td>'.$servHtml.'</td>';
						$html.='<td>'.$val['sale'].'</td>';
						$html.='<td>'.$val['sale_dep'].'</td>';
						$html.='<td>'.$val['score'].'</td>';
						$html.='<td>'.$val['CoordUser'].'</td>';
						$html.='<td>'.$_replace->ShowDate($val['preDate']).'</td>';
						$html.='<td>'.$val['preDisp'].'</td>';
						
						$html.='<td>';
						$html.='<table>';
						$html.='<tbody>';
						$html.='<tr>';
						$html.='<td><a class="btn btn-xs btn-default" href="/leads/view/'.$val['client_id'].'"><i class="fa fa-eye"></i></a></td>';
						$html.='<td><a onclick="NotesCoordOther('.(int)$val['client_id'].')" class="btn btn-xs btn-default"><i class="fa fa-comments"></i></a></td>';
						$html.='<td><a onclick="LogCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-file-archive-o"></i></a></td>';
						$html.='<td><a onclick="InfoCoordination('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-info-circle"></i></a></td>';
						$html.='<td><a onclick="PackageCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a></td>';
						$html.='</tr>';
						$html.='</tbody>';
						$html.='</table>';
						$html.='</td>';

						$html.='</tr>';
		            }
	   			}
	   		}

			$html.='</tbody>';
			$html.='</table>';
			$html.='</div>';
			
			return 	$html;

   		}else{	return $html;	}

   	}



////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function CoordCancelClient($iData)
   	{
   		$_replace 	= 	new Config();
   		$html 		= 	"";

   		if($iData <> false)
   		{
			$html.='<div class="table-responsive">';
			$html.='<table id="CoordCancelTable" class="table table-bordered" border="1" style="font-size: 10px;">';
			$html.='<thead>';
			$html.='<tr>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Ticket</th>';
			$html.='<th style="text-align: center;">Cliente</th>';
			$html.='<th style="text-align: center;">Tel&eacute;fono</th>';
			$html.='<th style="text-align: center;">Servicio</th>';
			$html.='<th style="text-align: center;">Vendedor</th>';
			$html.='<th style="text-align: center;">Depart</th>';
			$html.='<th style="text-align: center;">Score</th>';
			$html.='<th style="text-align: center;">Procesado</th>';
			$html.='<th style="text-align: center;">Raz&oacute;n.</th>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Accion</th>';
			$html.='</tr>';
			$html.='</thead>';

            $html.='<tbody >';

	   		foreach ($iData as $in => $inf) 
	   		{
	   			if($inf <> '')
	   			{
					foreach ($inf as $k => $val)  
		            {
				   		switch ($val['service']) 
				        {
				        	case 'TV':
				        		$servHtml =	'<span class="label label-primary">TELEVISION PR</span>';
				        		break;
				        	case 'SE':
				        		$servHtml = '<span class="label label-danger">SEGURIDAD PR</span>';
				        		break;
				        	case 'IN':
				        		$servHtml = '<span class="label label-success">INTERNET PR</span>';
				        		break;
				        	case 'IV':
				        		$servHtml = '<span class="label label-success">INTERNET VZLA</span>';
				        		break;
				        }

						$html.='<tr>';
						$html.='<td>'.$_replace->ShowDateAll($val['tCreated']).'</td>';
						$html.='<td>'.$val['ticket'].'</td>';
						$html.='<td>'.$val['name'].'</td>';
						$html.='<td>'.$val['phone_main'].'</td>';
						$html.='<td>'.$servHtml.'</td>';
						$html.='<td>'.$val['sale'].'</td>';
						$html.='<td>'.$val['sale_dep'].'</td>';
						$html.='<td>'.$val['score'].'</td>';
						$html.='<td>'.$val['CancelUser'].'</td>';
						$html.='<td>'.$val['reason'].'</td>';
						$html.='<td>'.$_replace->ShowDateAll($val['cCancel']).'</td>';
						
						$html.='<td>';
						$html.='<table>';
						$html.='<tbody>';
						$html.='<tr>';
						$html.='<td><a class="btn btn-xs btn-default" href="/leads/view/'.$val['client_id'].'"><i class="fa fa-eye"></i></a></td>';
						$html.='<td><a onclick="NotesCoordOther('.(int)$val['client_id'].')" class="btn btn-xs btn-default"><i class="fa fa-comments"></i></a></td>';
						$html.='<td><a onclick="LogCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-file-archive-o"></i></a></td>';
						$html.='</tr>';
						$html.='</tbody>';
						$html.='</table>';
						$html.='</td>';

						$html.='</tr>';
		            }
	   			}
	   		}

			$html.='</tbody>';
			$html.='</table>';
			$html.='</div>';
			
			return 	$html;

   		}else{	return $html;	}

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function CoordRefClient($iData)
   	{
   		$_replace 	= 	new Config();
   		$html 		= 	"";

   		if($iData <> false)
   		{
			$html.='<div class="table-responsive">';
			$html.='<table id="CoordReferredTable" class="table table-bordered" border="1" style="font-size: 10px;">';
			$html.='<thead>';
			$html.='<tr>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Ticket</th>';
			$html.='<th style="text-align: center;">Cliente</th>';
			$html.='<th style="text-align: center;">Tel&eacute;fono</th>';
			$html.='<th style="text-align: center;">Servicio</th>';
			$html.='<th style="text-align: center;">Vendedor</th>';
			$html.='<th style="text-align: center;">Depart</th>';
			$html.='<th style="text-align: center;">Score</th>';
			$html.='<th style="text-align: center;">Procesado</th>';
			$html.='<th style="text-align: center;">Fecha</th>';
			$html.='<th style="text-align: center;">Accion</th>';
			$html.='</tr>';
			$html.='</thead>';

            $html.='<tbody >';

	   		foreach ($iData as $in => $inf) 
	   		{
	   			if($inf <> '')
	   			{
					foreach ($inf as $k => $val)  
		            {
				   		switch ($val['service']) 
				        {
				        	case 'TV':
				        		$servHtml =	'<span class="label label-primary">TELEVISION PR</span>';
				        		break;
				        	case 'SE':
				        		$servHtml = '<span class="label label-danger">SEGURIDAD PR</span>';
				        		break;
				        	case 'IN':
				        		$servHtml = '<span class="label label-success">INTERNET PR</span>';
				        		break;
				        	case 'IV':
				        		$servHtml = '<span class="label label-success">INTERNET VZLA</span>';
				        		break;
				        }

						$html.='<tr>';
						$html.='<td>'.$_replace->ShowDateAll($val['tCreated']).'</td>';
						$html.='<td>'.$val['ticket'].'</td>';
						$html.='<td>'.$val['name'].'</td>';
						$html.='<td>'.$val['phone_main'].'</td>';
						$html.='<td>'.$servHtml.'</td>';
						$html.='<td>'.$val['sale'].'</td>';
						$html.='<td>'.$val['sale_dep'].'</td>';
						$html.='<td>'.$val['score'].'</td>';
						$html.='<td>'.$val['c_ref_user'].'</td>';
						$html.='<td>'.$_replace->ShowDateAll($val['rCancel']).'</td>';
						
						$html.='<td>';
						$html.='<table>';
						$html.='<tbody>';
						$html.='<tr>';
						$html.='<td><a class="btn btn-xs btn-default" href="/leads/view/'.$val['client_id'].'"><i class="fa fa-eye"></i></a></td>';
						$html.='<td><a onclick="NotesCoordOther('.(int)$val['client_id'].')" class="btn btn-xs btn-default"><i class="fa fa-comments"></i></a></td>';
						$html.='<td><a onclick="LogCoord('.(int)$val['id'].')" class="btn btn-xs btn-default"><i class="fa fa-file-archive-o"></i></a></td>';
						$html.='</tr>';
						$html.='</tbody>';
						$html.='</table>';
						$html.='</td>';
						
						$html.='</tr>';
		            }
	   			}
	   		}

			$html.='</tbody>';
			$html.='</table>';
			$html.='</div>';
			
			return 	$html;

   		}else{	return $html;	}

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////


   	public static function InfoViewClient($info, $type, $score)
   	{
   		$html = '';
   		$_replace 	= 	new Config();

		if($type == 'referred')
		{
			$html.='<div class="row">';
			$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
			$html.='<div class="alert alert-danger fade in"><i class="fa-fw fa fa-times"></i><strong>Datos de Cliente Principal!</strong> Esta Solicitud muestra los Datos del Cliente Principal.</div>';
			$html.='</div">';
			$html.='</div">';
		}

		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
	
		if($type == 'client')
		{
			$html.='<header style="text-align: center;">INFORMACION DE CLIENTE</header>';
		}else{
			$html.='<header style="text-align: center;">INFORMACION DEL REFERIDO</header>';
		}

		$html.='<table class="table table-bordered" style="background: #ecf8ff; font-size: 11px;" >';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>NOMBRE</strong></td>';
		$html.='<td>'.$info['name'].'</td>';
		$html.='<td><strong>CUMPLEA&Ntilde;OS</strong></td>';
		$html.='<td>'.$_replace->ShowDate($info['birthday']).'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TELEFONO</strong></td>';
		$html.='<td>'.$info['phone'].'</td>';
		$html.='<td><strong>PROVEEDOR</strong></td>';
		$html.='<td>'.$info['p_provider'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>ALTERNATIVO</strong></td>';
		$html.='<td>'.$info['p_alter'].'</td>';
		$html.='<td><strong>PROVEEDOR</strong></td>';
		$html.='<td>'.$info['a_provider'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>OTRO</strong></td>';
		$html.='<td>'.$info['p_other'].'</td>';
		$html.='<td><strong>PROVEEDOR</strong></td>';
		$html.='<td>'.$info['o_provider'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CIUDAD</strong></td>';
		$html.='<td>'.$info['town'].'</td>';
		$html.='<td><strong>PAIS</strong></td>';
		$html.='<td>'.$info['country'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';

		$html.='<tr>';
		$html.='<td><strong>TECHO</strong></td>';
		$html.='<td>'.$info['ceiling'].'</td>';
		$html.='<td><strong>NIVELES</strong></td>';
		$html.='<td>'.$info['level'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';


		$html.='<tr>';
		$html.='<td><strong>DIRECCI&Oacute;N</strong></td>';
		$html.='<td colspan="5">'.$info['add'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>POSTAL</strong></td>';
		$html.='<td colspan="5">'.$info['add_postal'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$info['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$info['coord_lo'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>SEGURO SOCIAL</strong></td>';
		$html.='<td>'.$info['ss'].'</td>';
		$html.='<td><strong>FECHA EXPIRACI&Oacute;N</strong></td>';
		$html.='<td>'.$info['ss_exp'].'</td>';
		$html.='<td><strong>TIPO</strong></td>';
		$html.='<td>'.$info['ss_type'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>ID</strong></td>';
		$html.='<td>'.$info['id'].'</td>';
		$html.='<td><strong>FECHA EXPIRACI&Oacute;N</strong></td>';
		$html.='<td>'.$info['id_exp'].'</td>';
		$html.='<td><strong>TIPO</strong></td>';
		$html.='<td>'.$info['id_type'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TARJETA DE CREDITO</strong></td>';
		$html.='<td>'.$info['cc'].'</td>';
		$html.='<td><strong>FECHA EXPIRACI&Oacute;N</strong></td>';
		$html.='<td>'.$info['cc_exp'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>NOMBRE DE LA CUENTA</strong></td>';
		$html.='<td>'.$info['ab'].'</td>';
		$html.='<td><strong>NUMERO DE CUENTA</strong></td>';
		$html.='<td>'.$info['ab_num'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>NOMBRE DEL BANCO</strong></td>';
		$html.='<td>'.$info['ab_bank'].'</td>';
		$html.='<td><strong>TIPO DE CUENTA</strong></td>';
		$html.='<td>'.$info['ab_type'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>SCORE</strong></td>';
		$html.='<td>'.$score['score'].'</td>';
		$html.='<td><strong>TRANSACTIONS</strong></td>';
		$html.='<td>'.$score['transaction'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EMAIL</strong></td>';
		$html.='<td colspan="5">'.$info['email'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INFORMACI&Oacute;N ADICIONAL</strong></td>';
		$html.='<td colspan="5">'.$info['additional'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

   		return $html;
   	}

///////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServTvPR($info)
   	{
   		$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>TV</strong></td>';
		$html.='<td>'.$info['tv'].'</td>';
		$html.='<td><strong>CAJA REGULAR</strong></td>';
		$html.='<td>'.$info['decoder_id'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PAQUETE</strong></td>';
		$html.='<td>'.$info['package'].'</td>';
		$html.='<td><strong>CAJA DVR</strong></td>';
		$html.='<td>'.$info['dvr_id'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PROVEEDOR ANTERIOR</strong></td>';
		$html.='<td>'.$info['provider'].'</td>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].' PAYMENT</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>HD</strong></td>';
		$html.='<td>'.$info['ch_hd'].'</td>';
		$html.='<td><strong>DVR</strong></td>';
		$html.='<td>'.$info['ch_dvr'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>HBO</strong></td>';
		$html.='<td>'.$info['ch_hbo'].'</td>';
		$html.='<td><strong>CINEMAX</strong></td>';
		$html.='<td>'.$info['ch_cinemax'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>STARTZ</strong></td>';
		$html.='<td>'.$info['ch_starz'].'</td>';
		$html.='<td><strong>SHOWTIME</strong></td>';
		$html.='<td>'.$info['ch_showtime'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>GIFT ENTERTAINMENT</strong></td>';
		$html.='<td>'.$info['gif_ent'].'</td>';
		$html.='<td><strong>GIFT CHOICE</strong></td>';
		$html.='<td>'.$info['gif_choice'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>GIFT XTRA</strong></td>';
		$html.='<td>'.$info['gif_xtra'].'</td>';
		$html.='<td><strong>PUBLICIDAD AT&T</strong></td>';
		$html.='<td>'.$info['advertising'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServSecPR($info)
   	{
   	   	$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>TIENE SISTEMA DE SEGURIDAD PREVIAMENTE</strong></td>';
		$html.='<td>'.$info['sist_previo'].'</td>';
		$html.='<td><strong>EMPRESA DE MONITOREO</strong></td>';
		$html.='<td>'.$info['compania'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CAMARAS</strong></td>';
		$html.='<td>'.$info['camaras'].'</td>';
		$html.='<td><strong>DVR</strong></td>';
		$html.='<td>'.$info['dvr'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PASSWORD DE ALARMA</strong></td>';
		$html.='<td>'.$info['password_alarm'].'</td>';
		$html.='<td><strong>METODO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DESCRIPCI&Oacute;N DE EQUIPOS</strong></td>';
		$html.='<td colspan="4">'.$info['equipment'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EQUIPOS ADICIONALES</strong></td>';
		$html.='<td colspan="4">'.$info['adicional'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACT 1</strong></td>';
		$html.='<td>'.$info['contact_1'].'</td>';
		$html.='<td><strong>DESCRIPCI&Oacute;N</strong></td>';
		$html.='<td>'.$info['contact_1_desc'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACT 2</strong></td>';
		$html.='<td>'.$info['contact_2'].'</td>';
		$html.='<td><strong>DESCRIPCI&Oacute;N</strong></td>';
		$html.='<td>'.$info['contact_2_desc'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACT 3</strong></td>';
		$html.='<td>'.$info['contact_2_desc'].'</td>';
		$html.='<td><strong>DESCRIPCI&Oacute;N</strong></td>';
		$html.='<td>'.$info['contact_3_desc'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;	
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServIntPR($info, $data)
   	{
   		$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['int_res'].'</td>';
		$html.='<td><strong>TELEFONO RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['pho_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET COMERCIAL</strong></td>';
		$html.='<td>'.$info['int_com'].'</td>';
		$html.='<td><strong>TELEFONO COMERCIAL</strong></td>';
		$html.='<td>'.$info['pho_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$data['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$data['coord_lo'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='<td><strong></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;

   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

   	public static function InfoServIntVZ($info, $data)
   	{

  		$html = "";
		$html.='<div class="row">';
		$html.='<div class="col-xs-12 col-col-sm-12 col-col-md-12 col-col-lg-12">';
		$html.='<header style="text-align: center;">DATOS DEL SERVICIO</header>';
		$html.='<table class="table table-bordered" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['int_res'].'</td>';
		$html.='<td><strong>TELEFONO RESIDENCIAL</strong></td>';
		$html.='<td>'.$info['phone_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INTERNET COMERCIAL</strong></td>';
		$html.='<td>'.$info['int_com'].'</td>';
		$html.='<td><strong>TELEFONO COMERCIAL</strong></td>';
		$html.='<td>'.$info['phone_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORDENADAS LATITUD</strong></td>';
		$html.='<td>'.$data['coord_la'].'</td>';
		$html.='<td><strong>COORDENADAS LONGITUD</strong></td>';
		$html.='<td>'.$data['coord_lo'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$info['payment'].'</td>';
		$html.='<td><strong>BANCO</strong></td>';
		$html.='<td>'.$info['bank'].'</td>';
		$html.='</tr>';
		$html.='<td><strong>MONTO</strong></td>';
		$html.='<td>'.$info['amount'].'</td>';
		$html.='<td><strong>TRANSACCION</strong></td>';
		$html.='<td>'.$info['trans_date'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EMAIL</strong></td>';
		$html.='<td colspan="4">'.$info['email'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CAMPO ADICIONAL</strong></td>';
		$html.='<td colspan="4">'.$info['additional'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';

		return $html;
   	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewCancel($cancel, $id)
	{
		
		$html = '';
		$html.='<div class="widget-body" >';
		$html.='<input type="text" id="id_can_coord" name="id_can_coord" value="'.$id.'" class="form-control hidden"/>';
		if($cancel <> false)
		{
			$html.='<fieldset style="background: aliceblue;">';
			$html.='<section>';
			$html.='<label class="label"><h1 style="text-align: center;">RAZON DE CANCELACI&Oacute;N</h1></label>';
			$html.='<label class="select select-multiple">';
			$html.='<select multiple="" name="cancel_sel_c[]" id="cancel_sel_c" style="height: 250px;">';

			foreach ($cancel as $k => $val) 
			{
				$html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';
			}
			$html.='</select> </label>';
			$html.='<div class="note">';
			$html.='<strong>Nota:</strong> Selecciona una o mas Opciones..';
			$html.='</div>';
			$html.='</section>';
			$html.='</fieldset>';

		}				

		$html.='<fieldset style="background: aliceblue;">';
		$html.='<section>';
		$html.='<label class="label">Nota</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="3" name="cancel_note_c" id="cancel_note_c" class="custom-scroll"></textarea> ';
		$html.='</label>';
		$html.='</section>';
		$html.='</fieldset>	';
		$html.='</div>';

		return $html;

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function LogCoordHtml($contact, $view, $pre, $coord, $ref, $cancel)
	{
		$_replace 	= 	new Config();
		$html = '';
		
		$html.='<div class="table-responsive">';
		$html.='<table id="TableLogCoordHtml" class="table table-bordered" border="1" style="font-size: 10px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th style="text-align: center;">Status</th>';
		$html.='<th style="text-align: center;">Respuesta</th>';
		$html.='<th style="text-align: center;">Raz&oacute;n</th>';
		$html.='<th style="text-align: center;">Coordinaci&oacute;n</th>';
		$html.='<th style="text-align: center;">Disponibilidad</th>';
		$html.='<th style="text-align: center;">Operador</th>';
		$html.='<th style="text-align: center;">Fecha</th>';
		$html.='</tr>';
		$html.='</thead>';

        $html.='<tbody>';

        if($contact <> false)
        {
			foreach ($contact as $c => $cont) 
	   		{

				$html.='<tr>';
				$html.='<td>CONTACTADO</td>';
				$html.='<td>'.$cont['contacto'].'</td>';
				$html.='<td>'.$cont['razon'].'</td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td>'.$cont['usuario'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($cont['fecha']).'</td>';
				$html.='</tr>';
	   		}
        }

        if($view <> false)
        {
			foreach ($view as $v => $vi) 
	   		{

				$html.='<tr>';
				$html.='<td>DATOS CLIENTE</td>';
				$html.='<td>'.$vi['contacto'].'</td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td>'.$vi['usuario'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($vi['fecha']).'</td>';
				$html.='</tr>';
	   		}
        }

        if($pre <> false)
        {
			foreach ($pre as $p => $pr) 
	   		{

				$html.='<tr>';
				$html.='<td>PRE COORDINACI&Oacute;N</td>';
				$html.='<td>'.$pr['contacto'].'</td>';
				$html.='<td>'.$pr['razon'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($pr['pre_date']).'</td>';
				$html.='<td>'.$pr['pre_disp'].'</td>';
				$html.='<td>'.$pr['usuario'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($pr['fecha']).'</td>';
				$html.='</tr>';
	   		}
        }

        if($coord <> false)
        {
			foreach ($coord as $co => $cor) 
	   		{

				$html.='<tr>';
				$html.='<td>COORDINACI&Oacute;N</td>';
				$html.='<td>'.$cor['contacto'].'</td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td>'.$cor['usuario'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($cor['fecha']).'</td>';
				$html.='</tr>';
	   		}
        }

        if($ref <> false)
        {
			foreach ($ref as $r => $re) 
	   		{

				$html.='<tr>';
				$html.='<td>BRINDO REFERIDO</td>';
				$html.='<td>'.$re['contacto'].'</td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td>'.$re['usuario'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($re['fecha']).'</td>';
				$html.='</tr>';
	   		}
        }

        if($cancel <> false)
        {
			foreach ($cancel as $ca => $can) 
	   		{

				$html.='<tr>';
				$html.='<td>CANCELACI&Oacute;N</td>';
				$html.='<td>'.$can['contacto'].'</td>';
				$html.='<td>'.$can['razon'].'</td>';
				$html.='<td></td>';
				$html.='<td></td>';
				$html.='<td>'.$can['usuario'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($can['fecha']).'</td>';
				$html.='</tr>';
	   		}
        }

		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		return 	$html;
	}


////////////////////////////////////////////////////////////////////////////////////////////////////

}