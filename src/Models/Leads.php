<?php
namespace App\Models;

use Model;

use App\Models\StatusLead;
use App\Models\CypherData;
use App\Models\Leads;
use App\Models\LeadsTmp;
use App\Models\Town;
use App\Models\Country;
use App\Models\ZipCode;
use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\Notes;
use App\Models\MarketingTmp;
use App\Models\OperatorAssigned;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBSmart;

use PDO;

class Leads extends Model 
{

	static $_table 		= 'cp_leads';

	Public $_fillable 	= array('client_id', 'referred_id', 'name', 'birthday', 'gender', 'age', 'repre_legal', 'type_acc', 'phone_main', 'phone_main_owner', 'phone_main_provider', 'phone_alt', 'phone_alt_owner', 'phone_alt_provider', 'phone_other', 'phone_other_owner', 'phone_other_provider', 'email_main', 'email_other', 'add_main', 'add_postal', 'coor_lati', 'coor_long', 'country_id', 'town_id', 'zip_id', 'h_type_id', 'h_roof_id', 'h_level_id', 'ident_id', 'ident_exp', 'ss_id', 'ss_exp', 'mkt_origen_id', 'mkt_medium_id', 'mkt_objective_id', 'mkt_post_id', 'mkt_forms_id', 'mkt_conv_id', 'mkt_service_id', 'mkt_campaign_id', 'additional', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function GetLeadById($id)
	{


		$query 	=	'SELECT client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_alt, phone_alt_owner, phone_other, phone_other_owner, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_at FROM cp_leads WHERE client_id = "'.$id.'"';

		$lea 	=	DBSmart::DBQuery($query);

		if($lea <> false)
		{
			return [
			'client' 			=> $lea['client_id'],
			'referred' 			=> $lea['referred_id'],
			'name' 				=> $lea['name'],
			'birthday' 			=> $lea['birthday'],
			'gender' 			=> $lea['gender'],
			'age' 				=> $lea['age'],
			'repre_legal'		=> $lea['repre_legal'],
			'type_acc'			=> $lea['type_acc'],
			'phone_main' 		=> $lea['phone_main'],
			'phone_main_owner' 	=> $lea['phone_main_owner'],
			'phone_alt' 		=> $lea['phone_alt'],
			'phone_alt_owner' 	=> $lea['phone_alt_owner'],
			'phone_other' 		=> $lea['phone_other'],
			'phone_other_owner' => $lea['phone_other_owner'],
			'email_main' 		=> $lea['email_main'],
			'email_other' 		=> $lea['email_other'],
			'add_main' 			=> $lea['add_main'],
			'add_postal' 		=> $lea['add_postal'],
			'coor_lati'			=> $lea['coor_lati'],
			'coor_long'			=> $lea['coor_long'],
			'country' 			=> $lea['country_id'],
			'town' 				=> $lea['town_id'],
			'zip' 				=> $lea['zip_id'],
			'h_type' 			=> $lea['h_type_id'],
			'h_roof' 			=> $lea['h_roof_id'],
			'h_level' 			=> $lea['h_level_id'],
			'ident' 			=> $lea['ident_id'],
			'ident_exp' 		=> $lea['ident_exp'],
			'ss' 				=> $lea['ss_id'],
			'ss_exp' 			=> $lea['ss_exp'],
			'mkt_origen'		=> $lea['mkt_origen_id'],
			'mkt_medium' 		=> $lea['mkt_medium_id'],
			'mkt_objective'		=> $lea['mkt_objective_id'],
			'mkt_post' 			=> $lea['mkt_post_id'],
			'mkt_form' 			=> $lea['mkt_forms_id'],
			'mkt_conv' 			=> $lea['mkt_conv_id'],
			'mkt_service' 		=> $lea['mkt_service_id'],
			'mkt_campaign' 		=> $lea['mkt_campaign_id'],
			'additional' 		=> $lea['additional'],
			'created_at' 		=> $lea['created_at']
		];
		}else{	return false;	}
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetLeadsByPhone($phone)
	{
        $aColumns 	=	['phone_main', 'phone_alt', 'phone_other'];
        $sTable 	=	'cp_leads';
        $sWhere 	=	"";

        $params 	=	isset($phone) ? $phone : "";

        if($params != "")
        {
        	$sWhere = "WHERE(";
        	for ($i=0; $i < count($aColumns) ; $i++) { 
        		$sWhere .= $aColumns[$i]." LIKE '%".$params."%' OR ";
        	}
        	$sWhere = substr_replace($sWhere, "", -3);
        	$sWhere .= ')';

        }else{
        	$sWhere = '';
        }

        $query      =   'SELECT client_id, name, phone_main, phone_alt, phone_other, add_main, add_postal, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id FROM '.$sTable.' '.$sWhere;

		$lea 	=	DBSmart::DBQueryAll($query);

		if($lea <> false)
		{
			$cont = "";
			foreach ($lea as $k => $val) 
			{
				$cont++;
				
				$lead[$k]	=	[
					'client'		=>	$val['client_id'],
					'name'			=>	$val['name'],
					'phone'			=>	$val['phone_main'],
					'p_alt'			=>	$val['phone_alt'],
					'p_other'		=>	$val['phone_other'],
					'add'			=>	$val['add_main'],
					'add_postal'	=>	$val['add_postal'], 
					'origen' 		=>	$val['mkt_origen_id'],
					'medium' 		=>	$val['mkt_medium_id'],
					'objetive' 		=>	$val['mkt_objective_id'],
					'post' 			=>	$val['mkt_post_id'],
					'form' 			=>	$val['mkt_forms_id'],
					'conv' 			=>	$val['mkt_conv_id'],
					'service' 		=>	$val['mkt_service_id'],
					'campaign' 		=>	$val['mkt_campaign_id']
				];
			}

			return ['cont' => $cont , 'lead' => $lead];
		}else{
			return false;
		}
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function LastClientID()
	{
		$query 	=	'SELECT client_id FROM cp_leads ORDER BY client_id DESC LIMIT 1';
		$lea 	=	DBSmart::DBQuery($query);

		if($lea <> false)
		{
			return [ 'client' => (($lea['client_id'])+1) ];
		}else{	return false;	}
	
	}

	public static function LastClientIDTest()
	{
		$query 	=	'SELECT client_id FROM cp_leads_copy ORDER BY client_id DESC LIMIT 1';
		$lea 	=	DBSmart::DBQuery($query);

		if($lea <> false)
		{
			return [ 'client' => (($lea['client_id'])+1) ];
		}else{	return [ 'client' => '10001' ];	}
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function AllClient()
	{
		$_replace 	=	New Config;

        $query   =   'SELECT client_id, referred_id, name, phone_main, type_acc, phone_alt, email_main, country_id, town_id, add_main, coor_lati, coor_long, created_at, created_by  FROM cp_leads ORDER BY created_at DESC LIMIT 1000';
        $lea     =   DBSmart::DBQueryAll($query);

		if($lea)
		{
			foreach ($lea as $k => $val) 
			{
				switch ($val['type_acc']) 
        		{
        			case '1':
        				$type 	=	"PRIMERA";
        				break;
        			
        			case '2':
        				$type 	=	"SEGUNDA";
        				break;

        			case '3':
        				$type 	=	"TERCERA";
        				break;

        			case '4':
        				$type 	=	"CUARTA";
        				break;

        			case '1':
        				$type 	=	"QUINTA";
        				break;

        			default:
						$type 	=	"PRIMERA";
        				break;
        		}

				$info[$k]	=	[
					'client'	=>	$val['client_id'],
					'referred'	=>	($val['referred_id'] <> "0") ? $val['referred_id'] : '',
					'name'		=>	$val['name'],
					'phone'		=>	$val['phone_main'],
					'alt'		=>	$val['phone_alt'],
					'country'	=>	($val['country_id'] <> "") 	? Country::GetCountryById($val['country_id'])['name'] : "",
					'town'		=>	($val['town_id'] <> "") 	? Town::GetTownById($val['town_id'])['name'] : "",
					'address'	=>	$val['add_main'],
					'type'		=>	$type,
					'operator'	=>	User::GetUserById($val['created_by'])['username'],
					'created'	=>	$_replace->ShowDate($val['created_at'])
				];
			}
			return $info;
		}else{	return false;	}

	}

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SearchSimpled($info)
	{

		$_replace 	=	New Config();

        $aColumns 	=	['client_id', 'name', 'phone_main', 'phone_alt', 'phone_other', 'type_acc'];
        $sTable 	=	'cp_leads';
        $sWhere 	=	"";

        $phone 		=	isset($info['search-fld']) ? $info['search-fld'] : "";

	    $t 			= 	substr($phone, '0','4');

	    if( ($t == "0414") || ($t == "0424") || ($t == "0416") || ($t == "0426") || (($t == "0251")))
	    {
	    	$params = substr($phone, "1", strlen($phone));
	    }else{
	    	$params = $phone;
	    }

        if($params != "")
        {
        	$sWhere = "WHERE(";
        	for ($i=0; $i < count($aColumns) ; $i++) { 
        		$sWhere .= $aColumns[$i]." LIKE '%".trim($params)."%' OR ";
        	}
        	$sWhere = substr_replace($sWhere, "", -3);
        	$sWhere .= ')';

        }else{
        	$sWhere = '';
        }

        $query      =   'SELECT client_id, referred_id, name, phone_main, type_acc, phone_alt, email_main, country_id, town_id, add_main, created_at, created_by FROM '.$sTable.' '.$sWhere .' ORDER BY created_at DESC LIMIT 1000';
        $result     =   DBSmart::DBQueryAll($query);

        if($result)
        {

        	foreach ($result as $k => $val) 
        	{
        		switch ($val['type_acc']) 
        		{
        			case '1':
        				$type 	=	"PRIMERA";
        				break;
        			
        			case '2':
        				$type 	=	"SEGUNDA";
        				break;

        			case '3':
        				$type 	=	"TERCERA";
        				break;

        			case '4':
        				$type 	=	"CUARTA";
        				break;

        			case '1':
        				$type 	=	"QUINTA";
        				break;

        			default:
						$type 	=	"PRIMERA";
        				break;
        		}

        		$res[$k]	=	[
					'client'	=>	$val['client_id'],
					'name'		=>	$val['name'],
					'referred'	=>	($val['referred_id'] <> 0 ) ? $val['referred_id'] : '',
					'phone'		=>	$val['phone_main'],
					'alt'		=>	$val['phone_alt'],
					'country'	=>	($val['country_id'] <> "") 	? 	Country::GetCountryById($val['country_id'])['name'] : 	"",
					'town'		=>	($val['town_id'] <> "") 	?	Town::GetTownById($val['town_id'])['name']			:	"",
					'address'	=>	$val['add_main'],
					'type'		=>	$type,
					'operator'	=>	User::GetUserById($val['created_by'])['username'],
					'created'	=>	$_replace->ShowDate($val['created_at'])
				];
        	}

        	return $res;
        	
        }else{	return false;	}
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SearchExterno($info)
	{
		$_replace 	=	New Config();

        $aColumns 	=	['phone_main', 'phone_alt', 'phone_other'];
        $sTable 	=	'cp_leads';
        $sWhere 	=	"";

        $phone 		=	isset($info['search-fld']) ? $info['search-fld'] : "";

 		$t 			= 	substr($phone, '0','4');

	    if( ($t == "0414") || ($t == "0424") || ($t == "0416") || ($t == "0426") || (($t == "0251")))
	    {
	    	$params = substr($phone, "1", strlen($phone));
	    }else{
	    	$params = $phone;
	    }

        if($params != "")
        {
        	$sWhere = "WHERE(";
        	for ($i=0; $i < count($aColumns) ; $i++) { 
        		$sWhere .= $aColumns[$i]." LIKE '%".trim($params)."%' OR ";
        	}
        	$sWhere = substr_replace($sWhere, "", -3);
        	$sWhere .= ')';

        }else{
        	$sWhere = '';
        }

        $query      =   'SELECT client_id, referred_id, name, phone_main, type_acc, phone_alt, email_main, country_id, town_id, add_main, created_at, created_by FROM '.$sTable.' '.$sWhere .' AND country_id = "4" ORDER BY created_at DESC LIMIT 1000';

        $result     =   DBSmart::DBQueryAll($query);

        if($result)
        {

        	foreach ($result as $k => $val) 
        	{
        		switch ($val['type_acc']) 
        		{
        			case '1':
        				$type 	=	"PRIMERA";
        				break;
        			
        			case '2':
        				$type 	=	"SEGUNDA";
        				break;

        			case '3':
        				$type 	=	"TERCERA";
        				break;

        			case '4':
        				$type 	=	"CUARTA";
        				break;

        			case '1':
        				$type 	=	"QUINTA";
        				break;

        			default:
						$type 	=	"PRIMERA";
        				break;
        		}

        		$res[$k]	=	[
					'client'	=>	$val['client_id'],
					'name'		=>	$val['name'],
					'referred'	=>	($val['referred_id'] <> 0 ) ? $val['referred_id'] : '',
					'phone'		=>	$val['phone_main'],
					'alt'		=>	$val['phone_alt'],
					'country'	=>	($val['country_id'] <> "") 	? 	Country::GetCountryById($val['country_id'])['name'] : 	"",
					'town'		=>	($val['town_id'] <> "") 	?	Town::GetTownById($val['town_id'])['name']			:	"",
					'address'	=>	$val['add_main'],
					'type'		=>	$type,
					'operator'	=>	User::GetUserById($val['created_by'])['username'],
					'created'	=>	$_replace->ShowDate($val['created_at'])
				];
        	}

        	return $res;
        	
        }else{	return false;	}


	}

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SearchCreate($info)
	{

		$_replace 	=	New Config();

        $aColumns 	=	['name', 'phone_main', 'phone_alt', 'phone_other', 'country_id'];
        $sTable 	=	'cp_leads';
        $sWhere 	=	"";

        $phone 		=	isset($info['search-fld']) ? $info['search-fld'] : "";

	    $t 			= 	substr($phone, '0','4');

	    if( ($t == "0414") || ($t == "0424") || ($t == "0416") || ($t == "0426") || (($t == "0251")))
	    {
	    	$pho = substr($phone, "1", strlen($phone));
	    }else{
	    	$pho = $phone;
	    }


        if($pho != "")
        {
        	$sWhere 	=	'WHERE ( ((name LIKE "%'.$info['c_name'].'$") OR (phone_main LIKE "%'.$pho.'%") OR (phone_alt LIKE "%'.$pho.'%") OR (phone_other LIKE "%'.$pho.'%")) AND (country_id = "'.$info['c_country'].'%"))';
        }else{
        	$sWhere = '';
        }

        $query      =   'SELECT client_id, referred_id, name, phone_main, type_acc, phone_alt, phone_other, email_main, country_id, town_id, add_main, created_at, created_by FROM '.$sTable.' '.$sWhere .' ORDER BY created_at DESC LIMIT 1000';

        $result     =   DBSmart::DBQueryAll($query);

        if($result)
        {

    		$html 	=	'';
			$html.='<table class="table">';
			$html.='<tbody>';
			$html.='<tr>';
			$html.='<td>ID</td>';
			$html.='<td>Cliente</td>';
			$html.='<td>Telefono</td>';
			$html.='<td>Alterno</td>';
			$html.='<td>Tipo</td>';
			$html.='<td>Operador</td>';
			$html.='<td>Fecha Creacion</td>';
			$html.='</tr>';

        	foreach ($result as $k => $val) 
        	{
        		switch ($val['type_acc']) 
        		{
        			case '1':
        				$type 	=	"PRIMERA";
        				break;
        			
        			case '2':
        				$type 	=	"SEGUNDA";
        				break;

        			case '3':
        				$type 	=	"TERCERA";
        				break;

        			case '4':
        				$type 	=	"CUARTA";
        				break;

        			case '1':
        				$type 	=	"QUINTA";
        				break;

        			default:
						$type 	=	"PRIMERA";
        				break;
        		}

        		$html.='<tr>';
        		$html.='<td>'.$val['client_id'].'</td>';
        		$html.='<td>'.$val['name'].'</td>';
        		$html.='<td>'.$val['phone_main'].'</td>';
        		$html.='<td>'.$val['phone_alt'].'</td>';
        		$html.='<td>'.$type.'</td>';
        		$html.='<td>'.User::GetUserById($val['created_by'])['username'].'</td>';
        		$html.='<td>'.$_replace->ShowDate($val['created_at']).'</td>';
        		$html.='</tr>';

        	}
			$html.='</tbody>';
			$html.='</table>';

        	return ['status' => true, 'html'	=>	$html];
        	
        }else{	return ['status' => false, 'html'	=>	''];	}
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetLeadByClient($client)
	{


		$_replace 	= new Config();

		$query 	=	'SELECT client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_medium_id, mkt_origen_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_at FROM cp_leads WHERE client_id = "'.$client.'"';

		$lea 	=	DBSmart::DBQuery($query);

		if($lea <> false)
		{
			$name 		=	mb_convert_encoding($lea['name'], 'UTF-8', 'UTF-8');
			$repre 		=	mb_convert_encoding($lea['repre_legal'], 'UTF-8', 'UTF-8');
			$nameRef 	=	'CLIENTE REFERIDO DE - '.$lea['referred_id'];

			return [
			'nameClient'			=> ($lea['referred_id'] <> 0) 		? $name.' - '.$nameRef 	: $name,	
			'client' 				=> ($lea['client_id']) 				? $lea['client_id'] : '',
			'referred' 				=> ($lea['referred_id']) 			? $lea['referred_id'] : '',
			'name' 					=> ($name) 							? $name : '',
			'birthday' 				=> ($lea['birthday'] <> '')			? $_replace->ShowDate($lea['birthday']) : '',
			'gender' 				=> ($lea['gender']) 				? $lea['gender'] : '',
			'age' 					=> ($lea['age']) 					? $lea['age'] : '',
			'repre'					=> ($repre) 						? $repre : '',
			'type_acc'				=> ($lea['type_acc']) 				? $lea['type_acc'] : '',
			'phone_main' 			=> ($lea['phone_main']) 			? $lea['phone_main'] : '',
			'phone_main_owner' 		=> ($lea['phone_main_owner']) 		? mb_convert_encoding($lea['phone_main_owner'], 'UTF-8', 'UTF-8') : '',
			'phone_main_provider' 	=> ($lea['phone_main_provider']) 	? $lea['phone_main_provider'] : '',
			'phone_alt' 			=> ($lea['phone_alt']) 				? $lea['phone_alt'] : '',
			'phone_alt_owner' 		=> ($lea['phone_alt_owner']) 		? mb_convert_encoding($lea['phone_alt_owner'], 'UTF-8', 'UTF-8') : '',
			'phone_alt_provider' 	=> ($lea['phone_alt_provider']) 	? $lea['phone_alt_provider'] : '',
			'phone_other' 			=> ($lea['phone_other']) 			? $lea['phone_other'] : '',
			'phone_other_owner' 	=> ($lea['phone_other_owner']) 		?mb_convert_encoding($lea['phone_other_owner'], 'UTF-8', 'UTF-8') : '',
			'phone_other_provider' 	=> ($lea['phone_other_provider'])	? $lea['phone_other_provider'] : '',
			'email_main' 			=> ($lea['email_main']) 			? $lea['email_main'] : '',
			'email_other' 			=> ($lea['email_other']) 			? $lea['email_other'] : '',
			'add_main' 				=> ($lea['add_main']) 				? mb_convert_encoding($lea['add_main'], 'UTF-8', 'UTF-8') : '',
			'add_postal' 			=> ($lea['add_postal']) 			? mb_convert_encoding($lea['add_postal'], 'UTF-8', 'UTF-8') : '',
			'coor_lati'				=> ($lea['coor_lati']) 				? $lea['coor_lati'] : '',
			'coor_long'				=> ($lea['coor_long']) 				? $lea['coor_long'] : '',
			'country' 				=> ($lea['country_id']) 			? $lea['country_id'] : '',
			'town' 					=> ($lea['town_id']) 				? $lea['town_id'] : '',
			'zip' 					=> ($lea['zip_id']) 				? $lea['zip_id'] : '',
			'h_type' 				=> ($lea['h_type_id']) 				? $lea['h_type_id'] : '',
			'h_roof' 				=> ($lea['h_roof_id']) 				? $lea['h_roof_id'] : '',
			'h_level' 				=> ($lea['h_level_id']) 			? $lea['h_level_id'] : '',
			'ident' 				=> ($lea['ident_id']) 				? $lea['ident_id'] : '',
			'ident_exp' 			=> ($lea['ident_exp']) 				? $lea['ident_exp'] : '',
			'ss' 					=> ($lea['ss_id']) 					? $lea['ss_id'] : '',
			'ss_exp' 				=> ($lea['ss_exp']) 				? $lea['ss_exp'] : '',
			'mkt_medium' 			=> ($lea['mkt_medium_id']) 			? $lea['mkt_medium_id'] : '',
			'mkt_origen' 			=> ($lea['mkt_origen_id']) 			? $lea['mkt_origen_id'] : '',
			'mkt_objective' 		=> ($lea['mkt_objective_id']) 		? $lea['mkt_objective_id'] : '',
			'mkt_post' 				=> ($lea['mkt_post_id']) 			? $lea['mkt_post_id'] : '',
			'mkt_form' 				=> ($lea['mkt_forms_id'])			? $lea['mkt_forms_id'] : '',
			'mkt_conv' 				=> ($lea['mkt_conv_id'])			? $lea['mkt_conv_id'] : '',
			'mkt_service' 			=> ($lea['mkt_service_id']) 		? $lea['mkt_service_id'] : '',
			'mkt_campaign' 			=> ($lea['mkt_campaign_id']) 		? $lea['mkt_campaign_id'] : '',
			'additional' 			=> ($lea['additional']) 			? mb_convert_encoding($lea['additional'], 'UTF-8', 'UTF-8') : '',
			'created_at' 			=> ($lea['created_at']) 			? $lea['created_at'] : ''
			];

		}else{	return false;	}
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveDB($info)
	{
		$date = date('Y-m-d h:i:s', time());
		
		$_replace 	= new Config();

		$iData = [
			'name' 					=> (isset($info['c_name'])) 		? strtoupper($_replace->deleteTilde($info['c_name'])) 			: "",
			'birthday' 				=> ($info['c_birthday'] <> '') 		? strtoupper($_replace->ChangeDate($info['c_birthday'])) 		: "",
			'gender' 				=> (isset($info['c_gender'])) 		? strtoupper($_replace->deleteTilde($info['c_gender'])) 		: "",
			'age' 					=> (isset($info['c_age'])) 			? strtoupper($_replace->deleteTilde($info['c_age'])) 			: "",
			'repre'					=> (isset($info['c_rep_legal'])) 	? strtoupper($_replace->deleteTilde($info['c_rep_legal'])) 		: "",
			'type_acc'				=> (isset($info['c_type_acc'])) 	? strtoupper($_replace->deleteTilde($info['c_type_acc'])) 		: "",
			'phone_main' 			=> (isset($info['c_phone'])) 		? strtoupper($_replace->deleteTilde($info['c_phone']))			: "",
			'phone_main_owner' 		=> (isset($info['c_phone_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_ow'])) 		: "",
			'phone_main_provider'	=> (isset($info['c_phone_pro'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_pro'])) 		: "",
			
			'phone_alt' 			=> (isset($info['c_phone_a'])) 		? strtoupper($_replace->deleteTilde($info['c_phone_a'])) 		: "",
			'phone_alt_owner' 		=> (isset($info['c_phone_a_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_a_ow'])) 	: "",
			'phone_alt_provider'	=> (isset($info['c_phone_a_pro'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_a_pro'])) 	: "",
			
			'phone_other' 			=> (isset($info['c_phone_o'])) 		? strtoupper($_replace->deleteTilde($info['c_phone_o'])) 		: "",
			'phone_other_owner' 	=> (isset($info['c_phone_o_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_o_ow'])) 	: "",
			'phone_other_provider'	=> (isset($info['c_phone_o_pro'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_o_pro'])) 	: "",

			'email_main'			=> (isset($info['c_email'])) 		? $info['c_email'] 												: "",
			'email_other'			=> (isset($info['c_email_o'])) 		? $info['c_email_o'] 											: "",
			'add_main'				=> (isset($info['c_address'])) 		? strtoupper($_replace->deleteTilde($info['c_address'])) 		: "",
			'add_postal'			=> (isset($info['c_postal'])) 		? strtoupper($_replace->deleteTilde($info['c_postal'])) 		: "",
			'coor_lati'				=> (isset($info['c_lati'])) 		? strtoupper($_replace->deleteTilde($info['c_lati'])) 			: "",
			'coor_long'				=> (isset($info['c_long'])) 		? strtoupper($_replace->deleteTilde($info['c_long'])) 			: "",
			'country_id'			=> (isset($info['c_country'])) 		? strtoupper($_replace->deleteTilde($info['c_country'])) 		: "1",
			'town_id'				=> (isset($info['c_town'])) 		? strtoupper($_replace->deleteTilde($info['c_town'])) 			: "",
			'zip_id'				=> (isset($info['c_zip'])) 			? strtoupper($_replace->deleteTilde($info['c_zip'])) 			: "",
			'zip_id'				=> (isset($info['c_zip'])) 			? strtoupper($_replace->deleteTilde($info['c_zip'])) 			: "",
			'h_type_id'				=> (isset($info['c_type_house']))	? strtoupper($_replace->deleteTilde($info['c_type_house'])) 	: "",
			'h_roof_id'				=> (isset($info['c_roof']))			? strtoupper($_replace->deleteTilde($info['c_roof'])) 			: "",
			'h_level_id'			=> (isset($info['c_number_level']))	? strtoupper($_replace->deleteTilde($info['c_number_level']))	: "",
			'additional'			=> (isset($info['additional']))		? strtoupper($_replace->deleteTilde($info['additional']))		: "",
			'created_at' 			=> $date
		];

		// var_dump($iData);
		// exit;

		$query 	=	'UPDATE cp_leads SET name="'.$iData['name'].'", birthday="'.$iData['birthday'].'", gender="'.$iData['gender'].'", age="'.$iData['age'].'", repre_legal="'.$iData['repre'].'", type_acc="'.$iData['type_acc'].'", phone_main="'.$iData['phone_main'].'", phone_main_owner="'.$iData['phone_main_owner'].'", phone_main_provider="'.$iData['phone_main_provider'].'", phone_alt="'.$iData['phone_alt'].'", phone_alt_owner="'.$iData['phone_alt_owner'].'", phone_alt_provider="'.$iData['phone_alt_provider'].'", phone_other="'.$iData['phone_other'].'", phone_other_owner="'.$iData['phone_other_owner'].'", phone_other_provider="'.$iData['phone_other_provider'].'", email_main="'.$iData['email_main'].'", email_other="'.$iData['email_other'].'", add_main="'.$iData['add_main'].'", add_postal="'.$iData['add_postal'].'", coor_lati="'.$iData['coor_lati'].'", coor_long="'.$iData['coor_long'].'", country_id="'.$iData['country_id'].'", town_id="'.$iData['town_id'].'", zip_id="'.$iData['zip_id'].'", h_type_id="'.$iData['h_type_id'].'", h_roof_id="'.$iData['h_roof_id'].'", h_level_id="'.$iData['h_level_id'].'", additional="'.$iData['additional'].'" WHERE client_id = "'.$info['c_client'].'"';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveMkt($info)
	{
		$date = date('Y-m-d h:i:s', time());
		
		$_replace 	= new Config();

		$iData 	=	[
			'mkt_origen_id'		=> (isset($info['mkt_origen'])) 	? strtoupper($_replace->deleteTilde($info['mkt_origen']))	: "",
			'mkt_medium_id'		=> (isset($info['mkt_medium'])) 	? strtoupper($_replace->deleteTilde($info['mkt_medium'])) 	: "",
			'mkt_objective_id'	=> (isset($info['mkt_objective']))	? strtoupper($_replace->deleteTilde($info['mkt_objective'])): "",
			'mkt_post_id' 		=> (isset($info['mkt_post'])) 	 	? strtoupper($_replace->deleteTilde($info['mkt_post']))		: "1",
			'mkt_forms_id'		=> (isset($info['mkt_form'])) 	 	? strtoupper($_replace->deleteTilde($info['mkt_form']))		: "",
			'mkt_conv_id'		=> (isset($info['mkt_conv'])) 	 	? strtoupper($_replace->deleteTilde($info['mkt_conv']))		: "",
			'mkt_service_id' 	=> (isset($info['mkt_service']))  	? strtoupper($_replace->deleteTilde($info['mkt_service'])) 	: "",
			'mkt_campaign_id'	=> (isset($info['mkt_campaign'])) 	? strtoupper($_replace->deleteTilde($info['mkt_campaign']))	: "",
		];

		$query 		=	'UPDATE cp_leads SET mkt_origen_id="'.$iData['mkt_origen_id'].'", mkt_medium_id="'.$iData['mkt_medium_id'].'", mkt_objective_id="'.$iData['mkt_objective_id'].'", mkt_post_id="'.$iData['mkt_post_id'].'", mkt_forms_id="'.$iData['mkt_forms_id'].'", mkt_conv_id="'.$iData['mkt_conv_id'].'", mkt_service_id="'.$iData['mkt_service_id'].'", mkt_campaign_id="'.$iData['mkt_campaign_id'].'" WHERE client_id = "'.$info['mkt_client'].'"';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveLead($info)
	{
		$date 		=	date('Y-m-d h:i:s', time());

		$_replace 	=	new Config();

		$lead 		=	Leads::create();

		$iData 	=	[
			'client_id'				=> (isset($info['client_id'])) 		? strtoupper($_replace->deleteTilde($info['client_id'])) 		: "" ,
			'referred_id'			=> (isset($info['referred_id'])) 	? strtoupper($_replace->deleteTilde($info['referred_id'])) 		: "0" ,
			'name' 					=> (isset($info['c_name'])) 		? strtoupper($_replace->deleteTilde($info['c_name'])) 			: "" ,
			'birthday' 				=> (isset($info['c_birthday'])) 	? strtoupper($_replace->deleteTilde($info['c_birthday'])) 		: "" ,
			'age' 					=> (isset($info['age'])) 			? strtoupper($_replace->deleteTilde($info['age'])) 				: "" ,
			'repre' 				=> (isset($info['c_rep_legal'])) 	? strtoupper($_replace->deleteTilde($info['c_rep_legal'])) 			: "" ,
			'type' 					=> (isset($info['c_type_acc'])) 	? strtoupper($_replace->deleteTilde($info['c_type_acc'])) 			: "" ,
			'phone_main' 			=> (isset($info['c_phone'])) 		? strtoupper($_replace->deleteTilde($info['c_phone']))			: "" ,
			'phone_main_owner' 		=> (isset($info['c_phone_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_ow'])) 		: "" ,
			'phone_main_provider' 	=> "1",
			'country_id'			=> (isset($info['c_country'])) 		? strtoupper($_replace->deleteTilde($info['c_country'])) 		: "1",
			'ident_id' 				=> (isset($info['ident_id'])) 		? strtoupper($_replace->deleteTilde($info['ident_id'])) 		: "" ,
			'ident_exp' 			=> (isset($info['ident_exp'])) 		? strtoupper($_replace->deleteTilde($info['ident_exp'])) 		: "" ,
			'ss_id' 				=> (isset($info['ss_id'])) 			? strtoupper($_replace->deleteTilde($info['ss_id'])) 			: "" ,
			'ss_exp' 				=> (isset($info['ss_exp'])) 		? strtoupper($_replace->deleteTilde($info['ss_exp'])) 			: "" ,
			'created_at' 			=> $date
		];

		$query =	'INSERT INTO cp_leads(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_at) VALUES ("'.$iData['client_id'].'", "'.$iData['referred_id'].'", "'.$iData['name'].'", "'.$iData['birthday'].'", "M", "'.$iData['age'].'", "'.$iData['repre'].'", "'.$iData['type'].'", "'.$iData['phone_main'].'", "'.$iData['phone_main_owner'].'", "'.$iData['phone_main_provider'].'", "", "", "1", "", "", "1", "", "", "", "", "", "", "'.$iData['country_id'].'", "1", "1", "1", "1", "1", "'.$iData['ident_id'].'", "'.$iData['ident_exp'].'", "'.$iData['ss_id'].'", "'.$iData['ss_exp'].'", "1", "1", "1", "1", "1", "1", "1", "1", "", "'.$iData['created_at'].'")';

		$lead 		=	DBSmart::DataExecute($query);

		return ($lead <> false ) ? true : false;

	}

	public static function SaveLeadRef($info)
	{
		$date 		=	date('Y-m-d h:i:s', time());

		$_replace 	=	new Config();

		$iData 	=	[
			'client_id'				=> (isset($info['client_id'])) 		? strtoupper($_replace->deleteTilde($info['client_id'])) 		: "" ,
			'referred_id'			=> (isset($info['referred_id'])) 	? strtoupper($_replace->deleteTilde($info['referred_id'])) 		: "0" ,
			'name' 					=> (isset($info['c_name'])) 		? strtoupper($_replace->deleteTilde($info['c_name'])) 			: "" ,
			'birthday' 				=> (isset($info['c_birthday'])) 	? $info['c_birthday'] 											: "" ,
			'age' 					=> (isset($info['age'])) 			? strtoupper($_replace->deleteTilde($info['age'])) 				: "" ,
			'phone_main' 			=> (isset($info['c_phone'])) 		? strtoupper($_replace->deleteTilde($info['c_phone']))			: "" ,
			'phone_main_owner' 		=> (isset($info['c_phone_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_ow'])) 		: "" ,
			'phone_main_provider' 	=> "1",
			'country_id'			=> (isset($info['c_country'])) 		? strtoupper($_replace->deleteTilde($info['c_country'])) 		: "1",
			'ident_id' 				=> (isset($info['ident_id'])) 		? strtoupper($_replace->deleteTilde($info['ident_id'])) 		: "" ,
			'ident_exp' 			=> (isset($info['ident_exp'])) 		? strtoupper($_replace->deleteTilde($info['ident_exp'])) 		: "" ,
			'ss_id' 				=> (isset($info['ss_id'])) 			? strtoupper($_replace->deleteTilde($info['ss_id'])) 			: "" ,
			'ss_exp' 				=> (isset($info['ss_exp'])) 		? strtoupper($_replace->deleteTilde($info['ss_exp'])) 			: "" ,
			'created_by'			=> $info['user'],
			'created_at' 			=> $date
		];

		$query =	'INSERT INTO cp_leads(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_by, created_at) VALUES ("'.$iData['client_id'].'", "'.$iData['referred_id'].'", "'.$iData['name'].'", "'.$iData['birthday'].'", "M", "'.$iData['age'].'", "", "1", "'.$iData['phone_main'].'", "'.$iData['phone_main_owner'].'", "'.$iData['phone_main_provider'].'", "", "", "1", "", "", "1", "", "", "", "", "", "", "'.$iData['country_id'].'", "1", "1", "1", "1", "1", "'.$iData['ident_id'].'", "'.$iData['ident_exp'].'", "'.$iData['ss_id'].'", "'.$iData['ss_exp'].'", "1", "1", "1", "1", "1", "1", "1", "1", "", "'.$iData['created_by'].'", "'.$iData['created_at'].'")';

		$lead 		=	DBSmart::DataExecute($query);

		return ($lead <> false ) ? true : false;

	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveID($client)
	{


		$cypherID 	=	CypherData::GetCypherClientType($client, 'ID');

		$ID 		=	($cypherID) ? ['id' => $cypherID['id'], 'exp' => $cypherID['ID_exp']] : ['id' => "", 'exp' => ""];
		
		$query 		=	'UPDATE cp_leads SET ident_id = "'.$ID['id'].'", ident_exp = "'.$_replace->ChangeDate($ID['exp']).'" WHERE client_id = "'.$client.'"';
		
		$dep 		=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;		
	}


	public static function UpdateID($client, $id)
	{
		$_replace 	= 	new Config();

		$query 		=	'UPDATE cp_leads SET ident_id="'.$id['id'].'", ident_exp="'.$_replace->ChangeDate($id['ID_exp']).'" WHERE client_id = "'.$client.'"';
		
		$dep 		=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;	
	}

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SaveCC($client)
	{

		$cypherSS 	=	CypherData::GetCypherClientType($client, 'SS');

		$SS 		=	($cypherSS) ? ['id' => $cypherSS['id'], 'exp' => $cypherSS['SS_exp']] : ['id' => "", 'exp' => ""];
		
		$query 		=	'UPDATE cp_leads SET ss_id = "'.$SS['id'].'", ss_exp = "'.$_replace->ChangeDate($SS['exp']).'" WHERE client_id = "'.$client.'"';
		
		$dep 		=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;	
	
	}

	public static function UpdateSS($client, $ss)
	{

		$_replace 	= 	new Config();

		$query 		=	'UPDATE cp_leads SET ss_id="'.$ss['id'].'", ss_exp="'.$_replace->ChangeDate($ss['SS_exp']).'" WHERE client_id = "'.$client.'"';
		
		$dep 		=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;		
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function CreateSimple($info, $type, $user)
	{
		
		$id 	= 	Leads::LastClientID();

		$client = ($id <> false) ? $id['client'] : "10001";

		$date	=	date('Y-m-d H:i:s', time());
		
		$data 	= ['c_client' => strtoupper($client), 'c_name' => strtoupper($info['c_name']), 'c_phone' => strtoupper($info['search-fld']), 'c_phone_ow' => strtoupper($info['c_name']), 'c_country' => $info['c_country']];

		$cou        =   $info['c_country'];
        $coun    	=   Country::GetCountryById($cou)['id'];
        $town       =   Town::GetTownCountry($cou)[0]['id'];
        $zip        =   ZipCode::GetZipByTownID($town)[0]['id'];
        $cei        =   Ceiling::GetCeilingByCountryID($cou)[0]['id'];
        $lev        =   Level::GetLevelByCountryID($cou)[0]['id'];
        $hou        =   House::GetHouseByCountryID($cou)[0]['id'];

		$info 		= 	LeadsTmp::SaveLead($data, "NEW", $user);

		if($info <> false)
		{
			$query 	=	'INSERT INTO cp_leads(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_by, created_at) VALUES ("'.$data['c_client'].'", "0", "'.$data['c_name'].'", "", "", "", "", "'.$type.'", "'.$data['c_phone'].'", "'.$data['c_phone_ow'].'", "1", "", "", "1", "", "", "1", "", "", "", "", "", "", "'.$coun.'", "'.$town.'", "'.$zip.'", "'.$hou.'", "'.$cei.'", "'.$lev.'", "", "", "", "", "1", "1", "1", "1", "1", "1", "1", "1", "", "'.$user.'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? ['status' => true, 'client' => $client ] : false;

		}else{ return ['status' => false, 'client' => $client ];	}
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function CreateMKT($info, $user)
	{
		$_replace 	= 	new Config();

		$id 	= 	Leads::LastClientID();

		$client = ($id <> false) ? $id['client'] : "10001";

		$date	=	date('Y-m-d h:i:s', time());

		$iData 	= [
			'c_client' 		=> $client,
			'mkt_client' 	=> $client,
			'c_name' 		=> (isset($info['m_name'])) 		? strtoupper($_replace->deleteTilde($info['m_name'])) 			: "EXCELENTE OPORTUNIDAD",
			'c_phone' 		=> (isset($info['m_phone'])) 		? strtoupper($_replace->deleteTilde($info['m_phone'])) 			: "",
			'c_phone_ow' 	=> (isset($info['m_name'])) 		? strtoupper($_replace->deleteTilde($info['m_name'])) 			: "EXCELENTE OPORTUNIDAD",
			'c_phone_pro' 	=> "1",
			'c_country' 	=> $info['m_country'],
			'mkt_origen' 	=> $info['origen_id'],
			'mkt_medium' 	=> $info['medium_id'],
			'mkt_objective' => $info['objetive_id'],
			'mkt_post' 		=> $info['post_id'],
			'mkt_form' 		=> $info['form_id'],
			'mkt_conv' 		=> $info['conv_id'],
			'mkt_service' 	=> $info['service_id'],
			'mkt_campaign' 	=> $info['campaign_id'],
			'user'			=> $user,
			'date'			=> $date
		];

		$info 	= LeadsTmp::SaveLeadMKT($iData);

		if($info == true)
		{
			$query 	=	'INSERT INTO cp_leads(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_by, created_at) VALUES ("'.$iData['c_client'].'", "0", "'.$iData['c_name'].'", "", "M", "0", "", "1", "'.$iData['c_phone'].'", "'.$iData['c_phone_ow'].'", "'.$iData['c_phone_pro'].'", "", "", "1", "", "", "1", "", "", "", "", "", "", "'.$iData['c_country'].'", "1", "1", "1", "1", "1", "", "", "", "", "'.$iData['mkt_origen'].'", "'.$iData['mkt_medium'].'", "'.$iData['mkt_objective'].'", "'.$iData['mkt_post'].'", "'.$iData['mkt_form'].'", "'.$iData['mkt_conv'].'", "'.$iData['mkt_service'].'", "'.$iData['mkt_campaign'].'", "", "'.$iData['user'].'", "'.$iData['date'].'")';

			$lead 		=	DBSmart::DataExecute($query);

			if($lead ==  true)
			{
				$mkt 	=	MarketingTmp::SaveDB($iData, 'NEW', $user);

				if($mkt == true)
				{
					return ['status' => true, 'client' => $client ];

				}else{
					return ['status' => false, 'client' => $client ];
				}	

			}else{

				return ['status' => false, 'client' => $client ];
			}

			return ($lead <> false ) ? true : false;

		}else{

			return ['status' => false, 'client' => $client ];
		}
	}

////////////////////////////// Submit HTML Email //////////////////////////////

	public static function GetLeadSubmitHtml($iData)
	{
		$_replace 	= 	new Config();

		$dateID 	=	($iData['id_exp'] <> '') 	? 	$_replace->ShowDate($iData['id_exp']) 		:	'';
		$dateB 		=	($iData['birthday'] <> '') 	?	$_replace->ShowDate($iData['birthday'])		:	'';

		$html = "";
		$html.='<table class="head-wrap" style="background: #ecf8ff">';							
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>NOMBRE</strong></td>';
		$html.='<td>'.$iData['name'].'</td>';
		$html.='<td><strong>CUMPLEA&Ntilde;OS</strong></td>';
		$html.='<td>'.$dateB.'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TELEFONO</strong></td>';
		$html.='<td>'.$iData['phone_main'].'</td>';
		$html.='<td><strong>ALTERNATIVO</strong></td>';
		$html.='<td>'.$iData['phone_alt'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>SEGURO SOCIAL</strong></td>';
		$html.='<td>'.$iData['ss'].'</td>';
		$html.='<td><strong>CIUDAD</strong></td>';
		$html.='<td>'.$iData['town'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>ID</strong></td>';
		$html.='<td>'.$iData['id'].'</td>';
		$html.='<td><strong>FECHA DE EXPIRACION</strong></td>';
		$html.='<td>'.$dateID.'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>COORD LATITUD</strong></td>';
		$html.='<td>'.$iData['coor_lati'].'</td>';
		$html.='<td><strong>COORD LONGITUD</strong></td>';
		$html.='<td>'.$iData['coor_long'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DIRECCI&Oacute;N</strong></td>';
		$html.='<td colspan="4">'.$iData['add_main'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>POSTAL</strong></td>';
		$html.='<td colspan="4">'.$iData['add_postal'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EMAIL</strong></td>';
		$html.='<td colspan="4">'.$iData['email_main'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DESCRIPCI&Oacute;N</strong></td>';
		$html.='<td colspan="4">'.$iData['additional'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';

		return $html;
	
	}

////////////////////////////// Referred HTML Email //////////////////////////////
	
	public static function GetReferredHtml($iData)
	{
		$html 	= "";
		
		$html='<h3 style="margin-top: 20px; text-align: center;">INFORMACION REFERIDO </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>REFERIDO</strong></td>';
		$html.='<td>'.$iData['ref'].'</td>';
		$html.='<td><strong>CLIENTE</strong></td>';
		$html.='<td>'.( ($iData['ref_client'] <> "") ? $iData['ref_client'] : "NINGUNO" ).'</td>';
		$html.='<td><strong>NOMBRE DEL CLIENTE</strong></td>';
		$html.='<td>'.( ($iData['ref_name'] <> "") ? $iData['ref_name'] : "NINGUNO" ).'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';

		return $html;	
	}

////////////////////////////// Support HTML Email //////////////////////////////

	public static function GetSupportHtml($iData)
	{

		$html 	= "";

		$html='<h3 style="margin-top: 20px; text-align: center;">INFORMACION DE APOYO </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>APOYO</strong></td>';
		$html.='<td>'.$iData['sup'].'</td>';
		$html.='<td><strong>PROPIETARIO</strong></td>';
		$html.='<td>'.$iData['prop'].'</td>';
		$html.='<td><strong>ASISTENTE</strong></td>';
		$html.='<td>'.$iData['assit'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';
		
		return $html;	
	}

////////////////////////////// Lead HTML Email //////////////////////////////

	public static function GetLeadSaleHtml($iData)
	{
		$_replace 	= 	new Config();
		$fecha 		=	($iData['created'] <> '') ? $_replace->ShowDate($iData['created']) : '';

		$html = "";
		$html='<h3 style="margin-top: 20px; text-align: center;">INFORMACION DE VENDEDOR </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>FECHA</strong></td>';
		$html.='<td>'.$fecha.'</td>';
		$html.='<td><strong>VENDEDOR</strong></td>';
		$html.='<td>'.$iData['prop'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DEPARTAMENTO</strong></td>';
		$html.='<td>'.$iData['prop_depart'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';

		return $html;
	
	}

////////////////////////////// Internet HTML Email //////////////////////////////

	public static function GetSubIntHtml($iData)
	{
		$_replace 	= 	new Config();

		$fecha 		=	($iData['created'] <> '') ? $_replace->ShowDate($iData['created']) : '';
		
		$html 	= "";
		$html='<h3 style="margin-top: 20px; text-align: center;">INTERNET </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';							
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INT. RESIDENCIAL</strong></td>';
		$html.='<td>'.$iData['int_res'].'</td>';
		$html.='<td><strong>TELF. RESIDENCIAL</strong></td>';
		$html.='<td>'.$iData['pho_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INT. COMERCIAL</strong></td>';
		$html.='<td>'.$iData['int_com'].'</td>';
		$html.='<td><strong>TELF. COMERCIAL</strong></td>';
		$html.='<td>'.$iData['pho_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$iData['payment'].'</td>';
		$html.='<td><strong>FECHA DE ENVIO</strong></td>';
		$html.='<td>'.$fecha.'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TIPO DE ORDEN</strong></td>';
		$html.='<td>'.$iData['type_order'].'</td>';
		$html.='<td><strong></strong></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';
		return $html;
	}

////////////////////////////// Internet HTML Email //////////////////////////////

	public static function GetSubIntVZHtml($iData)
	{
		$html 	= "";
		$html='<h3 style="margin-top: 20px; text-align: center;">INTERNET </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';							
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>INT. RESIDENCIAL</strong></td>';
		$html.='<td>'.$iData['int_res'].'</td>';
		$html.='<td><strong>TELF. RESIDENCIAL</strong></td>';
		$html.='<td>'.$iData['pho_res'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>INT. COMERCIAL</strong></td>';
		$html.='<td>'.$iData['int_com'].'</td>';
		$html.='<td><strong>TELF. COMERCIAL</strong></td>';
		$html.='<td>'.$iData['pho_com'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$iData['payment'].'</td>';
		$html.='<td><strong>BANCO</strong></td>';
		$html.='<td>'.$iData['bank'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>MONTO</strong></td>';
		$html.='<td>'.$iData['amount'].'</td>';
		$html.='<td><strong># TRANSFERENCIA</strong></td>';
		$html.='<td>'.$iData['transference'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EMAIL</strong></td>';
		$html.='<td>'.$iData['email'].'</td>';
		$html.='<td><strong>FECHA DE PAGO</strong></td>';
		$html.='<td>'.$iData['trans_date'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';

		return $html;
	}


////////////////////////////// Security HTML Email //////////////////////////////

	public static function GetSubSecHtml($iData)
	{
		$html 	= "";
		$html.='<h3 style="margin-top: 20px; text-align: center;">SECURITY </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>SISTEMA DE SEGURIDAD PREVBIO</strong></td>';
		$html.='<td>'.$iData['previosly'].'</td>';
		$html.='<td><strong>COMPANIA DE MONITOREO</strong></td>';
		$html.='<td>'.$iData['companay'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CAMARAS</strong></td>';
		$html.='<td>'.$iData['cameras'].'</td>';
		$html.='<td><strong>DVR</strong></td>';
		$html.='<td>'.$iData['dvrs'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PASSWORD ALARMA</strong></td>';
		$html.='<td>'.$iData['pass_alarm'].'</td>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$iData['payment'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DESCRIPCION DE EQUIPOS</strong></td>';
		$html.='<td colspan="2">'.$iData['equipment'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>EQUIPOS ADICIONALES</strong></td>';
		$html.='<td colspan="2">'.$iData['add_equip'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACTO 1</strong></td>';
		$html.='<td>'.$iData['cont1'].'</td>';
		$html.='<td><strong>DESCRIPCION</strong></td>';
		$html.='<td>'.$iData['cont1des'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACTO 2</strong></td>';
		$html.='<td>'.$iData['cont2'].'</td>';
		$html.='<td><strong>DESCRIPCION</strong></td>';
		$html.='<td>'.$iData['cont2des'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>CONTACTO 3</strong></td>';
		$html.='<td>'.$iData['cont3'].'</td>';
		$html.='<td><strong>DESCRIPCION</strong></td>';
		$html.='<td>'.$iData['cont3des'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TIPO DE ORDEN</strong></td>';
		$html.='<td>'.$iData['type_order'].'</td>';
		$html.='<td></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';
		return $html;
	}

////////////////////////////// Television HTML Email //////////////////////////////

	public static function GetSubTvHtml($iData)
	{
		$html 	= "";
		$html.='<h3 style="margin-top: 20px; text-align: center;">TELEVISION </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>TV</strong></td>';
		$html.='<td>'.$iData['tv'].'</td>';
		$html.='<td><strong>DECODIFICADORES</strong></td>';
		$html.='<td>'.$iData['decoder'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PAQUETES</strong></td>';
		$html.='<td>'.$iData['package'].'</td>';
		$html.='<td><strong>DECODERS  DVR</strong></td>';
		$html.='<td>'.$iData['dvrs'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>PROVEEDOR ANT.</strong></td>';
		$html.='<td>'.$iData['provider'].'</td>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$iData['payment'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>HD</strong></td>';
		$html.='<td>'.$iData['hd'].'</td>';
		$html.='<td><strong>DVR</strong></td>';
		$html.='<td>'.$iData['dvr'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>HBO</strong></td>';
		$html.='<td>'.$iData['hbo'].'</td>';
		$html.='<td><strong>CINEMAX</strong></td>';
		$html.='<td>'.$iData['cinemax'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>STARZ</strong></td>';
		$html.='<td>'.$iData['starz'].'</td>';
		$html.='<td><strong>SHOWTIME</strong></td>';
		$html.='<td>'.$iData['showtime'].'</td>';
		$html.='</tr>';

		$html.='<tr>';
		$html.='<td><strong>GIFT ENTERTAINMENT</td</strong></td>';
		$html.='<td>'.$iData['gift_ent'].'</td>';
		$html.='<td><strong>GIFT CHOICE</strong></td>';
		$html.='<td>'.$iData['gift_cho'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>GIFT XTRA</td</strong></td>';
		$html.='<td>'.$iData['gift_xtra'].'</td>';
		$html.='<td><strong></strong></td>';
		$html.='<td></td>';
		$html.='<tr>';
		$html.='<td><strong>PUBLICIDAD AT&T</td</strong></td>';
		$html.='<td>'.$iData['adv'].'</td>';
		$html.='<td><strong>TIPO DE ORDER</strong></td>';
		$html.='<td>'.$iData['type_order'].'</td>';
		$html.='</tr>';

		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';
		return $html;

		if($sub)
		{

		}else{

			return false;
		}
	
	}

////////////////////////////// Internet HTML Email //////////////////////////////
	
	public static function GetSubMRouterHtml($iData)
	{
		$html 	= "";
		$html='<h3 style="margin-top: 20px; text-align: center;">WIRELESS </h3>';
		$html.='<table class="head-wrap" style="background: #ecf8ff">';							
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>CANTIDAD</strong></td>';
		$html.='<td>'.$iData['cant'].'</td>';
		$html.='<td><strong>TIPO DE RED</strong></td>';
		$html.='<td>'.$iData['red'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>SSID</strong></td>';
		$html.='<td>'.$iData['ssid'].'</td>';
		$html.='<td><strong>PASSWORD</strong></td>';
		$html.='<td>'.$iData['password'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>METODO DE PAGO</strong></td>';
		$html.='<td>'.$iData['payment'].'</td>';
		$html.='<td><strong>FECHA DE ENVIO</strong></td>';
		$html.='<td>'.$iData['created'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>TIPO DE ORDEN</strong></td>';
		$html.='<td>'.$iData['type_order'].'</td>';
		$html.='<td><strong></strong></td>';
		$html.='<td></td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';
		return $html;
	}

////////////////////////////// New Lead Index //////////////////////////////

	public static function GetServObjectionHtml($info, $serv, $client)
	{


		$html 	=	"";
		$html.='<section>';
		$html.='<label class="select select-multiple">';
		$html.='<input name="cancel_service" id="cancel_service" value="'.$serv.'" readonly hidden>';
		$html.='<input name="cancel_client" id="cancel_client" value="'.$client.'" readonly hidden>';
		$html.='<select multiple="" name="objection_sel[]" id="objection_sel" class="custom-scroll" style="height: 250px;">';

		foreach ($info as $i => $inf) 
		{
				$html.='<option value="'.$inf['id'].'">'.$inf['name'].'</option>';
			// if(($inf['service'] == 1) || $inf['service'] == $serv)
			// {
			// }
		}
		$html.='</select> ';
		$html.='</label>';
		$html.='<div class="note">';
		$html.='<strong>Nota:</strong> Puede Seleccionar mas de una opcion';
		$html.='</div>';
		$html.='</section>';
		return $html;
	}

////////////////////////////// New Lead Index //////////////////////////////

}