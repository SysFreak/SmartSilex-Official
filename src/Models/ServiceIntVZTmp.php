<?php
namespace App\Models;

use Model;

use App\Models\ServiceIntVZTmp;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\Referred;
use App\Models\User;
use App\Models\Payment;
use App\Models\StatusService;
use App\Models\OrderType;

use App\Lib\Config;
use App\Lib\DBSmart;
use PDO;

class ServiceIntVZTmp extends Model 
{
	static $_table = 'cp_service_vzla_int_tmp';
	
	Public $_fillable = array('client_id', 'int_res_id', 'pho_res_id', 'int_com_id', 'pho_com_id', 'ref_id', 'ref_list_id', 'sup_id', 'sup_ope_id', 'bank_id', 'payment_id', 'amount', 'transference', 'trans_date', 'email', 'service_id', 'type_order_id', 'origen_id', 'additional', 'operator_id', 'created_at');

	public static function SaveService($info)
	{	
		$date = date('Y-m-d H:i:s', time());

		if($info['sup'] == 0)
		{	$user1 = $info['operator']; $user2 = $info['operator']; }
		else
		{	$user1 = $info['sup_list']; $user2 = $info['operator']; }

		$user =	($info['sup'] == 0) ? $info['operator'] : $info['sup_list'];

		$query = 'INSERT INTO cp_service_vzla_int_tmp(client_id, int_res_id, pho_res_id, int_com_id, pho_com_id, ref_id, ref_list_id, sup_id, sup_ope_id, bank_id, payment_id, amount, transference, trans_date, email, service_id, type_order_id, origen_id, additional, owen_id, assit_id, created_at) VALUES ("'.$info['client'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['ref'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$info['bank'].'", "'.$info['payment'].'", "'.$info['amount'].'", "'.$info['trans'].'", "'.$info['trans_date'].'", "'.$info['email'].'", "'.$info['service'].'", "'.$info['order_type'].'", "'.$info['origen'].'", "'.strtoupper($info['additional']).'", "'.$user1.'", "'.$user2.'", "'.$date.'") ';

		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	}

	public static function ServiceInt($client)
	{

		$query 	=	'SELECT client_id, int_res_id, pho_res_id, int_com_id, pho_com_id, ref_id, ref_list_id, sup_id, sup_ope_id, bank_id, payment_id, amount, transference, trans_date, email, service_id, type_order_id, origen_id, additional, owen_id, created_at FROM cp_service_vzla_int_tmp WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'status'		=>	true,
			 	'client' 		=>	$serv['client_id'],
			 	'int_res' 		=>	$serv['int_res_id'],
			 	'pho_res' 		=>	$serv['pho_res_id'],
			 	'int_com' 		=>	$serv['int_com_id'],
			 	'pho_com' 		=>	$serv['pho_com_id'],
			 	'ref' 			=>	"0",
			 	'ref_list' 		=>	"0",
			 	'sup' 			=>	"0",
			 	'sup_list' 		=>	"1",
			 	'bank' 			=>	$serv['bank_id'],
			 	'payment' 		=>	$serv['payment_id'],
			 	'amount' 		=>	$serv['amount'],
			 	'trans'			=>	$serv['transference'],
			 	'trans_date'	=>	$serv['trans_date'],
			 	'email'			=>	$serv['email'],
			 	'service' 		=>	$serv['service_id'],
			 	'order_type' 	=>	$serv['type_order_id'],
			 	'origen' 		=>	$serv['origen_id'],
			 	'additional'	=>	$serv['additional'],
			 	'operator' 		=>	$serv['owen_id'],
			 	'ope'			=>	User::GetUserById($serv['owen_id'])['username'],
			 	'created' 		=>	$serv['created_at'],
			];

		}else{

		 	return [
		 		'status' => false, 'client' => $client, 'int_res' => '14', 'pho_res' => "9", 'int_com' => "20", 'pho_com' => "13", 'ref' => "0", 'ref_list' => "0", 'sup' 	=> "0", 'sup_list' => "1", 'bank' => "115", 'payment' => "1", 'amount' => "0", 'trans' => "0", 'trans_date' => "", 'email' => "", 'service' => "1", 'order_type' => "1", 'origen' => "1",'additional' => ""
		 	];				
		}
	
	}


}