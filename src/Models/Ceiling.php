<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Ceiling;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Ceiling extends Model {

	static $_table 		= 'data_ceiling';

	Public $_fillable 	= array('name', 'status_id', 'country_id');

	public static function GetCeiling()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_ceiling ORDER BY id ASC';
		$cei 	=	DBSmart::DBQueryAll($query);

		$ceiling = array();

		if($cei <> false)
		{
			foreach ($cei as $k => $val) 
			{
				$ceiling[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $ceiling;
		}else{ 	return false;	}
	}

	public static function GetCeilingById($id)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_ceiling WHERE id = "'.$id.'" ORDER BY id ASC';
		$cei 	=	DBSmart::DBQuery($query);

		if($cei <> false)
		{
			return array(
				'id' => $cei['id'], 'name' => $cei['name'], 'status' => $cei['status_id'], 'country' => $cei['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetCeilingByStatus()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_ceiling WHERE status_id = "1" ORDER BY id ASC';
		$cei 	=	DBSmart::DBQuery($query);

		if($cei <> false)
		{
			return array(
				'id' => $cei['id'], 'name' => $cei['name'], 'status' => $cei['status_id'], 'country' => $cei['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetCeilingByName($info)
	{
		$query 	= 	'SELECT id, name, status_id, country_id FROM data_ceiling WHERE name = "'.$info.'"';
		$cei 	=	DBSmart::DBQuery($query);

		if($cei <> false)
		{
			return array(
				'id' => $cei['id'], 'name' => $cei['name'], 'status' => $cei['status_id'], 'country' => $cei['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetCeilingByCountryID($id)
	{

		$query 	= 	'SELECT id, name FROM data_ceiling WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$to 	=	DBSmart::DBQueryAll($query);
		return ($to <> false) ? $to : false;
	}

	public static function GetCeilingByCountry($id)
	{

		$query 	= 	'SELECT id, name FROM data_ceiling WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$cei 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($cei)
		{
			foreach ($cei as $k => $val) 
			{ $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';}

			return $html;

		}else{ return $html; }
	}

	public static function SaveCeiling($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_ce']));

		if($info['type_ce'] == 'new')
		{
			$query 	= 	'INSERT INTO data_ceiling(name, status_id, country_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_ce'].'", "'.$info['country_id_ce'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_ce'] == 'edit') 
		{
			$query 	=	'UPDATE data_ceiling SET name="'.$name.'", status_id="'.$info['status_id_ce'].'", country_id="'.$info['country_id_ce'].'" WHERE id = "'.$info['id_ce'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}

}

