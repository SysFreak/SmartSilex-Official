<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Role;

use App\Lib\Config;
use App\Lib\DBSmart;

use PDO;

class Role extends Model {

	static $_table = 'data_roles';
	
	Public $_fillable = array('name', 'status_id');

	public static function GetRole()
	{
		$query 	=	'SELECT id, name, status_id FROM data_roles ORDER BY id DESC';
		$dep 	=	DBSmart::DBQueryAll($query);

		$depart = array();

		if($dep <> false)
		{
			foreach ($dep as $k => $val) 
			{
				$depart[$k] = array(
					'id' 	 	=> 	$val['id'], 
					'name' 	 	=> 	$val['name'], 
					'status' 	=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'	=>	$val['status_id']
				);
			}	        
	        return $depart;

		}else{ return false; }
		
	}

	public static function GetRolById($id)
	{
		$query 		=	'SELECT id, name, status_id FROM data_roles WHERE id = "'.$id.'"';
		$user       =   DBSmart::DBQuery($query);

		if($user <> false)
		{
			return array(
				'id'           => $user['id'],
				'name'         => $user['name'],
				'status'       => $user['status_id']
			);

		}else{ return array('id' => '', 'name' => '',	'status' => ''); }

	}

	public static function GetRolByName($info)
	{

		$query 		=	'SELECT id, name, status_id FROM data_roles WHERE name = "'.$info.'"';
		$dep        =   DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return array('id' => $dep['id'], 'name' => $dep['name'], 'status' => $dep['status_id']);

		}else{ return false; }
	}

	public static function SaveRol($info)
	{

		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$name 		= 	strtoupper($_replace->deleteTilde($info['name_r']));

		if($info['type_r'] == 'new')
		{
			$query 	= 	'INSERT INTO data_roles(name, status_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_r'].'", "'.$date.'")';
			$dep 	=	DBSmart::DataExecuteLastID($query);

			return ($dep <> false ) ? $dep : false;
		}
		elseif ($info['type_r'] == 'edit') 
		{
			$query 	=	'UPDATE data_roles SET name="'.$name.'",status_id="'.$info['status_id_r'].'",created_at="'.$date.'" WHERE id = "'.$info['id_r'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}