<?php
namespace App\Models;

use Model;

use App\Models\Approvals;
use App\Models\ApprovalsRespCancel;
use App\Models\Status;
use App\Models\Country;
use App\Models\Service;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class ApprovalsRespCancel extends Model 
{
	static $_table 		= 'data_appro_obj_cancel';

	Public $_fillable 	= array('id', 'name', 'service_id', 'country_id', 'status_id', 'created_at');

/////////////////////////////////////////////////////////////////////////

	public static function SaveObjCancel($info)
	{

		$date 	=	date('Y-m-d H:i:s', time());

		$name 	= 	strtoupper($_replace->deleteTilde($info['name']));

		$query 	=	'INSERT INTO data_appro_obj_cancel(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service'].'", "'.$info['country'].'", "'.$info['status'].'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false) ? true : false;
	}

/////////////////////////////////////////////////////////////////////////

	public static function GetObjectionsByService($serv)
	{		
		$query	=	'SELECT id, name, status_id FROM data_appro_obj_cancel WHERE service_id = "'.$serv.'" AND status_id = "1" ORDER BY id ASC';
		$serv 	=	DBSmart::DBQueryAll($query);

		if($serv <> false)
		{
			foreach ($serv as $k => $val) 
			{ $obj[$k] = ['id' => $val['id'], 'name' => $val['name'], 'status_id' => $val['status_id'] ]; }

			return $obj;

		}else{	return false; 	}
	}

/////////////////////////////////////////////////////////////////////////

	public static function GetApproCancels()
	{
		$query	=	'SELECT * FROM data_appro_obj_cancel ORDER BY id ASC';
		$apro 	=	DBSmart::DBQueryAll($query);

		if($apro <> false)
		{
			foreach ($apro as $k => $sc) 
			{
				$cancel[$k]	=	[
					'id'			=>	$sc['id'],
					'name'			=>	$sc['name'],
					'country_id'	=>	$sc['country_id'],
					'country'		=>	Country::GetCountryById($sc['country_id'])['name'],
					'status_id'		=>	$sc['status_id'],
					'status'		=>	Status::GetStatusById($sc['status_id'])['name'],
					'service_id'	=>	$sc['service_id'],
					'service'		=>	Service::ServiceById($sc['service_id'])['name'],
					'created'		=>	$sc['created_at']
				];
			}
			return $cancel;
		}else{
			return false;
		}


	}
/////////////////////////////////////////////////////////////////////////

	public static function GetAproCancelById($id)
	{
	
		$query	=	'SELECT * FROM data_appro_obj_cancel WHERE id = "'.$id.'"';
		$sco 	=	DBSmart::DBQuery($query);

		if($sco)
		{
			return [
				'id' 			=> 	$sco['id'],
				'name' 			=> 	$sco['name'],
				'country_id'	=>	$sco['country_id'],
				'country'		=>	Country::GetCountryById($sco['country_id'])['name'],
				'status_id'		=>	$sco['status_id'],
				'status'		=>	Status::GetStatusById($sco['status_id'])['name'],
				'service_id'	=>	$sco['service_id'],
				'service'		=>	Service::ServiceById($sco['service_id'])['name'],
				'created'		=>	$sco['created_at']
			];

		}else{	return false;	}
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveService($info)
	{

		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_can']));

		if($info['type_can'] == 'new')
		{
			$query 	=	'INSERT INTO data_appro_obj_cancel(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'","'.$info['service_id_can'].'","'.$info['country_id_can'].'","'.$info['status_id_can'].'","'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? $dep['last_id'] : false;
		}
		elseif ($info['type_can'] == 'edit') 
		{
			$query 	=	'UPDATE data_appro_obj_cancel SET name="'.$name.'",service_id="'.$info['service_id_can'].'",country_id="'.$info['country_id_can'].'",status_id="'.$info['status_id_can'].'" WHERE id = "'.$info['id_can'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}

/////////////////////////////////////////////////////////////////////////

}