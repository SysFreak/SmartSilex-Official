<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;

class Statics extends Model {


	public static function GetStatics($dates)
	{
		
        $_replace   =   new Config();

        if($dates == '')
        {
	        $week       =   $_replace->CurrentMonthDays();
	        $betw       =   'created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
	        $betwT      =   't1.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';

        }else{
	        
	        $betw 	=	'created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ';
	        $betwT 	=	't1.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].'23:59:59"';

        }

        $query  =   'SELECT t1.id, t1.username FROM users AS t1 INNER JOIN data_departament AS t2 ON (t1.departament_id = t2.id) AND t2.id = "6" AND t1.status_id = "1"';
        $users  =   DBSmart::DBQueryAll($query);

        $query  =   'SELECT (SELECT COUNT(*) FROM cp_contracts WHERE country = "VENEZUELA" AND '.$betw.') AS TVen,
        (SELECT COUNT(*) FROM cp_contracts WHERE country = "PUERTO RICO" AND '.$betw.') AS TPr,
        (SELECT COUNT(*) FROM cp_contracts WHERE '.$betw.') AS T,
        (SELECT ROUND((TVen/T) * 100)) AS TPVen,
        (SELECT ROUND((TPr/T) * 100)) AS TPPr';
        $cData  =   DBSmart::DBQuery($query);

        $query 	=	'SELECT t2.username, COUNT(*) AS T FROM cp_contracts AS t1 INNER JOIN users AS t2 ON (t1.created_by = t2.username) AND t1.country = "VENEZUELA" AND '.$betwT.' GROUP BY t1.created_by ORDER BY T DESC LIMIT 5';
        $TopVE 	=	DBSmart::DBQueryAll($query);

        $query 	=	'SELECT t2.username, COUNT(*) AS T FROM cp_contracts AS t1 INNER JOIN users AS t2 ON (t1.created_by = t2.username) AND t1.country = "PUERTO RICO" AND '.$betwT.' GROUP BY t1.created_by ORDER BY T DESC LIMIT 5';
        $TopPR 	=	DBSmart::DBQueryAll($query);

        return 	['users' => $users, 'cData' => $cData, 'TopPR' => $TopPR, 'TopVE' => $TopVE];

	}

	public static function GetStatusById($id)
	{
		$query 	=	'SELECT id, name FROM data_status WHERE id = "'.$id.'"';
		$sta 	=	DBSmart::DBQuery($query);

		if($sta <> false)
		{
			return [ 'id'	=>	$sta['id'], 'name'	=>	$sta['name'] ];

		}else{ 	return false;	}

	}

	public static function TopTable($info, $country)
	{
		$html = "";
		$html.='<p style="text-align: center;"> <span>'.$country.'</span></p>';
		$html.='<div class="widget-body">';
		$html.='<div class="table-responsive">';
		$html.='<table class="table table-bordered table-striped" border="1" style="border-collapse: collapse;">';
		$html.='<tbody>';
		$html.='<td>#</td>';
		$html.='<td></td>';
		$html.='<td>OPERADOR</td>';
		$html.='<td>CANT</td>';
		$html.='</tr>';

		if($info <> false)
		{
			foreach ($info as $i => $inf) 
			{
				$html.='<tr>';
				$html.='<td>'.($i+1).'</td>';
				if($i == 0)
				{
				$html.='<td><img src="/assets/img/approvals/SC'.($i+1).'.jpg" style="width: 10px;"></td>';
				}elseif ($i == 1) {
				$html.='<td><img src="/assets/img/approvals/SC'.($i+1).'.jpg" style="width: 10px;"></td>';
				}elseif ($i == 2) {
				$html.='<td><img src="/assets/img/approvals/SC'.($i+1).'.jpg" style="width: 10px;"></td>';
				}else{
				$html.='<td></td>';
				}
				$html.='<td>'.$inf['username'].'</td>';
				$html.='<td>'.$inf['T'].'</td>';
			}

		}else{
			$html.='<tr>';
			$html.='<td colspan="4">SIN DATOS PROCESADOS</td>';
			$html.='</tr>';

		}
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';
      return $html;
	
	}

	public static function LoadStaticsDataEntry($serv, $dates)
	{
		$_replace 	= 	new Config();

		if($dates == '')
        {
	        $week	=   $_replace->CurrentMonthDays();
	        $betw 	=   'BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
        }else{        
        	$betw 	=	'BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ';
        }

		if($serv == "2")
		{
			$query 		=	'SELECT id FROM data_score WHERE service_id = "2" AND (id <> "1" AND id <> "2" AND id <> "3") ORDER BY id ASC';
			$scTV		=	DBSmart::DBQueryAll($query);
			$nscore  	=   "";
		    $nscore  	=   "(";
		    foreach ($scTV as $k => $ka) 
		    {
		        $nscore.='score = "'.$ka['id'].'" OR ';
		    }
		    $nscore 	= substr($nscore, 0, -4);
		    $nscore.=')';

			$betwA5		=	't1.created_at '.$betw.'';
			$betwC5		=	't2.created_at '.$betw.'';
			$score 		=	'(score = "1" OR score = "2" OR score = "3")';
			$score1 	=	'(t1.score = "1" OR t1.score = "2" OR t1.score = "3")';

			$query 		=	'SELECT t2.id, t2.name, COUNT(*) AS T FROM cp_appro_serv AS t1 INNER JOIN data_score AS t2 ON (t1.score = t2.id) AND t1.service_id = "'.$serv.'" AND t1.score = "1" AND '.$betwA5.' GROUP BY t1.score ORDER BY t2.id ASC';
			$score1 	=	DBSmart::DBQuery($query);
			$query 		=	'SELECT t2.id, t2.name, COUNT(*) AS T FROM cp_appro_serv AS t1 INNER JOIN data_score AS t2 ON (t1.score = t2.id) AND t1.service_id = "'.$serv.'" AND t1.score = "2" AND '.$betwA5.' GROUP BY t1.score ORDER BY t2.id ASC';
			$score2 	=	DBSmart::DBQuery($query);
			$query 		=	'SELECT t2.id, t2.name, COUNT(*) AS T FROM cp_appro_serv AS t1 INNER JOIN data_score AS t2 ON (t1.score = t2.id) AND t1.service_id = "'.$serv.'" AND t1.score = "3" AND '.$betwA5.' GROUP BY t1.score ORDER BY t2.id ASC';
			$score3 	=	DBSmart::DBQuery($query);

			$staScore[0]	=	['id'	=>	((isset($score1['id'])) ? $score1['id'] : '1'), 'name' => ((isset($score1['name'])) ? $score1['name'] : 'SCORE 1'), 'T' => ((isset($score1['T'])) ? $score1['T'] : '0')];
			$staScore[1]	=	['id'	=>	((isset($score2['id'])) ? $score2['id'] : '1'), 'name' => ((isset($score2['name'])) ? $score2['name'] : 'SCORE 2'), 'T' => ((isset($score2['T'])) ? $score2['T'] : '0')];
			$staScore[2]	=	['id'	=>	((isset($score3['id'])) ? $score3['id'] : '1'), 'name' => ((isset($score3['name'])) ? $score3['name'] : 'SCORE 3'), 'T' => ((isset($score3['T'])) ? $score3['T'] : '0')];
		}
		elseif($serv == "4")
		{
			$query 		=	'SELECT id FROM data_score WHERE service_id = "4" AND id <> "22" ORDER BY id ASC';
			$scTV		=	DBSmart::DBQueryAll($query);
			$nscore  	=   "";
		    $nscore  	=   "(";
		    foreach ($scTV as $k => $ka) 
		    {
		        $nscore.='score = "'.$ka['id'].'" OR ';
		    }
		    $nscore 	= substr($nscore, 0, -4);
		    $nscore.=')';

			$betwA5		=	't1.created_at '.$betw.'';
			$betwC5		=	't2.created_at '.$betw.'';
			$score 		=	'(score = "22")';
			$score1 	=	'(t1.score = "22")';

			$query 	=	'SELECT t2.id, t2.name, COUNT(*) AS T FROM cp_appro_serv AS t1 INNER JOIN data_score AS t2 ON (t1.score = t2.id) AND t1.service_id = "'.$serv.'" AND '.$score1.' AND '.$betwA5.' GROUP BY t1.score ORDER BY t2.id ASC';
			$staScore 	=	DBSmart::DBQueryAll($query);

		}
		elseif($serv == "6")
		{
			$query 		=	'SELECT id FROM data_score WHERE service_id = "6" AND id <> "29" ORDER BY id ASC';
			$scTV		=	DBSmart::DBQueryAll($query);
			$nscore  	=   "";
		    $nscore  	=   "(";
		    foreach ($scTV as $k => $ka) 
		    {
		        $nscore.='score = "'.$ka['id'].'" OR ';
		    }
		    $nscore 	= substr($nscore, 0, -4);
		    $nscore.=')';

			$betwA5		=	't1.created_at '.$betw.'';
			$betwC5		=	't2.created_at '.$betw.'';
			$score 		=	'(score = "29")';		
			$score1		=	'(t1.score = "29")';	

			$query 	=	'SELECT t2.id, t2.name, COUNT(*) AS T FROM cp_appro_serv AS t1 INNER JOIN data_score AS t2 ON (t1.score = t2.id) AND t1.service_id = "'.$serv.'" AND '.$score1.' AND '.$betwA5.' GROUP BY t1.score ORDER BY t2.id ASC';
			$staScore 	=	DBSmart::DBQueryAll($query);	
		
		}

		$query 	=	'
		SELECT 
		(SELECT COUNT(*) FROM cp_appro_serv WHERE service_id="'.$serv.'" AND created_at '.$betw.' ) AS T,
		(SELECT COUNT(*) FROM cp_appro_serv WHERE service_id="'.$serv.'" AND status_id="1" AND cancel="1" AND created_at '.$betw.' ) AS TPend,
		(SELECT COUNT(*) FROM cp_appro_serv WHERE service_id="'.$serv.'" AND status_id="2" AND cancel="1" AND '.$score.' AND created_at '.$betw.' ) AS TApro,
		(SELECT COUNT(*) FROM cp_appro_serv WHERE service_id="'.$serv.'" AND status_id="2" AND cancel="1" AND '.$nscore.' AND created_at '.$betw.' ) AS TNApro,
		(SELECT COUNT(*) FROM cp_appro_serv WHERE service_id="'.$serv.'" AND status_id="0" AND cancel="0" AND created_at '.$betw.') AS TCanc,
		(SELECT ROUND((TPend/T) *100)) AS PPen,
		(SELECT ROUND((TApro/T) *100)) AS PApro,
		(SELECT ROUND((TNApro/T) *100)) AS PNApro,
		(SELECT ROUND((TCanc/T) *100)) AS PCan
		';
		$static	=	DBSmart::DBQuery($query);
		return [
			'sta'		=>	$static,
			'score'		=>	$staScore
		];

	}

	public static function LoadStaticsDEUsers($dates)
	{

		$_replace 	= 	new Config();

		if($dates == '')
        {
	        $week	=   $_replace->CurrentMonthDays();
	        $betw 	=   'BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
        }else{        
        	$betw 	=	'BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" ';
        }

		$query 	=	'SELECT id, username FROM users WHERE departament_id = "6" AND status_id="1" ORDER BY id ASC';
		$users 	=	DBSmart::DBQueryAll($query);

		$serv 	=	'(service_id = "2" OR service_id = "4")';
		$serv1 	=	'(t1.service_id = "2" OR t1.service_id="4")';
		$table 	=	'cp_time_calls_pr_new';

		foreach ($users as $u => $use) 
		{
			$query	=	'SELECT 
			(SELECT COUNT(*) FROM cp_appro_serv WHERE finish_by = "'.$use['id'].'" AND '.$serv.' AND finish_at '.$betw.') AS TApro, 
			(SELECT COUNT(*) FROM cp_appro_serv AS t1 INNER JOIN cp_coord_serv AS t2 ON (t1.ticket = t2.ticket) INNER JOIN cp_coord_proc_tmp AS t3 ON (t2.coord_id = t3.id) AND '.$serv1.' AND t1.finish_at '.$betw.' AND t3.created_by = "'.$use['id'].'" AND t3.created_at '.$betw.') AS TCoord, 
			(SELECT SUM(inb) FROM '.$table.' WHERE username = "'.$use['username'].'" AND created_at '.$betw.') AS TCallIn, 
			(SELECT (SUM(oubto) + SUM(ouna)) FROM '.$table.' WHERE username = "'.$use['username'].'" AND created_at '.$betw.') AS TCallOut, 
			(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(ticalls))) FROM '.$table.' WHERE username = "'.$use['username'].'" AND created_at '.$betw.') AS TCall';
			$iData 	=	DBSmart::DBQuery($query) ;

			$userPR[$u]	=	[
				'username'	=>	$use['username'],
				'TApro'		=>	$iData['TApro'],
				'TCoord'	=>	$iData['TCoord'],
				'TCallIn'	=>	$iData['TCallIn'],
				'TCallOut'	=>	$iData['TCallOut'],
				'TCall'		=>	$iData['TCall'],
			];
		}


		$serv2 	=	'(service_id = "6")';
		$serv3 	=	'(t1.service_id = "6")';
		$table 	=	'cp_time_calls_vz_new';

		foreach ($users as $u => $use) 
		{
			$query	=	'SELECT 
			(SELECT COUNT(*) FROM cp_appro_serv WHERE finish_by = "'.$use['id'].'" AND '.$serv2.' AND finish_at '.$betw.') AS TApro, 
			(SELECT COUNT(*) FROM cp_appro_serv AS t1 INNER JOIN cp_coord_serv AS t2 ON (t1.ticket = t2.ticket) INNER JOIN cp_coord_proc_tmp AS t3 ON (t2.coord_id = t3.id) AND '.$serv3.' AND t1.finish_at '.$betw.' AND t3.created_by = "'.$use['id'].'" AND t3.created_at '.$betw.') AS TCoord, 
			(SELECT SUM(inb) FROM '.$table.' WHERE username = "'.$use['username'].'" AND created_at '.$betw.') AS TCallIn, 
			(SELECT (SUM(oubto) + SUM(ouna)) FROM '.$table.' WHERE username = "'.$use['username'].'" AND created_at '.$betw.') AS TCallOut, 
			(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(ticalls))) FROM '.$table.' WHERE username = "'.$use['username'].'" AND created_at '.$betw.') AS TCall';

			$iData 	=	DBSmart::DBQuery($query) ;

			$userVE[$u]	=	[
				'username'	=>	$use['username'],
				'TApro'		=>	$iData['TApro'],
				'TCoord'	=>	$iData['TCoord'],
				'TCallIn'	=>	$iData['TCallIn'],
				'TCallOut'	=>	$iData['TCallOut'],
				'TCall'		=>	$iData['TCall'],
			];
		}

		return 	['userPR' =>	$userPR, 'userVE' => $userVE];

	}

	public static function HTMLStaticsDataEntry($serv)
	{
		$html = '';
		$html.='<h1>'.$serv['sta']['T'].'<span class="subscript"> / mo</span></h1>';
		$htmlG = '';
		$htmlG.='<ul class="list-unstyled text-left">';
          $htmlG.='<li><i class="fa fa-check text-alert"></i> Pendientes <strong> '.$serv['sta']['TPend'].' / mon</strong></li>';
          $htmlG.='<li><i class="fa fa-check text-success"></i> Aprobadas <strong> '.$serv['sta']['TApro'].' / mon</strong></li>';
          $htmlG.='<li><i class="fa fa-check text-danger"></i> No Aprobadas <strong> '.$serv['sta']['TNApro'].' / mon</strong></li>';
          $htmlG.='<li><i class="fa fa-check text-danger"></i> Canceladas <strong> '.$serv['sta']['TCanc'].' / mon</strong></li>';
        $htmlG.='</ul>';

        return ['T' => $html, 'G' => $htmlG];

	}

	public static function HTMLStaticsDEUsers($info)
	{
		$html = '';
		$html.='<table class="table table-bordered table-striped" border="1" style="border-collapse: collapse;">';
			$html.='<tbody>';
				$html.='<tr>';
					$html.='<td>#</td>';
					$html.='<td>OPERADOR</td>';
					$html.='<td>APROBACIONES</td>';
					$html.='<td>COORDINACIONES</td>';
					$html.='<td>ENTRANTES</td>';
					$html.='<td>SALIENTES</td>';
					$html.='<td>TOTAL</td>';
				$html.='</tr>';
				if($info <> '')
				{
					foreach ($info as $i => $inf) 
					{
						if($inf['username'] <> 'APROCOORD')
						{
							$html.='<tr>';
								$html.='<td>'.$i.'</td>';
								$html.='<td>'.$inf['username'].'</td>';
								$html.='<td>'.(($inf['TApro'] <> null) ? $inf['TApro'] : '0').'</td>';
								$html.='<td>'.(($inf['TCoord'] <> null) ? $inf['TCoord'] : '0').'</td>';
								$html.='<td>'.(($inf['TCallIn'] <> null) ? $inf['TCallIn'] : '0').'</td>';
								$html.='<td>'.(($inf['TCallOut'] <> null) ? $inf['TCallOut'] : '0').'</td>';
								$html.='<td>'.(($inf['TCall'] <> null) ? $inf['TCall'] : '00:00:00').'</td>';
							$html.='</tr>';
						}
					}

				}else{
					$html.='<tr>';
						$html.='<td colspan="7">SIN INFORMACION PARA MOSTRAR</td>';
					$html.='</tr>';
				}
			$html.='</tbody>';
		$html.='</table>';
		
        return $html;

	}

}