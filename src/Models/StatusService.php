<?php
namespace App\Models;

use Model;
use App\Models\StatusService;
use App\Models\Country;
use App\Models\Status;
use App\Models\Service;

use App\Lib\Config;
use App\Lib\DBSmart;

class StatusService extends Model {

	static $_table = 'data_status_service';
	
	Public $_fillable = array('name', 'service_id', 'country_id', 'status_id', 'created_at');

	public static function GetStatus()
	{
		
		$query 	= 	'SELECT * FROM data_status_service ORDER BY id ASC';
		$serv 	=	DBSmart::DBQueryAll($query);

		$status = array();

		if($serv <> false)
		{
			foreach ($serv as $k => $val) 
			{
				$status[$k] = array('id' => $val['id'], 'name' => $val['name'], 'service_id' => $val['service_id'], 'country_id' => $val['country_id'], 'status_id' => $val['status_id']);
			}
	        
	        return $status;
		}else{ 	return false;	}
	}

	public static function GetStatusById($id)
	{
		$query 	= 	'SELECT * FROM data_status_service WHERE id = "'.$id.'"';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return array(
				'id'      		=> 	$serv['id'],
				'name'    		=> 	$serv['name'],
				'service' 		=> 	$serv['service_id'],
				'country' 		=> 	$serv['country_id'],
				'status'  		=> 	$serv['status_id'],
				'service_id'	=>	$serv['service_id'],
				'status_id'		=>	$serv['status_id'],
				'country_id'	=>	$serv['country_id'],
			);

		}else{	return false; }
	}
}