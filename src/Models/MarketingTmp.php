<?php
namespace App\Models;

use Model;

use App\Models\Marketing;
use App\Models\MarketingTmp;
use App\Models\Leads;

use App\Lib\Config;
use App\Lib\DBSmart;

class MarketingTmp extends Model 
{

	static $_table 		= 'cp_leads_tmp_mkt';

	Public $_fillable 	= array('client_id', 'origen_id', 'medium_id', 'objetive_id', 'post_id', 'forms_id', 'conv_id', 'services_id', 'campaign_id', 'type', 'user_id', 'created_at');

	public static function SaveDB($info, $type, $user)
	{

		$date 		= 	date('Y-m-d H:i:s', time());
		
		$_replace 	= 	new Config();

		$iData 	=	[
			'client' 	=> (isset($info['mkt_client'])) 		? strtoupper($_replace->deleteTilde($info['mkt_client']))	: "1",
			'origen'  	=> (isset($info['mkt_origen'])) 		? strtoupper($_replace->deleteTilde($info['mkt_origen']))	: "1",
			'medium'  	=> (isset($info['mkt_medium'])) 		? strtoupper($_replace->deleteTilde($info['mkt_medium']))	: "1",
			'objetive'  => (isset($info['mkt_objective']))		? strtoupper($_replace->deleteTilde($info['mkt_objective'])): "1",
			'post'  	=> (isset($info['mkt_post'])) 			? strtoupper($_replace->deleteTilde($info['mkt_post'])) 	: "1",
			'forms'  	=> (isset($info['mkt_form'])) 			? strtoupper($_replace->deleteTilde($info['mkt_form'])) 	: "1",
			'conv'  	=> (isset($info['mkt_conv'])) 			? strtoupper($_replace->deleteTilde($info['mkt_conv'])) 	: "1",
			'services'  => (isset($info['mkt_service']))		? strtoupper($_replace->deleteTilde($info['mkt_service'])) 	: "1",
			'campaign'  => (isset($info['mkt_campaign'])) 		? strtoupper($_replace->deleteTilde($info['mkt_campaign'])) : "1",
			'type'  	=> $type,
			'user'  	=> $user,
			'created'	=> $date
		];

		$query 	=	'INSERT INTO cp_leads_tmp_mkt(client_id, origen_id, medium_id, objetive_id, post_id, forms_id, conv_id, services_id, campaign_id, type, user_id, created_at) VALUES ("'.$iData['client'].'", "'.$iData['origen'].'", "'.$iData['medium'].'", "'.$iData['objetive'].'", "'.$iData['post'].'", "'.$iData['forms'].'", "'.$iData['conv'].'", "'.$iData['services'].'", "'.$iData['campaign'].'", "'.$iData['type'].'", "'.$iData['user'].'", "'.$iData['created'].'")';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}
}