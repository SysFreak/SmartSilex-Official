<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class IspStatus extends Model 
{

	static $table 		= 	'data_isp_status';
	static $fillable	=	array('id', 'name', 'status_id', 'created_at');

	public static function Created($data)
	{
		$query 	=	'INSERT INTO '.self::$table.' (name, status_id, created_at) VALUES ("'.$data.'", "1", "'.date("Y-m-d H:m:s").'")';
		return 	DBSmart::DataExecuteLastID($query);
	}

	public static function Edit($data)
	{
		$query 	=	'UPDATE '.self::$table.' SET name="'.$data['name'].'" WHERE id="'.$data['id'].'"';
		return 	DBSmart::DataExecute($query);
	}

	public static function Status($data)
	{
		$query 	=	'UPDATE '.self::$table.' SET status_id="'.$data['status'].'" WHERE id="'.$data['id'].'"';
		return 	DBSmart::DataExecute($query);
	}
	
	public static function GetAll()
	{
		$query 	=	'SELECT * FROM '.self::$table.' ORDER BY id ASC';
		return 	DBSmart::DBQueryAll($query);
	}

	public static function GetAllByStatus($data)
	{
		$query 	=	'SELECT * FROM '.self::$table.' WHERE status_id = "'.$data.'" ORDER BY id ASC';
		return 	DBSmart::DBQueryAll($query);
	}

	public static function GetByID($data)
	{
		$query 	=	'SELECT * FROM '.self::$table.' WHERE id = "'.$data.'" ORDER BY id ASC';
		return 	DBSmart::DBQuery($query);
	}

}