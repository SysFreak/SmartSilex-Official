<?php
namespace App\Models;

use Model;

use App\Models\User;
use App\Models\Notes;

use App\Lib\Config;
use App\Lib\DBSmart;

use PDO;

class Notes extends Model {

	static $_table 		= 'cp_notes';

	Public $_fillable 	= array('name', 'client_id', 'note', 'filename', 'conttype', 'attachment', 'size', 'user_id', 'created_at');

	public static function GetNotes()
	{

		$query 	= 	'SELECT id, name, client_id, note, filename, conttype, attachment, size, user_id, created_at FROM cp_notes ORDER BY id DESC';
		$not 	=	DBSmart::DBQueryAll($query);

		if($not <> false)
		{
			foreach ($not as $k => $val) 
			{
				$Notes[$k] = [
					'id' 	 	=> 	$val['id'],
					'note'	 	=> 	$val['note'],
					'name'		=>	$val['filename'],
					'conttype'	=>	$val['conttype'],
					'user' 		=> 	User::GetUserById($val['user_id'])['username'],
					'create' 	=> 	$val['created_at']
				];
			}
	        return $Notes;

		}else{ return false; }
	}

	public static function GetNotesHtml($id)
	{
		$_replace  	=	new Config();
		$query 		= 	'SELECT id, note, user_id, img, type, created_at FROM cp_notes WHERE client_id = "'.$id.'" ORDER BY id DESC';
		$not 		=	DBSmart::DBQueryAll($query);
		$html 		=	"";
		
		if($not <> false)
		{
			$html.='<ul>';
			foreach ($not as $k => $val) 
			{
				$html.='<li class="message">';
				$html.='<div class="message-text" style="text-align: justify;">';
				$html.='<h3><strong><time>'.$_replace->ShowDateAll($val['created_at']).'</time></strong></h3>';
				$html.='<a class="username">'.User::GetUserById($val['user_id'])['username'].'</a>';
				$html.=''.utf8_encode($val['note']).'';
				if($val['img'] <> "")
				{
					$html.='<p class="chat-file row">';
					$html.='<b class="pull-left col-sm-6"> <i class="fa fa-file"></i>Attachment</b>';
					$html.='<span class="col-sm-6 pull-right"> <a target="_blank" href="../../'.$val['img'].'" class="btn btn-xs btn-success">View</a></span>';
					$html.='</p>';
				}
				$html.='</div>';
				$html.='</li>';
			}
			$html.='</ul>';
		}
		return $html;
	}

	public static function SaveNote($info)
	{
		$_replace  	= 	new Config();

		$note  		= 	strtoupper($_replace->deleteTilde($info['notes']));

		$query 		= 	'INSERT INTO cp_notes(client_id, note, img, type, size, user_id, created_at) VALUES ("'.$info['client'].'", "'.$note.'", "", "",  "0", "'.$info['operator'].'", NOW())';

		$note 		=	DBSmart::DataExecute($query);

		return ($note <> false ) ? true : false;
	}

	public static function SaveNoteSF($info, $user)
	{
		$_replace  	= 	new Config();

		$date 		= 	date('Y-m-d H:i:s', time());

		$note  		= 	strtoupper($_replace->deleteTilde($info['notes']));

		$query 		=	'INSERT INTO cp_notes(client_id, note, img, type, size, user_id, created_at) VALUES ("'.$info['client_id_n'].'", "'.$note.'", "", "", "", "'.$user.'", NOW())';

		$note 		=	DBSmart::DataExecute($query);

		return ($note <> false ) ? true : false;

	}

	public static function SaveNoteCF($info, $file, $user)
	{
		$_replace  	= 	new Config();

		$date 		= 	date('Y-m-d H:i:s', time());

		$note  		= 	strtoupper($_replace->deleteTilde($info['notes']));

		$query 		=	'INSERT INTO cp_notes(client_id, note, img, type, size, user_id, created_at) VALUES ("'.$info['client_id_n'].'", "'.$note.'", "'.$file['dir'].'", "'.$file['type'].'", "'.$file['size'].'", "'.$user.'", NOW())';

		$note 		=	DBSmart::DataExecute($query);

		return ($note <> false ) ? true : false;
	}

}