<?php
namespace App\Models;

use Model;

use App\Models\StatusChange;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class StatusChange extends Model 
{

	static $_table 		= 'cp_ope_online_status';

	Public $_fillable 	= array('id', 'user_id', 'status_id', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////
	

//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetStatusChangeByUser($id)
	{
		$query 		= 	'SELECT * FROM cp_ope_online_status WHERE user_id = "'.$id.'" ORDER BY id DESC';
		$status 	=	DBSmart::DBQuery($query);

		if($status <> false)
		{ $res 	=	($status['status_id'] == 1) ? '1' : '0'; }else{ $res 	=	'0'; }

		return $res;
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveStatus($user, $sta)
	{
		$date 	=	date('Y-m-d H:i:s', time());

		$query  =	'INSERT INTO cp_ope_online_status(user_id, status_id, created_at) VALUES("'.$user.'", "'.$sta.'", "'.$date.'")';
		
		$stat 	=	DBSmart::DataExecute($query);

		return ($stat <> false ) ? true : false;
	}

//////////////////////////////////////////////////////////////////////////////////////////

}