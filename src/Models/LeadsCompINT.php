<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\LeadsCompSEC;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadsCompINT extends Model 
{

	static $_table 		= 'cp_coord_comp_data_int';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'installer_date', 'installer', 'contract', 'operator_id', 'created_at');


	public static function GetInfoClient($client)
	{
		$query 	=	'SELECT installer_date, installer, contract FROM cp_coord_comp_data_int WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		$result =	DBSmart::DBQuery($query);

		if($result <> false)
		{
			return [
				'inst_d'	=>	strtoupper($result['installer_date']),
				'instal'	=>	strtoupper($result['installer']),
				'contra'	=>	strtoupper($result['contract'])
			];
		}else{

			return ['inst_d' => '', 'instal' => '', 'contra' => ''];
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function InsertInfoClient($params)
	{
		$query 	=	'INSERT INTO cp_coord_comp_data_int(ticket, client_id, installer_date, installer, contract, operator_id, created_at) VALUES ("'.$params['ticket'].'","'.$params['client'].'", "'.$params['instal_d'].'","'.$params['instal'].'","'.$params['contra'].'","'.$params['operat'].'","'.date('Y-m-d H:m:s').'")';

		$leads 	=	DBSmart::DataExecute($query);

		return ($leads <> false ) ? true : false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

}