<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CoordProcessTmp extends Model
{

	static $_table 		= 'cp_coord_proc_tmp';

	Public $_fillable 	= array('id', 'client_id', 'ticket', 'resp_id', 'created_by', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SaveProcessCoord($info)
	{

		$date	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_coord_proc_tmp(client_id, ticket, resp_id, created_by, created_at) VALUES ("'.$info['client'].'", "'.$info['ticket'].'", "'.$info['answered'].'", "'.$info['created'].'", "'.$date.'") ';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;
	}

//////////////////////////////////////////////////////////////////////////////////////////

}