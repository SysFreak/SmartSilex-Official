<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Town;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Country extends Model 
{

	static $_table 		= 'data_country';

	Public $_fillable 	= array('name', 'status_id', 'created_at');

	public static function GetCountry()
	{
		$query 	= 	'SELECT id, name, status_id FROM data_country ORDER BY id ASC';
		$cit 	=	DBSmart::DBQueryAll($query);
		
		if($cit <> false)
		{
			foreach ($cit as $k => $val) 
			{
				$city[$k] = array(
					'id' 	 	=> 	$val['id'], 
					'name' 	 	=>	$val['name'],
					'status' 	=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'	=>	$val['status_id']
				);
			}
	        
	        return $city;

		}else{ return false; }
	}

	public static function GetCountryById($id)
	{
		$query 		= 	'SELECT id, name, status_id FROM data_country WHERE id = "'.$id.'" ORDER BY id DESC';
		$country 	=	DBSmart::DBQuery($query);

		if($country <> false)
		{
			return [
			 'id'           => $country['id'],
			 'name'         => $country['name'],
			 'status'       => $country['status_id']
			];

		}else{ return false; }
	}

	public static function GetCountryByStatus()
	{
		$query 		= 	'SELECT id, name, status_id FROM data_country WHERE status_id = "1" ORDER BY id DESC';
		$country 	=	DBSmart::DBQuery($query);

		if($country <> false)
		{
			return [
			 'id'           => $country['id'],
			 'name'         => $country['name'],
			 'status'       => $country['status_id']
			];

		}else{ return false; }
	}

	public static function GetCountryByName($info)
	{

		$query 		= 	'SELECT id, name, status_id FROM data_country WHERE name = "'.$info.'" ORDER BY id DESC';
		$country 	=	DBSmart::DBQuery($query);

		if($country <> false)
		{
			return array('id' => $cou['id'], 'name' => $cou['name'], 'status' => $cou['status_id']);
		}else{
			return false;
		}
	}

	public static function SaveCountry($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_c']));

		if($info['type_c'] == 'new')
		{
			$query 	= 	'INSERT INTO data_country(name, status_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_c'].'", "'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['type_c'] == 'edit') 
		{
			$query 	=	'UPDATE data_country SET name="'.$name.'", status_id="'.$info['status_id_c'].'" WHERE id = "'.$info['id_c'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}

}