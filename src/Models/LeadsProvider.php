<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\LeadsProvider;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadsProvider extends Model {

	static $_table 		= 'data_leads_provider';

	Public $_fillable 	= array('id', 'name', 'status_id', 'created_at');

	public static function GetLeadsProvider()
	{

		$query 	= 	'SELECT id, name, status_id, created_at FROM data_leads_provider ORDER BY id ASC';
		$sProv 	=	DBSmart::DBQueryAll($query);
		
		if($sProv <> false)
		{
			foreach ($sProv as $k => $val) 
			{
				$sProP[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'created_at'	=> $val['created_at']
				);
			}
	        
	        return $sProP;
		}else{ 	return false;	}
	}

	public static function GetLeadsProviderById($id)
	{
		$query 	= 	'SELECT id, name, status_id, created_at FROM data_leads_provider WHERE id = "'.$id.'"';

		$ord 	=	DBSmart::DBQuery($query);

		if($ord <> false)
		{
			return [
				'id' 	 		=> 	$ord['id'], 
				'name' 	 		=> 	$ord['name'],
				'status' 		=> 	Status::GetStatusById($ord['status_id'])['name'],
				'status_id'		=>	$ord['status_id'],
				'created_at'	=>	$ord['created_at']
			];      

		}else{ return false; }
	
	}
}

		