<?php
namespace App\Models;

use Model;

use App\Models\Cdr;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class Cdr extends Model 
{

   	static $_table 			= 'cdr';

	protected $_fillable 	= array('calldate','clid','src','dst','dcontext','channel','dstchannel','lastapp','lastdata','duration','billsec','disposition','amaflags','accountcode','uniqueid','userfield','did','	recordingfile','cnum','cnam','outbound_cnum','outbound_cnam','dst_cnam','linkedid','peeraccount','sequence','id');

	public static function SaveRecords($info)
	{

		$calls 	=	Cdr::create();

		$calls->calldate		=	 $info['calldate'];
		$calls->clid			=	 $info['clid'];
		$calls->src				=	 $info['src'];
		$calls->dst				=	 $info['dst'];
		$calls->dcontext		=	 $info['dcontext'];
		$calls->channel			=	 $info['channel'];
		$calls->dstchannel		=	 $info['dstchannel'];
		$calls->lastapp			=	 $info['lastapp'];
		$calls->lastdata		=	 $info['lastdata'];
		$calls->duration		=	 $info['duration'];
		$calls->billsec			=	 $info['billsec'];
		$calls->disposition		=	 $info['disposition'];
		$calls->amaflags		=	 $info['amaflags'];
		$calls->accountcode		=	 $info['accountcode'];
		$calls->uniqueid		=	 $info['uniqueid'];
		$calls->userfield		=	 $info['userfield'];
		$calls->did				=	 $info['did'];
		$calls->recordingfile	=	 $info['recordingfile'];
		$calls->cnum			=	 $info['cnum'];
		$calls->cnam			=	 $info['cnam'];
		$calls->outbound_cnum	=	 $info['outbound_cnum'];
		$calls->outbound_cnam	=	 $info['outbound_cnam'];
		$calls->dst_cnam		=	 $info['dst_cnam'];
		$calls->linkedid		=	 $info['linkedid'];
		$calls->peeraccount		=	 $info['peeraccount'];
		$calls->sequence		=	 $info['sequence'];
		
		return ($calls->save()) ? true : false;
	}

 }