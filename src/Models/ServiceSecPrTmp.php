<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Service;
use App\Models\Country;
use App\Models\ServiceSecPrTmp;

use App\Lib\Config;
use App\Lib\DBSmart;

class ServiceSecPrTmp extends Model 
{

	static $_table = 'cp_service_pr_sec_tmp';
	
	Public $_fillable = array('client_id', 'previously_id', 'equipment', 'cameras_id', 'comp_equipment', 'dvr_id', 'add_equipment', 'password_alarm', 'payment_id', 'ref_id', 'ref_list_id', 'sup_id', 'sup_ope_id', 'service_id', 'type_order_id', 'origen_id', 'contact_1', 'contact_1_desc', 'contact_2', 'contact_2_desc', 'contact_3', 'contact_3_desc', 'owen_id', 'assit_id', 'created_at');

	public static function SaveService($info)
	{	
		$_replace 	= 	new Config();
		$date 		=	date('Y-m-d H:i:s', time());

		if($info['sup'] == 0)
		{	$user1 = $info['operator']; $user2 = $info['operator']; }
		else
		{	$user1 = $info['sup_list']; $user2 = $info['operator']; }

		$equp 	= strtoupper($_replace->deleteTilde($info['equipment']));
		$comp 	= strtoupper($_replace->deleteTilde($info['comp_equip']));
		$add 	= strtoupper($_replace->deleteTilde($info['add_equip']));

		$cont1 	= strtoupper($_replace->deleteTilde($info['cont1desc']));
		$cont2 	= strtoupper($_replace->deleteTilde($info['cont2desc']));
		$cont3 	= strtoupper($_replace->deleteTilde($info['cont3desc']));

		$date = date('Y-m-d h:i:s', time());

		$query 	=	'INSERT INTO cp_service_pr_sec_tmp(client_id, previously_id, equipment, cameras_id, comp_equipment, dvr_id, add_equipment, password_alarm, payment_id, ref_id, ref_list_id, sup_id, sup_ope_id, service_id, type_order_id, origen_id, contact_1, contact_1_desc, contact_2, contact_2_desc, contact_3, contact_3_desc, owen_id, assit_id, created_at) VALUES ("'.$info['client'].'", "'.$info['previosly'].'", "'.$equp.'", "'.$info['cameras'].'", "'.$comp.'", "'.$info['dvr'].'", "'.$add.'", "'.$info['pass_alarm'].'", "'.$info['payment'].'", "'.$info['ref_id'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$info['service'].'", "'.$info['order_type'].'", "'.$info['origen'].'", "'.$info['cont1'].'", "'.$cont1.'", "'.$info['cont2'].'", "'.$cont2.'", "'.$info['cont3'].'", "'.$cont3.'", "'.$user1.'", "'.$user2.'", "'.$date.'") ';

		$lead 	=	DBSmart::DataExecute($query);

		return ($lead <> false ) ? true : false;
	}

	public static function ServiceSec($client)
	{

		$query 	=	'SELECT client_id, previously_id, equipment, cameras_id, comp_equipment, dvr_id, add_equipment, password_alarm, payment_id, ref_id, ref_list_id, sup_id, sup_ope_id, service_id, type_order_id, origen_id, contact_1, contact_1_desc, contact_2, contact_2_desc, contact_3, contact_3_desc, owen_id, created_at FROM cp_service_pr_sec_tmp WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		$servi 	=	DBSmart::DBQuery($query);

		if($servi <> false)
		{
			return [
				'status'		=>	true,
				'client' 		=>	($servi['client_id'] <> '') 		? $servi['client_id'] 		: $servi['client_id'],
				'previosly' 	=>	($servi['previously_id'] <> '') 	? $servi['previously_id'] 	: '0',
				'equipment' 	=>	($servi['equipment'] <> '') 		? $servi['equipment'] 		: '',
				'cameras' 		=>	($servi['cameras_id'] <> '') 		? $servi['cameras_id'] 		: '1',
				'comp_equip' 	=>	($servi['comp_equipment'] <> '') 	? $servi['comp_equipment'] 	: '',
				'dvr' 			=>	($servi['dvr_id'] <> '') 			? $servi['dvr_id'] 			: '1',
				'add_equip' 	=>	($servi['add_equipment'] <> '') 	? $servi['add_equipment'] 	: '',
				'pass_alarm' 	=>	($servi['password_alarm'] <> '') 	? $servi['password_alarm'] 	: '',
				'payment' 		=>	($servi['payment_id'] <> '') 		? $servi['payment_id'] 		: '1',
				'ref_id' 		=>	($servi['ref_id'] <> '') 			? $servi['ref_id']			: '0',
				'ref_list' 		=>	($servi['ref_list_id'] <> '') 		? $servi['ref_list_id']		: '0',
				'sup' 			=>	($servi['sup_id'] <> '') 			? $servi['sup_id']			: '0',
				'sup_list' 		=>	($servi['sup_ope_id'] <> '') 		? $servi['sup_ope_id']		: '0',
				'service' 		=>	($servi['service_id'] <> '') 		? $servi['service_id'] 		: '1',
				'order_type'	=>	($servi['type_order_id'] <> '') 	? $servi['type_order_id'] 	: '1',
				'origen'		=>	($servi['origen_id'] <> '') 		? $servi['origen_id'] 		: '1',
				'cont1' 		=>	($servi['contact_1'] <> '') 		? $servi['contact_1'] 		: '',
				'cont1desc' 	=>	($servi['contact_1_desc'] <> '') 	? $servi['contact_1_desc'] 	: '',
				'cont2' 		=>	($servi['contact_2'] <> '') 		? $servi['contact_2'] 		: '',
				'cont2desc' 	=>	($servi['contact_2_desc'] <> '') 	? $servi['contact_2_desc'] 	: '',
				'cont3' 		=>	($servi['contact_3'] <> '') 		? $servi['contact_3'] 		: '',
				'cont3desc' 	=>	($servi['contact_3_desc'] <> '') 	? $servi['contact_3_desc'] 	: '',
				'operator' 		=>	($servi['owen_id'] <> '') 			? $servi['owen_id'] 		: '',
				'created' 		=>	($servi['created_at'] <> '') 		? $servi['created_at'] 		: '',
			];

		}else{

			return [
				'status' =>	false, 'client' => $client, 'previosly' => "0", 'equipment' => "", 'cameras' => "1", 'comp_equip' => "", 'dvr' =>	"1", 'add_equip' => "", 'pass_alarm' => "", 'payment' =>	"1", 'ref_id' =>	"0", 'ref_list' =>	"0", 'sup' => "0", 'sup_list' => "1", 'service' =>	"1", 'order_type' => "1", 'origen' => "1", 'cont1' =>	"", 'cont1desc' =>	"", 'cont2' =>	"", 'cont2desc' =>	"", 'cont3' =>	"", 'cont3desc' =>	"", 'operator' =>	""
			];
		}
	
	}


}