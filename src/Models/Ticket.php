<?php
namespace App\Models;

use Model;
use Carbon\Carbon;

use App\Models\Ticket;
use App\Models\TicketService;
use App\Models\TicketDepartament;
use App\Models\Status;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class Ticket extends Model
{

	static $_table 		= 'cp_tickets';

	Public $_fillable 	= array('ticket', 'ticket_dep', 'dep_id', 'serv_id', 'code', 'information', 'status_id', 'description', 'operator_id', 'created_at', 'finish_by',  'finish_at');

///////////////////////////////////////////////////////////////
	
	public static function GetTicket()
   	{

		$query	=	'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by,  finish_at FROM cp_tickets ORDER BY id DESC';
		$tic 	=	DBSmart::DBQuery($query);
   		if($tic <> false)
   		{
   			$tick = (substr($tic['ticket'], 3, strlen($tic['ticket'])) + 1);   	
   			return "SER".$tick;
   		}else{
   			return "SER10001";
   		}
   	
   	}

///////////////////////////////////////////////////////////////
	
	public static function GetTicketById($id)
   	{
   		$query	=	'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by,  finish_at FROM cp_tickets WHERE id = "'.$id.'" ORDER BY id DESC';
		$tic 	=	DBSmart::DBQuery($query);

   		if($tic <> false)
   		{
   			return 	[
   				'id'			=>	$tic['id'],
   				'ticket'		=>	$tic['ticket'],
   				'ticket_dep'	=>	$tic['ticket_dep'],
   				'dep_id'		=>	$tic['dep_id'],
   				'serv_id'		=>	$tic['serv_id'],
   				'code'			=>	($tic['code'] <> '') 		? $tic['code'] : '',
   				'information'	=>	($tic['information'] <> '') ? $tic['information'] : '',
   				'description'	=>	($tic['description'] <> '') ? $tic['description'] : '',
   				'operator_id'	=>	$tic['operator_id'],
   				'operator'		=>	($tic['operator_id'] <> '' )? User::GetUserById($tic['operator_id'])['username'] : '',
   				'created'		=>	$tic['created_at'],
   				'finish'		=>	($tic['finish_by'] <> '' ) 	? User::GetUserById($tic['finish_by'])['username'] : '',
   				'finish_at'		=>	($tic['finish_at'] <> '') 	? $tic['finish_at'] : '',

   			];

   		}else{	return false;	}
   	
   	}

   public static function GetID($id)
   {
      $query   =  'SELECT id, ticket, ticket_dep, dep_id, serv_id, information, status_id, description, operator_id, created_at, finish_by,  finish_at FROM cp_tickets WHERE id = "'.$id.'" ORDER BY id DESC';
      $tic  =  DBSmart::DBQuery($query);

      if($tic <> false)
      {
         return   [
            'id'           => $tic['id'],
            'ticket'       => $tic['ticket'],
            'ticket_dep'   => $tic['ticket_dep'],
            'dep_id'       => $tic['dep_id'],
            'serv_id'      => $tic['serv_id'],
            'information'  => ($tic['information'] <> '')   ? $tic['information'] : '',
            'description'  => ($tic['description'] <> '')   ? $tic['description'] : '',
            'operator_id'  => $tic['operator_id'],
            'operator'     => ($tic['operator_id'] <> '' )  ? User::GetUsersID($tic['operator_id'])['username'] : '',
            'created'      => $tic['created_at'],
            'finish'       => ($tic['finish_by'] <> '' )    ? User::GetUsersID($tic['finish_by'])['username'] : '',
            'finish_at'    => ($tic['finish_at'] <> '')     ? $tic['finish_at'] : '',

         ];

      }else{   return false;  }
   
   }

///////////////////////////////////////////////////////////////

	public static function SaveTicket($info)
	{
		$_replace  			= 	new Config();
		$date 	= 	date('Y-m-d H:i:s', time());
		$code 	=	($info['pues']) ? strtoupper($info['pues']) : '';
		$infoT 	=	($info['desc']) ? strtoupper($_replace->deleteTilde($info['desc'])) : '';

		$query 	=	'INSERT INTO cp_tickets(ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by, finish_at) VALUES ("'.$info['ticket'].'", "'.$info['t_dep'].'", "'.$info['dep'].'", "'.$info['serv'].'", "'.$code.'","'.$infoT.'", "1", "", "'.$info['ope'].'", NOW(), "1", "'.$date.'")';
		
		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;

	}

///////////////////////////////////////////////////////////////
	
	public static function SaveDA($info)
	{
		$query 	=	'INSERT INTO cp_ticket_admin (id, ticket, status_id, operator_id, operator_at, deadline, asigned_by, asigned_at) VALUES("'.$info['id'].'", "'.$info['ticket'].'", "'.$info['status_id'].'", "'.$info['operator_id'].'", "'.$info['operator_at'].'", "'.$info['deadline'].'", "'.$info['asigned_by'].'", "'.$info['asigned_at'].'")';
		$res 	=	DBSmart::DataExecute($query);
		return ($res <> false) ? true : false;
	
	}

   public static function SaveGen($info, $table)
   {

      $query   =  'INSERT INTO '.$table.' (id, ticket, name, cedula, phone, alternative, email, type_id, comment, status_id, operator_id, operator_at, deadline, asigned_by, asigned_at) VALUES ("'.$info['id'].'", "'.$info['ticket'].'", "'.$info['name'].'", "'.$info['cedula'].'", "'.$info['phone'].'", "'.$info['alternative'].'", "'.$info['email'].'", "'.$info['type'].'", "'.$info['comment'].'", "'.$info['status_id'].'", "'.$info['operator_id'].'", "'.$info['operator_at'].'", "'.$info['deadline'].'", "'.$info['asigned_by'].'", NOW())';
      $res  =  DBSmart::DataExecute($query);

      return ($res <> false) ? true : false;
   
   }

///////////////////////////////////////////////////////////////
	
	public static function CloseTicket($info)
	{
		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();
		$descrip 	=	($info['descript']) ? strtoupper($_replace->deleteTilde($info['descript'])) : '';

		$query 		=	'UPDATE cp_tickets SET status_id = "2", description="'.$descrip.'", finish_by = "'.$info['finish'].'", finish_at = "'.$date.'" WHERE id = "'.$info['id'].'"';
		
		$dep 		=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;

	}

///////////////////////////////////////////////////////////////

	public static function TicketHtml($info)
	{
        return 	'<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
						<div class="panel panel-darken">
							<div class="panel-heading" style="text-align: center;">
								<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-ticket" style="font-size: 50px;"></i>
								<h3 class="panel-title" style="margin-top: 10px;">'.$info['departament'].'</h3>
							</div>
							<div class="panel-footer text-align-center"><h1>'.$info['ticket'].'</h1></div>
						</div>
					</div>
				</div>';
	
	}

///////////////////////////////////////////////////////////////

	public static function GetTicketHtml($info)
	{
		$html = "";

		$html.='<table class="head-wrap" style="background: #ecf8ff">';							
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>OPERADOR</strong></td>';
		$html.='<td>'.strtoupper($info['username']).'</td>';
		$html.='<td><strong>DEPARTAMENTO</strong></td>';
		$html.='<td>'.strtoupper($info['departament']).'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>SERVICIO</strong></td>';
		$html.='<td>'.strtoupper($info['service']).'</td>';
		$html.='<td><strong>CODIGO</strong></td>';
		$html.='<td>'.strtoupper($info['pues']).'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>DESCRIPCION</strong></td>';
		$html.='<td colspan="3">'.strtoupper($info['desc']).'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';

		return $html;
	
	}

///////////////////////////////////////////////////////////////

	public static function GetTicketRespHtml($info)
	{
		$html = "";

		$html.='<table class="head-wrap" style="background: #ecf8ff">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td><strong>RESPUESTA</strong></td>';
		$html.='<td colspan="3">'.$info['resp'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td><strong>REALIZADO</strong></td>';
		$html.='<td>'.$info['operador'].'</td>';
		$html.='<td><strong>FINALIZADO</strong></td>';
		$html.='<td>'.$info['f_username'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='<br/>';
		$html.='<br/>';

		return $html;
	
	}

///////////////////////////////////////////////////////////////

	public static function Load($id)
	{
		$date 		= 	date("Y-m-d");

		$type 		=	['1', '2'];

		$row 	=	$row2 	=	'';

		foreach ($type as $k => $val) 
		{
			if($val == 1)
			{
				if($id == "28")
				{
					$query 	= 	'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by,  finish_at, finish_by, finish_at FROM cp_tickets WHERE status_id = "'.$val.'" AND (dep_id = "'.$id.'" OR dep_id = "1") ORDER BY created_at DESC';
				}else{
					$query 	= 	'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by,  finish_at, finish_by, finish_at FROM cp_tickets WHERE status_id = "'.$val.'" AND dep_id = "'.$id.'" ORDER BY created_at DESC';
				}

				$info 	=	DBSmart::DBQueryAll($query);
 
 				if($info <> false)
				{ 
					foreach ($info as $ke => $value) 
					{

						$row.=''.Ticket::RowHtmlPending($value).'';
					}

				}else{

					$row.=''.Ticket::RowHtmlPending('').'';
				}
			}else{
				
				$query = 'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by,  finish_at FROM cp_tickets WHERE status_id = "'.$val.'" AND dep_id = "'.$id.'" AND finish_at LIKE "'.$date.'%" ORDER BY finish_at DESC';
				$info 	=	DBSmart::DBQueryAll($query);
				

				if($info <> false)
				{ 
					foreach ($info as $ke => $value) 
					{

						$row2.=''.Ticket::RowHtmlProcessed($value).'';
					}

				}else{
					$row2.=''.Ticket::RowHtmlProcessed('').'';
				}
			}
		}

		return [
			'pending'		=>	Ticket::TableHtmlPending($row),
			'processed'		=>	Ticket::TableHtmlProcessed($row2)	
		];
	
	}

///////////////////////////////////////////////////////////////

	public static function LoadByDate($id, $info, $date)
	{
		$type 	=	['1', '2'];

		$row 	=	$row2 	=	'';

		if($info == "0")
		{
			foreach ($type as $k => $val) 
			{

				if($val == 1)
				{
					$query 	= 	'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by,  finish_at, finish_by, finish_at FROM cp_tickets WHERE status_id = "'.$val.'" AND dep_id = "'.$id.'" AND created_at BETWEEN "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59" ORDER BY created_at DESC';


					$info 	=	DBSmart::DBQueryAll($query);
	 
	 				if($info <> false)
					{ 
						foreach ($info as $ke => $value) 
						{

							$row.=''.Ticket::RowHtmlPending($value).'';
						}

					}else{

						$row.=''.Ticket::RowHtmlPending('').'';
					}
				
				}else{
					
					$query = 'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by, finish_at FROM cp_tickets WHERE status_id = "'.$val.'" AND dep_id = "'.$id.'" AND created_at BETWEEN "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59" ORDER BY finish_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);
					

					if($info <> false)
					{ 
						foreach ($info as $ke => $value) 
						{

							$row2.=''.Ticket::RowHtmlProcessed($value).'';
						}

					}else{
						$row2.=''.Ticket::RowHtmlProcessed('').'';
					}
				
				}
			}

		}else{

			foreach ($type as $k => $val) 
			{
				if($val == 1)
				{
					$query 	= 	'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by, finish_at, finish_by, finish_at FROM cp_tickets WHERE status_id = "'.$val.'" AND dep_id = "'.$id.'" AND created_at BETWEEN "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59" ORDER BY created_at DESC';

					$info 	=	DBSmart::DBQueryAll($query);
	 
	 				if($info <> false)
					{ 
						foreach ($info as $ke => $value) 
						{

							$row.=''.Ticket::RowHtmlPending($value).'';
						}

					}else{

						$row.=''.Ticket::RowHtmlPending('').'';
					}
				
				}else{
					
					$query = 'SELECT id, ticket, ticket_dep, dep_id, serv_id, code, information, status_id, description, operator_id, created_at, finish_by, finish_at FROM cp_tickets WHERE status_id = "'.$val.'" AND dep_id = "'.$id.'" AND created_at BETWEEN "'.$date['dateIni'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59" ORDER BY finish_at DESC';
					
					$info 	=	DBSmart::DBQueryAll($query);
					

					if($info <> false)
					{ 
						foreach ($info as $ke => $value) 
						{

							$row2.=''.Ticket::RowHtmlProcessed($value).'';
						}

					}else{
						$row2.=''.Ticket::RowHtmlProcessed('').'';
					}
				
				}
			}
		}

		return [
			'pending'		=>	Ticket::TableHtmlPending($row),
			'processed'		=>	Ticket::TableHtmlProcessed($row2)	
		];

	}

///////////////////////////////////////////////////////////////

	public static function RowHtmlPending($info)
	{

		$html = '';

		if($info <> '')
		{

			$html.='<tr>';
			$html.='<td>'.$info['ticket'].'</td>';
			$html.='<td>'.User::GetUserById($info['operator_id'])['username'].'</td>';
			$html.='<td>'.TicketService::GetServiceById($info['serv_id'])['name'].'</td>';
			$html.='<td>'.$info['code'].'</td>';
			$html.='<td>'.$info['created_at'].'</td>';
			$html.='<td><span class="label label-danger">PENDIENTE</span></td>';
			$html.='<td>
						<ul class="demo-btns text-center">
		                  <li>
		                  	<a onclick="ViewTicketInfo('.(int)$info['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Client"><i class="fa fa-eye"></i></a>
		                  </li>
		                  <li>
		                    <a onclick="ChangeTicketType('.(int)$info['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Qualification"><i class="fa fa-check"></i></a>
		                  </li>
	                	</ul>
	                </td>';
			$html.='</tr>';

		}else{
			$html.='<tr>';
			$html.='<td colspan="7" style="text-align: center;"> <h1>SIN TICKET REGISTRADOS</h1> </td>';
			$html.='</tr>';
		}

		return $html;
	
	}

///////////////////////////////////////////////////////////////

	public static function RowHtmlProcessed($info)
	{

		$html = '';

		if($info <> '')
		{

			$html.='<tr>';
			$html.='<td>'.$info['ticket'].'</td>';
			$html.='<td>'.User::GetUserById($info['operator_id'])['username'].'</td>';
			$html.='<td>'.TicketService::GetServiceById($info['serv_id'])['name'].'</td>';
			$html.='<td>'.$info['code'].'</td>';
			$html.='<td>'.$info['created_at'].'</td>';
			$html.='<td><span class="label label-success">PROCESADO</span></td>';
			$html.='<td>'.User::GetUserById($info['finish_by'])['username'].'</td>';			
			$html.='<td>'.$info['finish_at'].'</td>';
			$html.='<td>
						<ul class="demo-btns text-center">
		                  <li>
		                  	<a onclick="ViewTicketProcess('.(int)$info['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Client"><i class="fa fa-eye"></i></a>
		                  </li>
	                	</ul>
	                </td>';
			$html.='</tr>';

		}else{
			$html.='<tr>';
			$html.='<td colspan="9" style="text-align: center;"> <h1>SIN TICKET REGISTRADOS</h1> </td>';
			$html.='</tr>';
		}

		return $html;
	
	}

///////////////////////////////////////////////////////////////

	public static function ViewProcess($info)
	{

		$html = '';
		$html.='<div class="row">';
		$html.='<div class="well">';
		$html.='<h3>Solicitud</h3>';
		$html.='<p>';
		$html.=''.$info['information'].'';
		$html.='</p>';
		$html.='<br>';
		$html.='<h3>Respuesta</h3>';
		$html.='<p>';
		$html.=''.$info['description'].'';
		$html.='</p>';
		$html.='<br>';
		$html.='<table class="table table-bordered" style="text-align: center;">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td>Ejecutado Por:</td>';
		$html.='<td>'.$info['finish'].'</td>';
		$html.='<td>Finalizado el</td>';
		$html.='<td>'.$info['finish_at'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';
		return $html;
	
	}

///////////////////////////////// Table HTML Pending ////////////////////////////////

	public static function TableHtmlPending($info)
	{
		$html = '';
		$html.='<table id="TicketPendingSystem" class="table table-bordered" style="text-align: center;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th style="text-align: center;">Ticket</th>';
		$html.='<th style="text-align: center;">Operador</th>';
		$html.='<th style="text-align: center;">Servicio</th>';
		$html.='<th style="text-align: center;">Puesto</th>';
		$html.='<th style="text-align: center;">Fecha Solicitud</th>';
		$html.='<th style="text-align: center;">Status</th>';
		$html.='<th style="text-align: center;">Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		$html.=''.$info.'';

		$html.='</tbody>';
		$html.='</table>';
		return $html;

	}

///////////////////////////////// Table HTML Processed ///////////////////////////////

	public static function TableHtmlProcessed($info)
	{

		$html = '';
		$html.='<table id="TicketProcessSystem" class="table table-bordered" style="text-align: center;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th style="text-align: center;">Ticket</th>';
		$html.='<th style="text-align: center;">Operador</th>';
		$html.='<th style="text-align: center;">Servicio</th>';
		$html.='<th style="text-align: center;">Puesto</th>';
		$html.='<th style="text-align: center;">Fecha Solicitud</th>';
		$html.='<th style="text-align: center;">Status</th>';
		$html.='<th style="text-align: center;">Realizado Por</th>';
		$html.='<th style="text-align: center;">Finalizado</th>';
		$html.='<th style="text-align: center;">Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		$html.=''.$info.'';

		$html.='</tbody>';
		$html.='</table>';
		return $html;

	}

///////////////////////////////////////////////////////////////
	
	public static function AdminsLoad()
   	{
		$_replace 	= 	new Config();

		$week 	=	$_replace->CurrentMonthDays();

		$betw 	=	't2.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
		$bwee 	=	'operator_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t1.asigned_by AS eje, t1.asigned_at, t1.deadline FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t1.status_id = "1" OR t1.status_id = "4" OR t1.status_id = "5") AND '.$betw.'';

		$Pending 	=	DBSmart::DBQueryAll($query);

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t1.status_id = "2" OR t1.status_id = "3") AND '.$betw.'';
		$Process 	=	DBSmart::DBQueryAll($query);

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND t1.status_id = "6" AND '.$betw.'';
		$Cancel 	=	DBSmart::DBQueryAll($query);

        $query     	=   'SELECT id, username FROM users WHERE status_id = "1" AND departament_id = "4" ORDER BY id ASC';
        $users     	=   DBSmart::DBQueryAll($query);

        foreach ($users as $k => $val) 
        {
            $query      =    'SELECT 
            (SELECT COUNT(*) FROM cp_ticket_admin WHERE asigned_by = "'.$val['id'].'" AND '.$bwee.') AS T, 
            (SELECT COUNT(*) FROM cp_ticket_admin WHERE asigned_by = "'.$val['id'].'" AND status_id <> "4" AND '.$bwee.') AS Tc, 
            (SELECT ROUND((Tc/T) * 100 )) AS Tp';

            $ticket     =   DBSmart::DBQuery($query);

            $statics[$k]    =   [
                'username'  =>  $val['username'],
                'total'     =>  $ticket['T'],
                'process'   =>  $ticket['Tc'],
                'porcent'   =>  ($ticket['Tp'] == null) ? "0" : $ticket['Tp']
            ];
        }

        $query 	=	'SELECT 
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE '.$bwee.') AS T,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE (status_id = "1" OR status_id = "4" OR status_id = "5")  AND '.$bwee.') AS pend,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "2"  AND '.$bwee.') AS napro,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "3"  AND '.$bwee.') AS apro,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "6"  AND '.$bwee.') AS canc,
		(SELECT ROUND((pend/T) *100)) AS PPen,
		(SELECT ROUND((napro/T) *100)) AS PNApro,
		(SELECT ROUND((apro/T) *100)) AS PApro,
		(SELECT ROUND((canc/T) *100)) AS PCan';

        $sta 	=	DBSmart::DBQuery($query);

   		if($Pending <> false)
   		{
   			foreach ($Pending as $t => $tic) 
   			{
				$now 	=	Carbon::now()->toDateTimeString();
				if($tic['deadline'] == '0000-00-00 00:00:00')
				{
					$diff = '';
				}else{
					$diff = round((strtotime($tic['deadline']) - strtotime($now))/(60*60));
				}
   				if ( ($diff < 2) AND ($diff > 0) ) 
   				{
   					$deadline 	=	"POR VENCER";

   				}
   				elseif($diff < 0 )
   				{
   					$deadline 	=	"VENCIDO";

   				}
   				elseif($diff >= 2 )
   				{
   					$deadline 	=	"A TIEMPO";

   				}
   				elseif($diff == '')
   				{
   					$deadline 	=	"";
   				}

   				switch ($tic['status_id']) 
   				{
   					case '':
   						$status = "PENDIENTE";
   						break;
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$pending[$t] 	=	[
   					'sta' 			=>  true,
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'dateasig'		=>	$tic['asigned_at'],
   					'deadline'		=>	$deadline,
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{
			$pending 	=	false;

   		}
   	
   	   	if($Process <> false)
   		{
   			foreach ($Process as $t => $tic) 
   			{
   				switch ($tic['status_id']) 
   				{
   					case '':
   						$status = "PENDIENTE";
   						break;
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$process[$t] 	=	[
   					'sta' 			=>  true,
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'dateasig'		=>	$tic['finish_at'],
   					'deadline'		=>	'',
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{
			$process 	=	false;

   		}

   	   	if($Cancel <> false)
   		{
   			foreach ($Cancel as $t => $tic) 
   			{
				
   				switch ($tic['status_id']) 
   				{
   					case '':
   						$status = "PENDIENTE";
   						break;
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$cancel[$t] 	=	[
   					'sta' 			=>  true,
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'dateasig'		=>	$tic['finish_at'],
   					'deadline'		=>	'',
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{
			$cancel 	=	false;

   		}

   		return [
   			'pending'		=>	$pending,
   			'cancel'		=>	$cancel,
   			'process'		=>	$process,
   			'statics'		=>	$statics,
   			'GenStatics'	=>	$sta			
   		];

   	}

	public static function AdminsLoadDate($info, $dates)
   	{
		$_replace 	= 	new Config();

		$week 	=	$_replace->CurrentMonthDays();

		if($info == "0")
		{
			$betw 	=	't2.created_at LIKE "'.$dates['dateEnd'].'%"';
			$bwee 	=	'operator_at LIKE "'.$dates['dateEnd'].'%"';
		}else{
			$betw 	=	't2.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59"';
			$bwee 	=	'operator_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59"';
		}

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t1.asigned_by AS eje, t1.asigned_at, t1.deadline FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t1.status_id = "1" OR t1.status_id = "4" OR t1.status_id = "5") AND '.$betw.'';

		$Pending 	=	DBSmart::DBQueryAll($query);

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t1.status_id = "2" OR t1.status_id = "3") AND '.$betw.'';
		$Process 	=	DBSmart::DBQueryAll($query);

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND t1.status_id = "6" AND '.$betw.'';
		$Cancel 	=	DBSmart::DBQueryAll($query);

        $query     	=   'SELECT id, username FROM users WHERE status_id = "1" AND departament_id = "4" ORDER BY id ASC';
        $users     	=   DBSmart::DBQueryAll($query);

        foreach ($users as $k => $val) 
        {
            $query      =    'SELECT 
            (SELECT COUNT(*) FROM cp_ticket_admin WHERE asigned_by = "'.$val['id'].'" AND '.$bwee.') AS T, 
            (SELECT COUNT(*) FROM cp_ticket_admin WHERE asigned_by = "'.$val['id'].'" AND status_id <> "4" AND '.$bwee.') AS Tc, 
            (SELECT ROUND((Tc/T) * 100 )) AS Tp';

            $ticket     =   DBSmart::DBQuery($query);

            $statics[$k]    =   [
                'username'  =>  $val['username'],
                'total'     =>  $ticket['T'],
                'process'   =>  $ticket['Tc'],
                'porcent'   =>  ($ticket['Tp'] == null) ? "0" : $ticket['Tp']
            ];
        }

        $query 	=	'SELECT 
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE '.$bwee.') AS T,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE (status_id = "1" OR status_id = "4" OR status_id = "5")  AND '.$bwee.') AS pend,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "2"  AND '.$bwee.') AS napro,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "3"  AND '.$bwee.') AS apro,
		(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "6"  AND '.$bwee.') AS canc,
		(SELECT ROUND((pend/T) *100)) AS PPen,
		(SELECT ROUND((napro/T) *100)) AS PNApro,
		(SELECT ROUND((apro/T) *100)) AS PApro,
		(SELECT ROUND((canc/T) *100)) AS PCan';

        $sta 	=	DBSmart::DBQuery($query);

   		if($Pending <> false)
   		{
   			foreach ($Pending as $t => $tic) 
   			{
				$now 	=	Carbon::now()->toDateTimeString();
				if($tic['deadline'] == '0000-00-00 00:00:00')
				{
					$diff = '';
				}else{
					$diff = round((strtotime($tic['deadline']) - strtotime($now))/(60*60));
				}
   				if ( ($diff < 2) AND ($diff > 0) ) 
   				{
   					$deadline 	=	"POR VENCER";

   				}
   				elseif($diff < 0 )
   				{
   					$deadline 	=	"VENCIDO";

   				}
   				elseif($diff >= 2 )
   				{
   					$deadline 	=	"A TIEMPO";

   				}
   				elseif($diff == '')
   				{
   					$deadline 	=	"";
   				}

   				switch ($tic['status_id']) 
   				{
   					case '':
   						$status = "PENDIENTE";
   						break;
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$pending[$t] 	=	[
   					'sta' 			=>  true,
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'dateasig'		=>	$tic['asigned_at'],
   					'deadline'		=>	$deadline,
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{
			$pending 	=	false;

   		}
   	
   	   	if($Process <> false)
   		{
   			foreach ($Process as $t => $tic) 
   			{
   				switch ($tic['status_id']) 
   				{
   					case '':
   						$status = "PENDIENTE";
   						break;
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$process[$t] 	=	[
   					'sta' 			=>  true,
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'dateasig'		=>	$tic['finish_at'],
   					'deadline'		=>	'',
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{
			$process 	=	false;

   		}

   	   	if($Cancel <> false)
   		{
   			foreach ($Cancel as $t => $tic) 
   			{
				
   				switch ($tic['status_id']) 
   				{
   					case '':
   						$status = "PENDIENTE";
   						break;
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$cancel[$t] 	=	[
   					'sta' 			=>  true,
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'dateasig'		=>	$tic['finish_at'],
   					'deadline'		=>	'',
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{
			$cancel 	=	false;

   		}

   		return [
   			'pending'		=>	$pending,
   			'cancel'		=>	$cancel,
   			'process'		=>	$process,
   			'statics'		=>	$statics,
   			'GenStatics'	=>	$sta			
   		];

   	}

   	public static function AdminLoadPeningHTML($iData)
   	{

   		$html = "";
		$html.='<table id="TablePending" class="table table-bordered" style="text-align: center;">';
			$html.='<thead>';
				$html.='<tr>';
					$html.='<th style="text-align: center;">Ticket</th>';
					$html.='<th style="text-align: center;">Operador</th>';
					$html.='<th style="text-align: center;">Servicio</th>';
					$html.='<th style="text-align: center;">Fecha Solicitud</th>';
					$html.='<th style="text-align: center;">Status</th>';
					$html.='<th style="text-align: center;">Ejecutivo</th>';
					$html.='<th style="text-align: center;">Fecha Asignaci&oacute;n</th>';
					$html.='<th style="text-align: center;">Deadline</th>';
					$html.='<th style="text-align: center;">Acci&oacute;n</th>';
				$html.='</tr>';
			$html.='</thead>';
		if($iData <> false)
		{
			$html.='<tbody>';
				foreach ($iData as $i => $iD) 
				{

					$now 	=	Carbon::now()->toDateTimeString();
					if($iD['deadline'] == '0000-00-00 00:00:00')
					{
						$diff = '';
					}else{
						$diff = round((strtotime($iD['deadline']) - strtotime($now))/(60*60));
					}
	   				if ( ($diff < 2) AND ($diff > 0) ) 
	   				{
	   					$deadline 	=	"POR VENCER";

	   				}
	   				elseif($diff < 0 )
	   				{
	   					$deadline 	=	"VENCIDO";

	   				}
	   				elseif($diff >= 2 )
	   				{
	   					$deadline 	=	"A TIEMPO";

	   				}
	   				elseif($diff == '')
	   				{
	   					$deadline 	=	"";
	   				}

					if($iD['status'] == "PENDIENTE")
			   		{ $status 	=	'<span class="label label-info">'.$iD['status'].'</span>'; }
			   		elseif($iD['status'] == "NO PROCEDE")
			   		{ $status 	=	'<span class="label label-danger">'.$iD['status'].'</span>'; }
			   		elseif($iD['status'] == "PROCESADO")
			   		{ $status 	=	'<span class="label label-success">'.$iD['status'].'</span>'; }
			   		elseif($iD['status'] == "ASIGNADO")
			   		{ $status 	=	'<span class="label label-warning">'.$iD['status'].'</span>';}
					elseif($iD['status'] == "REASIGNADO")
			   		{ $status 	=	'<span class="label label-warning">'.$iD['status'].'</span>'; }
					elseif($iD['status'] == "CANCELADO")
			   		{ $status 	=	'<span class="label label-danger">'.$iD['status'].'</span>'; }

					$html.='<tr>';
					$html.='<td>'.$iD['ticket'].'</td>';
					$html.='<td>'.$iD['operator'].'</td>';
					$html.='<td>'.$iD['service'].'</td>';
					$html.='<td>'.$iD['created'].'</td>';
					$html.='<td>'.$status.'</td>';
					$html.='<td>'.$iD['ejecutivo'].'</td>';
					$html.='<td>'.$iD['dateasig'].'</td>';
					$html.='<td>'.$deadline.'</td>';
					$html.='<td><table><tbody>';
						$html.='<tr><a onclick="InfoFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-info-circle"></i></a></tr>';
						$html.='<tr><a onclick="ProcessFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-spinner"></i></a></tr>';
					$html.='</tbody></table></td>';
					$html.='</tr>';
				}

			$html.='</tbody>';

		}else{
			$html.='<tbody>';
			$html.='</tbody>';			
		}

		$html.='</table>';

		return $html;
   	
   	}

   	public static function AdminLoadProcessHTML($iData, $table)
   	{
   	
   		$html = "";

		$html.='<table id="'.$table.'" class="table table-bordered" style="text-align: center;">';
			$html.='<thead>';
				$html.='<tr>';
					$html.='<th style="text-align: center;">Ticket</th>';
					$html.='<th style="text-align: center;">Operador</th>';
					$html.='<th style="text-align: center;">Servicio</th>';
					$html.='<th style="text-align: center;">Fecha Solicitud</th>';
					$html.='<th style="text-align: center;">Status</th>';
					$html.='<th style="text-align: center;">Ejecutivo</th>';
					$html.='<th style="text-align: center;">Fecha Proceso</th>';
					$html.='<th style="text-align: center;">Acci&oacute;n</th>';
				$html.='</tr>';
			$html.='</thead>';
			if($iData <> false)
			{
				$html.='<tbody>';
					foreach ($iData as $i => $iD) 
					{
						if($iD['status'] == "PENDIENTE")
				   		{ $status 	=	'<span class="label label-info">'.$iD['status'].'</span>'; }
				   		elseif($iD['status'] == "NO PROCEDE")
				   		{ $status 	=	'<span class="label label-danger">'.$iD['status'].'</span>'; }
				   		elseif($iD['status'] == "PROCESADO")
				   		{ $status 	=	'<span class="label label-success">'.$iD['status'].'</span>'; }
				   		elseif($iD['status'] == "ASIGNADO")
				   		{ $status 	=	'<span class="label label-warning">'.$iD['status'].'</span>';}
						elseif($iD['status'] == "REASIGNADO")
				   		{ $status 	=	'<span class="label label-warning">'.$iD['status'].'</span>'; }
						elseif($iD['status'] == "CANCELADO")
				   		{ $status 	=	'<span class="label label-danger">'.$iD['status'].'</span>'; }

						$html.='<tr>';
						$html.='<td>'.$iD['ticket'].'</td>';
						$html.='<td>'.$iD['operator'].'</td>';
						$html.='<td>'.$iD['service'].'</td>';
						$html.='<td>'.$iD['created'].'</td>';
						$html.='<td>'.$status.'</td>';
						$html.='<td>'.$iD['ejecutivo'].'</td>';
						$html.='<td>'.$iD['dateasig'].'</td>';
						$html.='<td><table><tbody>';
							$html.='<tr><a onclick="InfoFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-info-circle"></i></a></tr>';
						$html.='</tbody></table></td>';
						$html.='</tr>';
					}

				$html.='</tbody>';

			}else{
				$html.='<tbody>';
				$html.='</tbody>';			
			}

		return $html;
   	
   	}

	public static function AdminLoadHTML($iData, $table)
	{
	  $html = "";

	  $html.='<table id="'.$table.'" class="table table-bordered" style="text-align: center;">';
	     $html.='<thead>';
	        $html.='<tr>';
	           $html.='<th style="text-align: center;">Ticket</th>';
	           $html.='<th style="text-align: center;">Operador</th>';
	           $html.='<th style="text-align: center;">Tipo</th>';
	           $html.='<th style="text-align: center;">Cedula</th>';
	           $html.='<th style="text-align: center;">Servicio</th>';
	           $html.='<th style="text-align: center;">Fecha Solicitud</th>';
	           $html.='<th style="text-align: center;">Status</th>';
	           $html.='<th style="text-align: center;">Ejecutivo</th>';
	           $html.='<th style="text-align: center;">Fecha Proceso</th>';
	           $html.='<th style="text-align: center;">Acci&oacute;n</th>';
	        $html.='</tr>';
	     $html.='</thead>';
	     if($iData <> false)
	     {
	        $html.='<tbody>';

	           foreach ($iData as $i => $iD) 
	           {
	           		$type = '';

					if($iD['status'] == "PENDIENTE")
					{ $status   =  '<span class="label label-info">'.$iD['status'].'</span>'; }
					elseif($iD['status'] == "NO PROCEDE")
					{ $status   =  '<span class="label label-danger">'.$iD['status'].'</span>'; }
					elseif($iD['status'] == "PROCESADO")
					{ $status   =  '<span class="label label-success">'.$iD['status'].'</span>'; }
					elseif($iD['status'] == "ASIGNADO")
					{ $status   =  '<span class="label label-warning">'.$iD['status'].'</span>';}
					elseif($iD['status'] == "REASIGNADO")
					{ $status   =  '<span class="label label-warning">'.$iD['status'].'</span>'; }
					elseif($iD['status'] == "CANCELADO")
					{ $status   =  '<span class="label label-danger">'.$iD['status'].'</span>'; }
					if($iD['operator'] == "CLIENT.WEB")
					{ $type   =  '<span class="label label-danger">PAGINA WEB</span>'; }

					$html.='<tr>';
					$html.='<td>'.$iD['ticket'].'</td>';
					$html.='<td>'.$iD['operator'].'</td>';
					$html.='<td>'.$type.'</td>';
					$html.='<td>'.$iD['cedula'].'</td>';
					$html.='<td>'.$iD['service'].'</td>';
					$html.='<td>'.$iD['created'].'</td>';
					$html.='<td>'.$status.'</td>';
					$html.='<td>'.$iD['ejecutivo'].'</td>';
					$html.='<td>'.$iD['dateasig'].'</td>';
					$html.='<td><table><tbody>';
					$html.='<tr><a onclick="InfoFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-info-circle"></i></a></tr>';
					$html.='</tbody></table></td>';
					$html.='</tr>';
	           }

	        $html.='</tbody>';

	     }else{
	        $html.='<tbody>';
	        $html.='</tbody>';         
	     }

	  return $html;

	}

   	public static function AdminsInfo($id)
   	{
   		$data 	=	$note 	=	$log 	=	"";

   		$query	=	'SELECT * FROM cp_tickets WHERE id = "'.$id.'"';
   		$data 	=	DBSmart::DBQuery($query);
   		
   		if($data <> false)
   		{
	   		$query	=	'SELECT * FROM cp_ticket_notes WHERE ticket = "'.$data['ticket'].'" ORDER BY id DESC';
	   		$note 	=	DBSmart::DBQueryAll($query);

	   		$query	=	'SELECT * FROM cp_ticket_log WHERE ticket = "'.$data['ticket'].'" ORDER BY id DESC';
	   		$log 	=	DBSmart::DBQueryAll($query);
   		}

   		return 	[
   			'data'	=>	($data <> false) ? $data : false,
   			'notes'	=>	($note <> false) ? $note : false,
   			'logs'	=>	($log <> false)  ? $log  : false,
   		];
   	
   	}

	public static function GeneralInfo($id, $table)
	{
		$data    =  $note    =  $log  =  "";
		$query   =  'SELECT * FROM '.$table.' WHERE id = "'.$id.'"';
		$dataC    =  DBSmart::DBQuery($query);
		$query   =  'SELECT * FROM cp_tickets WHERE id = "'.$id.'"';
		$data    =  DBSmart::DBQuery($query);
		$descp   =  $dataC['comment']. "<br><br><table><tbody><tr><td colspan='2'>DATOS DEL CLIENTE</td></tr><tr><td>NOMBRE: &nbsp;</td><td><strong>".$dataC['name']."</strong></td></tr><tr><td>CEDULA: &nbsp;</td><td><strong>".$dataC['cedula']."</strong></td></tr><tr><td>PRINCIPAL: &nbsp;</td><td><strong>".$dataC['phone']."</strong></td></tr><tr><td>ALTERNO: &nbsp;</td><td><strong>".$dataC['alternative']."</strong></td></tr><tr><td>EMAIL: &nbsp;</td><td><strong>".$dataC['email']."</strong></td></tr></tbody></table>";
		if($data <> false)
		{
			$query   =  'SELECT * FROM cp_ticket_notes WHERE ticket = "'.$data['ticket'].'" ORDER BY id DESC';
			$note    =  DBSmart::DBQueryAll($query);
			$query   =  'SELECT * FROM cp_ticket_log WHERE ticket = "'.$data['ticket'].'" ORDER BY id DESC';
			$log  =  DBSmart::DBQueryAll($query);
		}
		return   [
			'desc'   => $descp,
			'data'   => ($data <> false) ? $data : false,
			'notes'  => ($note <> false) ? $note : false,
			'logs'   => ($log <> false)  ? $log  : false,
		];

	}

   	public static function HtmlNotes($iData)
   	{
		$_replace  	=	new Config();
		$html 		=	"";

		if($iData <> false)
		{
			$html.='<ul>';
			foreach ($iData as $k => $val) 
			{
				$html.='<li class="message">';
				$html.='<div class="message-text" style="text-align: justify; margin-left: 0px; color:black;">';
				$html.='<h3><strong><time>'.$_replace->ShowDateAll($val['created_at']).'</time></strong></h3>';
				$html.='<a class="username">'.User::GetUsersID($val['created_by'])['username'].'</a>';
				$html.=''.utf8_encode($val['notes']).'';
				$html.='</div>';
				$html.='</li>';
			}
			$html.='</ul>';
		}
		return $html;
   	
   	}

   	public static function HtmlLog($iData)
   	{
		$_replace  	=	new Config();
		$html 		=	"";

		$html.='<table id="TicketLog" class="table table-bordered" style="text-align: center;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th style="text-align: center;">ID</th>';
		$html.='<th style="text-align: center;">Ejecutivo</th>';
		$html.='<th style="text-align: center;">Contenido</th>';
		$html.='<th style="text-align: center;">Fecha Solicitud</th>';
		$html.='</tr>';
		$html.='</thead>';
		if($iData <> false)
		{
			$html.='<tbody>';
			foreach ($iData as $k => $val) 
			{
				$html.='<tr>';
				$html.='<td>'.$k.'</td>';
				$html.='<td>'.User::GetUsersID($val['created_by'])['username'].'</td>';
				$html.='<td>'.$val['information'].'</td>';
				$html.='<td>'.$_replace->ShowDateAll($val['created_at']).'</td>';
				$html.='</tr>';
			}
			$html.='</body>';
		}
		$html.='</table>';		
		
		return $html;
   	
   	}

   	public static function AdminsSaveNotes($info)
   	{
   		$query 	=	'INSERT INTO cp_ticket_notes (ticket, notes, created_by, created_at) VALUES ("'.$info['ticket'].'", "'.$info['note'].'", "'.$info['operator'].'", NOW())';
   		$note 	=	DBSmart::DataExecute($query);
   		return ($note <> false ) ? true : false;

   	}

  	public static function AdminsSaveLog($info)
   	{
   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$info['ticket'].'", "'.$info['info'].'", "'.$info['operator'].'", NOW())';
   		$note 	=	DBSmart::DataExecute($query);
   		return ($note <> false ) ? true : false;

   	}

   	public static function UpdNP($iData, $info)
   	{
   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "2", operator_id = "'.$iData['ope'].'", operator_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "2", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);

   		return ($upd <> false ) ? true : false;

   	}

   	public static function UpdP($iData, $info)
   	{
   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "3", operator_id = "'.$iData['ope'].'", operator_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "3", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);
   		
   		return ($upd <> false ) ? true : false;

   	}

   	public static function UpdAsig($iData, $info)
   	{

   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "4", operator_id = "'.$iData['ope'].'", operator_at = NOW(), asigned_by = "'.$iData['asig'].'", deadline = "'.$iData['deadline'].'", asigned_at = NOW()  WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "4" WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);
   		
   		return ($upd <> false ) ? true : false;

   	}

   	public static function UpdRAsig($iData, $info)
   	{
   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "5", deadline = "'.$iData['deadline'].'", asigned_by = "'.$iData['asig'].'", asigned_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "5" WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);
   		
   		return ($upd <> false ) ? true : false;

   	}

   	public static function UpdCancel($iData, $info)
   	{
   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "6", operator_id = "'.$iData['ope'].'", operator_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "6", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);

   		return ($upd <> false ) ? true : false;

   	}

   	public static function HtmlStatics($data)
    {
        $html = "";
        $html.='<p style="text-align: center;"> <span>SOLICITUDES RECIBIDAS</span></p>';
		$color = "bg-color-blueDark";
        
        foreach ($data as $d => $dat) 
        {
        	switch ($d) {
        		case 0:
        			$color = "bg-color-green";
        			break;
        		case 1:
        			$color = "bg-color-red";
        			break;
        		case 2:
        			$color = "bg-color-blue";
        			break;
        			
        	}
        	if($dat['username'] <> "ALBAN.LEAD")
        	{
	            $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> ';
	            $html.='<span class="text"> '.$dat['username'].' <span class="pull-right">'.$dat['process'].'/'.$dat['total'].'</span> </span>';
	            $html.='<div class="progress" style="margin-bottom: 0px;">';
	            $html.='<div class="progress-bar '.$color.'" style="width: '.$dat['porcent'].'%;"></div>';
	            $html.='</div>';
	            $html.='</div>';
        	}
        }

        return $html;
    
    }

   	public static function HtmlStaticsGen($data)
    {
        $html = "";
        $html.='<p style="text-align: center;"> <span>SOLICITUDES PROCESASADAS</span></p>';
		$color = "bg-color-blueDark";
        
        foreach ($data as $d => $dat) 
        {

			$html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> ';
            $html.='<span class="text"> '.$dat['username'].' <span class="pull-right">'.$dat['process'].'/'.$dat['tgeneral'].'</span> </span>';
            $html.='<div class="progress" style="margin-bottom: 0px;">';
            $html.='<div class="progress-bar bg-color-green" style="width: '.$dat['porcent'].'%;"></div>';
            $html.='</div>';
            $html.='</div>';
        }

        return $html;
    
    }

///////////////////////////////////////////////////////////////

   	public static function DashboardLoad($ope)
   	{
		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t1.operator_id AS asig, t1.asigned_at AS dateasig, t1.deadline FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t2.status_id = "1" OR t2.status_id = "4" OR t2.status_id = "5") AND t1.asigned_by = "'.$ope.'" ORDER BY t1.id DESC';
		$Pending 	=	DBSmart::DBQueryAll($query);

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t2.status_id = "2" OR t2.status_id = "3") AND t1.asigned_by = "'.$ope.'" ORDER BY t1.id DESC';
		$Process 	=	DBSmart::DBQueryAll($query);

		$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND t2.status_id = "6" AND t1.asigned_by = "'.$ope.'" ORDER BY t1.id DESC';
		$Cancel 	=	DBSmart::DBQueryAll($query);

   		if($Pending <> false)
   		{
   			foreach ($Pending as $t => $tic) 
   			{
				$now 	=	Carbon::now()->toDateTimeString();
				if($tic['deadline'] == '0000-00-00 00:00:00')
				{
					$diff = '';
				}else{
					$diff = round((strtotime($tic['deadline']) - strtotime($now))/(60*60));
				}
   				if ( ($diff < 2) AND ($diff > 0) ) 
   				{
   					$deadline 	=	"POR VENCER";

   				}
   				elseif($diff < 0 )
   				{
   					$deadline 	=	"VENCIDO";

   				}
   				elseif($diff >= 2 )
   				{
   					$deadline 	=	"A TIEMPO";

   				}
   				elseif($diff == '')
   				{
   					$deadline 	=	"";
   				}

   				switch ($tic['status_id']) 
   				{
   					case '':
   						$status = "PENDIENTE";
   						break;
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$pending[$t] 	=	[
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'status'		=>	$status,
   					'asignated'		=>	($tic['asig'] <> '1') ? User::GetUsersID($tic['asig'])['username'] : '',
   					'dateasig'		=>	$tic['dateasig'],
   					'dead'			=>	$tic['deadline'],
   					'deadline'		=>	$deadline,
   				];

   			}
   		
   		}else{ $pending = false;	}
   	
   	   	if($Cancel <> false)
   		{
   			foreach ($Cancel as $t => $tic) 
   			{
				
   				switch ($tic['status_id']) 
   				{
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$cancel[$t] 	=	[
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'dateasig'		=>	$tic['finish_at'],
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{ $cancel	= false; }

   	   	if($Process <> false)
   		{
   			foreach ($Process as $t => $tic) 
   			{
   				switch ($tic['status_id']) 
   				{
   					case '1':
   						$status = "PENDIENTE";
   						break;
   					case '2':
   						$status = "NO PROCEDE";
   						break;
   					case '3':
   						$status = "PROCESADO";
   						break;
   					case '4':
   						$status = "ASIGNADO";
   						break;
   					case '5':
   						$status = "REASIGNADO";
   						break;
   					case '6':
   						$status = "CANCELADO";
   						break;
   				
   				}

   				$process[$t] 	=	[
   					'id'			=>	$tic['id'],
   					'ticket'		=>	$tic['ticket'],
   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
   					'service'		=>	$tic['serv'],
   					'created'		=>	$tic['created_at'],
   					'dateasig'		=>	$tic['finish_at'],
   					'status'		=>	$status,
   				];

   			}
   		
   		}else{ $process = false; }

   		return [
   			'pending'	=>	$pending,
   			'cancel'	=>	$cancel,
   			'process'	=>	$process
   		];
   	
   	}

	// public static function AdminsLoadDate($info, $dates)
 //   	{
	// 	$_replace 	= 	new Config();

	// 	$week 	=	$_replace->CurrentMonthDays();

	// 	if($info == "0")
	// 	{
	// 		$betw 	=	't2.created_at LIKE "'.$dates['dateEnd'].'%"';
	// 		$bwee 	=	'operator_at LIKE "'.$dates['dateEnd'].'%"';
	// 	}else{
	// 		$betw 	=	't2.created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59"';
	// 		$bwee 	=	'operator_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59"';
	// 	}

	// 	$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t1.asigned_by AS eje, t1.asigned_at, t1.deadline FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t1.status_id = "1" OR t1.status_id = "4" OR t1.status_id = "5") AND '.$betw.'';

	// 	$Pending 	=	DBSmart::DBQueryAll($query);

	// 	$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t1.status_id = "2" OR t1.status_id = "3") AND '.$betw.'';
	// 	$Process 	=	DBSmart::DBQueryAll($query);

	// 	$query		=	'SELECT t1.id, t1.ticket, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM cp_ticket_admin AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND t1.status_id = "6" AND '.$betw.'';
	// 	$Cancel 	=	DBSmart::DBQueryAll($query);

 //        $query     	=   'SELECT id, username FROM users WHERE status_id = "1" AND departament_id = "4" ORDER BY id ASC';
 //        $users     	=   DBSmart::DBQueryAll($query);

 //        foreach ($users as $k => $val) 
 //        {
 //            $query      =    'SELECT 
 //            (SELECT COUNT(*) FROM cp_ticket_admin WHERE asigned_by = "'.$val['id'].'" AND '.$bwee.') AS T, 
 //            (SELECT COUNT(*) FROM cp_ticket_admin WHERE asigned_by = "'.$val['id'].'" AND status_id <> "4" AND '.$bwee.') AS Tc, 
 //            (SELECT ROUND((Tc/T) * 100 )) AS Tp';

 //            $ticket     =   DBSmart::DBQuery($query);

 //            $statics[$k]    =   [
 //                'username'  =>  $val['username'],
 //                'total'     =>  $ticket['T'],
 //                'process'   =>  $ticket['Tc'],
 //                'porcent'   =>  ($ticket['Tp'] == null) ? "0" : $ticket['Tp']
 //            ];
 //        }

 //        $query 	=	'SELECT 
	// 	(SELECT COUNT(*) FROM cp_ticket_admin WHERE '.$bwee.') AS T,
	// 	(SELECT COUNT(*) FROM cp_ticket_admin WHERE (status_id = "1" OR status_id = "4" OR status_id = "5")  AND '.$bwee.') AS pend,
	// 	(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "2"  AND '.$bwee.') AS napro,
	// 	(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "3"  AND '.$bwee.') AS apro,
	// 	(SELECT COUNT(*) FROM cp_ticket_admin WHERE status_id = "6"  AND '.$bwee.') AS canc,
	// 	(SELECT ROUND((pend/T) *100)) AS PPen,
	// 	(SELECT ROUND((napro/T) *100)) AS PNApro,
	// 	(SELECT ROUND((apro/T) *100)) AS PApro,
	// 	(SELECT ROUND((canc/T) *100)) AS PCan';

 //        $sta 	=	DBSmart::DBQuery($query);

 //   		if($Pending <> false)
 //   		{
 //   			foreach ($Pending as $t => $tic) 
 //   			{
	// 			$now 	=	Carbon::now()->toDateTimeString();
	// 			if($tic['deadline'] == '0000-00-00 00:00:00')
	// 			{
	// 				$diff = '';
	// 			}else{
	// 				$diff = round((strtotime($tic['deadline']) - strtotime($now))/(60*60));
	// 			}
 //   				if ( ($diff < 2) AND ($diff > 0) ) 
 //   				{
 //   					$deadline 	=	"POR VENCER";

 //   				}
 //   				elseif($diff < 0 )
 //   				{
 //   					$deadline 	=	"VENCIDO";

 //   				}
 //   				elseif($diff >= 2 )
 //   				{
 //   					$deadline 	=	"A TIEMPO";

 //   				}
 //   				elseif($diff == '')
 //   				{
 //   					$deadline 	=	"";
 //   				}

 //   				switch ($tic['status_id']) 
 //   				{
 //   					case '':
 //   						$status = "PENDIENTE";
 //   						break;
 //   					case '1':
 //   						$status = "PENDIENTE";
 //   						break;
 //   					case '2':
 //   						$status = "NO PROCEDE";
 //   						break;
 //   					case '3':
 //   						$status = "PROCESADO";
 //   						break;
 //   					case '4':
 //   						$status = "ASIGNADO";
 //   						break;
 //   					case '5':
 //   						$status = "REASIGNADO";
 //   						break;
 //   					case '6':
 //   						$status = "CANCELADO";
 //   						break;
   				
 //   				}

 //   				$pending[$t] 	=	[
 //   					'sta' 			=>  true,
 //   					'id'			=>	$tic['id'],
 //   					'ticket'		=>	$tic['ticket'],
 //   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
 //   					'service'		=>	$tic['serv'],
 //   					'created'		=>	$tic['created_at'],
 //   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
 //   					'dateasig'		=>	$tic['asigned_at'],
 //   					'deadline'		=>	$deadline,
 //   					'status'		=>	$status,
 //   				];

 //   			}
   		
 //   		}else{
	// 		$pending 	=	false;

 //   		}
   	
 //   	   	if($Process <> false)
 //   		{
 //   			foreach ($Process as $t => $tic) 
 //   			{
 //   				switch ($tic['status_id']) 
 //   				{
 //   					case '':
 //   						$status = "PENDIENTE";
 //   						break;
 //   					case '1':
 //   						$status = "PENDIENTE";
 //   						break;
 //   					case '2':
 //   						$status = "NO PROCEDE";
 //   						break;
 //   					case '3':
 //   						$status = "PROCESADO";
 //   						break;
 //   					case '4':
 //   						$status = "ASIGNADO";
 //   						break;
 //   					case '5':
 //   						$status = "REASIGNADO";
 //   						break;
 //   					case '6':
 //   						$status = "CANCELADO";
 //   						break;
   				
 //   				}

 //   				$process[$t] 	=	[
 //   					'sta' 			=>  true,
 //   					'id'			=>	$tic['id'],
 //   					'ticket'		=>	$tic['ticket'],
 //   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
 //   					'service'		=>	$tic['serv'],
 //   					'created'		=>	$tic['created_at'],
 //   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
 //   					'dateasig'		=>	$tic['finish_at'],
 //   					'deadline'		=>	'',
 //   					'status'		=>	$status,
 //   				];

 //   			}
   		
 //   		}else{
	// 		$process 	=	false;

 //   		}

 //   	   	if($Cancel <> false)
 //   		{
 //   			foreach ($Cancel as $t => $tic) 
 //   			{
				
 //   				switch ($tic['status_id']) 
 //   				{
 //   					case '':
 //   						$status = "PENDIENTE";
 //   						break;
 //   					case '1':
 //   						$status = "PENDIENTE";
 //   						break;
 //   					case '2':
 //   						$status = "NO PROCEDE";
 //   						break;
 //   					case '3':
 //   						$status = "PROCESADO";
 //   						break;
 //   					case '4':
 //   						$status = "ASIGNADO";
 //   						break;
 //   					case '5':
 //   						$status = "REASIGNADO";
 //   						break;
 //   					case '6':
 //   						$status = "CANCELADO";
 //   						break;
   				
 //   				}

 //   				$cancel[$t] 	=	[
 //   					'sta' 			=>  true,
 //   					'id'			=>	$tic['id'],
 //   					'ticket'		=>	$tic['ticket'],
 //   					'operator'		=>	($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
 //   					'service'		=>	$tic['serv'],
 //   					'created'		=>	$tic['created_at'],
 //   					'ejecutivo'		=>	($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
 //   					'dateasig'		=>	$tic['finish_at'],
 //   					'deadline'		=>	'',
 //   					'status'		=>	$status,
 //   				];

 //   			}
   		
 //   		}else{
	// 		$cancel 	=	false;

 //   		}

 //   		return [
 //   			'pending'		=>	$pending,
 //   			'cancel'		=>	$cancel,
 //   			'process'		=>	$process,
 //   			'statics'		=>	$statics,
 //   			'GenStatics'	=>	$sta			
 //   		];

 //   	}  	

   	public static function DashboardPeningHTML($iData)
   	{
		$html = "";
		$html.='<table id="TablePending" class="table table-bordered" style="text-align: center;">';
			$html.='<thead>';
				$html.='<tr>';
					$html.='<th style="text-align: center;">Ticket</th>';
					$html.='<th style="text-align: center;">Operador</th>';
					$html.='<th style="text-align: center;">Servicio</th>';
					$html.='<th style="text-align: center;">Fecha Solicitud</th>';
					$html.='<th style="text-align: center;">Status</th>';
					$html.='<th style="text-align: center;">Asigando Por</th>';
					$html.='<th style="text-align: center;">Fecha Asignaci&oacute;n</th>';
					$html.='<th style="text-align: center;">DeadLine</th>';
					$html.='<th style="text-align: center;">Fecha DeadLine</th>';
					$html.='<th style="text-align: center;">Acci&oacute;n</th>';
				$html.='</tr>';
			$html.='</thead>';
		if($iData <> false)
		{
			$html.='<tbody>';
				foreach ($iData as $i => $iD) 
				{
					if($iD['deadline'] == "POR VENCER")
			   		{ $deadline 	=	'<span class="label label-warning">'.$iD['deadline'].'</span>'; }
			   		elseif($iD['deadline'] == "VENCIDO")
			   		{ $deadline 	=	'<span class="label label-danger">'.$iD['deadline'].'</span>'; }
			   		elseif($iD['deadline'] == "A TIEMPO")
			   		{ $deadline 	=	'<span class="label label-info">'.$iD['deadline'].'</span>'; }
			   		else { $deadline 	=	''; }

					if($iD['status'] == "PENDIENTE")
			   		{ $status 	=	'<span class="label label-info">'.$iD['status'].'</span>'; }
			   		elseif($iD['status'] == "NO PROCEDE")
			   		{ $status 	=	'<span class="label label-danger">'.$iD['status'].'</span>'; }
			   		elseif($iD['status'] == "PROCESADO")
			   		{ $status 	=	'<span class="label label-success">'.$iD['status'].'</span>'; }
			   		elseif($iD['status'] == "ASIGNADO")
			   		{ $status 	=	'<span class="label label-warning">'.$iD['status'].'</span>';}
					elseif($iD['status'] == "REASIGNADO")
			   		{ $status 	=	'<span class="label label-warning">'.$iD['status'].'</span>'; }
					elseif($iD['status'] == "CANCELADO")
			   		{ $status 	=	'<span class="label label-danger">'.$iD['status'].'</span>'; }

					$html.='<tr>';
					$html.='<td>'.$iD['ticket'].'</td>';
					$html.='<td>'.$iD['operator'].'</td>';
					$html.='<td>'.$iD['service'].'</td>';
					$html.='<td>'.$iD['created'].'</td>';
					$html.='<td>'.$status.'</td>';
					$html.='<td>'.$iD['asignated'].'</td>';
					$html.='<td>'.$iD['dateasig'].'</td>';
					$html.='<td>'.$deadline.'</td>';
					$html.='<td>'.$iD['dead'].'</td>';
					$html.='<td><table><tbody>';
						$html.='<tr><a onclick="InfoFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-info-circle"></i></a></tr>';
						$html.='<tr><a onclick="ProcessFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-spinner"></i></a></tr>';
					$html.='</tbody></table></td>';
					$html.='</tr>';
				}

			$html.='</tbody>';

		}else{
			$html.='<tbody>';
			$html.='</tbody>';			
		}

		return $html;
	
	}

	public static function DashboardPeningGenHTML($iData)
	{
	  $html = "";
	  $html.='<table id="TablePending" class="table table-bordered" style="text-align: center;">';
	     $html.='<thead>';
	        $html.='<tr>';
	           $html.='<th style="text-align: center;">Ticket</th>';
	           $html.='<th style="text-align: center;">Operador</th>';
	           $html.='<th style="text-align: center;">Tipo</th>';
	           $html.='<th style="text-align: center;">Cedula</th>';
	           $html.='<th style="text-align: center;">Servicio</th>';
	           $html.='<th style="text-align: center;">Fecha Solicitud</th>';
	           $html.='<th style="text-align: center;">Status</th>';
	           $html.='<th style="text-align: center;">Acci&oacute;n</th>';
	        $html.='</tr>';
	     $html.='</thead>';
	  if($iData <> false)
	  {
	     $html.='<tbody>';
	        foreach ($iData as $i => $iD) 
	        {

	        	$type = '';

	           if($iD['status'] == "PENDIENTE")
	              { $status   =  '<span class="label label-info">'.$iD['status'].'</span>'; }
	              elseif($iD['status'] == "NO PROCEDE")
	              { $status   =  '<span class="label label-danger">'.$iD['status'].'</span>'; }
	              elseif($iD['status'] == "PROCESADO")
	              { $status   =  '<span class="label label-success">'.$iD['status'].'</span>'; }
	              elseif($iD['status'] == "ASIGNADO")
	              { $status   =  '<span class="label label-warning">'.$iD['status'].'</span>';}
	           elseif($iD['status'] == "REASIGNADO")
	              { $status   =  '<span class="label label-warning">'.$iD['status'].'</span>'; }
	           elseif($iD['status'] == "CANCELADO")
	              { $status   =  '<span class="label label-danger">'.$iD['status'].'</span>'; }

	          	if($iD['operator'] == "CLIENT.WEB")
	          	{
	          		$type   =  '<span class="label label-danger">PAGINA WEB</span>';
	          	}

	           $html.='<tr>';
	           $html.='<td>'.$iD['ticket'].'</td>';
	           $html.='<td>'.$iD['operator'].'</td>';
	           $html.='<td>'.$type.'</td>';
	           $html.='<td>'.$iD['cedula'].'</td>';
	           $html.='<td>'.$iD['service'].'</td>';
	           $html.='<td>'.$iD['created'].'</td>';
	           $html.='<td>'.$status.'</td>';
	           $html.='<td><table><tbody>';
	              $html.='<tr><a onclick="InfoFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-info-circle"></i></a></tr>';
	              $html.='<tr><a onclick="ProcessFunctions('.$iD['id'].')" class="btn btn-xs btn-xs btn-default"><i class="fa fa-spinner"></i></a></tr>';
	           $html.='</tbody></table></td>';
	           $html.='</tr>';
	        }

	     $html.='</tbody>';

	  }else{
	     $html.='<tbody>';
	     $html.='</tbody>';         
	  }

	  $html.='</table>';

	  return $html;

	}

   	public static function UpdDashNP($iData, $info)
   	{
   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "2" WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "2", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);

   		return ($upd <> false ) ? true : false;

   	}

   	public static function UpdDashP($iData, $info)
   	{
   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "3" WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "2", description = "'.$iData['text'].'", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);
   		
   		return ($upd <> false ) ? true : false;

   	}
   	
   	public static function UpdDashCancel($iData, $info)
   	{
   		$query 	=	'UPDATE cp_ticket_admin SET status_id = "6", operator_id = "'.$iData['ope'].'", operator_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'UPDATE cp_tickets SET status_id = "6", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
   		$upd 	=	DBSmart::DataExecute($query);

   		$query 	=	'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
   		$upd 	=	DBSmart::DataExecute($query);

   		return ($upd <> false ) ? true : false;

   	}

   public static function UpdGenNP($iData, $info, $table)
   {
      $query   =  'UPDATE '.$table.' SET status_id = "2" WHERE id = "'.$iData['id'].'"';
      $upd  =  DBSmart::DataExecute($query);

      $query   =  'UPDATE cp_tickets SET status_id = "2", description = "'.$iData['text'].'", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
      $upd  =  DBSmart::DataExecute($query);

      $query   =  'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
      $upd  =  DBSmart::DataExecute($query);

      return ($upd <> false ) ? true : false;

   }

   public static function UpdGenP($iData, $info, $table)
   {
      $query   =  'UPDATE '.$table.' SET status_id = "3" WHERE id = "'.$iData['id'].'"';
      $upd  =  DBSmart::DataExecute($query);

      $query   =  'UPDATE cp_tickets SET status_id = "2", description = "'.$iData['text'].'", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
      $upd  =  DBSmart::DataExecute($query);

      $query   =  'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
      $upd  =  DBSmart::DataExecute($query);
      
      return ($upd <> false ) ? true : false;

   }

   public static function UpdGenCancel($iData, $info, $table)
   {
      $query   =  'UPDATE '.$table.' SET status_id = "6", operator_id = "'.$iData['ope'].'", operator_at = NOW() WHERE id = "'.$iData['id'].'"';
      $upd  =  DBSmart::DataExecute($query);

      $query   =  'UPDATE cp_tickets SET status_id = "6", description = "'.$iData['text'].'", finish_by = "'.$iData['ope'].'", finish_at = NOW() WHERE id = "'.$iData['id'].'"';
      $upd  =  DBSmart::DataExecute($query);

      $query   =  'INSERT INTO cp_ticket_log (ticket, information, created_by, created_at) VALUES ("'.$iData['ticket'].'", "'.$info.'", "'.$iData['ope'].'", NOW())';
      $upd  =  DBSmart::DataExecute($query);

      return ($upd <> false ) ? true : false;

   }

	public static function DashboardGenLoad($table, $dep)
	{
	  $_replace   =  new Config();

	  $week    =  $_replace->CurrentMonthDays();

	  $betw    =  't2.created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
	  $bwee    =  'operator_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
	  $bweS    =  'finish_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';
	  $bwec    =  'created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59"';

	  $query      =  'SELECT t1.id, t1.ticket, t1.name, t1.cedula, t1.phone, t1.alternative, t1.email, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t1.operator_id AS asig, t1.asigned_at AS dateasig, t1.deadline FROM '.$table.' AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t2.status_id = "1" OR t2.status_id = "4" OR t2.status_id = "5") AND t1.asigned_by = "1" AND '.$betw.' ORDER BY t1.id DESC';
	  // var_dump($query);exit;
	  $Pending    =  DBSmart::DBQueryAll($query);

	  $query      =  'SELECT t1.id, t1.ticket, t1.name, t1.cedula, t1.phone, t1.alternative, t1.email, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM '.$table.' AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t2.status_id = "2" OR t2.status_id = "3") AND t1.asigned_by = "1" AND '.$betw.' ORDER BY t1.id DESC';
	  $Process    =  DBSmart::DBQueryAll($query);

	  $query      =  'SELECT t1.id, t1.ticket, t1.name, t1.cedula, t1.phone, t1.alternative, t1.email, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM '.$table.' AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND t2.status_id = "6" AND t1.asigned_by = "1" AND '.$betw.' ORDER BY t1.id DESC';
	  $Cancel  =  DBSmart::DBQueryAll($query);

	    $query    =  'SELECT (SELECT id FROM users WHERE id = finish_by) AS id, COUNT(id) AS T, (SELECT username FROM users WHERE id = finish_by) AS username FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND finish_by <> "1" AND created_at BETWEEN'.' "'.$week['yI'].'-'.$week['mI'].'-'.$week['dI'].' 00:00:00" AND "'.$week['yF'].'-'.$week['mF'].'-'.$week['dF'].' 23:59:59" GROUP BY finish_by order by T DESC LIMIT 5';
	    $users       =   DBSmart::DBQueryAll($query);

	    if($users <> false)
	    {
		    foreach ($users as $k => $val) 
		    {
		        $query      =    'SELECT 
		        (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND '.$bwec.') AS TG, 
		        (SELECT COUNT(*) FROM cp_tickets WHERE finish_by = "'.$val['id'].'" AND '.$bweS.') AS T, 
		        (SELECT COUNT(*) FROM cp_tickets WHERE finish_by = "'.$val['id'].'" AND status_id <> "4" AND '.$bweS.') AS Tc, 
		        (SELECT ROUND((Tc/TG) * 100 )) AS Tp';
		        $ticket     =   DBSmart::DBQuery($query);

		        $statics[$k]    =   [
		            'username'  =>  $val['username'],
		            'tgeneral'  =>  $ticket['TG'],
		            'total'     =>  $ticket['T'],
		            'process'   =>  $ticket['Tc'],
		            'porcent'   =>  ($ticket['Tp'] == null) ? "0" : $ticket['Tp']
		        ];
		    
		    }

	    }else{

	    	$query	 	=	'SELECT username FROm users WHERE departament_id = "'.$dep.'" AND status_id = "1" ORDER BY id ASC LIMIT 5';
	    	$users 		=	DBSmart::DBQueryAll($query);

	    	foreach ($users as $u => $us) 
	    	{
		    
		    	$statics[$u] 	=	[
		            'username'  =>  $us['username'],
		            'tgeneral'  => 	0,
		            'total'     =>	0,
		            'process'   =>	0,
		            'porcent'   =>	0
		    	];
	    	}

	    }


	   $query    =  'SELECT 
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND '.$bwec.') AS T,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND (status_id = "1" OR status_id = "4" OR status_id = "5")  AND '.$bwec.') AS pend,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND status_id = "2"  AND '.$bwec.') AS napro,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND status_id = "2"  AND '.$bwec.') AS apro,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND status_id = "6"  AND '.$bwec.') AS canc,
	  (SELECT ROUND((pend/T) *100)) AS PPen,
	  (SELECT ROUND((napro/T) *100)) AS PNApro,
	  (SELECT ROUND((apro/T) *100)) AS PApro,
	  (SELECT ROUND((canc/T) *100)) AS PCan';

	  $sta   =  DBSmart::DBQuery($query);

	  if($sta == false)
	  {
	  	$sta 	=	[
	  		'pend' 		=>	0,
	  		'napro' 	=>	0,
	  		'apro' 		=>	0,
	  		'canc' 		=>	0,
	  		'PPen' 		=>	0,
	  		'PNApro'	=>	0,
	  		'PApro'		=>	0,
	  		'PCan'		=>	0,
	  	];
	  }

	  if($Pending <> false)
	  {
	     foreach ($Pending as $t => $tic) 
	     {

	     $now  =  Carbon::now()->toDateTimeString();
	     if($tic['deadline'] == '0000-00-00 00:00:00')
	     {
	        $diff = '';
	     }else{
	        $diff = round((strtotime($tic['deadline']) - strtotime($now))/(60*60));
	     }
	        if ( ($diff < 2) AND ($diff > 0) ) 
	        {
	           $deadline   =  "POR VENCER";

	        }
	        elseif($diff < 0 )
	        {
	           $deadline   =  "VENCIDO";

	        }
	        elseif($diff >= 2 )
	        {
	           $deadline   =  "A TIEMPO";

	        }
	        elseif($diff == '')
	        {
	           $deadline   =  "";
	        }

	        switch ($tic['status_id']) 
	        {
	           case '':
	              $status = "PENDIENTE";
	              break;
	           case '1':
	              $status = "PENDIENTE";
	              break;
	           case '2':
	              $status = "NO PROCEDE";
	              break;
	           case '3':
	              $status = "PROCESADO";
	              break;
	           case '4':
	              $status = "ASIGNADO";
	              break;
	           case '5':
	              $status = "REASIGNADO";
	              break;
	           case '6':
	              $status = "CANCELADO";
	              break;
	        
	        }

	        

	        $pending[$t]   =  [
	           'id'           => $tic['id'],
	           'ticket'       => $tic['ticket'],
	           'operator'     => ($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
	           'service'      => $tic['serv'],
	           'cedula'    	  => $tic['cedula'],
	           'created'      => $tic['created_at'],
	           'status'       => $status,
	        ];

	     }
	  
	  }else{ $pending = false;   }

	  if($Cancel <> false)
	  {
	     foreach ($Cancel as $t => $tic) 
	     {
	     
	        switch ($tic['status_id']) 
	        {
	           case '1':
	              $status = "PENDIENTE";
	              break;
	           case '2':
	              $status = "NO PROCEDE";
	              break;
	           case '3':
	              $status = "PROCESADO";
	              break;
	           case '4':
	              $status = "ASIGNADO";
	              break;
	           case '5':
	              $status = "REASIGNADO";
	              break;
	           case '6':
	              $status = "CANCELADO";
	              break;
	        
	        }

	        $cancel[$t]    =  [
	           'id'        => $tic['id'],
	           'ticket'    => $tic['ticket'],
	           'operator'  => ($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
	           'ejecutivo' => ($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
	           'cedula'    => $tic['cedula'],
	           'service'   => $tic['serv'],
	           'created'   => $tic['created_at'],
	           'dateasig'  => $tic['finish_at'],
	           'status'    => $status,
	        ];

	     }
	  
	  }else{ $cancel = false; }

	     if($Process <> false)
	  {
	     foreach ($Process as $t => $tic) 
	     {
	        switch ($tic['status_id']) 
	        {
	           case '1':
	              $status = "PENDIENTE";
	              break;
	           case '2':
	              $status = "NO PROCEDE";
	              break;
	           case '3':
	              $status = "PROCESADO";
	              break;
	           case '4':
	              $status = "ASIGNADO";
	              break;
	           case '5':
	              $status = "REASIGNADO";
	              break;
	           case '6':
	              $status = "CANCELADO";
	              break;
	        
	        }

	        $process[$t]   =  [
	           'id'        => $tic['id'],
	           'ticket'    => $tic['ticket'],
	           'operator'  => ($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
	           'ejecutivo' => ($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
	           'cedula'    => $tic['cedula'],
	           'service'   => $tic['serv'],
	           'created'   => $tic['created_at'],
	           'dateasig'  => $tic['finish_at'],
	           'status'    => $status,
	        ];

	     }
	  
	  }else{ $process = false; }

	  return [
	     'pending'      => $pending,
	     'cancel'       => $cancel,
	     'process'      => $process,
	     'statics'      => $statics,
	     'GenStatics'   => $sta  
	  ];

	}

	public static function DashboardGenSD($info, $dates, $dep, $table)
	{
		$_replace 	= 	new Config();

		$week 	=	$_replace->CurrentMonthDays();

		if($info == "0")
		{
			$betw 	=	'created_at LIKE "'.$dates['dateEnd'].'%"';
			$bwee 	=	'operator_at LIKE "'.$dates['dateEnd'].'%"';
		}else{
			$betw 	=	'created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59"';
			$bwee 	=	'operator_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59"';
		}

		$query      =  'SELECT t1.id, t1.ticket, t1.name, t1.cedula, t1.phone, t1.alternative, t1.email, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t1.operator_id AS asig, t1.asigned_at AS dateasig, t1.deadline FROM '.$table.' AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t2.status_id = "1" OR t2.status_id = "4" OR t2.status_id = "5") AND t1.asigned_by = "1" AND '.$betw.' ORDER BY t1.id DESC';

		$Pending    =  DBSmart::DBQueryAll($query);

		$query      =  'SELECT t1.id, t1.ticket, t1.name, t1.cedula, t1.phone, t1.alternative, t1.email, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM '.$table.' AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND (t2.status_id = "2" OR t2.status_id = "3") AND t1.asigned_by = "1" AND '.$betw.' ORDER BY t1.id DESC';
		$Process    =  DBSmart::DBQueryAll($query);

		$query      =  'SELECT t1.id, t1.ticket, t1.name, t1.cedula, t1.phone, t1.alternative, t1.email, t2.operator_id AS ope, (SELECT name FROM data_ticket_serv WHERE id = t2.serv_id) AS serv, t2.created_at, t1.status_id, t2.finish_by AS eje, t2.finish_at FROM '.$table.' AS t1 INNER JOIN cp_tickets AS t2 ON t1.ticket = t2.ticket AND t2.status_id = "6" AND t1.asigned_by = "1" AND '.$betw.' ORDER BY t1.id DESC';
		$Cancel  =  DBSmart::DBQueryAll($query);

		$query    =  'SELECT (SELECT id FROM users WHERE id = finish_by) AS id, COUNT(id) AS T, (SELECT username FROM users WHERE id = finish_by) AS username FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND finish_by <> "1" AND created_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59" GROUP BY finish_by order by T DESC LIMIT 5';
		$users       =   DBSmart::DBQueryAll($query);

	    if($users <> false)
	    {
		    foreach ($users as $k => $val) 
		    {
		        $query      =    'SELECT 
		        (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND '.$betw.') AS TG, 
		        (SELECT COUNT(*) FROM cp_tickets WHERE finish_by = "'.$val['id'].'" AND '.$betw.') AS T, 
		        (SELECT COUNT(*) FROM cp_tickets WHERE finish_by = "'.$val['id'].'" AND status_id <> "4" AND '.$betw.') AS Tc, 
		        (SELECT ROUND((Tc/TG) * 100 )) AS Tp';
		        $ticket     =   DBSmart::DBQuery($query);

		        $statics[$k]    =   [
		            'username'  =>  $val['username'],
		            'tgeneral'  =>  $ticket['TG'],
		            'total'     =>  $ticket['T'],
		            'process'   =>  $ticket['Tc'],
		            'porcent'   =>  ($ticket['Tp'] == null) ? "0" : $ticket['Tp']
		        ];
		    
		    }

	    }else{

	    	$query	 	=	'SELECT username FROm users WHERE departament_id = "'.$dep.'" AND status_id = "1" ORDER BY id ASC LIMIT 5';
	    	$users 		=	DBSmart::DBQueryAll($query);

	    	foreach ($users as $u => $us) 
	    	{
		    
		    	$statics[$u] 	=	[
		            'username'  =>  $us['username'],
		            'tgeneral'  => 	0,
		            'total'     =>	0,
		            'process'   =>	0,
		            'porcent'   =>	0
		    	];
	    	}

	    }


	   $query    =  'SELECT 
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND '.$betw.') AS T,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND (status_id = "1" OR status_id = "4" OR status_id = "5")  AND '.$betw.') AS pend,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND status_id = "2"  AND '.$betw.') AS napro,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND status_id = "2"  AND '.$betw.') AS apro,
	  (SELECT COUNT(*) FROM cp_tickets WHERE ticket_dep = "'.$dep.'" AND status_id = "6"  AND '.$betw.') AS canc,
	  (SELECT ROUND((pend/T) *100)) AS PPen,
	  (SELECT ROUND((napro/T) *100)) AS PNApro,
	  (SELECT ROUND((apro/T) *100)) AS PApro,
	  (SELECT ROUND((canc/T) *100)) AS PCan';

	  $sta   =  DBSmart::DBQuery($query);

	  if($sta == false)
	  {
	  	$sta 	=	[
	  		'pend' 		=>	0,
	  		'napro' 	=>	0,
	  		'apro' 		=>	0,
	  		'canc' 		=>	0,
	  		'PPen' 		=>	0,
	  		'PNApro'	=>	0,
	  		'PApro'		=>	0,
	  		'PCan'		=>	0,
	  	];
	  }

	  if($Pending <> false)
	  {
	     foreach ($Pending as $t => $tic) 
	     {
	     $now  =  Carbon::now()->toDateTimeString();
	     if($tic['deadline'] == '0000-00-00 00:00:00')
	     {
	        $diff = '0000-00-00 00:00:00';
	     }else{
	        $diff = round((strtotime($tic['deadline']) - strtotime($now))/(60*60));
	     }
	        if ( ($diff < 2) AND ($diff > 0) ) 
	        {
	           $deadline   =  "POR VENCER";

	        }
	        elseif($diff < 0 )
	        {
	           $deadline   =  "VENCIDO";

	        }
	        elseif($diff >= 2 )
	        {
	           $deadline   =  "A TIEMPO";

	        }
	        elseif($diff == '')
	        {
	           $deadline   =  "";
	        }

	        switch ($tic['status_id']) 
	        {
	           case '':
	              $status = "PENDIENTE";
	              break;
	           case '1':
	              $status = "PENDIENTE";
	              break;
	           case '2':
	              $status = "NO PROCEDE";
	              break;
	           case '3':
	              $status = "PROCESADO";
	              break;
	           case '4':
	              $status = "ASIGNADO";
	              break;
	           case '5':
	              $status = "REASIGNADO";
	              break;
	           case '6':
	              $status = "CANCELADO";
	              break;
	        
	        }

	        $pending[$t]   =  [
				'id'           => $tic['id'],
				'ticket'       => $tic['ticket'],
				'operator'     => ($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
				'service'      => $tic['serv'],
				'cedula'       => $tic['cedula'],
				'created'      => $tic['created_at'],
				'status'       => $status,
	        ];

	     }
	  
	  }else{ $pending = false;   }

	  if($Cancel <> false)
	  {
	     foreach ($Cancel as $t => $tic) 
	     {
	     
	        switch ($tic['status_id']) 
	        {
	           case '1':
	              $status = "PENDIENTE";
	              break;
	           case '2':
	              $status = "NO PROCEDE";
	              break;
	           case '3':
	              $status = "PROCESADO";
	              break;
	           case '4':
	              $status = "ASIGNADO";
	              break;
	           case '5':
	              $status = "REASIGNADO";
	              break;
	           case '6':
	              $status = "CANCELADO";
	              break;
	        
	        }

	        $cancel[$t]    =  [
	           'id'        	=> $tic['id'],
	           'ticket'    	=> $tic['ticket'],
	           'operator'   => ($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
	           'ejecutivo'  => ($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
	           'service'    => $tic['serv'],
	           'cedula'     => $tic['cedula'],
	           'created'    => $tic['created_at'],
	           'dateasig'   => $tic['finish_at'],
	           'status'    	=> $status,
	        ];

	     }
	  
	  }else{ $cancel = false; }

	     if($Process <> false)
	  {
	     foreach ($Process as $t => $tic) 
	     {
	        switch ($tic['status_id']) 
	        {
	           case '1':
	              $status = "PENDIENTE";
	              break;
	           case '2':
	              $status = "NO PROCEDE";
	              break;
	           case '3':
	              $status = "PROCESADO";
	              break;
	           case '4':
	              $status = "ASIGNADO";
	              break;
	           case '5':
	              $status = "REASIGNADO";
	              break;
	           case '6':
	              $status = "CANCELADO";
	              break;
	        
	        }

	        $process[$t]   =  [
	           'id'        => $tic['id'],
	           'ticket'    => $tic['ticket'],
	           'operator'  => ($tic['ope'] <> '1') ? User::GetUsersID($tic['ope'])['username'] : '',
	           'ejecutivo' => ($tic['eje'] <> '1') ? User::GetUsersID($tic['eje'])['username'] : '',
	           'service'   => $tic['serv'],
	           'cedula'    => $tic['cedula'],
	           'created'   => $tic['created_at'],
	           'dateasig'  => $tic['finish_at'],
	           'status'    => $status,
	        ];

	     }
	  
	  }else{ $process = false; }

	  return [
	     'pending'      => $pending,
	     'cancel'       => $cancel,
	     'process'      => $process,
	     'statics'      => $statics,
	     'GenStatics'   => $sta  
	  ];

	}


}