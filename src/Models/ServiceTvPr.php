<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Service;
use App\Models\Country;
use App\Models\ServiceTvPr;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\User;
use App\Models\Leads;
use App\Models\LeadsProvider;

use App\Models\Package;
use App\Models\Provider;
use App\Models\OrderType;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;

use PDO;

class ServiceTvPr extends Model 
{

	static $_table = 'cp_service_pr_tv';
	
	Public $_fillable = array('ticket', 'client_id', 'tv', 'package_id', 'provider_id', 'decoder_id', 'dvr_id', 'payment_id', 'ref_id', 'ref_list_id', 'sup_id', 'sup_ope_id', 'service_id', 'advertising_id', 'type_order_id', 'origen_id', 'ch_hd', 'ch_dvr', 'ch_hbo', 'ch_cinemax', 'ch_starz', 'ch_showtime', 'gif_ent', 'gif_choice', 'gif_xtra', 'operator_id', 'created_at');

	public static function GetTicket($info)
	{
		$query 	= 	'SELECT ticket FROM cp_service_pr_tv ORDER BY ticket DESC';
		$tic 	=	DBSmart::DBQuery($query);

		if($tic <> false)
		{
			$tick = (substr($tic['ticket'], 2, strlen($tic['ticket'])) + 1);
			return "TV".$tick;
		}else{
			return "TV10001";
		}
	
	}

	public static function GetServiceByTicket($ticket)
	{
		$query 	= 	'SELECT * FROM cp_service_pr_tv WHERE ticket = "'.$ticket.'"';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{	
			return [
				'ticket'	=>	$serv['ticket'],
				'client'	=>	$serv['client_id'],
				'tv'		=>	$serv['tv'],
				'decoders'	=>	$serv['decoder_id'],
				'packages'	=>	$serv['package_id'],
				'dvrs'		=>	$serv['dvr_id'],
				'provider'	=>	$serv['provider_id'],
				'payment'	=>	$serv['payment_id'],
				'ref'		=>	$serv['ref_id'],
				'ref_name'	=>	$serv['ref_id'],
				'ref_client'=>	$serv['ref_id'],
				'sup'		=> 	$serv['sup_id'],
				'sup_pro'	=>	$serv['sup_id'],	
				'sup_apo'	=>	$serv['sup_ope_id'],
				'hd'		=>	$serv['ch_hd'],
				'dvr'		=>	$serv['ch_dvr'],
				'hbo'		=>	$serv['ch_hbo'],
				'cinemax'	=>	$serv['ch_cinemax'],
				'starz'		=>	$serv['ch_starz'],
				'showtime'	=>	$serv['ch_showtime'],
				'gift_ent'	=>	$serv['gif_ent'],
				'gift_cho'	=>	$serv['gif_choice'],
				'gift_xtra'	=>	$serv['gif_xtra'],
				'created'	=>	$serv['created_at']
			];

		}else{ return false; }

	}

	public static function GetService($ticket)
	{

        $query 		=	'SELECT t1.id, t1.ticket, t1.created_at, t1.client_id, t3.name, t3.phone_main as phone, t1.service_id, t5.name as service, (SELECT username FROM users WHERE id = t2.operator_id) as sale, t6.name as departament, t1.status_id, (SELECT name FROM data_score WHERE id = t1.score) AS score, (SELECT IF(t1.status_id = 1, "PENDIENTE", "PROCESADO")) AS status, (SELECT username FROM users WHERE id = t1.operator_id) AS operator, t2.created_at AS date_process FROM cp_appro_serv AS t1 INNER JOIN cp_service_pr_tv AS t2 ON (t1.ticket = t2.ticket) INNER JOIN cp_leads as t3 ON (t1.client_id = t3.client_id) INNER JOIN users as t4 ON (t2.operator_id = t4.id) INNER JOIN data_services AS t5 ON (t1.service_id = t5.id) INNER JOIN data_departament as t6 ON (t4.departament_id = t6.id) INNER JOIN cp_appro_score AS t7 ON (t1.score_id = t7.id) AND t1.ticket = "'.$ticket.'"';

		$result 	=	DBSmart::DBQuery($query);

        if($result <> false)
        {
			$staHtml = ($result['status_id'] == 1) 	? 
        		'<span class="label label-warning">'.$result['status'].'</span>': 
        		'<span class="label label-success">'.$result['status'].'</span>';

			return	[
				'id'			=>	$result['id'],
	    		'ticket'		=>	$result['ticket'],
	    		'created'		=>	$result['created_at'],
	    		'client'		=>	$result['client_id'],
	    		'name'			=>	$result['name'],
	    		'phone'			=>	$result['phone'],
	    		'serv'			=>	$result['service_id'],
	    		'service'		=>	$result['service'],
	    		'servHtml'		=>	'<span class="label label-primary">'.$result['service'].'</span>',
	    		'sale'			=>	$result['sale'],
	    		'departament'	=>	$result['departament'],
	    		'status'		=>	$result['status'],
	    		'statusHtml'	=>	$staHtml,
	    		'score'			=>	$result['score'],
	    		'operator'		=>	$result['operator'],
	    		'date_process'	=>	$result['date_process']
	    	];

        }else{
        	return false;
        }

	}

	public static function GetSubmit($info, $user)
	{
		
		$query 	=	'SELECT * FROM cp_service_pr_tv WHERE ticket = "'.$info.'"';
		$sub 	=	DBSmart::DBQuery($query);

		if($sub <> false)
		{
			if($sub['sup_id'] == 0)
			{$pro 	=	$user; $apo 	=	$user; }
			else
			{ $pro 	=	User::GetUserById($sub['sup_ope_id'])['username']; $apo 	=	$user;}

			$data =	[
				'ticket'		=>	$sub['ticket'],
				'client'		=>	$sub['client_id'],
				'tv'			=>	($sub['tv'] <> 0) 				? $sub['tv'] : 0,
				'decoders'		=>	($sub['decoder_id'] <> 0)		? $sub['decoder_id']											: 	"NO",
				'packages'		=>	($sub['package_id'] <> 0)		? Package::GetPackageById($sub['package_id'])['name']			: 	"NO",
				'dvrs'			=>	($sub['dvr_id'] <> 0) 			? $sub['dvr_id'] 												: 	"NO",
				'provider'		=>	($sub['provider_id'] <> 0) 		? Provider::GetProviderById($sub['provider_id'])['name'] 		: 	"NO",
				'payment'		=>	($sub['payment_id'] <> 0)		? Payment::GetPaymentById($sub['payment_id'])['name']			: 	"NO",
				'ref'			=>	($sub['ref_id'] == 1) 			? "SI" 															:	"NO",
				'ref_name'		=>	($sub['ref_id'] == 1) 			? Referred::GetRefId($sub['ref_list_id'])['name']  				: 	"NO",
				'ref_client'	=>	($sub['ref_id'] == 1) 			? Referred::GetRefId($sub['ref_list_id'])['client']				: 	"NO",
				'adv'			=>	($sub['advertising_id'] == 1)	? "SI" 															: 	"NO",
				'order_type'	=>	($sub['type_order_id'])			? $sub['type_order_id'] 										: 	"",
				'o_type'		=>	($sub['type_order_id'])			? OrderType::GetOrderTypeById($sub['type_order_id'])['name'] 	: 	"",
				'origen_type'	=>	($sub['origen_id'])				? $sub['origen_id'] 											: 	"",
				't_origen'		=>	($sub['origen_id'])				? LeadsProvider::GetLeadsProviderById($sub['type_order_id'])['name'] 	: 	"",
				'sup'			=> 	($sub['sup_id'] == 1) 			? "SI" : 	"NO",
				'sup_pro'		=>	$pro,	
				'sup_apo'		=>	$apo,
				'hd'			=>	($sub['ch_hd'] <> "off")		? "SI" :	"NO",
				'dvr'			=>	($sub['ch_dvr'] <> "off")		? "SI" :	"NO",
				'hbo'			=>	($sub['ch_hbo'] <> "off")		? "SI" :	"NO",
				'cinemax'		=>	($sub['ch_cinemax'] <> "off")	? "SI" :	"NO",
				'starz'			=>	($sub['ch_starz'] <> "off")		? "SI" :	"NO",
				'showtime'		=>	($sub['ch_showtime'] <> "off")	? "SI" :	"NO",
				'gift_ent'		=>	($sub['gif_ent'] <> "off")		? "SI" :	"NO",
				'gift_cho'		=>	($sub['gif_choice'] <> "off")	? "SI" :	"NO",
				'gift_xtra'		=>	($sub['gif_xtra'] <> "off")		? "SI" :	"NO",
				'created'		=>	$sub['created_at']
			];
			return $data;
		}else{
			return false;
		}

	}

	public static function SaveService($ticket, $info, $date)
	{

		$user 	=	($info['sup'] == 0) ? $info['operator'] : $info['sup_list'];

		$query 	= 	'INSERT INTO cp_service_pr_tv(ticket, client_id, tv, package_id, provider_id, decoder_id, dvr_id, payment_id, ref_id, ref_list_id, sup_id, sup_ope_id, service_id, advertising_id, type_order_id, origen_id, ch_hd, ch_dvr, ch_hbo, ch_cinemax, ch_starz, ch_showtime, gif_ent, gif_choice, gif_xtra, operator_id, created_at) VALUES ("'.$ticket.'", "'.$info['client'].'", "'.$info['tv'].'", "'.$info['packages'].'", "'.$info['providers'].'", "'.$info['decoders'].'", "'.$info['dvrs'].'", "'.$info['payment'].'", "'.$info['referred'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$info['service'].'", "'.$info['adv_id'].'", "'.$info['order_type'].'", "'.$info['origen'].'", "'.$info['hd'].'", "'.$info['dvr'].'", "'.$info['hbo'].'", "'.$info['cinemax'].'", "'.$info['starz'].'", "'.$info['showtime'].'", "'.$info['gif_ent'].'", "'.$info['gif_cho'].'", "'.$info['gif_xtra'].'", "'.$user.'", "'.$date.'") ';
		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	
	}

	public static function UpdateServiceCoord($iData)
	{
		$query  =   'UPDATE cp_service_pr_tv SET tv="'.$iData['tv'].'", package_id="'.$iData['package'].'", provider_id="'.$iData['provider'].'", decoder_id="'.$iData['decoders'].'", dvr_id="'.$iData['dvrs'].'", payment_id="'.$iData['payment'].'", ch_hd="'.$iData['hd'].'", ch_dvr="'.$iData['dvr'].'", ch_hbo="'.$iData['hbo'].'", ch_cinemax="'.$iData['cinemax'].'", ch_starz="'.$iData['starz'].'", ch_showtime="'.$iData['showtime'].'", gif_ent="'.$iData['gift_ent'].'", gif_choice="'.$iData['gift_cho'].'", gif_xtra="'.$iData['gift_ext'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	}
}