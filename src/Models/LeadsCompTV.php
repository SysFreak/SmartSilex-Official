<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\LeadsCompTV;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadsCompTV extends Model 
{

	static $_table 		= 'cp_coord_comp_data_tv';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'signature_date', 'signature',  'instalation_date', 'suscriptor', 'contract', 'operator_id', 'created_at');

/////////////////////////////////////////////////////////////////////////////////////////////////

	public static function GetInfoClient($client)
	{
		$query 	=	'SELECT ticket, client_id, signature_date, signature,  instalation_date, suscriptor, contract FROM cp_coord_comp_data_tv WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		$result =	DBSmart::DBQuery($query);

		if($result <> false)
		{
			return [
				'ticket'	=>	strtoupper($result['ticket']),
				'client'	=>	strtoupper($result['client_id']),
				'sign_d'	=>	strtoupper($result['signature_date']),
				'signat'	=>	strtoupper($result['signature']),
				'inst_d'	=>	strtoupper($result['instalation_date']),
				'suscrp'	=>	strtoupper($result['suscriptor']),
				'contra'	=>	strtoupper($result['contract'])
			];
		}else{
			return ['ticket' =>	'','client' => '','sign_d' => '','signat' => '','inst_d' => '','suscrp' => '','contra' => ''];
		}
	}

/////////////////////////////////////////////////////////////////////////////////////////////////

	public static function InsertInfoClient($params)
	{	
		$query 	=	'INSERT INTO cp_coord_comp_data_tv(ticket, client_id, signature_date, signature, instalation_date, suscriptor, contract, operator_id, created_at) VALUES ("'.$params['ticket'].'","'.$params['client'].'","'.$params['sign_d'].'","'.$params['sign'].'","'.$params['instal'].'","'.$params['suscri'].'","'.$params['contra'].'","'.$params['operat'].'","'.date('Y-m-d H:m:s').'")';

		$leads 	=	DBSmart::DataExecute($query);

		return ($leads <> false ) ? true : false;


	}

/////////////////////////////////////////////////////////////////////////////////////////////////

}