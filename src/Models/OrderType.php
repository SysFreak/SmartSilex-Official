<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\OrderType;

use App\Lib\Config;
use App\Lib\DBSmart;

class OrderType extends Model 
{
	static $_table 		= 'data_type_order';

	Public $_fillable 	= array('name', 'status_id', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function GetOrderType()
	{
		$query 	= 	'SELECT id, name, status_id, created_at FROM data_type_order ORDER BY id ASC';

		$ord 	=	DBSmart::DBQueryAll($query);
		
		if($ord <> false)
		{
			foreach ($ord as $k => $val) 
			{
				$order[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=> 	$val['name'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id'],
					'created_at'	=>	$val['created_at']
				);
			}
	        
	        return $order;

		}else{ return false; }
	
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function GetOrderTypeStatus()
	{
		$query 	= 	'SELECT id, name, status_id, created_at FROM data_type_order WHERE status_id = "1" ORDER BY id ASC';

		$ord 	=	DBSmart::DBQueryAll($query);
		
		if($ord <> false)
		{
			foreach ($ord as $k => $val) 
			{
				$order[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=> 	$val['name'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id'],
					'created_at'	=>	$val['created_at']
				);
			}
	        
	        return $order;

		}else{ return false; }
	
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function GetOrderTypeById($id)
	{
		$query 	= 	'SELECT id, name, status_id, created_at FROM data_type_order WHERE id = "'.$id.'"';

		$ord 	=	DBSmart::DBQuery($query);

		if($ord <> false)
		{
			return [
				'id' 	 		=> 	$ord['id'], 
				'name' 	 		=> 	$ord['name'],
				'status' 		=> 	Status::GetStatusById($ord['status_id'])['name'],
				'status_id'		=>	$ord['status_id'],
				'created_at'	=>	$ord['created_at']
			];      

		}else{ return false; }
	
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

}