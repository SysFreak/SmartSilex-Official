<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CoordPreTmp extends Model
{

	static $_table 		= 'cp_coord_pre_tmp';

	Public $_fillable 	= array('id', 'client_id', 'ticket', 'coord_id', 'reason_id', 'pre_date', 'pre_disp', 'sig_time_id', 'sig_reason_id', 'created_by', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SavePreCoord($info)
	{
		
		$date	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_coord_pre_tmp(client_id, ticket, coord_id, reason_id, pre_date, pre_disp, sig_time_id, sig_reason_id, created_by, created_at) VALUES ("'.$info['client'].'", "'.$info['ticket'].'", "'.$info['coord'].'", "'.$info['reason'].'", "'.$info['p_date'].'", "'.$info['p_disp'].'", "'.$info['sig_time'].'", "'.$info['sig_reason'].'", "'.$info['created'].'", "'.$date.'") ';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

}