<?php
namespace App\Models;

use Model;

use App\Models\Country;
use App\Models\Score;
use App\Models\Service;
use App\Models\Status;

use App\Lib\Config;
use App\Lib\DBSmart;

use PDO;

class Score extends Model 
{

	static $_table 		= 'data_score';

	Public $_fillable 	= array('id', 'name', 'service_id', 'country_id', 'status_id', 'created_at');


//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetScore()
	{
		$query	=	'SELECT * FROM data_score ORDER BY id ASC';
		$sco 	=	DBSmart::DBQueryAll($query);
		
		$scores = array();

		if($sco <> false)
		{
			foreach ($sco as $k => $val) 
			{
				$scores[$k] = array(
					'id'			=>	$val['id'],
					'name'			=>	$val['name'],
					'country_id'	=>	$val['country_id'],
					'country'		=>	Country::GetCountryById($val['country_id'])['name'],
					'status_id'		=>	$val['status_id'],
					'status'		=>	Status::GetStatusById($val['status_id'])['name'],
					'service_id'	=>	$val['service_id'],
					'service'		=>	Service::ServiceById($val['service_id'])['name'],
				);
			}
	        return $scores;

		}else{	return false; }
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetScoreId($id)
	{
		$query	=	'SELECT * FROM data_score WHERE id = "'.$id.'" ORDER BY id ASC';
		$sco 	=	DBSmart::DBQuery($query);

		if($sco <> false)
		{
			return [
				'id' 			=> 	$sco['id'],
				'name' 			=> 	$sco['name'],
				'country_id'	=>	$sco['country_id'],
				'country'		=>	Country::GetCountryById($sco['country_id'])['name'],
				'status_id'		=>	$sco['status_id'],
				'status'		=>	Status::GetStatusById($sco['status_id'])['name'],
				'service_id'	=>	$sco['service_id'],
				'service'		=>	Service::ServiceById($sco['service_id'])['name'],
				'created'		=>	$sco['created_at']
			];

		}else{ 	return false;	}
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function GetScoreByType($type, $info)
	{
		switch ($type) 
		{
			case 'service':
				$query	=	'SELECT * FROM data_score WHERE service_id = "'.$info.'" AND status_id = "1" ORDER BY id ASC';
				$sco 	=	DBSmart::DBQueryAll($query);
				break;
			
			case 'country':
				$query	=	'SELECT * FROM data_score WHERE country_id = "'.$info.'" AND status_id = "1" ORDER BY id ASC';
				$sco 	=	DBSmart::DBQueryAll($query);
				break;

			case 'id':
				$query	=	'SELECT * FROM data_score WHERE id = "'.$info.'" AND status_id = "1" ORDER BY id ASC';
				$sco 	=	DBSmart::DBQueryAll($query);
				break;
		}

		if($sco <> false)
		{
			foreach ($sco as $k => $val) 
			{
				$sc[$k]	=	[
					'id' 		=> 	$val['id'],
					'name' 		=> 	$val['name'],
					'service'	=>	$val['service_id'],
					'country'	=>	$val['country_id'],
					'status'	=>	$val['status_id'],
					'created'	=>	$val['created_at']
				];
			}

			return $sc;

		}else{ return false; }
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveService($info)
	{

		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_sc']));

		if($info['type_sc'] == 'new')
		{
			$query 	=	'INSERT INTO data_score(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'","'.$info['service_id_sc'].'","'.$info['country_id_sc'].'","'.$info['status_id_sc'].'","'.$date.'")';
			$dep 	=	DBSmart::DataExecuteLastID($query);

			return ($dep <> false ) ? $dep : false;
		}
		elseif ($info['type_sc'] == 'edit') 
		{
			$query 	=	'UPDATE data_score SET name="'.$name.'",service_id="'.$info['service_id_sc'].'",country_id="'.$info['country_id_sc'].'",status_id="'.$info['status_id_sc'].'" WHERE id = "'.$info['id_sc'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}


//////////////////////////////////////////////////////////////////////////////////////////

}


		