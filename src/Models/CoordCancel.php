<?php
namespace App\Models;

use Model;

use App\Models\Coordination;
use App\Models\CoordCancel;
use App\Models\Status;
use App\Models\Country;
use App\Models\Service;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CoordCancel extends Model 
{
	static $_table 		= 'data_cancel_coord_objection';

	Public $_fillable 	= array('id', 'name', 'country_id', 'status_id', 'created_at');

/////////////////////////////////////////////////////////////////////////

	public static function SaveObjCancel($info)
	{

		$date 	=	date('Y-m-d H:i:s', time());

		$name 	= 	strtoupper($_replace->deleteTilde($info['name']));

		$query 	=	'INSERT INTO data_cancel_coord_objection(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country'].'", "'.$info['status'].'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false) ? true : false;
	}

/////////////////////////////////////////////////////////////////////////

	public static function GetCoordCancels()
	{
		$query	=	'SELECT id, name, country_id, status_id, created_at FROM data_cancel_coord_objection ORDER BY id ASC';
		$apro 	=	DBSmart::DBQueryAll($query);

		if($apro <> false)
		{
			foreach ($apro as $k => $sc) 
			{
				$cancel[$k]	=	[
					'id'			=>	$sc['id'],
					'name'			=>	$sc['name'],
					'country_id'	=>	$sc['country_id'],
					'status_id'		=>	$sc['status_id'],
					'country'		=>	Country::GetCountryById($sc['country_id'])['name'],
					'status'		=>	Status::GetStatusById($sc['status_id'])['name'],
					'created'		=>	$sc['created_at']
				];
			}
			return $cancel;
		}else{
			return false;
		}


	}
/////////////////////////////////////////////////////////////////////////

	public static function GetCoordCancelById($id)
	{
	
		$query	=	'SELECT id, name, country_id_status, created_at FROM data_cancel_coord_objection WHERE id = "'.$id.'"';
		$sco 	=	DBSmart::DBQuery($query);

		if($sco)
		{
			return [
				'id' 			=> 	$sco['id'],
				'name' 			=> 	$sco['name'],
				'country_id'	=>	$sco['country_id'],
				'status_id'		=>	$sco['status_id'],
				'country'		=>	Country::GetCountryById($sco['country_id'])['name'],
				'status'		=>	Status::GetStatusById($sco['status_id'])['name'],
				'created'		=>	$sco['created_at']
			];

		}else{	return false;	}
	}

//////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveService($info)
	{

		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_can']));

		if($info['type_can'] == 'new')
		{
			$query 	=	'INSERT INTO data_cancel_coord_objection(name, country_id, status_id, created_at) VALUES ("'.$name.'","'.$info['country_id_can'].'","'.$info['status_id_can'].'","'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? $dep['last_id'] : false;
		}
		elseif ($info['type_can'] == 'edit') 
		{
			$query 	=	'UPDATE data_cancel_coord_objection SET name="'.$name.'", country_id="'.$info['country_id_can'].'", status_id="'.$info['status_id_can'].'" WHERE id = "'.$info['id_can'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}

/////////////////////////////////////////////////////////////////////////

}