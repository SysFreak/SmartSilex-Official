<?php
namespace App\Models;

use Model;
use App\Models\Auth;
use App\Models\Departament;
use App\Models\Status;
use App\Models\Role;
use App\Models\Team;
use App\Models\User;
use App\Models\UserRol;

use App\Lib\DBSmart;
use App\Lib\Config;

use PDO;

class UserRol extends Model 
{

 	static $_table = 'user_rols';
	
	protected $_fillable = array('id', 'rol_id', 'calendar', 'operator', 'leads', 'leaders', 'marketing', 'creative', 'data_entry', 'approvals', 'coordinations', 'reporting', 'config', 'ticket', 'support', 'collection', 'contracts', 'created_at');  

    public static function GetRolsByRol($id)
	{
		$query 		=	'SELECT * FROM user_rols WHERE rol_id = "'.$id.'"';
		$rol        =   DBSmart::DBQueryAll($query);

		if($rol <> false)
		{
			foreach ($rol as $r => $ro) 
			{
				$rols[$r]	=	[
					'rol_id'		=>	$ro['rol_id'],
					'rol'			=>	Role::GetRolById($ro['rol_id'])['name'],
					'calendar'		=>	$ro['calendar'],
					'operator'		=>	$ro['operator'],
					'leads'			=>	$ro['leads'],
					'leaders'		=>	$ro['leaders'],
					'marketing'		=>	$ro['marketing'],
					'creative'		=>	$ro['creative'],
					'data_entry'	=>	$ro['data_entry'],
					'approvals'		=>	$ro['approvals'],
					'coordinations'	=>	$ro['coordinations'],
					'reporting'		=>	$ro['reporting'],
					'config'		=>	$ro['config'],
					'ticket'		=>	$ro['ticket'],
					'support'		=>	$ro['support'],
					'collection'	=>	$ro['collection'],
					'contracts'		=>	$ro['contracts']
				];
			}

			return $rols;

		}else{ return false; }
	
	}

	public static function SaveUserRol($inf)
	{
		$date = date('Y-m-d H:i:s', time());

		if($inf['type'] == 'new')
		{
			$query 	=	'INSERT INTO user_rols(rol_id, calendar, operator, leads, leaders, marketing, creative, data_entry, approvals, coordinations, reporting, config, ticket, support, collection, contracts, created_at) VALUES ("'.$inf['rol'].'","'.$inf['calendar'].'","'.$inf['operator'].'","'.$inf['lead'].'","'.$inf['leader'].'","'.$inf['marketing'].'","'.$inf['creative'].'","'.$inf['data_entry'].'","'.$inf['approvals'].'","'.$inf['coordinations'].'","'.$inf['reporting'].'","'.$inf['config'].'","'.$inf['ticket'].'", "'.$inf['support'].'", "'.$inf['collection'].'", "'.$inf['contracts'].'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}

		elseif ($inf['type'] == 'edit') 
		{
			$query 	= 'UPDATE user_rols SET rol_id="'.$inf['rol'].'", calendar="'.$inf['calendar'].'", operator="'.$inf['operator'].'", leads="'.$inf['lead'].'", leaders="'.$inf['leader'].'", marketing="'.$inf['marketing'].'", creative="'.$inf['creative'].'", data_entry="'.$inf['data_entry'].'", approvals="'.$inf['approvals'].'", coordinations="'.$inf['coordinations'].'", reporting="'.$inf['reporting'].'", config="'.$inf['config'].'", ticket="'.$inf['ticket'].'", support="'.$inf['support'].'", collection="'.$inf['collection'].'", contracts="'.$inf['contracts'].'", created_at="'.$date.'" WHERE id = "'.$inf['id_r'].'"';


			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}
