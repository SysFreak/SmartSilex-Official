<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CoordPackageTmp extends Model
{

	static $_table 		= 'cp_coord_serv_edit';

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SaveEditCoord($info)
	{

		$query 	=	'INSERT INTO cp_coord_serv_edit(ticket, client_id, service_id, user_id, created_at) VALUES ("'.$info['ticket'].'", "'.$info['client'].'", "'.$info['service'].'", "'.$info['operator'].'", "'.$info['date'].'")';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

}