<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Town;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Town extends Model 
{

	static $_table 		= 'data_town';

	Public $_fillable 	= array('name', 'signals', 'country_id', 'status_id', 'created_at');

	public static function GetTown()
	{

		$query 	= 	'SELECT id, name, signals, country_id, status_id FROM data_town ORDER BY id ASC';
		$tow 	=	DBSmart::DBQueryAll($query);

		$towns = array();

		if($tow <> false)
		{
			foreach ($tow as $k => $val) 
			{
				$towns[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=> 	$val['name'],
					'signal' 		=> 	$val['signals'],
					'country'	 	=> 	Country::GetCountryById($val['country_id'])['name'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id'],
					'country_id'	=>	$val['country_id'],
				);
			}
	        
	        return $towns;
		}else{ 	return false;	}
	}

	public static function GetTownById($id)
	{
		$query 	= 	'SELECT id, name, signals, country_id, status_id FROM data_town WHERE id = "'.$id.'"';
		$tow 	=	DBSmart::DBQuery($query);

		if($tow <> false)
		{
			return array(
				'id' => $tow['id'], 'name' => $tow['name'], 'signals' => $tow['signals'], 'country' => $tow['country_id'], 'status' => $tow['status_id']
			);

		}else{ 	return false;	}
	}

	public static function GetTownByStatus()
	{
		$query 	= 	'SELECT id, name, signals, country_id, status_id FROM data_town WHERE status_id = "1"';
		$tow 	=	DBSmart::DBQuery($query);

		if($tow <> false)
		{
			return array(
				'id' => $tow['id'], 'name' => $tow['name'], 'signals' => $tow['signals'], 'country' => $tow['country_id'], 'status' => $tow['status_id']
			);

		}else{ 	return false;	}
	}

	public static function GetTownByCountry($id)
	{

		$query 	= 	'SELECT id, name FROM data_town WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$to 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($to)
		{
			foreach ($to as $k => $val) 
			{ $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';}

			return $html;

		}else{

			return $html;

		}
	}

	public static function GetTownCountry($id)
	{

		$query 	= 	'SELECT id, name FROM data_town WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$to 	=	DBSmart::DBQueryAll($query);

		return ($to <> false) ? $to : false;
	}

	public static function GetTownSingleName($info)
	{

		$query 	= 	'SELECT id, name, signals, country_id, status_id FROM data_town WHERE name = "'.$id.'"';
		$tow 	=	DBSmart::DBQuery($query);

		if($tow <> false)
		{
			return array('id' => $tow['id'], 'name' => $tow['name'], 'signals' => $tow['signals'], 'country' => $tow['country_id'], 'status' => $tow['status_id']);
			
		}else{
			return false;
		}
	}

	public static function GetTownByName($info)
	{

		$query 	= 	'SELECT id, name, signals, country_id, status_id FROM data_town WHERE name = "'.$info['name_town'].'" AND signals = "'.$info['signal_town'].'" AND country_id = "'.$info['country_id_town'].'" AND status_id = "'.$info['status_id_town'].'"';
		$tow 	=	DBSmart::DBQuery($query);

		if($tow <> false)
		{
			return array('id' => $tow['id'], 'name' => $tow['name'], 'signals' => $tow['signals'], 'country' => $tow['country_id'], 'status' => $tow['status_id']);
		}else{
			return false;
		}
	}

	public static function SaveTown($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_town']));

		if($info['type_town'] == 'new')
		{
			$query 	= 	'INSERT INTO data_town(name, signals, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['signal_town'].'", "'.$info['country_id_town'].'", "'.$info['status_id_town'].'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;

		}
		elseif ($info['type_town'] == 'edit') 
		{
			$query 	=	'UPDATE data_town SET name="'.$name.'", signals="'.$info['signal_town'].'", country_id="'.$info['country_id_town'].'", status_id="'.$info['status_id_town'].'" WHERE id = "'.$info['id_town'].'"';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}

}


		