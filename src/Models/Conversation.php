<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Conversation;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Conversation extends Model {

	static $_table 		= 'data_mkt_conversation';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id');

	public static function GetPost()
	{

		$query 	= 	'SELECT * FROM data_mkt_conversation ORDER BY id ASC';
		$post 	=	DBSmart::DBQueryAll($query);
		
		if($post <> false)
		{
			foreach ($post as $k => $val) 
			{
				$Post[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=>	$val['name'],
					'service'  		=>	Service::ServiceById($val['service_id'])['name'],
					'service_di'  	=>	$val['service_id'],
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=>	$val['country_id'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id']
				);
			}
	        
	        return $Post;

		}else{ return false; }
	}

	public static function GetPostById($id)
	{

		$query 	= 	'SELECT * FROM data_mkt_conversation WHERE id = "'.$id.'" ORDER BY id ASC';
		$post 	=	DBSmart::DBQuery($query);
		
		if($post <> false)
		{
			return array(
			 	'id'           => $post['id'],
			 	'name'         => $post['name'],
			 	'service'      => $post['service_id'],
			 	'country'      => $post['country_id'],
			 	'status'       => $post['status_id']
			);

		}else{ return false; }

	}

	public static function GetPostByCountryID($id)
	{

		$query 	= 	'SELECT id, name, status_id FROM data_mkt_conversation WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$post 	=	DBSmart::DBQueryAll($query);
		
		if($post <> false)
		{
			foreach ($post as $k => $val) 
			{
				$conv[$k] = [
					'id'		=>	$val['id'],
					'name'		=>	$val['name'],
					'status'	=>	$val['status_id']
				];
			}
			return $conv;

		}else{ return false; }
		
	}

	public static function GetPostByCountry($id)
	{
		$query 	= 	'SELECT id, name FROM data_mkt_conversation WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$post 	=	DBSmart::DBQueryAll($query);

		$html = "";

		if($post <>false)
		{
			foreach ($post as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			return $html;

		}else{ return $html; }
	}

	public static function GetPostByName($info)
	{

		$query 	= 	'SELECT * FROM data_mkt_conversation WHERE name = "'.$info['name_po'].'" AND service_id = "'.$info['service_id_po'].'" ORDER BY id ASC';
		$post 	=	DBSmart::DBQuery($query);

		if($post <> false )
		{
			return array('id' => $post['id'], 'name' => $post['name'], 'service_id' => $post['service_id'], 'country_id' => $post['country_id'],'status' => $post['status_id']);
			
		}else{	return false; 	}

	}

	public static function SavePost($info)
	{
		$date 		= date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_po']));

		if($info['type_po'] == 'new')
		{
			$query 	= 	'INSERT INTO data_mkt_conversation(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_po'].'", "'.$info['country_id_po'].'", "'.$info['status_id_po'].'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['type_po'] == 'edit') 
		{
			$query 	=	'UPDATE data_mkt_conversation SET name="'.$name.'", service_id="'.$info['service_id_po'].'", country_id="'.$info['country_id_po'].'", status_id="'.$info['status_id_po'].'" WHERE id = "'.$info['id_po'].'"';
			
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}