<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Objetive;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Objetive extends Model {

	static $_table 		= 'data_mkt_objetive';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id');

	public static function GetObjetive()
	{
		
		$query 	= 	'SELECT * FROM data_mkt_objetive ORDER BY id ASC';
		$obj 	=	DBSmart::DBQueryAll($query);
		
		if($obj <> false)
		{
			foreach ($obj as $k => $val) 
			{
				$objectives[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=>	$val['name'],
					'service'  		=>	Service::ServiceById($val['service_id'])['name'],
					'service_di'  	=>	$val['service_id'],
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=>	$val['country_id'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id']
				);
			}
	        
	        return $objectives;

		}else{ return false; }
	}

	public static function GetObjetiveById($id)
	{
		
		$query 	= 	'SELECT * FROM data_mkt_objetive WHERE id = "'.$id.'" ORDER BY id ASC';
		$med 	=	DBSmart::DBQuery($query);
		
		if($med <> false)
		{
			return array(
			 	'id'           => $med['id'],
			 	'name'         => $med['name'],
			 	'service'      => $med['service_id'],
			 	'country'      => $med['country_id'],
			 	'status'       => $med['status_id']
			);

		}else{ return false; }
	}

	public static function GetObjectiveByCountryID($id)
	{

		$query 	= 	'SELECT id, name, status_id FROM data_mkt_objetive WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$obj 	=	DBSmart::DBQueryAll($query);

		if($obj <> false)
		{
			foreach ($obj as $k => $val) 
			{
				$objectivos[$k] = [
					'id'		=>	$val['id'],
					'name'		=>	$val['name'],
					'status'	=>	$val['status_id']
				];
			}
			return $objectivos;
		}else{ return false; }
		
	}

	public static function GetObjectiveByCountry($id)
	{

		$query 	= 	'SELECT id, name FROM data_mkt_objetive WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$obj 	=	DBSmart::DBQueryAll($query);

		$html = "";

		if($obj)
		{
			foreach ($obj as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			return $html;

		}else{ return $html; }
	}

	public static function GetObjetiveByName($info)
	{

		$query 	= 	'SELECT * FROM data_mkt_objetive WHERE name = "'.$info['name_ob'].'" AND service_id = "'.$info['service_id_ob'].'" ORDER BY id ASC';
		$obj 	=	DBSmart::DBQuery($query);

		if($obj)
		{
			return array('id' => $obj->id, 'name' => $obj->name, 'service_id' => $obj->service_id, 'country_id' => $obj->country_id, 'status' => $obj->status_id);
		}else{
			return false;
		}
	}

	public static function SaveObjetive($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_ob']));

		if($info['type_ob'] == 'new')
		{

			$query 	= 	'INSERT INTO data_mkt_objetive(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_ob'].'", "'.$info['country_id_ob'].'", "'.$info['status_id_ob'].'", NOW())';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['type_ob'] == 'edit') 
		{
			$query 	=	'UPDATE data_mkt_objetive SET name="'.$name.'", service_id="'.$info['service_id_ob'].'", country_id="'.$info['country_id_ob'].'", status_id="'.$info['status_id_ob'].'" WHERE id = "'.$info['id_ob'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}

