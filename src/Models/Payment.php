<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Payment extends Model {

	static $_table 		= 'data_payment';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id');

	public static function GetPayment()
	{

		$query 	= 	'SELECT * FROM data_payment ORDER BY id ASC';
		$pay 	=	DBSmart::DBQueryAll($query);

		$payment = array();

		if($pay <> false)
		{
			foreach ($pay as $k => $val) 
			{
				$payment[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'service'		=> Service::ServiceById($val['service_id'])['name'],
					'service_id'	=> $val['service_id'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $payment;

		}else{ 	return false;	}
	}

	public static function GetPaymentById($id)
	{

		$query 	= 	'SELECT * FROM data_payment WHERE id = "'.$id.'"';
		$pay 	=	DBSmart::DBQuery($query);

		if($pay <> false)
		{
			return array('id' => $pay['id'], 'name' => $pay['name'], 'service' => $pay['service_id'], 'status' => $pay['status_id'], 'country' => $pay['country_id']);

		}else{ 	return false;	}
	}

	public static function GetPaymentByName($info)
	{

		$query 	= 	'SELECT * FROM data_payment WHERE name = "'.$info.'"';
		$pay 	=	DBSmart::DBQuery($query);

		if($pay <> false)
		{
			return array('id' => $pay['id'], 'name' => $pay['name'], 'service' => $pay['service_id'], 'status' => $pay['status_id'], 'country' => $pay['country_id']);

		}else{ 	return false;	}
	}

	public static function GetFirst($service, $country)
   	{
		$query 	= 	'SELECT id, name, service_id, country_id, status_id FROM data_payment WHERE country_id = "'.$country.'" AND service_id = "'.$service.'" ORDER BY id ASC LIMIT 1';
		$pay 	=	DBSmart::DBQuery($query);

		if($pay <> false)
		{
			return 	[
				'id'		=>	$pay['id'],
				'name'		=>	$pay['name'],
				'status'	=>	Status::GetStatusById($pay['status_id'])['name'],
				'status_id'	=>	$pay['status_id']
			];

		}else{

			return 	[
				'id'		=>	1,
				'name'		=>	"SIN METODO DE PAGO",
				'status_id'	=>	1
			];
		}
   	
   	}


	public static function SavePayment($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_pa']));

		if($info['type_pa'] == 'new')
		{
			$query 	= 	'INSERT INTO data_payment(name, service_id, status_id, country_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_pa'].'", "'.$info['country_id_pa'].'", "'.$info['status_id_pa'].'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;

		}
		elseif ($info['type_pa'] == 'edit') 
		{
			$query 	=	'UPDATE data_payment SET name="'.$name.'", service_id="'.$info['service_id_pa'].'", country_id="'.$info['country_id_pa'].'", status_id="'.$info['status_id_pa'].'" WHERE id = "'.$info['id_pa'].'"';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
	}

}

		