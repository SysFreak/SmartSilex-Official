<?php

namespace App\Models;

require '../vendor/autoload.php';

use App\Models\User;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;

class StatusLead extends Model {

	static $_table 		= 'cp_status_leads';

	Public $_fillable 	= array('id', 'client_id', 'user_id', 'status_id', 'online', 'offline');

	public static function GetStatusOffline($client)
	{
		
		$query 	= 	'SELECT * FROM cp_status_leads WHERE client_id = "'.$client.'" ORDER BY id DESC';
		$serv 	=	DBSmart::DBQuery($query);

		if($leads == false)
		{
			return false;

		}else{

			if($leads->status == "0")
			{
				return  false;

			}else{
				return array('username'	=>	$leads['username'], 'online'	=>	$leads['online'] );
			}
		}
	
	}

	public static function InsertStatus($client, $status, $user)
	{
		$date 	=	date('Y-m-d H:i:s', time()); 
		$query	=	'INSERT INTO cp_status_leads(client_id, user_id, status_id, online, offline) VALUES ("'.$client.'", "'.$user.'", "'.$status.'", "'.$date.'", "")';
		$leads 	=	DBSmart::DataExecute($query);

		return ($leads <> false ) ? true : false;

	}

	public static function UpdateStatus($client, $status, $user)
	{
		$date 	=	date('Y-m-d H:i:s', time());

		$query 	=	'UPDATE cp_status_leads SET offline="'.$date.'" WHERE client_id = "'.$client.'" AND status_id = "'.$status.'" AND user_id = "'.$user.'" AND offline = ""';
		$leads 	=	DBSmart::DataExecute($query);

		return ($leads <> false ) ? true : false;

	}

	public static function StatusLead($client)
	{

		$query 	= 	'SELECT * FROM cp_status_leads WHERE client_id = "'.$client.'" ORDER BY id DESC';
		$lead 	=	DBSmart::DBQuery($query);

		if($lead <> false)
		{
			
			if( ($lead['status_id'] == 1 AND $lead['online'] == "") OR ($lead['status_id'] == 2 AND $lead['offline'] <> '') ) 
			{
				return [
					'status'	=>	'offline',
					'client'	=>	$lead['client_id'],
					'user'		=>	$lead['user_id'],
					'ope'		=>	User::GetUserById($lead['user_id'])['username'],
					'online'	=>	$lead['online'],
					'offline'	=>	$lead['offline'],
				];

			}
			elseif( ($lead['status_id'] == 2 AND $lead['online'] <> "") OR ($lead['status_id'] == 2 AND $lead['offline'] == '') )
			{
				return [
					'status'	=>	'online',
					'client'	=>	$lead['client_id'],
					'user'		=>	$lead['user_id'],
					'ope'		=>	User::GetUserById($lead['user_id'])['username'],
					'online'	=>	$lead['online'],
					'offline'	=>	$lead['offline'],
				];
			}

		}else{ 

			return [ 
				'status' => 'offline', 'client' => $client, 'user' => '', 'ope' =>	'', 'online'	=>	'', 'offline' => ''
			];
		}
	
	}
	
	public static function StatusOperator($ope)
	{

		$query 		=	'SELECT * FROM cp_status_leads WHERE user_id = "'.$ope.'" AND status_id = "2" AND offline = "" ORDER BY id DESC';
		$status 	=	DBSmart::DBQuery($query);

		if($status <> false)
		{

			return [ 
				'status' 	=> 	'online',
				'client'	=>	$status['client_id'],
				'user'		=>	$status['user_id'],
				'ope'		=>	User::GetUserById($status['user_id'])['username'],
				'online'	=>	$status['online'],
				'offline'	=>	$status['offline'],
			];

		}else{ 

			return [ 
				'status' => 'offline', 'client' => '', 'user' => $ope, 'ope' =>	User::GetUserById($ope)['username'], 'online'	=>	'', 'offline' => ''
			];
		}
	
	}

	public static function StatusOpe($ope)
	{

		$query 		=	'SELECT client_id, status_id, user_id FROM cp_status_leads WHERE user_id = "'.$ope.'" ORDER BY id DESC';
		$SOpera 	=	DBSmart::DBQuery($query);

		return ($SOpera <> false ) ? $SOpera : false;
	
	}

	public static function OnlineStatus($iData)
	{
		$query	=	'INSERT INTO cp_status_leads(client_id, user_id, status_id, online, offline) VALUES ("'.$iData['client'].'", "'.$iData['user'].'", "'.$iData['status'].'", NOW(), "")';
		$leads 	=	DBSmart::DataExecute($query);

		return ($leads <> false ) ? true : false;
	}

	public static function OfflineStatus($iData)
	{
		$query 	=	'SELECT id FROM cp_status_leads WHERE client_id="'.$iData['client'].'" AND status_id = "2" AND offline = ""';
		$lead 	=	DBSmart::DBQuery($query);

		$query 	=	'UPDATE cp_status_leads SET offline = NOW(), status_id = "'.$iData['status'].'" WHERE id="'.$lead['id'].'"';
		$leads 	=	DBSmart::DataExecute($query);

		return ($leads <> false ) ? true : false;
	}


}