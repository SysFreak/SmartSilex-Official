<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Service;
use App\Models\Country;
use App\Models\ServiceIntPrTmp;
use App\Models\LeadsProvider;

use App\Lib\Config;
use App\Lib\DBSmart;


class ServiceIntPrTmp extends Model 
{

	static $_table = 'cp_service_pr_int_tmp';
	
	Public $_fillable = array('client_id', 'int_res_id', 'pho_res_id', 'int_com_id', 'pho_com_id', 'ref_id', 'ref_list_id', 'sup_id', 'sup_ope_id', 'payment_id', 'service_id', 'type_order_id', 'origen_id', 'owen_id', 'assit_id', 'created_at');

	public static function SaveService($info)
	{	
		$date = date('Y-m-d H:i:s', time());

		if($info['sup'] == 0)
		{	$user1 = $info['operator']; $user2 = $info['operator']; }
		else
		{	$user1 = $info['sup_list']; $user2 = $info['operator']; }

		$user =	($info['sup'] == 0) ? $info['operator'] : $info['sup_list'];

		$query 	=	'INSERT INTO cp_service_pr_int_tmp(client_id, int_res_id, pho_res_id, int_com_id, pho_com_id, ref_id, ref_list_id, sup_id, sup_ope_id, payment_id, service_id, type_order_id, origen_id, owen_id, assit_id, created_at) VALUES ("'.$info['client'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['ref'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$info['payment'].'", "'.$info['service'].'", "'.$info['order_type'].'", "'.$info['origen'].'", "'.$user1.'", "'.$user2.'", "'.$date.'")';

		$serv  	=	DBSmart::DataExecute($query);
		
		return ($serv <> false) ? true : false;
	
	}

	public static function ServiceInt($client)
	{

		$query 	=	'SELECT client_id, int_res_id, pho_res_id, int_com_id, pho_com_id, ref_id, ref_list_id, sup_id, sup_ope_id, payment_id, service_id, type_order_id, origen_id, owen_id, created_at FROM cp_service_pr_int_tmp WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'status'		=>	true,
			 	'client' 		=>	($serv['client_id'] <> '')  	? $serv['client_id'] 		: '',
			 	'int_res' 		=>	($serv['int_res_id'] <> '') 	? $serv['int_res_id'] 		: '1',
			 	'pho_res' 		=>	($serv['pho_res_id'] <> '') 	? $serv['pho_res_id'] 		: '1',
			 	'int_com' 		=>	($serv['int_com_id'] <> '') 	? $serv['int_com_id'] 		: '6',
			 	'pho_com' 		=>	($serv['pho_com_id'] <> '') 	? $serv['pho_com_id'] 		: '5',
			 	'ref' 			=>	($serv['ref_id'] <> '') 		? $serv['ref_id'] 			: '0',
			 	'ref_list' 		=>	($serv['ref_list_id'] <> '') 	? $serv['ref_list_id'] 		: '0',
			 	'sup' 			=>	($serv['sup_id'] <> '') 		? $serv['sup_id'] 			: '0',
			 	'sup_list' 		=>	($serv['sup_ope_id'] <> '') 	? $serv['sup_ope_id'] 		: '1',
			 	'payment' 		=>	($serv['payment_id'] <> '') 	? $serv['payment_id'] 		: '1',
			 	'service' 		=>	($serv['service_id'] <> '') 	? $serv['service_id'] 		: '1',
			 	'order_type'	=>	($serv['type_order_id'] <> '') 	? $serv['type_order_id']  	: '1',
				'o_type'		=>	($serv['type_order_id'] <> '') 	? OrderType::GetOrderTypeById($serv['type_order_id'])['name'] 		: '', 
			 	'origen'		=>	($serv['origen_id'] <> '') 		? $serv['origen_id']  		: '1',
				't_origen'		=>	($serv['origen_id'] <> '') 		? LeadsProvider::GetLeadsProviderById($serv['origen_id'])['name'] 	: '',
			 	'operator' 		=>	($serv['owen_id'] <> '') 		? $serv['owen_id'] 			: '',
			 	'created' 		=>	($serv['created_at'] <> '') 	? $serv['created_at'] 		: '',
			];

		}else{

		 	return [
		 		'status' => false, 'client' => $client, 'int_res' => "1", 'pho_res' => "1", 'int_com' => "6", 'pho_com' => "5", 'ref' => "0", 'ref_list' => "0", 'sup' 	=> "0", 'sup_list' => "1", 'payment' => "1", 'service' => "1", 'order_type' => "1", 'origen_id' => "1"
		 	];				
		}
	
	}

}