<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CoordContactTmp extends Model
{

	static $_table 		= 'cp_coord_contact_tmp';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'cont_id', 'reason_id', 'created_by', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SaveContact($info)
	{
		
		$date 	= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_coord_contact_tmp(ticket, client_id, cont_id, reason_id, created_by, created_at) VALUES ("'.$info['ticket'].'", "'.$info['client'].'", "'.$info['contact'].'", "'.$info['reason'].'", "'.$info['created'].'", "'.$date.'")';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;
	
	}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
///
///

}