<?php
namespace App\Models;

use Model;
use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\DBMikro;
use App\Lib\UnCypher;
use PDO;

class Mikrowisp extends Model 
{
	public static function ClientSearch($iData)
	{
		$query 	=	'SELECT t1.id, t1.nombre, t1.telefono, t1.movil, t1.Direccion_Fisica, t2.idcliente, t2.ip, t2.ipap, t2.coordenadas, t2.id AS contrato, t2.emisor, t3.plan FROM usuarios AS t1 INNER JOIN tblservicios AS t2 ON (t1.id = t2.idcliente) INNER JOIN perfiles AS t3 ON (t2.idperfil = t3.id) AND t1.id = "'.$iData['client'].'" LIMIT 1';
		
		$res 	=	DBMikro::DBQuery($query);
		
		return ($res <> false) ? $res : false;

	}

	public static function ClientPlans($iData)
	{
		$query 	=	'SELECT t1.id AS contrato, t1.emisor, t2.plan FROM tblservicios AS t1 INNER JOIN perfiles AS t2 ON (t1.idperfil = t2.id) AND t1.idcliente = "'.$iData['client'].'"';
		$res 	=	DBMikro::DBQueryAll($query);
		
		return ($res <> false) ? $res : false;
	}

	public static function ClientAP($iData)
	{
		$query 	=	'SELECT nombre FROM emisores WHERE id = "'.$iData['client'].'"';
		$res 	=	DBMikro::DBQuery($query);
		return 	($res <> false) ? $res['nombre'] : 'SIN AP ASOCIADO';
	}

	public static function GetPlan($iData)
	{
		$query 	=	'SELECT t2.id, t2.ip, t2.emisor, t1.plan FROM perfiles AS t1 INNER JOIN tblservicios AS t2 ON (t2.idperfil = t1.id and t2.id = "'.$iData['plan'].'") LIMIT 1';
		$res 	=	DBMikro::DBQuery($query);

		return 	($res <> false) ? $res : false;
	}

	public static function GetEmisor($iData)
	{
		$query 	=	'SELECT nombre FROM emisores where id = '.$iData['emisor'].' LIMIT 1';
		$res 	=	DBMikro::DBQuery($query);

		return 	($res <> false) ? $res : false;	
	}
}