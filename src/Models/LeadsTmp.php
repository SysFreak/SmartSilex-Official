<?php
namespace App\Models;

use Model;

use App\Models\LeadsTmp;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadsTmp extends Model 
{

	static $_table 		= 'cp_leads_tmp';

	Public $_fillable 	= array('client_id', 'referred_id', 'name', 'birthday', 'gender', 'age', 'repre_legal', 'type_acc', 'phone_main', 'phone_main_owner', 'phone_main_provider', 'phone_alt', 'phone_alt_owner', 'phone_alt_provider', 'phone_other', 'phone_other_owner', 'phone_other_provider', 'email_main', 'email_other', 'add_main', 'add_postal', 'coor_lati', 'coor_long', 'country_id', 'town_id', 'zip_id', 'h_type_id', 'h_roof_id', 'h_level_id', 'type', 'additional', 'created_at');

	public static function SaveDB($info, $type, $user)
	{
		$date 		= date('Y-m-d H:i:s', time());
		
		$_replace 	= new Config();

		$iData		=	[
			'client_id'				=> (isset($info['c_client'])) 		? strtoupper($_replace->deleteTilde($info['c_client']))			: "",
			'name' 					=> (isset($info['c_name'])) 		? strtoupper($_replace->deleteTilde($info['c_name'])) 			: "",
			'birthday' 				=> ($info['c_birthday'] <> '') 		? strtoupper($_replace->ChangeDate($info['c_birthday'])) 		: "",
			'gender' 				=> (isset($info['c_gender'])) 		? strtoupper($_replace->deleteTilde($info['c_gender'])) 		: "M",
			'age' 					=> (isset($info['c_age'])) 			? strtoupper($_replace->deleteTilde($info['c_age'])) 			: "0",
			'repre'					=> (isset($info['c_rep_legal'])) 	? strtoupper($_replace->deleteTilde($info['c_rep_legal'])) 		: "",
			'type_acc'				=> (isset($info['c_type_acc'])) 	? strtoupper($_replace->deleteTilde($info['c_type_acc'])) 		: "",
			'phone_main' 			=> (isset($info['c_phone'])) 		? strtoupper($_replace->deleteTilde($info['c_phone']))			: "",
			'phone_main_owner' 		=> (isset($info['c_phone_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_ow'])) 		: "",
			'phone_main_provider'	=> (isset($info['c_phone_pro'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_pro'])) 		: "1",
			
			'phone_alt' 			=> (isset($info['c_phone_a'])) 		? strtoupper($_replace->deleteTilde($info['c_phone_a'])) 		: "",
			'phone_alt_owner' 		=> (isset($info['c_phone_a_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_a_ow'])) 	: "",
			'phone_alt_provider'	=> (isset($info['c_phone_a_pro'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_a_pro'])) 	: "1",
			
			'phone_other' 			=> (isset($info['c_phone_o'])) 		? strtoupper($_replace->deleteTilde($info['c_phone_o'])) 		: "",
			'phone_other_owner' 	=> (isset($info['c_phone_o_ow'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_o_ow'])) 	: "",
			'phone_other_provider'	=> (isset($info['c_phone_o_pro'])) 	? strtoupper($_replace->deleteTilde($info['c_phone_o_pro'])) 	: "1",
			
			'email_main'			=> (isset($info['c_email'])) 		? strtoupper($_replace->deleteTilde($info['c_email'])) 			: "",
			'email_other'			=> (isset($info['c_email_o'])) 		? strtoupper($_replace->deleteTilde($info['c_email_o'])) 		: "",
			'add_main'				=> (isset($info['c_address'])) 		? strtoupper($_replace->deleteTilde($info['c_address'])) 		: "",
			'add_postal'			=> (isset($info['c_postal'])) 		? strtoupper($_replace->deleteTilde($info['c_postal'])) 		: "",
			'country_id'			=> (isset($info['c_country'])) 		? strtoupper($_replace->deleteTilde($info['c_country'])) 		: "",
			'coor_lati'				=> (isset($info['c_lati'])) 		? strtoupper($_replace->deleteTilde($info['c_lati'])) 			: "",
			'coor_long'				=> (isset($info['c_long'])) 		? strtoupper($_replace->deleteTilde($info['c_long'])) 			: "",
			'country_id'			=> (isset($info['c_country'])) 		? strtoupper($_replace->deleteTilde($info['c_country'])) 		: "",
			'town_id'				=> (isset($info['c_town'])) 		? strtoupper($_replace->deleteTilde($info['c_town'])) 			: "",
			'zip_id'				=> (isset($info['c_zip'])) 			? strtoupper($_replace->deleteTilde($info['c_zip'])) 			: "",
			'h_type_id'				=> (isset($info['c_type_house']))	? strtoupper($_replace->deleteTilde($info['c_type_house'])) 	: "",
			'h_roof_id'				=> (isset($info['c_roof']))			? strtoupper($_replace->deleteTilde($info['c_roof'])) 			: "",
			'h_level_id'			=> (isset($info['c_number_level']))	? strtoupper($_replace->deleteTilde($info['c_number_level']))	: "",
			'type'					=> $type,
			'user_id'				=> $user,
			'additional'			=> (isset($info['additional']))	? strtoupper($_replace->deleteTilde($info['additional']))		: "",
			'created_at' 			=> $date
		];

		$query 	=	'INSERT INTO cp_leads_tmp(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, type, user_id, additional, created_at) VALUES ("'.$iData['client_id'].'", "0", "'.$iData['name'].'", "'.$iData['birthday'].'", "'.$iData['gender'].'", "'.$iData['age'].'", "'.$iData['repre'].'", "'.$iData['type_acc'].'", "'.$iData['phone_main'].'", "'.$iData['phone_main_owner'].'", "'.$iData['phone_main_provider'].'", "'.$iData['phone_alt'].'", "'.$iData['phone_alt_owner'].'", "'.$iData['phone_alt_provider'].'", "'.$iData['phone_other'].'", "'.$iData['phone_other_owner'].'", "'.$iData['phone_other_provider'].'", "'.$iData['email_main'].'", "'.$iData['email_other'].'", "'.$iData['add_main'].'", "'.$iData['add_postal'].'", "'.$iData['coor_lati'].'", "'.$iData['coor_long'].'", "'.$iData['country_id'].'", "'.$iData['town_id'].'", "'.$iData['zip_id'].'", "'.$iData['h_type_id'].'", "'.$iData['h_roof_id'].'", "'.$iData['h_level_id'].'", "'.$type.'", "'.$user.'", "'.$iData['additional'].'", "'.$iData['created_at'].'")';
		
		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;

	}

	public static function SaveLead($info, $type, $user)
	{
		// $date 		= 	date('Y-m-d H:i:s', time());

		// $_replace 	= 	new Config();

		// $name 		=	strtoupper($_replace->deleteTilde($info['c_name']));

		// $query 		=	'INSERT INTO cp_leads_tmp(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, type, user_id, additional, created_at) VALUES ("'.$info['c_client'].'", "0", "'.$name.'", "", "", "", "", "1", "'.$info['c_phone'].'", "'.$name.'", "1", "", "", "1", "", "", "1", "", "", "", "", "", "", "1", "1", "1", "1", "1", "1", "'.$type.'", "'.$user.'", "", "'.$date.'")';

		// $dep 		=	DBSmart::DataExecute($query);

		// return ($dep <> false ) ? true : false;

		$cou        =   $info['c_country'];
        $coun    	=   Country::GetCountryById($cou)['id'];
        $town       =   Town::GetTownCountry($cou)[0]['id'];
        $zip        =   ZipCode::GetZipByTownID($town)[0]['id'];
        $cei        =   Ceiling::GetCeilingByCountryID($cou)[0]['id'];
        $lev        =   Level::GetLevelByCountryID($cou)[0]['id'];
        $hou        =   House::GetHouseByCountryID($cou)[0]['id'];

		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace 	= 	new Config();

		$name 		=	strtoupper($_replace->deleteTilde($info['c_name']));

		$query 		=	'INSERT INTO cp_leads_tmp(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, type, user_id, additional, created_at) VALUES ("'.$info['c_client'].'", "0", "'.$name.'", "", "", "", "", "1", "'.$info['c_phone'].'", "'.$name.'", "1", "", "", "1", "", "", "1", "", "", "", "", "", "", "'.$coun.'", "'.$town.'", "'.$zip.'", "'.$hou.'", "'.$cei.'", "'.$lev.'", "'.$type.'", "'.$user.'", "", "'.$date.'")';

		$dep 		=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}

	public static function SaveLeadMKT($iData)
	{

		$query 		=	'INSERT INTO cp_leads_tmp( client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, type, user_id, additional, created_at) VALUES ("'.$iData['c_client'].'", "0", "'.$iData['c_name'].'", "", "M", "0", "", "1", "'.$iData['c_phone'].'", "'.$iData['c_phone_ow'].'", "'.$iData['c_phone_pro'].'", "", "", "1", "", "", "1", "", "", "", "", "", "", "'.$iData['c_country'].'", "1", "1", "1", "1", "1", "NEW", "'.$iData['user'].'", "", "'.$iData['date'].'")';

		$mkt 		=	DBSmart::DataExecute($query);

		return ($mkt <> false ) ? true : false;
	}

}