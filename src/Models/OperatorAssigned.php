<?php
namespace App\Models;

use Model;

use App\Models\OperatorAssigned;
use App\Models\OpertorAssignedResp;
use App\Models\Leads;
use App\Models\Service;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBSmart;
use PDO;

class OperatorAssigned extends Model 
{
	static $_table 		= 'cp_ope_assig';

	Public $_fillable 	= array('id', 'client_id', 'mkt_id', 'service_id', 'status_id', 'data_id', 'process_by', 'process_at', 'assigned_by', 'assigned_at');

////////////////////////////////////////////////////////////

	public static function SaveAssig($info, $user, $mkt)
	{
		$Imkt 	=	($mkt <> false) ? $mkt : '';

		$query 	=	'INSERT INTO cp_ope_assig(client_id, mkt_id, service_id, status_id, data_id, process_by, process_at, assigned_by, assigned_at) VALUES ("'.$info['client'].'", "'.$Imkt.'", "'.$info['serviceI'].'", "0", "", "'.$user['operator'].'", "", "'.$user['user'].'", "'.$info['date'].'")';
		
		$dep 	=	DBSmart::DataExecute($query);

		return 	($dep <> false ) ? true : false;
	}

////////////////////////////////////////////////////////////

	public static function GetAssignedById($id)
	{
		$info 	=	OperatorAssigned::where('id', $id)->find_one();

		$query 	=	'SELECT * FROM cp_ope_assig WHERE id = "'.$id.'"';
		$info 	=	DBSmart::DBQuery($query);

		if($info <> false)
		{
			return 	[
				'id'		=>	$info['id'],
				'client'	=>	$info['client_id'],
				'mkt'		=>	$info['mkt_id'],
				'service'	=>	$info['service_id'],
				'status'	=>	$info['status_id'],
				'data'		=>	$info['data_id'],
				'process'	=>	$info['process_by'],
				'p_date'	=>	$info['process_at'],
				'assigned'	=>	$info['assigned_by'],
				'a_date'	=>	$info['assigned_at']
			];

		}else{	return false;	}
	}

////////////////////////////////////////////////////////////
	
	public static function GetAssigned($ope, $date)
	{

        $month		=	date("m");

        $year 		=	date("Y");

        $query 		=	'SELECT * FROM cp_ope_assig WHERE process_by = "'.$ope.'" AND (status_id = "0" OR status_id = "1") AND assigned_at BETWEEN "'.$year.'-'.$month.'-01 00:00:00" AND "'.$year.'-'.$month.'-31 23:59:59" ORDER BY id DESC';

    	$info     	=   DBSmart::DBQueryAll($query);

		$service 	=	Service::GetService();

        $operator	=	User::GetUserById($ope)['username'];

        $serv 		=	['tv_pr_contacted', 'sec_pr_contacted', 'int_pr_contacted', 'wrl_pr_contacted'];

        $table 		=	['cp_service_pr_tv', 'cp_service_pr_sec', 'cp_service_pr_int', 'cp_service_pr_mr', 'cp_service_vzla_int'];

        foreach ($serv as $s => $sal) 
        {
        	$query 		= 'SELECT COUNT(IF('.$sal.'="1",1,NULL)) as cont FROM cp_ope_assig_resp WHERE created_by = "'.$ope.'" AND created_at BETWEEN "'.$year.'-'.$month.'-01 00:00:00" AND "'.$year.'-'.$month.'-31 23:59:59" ORDER BY id DESC';

	    	$result     =   DBSmart::DBQuery($query);

        	$cont[$s]	=	[	''.$sal.'' 	=>	($result['cont']) ? $result['cont'] : "0"];
        }

        foreach ($service as $k => $val) 
        {
        	$query 		= 	'SELECT COUNT(service_id) as cont FROM cp_ope_assig WHERE service_id = "'.$val['id'].'" AND process_by = "'.$ope.'" AND assigned_at BETWEEN "'.$year.'-'.$month.'-01 00:00:00" AND "'.$year.'-'.$month.'-31 23:59:59" ORDER BY id DESC';

	    	$result     =   DBSmart::DBQuery($query);

        	$asig[$k]	=	['service' => $val['id'], 'cont' => ($result['cont']) ? $result['cont'] : "0", 'ope' => $operator];
        }

        foreach ($table as $t => $tal) 
        {
        	$query 		= 'SELECT COUNT(id) as cont FROM '.$tal.' WHERE operator_id = "'.$ope.'" AND created_at BETWEEN "'.$year.'-'.$month.'-01 00:00:00" AND "'.$year.'-'.$month.'-31 23:59:59" ORDER BY id DESC';

	    	$result     =   DBSmart::DBQuery($query);

        	$close[$t]	=	[	''.$tal.'' 	=>	($result['cont']) ? $result['cont'] : "0"];

        }

		if($info <> false)
		{
			foreach ($info as $k => $val) 
			{
				$data[$k]	=	[
					'id'		=>	$val['id'],
					'client'	=>	$val['client_id'],
					'mkt'		=>	$val['mkt_id'],
					'service'	=>	$val['service_id'],
					'status'	=>	$val['status_id'],
					'data'		=>	$val['data_id'],
					'process'	=>	$val['process_by'],
					'p_date'	=>	$val['process_at'],
					'assigned'	=>	$val['assigned_by'],
					'a_date'	=>	$val['assigned_at']
				];
			}
			return ['panel' => $data, 'assig' => $asig, 'contact' => $cont, 'close' => $close];

		}else{	return ['panel' => false, 'assig' => $asig, 'contact' => $cont, 'close' => $close];	}
	}
	
	public static function GetStaticAssigned($ope, $date)
	{

		$query 	=	'SELECT	* FROM cp_ope_assig WHERE process_by = "'.$ope.'" AND assigned_at LIKE "'.$date.'%" ORDER BY id ASC';

		$info 	=	DBSmart::DBQueryAll($query);

		if($info <> false)
		{
			foreach ($info as $k => $val) 
			{
				$data[$k]	=	[
					'id'		=>	$val['id'],
					'client'	=>	$val['client_id'],
					'mkt'		=>	$val['mkt_id'],
					'service'	=>	$val['service_id'],
					'status'	=>	$val['status_id'],
					'data'		=>	$val['data_id'],
					'process'	=>	$val['process_by'],
					'p_date'	=>	$val['process_at'],
					'assigned'	=>	$val['assigned_by'],
					'a_date'	=>	$val['assigned_at']
				];
			}
			return $data;
		}else{	return false;	}
	
	}

////////////////////////////////////////////////////////////

	public static function GetAssignedByDate($info, $type)
	{

		$service 	=	Service::GetService();

        $operator	=	User::GetUserById($info['id'])['username'];

        $serv 		=	['tv_pr_contacted', 'sec_pr_contacted', 'int_pr_contacted', 'wrl_pr_contacted'];

        $table 		=	['cp_service_pr_tv', 'cp_service_pr_sec', 'cp_service_pr_int', 'cp_service_pr_mr', 'cp_service_vzla_int'];

        foreach ($serv as $s => $sal) 
        {
        	if($type == 1)
        	{
        		$qCont 	= 	'SELECT COUNT(IF('.$sal.'="1",1,NULL)) as cont FROM cp_ope_assig_resp WHERE created_by = "'.$info['id'].'" AND created_at BETWEEN "'.$info['dateIni'].' 00:00:00" AND "'.$info['dateEnd'].' 23:59:59"';
        	}else{
				$qCont 	= 	'SELECT COUNT(IF('.$sal.'="1",1,NULL)) as cont FROM cp_ope_assig_resp WHERE created_by = "'.$info['id'].'" AND created_at LIKE "'.$info['dateEnd'].'%"';
        	}

	    	$result     =   DBSmart::DBQuery($qCont);

        	$cont[$s]	=	[	''.$sal.'' 	=>	($result['cont']) ? $result['cont'] : "0" ,];
        }

        foreach ($service as $k => $val) 
        {
        	if($type == 1)
        	{
        		$qAssig = 	'SELECT COUNT(service_id) as cont FROM cp_ope_assig WHERE service_id = "'.$val['id'].'" AND process_by = "'.$info['id'].'" AND assigned_at BETWEEN "'.$info['dateIni'].' 00:00:00" AND "'.$info['dateEnd'].' 23:59:59"';
        	}else{
				$qAssig = 	'SELECT COUNT(service_id) as cont FROM cp_ope_assig WHERE service_id = "'.$val['id'].'" AND process_by = "'.$info['id'].'" AND assigned_at LIKE "'.$info['dateEnd'].'%"';
        	}

	    	$result     =    DBSmart::DBQuery($qAssig);

        	$asig[$k]	=	['service' => $val['id'], 'cont' => ($result['cont']) ? $result['cont'] : "0" , 'ope' => $operator];
        }

        foreach ($table as $t => $tal) 
        {
        	if($type == 1)
        	{
        		$qCont 	= 	'SELECT COUNT(id) as cont FROM '.$tal.' WHERE operator_id = "'.$info['id'].'" AND created_at BETWEEN "'.$info['dateIni'].' 00:00:00" AND "'.$info['dateEnd'].' 23:59:59"';
        	}else{
				$qCont 	= 	'SELECT COUNT(id) as cont FROM '.$tal.' WHERE operator_id = "'.$info['id'].'" AND created_at LIKE "'.$info['dateEnd'].'%"';
        	}

	    	$result     =    DBSmart::DBQuery($qCont);

        	$close[$t]	=	[	''.$tal.'' 	=>	($result['cont']) ? $result['cont'] : "0" ,];
        }

        if($type == 1)
        {
        	$query 		=	'SELECT * FROM cp_ope_assig WHERE process_by = "'.$info['id'].'" AND assigned_at BETWEEN "'.$info['dateIni'].' 00:00:00" AND "'.$info['dateEnd'].' 23:59:59"';
        }else{
        	$query		=	'SELECT * FROM cp_ope_assig WHERE process_by = "'.$info['id'].'" AND assigned_at LIKE "'.$info['dateEnd'].'%"';
        }

        $result     	=   DBSmart::DBQueryAll($query);

        if($result <> false)
        {
			foreach ($result as $k => $val) 
			{
				$data[$k]	=	[
					'id'		=>	$val['id'],
					'client'	=>	$val['client_id'],
					'mkt'		=>	$val['mkt_id'],
					'service'	=>	$val['service_id'],
					'status'	=>	$val['status_id'],
					'data'		=>	$val['data_id'],
					'process'	=>	$val['process_by'],
					'p_date'	=>	$val['process_at'],
					'assigned'	=>	$val['assigned_by'],
					'a_date'	=>	$val['assigned_at']
				];
			}
			return ['panel' => $data, 'assig' => $asig, 'contact' => $cont, 'close' => $close];
        }

        return ['panel' => false, 'assig' => $asig, 'contact' => $cont, 'close' => $close];;
	}


////////////////////////////////////////////////////////////

	public static function GetTableAssigned($data)
	{

		$_replace   =   new Config();
		$html = '';
		$html.='<table id="opeTableAsig" class="table table-bordered" >';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>Id</th>';
		$html.='<th>Cliente</th>';
		$html.='<th>Telefono</th>';
		$html.='<th>Servicio</th>';
		$html.='<th>Status</th>';
		$html.='<th>Procesado</th>';
		$html.='<th>Asignado</th>';
		$html.='<th>Fecha</th>';
		$html.='<th>Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody style="font-size: 10px; text-align: center;">';
		if($data <> false)
		{
		foreach ($data as $k => $val) 
		{
		$status 	=	($val['status'] == 0 ) ? '<span class="label label-warning">PENDIENTE</span>' : '<span class="label label-success">PROCESADO</span>';

		$lead 		=	Leads::GetLeadByClient($val['client']);

		$html.='<tr>';
		$html.='<td>'.$val['client'].'</td>';
		$html.='<td>'.$lead['name'].'</td>';
		$html.='<td>'.$lead['phone_main'].'</td>';
		$html.='<td>'.Service::ServiceHTML($val['service']).'</td>';
		$html.='<td>'.$status.'</td>';
		$html.='<td>'.$val['p_date'].'</td>';
		$html.='<td>'.User::GetUserById($val['assigned'])['name'].'</td>';
		$html.='<td>'.$_replace->ShowDateAll($val['a_date']).'</td>';
		if($val['status'] == 0)
		{
			$html.='<td  style="margin-top: 0px;"><ul class="demo-btns text-center">
			<li style="margin-top: 0px;">
			<a href="/leads/view/'.(int)$val['client'].'" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Clien"><i class="fa fa-eye"></i></a>
			</li>
			<li style="margin-top: 0px;">
			<a onclick="ProAssigned('.(int)$val['id'].')" class="btn btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Notes"><i class="fa fa-edit"></i></a>
			</li>
			</ul> </td>';
			$html.='</tr>';

		}else{
			$html.='<td>
				<ul class="demo-btns text-center">
				<li style="margin-top: 0px;">
				<a href="/leads/view/'.(int)$val['client'].'" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Clien"><i class="fa fa-eye"></i></a>
				</li>
				</ul> 
				</td>';
			$html.='</tr>';			
		}
		}

		}else{
		$html.='<tr>';
		$html.='<td colspan="9"><h5>Sin Leads Asignados</h5></td>';
		$html.='</tr>';
		}
		$html.='</tbody>';
		$html.='</table>';

		return $html;
	}

////////////////////////////////////////////////////////////

	public static function GetTableStatic($data, $contact, $close)
	{
		$html='';
		$html.='<table class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<tbody style="text-align: center;">';
		$html.='<tr style="text-align: center;">';
		$html.='<td>'.$data[0]['ope'].'</td>';
		$html.='<td><i class="fa fa-lg fa-desktop text-muted hidden-md hidden-sm hidden-xs"></td>';
		$html.='<td><i class="fa fa-lg fa-lock text-muted hidden-md hidden-sm hidden-xs"></td>';
		$html.='<td><i class="fa fa-lg fa-globe text-muted hidden-md hidden-sm hidden-xs"></td>';
		$html.='<td><i class="fa fa-lg fa-signal text-muted hidden-md hidden-sm hidden-xs"></td>';
		$html.='<td><i class="fa fa-lg fa-signal text-muted hidden-md hidden-sm hidden-xs"></td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td>Asignados</td>';
		$html.='<td>'.$data[1]['cont'].'</td>';
		$html.='<td>'.$data[2]['cont'].'</td>';
		$html.='<td>'.$data[3]['cont'].'</td>';
		$html.='<td>'.$data[4]['cont'].'</td>';
		$html.='<td>'.$data[5]['cont'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td>Contactados</td>';
		$html.='<td>'.$contact[0]['tv_pr_contacted'].'</td>';
		$html.='<td>'.$contact[1]['sec_pr_contacted'].'</td>';
		$html.='<td>'.$contact[2]['int_pr_contacted'].'</td>';
		$html.='<td>'.$contact[3]['wrl_pr_contacted'].'</td>';
		$html.='<td>'.$contact[3]['wrl_pr_contacted'].'</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td>Cerrados</td>';
		$html.='<td>'.$close[0]['cp_service_pr_tv'].'</td>';
		$html.='<td>'.$close[1]['cp_service_pr_sec'].'</td>';
		$html.='<td>'.$close[2]['cp_service_pr_int'].'</td>';
		$html.='<td>'.$close[3]['cp_service_pr_mr'].'</td>';
		$html.='<td>'.$close[4]['cp_service_vzla_int'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';

		return $html;
	}

////////////////////////////////////////////////////////////


}