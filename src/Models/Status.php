<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;

class Status extends Model {

	static $_table = 'data_status';
	
	Public $_fillable = array('name');

	public static function GetStatus()
	{
		
		$query 	=	'SELECT id, name FROM data_status ORDER BY id ASC';
		$sta 	=	DBSmart::DBQueryAll($query);

		$status = array();

		if($sta <> false)
		{
			foreach ($sta as $k => $val) 
			{	$status[$k] = array('id' 	=> $val['id'], 'name' 	=> $val['name']);	}
	        
	        return $status;

		}else{ 	return false;	}

	}

	public static function GetStatusById($id)
	{
		$query 	=	'SELECT id, name FROM data_status WHERE id = "'.$id.'"';
		$sta 	=	DBSmart::DBQuery($query);

		if($sta <> false)
		{
			return [ 'id'	=>	$sta['id'], 'name'	=>	$sta['name'] ];

		}else{ 	return false;	}

	}

	public static function GetID($id)
	{
	  $query  =  'SELECT * FROM data_status WHERE id = "'.$id.'"';
	  $val    =   DBSmart::DBQuery($query);

	  return   [
	     'id'       => $val['id'],
	     'name'     => $val['name']
	  ];  

	}

}