<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Camera;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Camera extends Model {

	static $_table 		= 'data_camera';

	Public $_fillable 	= array('name', 'country_id', 'status_id');

	public static function GetCamera()
	{

		$query 	= 	'SELECT * FROM data_camera ORDER BY id ASC';
		$cam 	=	DBSmart::DBQueryAll($query);

		$cameras = array();

		if($cam <> false)
		{
			foreach ($cam as $k => $val) 
			{
				$cameras[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $cameras;
		}else{ 	return false;	}

	}

	public static function GetCameraById($id)
	{

		$query 	= 	'SELECT * FROM data_camera WHERE id = "'.$id.'"';
		$gen 	=	DBSmart::DBQuery($query);

		if($gen <> false)
		{
			return array('id' => $gen['id'], 'name' => $gen['name'], 'status' => $gen['status_id'], 'country' => $gen['country_id'] );

		}else{ 	return false;	}

	}

	public static function GetCameraByName($info)
	{

		$query 	= 	'SELECT * FROM data_camera WHERE name = "'.$info.'"';
		$gen 	=	DBSmart::DBQuery($query);

		if($gen <> false)
		{
			return array( 'id' => $gen['id'], 'name' => $gen['name'], 'status' => $gen['status_id'], 'country' => $gen['country_id'] );

		}else{ 	return false;	}

	}

	public static function SaveCamera($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_cam']));

		if($info['type_cam'] == 'new')
		{
			$query 	= 	'INSERT INTO data_camera(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_cam'].'", "'.$info['status_id_cam'].'", "'.$date.'")';

			$gen 	=	DBSmart::DataExecute($query);

			return ($gen <> false ) ? true : false;

		}
		elseif ($info['type_cam'] == 'edit') 
		{
			$query 	=	'UPDATE data_camera SET name="'.$name.'", country_id="'.$info['country_id_cam'].'", status_id="'.$info['status_id_cam'].'" WHERE id = "'.$info['id_cam'].'"';

			$gen 	=	DBSmart::DataExecute($query);

			return ($gen <> false) ? true : false;
		}
	}

}