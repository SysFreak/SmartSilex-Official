<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\ServiceM;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class ServiceM extends Model {

	static $_table 		= 'data_mkt_service';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id');

	public static function GetServiceM()
	{
		$query 	= 	'SELECT * FROM data_mkt_service ORDER BY id ASC';
		$serv 	=	DBSmart::DBQueryAll($query);
		
		if($serv <> false)
		{
			foreach ($serv as $k => $val) 
			{
				$ServiceMs[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=>	$val['name'],
					'service'  		=>	Service::ServiceById($val['service_id'])['name'],
					'service_di'  	=>	$val['service_id'],
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=>	$val['country_id'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id']
				);
			}
	        
	        return $ServiceMs;

		}else{ return false; }


	}

	public static function GetServiceMById($id)
	{
		
		$query 	= 	'SELECT * FROM data_mkt_service WHERE id = "'.$id.'" ORDER BY id ASC';
		$serv 	=	DBSmart::DBQuery($query);
		
		if($serv <> false)
		{
			return array(
			 	'id'           => $serv['id'],
			 	'name'         => $serv['name'],
			 	'service'      => $serv['service_id'],
			 	'country'      => $serv['country_id'],
			 	'status'       => $serv['status_id']
			);

		}else{ return false; }
	}


	public static function GetServiceMByCountryID($id)
	{
		$query 	= 	'SELECT id, name, status_id FROM data_mkt_service WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$serv 	=	DBSmart::DBQueryAll($query);
		
		if($serv <> false)
		{
			
			foreach ($serv as $k => $val) 
			{
				$service[$k] = [
					'id'		=>	$val['id'],
					'name'		=>	$val['name'],
					'status'	=>	$val['status_id']
				];
			}
			return $service;

		}else{ return false; }
		
	}

	public static function GetServiceMByCountry($id)
	{


		$query 	= 	'SELECT id, name FROM data_mkt_service WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$serv 	=	DBSmart::DBQueryAll($query);

		$html 	= 	"";

		if($serv <> false)
		{
			foreach ($serv as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			return $html;

		}else{ return $html; }

	}

	public static function GetServiceMByName($info)
	{
		
		$query 	= 	'SELECT * FROM data_mkt_service WHERE name = "'.$info['name_se'].'" AND service_id = "'.$info['service_id_se'].'" ORDER BY id ASC';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false )
		{
			return array('id' => $serv['id'], 'name' => $serv['name'], 'service_id' => $serv['service_id'], 'country_id' => $serv['country_id'],'status' => $serv['status_id']);
			
		}else{	return false; 	}
	}

	public static function SaveServiceM($info)
	{
		$date 		= date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_se']));

		if($info['type_se'] == 'new')
		{
			$query 	= 	'INSERT INTO data_mkt_service(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_se'].'", "'.$info['country_id_se'].'" "'.$info['status_id_se'].'", "'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? $dep['last_id'] : false;
		}
		elseif ($info['type_se'] == 'edit') 
		{
			$query 	=	'UPDATE data_mkt_service SET name="'.$name.'", service_id="'.$info['service_id_se'].'", country_id="'.$info['country_id_se'].'", status_id="'.$info['status_id_se'].'" WHERE id = "'.$info['id_se'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}


