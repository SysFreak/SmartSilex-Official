<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Origen;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Origen extends Model {

	static $_table 		= 'data_mkt_origen';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id');

	public static function GetOrigen()
	{
		$query 	=	'SELECT * FROM data_mkt_origen ORDER BY id ASC';
		$org 	=	DBSmart::DBQueryAll($query);

		$origen = array();

		foreach ($org as $k => $val) 
		{
			$origen[$k] = array(
				'id' 	 		=> 	$val['id'], 
				'name' 	 		=> 	$val['name'], 
				'service' 		=> 	Service::ServiceById($val['service_id'])['name'],
				'country'		=> 	Country::GetCountryById($val['country_id'])['name'],
				'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
				'service_id'	=>	$val['service_id'],
				'status_id'		=>	$val['status_id'],
				'country_id'	=>	$val['country_id']
			);
		}

        return $origen;
	}

	public static function GetOrigenById($id)
	{
		$query 	=	'SELECT * FROM data_mkt_origen WHERE id = "'.$id.'"';
		$org 	=	DBSmart::DBQuery($query);

		return array(
			'id'           => $org['id'],
			'name'         => $org['name'],
			'service'      => $org['service_id'],
			'country'      => $org['country_id'],
			'status'       => $org['status_id']
		);
	}

	public static function GetOrigenByCountryID($id)
	{
		$query 	=	'SELECT id, name, status_id FROM data_mkt_origen WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$org 	=	DBSmart::DBQueryAll($query);
		$origen = 	array();

		if($org <> false)
		{
			foreach ($org as $k => $val) 
			{
				$medio[$k] = [
					'id'		=>	$val['id'],
					'name'		=>	$val['name'],
					'status'	=>	$val['status_id']
				];

			}
			return $medio;

		}else{ return false; }
	}

	public static function GetOrigenByCountry($id)
	{

		$query 	=	'SELECT id, name FROM data_mkt_origen WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$org 	=	DBSmart::DBQueryAll($query);
		$html 	= 	"";

		if($org <> false)
		{
			foreach ($org as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			return $html;

		}else{ return $html; }
	
	}

	public static function GetOrigenByName($info)
	{

		$query 	=	'SELECT * FROM data_mkt_origen WHERE name = "'.$info['name_ob'].'" AND service_id = "'.$info['service_id_ob'].'"';
		$org 	=	DBSmart::DBQuery($query);

		if($org <> false)
		{
			
			return array('id' => $org['id'], 'name' => $org['name'], 'service_id' => $org['service_id'], 'country_id' => $org['country_id'],'status' => $org['status_id']);

		}else{	return false;	}
	
	}

	public static function SaveOrigen($info)
	{
		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$name 		= 	strtoupper($_replace->deleteTilde($info['name_org']));

		if($info['type_org'] == 'new')
		{

			$query 	=	'INSERT INTO data_mkt_origen(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_org'].'", "'.$info['country_id_org'].'", "'.$info['status_id_org'].'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['type_org'] == 'edit') 
		{
			$query 	=	'UPDATE data_mkt_origen SET name="'.$name.'", service_id = "'.$info['service_id_org'].'", country_id="'.$info['country_id_org'].'", status_id="'.$info['status_id_org'].'" WHERE id = "'.$info['id_org'].'"';
			
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
	}
}