<?php
namespace App\Models;

use Model;

use App\Models\LeadsServObjection;
use App\Models\Objection;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadsServObjection extends Model 
{
	static $_table 		= 'cp_leads_serv_objections';

	Public $_fillable 	= array('client_id', 'objection_id', 'service_id', 'created_by', 'created_at');

	public static function GetObjectionById($id)
	{
		$query 	=	'SELECT id, client_id, objection_id, service_id, created_at FROM cp_leads_serv_objections WHERE id = "'.$id.'"';
		$obj 	=	DBSmart::DBQuery($query);;

		if($obj <> false)
		{
			return [
				'id' 		=> 	$obj['id'],
				'client'	=>	$obj['client_id'],
				'objection'	=> 	$obj['objection_id'],
				'service'	=>	$obj['service_id']
			];

		}else{ return false; }
	}

	public static function GetObjectionByClient($info)
	{

		$query 	=	'SELECT id, client_id, objection_id, service_id, created_at  FROM cp_leads_serv_objections WHERE client_id = "'.$info['client'].'" AND service_id = "'.$info['service'].'" ORDER BY created_at ASC';
		$obj 	=	DBSmart::DBQueryAll($query);;
		
		if($obj <> false)
		{
			foreach ($obj as $o => $ob) 
			{
				$objections[$o]	=	[
					'id'			=>	$ob['id'],
					'client'		=>	$ob['client_id'],
					'objection_id'	=>	$ob['objection_id'],
					'objection'		=>	Objection::GetObjectionById($ob['objection_id'])['name'],
					'date'			=>	$ob['created_at']
				];
			}
			return $objections;
		}else{
			return false;
		}
	}


	public static function SaveObjections($info)
	{
		$query 	=	'INSERT INTO cp_leads_serv_objections(client_id, objection_id, service_id, created_by, created_at) VALUES ("'.$info['client'].'", "'.$info['objection'].'", "'.$info['service'].'", "'.$info['operator'].'", "'.$info['date'].'")';

		$serv  	=	DBSmart::DataExecute($query);	

		return 	($serv <> false) ? true : false;
	}
}