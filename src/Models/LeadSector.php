<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Town;
use App\Models\LeadStates;
use App\Models\LeadTownShip;
use App\Models\LeadParish;
use App\Models\LeadSector;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadSector extends Model 
{

	static $_table 		= 'cp_leads_sector';

	Public $_fillable 	= array('name', 'parish_id', 'status_id', 'created_at');

	public static function GetSector()
	{
		$query 	= 	'SELECT id, name, parish_id, status_id FROM cp_leads_sector ORDER BY name ASC';
		$cit 	=	DBSmart::DBQueryAll($query);
		
		if($cit <> false)
		{
			foreach ($cit as $k => $val) 
			{
				$city[$k] = array(
					'id' 	 	=> 	$val['id'], 
					'name' 	 	=>	$val['name'],
					'states'	=>	LeadParish::GetParishById($val['parish_id'])['name'],
					'status' 	=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'	=>	$val['status_id']
				);
			}
	        
	        return $city;

		}else{ return false; }
	}

	public static function GetSectorById($id)
	{
		$query 		= 	'SELECT id, name, parish_id, status_id FROM cp_leads_sector WHERE id = "'.$id.'" ORDER BY id DESC';
		$country 	=	DBSmart::DBQuery($query);

		if($country <> false)
		{
			return [
			 'id'           => $country['id'],
			 'name'         => $country['name'],
			 'states'		=> $country['parish_id'],
			 'status'       => $country['status_id']
			];

		}else{ return false; }
	}

	public static function GetListBySector($id)
	{
		$query 	= 	'SELECT id, name, parish_id, status_id FROM cp_leads_sector WHERE status_id = "1" AND parish_id = "'.$id.'" ORDER BY id ASC';
		$tship 	=	DBSmart::DBQueryAll($query);

		$html 	=	'';
		$html.='<select class="form-control" name="c_sector" id="c_sector">';
		if($tship <> false)
		{
      		foreach ($tship as $t => $tsh) 
      		{
      			$html.='<option value="'.$tsh['id'].'"> '.$tsh['name'].'</option>';
      		}

      	}else{ 
      		$html.='<option value=""> SELECCIONE</option>';
      	}

      	$html.='</select>';
      	return $html;
	}

	public static function GetListBySectorGen($parish, $id)
	{
		$query 	= 	'SELECT id, name, parish_id, status_id FROM cp_leads_sector WHERE status_id = "1" AND parish_id = "'.$parish.'" ORDER BY id ASC';
		$tship 	=	DBSmart::DBQueryAll($query);

		$html 	=	'';
		$html.='<select class="form-control" name="c_sector" id="c_sector">';
		if($tship <> false)
		{
      		foreach ($tship as $t => $tsh) 
      		{
      			if($tsh['id'] == $id)
      			{
      				$html.='<option value="'.$tsh['id'].'" selected> '.$tsh['name'].'</option>';
      			}else{
	      			$html.='<option value="'.$tsh['id'].'"> '.$tsh['name'].'</option>';
      			}
      		}

      	}else{ 
      		$html.='<option value=""> SELECCIONE</option>';
      	}

      	$html.='</select>';
      	return $html;
	}

	public static function GetListBySectorSelect()
	{
		$html 	=	'';
		$html.='<select class="form-control" name="c_sector" id="c_sector">';
  		$html.='<option value=""> SELECCIONE</option>';
		$html.='</select>';
      	return $html;
	
	}

	public static function InsertSector($iData)
	{
		$query 	= 	'INSERT INTO cp_leads_sector(name, parish_id, status_id, created_at) VALUES ("'.strtoupper($iData['name']).'", "'.$iData['parish_id'].'", "'.$iData['status_id'].'",  NOW())';
		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}

	public static function UpdateSector($iData, $id)
	{
		$query 	=	'UPDATE cp_leads_sector SET name="'.strtoupper($iData['name']).'", parish_id = "'.$iData['parish_id'].'", status_id="'.$iData['status_id'].'" WHERE id = "'.$id.'"';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false) ? true : false;
	}

	public static function SearchSector($sector)
	{
		$query 	=	'SELECT id AS sector_id, name AS sector, (SELECT id FROM cp_leads_parish WHERE id = parish_id) AS parish_id, (SELECT name FROM cp_leads_parish WHERE id = parish_id) AS parroquia, (SELECT id FROM cp_leads_township WHERE id = (SELECT township_id FROM cp_leads_parish WHERE id = parish_id)) AS township_id, (SELECT name FROM cp_leads_township WHERE id = (SELECT township_id FROM cp_leads_parish WHERE id = parish_id)) AS municipio, (SELECT id FROM cp_leads_states WHERE id = (SELECT states_id FROM cp_leads_township WHERE id = (SELECT township_id FROM cp_leads_parish WHERE id = parish_id))) AS estado_id, (SELECT name FROM cp_leads_states WHERE id = (SELECT states_id FROM cp_leads_township WHERE id = (SELECT township_id FROM cp_leads_parish WHERE id = parish_id))) AS estado FROM cp_leads_sector WHERE name LIKE "%'.$sector.'%"';

		return 	DBSmart::DBQueryAll($query);

	}

	public static function SearchSectorById($id)
	{
		$query 	=	'SELECT id AS sector_id, (SELECT id FROM cp_leads_parish WHERE id = parish_id) AS parish_id, (SELECT id FROM cp_leads_township WHERE id = (SELECT township_id FROM cp_leads_parish WHERE id = parish_id)) AS township_id, (SELECT id FROM cp_leads_states WHERE id = (SELECT states_id FROM cp_leads_township WHERE id = (SELECT township_id FROM cp_leads_parish WHERE id = parish_id))) AS estado_id FROM cp_leads_sector WHERE id = "'.$id.'"';

		return 	DBSmart::DBQuery($query);

	}

	public static function SearchSectorHTML($iData)
	{
		$html 	=	'';

		$html.='<table id="dt_SectorSearch" class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<thead>                     ';
		$html.='<tr>';
		$html.='<th style="text-align: center;"> Estado </th>';
		$html.='<th style="text-align: center;"> Muncicipo</th>';
		$html.='<th style="text-align: center;"> Parroquia</th>';
		$html.='<th style="text-align: center;"> Sector</th>';
		$html.='<th style="text-align: center;"> Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		if($iData <> false)
		{
			$html.='<tbody>';
			foreach ($iData as $k => $dat) 
			{
				$html.='<tr>';
				$html.='<td style="text-align: center;">'.$dat['estado'].'</td>';
				$html.='<td style="text-align: center;">'.$dat['municipio'].'</td>';
				$html.='<td style="text-align: center;">'.$dat['parroquia'].'</td>';
				$html.='<td style="text-align: center;">'.$dat['sector'].'</td>';
				$html.='<td style="text-align: center;">';
				$html.='<div class="btn-group">';
				$html.='<a class="btn btn-primary btn-xs" onclick="SectorSelect('.(int)$dat['sector_id'].');"><i class="fa fa-plus"></i></a>';
				$html.='</div>';
				$html.='</td>';
				$html.='</tr>';
			}
			$html.='</tbody>';
			$html.='</table>';
		}else{
			$html.='</tbody>';
			$html.='</table>';
		}

		return $html;

	}


	public static function GetSectorHtml($iData)
	{
		$html 	=	'';

		$html.='<div class="widget-body no-padding">';
		$html.='<table id="dt_sector" class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<thead>                     ';
		$html.='<tr>';
		$html.='<th data-hide="user">ID</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-check text-muted hidden-md hidden-sm hidden-xs"></i> Nombre</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-check text-muted hidden-md hidden-sm hidden-xs"></i> Parroquia</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-check text-muted hidden-md hidden-sm hidden-xs"></i> Status</th>';
		$html.='<th data-class="expand"><i class="fa fa-fw fa-edit text-muted hidden-md hidden-sm hidden-xs"></i> Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		if($iData <> false)
		{
			$html.='<tbody>';
			foreach ($iData as $k => $dat) 
			{
				$html.='<tr>';
				$html.='<td>'.$dat['id'].'</td>';
				$html.='<td>'.$dat['name'].'</td>';
				$html.='<td>'.$dat['states'].'</td>';
				$html.='<td>'.$dat['status'].'</td>';
				$html.='<td>';
				$html.='<div class="btn-group">';
				$html.='<a class="btn btn-primary btn-xs" onclick="SectorEdit('.$dat['id'].');"><i class="fa fa-edit"></i> Editar </a>';
				$html.='</div>';
				$html.='</td>';
				$html.='</tr>';
			}
			$html.='</tbody>';
			$html.='</table>';
		}else{
			$html.='</tbody>';
			$html.='</table>';
		}
		$html.='</div>';

		return $html;

	}

	public static function GetShowSectorHtml($country, $status)
	{
		$html = "";

		$html.='<form class="FormSector" method="post" id="FormSector" data-parsley-validate="">';
		$html.='<div class="modal-body" style="background: aliceblue;">';
		$html.='<div class="row">';
		$html.='<div id="alertStates"></div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-12">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Nombre</label>';
		$html.='<input type="text" id="name" name="name" class="form-control" placeholder="Nombre"required />';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Parroquia</label>';
		$html.='<select class="form-control" id="parish_id" name="parish_id" required style="font-size: 10px;">';
		foreach ($country as $c => $cou) 
		{
			if($cou['status_id'] == "1")
			{	$html.='<option value="'.$cou['id'].'"> '.$cou['name'].' </option>'; }else{
				$html.='<option value="" selected > SELECCIONE </option>';
			}

		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Status</label>';
		$html.='<select class="form-control" id="status_id" name="status_id" required style="font-size: 10px;">';
		foreach ($status as $s => $sta) 
		{
			$html.='<option value="'.$sta['id'].'"> '.$sta['name'].'</option>';
		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="modal-footer">';
		$html.='<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
		$html.='<a class="btn btn-primary" onclick="SectorCreated()">Registrar</a>';
		$html.='</div>';
		$html.='</form>';

		return $html;

	}

	public static function GetSectorEditHtml($iData, $country, $status)
	{
		$html = "";

		$html.='<form class="FormSector" method="post" id="FormSector" data-parsley-validate="">';
		$html.='<div class="modal-body" style="background: aliceblue;">';
		$html.='<div class="row">';
		$html.='<div id="alertStates"></div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-12">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Nombre</label>';
		$html.='<input type="text" id="name" name="name" class="form-control" placeholder="Nombre" value="'.$iData['name'].'" required />';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row">';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Parroquia</label>';
		$html.='<select class="form-control" id="parish_id" name="parish_id" required style="font-size: 10px;">';
		foreach ($country as $c => $cou) 
		{
			if($iData['states'] == $cou['id'])
			{	$html.='<option value="'.$cou['id'].'" selected > '.$cou['name'].''; }
			else
			{ 	$html.='<option value="'.$cou['id'].'"> '.$cou['name'].''; }
		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="col-md-6">';
		$html.='<div class="form-group">';
		$html.='<label for="category"> Status</label>';
		$html.='<select class="form-control" id="status_id" name="status_id" required style="font-size: 10px;">';
		foreach ($status as $s => $sta) 
		{
			if($iData['status'] == $sta['id'])
			{	$html.='<option value="'.$sta['id'].'" selected > '.$sta['name'].''; }
			else
			{ 	$html.='<option value="'.$sta['id'].'"> '.$sta['name'].''; }
		}
		$html.='</select>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="modal-footer">';
		$html.='<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
		$html.='<a class="btn btn-primary" onclick="SectorUpdate('.$iData['id'].')">Actualizar</a>';
		$html.='</div>';
		$html.='</form>';

		return $html;
	
	}


}