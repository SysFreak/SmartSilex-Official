<?php
namespace App\Models;

use Model;
use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class Auth extends Model {

   static $_table = 'users';
   
   protected $_fillable = array('name', 'password', 'username', 'email', 'ext', 'departament_id', 'role_id', 'status_id', 'team', 'hours_in', 'hours_out', 'tv', 'sec', 'net', 'ene');  

	public static function GetUser($input)
	{
		$query 		=	'SELECT * FROM users WHERE username = "'.strtolower($input['username']).'" AND status_id = "1"';
		$res        =   DBSmart::DBQuery($query);
		
		return 	$res;
	}

	public static function ValPass($pass, $pass2)
	{

		return ($pass == $pass2) ? true :  false;
		
	}

	public static function Notification($message, $error)
	{
		if($error == true)
		{
			return '<div class="alert alert-danger fade in"><button class="close" data-dismiss="alert">×</button><strong>Error!</strong> '.$message.'.</div>';
		}else{
			return '<div class="alert alert-success fade in"><button class="close" data-dismiss="alert">×</button><strong>Satisfactorio</strong> '.$message.'.</div>';
		}
	}
}