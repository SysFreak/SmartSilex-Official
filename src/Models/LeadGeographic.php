<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\LeadStates;
use App\Models\LeadTownShip;
use App\Models\LeadParish;
use App\Models\LeadSector;

use App\Lib\Config;
use App\Lib\DBSmart;

class LeadGeographic extends Model 
{

	static $_table 		= 'cp_leads_geographic';

	Public $_fillable 	= array('id', 'client_id', 'state_id', 'township_id', 'parish_id', 'sector_id', 'created_by', 'created_at');

	public static function GetGeographicByClient($id)
	{
		$query 	= 	'SELECT id, client_id, state_id, township_id, parish_id, sector_id, created_by FROM cp_leads_geographic WHERE client_id = "'.$id.'" ORDER BY id DESC LIMIT 1';
		$geo 	=	DBSmart::DBQuery($query);
		
		if($geo <> false)
		{
			$geo 	=	[
				'client_id'		=>	$geo['client_id'],
				'state_id'		=>	$geo['state_id'],
				'township_id'	=>	$geo['township_id'],
				'parish_id'		=>	$geo['parish_id'],
				'sector_id'		=>	$geo['sector_id'],
			];
	        
	        return $geo;

		}else{ return false; }
	
	}

	public static function InsertGeographic($client, $idata, $user)
	{
		$iData 	=	[
			'c_state'		=>	(!isset($idata['c_state'])) 	? "2" : $idata['c_state'],
			'c_township'	=>	(!isset($idata['c_township'])) 	? "3" : $idata['c_township'],
			'c_parish'		=>	(!isset($idata['c_parish'])) 	? "11" : $idata['c_parish'],
			'c_sector'		=>	(!isset($idata['c_sector'])) 	? "802" : $idata['c_sector'],
		];

		$query 	= 	'INSERT INTO cp_leads_geographic(client_id, state_id, township_id, parish_id, sector_id, created_by, created_at) VALUES ("'.$client.'", "'.$iData['c_state'].'", "'.$iData['c_township'].'", "'.$iData['c_parish'].'", "'.$iData['c_sector'].'", "'.$user.'", NOW())';		
		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;

		
	}

}