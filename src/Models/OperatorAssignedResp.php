<?php
namespace App\Models;

use Model;

use App\Models\OperatorAssigned;
use App\Models\OperatorAssignedResp;
use App\Models\Leads;

use App\Lib\Config;
use App\Lib\DBSmart;

class OperatorAssignedResp extends Model 
{
	static $_table 		= 'cp_ope_assig_resp';

	Public $_fillable 	= array('id', 'client_id', 'mkt_id', 'contacted', 'tv_pr_contacted', 'tv_pr_objection', 'sec_pr_contacted', 'sec_pr_objection',  'int_pr_contacted', 'int_pr_objection', 'wrl_pr_contacted', 'wrl_pr_objection', 'created_by', 'created_at');

	////////////////////////////////////////////////////////////

	public static function SaveResp($info)
	{
		$query 	=	'INSERT INTO cp_ope_assig_resp(client_id, mkt_id, contacted, tv_pr_contacted, tv_pr_objection, sec_pr_contacted, sec_pr_objection, int_pr_contacted, int_pr_objection, wrl_pr_contacted, wrl_pr_objection, created_by, created_at) VALUES ("'.$info['client'].'", "'.$info['mkt'].'", "'.$info['contacted'].'", "'.$info['tv_pr_c'].'", "'.$info['tv_pr_o'].'", "'.$info['sec_pr_c'].'", "'.$info['sec_pr_o'].'", "'.$info['int_pr_c'].'", "'.$info['int_pr_o'].'", "'.$info['wrl_pr_c'].'", "'.$info['wrl_pr_o'].'", "'.$info['operator'].'", "'.$info['date'].'")';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return 	($dep <> false ) ? $dep : false;
	}
}