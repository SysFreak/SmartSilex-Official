<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\House;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class House extends Model {

	static $_table 		= 'data_house';

	Public $_fillable 	= array('name', 'status_id', 'country_id');

	public static function GetHouse()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_house ORDER BY id ASC';
		$Hou 	=	DBSmart::DBQueryAll($query);

		$house = array();

		if($Hou <> false)
		{
			foreach ($Hou as $k => $val) 
			{
				$house[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $house;
		}else{ 	return false;	}
	}

	public static function GetHouseById($id)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_house WHERE id = "'.$id.'"';
		$hou 	=	DBSmart::DBQuery($query);

		if($hou <> false)
		{
			return array(
				'id' => $hou['id'], 'name' => $hou['name'], 'status' => $hou['status_id'], 'country' => $hou['country_id']
			);

		}else{ 	return false;	}

	}

	public static function GetHouseByStatus()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_house WHERE status_idid = "1"';
		$hou 	=	DBSmart::DBQuery($query);

		if($hou <> false)
		{
			return array(
				'id' => $hou['id'], 'name' => $hou['name'], 'status' => $hou['status_id'], 'country' => $hou['country_id']
			);

		}else{ 	return false;	}

	}

	public static function GetHouseByName($info)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_house WHERE name = "'.$info.'"';
		$hou 	=	DBSmart::DBQuery($query);

		if($hou <> false)
		{
			return array(
				'id' => $hou['id'], 'name' => $hou['name'], 'status' => $hou['status_id'], 'country' => $hou['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetHouseByCountryID($id)
	{
		
		$query 	= 	'SELECT id, name FROM data_house WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$to 	=	DBSmart::DBQueryAll($query);
		return ($to <> false) ? $to : false;
	}

	public static function GetHouseByCountry($id)
	{
		
		$query 	= 	'SELECT id, name FROM data_house WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$to 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($to)
		{
			foreach ($to as $k => $val) 
			{ $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';}

			return $html;

		}else{ return $html; }
	}

	public static function SaveHouse($info)
	{

		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_ho']));

		if($info['type_ho'] == 'new')
		{
			$query 	= 	'INSERT INTO data_house(name, status_id, country_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_ho'].'", "'.$info['country_id_ho'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_ho'] == 'edit') 
		{
			$query 	=	'UPDATE data_house SET name="'.$name.'", status_id="'.$info['status_id_ho'].'", country_id="'.$info['country_id_ho'].'" WHERE id = "'.$info['id_ho'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}

}


