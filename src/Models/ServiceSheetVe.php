<?php
namespace App\Models;

use Model;
use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\DBMikro;
use App\Lib\UnCypher;
use PDO;

class ServiceSheetVe extends Model 
{

	static $_table 		= 'cp_sup_serv_sheet_ve';

	Public $_fillable 	= array('id', 'client_id', 'visit_date', 'technical_id', 'phone_main', 'name', 'coordinations', 'town_id', 'add_main', 'plan', 'ip_current', 'ip_public', 'signal_r', 'transmitions', 'ap_current', 'speedtest_rx', 'speedtest_tx', 'reason', 'prev_int_id', 'status_id', 'created_by', 'created_at');

	public static function LoadSheet($date)
	{
		$query 	=	'SELECT id, client_id, visit_date, name, phone_main, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT username FROM users WHERE id = created_by) AS operator, (IF(prev_int_id = "1", "PREVISTA", "GENERAL")) AS prevista, (IF(status_id = "0", "ACTIVO", "SUSPENDIDO")) AS status FROM cp_sup_serv_sheet_ve WHERE visit_date LIKE "'.$date.'%" AND status_id = "0" ORDER BY id DESC';

		$res 	=	DBSmart::DBQueryAll($query);

		return 	ServiceSheetVe::SheetTableLoad($res);
	
	}

	public static function LoadSheetDate($date)
	{
		$query 	=	'SELECT id, client_id, visit_date, name, phone_main, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT username FROM users WHERE id = created_by) AS operator, (IF(prev_int_id = "1", "PREVISTA", "GENERAL")) AS prevista, (IF(status_id = "0", "ACTIVO", "SUSPENDIDO")) AS status FROM cp_sup_serv_sheet_ve WHERE visit_date BETWEEN "'.$date['dateIni'].'" AND "'.$date['dateEnd'].'" AND status_id = "0" ORDER BY id DESC';

		$res 	=	DBSmart::DBQueryAll($query);

		return 	ServiceSheetVe::SheetTableLoad($res);
	
	}

	public static function SearchSheet($iData)
	{
		$query 	=	'SELECT id FROM cp_sup_serv_sheet_ve WHERE client_id = "'.$iData['client'].'" AND visit_date = "'.$iData['date'].'" AND status_id = "0" ORDER BY id DESC LIMIT 1';
		$res 	=	DBSmart::DBQuery($query);
		
		return ($res <> false) ? $res : false;

	}

	public static function LoadSheetFib($date)
	{
		$query 	=	'SELECT id, client_id, visit_date, name, phone_main, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT username FROM users WHERE id = created_by) AS operator, (IF(prev_int_id = "1", "PREVISTA", "GENERAL")) AS prevista, (IF(status_id = "0", "ACTIVO", "SUSPENDIDO")) AS status FROM cp_sup_serv_sheet_ve_fib WHERE visit_date LIKE "'.$date.'%" AND status_id = "0" ORDER BY id DESC';

		$res 	=	DBSmart::DBQueryAll($query);

		return 	ServiceSheetVe::SheetTableLoadFib($res);
	
	}


	public static function LoadSheetDateFib($date)
	{
		$query 	=	'SELECT id, client_id, visit_date, name, phone_main, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT username FROM users WHERE id = created_by) AS operator, (IF(prev_int_id = "1", "PREVISTA", "GENERAL")) AS prevista, (IF(status_id = "0", "ACTIVO", "SUSPENDIDO")) AS status FROM cp_sup_serv_sheet_ve_fib WHERE visit_date BETWEEN "'.$date['dateIni'].'" AND "'.$date['dateEnd'].'" AND status_id = "0" ORDER BY id DESC';

		$res 	=	DBSmart::DBQueryAll($query);

		return 	ServiceSheetVe::SheetTableLoadFib($res);
	
	}

	public static function SearchSheetFib($iData)
	{
		$query 	=	'SELECT id FROM cp_sup_serv_sheet_ve_fib WHERE client_id = "'.$iData['client'].'" AND visit_date = "'.$iData['date'].'" AND status_id = "0" ORDER BY id DESC LIMIT 1';
		$res 	=	DBSmart::DBQuery($query);
		
		return ($res <> false) ? $res : false;

	}

	public static function SaveSheet($iData)
	{

		$query	=	'INSERT INTO cp_sup_serv_sheet_ve(client_id, visit_date, technical_id, phone_main, name, coordinations, town_id, add_main, plan, ip_current, ip_public, signal_r, transmitions, ap_current, speedtest_rx, speedtest_tx, reason, prev_int_id, status_id, created_by, created_at) VALUES ("'.$iData['client'].'", "'.$iData['date'].'", "'.$iData['tecnical'].'", "'.$iData['phone'].'", "'.$iData['name'].'", "'.$iData['coord'].'", "'.$iData['town'].'", "'.$iData['add'].'", "'.$iData['plan'].'", "'.$iData['ip'].'", "'.$iData['ip_p'].'", "'.$iData['signal'].'", "'.$iData['transf'].'", "'.$iData['ap'].'", "'.$iData['sppedrx'].'", "'.$iData['sppedtx'].'", "'.$iData['motive'].'", "0", "0", "'.$iData['operator'].'", NOW())';

		$res 	=	DBSmart::DataExecuteLastID($query);

		return ($res <>  false) ? $res : false;
	
	}

	public static function SaveSheetFib($iData)
	{
		$query	=	'INSERT INTO cp_sup_serv_sheet_ve_fib(client_id, visit_date, technical_id, phone_main, name, coordinations, town_id, add_main, plan, ip_current, ip_public, signal_r, transmitions, ap_current, speedtest_rx, speedtest_tx, reason, prev_int_id, pack, fee, ap_fiber, status_id, created_by, created_at) VALUES ("'.$iData['client'].'", "'.$iData['date'].'", "'.$iData['tecnical'].'", "'.$iData['phone'].'", "'.$iData['name'].'", "'.$iData['coord'].'", "'.$iData['town'].'", "'.$iData['add'].'", "'.$iData['plan'].'", "'.$iData['ip'].'", "'.$iData['ip_p'].'", "'.$iData['signal'].'", "'.$iData['transf'].'", "'.$iData['ap'].'", "'.$iData['sppedrx'].'", "'.$iData['sppedtx'].'", "'.$iData['motive'].'", "0", "'.$iData['pack'].'", "'.$iData['fee'].'", "'.$iData['apfib'].'", "0", "'.$iData['operator'].'", NOW())';

		$res 	=	DBSmart::DataExecuteLastID($query);

		return ($res <>  false) ? $res : false;
	
	}

	public static function CancelSheet($iData)
	{
		$query 	=	'UPDATE cp_sup_serv_sheet_ve SET status_id = "'.$iData['status'].'" WHERE id = "'.$iData['id'].'"';
		
		$res 	=	DBSmart::DataExecute($query);

		return ($res <>  false) ? true : false;
	
	}

	public static function CancelSheetFib($iData)
	{
		$query 	=	'UPDATE cp_sup_serv_sheet_ve_fib SET status_id = "'.$iData['status'].'" WHERE id = "'.$iData['id'].'"';
		
		$res 	=	DBSmart::DataExecute($query);

		return ($res <>  false) ? true : false;
	
	}

	public static function PrevSheet($iData)
	{
		$query 	=	'UPDATE cp_sup_serv_sheet_ve SET prev_int_id = "'.$iData['status'].'" WHERE id = "'.$iData['id'].'"';
		
		$res 	=	DBSmart::DataExecute($query);

		return ($res <>  false) ? true : false;
	
	}

	public static function SearchSheetById($iData)
	{
		$query 	=	'SELECT id, client_id, visit_date, technical_id, phone_main, name, coordinations, town_id, add_main, plan, ip_current, ip_public, signal_r, transmitions, ap_current, speedtest_rx, speedtest_tx, reason, prev_int_id, status_id, created_by, created_at FROM cp_sup_serv_sheet_ve WHERE id = "'.$iData['id'].'" ORDER BY id DESC LIMIT 1';

		$res 	=	DBSmart::DBQuery($query);

		return ($res <>  false) ? $res : false;
	
	}

	public static function SearchSheetFibById($iData)
	{
		$query 	=	'SELECT * FROM cp_sup_serv_sheet_ve_fib WHERE id = "'.$iData['id'].'" ORDER BY id DESC LIMIT 1';

		$res 	=	DBSmart::DBQuery($query);

		return ($res <>  false) ? $res : false;
	
	}


	public static function SaveContractFib($iData)
	{
		$query	=	'INSERT INTO cp_contracts_fibra(ticket, client_id, name, add_main, add_postal, phone_main, phone_alt, email, nivel, techo, house, coor_lati, coor_long, package, type, telephone, tel_type, tel_price, pre_date, last_date, date_res, price, prorateo, mes, instalation, cc_last, cc_exp, ab_last, ab_exp, ab_bank, id_last, id_exp, ss_last, ss_exp, contract, country, sale, repre_legal, tp, created_by, created_at) VALUES ("'.$iData['ticket'].'","'.$iData['client_id'].'","'.$iData['name'].'","'.$iData['add_main'].'","'.$iData['add_postal'].'","'.$iData['phone_main'].'","'.$iData['phone_alt'].'","'.$iData['email'].'","'.$iData['nivel'].'","'.$iData['techo'].'","'.$iData['house'].'","'.$iData['coor_lati'].'","'.$iData['coor_long'].'","'.$iData['package'].'","'.$iData['type'].'","'.$iData['telephone'].'","'.$iData['tel_type'].'","'.$iData['tel_price'].'","'.$iData['pre_date'].'","'.$iData['last_date'].'","'.$iData['date_res'].'","'.$iData['price'].'","'.$iData['prorateo'].'","'.$iData['mes'].'","'.$iData['instalation'].'","'.$iData['cc_last'].'","'.$iData['cc_exp'].'","'.$iData['ab_last'].'","'.$iData['ab_exp'].'","'.$iData['ab_bank'].'","'.$iData['id_last'].'","'.$iData['id_exp'].'","'.$iData['ss_last'].'","'.$iData['ss_exp'].'","'.$iData['contract'].'","'.$iData['country'].'","'.$iData['sale'].'","'.$iData['repre_legal'].'","'.$iData['tp'].'","'.$iData['created_by'].'", NOW())';

		$res 	=	DBSmart::DataExecute($query);

		return ($res <>  false) ? true : false;

	}

	public static function SheetBodyHTML($iClient, $iPlan, $iEmi, $iTown)
	{
		$html = '';

		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertSubmit"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">CLIENTE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="s_client" id="s_01" placeholder="Id del Cliente" value="'.$iClient['id'].'" readonly></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">NOMBRE COMPLETO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="s_name" id="s_02" value="'.$iClient['name'].'"placeholder="Nombre del Cliente" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TEL&Eacute;FONO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-phone"></i><input type="text" name="s_phone" id="s_03" value="'.$iClient['phone'].' - '.$iClient['movil'].'" placeholder="Telefonos"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">FECHA DE SERVICIO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-calendar"></i><input type="text" name="s_calendar" id="s_04" value="'.$iClient['date'].'"placeholder="Fecha de Servicio"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PUEBLO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="s_town" id="s_05">';
		$html.=''.$iTown.'';
		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">COORDENADAS</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_coord" id="s_06" value="'.$iClient['coord'].'"placeholder="Coordenadas"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<section style="margin-top: 10px;">';
		$html.='<label class="label">DIRECCI&Oacute;N</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="2" class="custom-scroll" name="s_add" id="s_07">'.$iClient['add'].'</textarea>';
		$html.='</label>';
		$html.='</section>';
		$html.='<br>';
		$html.='</fieldset>';
		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertPlan"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN CONTRATADO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="s_plan" id="s_08">';
		foreach ($iEmi as $e => $emi) 
		{
			$html.='<option value="'.$emi['contrato'].'">'.$emi['contrato'].' - '.$emi['plan'].'</option>';
		}

		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN SELECCIONADO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="s_plan_s" id="s_09" value="'.$iPlan['plan'].'"placeholder="Plan Seleccionado" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_ip_a" id="s_10" value="'.$iPlan['ip'].'" placeholder="IP Actual" readonly></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SE&Ntilde;AL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="s_signal" id="s_11" placeholder="Se&ntilde;al"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TRANSMISI&Oacute;N </label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="s_trans" id="s_12" placeholder="Transmisi&oacute;n"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP P&Uacute;BLICA</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_ip_p" id="s_13" placeholder="IP P&uacute;blica"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST TX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="s_speedtx" id="s_14" placeholder="SpeedTest TX"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST RX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="s_speedrx" id="s_15" placeholder="SpeedTest RX"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">AP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_ap" id="s_16" value="'.$iPlan['ap'].'" placeholder="AP Actual"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<section style="margin-top: 10px;">';
		$html.='<label class="label">MOTIVO</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="2" class="custom-scroll" name="s_motive" id="s_17"></textarea>';
		$html.='</label>';
		$html.='</section>';
		$html.='<br>';
		$html.='</fieldset>';

		return $html;
	
	}

	public static function SheetBodyEditHTML($iClient, $iPlan, $iEmi, $iTown)
	{
		$html = '';

		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertSubmit"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">CLIENTE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="s_client" id="s_01" placeholder="Id del Cliente" value="'.$iClient['id'].'" readonly></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">NOMBRE COMPLETO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="s_name" id="s_02" value="'.$iClient['name'].'"placeholder="Nombre del Cliente" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TEL&Eacute;FONO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-phone"></i><input type="text" name="s_phone" id="s_03" value="'.$iClient['phone'].'" placeholder="Telefonos"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">FECHA DE SERVICIO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-calendar"></i><input type="text" name="s_calendar" id="s_04" value="'.$iClient['date'].'"placeholder="Fecha de Servicio"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PUEBLO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="s_town" id="s_05">';
		foreach ($iTown as $t => $to) 
		{
			if($to['id'] == $iClient['town'])
			{
				$html.='<option value="'.$to['id'].'" selected>'.$to['name'].'</option>';
			}else{
				$html.='<option value="'.$to['id'].'">'.$to['name'].'</option>';
			}
		}

		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">COORDENADAS</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_coord" id="s_06" value="'.$iClient['coord'].'"placeholder="Coordenadas"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<section style="margin-top: 10px;">';
		$html.='<label class="label">DIRECCI&Oacute;N</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="2" class="custom-scroll" name="s_add" id="s_07">'.$iClient['add'].'</textarea>';
		$html.='</label>';
		$html.='</section>';
		$html.='<br>';
		$html.='</fieldset>';
		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertPlan"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">LISTADO DE PLANES CONTRATADO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="s_plan" id="s_08">';
		foreach ($iEmi as $e => $emi) 
		{
			$html.='<option value="'.$emi['contrato'].'">'.$emi['contrato'].' - '.$emi['plan'].'</option>';
		}

		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN SELECCIONADO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="s_plan_s" id="s_09" value="'.$iPlan['plan'].'"placeholder="Plan Seleccionado" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_ip_a" id="s_10" value="'.$iPlan['ip'].'" placeholder="IP Actual" readonly></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SE&Ntilde;AL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="s_signal" id="s_11" value="'.$iPlan['sig'].'" placeholder="Se&ntilde;al"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TRANSMISI&Oacute;N </label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="s_trans" id="s_12" value="'.$iPlan['trans'].'" placeholder="Transmisi&oacute;n"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP P&Uacute;BLICA</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_ip_p" id="s_13" value="'.$iPlan['ip_p'].'" placeholder="IP P&uacute;blica"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST TX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="s_speedtx" id="s_14" value="'.$iPlan['tx'].'" placeholder="SpeedTest TX"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST RX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="s_speedrx" id="s_15" value="'.$iPlan['rx'].'" placeholder="SpeedTest RX"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">AP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="s_ap" id="s_16" value="'.$iPlan['ap'].'" placeholder="AP Actual"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<section style="margin-top: 10px;">';
		$html.='<label class="label">MOTIVO</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="2" class="custom-scroll" name="s_motive" id="s_17">'.$iPlan['reas'].'</textarea>';
		$html.='</label>';
		$html.='</section>';
		$html.='<br>';
		$html.='</fieldset>';

		return $html;
	
	}

	public static function SheetEditFibHTML($iClient, $iPlan, $iEmi, $iTown, $iPack)
	{
		$html = '';

		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertSubmit"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">CLIENTE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="sh_client" id="sh_01" placeholder="Id del Cliente" value="'.$iClient['id'].'" readonly></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">NOMBRE COMPLETO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="sh_name" id="sh_02" value="'.$iClient['name'].'"placeholder="Nombre del Cliente" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TEL&Eacute;FONO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-phone"></i><input type="text" name="sh_phone" id="sh_03" value="'.$iClient['phone'].'" placeholder="Telefonos"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">FECHA DE SERVICIO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-calendar"></i><input type="text" name="sh_calendar" id="sh_04" value="'.$iClient['date'].'"placeholder="Fecha de Servicio"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PUEBLO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="sh_town" id="sh_05">';
		foreach ($iTown as $t => $to) 
		{
			if($to['id'] == $iClient['town'])
			{
				$html.='<option value="'.$to['id'].'" selected>'.$to['name'].'</option>';
			}else{
				$html.='<option value="'.$to['id'].'">'.$to['name'].'</option>';
			}
		}

		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">COORDENADAS</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_coord" id="sh_06" value="'.$iClient['coord'].'"placeholder="Coordenadas"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<section style="margin-top: 10px;">';
		$html.='<label class="label">DIRECCI&Oacute;N</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="2" class="custom-scroll" name="sh_add" id="sh_07">'.$iClient['add'].'</textarea>';
		$html.='</label>';
		$html.='</section>';
		$html.='<br>';
		$html.='</fieldset>';
		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertPlan"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">LISTADO DE PLANES CONTRATADO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="sh_plan" id="sh_08">';
		foreach ($iEmi as $e => $emi) 
		{
			$html.='<option value="'.$emi['contrato'].'">'.$emi['contrato'].' - '.$emi['plan'].'</option>';
		}

		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN SELECCIONADO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="sh_plan_s" id="sh_09" value="'.$iPlan['plan'].'"placeholder="Plan Seleccionado" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_ip_a" id="sh_10" value="'.$iPlan['ip'].'" placeholder="IP Actual" readonly></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SE&Ntilde;AL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="sh_signal" id="sh_11" value="'.$iPlan['sig'].'" placeholder="Se&ntilde;al"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TRANSMISI&Oacute;N </label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="sh_trans" id="sh_12" value="'.$iPlan['trans'].'" placeholder="Transmisi&oacute;n"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP P&Uacute;BLICA</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_ip_p" id="sh_13" value="'.$iPlan['ip_p'].'" placeholder="IP P&uacute;blica"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST TX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="sh_speedtx" id="sh_14" value="'.$iPlan['tx'].'" placeholder="SpeedTest TX"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST RX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="sh_speedrx" id="sh_15" value="'.$iPlan['rx'].'" placeholder="SpeedTest RX"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">AP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_ap" id="sh_16" value="'.$iPlan['ap'].'" placeholder="AP Actual"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN FIBRA INALAMBRICA</label>';
		$html.='<select class="form-control" name="sh_pack" id="sh_18">';
		foreach ($iPack as $p => $pack) 
		{

			if($pack['name'] == $iPlan['pack'])
			{
				$html.='<option value="HASTA '.$pack['name'].'" selected> HASTA '.$pack['name'].'</option>';
			}else{
				$html.='<option value="HASTA '.$pack['name'].'"> HASTA '.$pack['name'].'</option>';
			}

		}
		$html.='</select>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">FEE UPGRADE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-dollar"></i><input type="number" name="sh_fee" id="sh_19" placeholder="Fee Upgrade" value="99.99"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">AP UPGRADE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_apfib" id="sh_20" placeholder="AP Upgrade"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<br>';
		$html.='</fieldset>';

		return $html;
	
	}

	public static function SheetTableLoad($iData)
	{
		$_replace 	= 	new Config();

		$html = '';
		$html.='<table id="ServTableVe" class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<thead>                     ';
		$html.='<tr>';
		$html.='<th style="text-align: center;"> Id</th>';
		$html.='<th style="text-align: center;"> Nombre</th>';
		$html.='<th style="text-align: center;"> Fecha de Visita</th>';
		$html.='<th style="text-align: center;"> Telefono</th>';
		$html.='<th style="text-align: center;"> Pueblo</th>';
		$html.='<th style="text-align: center;"> Operador</th>';
		$html.='<th style="text-align: center;"> Tipo</th>';
		$html.='<th style="text-align: center;"> Status</th>';
		$html.='<th style="text-align: center;"> Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';
		if($iData <> false)
		{
	        foreach ($iData as $i => $data) 
	        {
				$html.='<tr>';
				$html.='<td style="text-align: center;">'.$data['client_id'].'</td>';
				$html.='<td style="text-align: center;">'.$data['name'].'</td>';
				$html.='<td style="text-align: center;">'.$_replace->ShowDate($data['visit_date']).'</td>';
				$html.='<td style="text-align: center;">'.$data['phone_main'].'</td>';
				$html.='<td style="text-align: center;">'.$data['town'].'</td>';
				$html.='<td style="text-align: center;">'.$data['operator'].'</td>';
				$html.='<td style="text-align: center;">'.$data['prevista'].'</td>';
				$html.='<td style="text-align: center;">'.$data['status'].'</td>';
				$html.='<td <td style="text-align: center;">';
				$html.='<div class="btn-group">';
				$html.='<a class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>';
				$html.='<ul class="dropdown-menu">';
				$html.='<li><a onclick="ViewSheetVe('.(int)$data['id'].')">Ver</a></li>';
				$html.='<li><a onclick="EditSheetVe('.(int)$data['id'].')">Editar</a></li>';
				$html.='<li><a onclick="CheckSheetVe('.(int)$data['id'].')">Prevista</a></li>';
				$html.='<li><a onclick="CancelSheetVe('.(int)$data['id'].')">Cancelar</a></li>';
				$html.='</ul>';
				$html.='</div>';
				$html.='</td>';
				$html.='</tr>';
	        }
			
		}
		$html.='</tbody>';
		$html.='</table>';

		return $html;
	
	}

	public static function UploadSheet($iData)
	{
		$query 	=	'UPDATE cp_sup_serv_sheet_ve SET visit_date="'.$iData['date'].'", phone_main="'.$iData['phone'].'", name="'.$iData['name'].'", coordinations="'.$iData['coord'].'", town_id="'.$iData['town'].'", add_main="'.$iData['add'].'", plan="'.$iData['plan'].'", ip_current="'.$iData['ip'].'", ip_public="'.$iData['ip_p'].'", signal_r="'.$iData['signal'].'", transmitions="'.$iData['trans'].'", ap_current="'.$iData['ap'].'", speedtest_rx="'.$iData['rx'].'", speedtest_tx="'.$iData['tx'].'", reason="'.$iData['reason'].'", created_by="'.$iData['operator'].'" WHERE id = "'.$iData['id'].'"';

		$res 	=	DBSmart::DataExecute($query);

		return ($res <>  false) ? $iData['id']: false;

	}

	public static function UploadSheetFib($iData)
	{
		$query 	=	'UPDATE cp_sup_serv_sheet_ve_fib SET visit_date="'.$iData['date'].'", phone_main="'.$iData['phone'].'", name="'.$iData['name'].'", coordinations="'.$iData['coord'].'", town_id="'.$iData['town'].'", add_main="'.$iData['add'].'", plan="'.$iData['plan'].'", ip_current="'.$iData['ip'].'", ip_public="'.$iData['ip_p'].'", signal_r="'.$iData['signal'].'", transmitions="'.$iData['trans'].'", ap_current="'.$iData['ap'].'", speedtest_rx="'.$iData['rx'].'", speedtest_tx="'.$iData['tx'].'", pack = "'.$iData['pack'].'", fee = "'.$iData['fee'].'", ap_fiber = "'.$iData['ap_fibra'].'", reason="'.$iData['reason'].'", created_by="'.$iData['operator'].'" WHERE id = "'.$iData['id'].'"';

		$res 	=	DBSmart::DataExecute($query);

		return ($res <>  false) ? $iData['id']: false;

	}


	public static function SheetTableLoadFib($iData)
	{
		$_replace 	= 	new Config();

		$html = '';
		$html.='<table id="ServTableFib" class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<thead>                     ';
		$html.='<tr>';
		$html.='<th style="text-align: center;"> Id</th>';
		$html.='<th style="text-align: center;"> Nombre</th>';
		$html.='<th style="text-align: center;"> Fecha de Visita</th>';
		$html.='<th style="text-align: center;"> Telefono</th>';
		$html.='<th style="text-align: center;"> Pueblo</th>';
		$html.='<th style="text-align: center;"> Operador</th>';
		$html.='<th style="text-align: center;"> Tipo</th>';
		$html.='<th style="text-align: center;"> Status</th>';
		$html.='<th style="text-align: center;"> Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';
		if($iData <> false)
		{
	        foreach ($iData as $i => $data) 
	        {
				$html.='<tr>';
				$html.='<td style="text-align: center;">'.$data['client_id'].'</td>';
				$html.='<td style="text-align: center;">'.$data['name'].'</td>';
				$html.='<td style="text-align: center;">'.$_replace->ShowDate($data['visit_date']).'</td>';
				$html.='<td style="text-align: center;">'.$data['phone_main'].'</td>';
				$html.='<td style="text-align: center;">'.$data['town'].'</td>';
				$html.='<td style="text-align: center;">'.$data['operator'].'</td>';
				$html.='<td style="text-align: center;">'.$data['prevista'].'</td>';
				$html.='<td style="text-align: center;">'.$data['status'].'</td>';
				$html.='<td <td style="text-align: center;">';
				$html.='<div class="btn-group">';
				$html.='<a class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>';
				$html.='<ul class="dropdown-menu">';
				$html.='<li><a onclick="ViewSheetVe('.(int)$data['id'].')">Ver</a></li>';
				$html.='<li><a onclick="EditSheetVe('.(int)$data['id'].')">Editar</a></li>';
				$html.='<li><a onclick="ContractFiber('.(int)$data['id'].')">Contrato</a></li>';
				$html.='<li><a onclick="CancelSheetVe('.(int)$data['id'].')">Cancelar</a></li>';
				$html.='</ul>';
				$html.='</div>';
				$html.='</td>';
				$html.='</tr>';
	        }
			
		}
		$html.='</tbody>';
		$html.='</table>';

		return $html;
	
	}

	public static function SheetFiberEdit($iClient, $iPlan, $iEmi, $iTown, $iPack)
	{
		$html = '';

		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertSubmit"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">CLIENTE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="sh_client" id="sh_01" placeholder="Id del Cliente" value="'.$iClient['id'].'" readonly></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">NOMBRE COMPLETO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="sh_name" id="sh_02" value="'.$iClient['name'].'"placeholder="Nombre del Cliente" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TEL&Eacute;FONO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-phone"></i><input type="text" name="sh_phone" id="sh_03" value="'.$iClient['phone'].'" placeholder="Telefonos"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">FECHA DE SERVICIO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-calendar"></i><input type="text" name="sh_calendar" id="sh_04" value="'.$iClient['date'].'"placeholder="Fecha de Servicio"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PUEBLO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="sh_town" id="sh_05">';
		$html.=''.$iTown.'';
		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">COORDENADAS</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_coord" id="sh_06" value="'.$iClient['coord'].'"placeholder="Coordenadas"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<section style="margin-top: 10px;">';
		$html.='<label class="label">DIRECCI&Oacute;N</label>';
		$html.='<label class="textarea">';
		$html.='<textarea rows="2" class="custom-scroll" name="sh_add" id="sh_07">'.$iClient['add'].'</textarea>';
		$html.='</label>';
		$html.='</section>';
		$html.='<br>';
		$html.='</fieldset>';
		$html.='<fieldset style="background: aliceblue;">';
		$html.='<div class="row"><div id="AlertPlan"></div></div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN CONTRATADO</label>';
		$html.='<label class="select"class="input">';
		$html.='<div class="icon-addon addon-md">';
		$html.='<select class="form-control" name="sh_plan" id="sh_08">';
		foreach ($iEmi as $e => $emi) 
		{
			$html.='<option value="'.$emi['contrato'].'">'.$emi['contrato'].' - '.$emi['plan'].'</option>';
		}
		$html.='</select>';
		$html.='<label for="country" class="glyphicon glyphicon-sort-by-attributes"></label>';
		$html.='</div>';
		$html.='</label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN SELECCIONADO</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-user"></i><input type="text" name="sh_plan_s" id="sh_09" value="'.$iPlan['plan'].'"placeholder="Plan Seleccionado" readonly></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_ip_a" id="sh_10" value="'.$iPlan['ip'].'" placeholder="IP Actual" readonly></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SE&Ntilde;AL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="sh_signal" id="sh_11" placeholder="Se&ntilde;al"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">TRANSMISI&Oacute;N </label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-signal"></i><input type="text" name="sh_trans" id="sh_12" placeholder="Transmisi&oacute;n"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">IP P&Uacute;BLICA</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_ip_p" id="sh_13" placeholder="IP P&uacute;blica"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST TX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="sh_speedtx" id="sh_14" placeholder="SpeedTest TX"></label>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">SPEEDTEST RX</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-tachometer"></i><input type="text" name="sh_speedrx" id="sh_15" placeholder="SpeedTest RX"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">AP ACTUAL</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_ap" id="sh_16" value="'.$iPlan['ap'].'" placeholder="AP Actual"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="row" style="margin-top: 10px;">';
		$html.='<div class="col col-4">';
		$html.='<label class="label">PLAN FIBRA INALAMBRICA</label>';
		$html.='<select class="form-control" name="sh_pack" id="sh_18">';
		foreach ($iPack as $p => $pack) 
		{
			$html.='<option value="HASTA '.$pack['name'].'"> HASTA '.$pack['name'].'</option>';
		}
		$html.='</select>';
		$html.='</div>';
		$html.='<div class="col col-4">';
		$html.='<label class="label">FEE UPGRADE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-dollar"></i><input type="text" name="sh_fee" id="sh_19" placeholder="Fee Upgrade" value="99.99"></label>';
		$html.='</div>                   ';
		$html.='<div class="col col-4">';
		$html.='<label class="label">AP UPGRADE</label>';
		$html.='<label class="input"> <i class="icon-prepend fa fa-map-marker"></i><input type="text" name="sh_apfib" id="sh_20" placeholder="AP Upgrade"></label>';
		$html.='</div>';
		$html.='</div>';
		$html.='<br>';
		$html.='</fieldset>';

		return $html;
	
	}


}