<?php
namespace App\Models;

use Model;

use App\Models\CoordContactSel;
use App\Models\status;

use App\Lib\Config;
use App\Lib\DBSmart;

class CoordContactSel extends Model 
{
	static $_table 		= 'data_coord_contact';

	Public $_fillable 	= array('name', 'status_id', 'created_by', 'created_at');

	public static function GetContactSel($info)
	{

		$query 	=	'SELECT id, name, status_id, created_by, created_at FROM data_coord_contact ORDER BY id ASC';
		$obj 	=	DBSmart::DBQueryAll($query);;
		
		if($obj <> false)
		{
			foreach ($obj as $o => $ob) 
			{
				$objections[$o]	=	[
					'id'			=>	$ob['id'],
					'name'			=>	$ob['name'],
					'status_id'		=>	$ob['status_id'],
					'status'		=>	Status::GetStatusById($ob['status_id'])['name']
				];
			}
			return $objections;
		}else{
			return false;
		}
	}


	public static function GetContactSelById($id)
	{
		$query 	=	'SELECT id, name, status_id, created_by, created_at FROM data_coord_contact WHERE id = "'.$id.'"';
		$obj 	=	DBSmart::DBQuery($query);;

		if($obj <> false)
		{
			return [
				'id' 		=> 	$obj['id'],
				'name'		=>	$obj['name'],
				'status_id'	=> 	$obj['status_id'],
				'status'	=>	Status::GetStatusById($obj['status_id'])['name']
			];

		}else{ return false; }
	}

	public static function GetContactSelByStatus()
	{

		$query 	=	'SELECT id, name, status_id, created_by, created_at  FROM data_coord_contact WHERE status_id = "1" ORDER BY id ASC';
		$obj 	=	DBSmart::DBQueryAll($query);;
		
		if($obj <> false)
		{
			foreach ($obj as $o => $ob) 
			{
				$objections[$o]	=	[
					'id'			=>	$ob['id'],
					'name'			=>	$ob['name'],
					'status_id'		=>	$ob['status_id'],
					'status'		=>	Status::GetStatusById($ob['status_id'])['name']
				];
			}
			return $objections;
		}else{
			return false;
		}
	}


	public static function SaveContactSel($info)
	{
		$query 	=	'INSERT INTO data_coord_contact(name, status_id, created_by, created_at) VALUES ("'.$info['name'].'", "'.$info['status_id'].'", "'.$info['operator'].'", "'.$info['date'].'")';

		$serv  	=	DBSmart::DataExecute($query);	

		return 	($serv <> false) ? true : false;
	}
}