<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Service;
use App\Models\Country;
use App\Models\ServiceTvPrTmp;
use App\Models\OrderType;

use App\Lib\Config;
use App\Lib\DBSmart;

class ServiceTvPrTmp extends Model 
{

	static $_table = 'cp_service_pr_tv_tmp';
	
	Public $_fillable = array('client_id', 'tv', 'package_id', 'provider_id', 'decoder_id', 'dvr_id', 'payment_id', 'ref_id', 'ref_list_id', 'sup_id', 'sup_ope_id', 'service_id', 'advertising_id', 'type_order_id', 'origen_id', 'ch_hd', 'ch_dvr', 'ch_hbo', 'ch_cinemax', 'ch_starz', 'ch_showtime', 'gif_ent', 'gif_choice', 'gif_xtra', 'owen_id', 'assit_id', 'created_at');

	public static function SaveService($info)
	{	
		if($info['sup'] == 0)
		{	$user1 = $info['operator']; $user2 = $info['operator']; }
		else
		{	$user1 = $info['sup_list']; $user2 = $info['operator'];}

		$date = date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO cp_service_pr_tv_tmp(client_id, tv, package_id, provider_id, decoder_id, dvr_id, payment_id, ref_id, ref_list_id, sup_id, sup_ope_id, service_id, advertising_id, type_order_id, origen_id, ch_hd, ch_dvr, ch_hbo, ch_cinemax, ch_starz, ch_showtime, gif_ent, gif_choice, gif_xtra, owen_id, assit_id, created_at) VALUES ("'.$info['client'].'", "'.$info['tv'].'", "'.$info['packages'].'", "'.$info['providers'].'", "'.$info['decoders'].'", "'.$info['dvrs'].'", "'.$info['payment'].'", "'.$info['referred'].'", "'.$info['ref_list'].'", "'.$info['sup'].'", "'.$info['sup_list'].'", "'.$info['service'].'", "'.$info['advertising'].'", "'.$info['type_order'].'", "'.$info['origen'].'", "'.$info['hd'].'", "'.$info['dvr'].'", "'.$info['hbo'].'", "'.$info['cinemax'].'", "'.$info['starz'].'", "'.$info['showtime'].'", "'.$info['gif_ent'].'", "'.$info['gif_cho'].'", "'.$info['gif_xtra'].'", "'.$user1.'", "'.$user2.'", "'.$date.'") ';

		$lead 	=	DBSmart::DataExecute($query);

		return ($lead <> false ) ? true : false;
	}

	public static function ServiceTv($client)
	{

		$query 	= 	'SELECT client_id, tv, package_id, provider_id, decoder_id, dvr_id, payment_id, ref_id, ref_list_id, sup_id, sup_ope_id, service_id, advertising_id, advertising_id, type_order_id, type_order_id, origen_id, ch_hd, ch_dvr, ch_hbo, ch_cinemax, ch_starz, ch_showtime, gif_ent, gif_choice, gif_xtra, owen_id, created_at FROM cp_service_pr_tv_tmp WHERE client_id = "'.$client.'" ORDER BY id DESC LIMIT 1';
		$serv 	=	DBSmart::DBQuery($query);

		if($serv <> false)
		{
			return [
				'status'		=>	true,
				'client' 		=>	($serv['client_id'] <> '') 			? $serv['client_id'] 	: $serv['client_id'],
				'tv' 			=>	($serv['tv'] <> '') 				? $serv['tv'] 			: '0',
				'packages' 		=>	($serv['package_id'] <> '') 		? $serv['package_id'] 	: '1',
				'providers' 	=>	($serv['provider_id'] <> '') 		? $serv['provider_id'] 	: '1',
				'decoders' 		=>	($serv['decoder_id'] <> '') 		? $serv['decoder_id'] 	: '0',
				'dvrs' 			=>	($serv['dvr_id'] <> '') 			? $serv['dvr_id'] 		: '0',
				'payment' 		=>	($serv['payment_id'] <> '') 		? $serv['payment_id'] 	: '1',
				'referred' 		=>	($serv['ref_id'] <> '') 			? $serv['ref_id']		: '0',
				'ref_list' 		=>	($serv['ref_list_id'] <> '') 		? $serv['ref_list_id']	: '0',
				'sup' 			=>	($serv['sup_id'] <> '' )			? $serv['sup_id']		: '0',
				'sup_list' 		=>	($serv['sup_ope_id'] <> '' )		? $serv['sup_ope_id']	: '0',
				'service' 		=>	($serv['service_id'] <> '') 		? $serv['service_id'] 	: '1',
				'adv_id'		=>	($serv['advertising_id'] <> '') 	? $serv['advertising_id'] : '0',
				'adv'			=>	($serv['advertising_id'] = '1')		? "SI" 					: "NO",
				'order_type'	=>	($serv['type_order_id'] <> '') 		? $serv['type_order_id']  : '1',
				'o_type'		=>	($serv['type_order_id'] <> '') 		? OrderType::GetOrderTypeById($serv['type_order_id'])['name'] : '', 
				'origen'		=>	($serv['origen_id'] <> '') 			? $serv['origen_id']  : '1',
				'hd' 			=>	($serv['ch_hd'] <> '') 				? $serv['ch_hd'] 		: 'off',
				'dvr' 			=>	($serv['ch_dvr'] <> '') 			? $serv['ch_dvr'] 		: 'off',
				'hbo' 			=>	($serv['ch_hbo'] <> '') 			? $serv['ch_hbo'] 		: 'off',
				'cinemax' 		=>	($serv['ch_cinemax'] <> '') 		? $serv['ch_cinemax'] 	: 'off',
				'starz' 		=>	($serv['ch_starz'] <> '') 			? $serv['ch_starz'] 	: 'off',
				'showtime' 		=>	($serv['ch_showtime'] <> '') 		? $serv['ch_showtime'] 	: 'off',
				'gif_ent' 		=>	($serv['gif_ent'] <> '') 			? $serv['gif_ent'] 		: 'off',
				'gif_cho' 		=>	($serv['gif_choice'] <> '') 		? $serv['gif_choice'] 	: 'off',
				'gif_xtra' 		=>	($serv['gif_xtra'] <> '') 			? $serv['gif_xtra'] 	: 'off',
				'operator' 		=>	($serv['owen_id'] <> '') 			? $serv['owen_id'] 		: '',
				'created'		=>	($serv['created_at'] <> '') 		? $serv['created_at'] 	: ''
			];

		}else{
			return ['status' => false, 'client' => $client, 'tv' =>	'0', 'packages' =>	'1', 'providers' =>	'1', 'decoders' =>	'0', 'dvrs'	=>	'0', 'payment'	=>	'1', 'referred'	=>	'0', 'ref_list'	=>	'0', 'sup' => '0', 'sup_list' => '1', 'service'	=>'1', 'adv'=> 'NO', 'order_type' =>	'1', 'o_type' => '1', 'origen' => '1', 'hd'	=>	'off', 'dvr'=>	'off', 'hbo' => 'off', 'cinemax'	=>	'off', 'starz' =>	'off', 'showtime'	=>	'off', 'gif_ent' => 'off', 'gif_cho'	=>	'off', 'gif_xtra'	=>	'off', 'operator'	=>	'1', 'created' =>	''];
		}
	}
}