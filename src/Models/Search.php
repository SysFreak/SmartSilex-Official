<?php
namespace App\Models;

use Model;

use App\Models\Search;
use App\Models\Country;
use App\Models\Town;
use App\Models\Package;
use App\Models\Provider;
use App\Models\Payment;
use App\Models\Score;
use App\Models\StatusService;
use App\Models\LeadsServObjection;
use App\Models\Objection;

use App\Lib\Config;
use App\Lib\DBSmart;
use PDO;

class Search extends Model 
{
	public static function NormalQuery($info, $search, $field)
	{
        $params     =   "";

        $phone      =   isset($info[$search]) ? $info[$search] : "";

        $t          =   substr($phone, '0','4');

        if( ($t == "0414") || ($t == "0424") || ($t == "0416") || ($t == "0426") || (($t == "0251")))
        {
            $params = substr($phone, "1", strlen($phone));
        }else{
            $params = $phone;
        }

        return ($params <> "") ? $field." LIKE '%".$params."%'" : "";
	
    }

    public static function NormalQueryExact($info, $search, $field)
    {
        return ($info[$search] <> "") ? $field." = ".$info[$search]."" : "";
    }

	public static function SelectQuery($info, $search, $field)
	{
		return (isset($info[$search])) ? $field." LIKE '%".$info[$search]."%'" : "";
	
    }

    public static function DateQuery($info, $search, $field)
    {
        return (isset($info[$search])) ? $info[$search]." ".$field."" : "";
    
    }

    public static function BetweenQuery($info, $search1, $search2, $var)
    {
        $_replace   =   new Config();
        return (isset($info[$search1])) ? " ".$var." BETWEEN '".$_replace->ChangeDate($info[$search1])." 00:00:00' AND '".$_replace->ChangeDate($info[$search2])." 23:59:59'" : "";
    }

	public static function LikeQuerys($info, $search, $field)
	{
      
        if($info[$search][0] <> '')
        {
            $sQuery = '(';
            foreach ($info[$search] as $k => $val) 
            {
                $sQuery.=$field." = '".$val."' OR ";
            }
            $sQuery = substr_replace($sQuery, "", -3);
            $sQuery .= ')';
            return $sQuery;
        }else{
            return "";
        }
	}

    public static function GetDBQuery($info)
    {
        $_replace     = new Config();
        $sTable     =   'cp_leads';
        $sWhere     =   '';
        $sClient    =   $sName    = $sPhone   = $sPhoneAlt    = "";
        $sBirthday  =   $sEmail   = $sCountry = $sZip         = "";
        $sTown      =   $sHouse   = $sCeiling = $sLevel       = "";

        $sInfo       =   [
            'sName'      =>   ($info['sa_name'] <> '')      ?   Search::NormalQuery($info, 'sa_name',           'name')                 :   '',
            'sCountry'   =>   ($info['sa_country'] <> '')   ?   Search::NormalQueryExact($info, 'sa_country',   'country_id')           :   '',
            'sPhone'     =>   ($info['sa_phone'] <> '')     ?   Search::NormalQuery($info, 'sa_phone',          'phone_main')           :   '',
            'sPhoneAlt'  =>   ($info['sa_phone_alt'] <> '') ?   Search::NormalQuery($info, 'sa_phone_alt',      'phone_alt')            :   '',
            'sTown'      =>   ($info['sa_town'] <> '')      ?   Search::LikeQuerys($info, 'sa_town',            'town_id')              :   '',
            'sHouse'     =>   ($info['sa_house'] <> '')     ?   Search::LikeQuerys($info, 'sa_house',           'h_type_id')            :   '',
            'sCeiling'   =>   ($info['sa_ceiling'] <> '')   ?   Search::LikeQuerys($info, 'sa_ceiling',         'h_roof_id')            :   '',
            'sLevel'     =>   ($info['sa_level'] <> '')     ?   Search::LikeQuerys($info, 'sa_level',           'h_level_id')           :   '',
        ];

        $sBirthday      =   (($info['sa_nac_in'] <> "") AND ($info['sa_nac_fin'] <> "")) 
                            ?   ' AND '. Search::BetweenQuery($info, 'sa_nac_in', 'sa_nac_fin', 'birthday') 
                            :   (($info['sa_nac_in'] <> "") ? Search::SelectQuery($info, 'sa_nac_in', 'birthday') : '');

        $sCreated      =   (($info['sa_cre_in'] <> "") AND ($info['sa_cre_fin'] <> "")) 
                            ?   ' AND '. Search::BetweenQuery($info, 'sa_cre_in', 'sa_cre_fin', 'created_at') 
                            :   (($info['sa_cre_in'] <> "") ? Search::SelectQuery($info, 'sa_cre_in', 'created_at') : '');

        $sWhere =   "WHERE (";
        foreach ($sInfo as $i => $inf) 
        { 
            if($inf <> "")
            $sWhere.=($inf <> "") ? $inf." OR " : ""; 
        }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere.=')';

        if($sWhere <> "WHER)" )
        {
            $query  =   'SELECT client_id, name, phone_main, town_id, h_type_id, h_roof_id, h_level_id, birthday, created_at, created_by  FROM '.$sTable.' '.$sWhere.' '.$sBirthday.' '.$sCreated.' GROUP BY client_id ORDER BY created_at ASC LIMIT 2000';

            $leads  =   DBSmart::DBQueryAll($query);

        }else{  

            if($sBirthday <> "" AND $sCreated == "")
            {
                $birth  =   (substr($sBirthday, 1,3) == "AND") ? substr_replace($sBirthday, '', 1, 3) : $sBirthday;
                $creat  =   "";
            }
            elseif($sBirthday <> "" AND $sCreated <> "")
            {
                $birth  =   (substr($sBirthday, 1,3) == "AND") ? substr_replace($sBirthday, '', 1, 3) : $sBirthday;
                $creat  =   (substr($sCreated, 1,3) == "AND") ? $sCreated : 'AND '.$sCreated;
            }
            elseif($sBirthday == "" AND $sCreated <> "")
            {
                $birth  =   "";
                $creat  =   (substr($sCreated, 1,3) == "AND") ? substr_replace($sCreated, '', 1, 3) : $sCreated ;
            }else{
                $birth  =   "";
                $creat  =   'created_at LIKE "'.date("Y-m-d").'%"';
            }

            $query  =   'SELECT client_id, name, phone_main, town_id, h_type_id, h_roof_id, h_level_id, birthday, created_at, created_by  FROM '.$sTable.' WHERE '.$birth.' '.$creat.' GROUP BY client_id ORDER BY created_at ASC LIMIT 2000';

            $leads  =   DBSmart::DBQueryAll($query);  


        }

        if($leads <> false)
        {
            foreach ($leads as $j => $jal) 
            {
                $iDataLeads[$j]   =   [
                    'client'    =>  $jal['client_id'],
                    'name'      =>  $jal['name'],
                    'phone'     =>  $jal['phone_main'],
                    'town'      =>  ($jal['town_id'] <> "")       ? Town::GetTownById($jal['town_id'])['name']          : "",
                    'house'     =>  ($jal['h_type_id'] <> "")     ? House::GetHouseById($jal['h_type_id'])['name']      : "",
                    'roof'      =>  ($jal['h_roof_id'] <> "")     ? Ceiling::GetCeilingById($jal['h_roof_id'])['name']  : "",
                    'level'     =>  ($jal['h_level_id'] <> "")    ? Level::GetLevelById($jal['h_level_id'])['name']     : "",
                    'birthday'  =>  ($jal['birthday'] <>  "")     ? ($_replace->ShowDate($jal['birthday']))             : "",
                    'created'   =>  ($jal['created_at'] <> "")    ? ($_replace->ShowDate($jal['created_at']))           : "",
                    'operator'  =>  ($jal['created_by'] <> "")    ? User::GetUserById($jal['created_by'])['username']   : "",
                ];
            }

            return $iDataLeads;
        }else{  return false;   }
    }

////////////////////////////// Objeciones PR //////////////////////////////

    public static function SearchObjections($info)
    {
        return  LeadsServObjection::GetObjectionByClient($info);
    }

////////////////////////////// General PR //////////////////////////////

    public static function ScorePR($info) 
    {

        $_replace     = new Config();

        $sType =   (isset($info['search-type'])) ? $info['search-type'] : false;

        switch ($sType) 
        {
            case 'score_tvpr':
                $serv = "2";
            break;
            
            case 'score_secpr':
                $serv = "3";
            break;
            
            case 'score_netpr':
                $serv = "4";
            break;
            
            case 'score_wrlpr':
                $serv = "5";
            break;

            case 'score_netvz':
                $serv = "6";
            break;
        }

        $sTable     =   'cp_appro_score';

        $sClient    =   $sTicket    =   $sScore     =   "";

        $sDate      =   (($info['tvi_fini'] <> "") AND ($info['tvi_fend'] <> "")) 
                        ?   ' AND '. Search::BetweenQuery($info, 'tvi_fini', 'tvi_fend', 't1.created_at') 
                        :   (($info['tvi_fini'] <> "") ? Search::SelectQuery($info, 'tvi_fini', 't1.created_at') : '');


        $sInfo      =   ['sScore'  =>   Search::LikeQuerys($info, 'tvi_score',     't1.score') ];
        
        $sWhere =   "AND ";
        foreach ($sInfo as $i => $inf) 
        { 
            if($inf <> "")
            $sWhere.=($inf <> "") ? $inf." OR " : ""; 
        }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere.='';


        if($sWhere <> "A" )
        {
            $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

            $query  =   'SELECT t1.ticket, t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_score WHERE id = t1.score) AS score, t1.created_at FROM cp_appro_score AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) '.$sWhere.' AND '.$fDate.' AND t1.service_id = "'.$serv.'" GROUP BY t1.ticket ORDER BY t1.created_at DESC LIMIT 1000';

            $ScoreTmp   =   DBSmart::DBQueryAll($query);

        }else{

            if($sDate <> "")
            {
                $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

                $query  =   'SELECT t1.ticket, t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_score WHERE id = t1.score) AS score, t1.created_at FROM cp_appro_score AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND '.$fDate.' AND t1.service_id = "'.$serv.'" GROUP BY t1.ticket ORDER BY t1.created_at DESC LIMIT 1000';

                $ScoreTmp   =   DBSmart::DBQueryAll($query);

            }else{  $ScoreTmp   =   false;  }
        }

        if($ScoreTmp)
        {
            foreach ($ScoreTmp as $t => $tmp) 
            {
                $iData[$t]   =   [
                    'ticket'    =>  $tmp['ticket'],
                    'client'    =>  $tmp['client_id'],
                    'name'      =>  $tmp['name'],
                    'phone'     =>  $tmp['phone_main'],
                    'score'     =>  $tmp['score'],
                    'date'      =>  ($tmp['created_at'] <> '') ? $_replace->ShowDate($tmp['created_at']) : ''
                ];
            }
            return $iData;

        }else{  return false;   }
    
    }

    public static function ObjectionPR($info) 
    {
        $_replace   =   new Config();

        $sType      =   (isset($info['search-type'])) ? $info['search-type'] : false;

        switch ($sType) {
            case 'objection_tvpr':
                $serv = "2";
            break;

            case 'objection_secpr':
                $serv = "3";
            break;

            case 'objection_netpr':
                $serv = "4";
            break;

            case 'objection_wrlpr':
                $serv = "5";
            break;

            case 'objection_netvz':
                $serv = "6";
            break;
        }

        $sTable     =   'cp_leads_serv_objections';

        $sClient    =   $sObjection     =   $sService     =   "";

         $sDate      =   (($info['tvo_fini'] <> "") AND ($info['tvo_fend'] <> "")) 
                        ?   ' AND '. Search::BetweenQuery($info, 'tvo_fini', 'tvo_fend', 't1.created_at') 
                        :   (($info['tvo_fini'] <> "") ? Search::SelectQuery($info, 'tvo_fini', 't1.created_at') : '');

  
        $sInfo      =   ['sObjection'  =>   Search::LikeQuerys($info, 'tvo_objection',     't1.objection_id') ];
        
        $sWhere     =   "AND ";
        foreach ($sInfo as $i => $inf) 
        { 
            if($inf <> "")
            $sWhere.=($inf <> "") ? $inf." OR " : ""; 
        }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere.='';


        if($sWhere <> "A" )
        {
            $fDate      =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

            $query      =   'SELECT t2.client_id, t2.name, t2.phone_main, (SELECT name FROM data_objections WHERE id = t1.objection_id) AS objection, t1.service_id, t1.created_at FROM cp_leads_serv_objections AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.service_id = "'.$serv.'" '.$sWhere.' AND '.$fDate.' GROUP BY t1.objection_id ORDER BY t1.created_at DESC LIMIT 1000';

            $ObjTemp    =   DBSmart::DBQueryAll($query);

        }else{

            if($sDate <> "")
            {
                $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

                $query  =   'SELECT t2.client_id, t2.name, t2.phone_main, (SELECT name FROM data_objections WHERE id = t1.objection_id) AS objection, t1.service_id, t1.created_at FROM cp_leads_serv_objections AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.service_id = "'.$serv.'" AND '.$fDate.' GROUP BY t1.objection_id ORDER BY t1.created_at DESC LIMIT 1000';

                $ObjTemp    =   DBSmart::DBQueryAll($query);

            }else{  $ObjTemp   =   false;   }

        }

        if($ObjTemp)
        {
            foreach ($ObjTemp as $t => $tmp) 
            {

                $iData[$t]   =   [
                    'client'    =>  $tmp['client_id'],
                    'name'      =>  $tmp['name'],
                    'phone'     =>  $tmp['phone_main'],
                    'objection' =>  $tmp['objection'],
                    'service'   =>  $tmp['service_id'],
                    'date'      =>  ($tmp['created_at'] <> '') ? $_replace->ShowDate($tmp['created_at']) : ''
                ];            
            }
            return $iData;

        }else{  return false;   }
    
    }

////////////////////////////// Television PR //////////////////////////////

    public static function BasicTvPR($info) 
    {
        $_replace   =   new Config();

        $sTable     =   'cp_service_pr_tv_tmp';
        $sWhere     =   '';

        $sTv        =   $sDeco      =   $sDvr       =   "";
        $sPackage   =   $sProvider  =   $sPayment   =   "";
        $sStatusS   =   "";
        

        $sDate      =   (($info['tv_fini'] <> "") AND ($info['tv_fend'] <> "")) 
            ?   ' AND '. Search::BetweenQuery($info, 'tv_fini', 'tv_fend', 't1.created_at') 
            :   (($info['tv_fini'] <> "") ? Search::SelectQuery($info, 'tv_fini', 't1.created_at') : '');

        $sInfo       =   [
            'sDeco'     =>  ($info['tv_decos'] <> '')       ?   Search::LikeQuerys($info, 'tv_decos',     't1.decoder_id')     : '',
            'sDvr'      =>  ($info['tv_dvr'] <> '')         ?   Search::LikeQuerys($info, 'tv_dvr',       't1.dvr_id')         : '',
            'sPackage'  =>  ($info['tv_package'] <> '')     ?   Search::LikeQuerys($info, 'tv_package',   't1.package_id')     : '',
            'sProvider' =>  ($info['tv_provider'] <> '')    ?   Search::LikeQuerys($info, 'tv_provider',  't1.provider_id')    : '',
            'sPayment'  =>  ($info['tv_payment'] <> '')     ?   Search::LikeQuerys($info, 'tv_payment',   't1.payment_id')     : '',
            'sStatusS'  =>  ($info['tv_StaServ'] <> '')     ?   Search::LikeQuerys($info, 'tv_StaServ',   't1.service_id')     : '',
            'sOrderTy'  =>  ($info['tv_typeorder'] <> '')   ?   Search::LikeQuerys($info, 'tv_typeorder', 't1.type_order_id')  : '',
        ];

        $sWhere =   "AND (";
        foreach ($sInfo as $i => $inf) 
        { 
            if($inf <> "")
            $sWhere.=($inf <> "") ? $inf." OR " : ""; }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere.=')';

        if($sWhere <> "AN)" )
        {
            $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

            $query  =   'SELECT t1.client_id, t2.name, t2.phone_main, t1.tv, 
                (SELECT name FROM data_package WHERE id = t1.package_id) AS package, 
                (SELECT name FROM data_provider WHERE id = t1.provider_id) AS provider, 
                (SELECT name FROM data_payment  WHERE id = t1.payment_id) AS payment, 
                (SELECT name FROM data_status_service  WHERE id = t1.service_id) AS service,
                (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, 
                t1.created_at FROM cp_service_pr_tv_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) '.$sWhere.' AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';
            $leadsTmp   =   DBSmart::DBQueryAll($query);

        }else{

            if($sDate <> "")
            {
                $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

                $query      =   'SELECT t1.client_id, t2.name, t2.phone_main, t1.tv, 
                (SELECT name FROM data_package WHERE id = t1.package_id) AS package, 
                (SELECT name FROM data_provider WHERE id = t1.provider_id) AS provider, 
                (SELECT name FROM data_payment  WHERE id = t1.payment_id) AS payment, 
                (SELECT name FROM data_status_service  WHERE id = t1.service_id) AS service,
                (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, 
                t1.created_at FROM cp_service_pr_tv_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) 
                AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';

                $leadsTmp   =   DBSmart::DBQueryAll($query);

            }else{  $leadsTmp   =   false;  }
        }

        if($leadsTmp <> false)
        {
            foreach ($leadsTmp as $t => $tmp) 
            {
                $iData[$t]   =   [
                    'client'    =>  $tmp['client_id'],
                    'name'      =>  $tmp['name'],
                    'phone'     =>  $tmp['phone_main'],
                    'tv'        =>  $tmp['tv'],
                    'package'   =>  $tmp['package'],
                    'provider'  =>  $tmp['provider'],
                    'payment'   =>  $tmp['payment'],
                    'status'    =>  $tmp['service'],
                    'type'      =>  $tmp['type'],
                    'date'      =>  ($tmp['created_at'] <> '') ? $_replace->ShowDate($tmp['created_at']) : ''
                ];
            
            }
            return $iData;

        }else{  return false;   }
    
    }

////////////////////////////// Security PR //////////////////////////////

    public static function BasicSecPR($info) 
    {
        $_replace   =   new Config();

        $sTable     =   'cp_service_pr_sec_tmp';
        $sWhere     =   '';

        $sCam       =   $sPrev      =   $sDvr       =   "";
        $sPayment   =   $sStatusS   =   "";

        $sDate      =   (($info['sec_fini'] <> "") AND ($info['sec_fend'] <> "")) 
            ?   ' AND '. Search::BetweenQuery($info, 'sec_fini', 'sec_fend', 't1.created_at') 
            :   (($info['sec_fini'] <> "") ? Search::SelectQuery($info, 'sec_fini', 't1.created_at') : '');

        $sInfo       =   [
            'sCam'      =>  ($info['sec_cam'] <> '')        ?   Search::LikeQuerys($info, 'sec_cam',      't1.cameras_id')     : '',
            'sPrev'     =>  ($info['sec_prev'] <> '')       ?   Search::LikeQuerys($info, 'sec_prev',     't1.previously_id')  : '',
            'sDvr'      =>  ($info['sec_dvr'] <> '')        ?   Search::LikeQuerys($info, 'sec_dvr',      't1.dvr_id')         : '',
            'sPayment'  =>  ($info['sec_payment'] <> '')    ?   Search::LikeQuerys($info, 'sec_payment',  't1.payment_id')     : '',
            'sStatusS'  =>  ($info['sec_StaServ'] <> '')    ?   Search::LikeQuerys($info, 'sec_StaServ',  't1.service_id')     : '',
            'sOrderTy'  =>  ($info['sec_typeorder'] <> '')  ?   Search::LikeQuerys($info, 'sec_typeorder','t1.type_order_id')  : ''
        ];


        $sWhere =   "AND (";
        foreach ($sInfo as $i => $inf) 
        { 
            if($inf <> "")
            $sWhere.=($inf <> "") ? $inf." OR " : ""; }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere.=')';


        if($sWhere <> "AN)" )
        {
            $fDate      =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

            $query1     =   'SELECT t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_camera WHERE id = t1.cameras_id) AS cameras, (IF(t1.previously_id = "1", "SI", "NO")) AS previously, (SELECT name FROM data_dvr WHERE id = t1.dvr_id) AS dvr, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS service, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, t1.created_at FROM cp_service_pr_sec_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) '.$sWhere.' AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';

            $leadsTmp   =   DBSmart::DBQueryAll($query1);

        }else{

            if($sDate <> "")
            {
                $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

                $query2     =   'SELECT t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_camera WHERE id = t1.cameras_id) AS cameras, (IF(t1.previously_id = "1", "SI", "NO")) AS previously, (SELECT name FROM data_dvr WHERE id = t1.dvr_id) AS dvr, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS service, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, t1.created_at FROM cp_service_pr_sec_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';

                $leadsTmp   =   DBSmart::DBQueryAll($query2);

            }else{  $leadsTmp   =   false;  }
        }

        if($leadsTmp)
        {
            foreach ($leadsTmp as $t => $tmp) 
            {

                $iData[$t]   =   [
                    'client'    =>  $tmp['client_id'],
                    'name'      =>  $tmp['name'],
                    'phone'     =>  $tmp['phone_main'],
                    'prev'      =>  $tmp['previously'],
                    'cam'       =>  $tmp['cameras'],
                    'dvr'       =>  $tmp['dvr'],
                    'payment'   =>  $tmp['payment'],
                    'status'    =>  $tmp['service'],
                    'type'      =>  $tmp['type'],
                    'date'      =>  ($tmp['created_at'] <> '')  ?   $_replace->ShowDate($tmp['created_at'])     : ''
                ];
            }
            return $iData;

        }else{  return false;   }
    
    }

////////////////////////////// Internet PR //////////////////////////////

    public static function BasicNetPR($info) 
    {
        $_replace   =   new Config();

        $sTable     =   'cp_service_pr_int_tmp';
        $sWhere     =   '';

        $sResI      =   $sResC  =   "";
        
        $sDate      =   (($info['net_fini'] <> "") AND ($info['net_fend'] <> "")) 
            ?   ' AND '. Search::BetweenQuery($info, 'net_fini', 'net_fend', 't1.created_at') 
            :   (($info['net_fini'] <> "") ? Search::SelectQuery($info, 'net_fini', 't1.created_at') : '');

        $sInfo      =   [
            'sResI'     =>  ($info['net_ires'] <> '')       ? Search::LikeQuerys($info, 'net_ires',     't1.int_res_id')    : '',
            'sResC'     =>  ($info['net_icom'] <> '')       ? Search::LikeQuerys($info, 'net_icom',     't1.int_com_id')    : '',
            'sPayment'  =>  ($info['net_payment'] <> '')    ? Search::LikeQuerys($info, 'net_payment',  't1.payment_id')    : '',
            'sStatusS'  =>  ($info['net_StaServ'] <> '')    ? Search::LikeQuerys($info, 'net_StaServ',  't1.service_id')    : '',
            'sOrderTy'  =>  ($info['net_typeorder'] <> '')  ? Search::LikeQuerys($info, 'net_typeorder','t1.type_order_id') : '',
        ];

        $sWhere =   "AND (";
        foreach ($sInfo as $i => $inf) 
        { 
            if($inf <> "")
            $sWhere.=($inf <> "") ? $inf." OR " : ""; }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere.=')';

        if($sWhere <> "AN)" )
        {
            $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

            $query1     =   'SELECT t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS service, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, t1.created_at FROM cp_service_pr_int_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) '.$sWhere.' AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';

            $leadsTmp   =   DBSmart::DBQueryAll($query1);

        }else{

            if($sDate <> "")
            {
                $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

                $query2      =   'SELECT t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS service, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, t1.created_at FROM cp_service_pr_int_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';

                $leadsTmp   =   DBSmart::DBQueryAll($query2);

            }else{  $leadsTmp   =   false;  }
        }

        if($leadsTmp)
        {
            foreach ($leadsTmp as $t => $tmp) 
            {
                $iData[$t]   =   [
                    'client'    =>  $tmp['client_id'],
                    'name'      =>  $tmp['name'],
                    'phone'     =>  $tmp['phone_main'],
                    'ires'      =>  $tmp['int_res'],
                    'icom'      =>  $tmp['int_com'],
                    'pres'      =>  $tmp['pho_res'],
                    'pcom'      =>  $tmp['pho_com'],
                    'payment'   =>  $tmp['payment'],
                    'status'    =>  $tmp['service'],
                    'date'      =>  ($tmp['created_at'] <> "")     ? $_replace->showDate($tmp['created_at']) : ''
                ];
            }
            return $iData;
            
        }else{  return false;   }
    
    }

////////////////////////////// Internet VZ //////////////////////////////

    public static function BasicNetVz($info) 
    {
        $_replace   =   new Config();

        $sTable     =   'cp_service_vzla_int_tmp';
        $sWhere     =   '';

        $sResI      =   $sResC  =   "";
        
        $sDate      =   (($info['net_fini'] <> "") AND ($info['net_fend'] <> "")) 
            ?   ' AND '. Search::BetweenQuery($info, 'net_fini', 'net_fend', 't1.created_at') 
            :   (($info['net_fini'] <> "") ? Search::SelectQuery($info, 'net_fini', 't1.created_at') : '');

        $sInfo      =   [
            'sResI'     =>  ($info['net_ires'] <> '')       ? Search::LikeQuerys($info, 'net_ires',     't1.int_res_id')    : '',
            'sResC'     =>  ($info['net_icom'] <> '')       ? Search::LikeQuerys($info, 'net_icom',     't1.int_com_id')    : '',
            'sPayment'  =>  ($info['net_payment'] <> '')    ? Search::LikeQuerys($info, 'net_payment',  't1.payment_id')    : '',
            'sStatusS'  =>  ($info['net_StaServ'] <> '')    ? Search::LikeQuerys($info, 'net_StaServ',  't1.service_id')    : '',
            'sOrderTy'  =>  ($info['net_typeorder'] <> '')  ? Search::LikeQuerys($info, 'net_typeorder','t1.type_order_id') : '',
        ];

        $sWhere =   "AND (";
        foreach ($sInfo as $i => $inf) 
        { 
            if($inf <> "")
            $sWhere.=($inf <> "") ? $inf." OR " : ""; }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere.=')';

        if($sWhere <> "AN)" )
        {
            $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

            $query1     =   'SELECT t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS service, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, t1.created_at FROM cp_service_vzla_int_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) '.$sWhere.' AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';

            $leadsTmp   =   DBSmart::DBQueryAll($query1);

        }else{

            if($sDate <> "")
            {
                $fDate  =   (substr($sDate, 1,3) == "AND") ? substr_replace($sDate, '', 1, 3) : $sDate ;

                $query2      =   'SELECT t1.client_id, t2.name, t2.phone_main, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS service, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type, t1.created_at FROM cp_service_vzla_int_tmp AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND '.$fDate.' GROUP BY t1.client_id ORDER BY t1.created_at DESC LIMIT 1000';

                $leadsTmp   =   DBSmart::DBQueryAll($query2);

            }else{  $leadsTmp   =   false;  }
        }

        if($leadsTmp)
        {
            foreach ($leadsTmp as $t => $tmp) 
            {
                $iData[$t]   =   [
                    'client'    =>  $tmp['client_id'],
                    'name'      =>  $tmp['name'],
                    'phone'     =>  $tmp['phone_main'],
                    'ires'      =>  $tmp['int_res'],
                    'icom'      =>  $tmp['int_com'],
                    'pres'      =>  $tmp['pho_res'],
                    'pcom'      =>  $tmp['pho_com'],
                    'payment'   =>  $tmp['payment'],
                    'status'    =>  $tmp['service'],
                    'date'      =>  ($tmp['created_at'] <> "")     ? $_replace->showDate($tmp['created_at']) : ''
                ];
            }
            return $iData;
            
        }else{  return false;   }
    
    }

//////////////////////////// HTML DataBasic /////////////////////////////

    public static function HtmlDBQuery($info)
    {
        $_replace   =   new Config();

        $html="";
        $html.='<div class="row">';
            $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">';
                $html.='<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" role="widget">';
                    $html.='<header role="heading"><div class="jarviswidget-ctrls" role="menu">   </div>';
                    $html.='<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>';
                    $html.='<h2> LISTADO DE CLIENTES </h2>';
                    $html.='<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>';
                    $html.='<div role="content">';

                    $html.='<table id="SearchDataBasic" class="table table-striped table-bordered table-hover" width="100%">';
                    $html.='<thead>';
                    $html.='<tr>';
                    $html.='<th data-hide="user"> Id</th>';
                    $html.='<th data-hide="user"> Nombre</th>';
                    $html.='<th data-hide="user"> Telefono</th>';
                    $html.='<th data-hide="user"> Cumplea&ntilde;os</th>';
                    $html.='<th data-hide="user"> Ciudad</th>';
                    $html.='<th data-hide="user"> Casa</th>';
                    $html.='<th data-hide="user"> Techo</th>';
                    $html.='<th data-hide="user"> Nivel</th>';
                    $html.='<th data-hide="user"> Operador</th>';
                    $html.='<th data-hide="user"> Creado</th>';
                    $html.='<th data-hide="user"> Accion</th>';
                    $html.='</tr>';
                    $html.='</thead>';
                    $html.='<tbody >';
                    if($info <> false)
                    {
                        foreach ($info as $k => $val) 
                        {
                            $html.='<tr>';
                            $html.='<td>'.$val['client'].'</td>';
                            $html.='<td>'.mb_convert_encoding($val['name'], 'UTF-8', 'UTF-8').'</td>';
                            $html.='<td>'.$val['phone'].'</td>';
                            $html.='<td>'.$val['birthday'].'</td>';
                            $html.='<td>'.$val['town'].'</td>';
                            $html.='<td>'.$val['house'].'</td>';
                            $html.='<td>'.$val['roof'].'</td>';
                            $html.='<td>'.$val['level'].'</td>';
                            $html.='<td>'.$val['operator'].'</td>';
                            $html.='<td>'.$val['created'].'</td>';
                            $html.='<td><div class="btn-group"><a class="btn btn-primary btn-xs" href="/leads/view/'.$val['client'].'"><i class="fa fa-eye"></i> VER </a></div></td>';
                            $html.='</tr>';
                        }
                        $status = true;
                    }else{
                        $html.='<tr>';
                        $html.='<td colspan="10" style="text-align: center;"><h3>Sin Registros Encontrados</h3></td>';
                        $html.='</tr>';
                        $status = false;
                    }
                    $html.='</tbody>';
                    $html.='</table>';

                    $html.='</div>';
                $html.='</div>';
            $html.='</div>';
        $html.='</div>';
        return ['status' => $status, 'html' => $html];
    
    }

//////////////////////////// HTML Television PR /////////////////////////////

    public static function HtmlBasicTvPR($info)
    {
        $_replace   =   new Config();

        $html="";
        $html.='<div class="row">';
            $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">';
                $html.='<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" role="widget">';
                    $html.='<header role="heading"><div class="jarviswidget-ctrls" role="menu">   </div>';
                    $html.='<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>';
                    $html.='<h2> LISTADO DE CLIENTES </h2>';
                    $html.='<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>';
                    $html.='<div role="content">';

                    if($info <> false)
                    {
                        $html.='<table id="SearchTVService" class="table table-striped table-bordered table-hover" width="100%">';
                        $html.='<thead>';
                        $html.='<tr>';

                        $html.='<th data-hide="user"> Id</th>';
                        $html.='<th data-hide="user"> Nombre</th>';
                        $html.='<th data-hide="user"> Telefono</th>';
                        $html.='<th data-hide="user"> TV</th>';
                        $html.='<th data-hide="user"> Paquete</th>';
                        $html.='<th data-hide="user"> Provider</th>';
                        $html.='<th data-hide="user"> Pago</th>';
                        $html.='<th data-hide="user"> Status</th>';
                        $html.='<th data-hide="user"> Tipo</th>';
                        $html.='<th data-hide="user"> Fecha</th>';
                        $html.='<th data-hide="user"> Accion</th>';

                        $html.='</tr>';
                        $html.='</thead>';
                        $html.='<tbody >';
                        foreach ($info as $k => $val) 
                        {
                            $html.='<tr>';
                            $html.='<td>'.$val['client'].'</td>';
                            $html.='<td>'.mb_convert_encoding($val['name'], 'UTF-8', 'UTF-8').'</td>';
                            $html.='<td>'.$val['phone'].'</td>';
                            $html.='<td>'.$val['tv'].'</td>';
                            $html.='<td>'.$val['package'].'</td>';
                            $html.='<td>'.$val['provider'].'</td>';
                            $html.='<td>'.$val['payment'].'</td>';
                            $html.='<td>'.$val['status'].'</td>';
                            $html.='<td>'.$val['type'].'</td>';
                            $html.='<td>'.$val['date'].'</td>';
                            $html.='<td><div class="btn-group"><a class="btn btn-primary btn-xs" href="/leads/view/'.$val['client'].'"><i class="fa fa-eye"></i> VER </a></div></td>';

                            $html.='</tr>';
                        }
                        $html.='</tbody>';
                        $html.='</table>';
                        $status = true;
                    }else{
                        $html.='<table id="SearchTVService" class="table table-striped table-bordered table-hover" width="100%">';
                        $html.='<thead>';
                        $html.='<tr>';

                        $html.='<th data-hide="user"> Id</th>';
                        $html.='<th data-hide="user"> Nombre</th>';
                        $html.='<th data-hide="user"> Telefono</th>';
                        $html.='<th data-hide="user"> TV</th>';
                        $html.='<th data-hide="user"> Paquete</th>';
                        $html.='<th data-hide="user"> Provider</th>';
                        $html.='<th data-hide="user"> Pago</th>';
                        $html.='<th data-hide="user"> Status Servicio</th>';
                        $html.='<th data-hide="user"> Tipo</th>';
                        $html.='<th data-hide="user"> Fecha</th>';
                        $html.='<th data-hide="user"> Accion</th>';

                        $html.='</tr>';
                        $html.='</thead>';
                        $html.='<tbody >';
                        $html.='<tr>';
                        $html.='<td colspan="11" style="text-align: center;"><h3>Sin Registros Encontrados</h3></td>';
                        $html.='</tr>';
                        $html.='</tbody>';
                        $html.='</table>';
                        $status = false;
                    }

                    $html.='</div>';
                $html.='</div>';
            $html.='</div>';
        $html.='</div>';

        return ['status' => $status, 'html' => $html];
    
    }

//////////////////////////// HTML Television PR /////////////////////////////

    public static function HtmlBasicSecPR($info)
    {
        $_replace   =   new Config();
        $html = "";

        $html.='<div class="row">';
        $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">';
        $html.='<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" role="widget">';
        $html.='<header role="heading"><div class="jarviswidget-ctrls" role="menu">   </div>';
        $html.='<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>';
        $html.='<h2> LISTADO DE CLIENTES </h2>';
        $html.='<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>';
        $html.='<div role="content">';
        $html.='<div class="widget-body widget-hide-overflow no-padding">';
        $html.='<div id="chat-body" class="chat-body custom-scroll" style="height: 700px;">';
        $html.='<div class="table-responsive" style="margin-top: 10px;">';
        $html.='<table id="SearchSECService" class="table table-striped table-bordered table-hover" width="100%">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th data-hide="user"> Id</th>';
        $html.='<th data-hide="user"> Nombre</th>';
        $html.='<th data-hide="user"> Telefono</th>';
        $html.='<th data-hide="user"> Sist. Previo?</th>';
        $html.='<th data-hide="user"> Camaras</th>';
        $html.='<th data-hide="user"> Dvr</th>';
        $html.='<th data-hide="user"> Pago</th>';
        $html.='<th data-hide="user"> Status</th>';
        $html.='<th data-hide="user"> Tipo</th>';
        $html.='<th data-hide="user"> Fecha</th>';
        $html.='<th data-hide="user"> Accion</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';

        if($info <> false)
        {
            foreach ($info as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td>'.$val['client'].'</td>';
                $html.='<td>'.$val['name'].'</td>';
                $html.='<td>'.$val['phone'].'</td>';
                $html.='<td>'.$val['prev'].'</td>';
                $html.='<td>'.$val['cam'].'</td>';
                $html.='<td>'.$val['dvr'].'</td>';
                $html.='<td>'.$val['payment'].'</td>';
                $html.='<td>'.$val['status'].'</td>';
                $html.='<td>'.$val['type'].'</td>';
                $html.='<td>'.$val['date'].'</td>';
                $html.='<td><div class="btn-group"><a class="btn btn-primary btn-xs" href="/leads/view/'.$val['client'].'"><i class="fa fa-eye"></i> VER </a></div></td>';
                $html.='</tr>';

            }
            $status = true;
        }
        else
        {
            $html.='<tr>';
            $html.='<td colspan="11" style="text-align: center;"><h3>Sin Registros Encontrados</h3></td>';
            $html.='</tr>';
            $status = false;
        }
        $html.='</tbody>';
        $html.='</table>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';

        return ['status' => $status, 'html' => $html];
    
    }

//////////////////////////// HTML Television PR /////////////////////////////

    public static function HtmlBasicNetPR($info)
    {
        $_replace   =   new Config();
        $html = "";

        $html.='<div class="row">';
        $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">';
        $html.='<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" role="widget">';
        $html.='<header role="heading"><div class="jarviswidget-ctrls" role="menu">   </div>';
        $html.='<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>';
        $html.='<h2> LISTADO DE CLIENTES </h2>';
        $html.='<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>';
        $html.='<div role="content">';
        $html.='<div class="widget-body widget-hide-overflow no-padding">';
        $html.='<div id="chat-body" class="chat-body custom-scroll" style="height: 700px;">';
        $html.='<div class="table-responsive" style="margin-top: 10px;">';
        $html.='<table id="SearchINTService" class="table table-striped table-bordered table-hover" width="100%">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th data-hide="user"> Id</th>';
        $html.='<th data-hide="user"> Nombre</th>';
        $html.='<th data-hide="user"> Telefono</th>';
        $html.='<th data-hide="user"> Int. Res.</th>';
        $html.='<th data-hide="user"> Int. Com.</th>';
        $html.='<th data-hide="user"> Pho. Res.</th>';
        $html.='<th data-hide="user"> Pho. Com.</th>';
        $html.='<th data-hide="user"> Metodo de Pago</th>';
        $html.='<th data-hide="user"> Status Service</th>';
        $html.='<th data-hide="user"> Fecha</th>';
        $html.='<th data-hide="user"> Accion</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';

        if($info <> false)
        {
            foreach ($info as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td>'.$val['client'].'</td>';
                $html.='<td>'.$val['name'].'</td>';
                $html.='<td>'.$val['phone'].'</td>';
                $html.='<td>'.$val['ires'].'</td>';
                $html.='<td>'.$val['icom'].'</td>';
                $html.='<td>'.$val['pres'].'</td>';
                $html.='<td>'.$val['pcom'].'</td>';
                $html.='<td>'.$val['payment'].'</td>';
                $html.='<td>'.$val['status'].'</td>';
                $html.='<td>'.$val['date'].'</td>';
                $html.='<td><div class="btn-group"><a class="btn btn-primary btn-xs" href="/leads/view/'.$val['client'].'"><i class="fa fa-eye"></i> VER </a></div></td>';
                $html.='</tr>';
            }
            $status = true;
        }
        else
        {
            $html.='<tr>';
            $html.='<td colspan="9" style="text-align: center;"><h3>Sin Registros Encontrados</h3></td>';
            $html.='</tr>';
            $status = false;
        }
        $html.='</tbody>';
        $html.='</table>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';

        return ['status' => $status, 'html' => $html ];
    
    }

//////////////////////////// HTML Television PR /////////////////////////////
    
    public static function HtmlInfoPr($info)
    {
        $_replace   =   new Config();
        $html = "";
        $html.='<div class="row">';
        $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">';
        $html.='<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" role="widget">';
        $html.='<header role="heading"><div class="jarviswidget-ctrls" role="menu">   </div>';
        $html.='<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>';
        $html.='<h2> LISTADO DE CLIENTES </h2>';
        $html.='<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>';
        $html.='<div role="content">';
        $html.='<div class="widget-body widget-hide-overflow no-padding">';
        $html.='<div id="chat-body" class="chat-body custom-scroll" style="height: 700px;">';
        $html.='<div class="table-responsive" style="margin-top: 10px;">';
        $html.='<table id="ScoreSearchService" class="table table-striped table-bordered table-hover" width="100%">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th data-hide="user"> Ticket</th>';
        $html.='<th data-hide="user"> Cliente</th>';
        $html.='<th data-hide="user"> Nombre</th>';
        $html.='<th data-hide="user"> Telefono</th>';
        $html.='<th data-hide="user"> Score</th>';
        $html.='<th data-hide="user"> Fecha</th>';
        $html.='<th data-hide="user"> Accion</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';
        if($info <> false)
        {
            foreach ($info as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td>'.$val['ticket'].'</td>';
                $html.='<td>'.$val['client'].'</td>';
                $html.='<td>'.$val['name'].'</td>';
                $html.='<td>'.$val['phone'].'</td>';
                $html.='<td>'.$val['score'].'</td>';
                $html.='<td>'.$val['date'].'</td>';
                $html.='<td><div class="btn-group"><a class="btn btn-primary btn-xs" href="/leads/view/'.$val['client'].'"><i class="fa fa-eye"></i> VER </a></div></td>';
                $html.='</tr>';
            }
            $status = true;
        }
        else
        {
            $html.='<tr>';
            $html.='<td colspan="7" style="text-align: center;"><h3>Sin Registros Encontrados</h3></td>';
            $html.='</tr>';
            $status = false;
        }
        $html.='</tbody>';
        $html.='</table>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        return ['status' => $status, 'html' => $html];

    }

    public static function HtmlObjectionPr($info)
    {
        $_replace   =   new Config();
        $html = "";
        $html.='<div class="row">';
        $html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">';
        $html.='<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" role="widget">';
        $html.='<header role="heading"><div class="jarviswidget-ctrls" role="menu">   </div>';
        $html.='<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>';
        $html.='<h2> LISTADO DE CLIENTES </h2>';
        $html.='<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>';
        $html.='<div role="content">';
        $html.='<div class="widget-body widget-hide-overflow no-padding">';
        $html.='<div id="chat-body" class="chat-body custom-scroll" style="height: 700px;">';
        $html.='<div class="table-responsive" style="margin-top: 10px;">';
        $html.='<table id="objectionSearchService" class="table table-striped table-bordered table-hover" width="100%">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th data-hide="user"> Cliente</th>';
        $html.='<th data-hide="user"> Nombre</th>';
        $html.='<th data-hide="user"> Telefono</th>';
        $html.='<th data-hide="user"> Objecion</th>';
        $html.='<th data-hide="user"> Fecha</th>';
        $html.='<th data-hide="user"> Accion</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';
        if($info <> false)
        {
            foreach ($info as $k => $val) 
            {
                $html.='<tr>';
                $html.='<td>'.$val['client'].'</td>';
                $html.='<td>'.$val['name'].'</td>';
                $html.='<td>'.$val['phone'].'</td>';
                $html.='<td>'.$val['objection'].'</td>';
                $html.='<td>'.$val['date'].'</td>';
                $html.='<td style="text-align: center;">
                    <div class="btn-group"><a class="btn btn-primary btn-xs" href="/leads/view/'.$val['client'].'"><i class="fa fa-eye"></i> VER </a></div>
                    <div class="btn-group"><a class="btn btn-primary btn-xs" onclick="ViewObjections('.(int)$val['client'].', '.(int)$val['service'].');"><i class="fa fa-eye"></i> Objeciones </a></div>
                    </td>';
                $html.='</tr>';
            }
            $status = true;
        }
        else
        {
            $html.='<tr>';
            $html.='<td colspan="6" style="text-align: center;"><h3>Sin Registros Encontrados</h3></td>';
            $html.='</tr>';
            $status = false;
        }
        $html.='</tbody>';
        $html.='</table>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        $html.='</div>';
        return ['status' => $status, 'html' => $html];

    }

//////////////////////////// HTML DataBasic /////////////////////////////

    public static function HtmlObjection($info)
    {
        $_replace   =   new Config();
        $html = '';
        $html.='<div class="table-responsive" style="margin-top: 10px;">';
        $html.='<table class="table table-striped table-bordered table-hover" width="100%">';
        $html.='<thead>';
        $html.='<tr>';
        $html.='<th data-hide="user"> Objecion</th>';
        $html.='<th data-hide="user"> Fecha</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';

        foreach ($info as $i => $inf) 
        {
            $html.='<tr>';    
            $html.='<td>'.$inf['objection'].'</td>';
            $html.='<td>'.$inf['date'].'</td>';
            $html.='</tr>';
        }
        $html.='</tbody>';
        $html.='</table>';
        $html.='</div>';
        return $html;
    }

}