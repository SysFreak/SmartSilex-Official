<?php
namespace App\Models;

use Model;

use App\Models\Marketing;
use App\Models\MarketingTmp;
use App\Models\Leads;
use App\Models\StatusChange;

use App\Lib\Config;
use App\Lib\DBSmart;
use PDO;

class Marketing extends Model 
{

	static $_table 		= 'cp_mkt_leads';

	Public $_fillable 	= array('client_id', 'medium_id', 'objetive_id', 'post_id', 'services_id', 'campaign_id', 'created_at');

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function DataOperator($info)
	{
		$service 	=	Service::GetService();

 		$date 		=	date("Y-m-d");

		foreach ($service as $s => $ser) 
		{
			if($ser['status_id'] == 1)	{	$serv[$s] = ['id'	=>	$ser['id'] ];	}
		}

        foreach ($info as $o => $ope) 
        {

			$query		=	'SELECT COUNT(IF(service_id="'.$serv[1]['id'].'",1,NULL)) AS tv, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[1]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at LIKE "'.$date.'%") AS rtv, COUNT(IF(service_id="'.$serv[2]['id'].'",1,NULL)) AS sec, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[2]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at LIKE "'.$date.'%") AS rsec, COUNT(IF(service_id="'.$serv[3]['id'].'",1,NULL)) AS net, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[3]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at LIKE "'.$date.'%") AS rnet, COUNT(IF(service_id="'.$serv[3]['id'].'",1,NULL)) AS mr, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[3]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at LIKE "'.$date.'%") AS rmr, (SELECT COUNT(*) FROM cp_ope_assig WHERE process_by = "'.$ope['id'].'" AND assigned_at LIKE "'.$date.'%") AS total, (SELECT COUNT(*) FROM cp_ope_assig WHERE process_by = "'.$ope['id'].'" AND status_id = "1" AND assigned_at LIKE "'.$date.'%") AS tresp FROM cp_ope_assig WHERE process_by = "'.$ope['id'].'" AND assigned_at LIKE "'.$date.'%"';

	    	$result     =   DBSmart::DBQuery($query);

        	$res[$o]	=	[
	        	'id'		=>	$ope['id'],
	        	'name'		=>	$ope['name'],
	        	'username'	=>	$ope['username'],
	        	'team'		=>	$ope['team'],
	        	'tvpr'		=>	$ope['tvpr'],
	        	'wrlpr'		=>	$ope['wrlpr'],
	        	'secpr'		=>	$ope['secpr'],
	        	'netpr'		=>	$ope['netpr'],
				'tvspr'		=>	($result['tv'])		? $result['tv'] 	: "0",
				'tvsprR'	=>	($result['rtv'])	? $result['rtv'] 	: "0",
	        	'secspr'	=>	($result['sec']) 	? $result['sec'] 	: "0",
	        	'secsprR'	=>	($result['rsec']) 	? $result['rsec'] 	: "0",
	        	'netspr'	=>	($result['net']) 	? $result['net'] 	: "0",
	        	'netsprR'	=>	($result['rnet']) 	? $result['rnet'] 	: "0",
	        	'wrlspr'	=>	($result['mr']) 	? $result['mr'] 	: "0",
	        	'wrlsprR'	=>	($result['rmr']) 	? $result['rmr'] 	: "0",
	        	'total'		=>	($result['total']) 	? $result['total'] 	: "0",
	        	'tresp'		=>	($result['tresp']) 	? $result['tresp'] 	: "0",
        	];

        }
       return 	$res;	
	}	

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function DataOperatorByDate($info, $dates, $type)
	{

		$service 	=	Service::GetService();

		foreach ($service as $s => $ser) 
		{
			if($ser['status_id'] == 1)	{	$serv[$s] = ['id'	=>	$ser['id'] ];	}
		}

        foreach ($info as $o => $ope) 
        {

        	$query 		= 	'SELECT COUNT(IF(service_id="'.$serv[1]['id'].'",1,NULL)) AS tv, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[1]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59") AS rtv, COUNT(IF(service_id="'.$serv[2]['id'].'",1,NULL)) AS sec, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[2]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59") AS rsec, COUNT(IF(service_id="'.$serv[3]['id'].'",1,NULL)) AS net, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[3]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59") AS rnet, COUNT(IF(service_id="'.$serv[4]['id'].'",1,NULL)) AS mr, (SELECT COUNT(*) FROM cp_ope_assig WHERE service_id = "'.$serv[3]['id'].'" AND status_id ="1" AND process_by = "'.$ope['id'].'" AND assigned_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59") AS rmr, (SELECT COUNT(*) FROM cp_ope_assig WHERE process_by = "'.$ope['id'].'" AND assigned_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59") AS total, (SELECT COUNT(*) FROM cp_ope_assig WHERE process_by = "'.$ope['id'].'" AND status_id = "1" AND assigned_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59") AS tresp FROM cp_ope_assig WHERE process_by = "'.$ope['id'].'" AND assigned_at BETWEEN "'.$dates['dateIni'].' 00:00:00" AND "'.$dates['dateEnd'].' 23:59:59"';

	    	$result     =   DBSmart::DBQuery($query);

        	$res[$o]	=	[
	        	'id'		=>	$ope['id'],
	        	'name'		=>	$ope['name'],
	        	'username'	=>	$ope['username'],
	        	'team'		=>	$ope['team'],
	        	'tvpr'		=>	$ope['tvpr'],
	        	'wrlpr'		=>	$ope['wrlpr'],
	        	'secpr'		=>	$ope['secpr'],
	        	'netpr'		=>	$ope['netpr'],
				'tvspr'		=>	($result['tv'])		? $result['tv'] 	: "0",
				'tvsprR'	=>	($result['rtv'])	? $result['rtv'] 	: "0",
	        	'secspr'	=>	($result['sec']) 	? $result['sec'] 	: "0",
	        	'secsprR'	=>	($result['rsec']) 	? $result['rsec'] 	: "0",
	        	'netspr'	=>	($result['net']) 	? $result['net'] 	: "0",
	        	'netsprR'	=>	($result['rnet']) 	? $result['rnet'] 	: "0",
	        	'wrlspr'	=>	($result['mr']) 	? $result['mr'] 	: "0",
	        	'wrlsprR'	=>	($result['rmr']) 	? $result['rmr'] 	: "0",
	        	'total'		=>	($result['total']) 	? $result['total'] 	: "0",
	        	'tresp'		=>	($result['tresp']) 	? $result['tresp'] 	: "0",
        	];
        }
       return $res;
	}


////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// public static function SaveDB($info)
	// {
	// 	$date = date('Y-m-d H:i:s', time());
		
	// 	$_replace 	= new Config();

	// 	$lead = Marketing::create();

	// 	if($lead)
	// 	{	
	// 		$lead->client_id			= (isset($info['c_name'])) 			? strtoupper($_replace->deleteTilde($info['c_name'])) 			: "" ;
	// 		$lead->medium_id 			= (isset($info['mkt_medium'])) 		? strtoupper($_replace->deleteTilde($info['mkt_medium'])) 		: "" ;
	// 		$lead->objetive_id 			= (isset($info['mkt_objective']))	? strtoupper($_replace->deleteTilde($info['mkt_objective']))	: "" ;
	// 		$lead->post_id 				= (isset($info['mkt_post'])) 		? strtoupper($_replace->deleteTilde($info['mkt_post'])) 		: "" ;
	// 		$lead->services_id 			= (isset($info['mkt_service']))		? strtoupper($_replace->deleteTilde($info['mkt_service'])) 		: "" ;
	// 		$lead->campaign_id 			= (isset($info['mkt_campaign'])) 	? strtoupper($_replace->deleteTilde($info['mkt_campaign'])) 	: "" ;
	// 		$lead->created_at 			= $date;
			
	// 		return ($lead->save()) ? true : false;

	// 	}else{

	// 		return false;
	// 	}
	
	// }

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function LoadHtml($info)
	{
		$html = '';

		$html.='<table class="table table-striped table-bordered table-hover" width="100%">';
		$html.='<tbody style="font-size: 10px;">';
		$html.='<tr style="text-align: center;">';
		$html.='<td style="width: 10px;">DISP</td>';
		$html.='<td style="width: 10px;">EJECUTIVO</td>';
		$html.='<td style="width: 10px;">TEAM</td>';
		$html.='<td style="width: 10px;">TV</td>';
		$html.='<td style="width: 10px;">SEC</td>';
		$html.='<td style="width: 10px;">NET</td>';
		$html.='<td style="width: 10px;">WRL</td>';
		$html.='<td style="width: 10px;">TV</td>';
		$html.='<td style="width: 10px;">R</td>';
		$html.='<td style="width: 10px;">WRL</td>';
		$html.='<td style="width: 10px;">R</td>';
		$html.='<td style="width: 10px;">INT</td>';
		$html.='<td style="width: 10px;">R</td>';
		$html.='<td style="width: 10px;">SEC</td>';
		$html.='<td style="width: 10px;">R</td>';
		$html.='<td style="width: 10px;">Total</td>';
		$html.='<td style="width: 10px;">R</td>';
		$html.='</tr>';
		
		foreach ($info as $k => $val) 
		{	
		
			$sta 	=	(StatusChange::GetStatusChangeByUser($val['id']) == 1) 
			?	'<td style="width: 10px; background: #008000ad; color: white;"><i class="fa fa-check"></i></td>'
			: 	'<td style="width: 10px; background: #ff0000b3; color: white;"><i class="fa fa-times"></i></td>';
			
			$tvpr 	=	($val['tvpr'] == "on") 
				? '<td style="width: 10px; background: blue; color: white;"><i class="fa fa-check"></i>' 
				: '<td style="width: 10px;"> <i class="fa fa-times"></i></td>'; 

			$wrlpr 	=	($val['wrlpr'] == "on") 
				? '<td style="width: 10px; background: darkmagenta; color: white;"> <i class="fa fa-check"></i>' 
				: '<td style="width: 10px;"> <i class="fa fa-times"></i></td>'; 

			$secpr 	=	($val['secpr'] == "on") 
				? '<td style="width: 10px; background: red; color: white;"><i class="fa fa-check"></i>' 
				: '<td style="width: 10px;"> <i class="fa fa-times"></i></td>'; 

			$netpr 	=	($val['netpr'] == "on") 
				? '<td style="width: 10px; background: green; color: white;"><i class="fa fa-check"></i>' 
				: '<td style="width: 10px;"> <i class="fa fa-times"></i></td>'; 


			$html.='<tr style="text-align: center;">';
			$html.=''.$sta.'';
			$html.='<td style="width: 15px;">'.$val['username'].'</td>';
			$html.='<td style="width: 15px;">'.$val['team'].'</td>';
			$html.=''.$tvpr.'</td>';
			$html.=''.$secpr.'</td>';
			$html.=''.$netpr.'</td>';
			$html.=''.$wrlpr.'</td>';
			$html.='<td style="width: 10px; font-size: 15px; background: #E8E8E8;"><strong>'.$val['tvspr'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px;"><strong>'.$val['tvsprR'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px; background: #E8E8E8;"><strong>'.$val['wrlspr'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px;"><strong>'.$val['wrlsprR'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px; background: #E8E8E8;"><strong>'.$val['secspr'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px;"><strong>'.$val['secsprR'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px; background: #E8E8E8;"><strong>'.$val['netspr'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px;"><strong>'.$val['netsprR'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px; background: #E8E8E8;"><strong>'.$val['total'].'</strong></td>';
			$html.='<td style="width: 10px; font-size: 15px;"><strong>'.$val['tresp'].'</strong></td>';
			$html.='</tr>';

		}

		$html.='</tbody>';
		$html.='</table>';
		return $html;	
	}
	

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function BasicMKT($info)
	{
		$html = '';
		$html.='<div class="modal-body" style="background: aliceblue;">';
		$html.='<div class="row">';
		$html.='<div class="col-sm-12">';
			$html.='<div class="col-sm-6">';
				$html.='<div class="form-group">';
				$html.='<div class="col-md-12">';
				$html.='<select class="form-control" id="country_id" name="country_id" required>';
					foreach ($info as $k => $val) { $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';  }
				$html.='</select>';
				$html.='</div>';
				$html.='</div>';
			$html.='</div>';
		
			$html.='<div class="col-sm-6">';
				$html.='<div class="input-group">';
					$html.='<input type="text" id="LeadType" name="LeadType" value="phone" readonly hidden>';
					$html.='<input type="number" class="form-control" id="phone" name="phone" placeholder="Introduzca Telefono">';
					$html.='<div class="input-group-btn">';
					$html.='<button class="btn btn-default" type="submit">Search</button>';
					$html.='</div>';
				$html.='</div>';
			$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		return $html;
	
	}

	public static function GetHtmlRegSingle($info, $data, $pho, $notes, $country)
	{

		$phone 	= 	($info['phone'] <> "") ? $info['phone'] : $pho;
		$status =	($info['phone'] <> "") ? 'update' : 'new';

		$html 	=	'';
				$html.='<div class="modal-body" style="background: aliceblue;">';
					$html.='<div class="row">';
						$html.='<div class="widget-body">';
							$html.='<hr class="simple">';
							$html.='<ul id="myTab1" class="nav nav-tabs bordered">';
								$html.='<li class="active">';
									$html.='<a href="#s1" data-toggle="tab"><i class="fa fa-fw fa-lg fa-users"></i> Datos del Leads</a>';
								$html.='</li>';
								$html.='<li>';
									$html.='<a href="#s2" data-toggle="tab"><i class="fa fa-fw fa-lg fa-comments"></i> Notas</a>';
								$html.='</li>';
							$html.='</ul>';

							$html.='<div id="myTabContent1" class="tab-content padding-10">';
								$html.='<div class="tab-pane fade in active" id="s1">';
									$html.='<div class="row">';
										$html.='<input type="text" id="LeadType" name="LeadType" value="'.$status.'" readonly hidden>';
										$html.='<input type="text" id="m_client" name="m_client" value="'.$info['client'].'" readonly hidden/>';
										$html.='<input type="text" id="m_country" name="m_country" value="'.$country.'" readonly hidden/>';
										
										$html.='<div class="col-md-4">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Nombre</label>';
												$html.='<input type="text" id="m_name" name="m_name" class="form-control" placeholder="Nombre" value="'.$info['name'].'" required />';
											$html.='</div>';
										$html.='</div>';
										$html.='<div class="col-md-4">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Telefono</label>';
												$html.='<input type="number" id="m_phone" name="m_phone" class="form-control" placeholder="Telefono" value="'.$phone.'" required />';
											$html.='</div>';
										$html.='</div>';
										$html.='<div class="col-md-4">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Servicio Interesado</label>';
												$html.='<select class="form-control" id="serviceI_id" name="serviceI_id" required>';
												foreach ($data['serviceI'] as $se => $serv) 
												{ 
													if($serv['id'] <> "1")
													{
														$html.='<option value="'.$serv['id'].'">'.$serv['name'].'</option>';
													}else{
														$html.='<option value="'.$serv['id'].'">'.$serv['name'].'</option>';
													}
												}
												$html.='</select>';
											$html.='</div>';
										$html.='</div>';
									$html.='</div>';
									$html.='<div class="row">';
										$html.='<div class="col-md-4">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Origen</label>';
												$html.='<select class="form-control" id="origen_id" name="origen_id" required>';
 												foreach ($data['origen'] as $o => $org) 
												{ 
													if($org['id'] == $info['origen'])
													{
														$html.='<option value="'.$org['id'].'" select>'.$org['name'].'</option>'; 
													}else{
														$html.='<option value="'.$org['id'].'">'.$org['name'].'</option>'; 
													}
												}
												$html.='</select>';
											$html.='</div>';

											$html.='<input type="text" name="objetive_id" id="objetive_id" value="1" hidden>';

											$html.='<div class="form-group">';
												$html.='<label for="category"> Formulario</label>';
												$html.='<select class="form-control" id="form_id" name="form_id" required>';
												foreach ($data['form'] as $o => $oal) 
												{ 
													if($oal['id'] == $info['form'])
													{
														$html.='<option value="'.$oal['id'].'" select>'.$oal['name'].'</option>'; 
													}else{
														$html.='<option value="'.$oal['id'].'">'.$oal['name'].'</option>'; 
													}
												}
												$html.='</select>';
											$html.='</div>';
										$html.='</div>';
										$html.='<div class="col-md-4">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Medio</label>';
												$html.='<select class="form-control" id="medium_id" name="medium_id" required>';
												foreach ($data['medium'] as $m => $mal) 
												{ 
													if($mal['id'] == $info['medium'])
													{
														$html.='<option value="'.$mal['id'].'" select>'.$mal['name'].'</option>'; 
													}else{
														$html.='<option value="'.$mal['id'].'">'.$mal['name'].'</option>'; 
													}
												}
												$html.='</select>';
											$html.='</div>';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Servicio</label>';
												$html.='<select class="form-control" id="service_id" name="service_id" required>';
												foreach ($data['service'] as $s => $sal) 
												{ 
													if($sal['id'] == $info['service'])
													{
														$html.='<option value="'.$sal['id'].'" select>'.$sal['name'].'</option>'; 
													}else{
														$html.='<option value="'.$sal['id'].'">'.$sal['name'].'</option>'; 
													}
												}
												$html.='</select>';
											$html.='</div>';
										$html.='</div>';

										$html.='<input type="text" name="post_id" id="post_id" value="1" hidden>';

										$html.='<div class="col-md-4">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Conversaciones</label>';
												$html.='<select class="form-control" id="conv_id" name="conv_id" required>';
												foreach ($data['post'] as $p => $pal) 
												{ 
													if($pal['id'] == $info['post'])
													{
														$html.='<option value="'.$pal['id'].'" select>'.$pal['name'].'</option>'; 
													}else{
														$html.='<option value="'.$pal['id'].'">'.$pal['name'].'</option>'; 
													}
												}
												$html.='</select>';
											$html.='</div>';
											
										$html.='</div>';
									$html.='</div>';
									$html.='<div class="row">';
										$html.='<div class="col-md-6">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Campa&ntilde;a</label>';
												$html.='<select class="form-control" id="campaign_id" name="campaign_id" required>';
												foreach ($data['campaign'] as $c => $cal) 
												{ 
													if($cal['id'] == $info['campaign'])
													{
														$html.='<option value="'.$cal['id'].'" select>'.$cal['name'].'</option>'; 
													}else{
														$html.='<option value="'.$cal['id'].'">'.$cal['name'].'</option>'; 
													}
												}
												$html.='</select>';
											$html.='</div>';
										$html.='</div>';
										$html.='<div class="col-md-6">';
											$html.='<div class="form-group">';
												$html.='<div id="imgMKT" style="text-align: center;"></div>';
											$html.='</div>';
										$html.='</div>';
									$html.='</div>';
									$html.='<div class="row">';
										$html.='<div class="col-md-6">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Desea Asignar Leads</label>';
												$html.='<select class="form-control" id="lead_assig" name="lead_assig" required>';
													$html.='<option value="0">NO</option>';
													$html.='<option value="1">SI</option>';
												$html.='</select>';
											$html.='</div>';
										$html.='</div>';
										$html.='<div class="col-md-6">';
											$html.='<div class="form-group">';
												$html.='<label for="category"> Operadores</label>';
												$html.='<select class="form-control" id="operator_id" name="operator_id" required>';
												foreach ($data['user'] as $u => $ual) { 
													if($ual['status_id'] == 1)
													{
														$html.='<option value="'.$ual['id'].'">'.$ual['username'].'</option>'; 
													}
												}
												$html.='</select>';
											$html.='</div>';
										$html.='</div>';
									$html.='</div>';
								$html.='</div>';
								$html.='<div class="tab-pane fade" id="s2">';
									$html.='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">';
										$html.='<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" role="widget">';
											$html.='<header role="heading"><div class="jarviswidget-ctrls" role="menu">   </div>';
											$html.='<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>';
											$html.='<h2> NOTAS DEL CLIENTE </h2>';
											$html.='<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>';
											$html.='<div role="content">';
												$html.='<div class="widget-body widget-hide-overflow no-padding">';
													$html.='<div id="chat-body" class="chat-body custom-scroll" style="height: 500px;">';
														$html.=''.$notes.'';
													$html.='</div>';
												$html.='</div>';
											$html.='</div>';
										$html.='</div>';
									$html.='</div>';
								$html.='</div>';
							$html.='</div>';
						$html.='</div>';
					$html.='</div>';
				$html.='</div>';
				$html.='<div class="modal-footer">';
					$html.='<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
					$html.='<button type="submit" class="btn btn-primary">Salvar</button>';
				$html.='</div>';

		return $html;

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function GetHtmlRegMultiple($info)
	{

		$html = '';

		$html.='<div class="modal-body" style="background: aliceblue;">';
		$html.='<div class="row">';
		$html.='<div class="col-sm-12">';
		$html.='<div class="table-responsive">';
		$html.='<table class="table table-bordered" style="font-size: 11px; text-align: center;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th>ID</th>';
		$html.='<th>Nombre</th>';
		$html.='<th>Telefono</th>';
		$html.='<th>Alternativo</th>';
		$html.='<th>Direccion</th>';
		$html.='<th>Accion</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';

		foreach ($info as $k => $val) 
		{
			$html.='<tr>';
			$html.='<td>'.$val['client'].'</td>';
			$html.='<td>'.$val['name'].'</td>';
			$html.='<td>'.$val['phone'].'</td>';
			$html.='<td>'.$val['p_alt'].'</td>';
			$html.='<td>'.$val['add'].'</td>';
			$html.='<td><a style="margin-top: 15px;" onclick="SelectLead('.(int)$val['client'].');" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="Select Lead"><i class="fa fa-check"></i></a></td>';
			$html.='</tr>';

		}

		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='</div>';
		$html.='<div class="modal-footer">';
		$html.='<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>';
		$html.='</div>';

		return $html;
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////


}