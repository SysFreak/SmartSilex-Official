<?php

namespace App\Models;

require '../vendor/autoload.php';

use Model;

use App\Models\Departament;
use App\Models\Did;
use App\Models\Leaders;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\Conexion;

use PDO;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use Carbon\Carbon;

class Leader extends Model {

	public static function GetInfo($info)
	{

        $date 		=	date("Y-m-d");
        $dateEnd 	=	Carbon::yesterday()->format('Y-m-d');
        $dateIni 	=	(substr($date, 5, 2) <= substr($dateEnd, 5, 2)) ? date("Y-m")."-01": date("Y").'-'.(date("m")-1)."-01";

		foreach ($info as $u => $user) 
		{
			$query = 'SELECT count(*) AS total, (SELECT SEC_TO_TIME(SUM(billsec)) ) AS tOnline FROM cdr WHERE dst = "'.$user['ext'].'" AND disposition = "ANSWERED" AND calldate BETWEEN "'.$dateIni.' 00:00:00" AND "'.$dateEnd.' 23:59:59"';

        	$inbound    =  DBSmart::DBQuery($query);

        	$query = 'SELECT count(*) AS total, (SELECT SEC_TO_TIME(SUM(billsec)) ) AS tOnline FROM cdr WHERE (cnum = "'.$user['ext'].'" OR src = "'.$user['ext'].'") AND disposition = "ANSWERED" AND calldate BETWEEN "'.$dateIni.' 00:00:00" AND "'.$dateEnd.' 23:59:59"';

       		$outAnsw    =   DBSmart::DBQuery($query);
       		
        	$query = 'SELECT count(*) AS total, (SELECT SEC_TO_TIME(SUM(duration)) ) AS tOnline FROM cdr WHERE (cnum = "'.$user['ext'].'" OR src = "'.$user['ext'].'") AND disposition = "NO ANSWER" AND calldate BETWEEN "'.$dateIni.' 00:00:00" AND "'.$dateEnd.' 23:59:59"';

       		$outNoAnsw  =   DBSmart::DBQuery($query);
       		
       		$t1 	=	($inbound['tOnline'] <> NULL) 	? $inbound['tOnline'] 	: "00:00:00";
       		$t2		=	($outAnsw['tOnline'] <> NULL) 	? $outAnsw['tOnline'] 	: "00:00:00";
       		$t3		=	($outNoAnsw['tOnline'] <> NULL) ? $outNoAnsw['tOnline'] : "00:00:00";
       		$time 	=	[$t1, $t2];

       		$total = 0;

		    foreach($time as $h) {
		        $parts = explode(":", $h);
		        $total += $parts[2] + $parts[1]*60 + $parts[0]*3600;        
		    }   

       		$info[$u]	=	[
       			'id'		=>	$user['id'],
       			'operator'	=>	$user['username'],
       			'ext'		=>	$user['ext'],
       			'InbAns'	=>	$inbound['total'],
       			'InbTim'	=>	($inbound['tOnline'] <> NULL) ? $inbound['tOnline'] : "00:00:00",
       			'OutAns'	=>	$outAnsw['total'],
       			'OutTim'	=>	($outAnsw['tOnline'] <> NULL) ? $outAnsw['tOnline'] : "00:00:00",
       			'OutNoAns'	=>	$outNoAnsw['total'],
       			'OutNoTim'	=>	($outNoAnsw['tOnline'] <> NULL) ? $outNoAnsw['tOnline'] : "00:00:00",
       			'tTotal'	=>	gmdate("H:i:s", $total)
       		];
		}

		return $info;
	}

	public static function GetInfoByOperator($info)
	{
		$_replace 	=	New Config;
	
        $user       =   User::GetUserById($info['operator']); 

        switch ($info['country']) 
        {
            case '1':
                $table  =   "cp_time_calls_pr_new";
                $ext    =   $user['ext'];
                break;
            case '2':
                $table = "cp_time_calls_us_new";
                $ext    =   $user['ext_us'];
                break;
            case '4':
                $table = "cp_time_calls_vz_new";
                $ext    =   $user['ext_ve'];
                break;
        }

        if(($info['dateTime_in'] == "") OR ($info['dateTime_out'] == ""))
        {
            return $app->json(array(
                'status'    =>  false,
                'content'   =>  "Existen Valores faltantes por favor verificar."
            )); 

        }else{
            
            $query      =   'SELECT * FROM '.$table.' WHERE ext = "'.$ext.'" AND created_at BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'"';
            $iData      =   DBSmart::DBQueryAll($query);

            if($iData <> false)
            {
                foreach ($iData as $i => $iDa) 
                {
                    $iTotal[$i]     =   [
                        'status'        =>  true, 
                        'id'            =>  $user['id'],
                        'operator'      =>  $user['username'],
                        'ext'           =>  $iDa['ext'],
                        'inma120'       =>  $iDa['inma120'],
                        'inma120t'      =>  $iDa['inma120t'],
                        'inme120'       =>  $iDa['inme120'],
                        'inme120t'      =>  $iDa['inme120t'],
                        'inb'           =>  $iDa['inb'],
                        'inbt'          =>  $iDa['inbt'],
                        'ouma120'       =>  $iDa['ouma120'],
                        'ouma120t'      =>  $iDa['ouma120t'],
                        'oume120'       =>  $iDa['oume120'],
                        'oume120t'      =>  $iDa['oume120t'],
                        'oubto'         =>  $iDa['oubto'],
                        'oubtot'        =>  $iDa['oubtot'],
                        'ouna'          =>  $iDa['ouna'],
                        'aounat'        =>  $iDa['ounat'],
                        'cacalls'       =>  $iDa['cacalls'],
                        'ticalls'       =>  $iDa['ticalls'],
                        'created_at'    =>  $iDa['created_at'],
                    ];
                }

				$query 	=	'SELECT
(SELECT SUM(inb) FROM '.$table.' WHERE ext = "'.$ext.'" AND created_at BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'") AS CInb,
(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(inbt))) FROM '.$table.' WHERE ext = "'.$ext.'" AND created_at BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'") AS TInb,
(SELECT SUM(oubto)+SUM(ouna) FROM '.$table.' WHERE ext = "'.$ext.'" AND created_at BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'") AS COut,
(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(oubtot))) FROM '.$table.' WHERE ext = "'.$ext.'" AND created_at BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'") AS TOut,
(SELECT SUM(cacalls) FROM '.$table.' WHERE ext = "'.$ext.'" AND created_at BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'") AS CTotal,
(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(ticalls))) FROM '.$table.' WHERE ext = "'.$ext.'" AND created_at BETWEEN "'.$info['dateTime_in'].'" AND "'.$info['dateTime_out'].'") AS TTotal';


// var_dump($query); exit;

				$iStatics 	=	DBSmart::DBQuery($query);

                return ['iTotal' => $iTotal, 'iStatics' => $iStatics];
                
            }else{ 
            	$iTotal[0] = ['status' => true, 'id' => $user['id'], 'operator' => $user['username'], 'ext' => $user['ext'], 'inma120' => '0', 'inma120t' => '00:00:00', 'inme120' => '0', 'inme120t' => '00:00:00', 'inb' => '0', 'inbt' => '00:00:00', 'ouma120' => '0', 'ouma120t' => '00:00:00', 'oume120' => '0', 'oume120t' => '00:00:00', 'oubto' => '0', 'oubtot' => '00:00:00', 'ouna' => '0', 'aounat' => '00:00:00', 'cacalls' => '0', 'ticalls' => '00:00:00', 'created_at' => '0000-00-00' ];

            	$iStatics 	=	['CInb' => '0', 'TInb' => '0',  'COut' => '0', 'TOut' => '0', 'CTotal' => '0', 'TTotal' => '0'];

                 return ['iTotal' => $iTotal, 'iStatics' => $iStatics];
            }
        }


	}

	public static function GetHtmlInfo($info)
	{

		$html = "";
		$html.='<div class="row">';
			$html.='<section class="col col-md-12">';
				$html.='<table class="table table-bordered table-striped" width="100%" style="font-size: 11px;">';
					$html.='<tbody>';
						$html.='<tr style="text-align: center;">';
							$html.='<td colspan="3">INFORMACION</td>';
							$html.='<td colspan="6">INBOUND</td>';
							$html.='<td colspan="8">OUTBOUND</td>';
							$html.='<td colspan="2">TOTAL</td>';
						$html.='</tr>';
						$html.='<tr style="text-align: center;">';
							$html.='<td>FECHA</td>';
							$html.='<td>OPERADOR</td>';
							$html.='<td>EXT</td>';
							$html.='<td colspan="4">CONTESTADAS</td>';
							$html.='<td colspan="2">TOTAL</td>';
							$html.='<td colspan="4">CONTESTADAS</td>';
							$html.='<td colspan="2">TOTAL</td>';
							$html.='<td colspan="2">NO CONTESTADAS</td>';
							$html.='<td colspan="2">TOTAL</td>';
						$html.='</tr>';

						if(isset($info[0]['status']))
						{
							foreach ($info as $key => $val) 
							{
								$html.='<tr style="text-align: center;">';
								$html.='<td>'.$val['created_at'].'</td>';
								$html.='<td>'.$val['operator'].'</td>';
								$html.='<td>'.$val['ext'].'</td>';
								$html.='<td>'.$val['inma120'].'</td>';
								$html.='<td>'.$val['inma120t'].'</td>';
								$html.='<td>'.$val['inme120'].'</td>';
								$html.='<td>'.$val['inme120t'].'</td>';
								$html.='<td>'.$val['inb'].'</td>';
								$html.='<td>'.$val['inbt'].'</td>';
								$html.='<td>'.$val['ouma120'].'</td>';
								$html.='<td>'.$val['ouma120t'].'</td>';
								$html.='<td>'.$val['oume120'].'</td>';
								$html.='<td>'.$val['oume120t'].'</td>';
								$html.='<td>'.$val['oubto'].'</td>';
								$html.='<td>'.$val['oubtot'].'</td>';
								$html.='<td>'.$val['ouna'].'</td>';
								$html.='<td>'.$val['aounat'].'</td>';
								$html.='<td>'.$val['cacalls'].'</td>';
								$html.='<td>'.$val['ticalls'].'</td>';
								$html.='</tr>';
							}

						}else{
							$html.='<tr style="text-align: center;">';
							$html.='<td>'.$info['created_at'].'</td>';
							$html.='<td>'.$info['operator'].'</td>';
							$html.='<td>'.$info['ext'].'</td>';
							$html.='<td>'.$info['inma120'].'</td>';
							$html.='<td>'.$info['inma120t'].'</td>';
							$html.='<td>'.$info['inme120'].'</td>';
							$html.='<td>'.$info['inme120t'].'</td>';
							$html.='<td>'.$info['inb'].'</td>';
							$html.='<td>'.$info['inbt'].'</td>';
							$html.='<td>'.$info['ouma120'].'</td>';
							$html.='<td>'.$info['ouma120t'].'</td>';
							$html.='<td>'.$info['oume120'].'</td>';
							$html.='<td>'.$info['oume120t'].'</td>';
							$html.='<td>'.$info['oubto'].'</td>';
							$html.='<td>'.$info['oubtot'].'</td>';
							$html.='<td>'.$info['ouna'].'</td>';
							$html.='<td>'.$info['aounat'].'</td>';
							$html.='<td>'.$info['cacalls'].'</td>';
							$html.='<td>'.$info['ticalls'].'</td>';
							$html.='</tr>';
						}
						$html.='</tr>';
					$html.='</tbody>';
				$html.='</table>';
			$html.='</section>';
		$html.='</div>';

		// $html = "";
		// $html.='<div class="row">';
		// $html.='<section class="col col-md-12">';
		// $html.='<table class="table table-bordered table-striped" width="100%" style="font-size: 11px;">';
		// $html.='<tbody>';
		// $html.='<tr>';
		// $html.='<td style="text-align: center; vertical-align: middle;" colspan="11">GENERAL</td>';
		// $html.='</tr>';
		// $html.='<tr>';
		// $html.='<td style="text-align: center; vertical-align: middle;" colspan="2"></td>';
		// $html.='<td style="text-align: center; vertical-align: middle;" colspan="2">INBOUND</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;" colspan="6">OUTBOUND</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;"></td>';
		// $html.='</tr>';
		// $html.='<tr>';
		// $html.='<td style="text-align: center; vertical-align: middle;">Date</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">OPERADOR</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">EXT</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">ANSWERED</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">TIME</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">ANSWERED</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">TIME</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">NO ANSWER</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">TIME</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">ONLINE</td>';
		// $html.='<td style="text-align: center; vertical-align: middle;">ACTION</td>';
		// $html.='</tr>';

		// if($info[0]['status'] <> false)
		// {
		// 	foreach ($info as $key => $val) 
		// 	{
		// 	$html.='<tr>';
		// 	$html.='<td style="text-align: center;">'.$val['call_date'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['operator'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['ext'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['InbAns'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['InbTim'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['OutAns'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['OutTim'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['OutNoAns'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['OutNoTim'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$val['tTotal'].'</td>';
		// 	$html.='<td style="text-align: center;">
	 //    	      		<a onclick="EditUser('.(int)$val['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Client"><i class="fa fa-eye"></i></a>
	 //                </td>';
		// 	$html.='</tr>';
		// 	}

		// }else{
		// 	$html.='<tr>';
		// 	$html.='<td style="text-align: center;">'.$info['operator'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['ext'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['InbAns'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['InbTim'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['OutAns'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['OutTim'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['OutNoAns'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['OutNoTim'].'</td>';
		// 	$html.='<td style="text-align: center;">'.$info['tTotal'].'</td>';
		// 	$html.='<td style="text-align: center;">
	 //    	      		<a onclick="EditUser('.(int)$info['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Client"><i class="fa fa-eye"></i></a>
	 //                </td>';
		// 	$html.='</tr>';
		// }
		// $html.='</tbody>';
		// $html.='</table>';
		// $html.='</section>';
		// $html.='</div>';

		return $html;
	
	}

	public static function GetHtmlInfoStatics($info)
	{

		$html = "";
		$html.='<table table class="table table-bordered table-striped" width="100%" style="font-size: 11px;">';
		$html.='<tbody>';
		$html.='<tr>';
		$html.='<td rowspan="2"></td>';
		$html.='<td>INBOUND</td>';
		$html.='<td>TIME</td>';
		$html.='<td>OUTBOUND</td>';
		$html.='<td>TIME</td>';
		$html.='<td>TOTAL</td>';
		$html.='<td>TIME</td>';
		$html.='</tr>';
		$html.='<tr>';
		$html.='<td>'.$info['CInb'].'</td>';
		$html.='<td>'.$info['TInb'].'</td>';
		$html.='<td>'.$info['COut'].'</td>';
		$html.='<td>'.$info['TOut'].'</td>';
		$html.='<td>'.$info['CTotal'].'</td>';
		$html.='<td>'.$info['TTotal'].'</td>';
		$html.='</tr>';
		$html.='</tbody>';
		$html.='</table>';

		return $html;
	
	}

	public static function GetHtmlUserInfo($info)
	{
		$html = "";

		$html.='<div class="table-responsive">';
		$html.='<table id="leaderUsertable" class="table table-bordered" border="1" style="font-size: 10px;">';
		$html.='<thead>';
		$html.='<tr>';
		$html.='<th style="text-align: center;">OPERADOR</th>';
		$html.='<th style="text-align: center;">EXT</th>';
		$html.='<th style="text-align: center;">ACCION</th>';
		$html.='</tr>';
		$html.='</thead>';
		$html.='<tbody>';
		if($info <> false)
		{
			foreach ($info as $key => $val) 
			{
				$html.='<tr>';
				$html.='<td style="text-align: center;">'.$val['username'].'</td>';
				$html.='<td style="text-align: center;">'.$val['ext'].'</td>';
				$html.='<td style="text-align: center;"><a onclick="EditUser('.(int)$val['id'].')" class="btn btn-xs btn-xs btn-default" rel="tooltip" data-placement="top" data-original-title="View Client"><i class="fa fa-eye"></i></a></td>';
				$html.='</tr>';
			}
		}else{
			$html.='<tr>';
			$html.='<td style="text-align: center; vertical-align: middle;" colspan="3">SIN REGISTROS DE OPERADORES</td>';
			$html.='</tr>';
		}


		$html.='</tbody>';
		$html.='</table>';
		$html.='</div>';	
		return $html;	
	}

}
