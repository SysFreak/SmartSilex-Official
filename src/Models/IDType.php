<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Country;
use App\Models\IDType;

use App\Lib\Config;
use App\Lib\DBSmart;

class IDType extends Model 
{

	static $_table 		= 'data_id_type';

	Public $_fillable 	= array('name', 'status_id', 'country_id');

	public static function GetIdType()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_id_type ORDER BY id ASC';
		$type 	=	DBSmart::DBQueryAll($query);

		$iType = array();

		if($type <> false)
		{
			foreach ($type as $k => $val) 
			{
				$iType[$k] = array(
					'id' 	 		=> $val['id'], 
					'name' 	 		=> $val['name'],
					'status' 		=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=> $val['status_id'],
					'country' 		=> Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=> $val['country_id']
				);
			}
	        
	        return $iType;
		}else{ 	return false;	}
	}

	public static function GetIdTypeById($id)
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_id_type WHERE id = "'.$id.'" ORDER BY id ASC';
		$cei 	=	DBSmart::DBQuery($query);

		if($cei <> false)
		{
			return array(
				'id' => $cei['id'], 'name' => $cei['name'], 'status' => $cei['status_id'], 'country' => $cei['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetIdTypeByStatus()
	{

		$query 	= 	'SELECT id, name, status_id, country_id FROM data_id_type WHERE status_id = "1" ORDER BY id ASC';
		$cei 	=	DBSmart::DBQuery($query);

		if($cei <> false)
		{
			return array(
				'id' => $cei['id'], 'name' => $cei['name'], 'status' => $cei['status_id'], 'country' => $cei['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetIdTypeByName($info)
	{
		$query 	= 	'SELECT id, name, status_id, country_id FROM data_id_type WHERE name = "'.$info.'"';
		$cei 	=	DBSmart::DBQuery($query);

		if($cei <> false)
		{
			return array(
				'id' => $cei['id'], 'name' => $cei['name'], 'status' => $cei['status_id'], 'country' => $cei['country_id']
			);

		}else{ 	return false;	}
	}

	public static function GetIdTypeByCountry($id)
	{

		$query 	= 	'SELECT id, name FROM data_id_type WHERE country_id = "'.$id.'" and status_id = "1" ORDER BY id ASC';
		$cei 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($cei)
		{
			foreach ($cei as $k => $val) 
			{ $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>';}

			return $html;

		}else{ return $html; }
	}

	public static function SaveIdType($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_ce']));

		if($info['type_ce'] == 'new')
		{
			$query 	= 	'INSERT INTO data_id_type(name, status_id, country_id, created_at) VALUES ("'.$name.'", "'.$info['status_id_ce'].'", "'.$info['country_id_ce'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_ce'] == 'edit') 
		{
			$query 	=	'UPDATE data_id_type SET name="'.$name.'", status_id="'.$info['status_id_ce'].'", country_id="'.$info['country_id_ce'].'" WHERE id = "'.$info['id_ce'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}

}

