<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class CoordCancelTmp extends Model
{

	static $_table 		= 'cp_coord_cancel_tmp';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'service_id', 'reason_id', 'operator_id', 'created_at');

//////////////////////////////////////////////////////////////////////////////////////////
	
	public static function SaveCancelCoord($params, $cData)
	{

		$query 	=	'INSERT INTO cp_coord_cancel_tmp(ticket, client_id, service_id, reason_id, operator_id, created_at) VALUES ("'.$cData['ticket'].'", "'.$cData['client'].'", "'.$cData['service'].'", "'.$params.'", "'.$cData['operator'].'", "'.$cData['date'].'") ';

		$dep 	=	DBSmart::DataExecuteLastID($query);

		return ($dep <> false ) ? $dep : false;
	
	}

//////////////////////////////////////////////////////////////////////////////////////////

}