<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Phone extends Model {

	static $_table 		= 'data_phone';

	Public $_fillable 	= array('name', 'type_id', 'country_id', 'status_id');

	public static function GetPhone()
	{
		$query 	= 	'SELECT * FROM data_phone ORDER BY id ASC';
		$pho 	=	DBSmart::DBQueryAll($query);

		$phones = array();

		if($pho <> false)
		{
			foreach ($pho as $k => $val) 
			{
				$phones[$k] = array(
					'id' 	 	=> $val['id'], 
					'name' 	 	=> $val['name'], 
					'type_id'	=> $val['type_id'],
					'coun_id' 	=> $val['country_id'],
					'stat_id' 	=> $val['status_id'],
					'type' 	 	=> InternetType::GetInternetTypeById($val['type_id'])['name'],
					'country' 	=> Country::GetCountryById($val['country_id'])['name'],
					'status' 	=> Status::GetStatusById($val['status_id'])['name']
				);
			}
	        
	        return $phones;

		}else{ 	return false;	}
	}

	public static function GetType($type, $country)
	{
		$query 	= 	'SELECT * FROM data_phone WHERE type_id = "'.$type.'" AND country_id = "'.$country.'" ORDER BY id ASC';
		$pho 	=	DBSmart::DBQueryAll($query);

		$phones = array();

		if($pho <> false)
		{
			foreach ($pho as $k => $val) 
			{
				$phones[$k] = array(
					'id' 	 	=> $val['id'], 
					'name' 	 	=> $val['name'],
					'status' 	=> Status::GetStatusById($val['status_id'])['name'],
					'status_id'	=> $val['status_id'],
				);
			}
	        return $phones;

		}else{	return false;	}

	}

	public static function GetPhoneById($id)
	{
		$query 	= 	'SELECT * FROM data_phone WHERE id = "'.$id.'"';
		$int 	=	DBSmart::DBQuery($query);

		if($int <> false)
		{
			return array('id' => $int['id'], 'name' => $int['name'], 'type' => $int['type_id'], 'status' => $int['status_id'], 'country' => $int['country_id']);

		}else{ 	return false;	}
	}

	public static function GetFirst($type, $country)
	{

		$query 		= 	'SELECT * FROM data_phone WHERE type_id = "'.$type.'" AND country_id = "'.$country.'" AND status_id = "1" ORDER BY id ASC LIMIT 1';
		$int 		=	DBSmart::DBQuery($query);

		if($int <> false)
		{
			return 	[
				'id'		=>	$int['id'],
				'name'		=>	$int['name'],
				'status'	=>	Status::GetStatusById($int['status_id'])['name'],
				'status_id'	=>	$int['status_id']
			];

		}else{

			return 	[
				'id'		=>	1,
				'name'		=>	"NONE",
				'status_id'	=>	1
			];
		}
	
	}


	public static function GetPhoneByName($info)
	{

		$query 	= 	'SELECT * FROM data_phone WHERE name = "'.$info.'"';
		$int 	=	DBSmart::DBQuery($query);

		if($int <> false)
		{
			return array('id' => $int['id'], 'name' => $int['name'], 'type' => $int['type_id'], 'status' => $int['status_id'], 'country' => $int['country_id']);

		}else{ 	return false;	}
	}

	public static function SavePhone($info)
	{
		$date 		= 	date('Y-m-d H:i:s', time());

		$_replace  	= 	new Config();

		$name 		= 	strtoupper($_replace->deleteTilde($info['name_pho']));

		if($info['type_pho'] == 'new')
		{
			$query 	= 	'INSERT INTO data_phone(name, type_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['type_id_pho'].'", "'.$info['country_id_pho'].'", "'.$info['status_id_pho'].'", "'.$date.'")';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;

		}
		elseif ($info['type_pho'] == 'edit') 
		{
			$query 	=	'UPDATE data_phone SET name="'.$name.'", type_id="'.$info['type_id_pho'].'", country_id="'.$info['country_id_pho'].'", status_id="'.$info['status_id_pho'].'" WHERE id = "'.$info['id_pho'].'"';

			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
	}
}