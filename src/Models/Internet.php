<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Internet;
use App\Models\InternetType;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Internet extends Model {

	static $_table 		= 'data_internet';

	Public $_fillable 	= array('name', 'type_id', 'country_id', 'status_id');

	public static function GetInternet()
	{

		$query 	= 	'SELECT * FROM data_internet ORDER BY id ASC';
		$int 	=	DBSmart::DBQueryAll($query);

		$internet = array();

		if($int <> false)
		{
			foreach ($int as $k => $val) 
			{
				$internet[$k] = array(
					'id' 	 	=> $val['id'], 
					'name' 	 	=> $val['name'], 
					'type_id'	=> $val['type_id'],
					'coun_id' 	=> $val['country_id'],
					'stat_id' 	=> $val['status_id'],
					'type' 	 	=> InternetType::GetInternetTypeById($val['type_id'])['name'],
					'country' 	=> Country::GetCountryById($val['country_id'])['name'],
					'status' 	=> Status::GetStatusById($val['status_id'])['name']
				);
			}
	        
	        return $internet;

		}else{ 	return false;	}

	}

	public static function GetType($type, $country)
	{

		$query 	= 	'SELECT * FROM data_internet WHERE type_id = "'.$type.'" AND country_id = "'.$country.'" ORDER BY id ASC';
		$int 	=	DBSmart::DBQueryAll($query);

		$internet = array();

		foreach ($int as $k => $val) 
		{
			$internet[$k] = array(
				'id' 	 	=> $val['id'], 
				'name' 	 	=> $val['name'],
				'status' 	=> Status::GetStatusById($val['status_id'])['name'],
				'status_id'	=> $val['status_id'],
			);
		}

        return $internet;
	}

	public static function GetFirst($type, $country)
	{

		$query 		= 	'SELECT * FROM data_internet WHERE type_id = "'.$type.'" AND country_id = "'.$country.'" and status_id = "1" ORDER BY id ASC limit 1';
		$int 		=	DBSmart::DBQuery($query);

		if($int <> false)
		{
			return 	[
				'id'		=>	$int['id'],
				'name'		=>	$int['name'],
				'status'	=>	Status::GetStatusById($int['status_id'])['name'],
				'status_id'	=>	$int['status_id']
			];

		}else{

			return 	[
				'id'		=>	1,
				'name'		=>	"1Mb",
				'status_id'	=>	1
			];
		}
	
	}	


	public static function GetTypeVzRes($type, $country)
	{

		$query 	= 	'SELECT * FROM data_internet WHERE (type_id = "'.$type.'" OR type_id = "4") AND country_id = "'.$country.'"';
		$int 	=	DBSmart::DBQueryAll($query);

		$internet = array();

		foreach ($int as $k => $val) 
		{
			$internet[$k] = array(
				'id' 	 	=> $val['id'], 
				'name' 	 	=> $val['name'],
				'status' 	=> Status::GetStatusById($val['status_id'])['name'],
				'status_id'	=> $val['status_id'],
			);
		}

        return $internet;
	
	}

	public static function GetTypeVzResTest($type, $country)
	{

		$query 	= 	'SELECT * FROM data_internet WHERE (type_id = "'.$type.'" OR type_id = "4") AND country_id = "'.$country.'" AND status_id = "1"';
		$int 	=	DBSmart::DBQueryAll($query);

		$internet = array();
		$str 	= 	"";

		if($type == "1") { $str =	"Res - "; }
		if($type == "2") { $str =	"Com - "; }
		if($type == "3") { $str =	"Gam - "; }
		if($type == "4") { $str =	"Fib - "; }

		foreach ($int as $k => $val) 
		{
			$internet[$k] = array(
				'id' 	 	=> $val['id'], 
				'name' 	 	=> $str.$val['name'],
				'status' 	=> Status::GetStatusById($val['status_id'])['name'],
				'status_id'	=> $val['status_id'],
			);
		}

        return $internet;
	
	}

	public static function GetTypeVzCom($type, $country)
	{

		$query 	= 	'SELECT * FROM data_internet WHERE (type_id = "'.$type.'" OR type_id = "5") AND country_id = "'.$country.'"';
		$int 	=	DBSmart::DBQueryAll($query);

		$internet = array();

		foreach ($int as $k => $val) 
		{
			$internet[$k] = array(
				'id' 	 	=> $val['id'], 
				'name' 	 	=> $val['name'],
				'status' 	=> Status::GetStatusById($val['status_id'])['name'],
				'status_id'	=> $val['status_id'],
			);
		}

        return $internet;
	}


	public static function GetInternetById($id)
	{
		
		$query 	= 	'SELECT * FROM data_internet WHERE id = "'.$id.'"';
		$int 	=	DBSmart::DBQuery($query);

		if($int <> false)
		{
			return array('id' => $int['id'], 'name' => $int['name'], 'type' => $int['type_id'], 'status' => $int['status_id'], 'country' => $int['country_id']);

		}else{ 	return false;	}
	
	}

	public static function GetInternetByName($info)
	{

		$query 	= 	'SELECT * FROM data_internet WHERE name = "'.$info.'"';
		$int 	=	DBSmart::DBQuery($query);

		if($int <> false)
		{
			return array('id' => $int['id'], 'name' => $int['name'], 'type' => $int['type_id'], 'status' => $int['status_id'], 'country' => $int['country_id'], 'price' => $int['price']);

		}else{ 	return false;	}
	
	}

	public static function GetTypeVzFib($type)
	{

		$query 	= 	'SELECT * FROM data_internet WHERE type_id = "'.$type.'" AND country_id = "4" AND status_id = "1" ORDER BY id ASC';
		$int 	=	DBSmart::DBQueryAll($query);

		$internet = array();

		foreach ($int as $k => $val) 
		{
			$internet[$k] = array(
				'id' 	 	=> $val['id'], 
				'name' 	 	=> $val['name'],
				'status' 	=> Status::GetStatusById($val['status_id'])['name'],
				'status_id'	=> $val['status_id'],
			);
		}

        return $internet;
	
	}

	public static function SaveInternet($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_int']));

		if($info['type_int'] == 'new')
		{
			$query 	= 	'INSERT INTO data_internet(name, type_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['type_id_int'].'", "'.$info['country_id_int'].'", "'.$info['status_id_int'].'", "'.$date.'")';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false ) ? true : false;

		}
		elseif ($info['type_int'] == 'edit') 
		{
			$query 	=	'UPDATE data_internet SET name="'.$name.'", type_id="'.$info['type_id_int'].'", country_id="'.$info['country_id_int'].'", status_id="'.$info['status_id_int'].'" WHERE id = "'.$info['id_int'].'"';

			$hou 	=	DBSmart::DataExecute($query);

			return ($hou <> false) ? true : false;
		}
	}
}

