<?php
namespace App\Models;

use Model;
use App\Models\Auth;
use App\Models\Departament;
use App\Models\Status;
use App\Models\Role;
use App\Models\Team;
use App\Models\User;

use App\Lib\DBSmart;
use App\Lib\Config;

use PDO;

class User extends Model 
{

 static $_table = 'users';
   
   protected $_fillable = array('id', 'name', 'password', 'username', 'email', 'ext', 'ext_ve', 'departament_id', 'role_id', 'status_id', 'team_id', 'hours_in', 'hours_out', 'tv_pr', 'sec_pr', 'net_pr', 'wrl_pr', 'tv_usa', 'wir_usa', 'net_usa', 'tv_col', 'net_col', 'sec_col');  

//////////////////////////////////////////////////////////////////////////////////////////////
  
   public static function GetUsers()
   {

      $query      =  'SELECT id, name, password, username, email, ext, ext_ve, departament_id, role_id, status_id, team_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr, tv_usa, wir_usa, net_usa, tv_col, net_col, sec_col FROM users ORDER BY id ASC';
      $user       =   DBSmart::DBQueryAll($query);

      $users      =  array();
      
      foreach ($user as $k => $val) 
      {
         $users[$k]  =  array(
            'id'           => $val['id'],
            'name'         => $val['name'],
            'username'     => $val['username'],
            'email'        => $val['email'],
            'ext'          => $val['ext'],
            'ext_ve'       => $val['ext_ve'],
            'departament'  => Departament::GetDepById($val['departament_id'])['name'],
            'status'       => Status::GetStatusById($val['status_id'])['name'],
            'status_id'    => $val['status_id'],
            'role'         => Role::GetRolById($val['role_id'])['name'],
            'team'         => Team::GetTeamById($val['team_id'])['name'],
            'hours_in'     => $val['hours_in'],
            'hours_out'    => $val['hours_out'],
            
            'tv_pr'        => $val['tv_pr'],
            'sec_pr'       => $val['sec_pr'],
            'net_pr'       => $val['net_pr'],
            'wrl_pr'       => $val['wrl_pr'],
            'tv_usa'       => $val['tv_pr'],

            'tv_usa'       => $val['tv_usa'],
            'wir_usa'      => $val['wir_usa'],
            'net_usa'      => $val['net_usa'],
            
            'tv_col'       => $val['tv_col'],
            'net_col'      => $val['net_col'],
            'sec_col'      => $val['sec_col']
         );
      }
      return $users;
   
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetUsersHtml()
   {

      $query      =  'SELECT id, username FROM users WHERE status_id = "1" ORDER BY username ASC';
      $user       =   DBSmart::DBQueryAll($query);

      $html = "";

      if($user <> false)
      {
         $html.='<option value="0" selected="" disabled="">SELECTED</option>';

         foreach ($user as $k => $val) 
         {  $html.='<option value="'.$val['id'].'">'.$val['username'].'</option>'; }

         return $html;

      }else{ return $html; }

   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetUsersHtmlContracts()
   {

      $query      =  'SELECT id, username FROM users WHERE status_id = "1" ORDER BY username ASC';
      $user       =   DBSmart::DBQueryAll($query);

      $html = "";

      if($user <> false)
      {
         foreach ($user as $k => $val) 
         {  $html.='<option value="'.$val['username'].'">'.$val['username'].'</option>'; }

         return $html;

      }else{ return $html; }

   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetUsersHtmlTicket()
   {
      $query      =  'SELECT id, username FROM users WHERE status_id = "1" ORDER BY username ASC';
      $user       =   DBSmart::DBQueryAll($query);
      $html       =  "";

      if($user <> false)
      {
       
         $html.='<select class="form-control" id="tic_ope" name="tic_ope" required>';
         $html.='<option value="0" selected="" disabled="">SELECTED</option>';

         foreach ($user as $k => $val) 
         {  $html.='<option value="'.$val['id'].'">'.$val['username'].'</option>'; }

         $html.='</select>';
         
         return $html;

      }else{ return $html; }

   }

//////////////////////////////////////////////////////////////////////////////////////////////
   
   public static function GetUserById($id)
   {
      $query      =  'SELECT id, name, password, username, email, ext, ext_ve, ext_us, departament_id, role_id, status_id, team_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr, tv_usa, wir_usa, net_usa, tv_col, net_col, sec_col FROM users WHERE id = "'.$id.'"';
      $user       =   DBSmart::DBQuery($query);

      if($user <> false)
      {
         return array(
            'id'           => $user['id'],
            'name'         => $user['name'],
            'username'     => $user['username'],
            'email'        => $user['email'],
            'departament'  => $user['departament_id'],
            'dept'         => $user['departament_id'],
            'role'         => $user['role_id'],
            'status'       => $user['status_id'],
            'ext'          => $user['ext'],
            'ext_ve'       => $user['ext_ve'],
            'ext_us'       => $user['ext_us'],
            'team'         => $user['team_id'],
            'hours_in'     => $user['hours_in'],
            'hours_out'    => $user['hours_out'],
            
            'tv_pr'        => $user['tv_pr'],
            'sec_pr'       => $user['sec_pr'],
            'net_pr'       => $user['net_pr'],
            'wrl_pr'       => $user['wrl_pr'],

            'tv_usa'       => $user['tv_usa'],
            'wir_usa'      => $user['wir_usa'],
            'net_usa'      => $user['net_usa'],
            
            'tv_col'       => $user['tv_col'],
            'net_col'      => $user['net_col'],
            'sec_col'      => $user['sec_col']
         );

      }else{ return false; }
      
   }

//////////////////////////////////////////////////////////////////////////////////////////////
   
   public static function GetUserByUsername($username)
   {

      $query      =  'SELECT id, name, password, username, email, ext, ext_ve, departament_id, role_id, status_id, team_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr, tv_usa, wir_usa, net_usa, tv_col, net_col, sec_col FROM users WHERE username = "'.$username.'" ORDER BY id ASC';
      $user       =   DBSmart::DBQuery($query);

      if($user <> false)
      {
         return array(
            'id'           => $user['id'],
            'name'         => $user['name'],
            'username'     => $user['username'],
            'email'        => $user['email'],
            'departament'  => $user['departament_id'],
            'role'         => $user['role_id'],
            'status'       => $user['status_id'],
            'ext'          => $user['ext'],
            'ext_ve'       => $user['ext_ve'],
            'team'         => $user['team_id'],
            'hours_in'     => $user['hours_in'],
            'hours_out'    => $user['hours_out'],
            
            'tv_pr'        => $user['tv_pr'],
            'sec_pr'       => $user['sec_pr'],
            'net_pr'       => $user['net_pr'],
            'wrl_pr'       => $user['wrl_pr'],

            'tv_usa'       => $user['tv_usa'],
            'wir_usa'      => $user['wir_usa'],
            'net_usa'      => $user['net_usa'],
            
            'tv_col'       => $user['tv_col'],
            'net_col'      => $user['net_col'],
            'sec_col'      => $user['sec_col']
         );

      }else{ return true; }
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function SaveUser($info, $pass)
   {
      $date       =  date('Y-m-d H:i:s', time());
      $_replace   =  new Config();

      $tv   = (isset($info['tv_pr']))  ? $info['tv_pr']  : 'off';
      $sec  = (isset($info['sec_pr'])) ? $info['sec_pr'] : 'off';
      $net  = (isset($info['net_pr'])) ? $info['net_pr'] : 'off';
      $wrl  = (isset($info['wrl_pr'])) ? $info['wrl_pr'] : 'off';

      $tvU  = (isset($info['tv_usa']))  ? $info['tv_usa']  : 'off';
      $wirU = (isset($info['wir_usa'])) ? $info['wir_usa'] : 'off';
      $netU = (isset($info['net_usa'])) ? $info['net_usa'] : 'off';

      $tvC  = (isset($info['tv_col']))  ? $info['tv_col']  : 'off';
      $secC = (isset($info['sec_col'])) ? $info['sec_col'] : 'off';
      $netC = (isset($info['net_col'])) ? $info['net_col'] : 'off';
      
      $uData   =  [
         'name'            =>    strtoupper($_replace->deleteTilde($info['name'])),
         'username'        =>    strtoupper($_replace->deleteTilde($info['username'])),
         'email'           =>    $info['email'],
         'ext'             =>    $info['ext'],
         'ext_ve'          =>    $info['ext_ve'],
         'departament_id'  =>    $info['departament_id'],
         'role_id'         =>    $info['role_id'],
         'status_id'       =>    $info['status_id'],
         'team_id'         =>    $info['team_id'],
         'hours_in'        =>    $info['hours_in'],
         'hours_out'       =>    $info['hours_out'],
         'tv_pr'           =>    $tv,
         'sec_pr'          =>    $sec,
         'net_pr'          =>    $net,
         'wrl_pr'          =>    $wrl,
         'tv_usa'          =>    $tvU,
         'wir_usa'         =>    $wirU,
         'net_usa'         =>    $netU,            
         'tv_col'          =>    $tvC,
         'net_col'         =>    $netC,
         'sec_col'         =>    $secC,
         'created_at'      =>    $date
      ];

      if($info['type'] == "edit")
      {
       
         if( ($info['password'] == "") OR ($info['cpassword'] == "") )
         {
            $query = 'UPDATE users SET name="'.$uData['name'].'", username="'.$uData['username'].'", email="'.$uData['email'].'", ext="'.$uData['ext'].'", ext_ve="'.$uData['ext_ve'].'", departament_id="'.$uData['departament_id'].'", role_id="'.$uData['role_id'].'", status_id="'.$uData['status_id'].'", team_id="'.$uData['team_id'].'", hours_in="'.$uData['hours_in'].'", hours_out="'.$uData['hours_out'].'", tv_pr="'.$uData['tv_pr'].'", sec_pr="'.$uData['sec_pr'].'", net_pr="'.$uData['net_pr'].'", wrl_pr="'.$uData['wrl_pr'].'", tv_usa="'.$uData['tv_usa'].'", wir_usa="'.$uData['wir_usa'].'", net_usa="'.$uData['net_usa'].'", tv_col="'.$uData['tv_col'].'", net_col="'.$uData['net_col'].'", sec_col="'.$uData['sec_col'].'" WHERE id = "'.$info['id'].'"';

            $user =  DBSmart::DataExecute($query);

            return ($user <> false) ? true : false;

         }else{

            $query = 'UPDATE users SET name="'.$uData['name'].'", password = "'.$pass.'", username="'.$uData['username'].'", email="'.$uData['email'].'", ext="'.$uData['ext'].'", ext_ve="'.$uData['ext_ve'].'", departament_id="'.$uData['departament_id'].'", role_id="'.$uData['role_id'].'", status_id="'.$uData['status_id'].'", team_id="'.$uData['team_id'].'", hours_in="'.$uData['hours_in'].'", hours_out="'.$uData['hours_out'].'", tv_pr="'.$uData['tv_pr'].'", sec_pr="'.$uData['sec_pr'].'", net_pr="'.$uData['net_pr'].'", wrl_pr="'.$uData['wrl_pr'].'", tv_usa="'.$uData['tv_usa'].'", wir_usa="'.$uData['wir_usa'].'", net_usa="'.$uData['net_usa'].'", tv_col="'.$uData['tv_col'].'", net_col="'.$uData['net_col'].'", sec_col="'.$uData['sec_col'].'" WHERE id = "'.$info['id'].'"';

            $user =  DBSmart::DataExecute($query);

            return ($user <> false) ? true : false;  
         }

      }
      elseif ($info['type'] == 'new') 
      {
         $query   =  'INSERT INTO users(name, password, username, email, ext, ext_ve, departament_id, role_id, status_id, team_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr, tv_usa, wir_usa, net_usa, tv_col, net_col, sec_col) VALUES ("'.$uData['name'].'", "'.$pass.'", "'.$uData['username'].'", "'.$uData['email'].'", "'.$uData['ext'].'", "'.$uData['ext_ve'].'", "'.$uData['departament_id'].'", "'.$uData['role_id'].'", "'.$uData['status_id'].'", "'.$uData['team_id'].'", "'.$uData['hours_in'].'", "'.$uData['hours_out'].'", "'.$uData['tv_pr'].'", "'.$uData['sec_pr'].'", "'.$uData['net_pr'].'", "'.$uData['wrl_pr'].'", "'.$uData['tv_usa'].'", "'.$uData['wir_usa'].'", "'.$uData['net_usa'].'", "'.$uData['tv_col'].'", "'.$uData['net_col'].'", "'.$uData['sec_col'].'")';

         $user =  DBSmart::DataExecuteLastID($query);

         return ($user <> false) ? $user : false; 
      }
   
   }

   public static function EditUserLeader($info)
   {

      $date = date('Y-m-d H:i:s', time());

      $_replace  = new Config();

      $iData   =  [
         'hours_in'     =>  $info['hours_in'],
         'hours_out'    =>  $info['hours_out'],
         'tv'           => (isset($info['tv_pr']))  ? $info['tv_pr']  : 'off',
         'sec'          => (isset($info['sec_pr'])) ? $info['sec_pr'] : 'off',
         'net'          => (isset($info['net_pr'])) ? $info['net_pr'] : 'off',
         'wrl'          => (isset($info['wrl_pr'])) ? $info['wrl_pr'] : 'off',
         'tvU'          => (isset($info['tv_usa']))  ? $info['tv_usa']  : 'off',
         'wirU'         => (isset($info['wir_usa'])) ? $info['wir_usa'] : 'off',
         'netU'         => (isset($info['net_usa'])) ? $info['net_usa'] : 'off',
         'tvC'          => (isset($info['tv_col']))  ? $info['tv_col']  : 'off',
         'secC'         => (isset($info['sec_col'])) ? $info['sec_col'] : 'off',
         'netC'         => (isset($info['net_col'])) ? $info['net_col'] : 'off',
         'date'         => $date
      ];

      $query = 'UPDATE users SET hours_in ="'.$iData['hours_in'].'", hours_out ="'.$iData['hours_out'].'", tv_pr ="'.$iData['tv'].'", sec_pr ="'.$iData['sec'].'", net_pr ="'.$iData['net'].'", wrl_pr ="'.$iData['wrl'].'", tv_usa ="'.$iData['tvU'].'", wir_usa ="'.$iData['wirU'].'",net_usa ="'.$iData['netU'].'",tv_col ="'.$iData['tvC'].'",net_col ="'.$iData['netC'].'",sec_col ="'.$iData['secC'].'" WHERE id = "'.$info['id'].'"';

      $user =  DBSmart::DataExecute($query);

      return (($user <> false ) ? true : false);
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetUsersByTeamLeaders($info)
   {

      if($info == 1)
      {
         $query   =  'SELECT id, name, username, email, ext, ext_ve, departament_id, team_id, status_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr FROM users WHERE status_id = "1" ORDER BY username ASC';
         $user    =  DBSmart::DBQueryAll($query);

      }else{
         $query   =  'SELECT id, name, username, email, ext, ext_ve, departament_id, team_id, status_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr FROM users WHERE status_id = "1" ORDER BY username ASC';
         $user    =  DBSmart::DBQueryAll($query);
      }

      if($user <> false)
      {
         foreach ($user as $k => $val) 
         {
            $users[$k] = [
               'id'           => $val['id'],
               'name'         => $val['name'],
               'username'     => $val['username'],
               'email'        => $val['email'],
               'ext'          => $val['ext'],
               'ext_ve'       => $val['ext_ve'],
               'depatament'   => Departament::GetDepById($val['departament_id'])['name'],
               'team'         => Team::GetTeamById($val['team_id'])['name'],
               'status'       => Status::GetStatusById($val['status_id'])['name'],
               'hours_in'     => $val['hours_in'],
               'hours_out'    => $val['hours_out'],
               'tv_pr'        => $val['tv_pr'],
               'sec_pr'       => $val['sec_pr'],
               'net_pr'       => $val['net_pr'],
               'wrl_pr'       => $val['wrl_pr']
            ];
         }

         return $users;

      }else{   return false;  }
   }



   public static function GetUsersByTeam($info)
   {

      if($info == 1)
      {
         $query   =  'SELECT id, name, username, email, ext, ext_ve, departament_id, team_id, status_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr FROM users WHERE status_id = "1" ORDER BY username ASC';
         $user    =  DBSmart::DBQueryAll($query);

      }else{
         $query   =  'SELECT id, name, username, email, ext, ext_ve, departament_id, team_id, status_id, hours_in, hours_out, tv_pr, sec_pr, net_pr, wrl_pr FROM users WHERE team_id = "'.$info.'" and status_id = "1" ORDER BY username ASC';
         $user    =  DBSmart::DBQueryAll($query);
      }

      if($user <> false)
      {
         foreach ($user as $k => $val) 
         {
            $users[$k] = [
               'id'           => $val['id'],
               'name'         => $val['name'],
               'username'     => $val['username'],
               'email'        => $val['email'],
               'ext'          => $val['ext'],
               'ext_ve'       => $val['ext_ve'],
               'depatament'   => Departament::GetDepById($val['departament_id'])['name'],
               'team'         => Team::GetTeamById($val['team_id'])['name'],
               'status'       => Status::GetStatusById($val['status_id'])['name'],
               'hours_in'     => $val['hours_in'],
               'hours_out'    => $val['hours_out'],
               'tv_pr'        => $val['tv_pr'],
               'sec_pr'       => $val['sec_pr'],
               'net_pr'       => $val['net_pr'],
               'wrl_pr'       => $val['wrl_pr']
            ];
         }

         return $users;

      }else{   return false;  }
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetEmailDepartament($depart)
   {

      $query   =  'SELECT id, username, email FROM users WHERE departament_id ="'.$depart.'" AND (role_id = "4" OR  role_id = "2") ORDER BY username ASC';
      $user    =  DBSmart::DBQueryAll($query);

      if($user <> false)
      {
         foreach ($user as $k => $val) 
         {
            $email[$k] =   ['username' => $val['username'], 'email' => $val['email']];
         }  
            return $email;
      }else{   return false;  }
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetUserMkt($dep1, $dep2, $sta)
   {

      $query    =  'SELECT id, name, username, (SELECT name FROM data_teams WHERE id = team_id) as team, tv_pr, sec_pr, net_pr, wrl_pr FROM users WHERE status_id = "'.$sta.'" AND (departament_id = "'.$dep1.'" OR departament_id = "'.$dep2.'") ORDER BY hours_in, username';
      
      $result   =  DBSmart::DBQueryAll($query);

      if($result <> false)
      {
         foreach ($result as $k => $val) 
         {
            $info[$k]   =  [
               'id'        => $val['id'],
               'name'      => $val['name'],
               'username'  => $val['username'],
               'team'      => $val['team'],
               'tvpr'      => $val['tv_pr'],
               'wrlpr'     => $val['wrl_pr'],
               'secpr'     => $val['sec_pr'],
               'netpr'     => $val['net_pr']
            ];
         }
         return $info;
      }else{   return false; }
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function UserPasswordUpdate($pass, $ope)
   {
      $query    =  'UPDATE users SET password = "'.$pass.'" WHERE id = "'.$ope.'"';
      
      $password =  DBSmart::DataExecute($query);

      return (($password <> false ) ? true : false);
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetUsersID($id)
   {
      $query   =  'SELECT * FROM users WHERE id = "'.$id.'"';
      $val    =   DBSmart::DBQuery($query);

      return   [
         'id'              => $val['id'],
         'name'            => $val['name'],
         'username'        => $val['username'],
         'email'           => $val['email'],
         'departament_id'  => $val['departament_id'],
         'departament'     => $val['departament_id'],
         'status_id'       => $val['status_id'],
         'team_id'         => $val['team_id'],
         'hours_in'        => $val['hours_in'],
         'hours_out'       => $val['hours_out'],            
      ];  
   
   }

//////////////////////////////////////////////////////////////////////////////////////////////


   public static function GetUsersDep($id)
   {
      $query   =  'SELECT * FROM users WHERE departament_id = "'.$id.'" AND status_id="1"';
      $val    =   DBSmart::DBQueryAll($query);

      if($val <> false)
      {
         foreach ($val as $k => $va) 
         {
            $user[$k]   =  [
               'id'              => $va['id'],
               'name'            => $va['name'],
               'username'        => $va['username'],
               'email'           => $va['email'],
               'departament_id'  => $va['departament_id'],
               'departament'     => $va['departament_id'],
               'status_id'       => $va['status_id'],
               'team_id'         => $va['team_id'],
               'hours_in'        => $va['hours_in'],
               'hours_out'       => $va['hours_out'],            
            ];  
         }
         return $user;

      }else{
         return false;

      } 
   
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function GetUsersDepDA($id)
   {
      $query   =  'SELECT * FROM users WHERE departament_id = "'.$id.'" AND status_id="1" AND id <> "291"';
      $val    =   DBSmart::DBQueryAll($query);

      if($val <> false)
      {
         foreach ($val as $k => $va) 
         {
            $user[$k]   =  [
               'id'              => $va['id'],
               'name'            => $va['name'],
               'username'        => $va['username'],
               'email'           => $va['email'],
               'departament_id'  => $va['departament_id'],
               'departament'     => $va['departament_id'],
               'status_id'       => $va['status_id'],
               'team_id'         => $va['team_id'],
               'hours_in'        => $va['hours_in'],
               'hours_out'       => $va['hours_out'],            
            ];  
         }
         return $user;

      }else{
         return false;

      } 
   
   }

//////////////////////////////////////////////////////////////////////////////////////////////

   public static function HtmlTicket()
   {
      $query      =  'SELECT id, username FROM users WHERE status_id = "1" ORDER BY username ASC';
      $user       =   DBSmart::DBQueryAll($query);
      $html       =  "";

      if($user <> false)
      {
       
         $html.='<select class="form-control" id="tic_ope" name="tic_ope" required>';
         $html.='<option value="0" selected="" disabled="">SELECTED</option>';

         foreach ($user as $k => $val) 
         {  $html.='<option value="'.$val['id'].'">'.$val['username'].'</option>'; }

         $html.='</select>';
         
         return $html;

      }else{ return $html; }

   }

//////////////////////////////////////////////////////////////////////////////////////////////

}