<?php
namespace App\Models;

use Model;

use App\Models\TicketService;
use App\Models\TicketDepartament;
use App\Models\Departament;
use App\Models\Status;

use App\Lib\Config;
use App\Lib\DBSmart;

class TicketService extends Model
{

	static $_table 		= 'data_ticket_serv';

	Public $_fillable 	= array('name', 'dep_id', 'status_id', 'created_at');

	public static function GetService()
	{
		$query	=	'SELECT * FROM data_ticket_serv ORDER BY id DESC';
		$dep 	=	DBSmart::DBQueryAll($query);

		if($dep <> false)
		{
			foreach ($dep as $d => $dal) 
			{
				$tDep[$d]	=	[
					'id'			=>	$dal['id'],
					'name'			=>	$dal['name'],
					'dep_id'		=>	$dal['dep_id'],
					'departament'	=>	Departament::GetDepById($dal['dep_id'])['name'],
					'status_id'		=>	$dal['status_id'],
					'status'		=>	Status::GetStatusById($dal['status_id'])['name']
				];
			}

			return $tDep;

		}else{ return false; }
	
	}

	public static function GetServiceHtml($id)
	{
		$query	=	'SELECT id, name FROM data_ticket_serv WHERE dep_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$dep 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($dep <> false)
		{
			$html.='<select class="form-control" id="tic_serv" name="tic_serv" required>';
			$html.='<option value="0" selected="" disabled="">SELECTED</option>';

			foreach ($dep as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			$html.='</select>';
			return $html;

		}else{ return $html; }
	
	}

	public static function GetHtmlDepart()
	{
		$query	=	'SELECT id, name FROM data_ticket_dep WHERE (departament_id = "1" OR departament_id = "4" OR departament_id = "11") AND status_id = "1" ORDER BY id ASC';

		$dep 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($dep <> false)
		{
			$html.='<select class="form-control" id="tic_departM" name="tic_departM" required>';
			$html.='<option value="0" selected="" disabled="">SELECTED</option>';

			foreach ($dep as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			$html.='</select>';
			return $html;

		}else{ return $html; }
	
	}

	public static function ServiceID($id)
	{
		$query	=	'SELECT * FROM data_ticket_serv WHERE id = "'.$id.'"';
		$dep 	=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return [
				'id'			=>	$dep['id'],
				'name'			=>	$dep['name'],
				'status_id'		=>	$dep['status_id'],
				'status'		=>	Status::GetID($dep['status_id'])['name'],
				'dep_id'		=>	$dep['dep_id'],
				'departament'	=>	Departament::GetID($dep['dep_id'])['name']
			];

		}else{ return false; }
	
	}

	public static function HtmlService($id)
	{
		$query	=	'SELECT id, name FROM data_ticket_serv WHERE dep_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';

		$dep 	=	DBSmart::DBQueryAll($query);
		$html = "";

		if($dep <> false)
		{
			$html.='<select class="form-control" id="tic_serv" name="tic_serv" required>';
			$html.='<option value="0" selected="" disabled="">SELECTED</option>';

			foreach ($dep as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			$html.='</select>';
			return $html;

		}else{ return $html; }
	
	}

	public static function GetServiceById($id)
	{
		$query	=	'SELECT * FROM data_ticket_serv WHERE id = "'.$id.'"';
		$dep 	=	DBSmart::DBQuery($query);

		if($dep <> false)
		{
			return [
				'id'			=>	$dep['id'],
				'name'			=>	$dep['name'],
				'status_id'		=>	$dep['status_id'],
				'status'		=>	Status::GetStatusById($dep['status_id'])['name'],
				'dep_id'		=>	$dep['dep_id'],
				'departament'	=>	Departament::GetDepById($dep['dep_id'])['name']
			];

		}else{ return false; }
	
	}

	public static function SaveService($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['ticket_serv_name']));

		if($info['ticket_type_serv_id'] == 'new')
		{
			$query 	=	'INSERT INTO data_ticket_serv(name, dep_id, status_id, created_at) VALUES ("'.$name.'","'.$info['ticket_dept_serv_id'].'", "'.$info['ticket_serv_status_id'].'","'.$date.'")';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['ticket_type_serv_id'] == 'edit') 
		{
			$query 	=	'UPDATE data_ticket_serv SET name="'.$name.'", dep_id="'.$info['ticket_dept_serv_id'].'", status_id="'.$info['ticket_serv_status_id'].'" WHERE id = "'.$info['ticket_serv_id'].'"';
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	
	}
}

