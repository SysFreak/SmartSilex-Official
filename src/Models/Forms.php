<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Forms;
use App\Models\Service;
use App\Models\Country;

use App\Lib\Config;
use App\Lib\DBSmart;

class Forms extends Model {

	static $_table 		= 'data_mkt_forms';

	Public $_fillable 	= array('name', 'service_id', 'country_id', 'status_id');

	public static function GetForms()
	{
		
		$query 	= 	'SELECT * FROM data_mkt_forms ORDER BY id ASC';
		$form 	=	DBSmart::DBQueryAll($query);
		
		if($form <> false)
		{
			foreach ($form as $k => $val) 
			{
				$forms[$k] = array(
					'id' 	 		=> 	$val['id'], 
					'name' 	 		=>	$val['name'],
					'service'  		=>	Service::ServiceById($val['service_id'])['name'],
					'service_di'  	=>	$val['service_id'],
					'country' 		=> 	Country::GetCountryById($val['country_id'])['name'],
					'country_id'	=>	$val['country_id'],
					'status' 		=> 	Status::GetStatusById($val['status_id'])['name'],
					'status_id'		=>	$val['status_id']
				);
			}
	        
	        return $forms;

		}else{ return false; }
	}

	public static function GetFormsById($id)
	{
		
		$query 	= 	'SELECT * FROM data_mkt_forms WHERE id = "'.$id.'" ORDER BY id ASC';
		$form 	=	DBSmart::DBQuery($query);
		
		if($form <> false)
		{
			return array(
			 	'id'           => $form['id'],
			 	'name'         => $form['name'],
			 	'service'      => $form['service_id'],
			 	'country'      => $form['country_id'],
			 	'status'       => $form['status_id']
			);

		}else{ return false; }
	}

	public static function GetFormsByCountryID($id)
	{

		$query 	= 	'SELECT id, name, status_id FROM data_mkt_forms WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id DESC';
		$form 	=	DBSmart::DBQueryAll($query);

		if($form <> false)
		{

			foreach ($form as $k => $val) 
			{
				$forms[$k] = [
					'id'		=>	$val['id'],
					'name'		=>	$val['name'],
					'status'	=>	$val['status_id']
				];
			}
			return $forms;

		}else{ return false; }
		
	}

	public static function GetFormsByCountry($id)
	{

		$query 	= 	'SELECT id, name FROM data_mkt_forms WHERE country_id = "'.$id.'" AND status_id = "1" ORDER BY id ASC';
		$form 	=	DBSmart::DBQueryAll($query);

		$html = "";

		if($form <> false)
		{
			foreach ($form as $k => $val) 
			{  $html.='<option value="'.$val['id'].'">'.$val['name'].'</option>'; }

			return $html;

		}else{ return $html; }
	}

	public static function GetFormsByName($info)
	{

		$query 	= 	'SELECT * FROM data_mkt_forms WHERE name = "'.$info['name_ob'].'" AND service_id = "'.$info['service_id_ob'].'" ORDER BY id ASC';
		$form 	=	DBSmart::DBQuery($query);

		if($form <> false)
		{
			return array('id' => $form['id'], 'name' => $form['name'], 'service_id' => $form['service_id'], 'country_id' => $form['country_id'], 'status' => $form['status_id']);
		}else{
			return false;
		}
	}

	public static function SaveForms($info)
	{
		
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name_forms']));

		if($info['type_forms'] == 'new')
		{
			$query 	= 	'INSERT INTO data_mkt_forms(name, service_id, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['service_id_forms'].'", "'.$info['country_id_forms'].'", "'.$info['status_id_forms'].'", "'.$date.'")';
			
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false ) ? true : false;
		}
		elseif ($info['type_forms'] == 'edit') 
		{
			$query 	=	'UPDATE data_mkt_forms SET name="'.$name.'", service_id="'.$info['service_id_forms'].'", country_id="'.$info['country_id_forms'].'", status_id="'.$info['status_id_forms'].'" WHERE id = "'.$info['id_forms'].'"';
			
			$dep 	=	DBSmart::DataExecute($query);

			return ($dep <> false) ? true : false;
		}
	}
}

