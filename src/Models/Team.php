<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Team;

use App\Lib\Config;
use App\Lib\DBSmart;

class Team extends Model {

	static $_table 		= 'data_teams';

	Public $_fillable 	= array('name', 'status_id');

	public static function GetTeams()
	{
		$query 	= 	'SELECT id, name, status_id FROM data_teams ORDER BY id DESC';
		$team 	=	DBSmart::DBQueryAll($query);

		$teams = array();

		if($team <> false)
		{
			foreach ($team as $k => $val) 
			{
				$teams[$k] = array(
					'id' 	 	=> $val['id'], 
					'name' 	 	=> $val['name'], 
					'status' 	=> Status::GetStatusById($val['status_id'])['name'],
					'status_id' => $val['status_id']
				);
			}
	        
	        return $teams;

		}else{ 	return false;	}
	}

	public static function GetTeamStatus()
	{
		$query 	= 	'SELECT id, name, status_id FROM data_teams WHERE status_id = "1" ORDER BY ID ASC';
		$team 	=	DBSmart::DBQueryAll($query);
		
		$teams = array();

		if($team <> false)
		{
			foreach ($team as $k => $val) 
			{
				$teams[$k] = array(
					'id' 	 	=> $val['id'], 
					'name' 	 	=> $val['name'], 
					'status' 	=> Status::GetStatusById($val['status_id'])['name'],
					'status_id' => $val['status_id']
				);
			}
	        
	        return $teams;

		}else{ 	return false;	}
	}


	public static function GetTeamById($id)
	{
		$query 	= 	'SELECT id, name, status_id FROM data_teams WHERE id = "'.$id.'"';
		$team 	=	DBSmart::DBQuery($query);

		if($team <> false)
		{
			return array(
				'id' => $team['id'], 'name' => $team['name'], 'status' => $team['status_id']
			);

		}else{ 	return false;	}
	}

	public static function GetTeamByName($info)
	{

		$query 	= 	'SELECT id, name, status_id FROM data_teams WHERE name = "'.$info.'"';
		$team 	=	DBSmart::DBQuery($query);

		if($team <> false)
		{
			return array('id' => $team['id'], 'name' => $team['name'], 'status' => $team['status_id']);

		}else{
			return false;
		}
	}

	public static function SaveTeam($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();
		$name 		= strtoupper($_replace->deleteTilde($info['name_t']));

		if($info['type_t'] == 'new')
		{
			$query 	= 	'INSERT INTO data_teams(name, status_id) VALUES ("'.$name.'", "'.$info['status_id_t'].'")';
			$team 	=	DBSmart::DataExecute($query);

			return ($team <> false ) ? true : false;
		}
		elseif ($info['type_t'] == 'edit') 
		{
			$query 	=	'UPDATE data_teams SET name="'.$name.'", status_id="'.$info['status_id_t'].'" WHERE id = "'.$info['id_t'].'"';
			$team 	=	DBSmart::DataExecute($query);
			return ($team <> false) ? true : false;
		}
	}

}

