<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\Country;
use App\Models\WirelessPlan;

use App\Lib\Config;
use App\Lib\DBSmart;

class WirelessPlan extends Model 
{

	static $_table 		= 'data_wireless';

	public $_fillable 	= array('name', 'country_id', 'status_id', 'created_at');

	public static function GetWireless()
	{
		$query 	= 	'SELECT * FROM data_wireless ORDER BY id ASC';
		$wrl 	=	DBSmart::DBQueryAll($query);
		
		if($wrl <> false)
		{
			foreach ($wrl as $w => $wr) 
			{
				$wireless[$w] = [
					'id'			=>	$wr['id'],
					'name'			=>	$wr['name'],
					'country_id'	=>	$wr['country_id'],
					'country'		=>	Country::GetCountryById($wr['country_id'])['name'],
					'status_id'		=>	$wr['status_id'],
					'status'		=>	Status::GetStatusById($wr['status_id'])['name'],
				];
			}
			return $wireless;
		}else{
			return false;

		}
	}

	public static function GetWirelessById($id)
	{

		$query 	= 	'SELECT * FROM data_wireless WHERE id = "'.$id.'" ORDER BY id ASC';
		$wrl 	=	DBSmart::DBQueryAll($query);

		if($wrl <> false)
		{
			$wireless = [
				'id'			=>	$wrl['id'],
				'name'			=>	$wrl['name'],
				'country_id'	=>	$wrl['country_id'],
				'country'		=>	Country::GetCountryById($wrl['country_id'])['name'],
				'status_id'		=>	$wrl['status_id'],
				'status'		=>	Status::GetStatusById($wrl['status_id'])['name'],
			];
			return $wireless;
		}else{
			return false;

		}
	}

	public static function SaveWireless($info)
	{
		$date = date('Y-m-d H:i:s', time());

		$_replace  	= new Config();

		$name 		= strtoupper($_replace->deleteTilde($info['name']));

		if($info['type_wrl'] == 'new')
		{
			$query 	= 	'INSERT INTO data_wireless(name, country_id, status_id, created_at) VALUES ("'.$name.'", "'.$info['country_id_wrl'].'", "'.$info['status_id_wrl'].'", "'.$date.'")';
			$zip 	=	DBSmart::DataExecute($query);

			return ($zip <> false ) ? true : false;
		}
		elseif ($info['type_wrl'] == 'edit') 
		{
			$query 	=	'UPDATE data_wireless SET name="'.$name.'", country_id="'.$info['country_id_wrl'].'", status_id="'.$info['status_id_wrl'].'" WHERE id = "'.$info['id_wrl'].'"';
			$zip 	=	DBSmart::DataExecute($query);

			return ($zip <> false) ? true : false;
		}
	}
}

		