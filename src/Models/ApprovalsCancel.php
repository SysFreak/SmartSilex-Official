<?php
namespace App\Models;

use Model;

use App\Models\Approvals;
use App\Models\ApprovalsCancel;
use App\Models\ApprovalsRespCancel;
use App\Models\Status;
use App\Models\Country;
use App\Models\Service;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

class ApprovalsCancel extends Model 
{
	static $_table 		= 'cp_appro_resp_cancel';

	Public $_fillable 	= array('id', 'ticket', 'client_id', 'service_id', 'reason_id', 'operator_id', 'created_at');

/////////////////////////////////////////////////////////////////////////

	public static function SaveCancel($info, $data)
	{
		$query 	=	'INSERT INTO cp_appro_resp_cancel(ticket, client_id, service_id, reason_id, operator_id, created_at) VALUES ("'.$data['ticket'].'", "'.$data['client'].'", "'.$data['service'].'", "'.$info.'", "'.$data['operator'].'", "'.$data['date'].'")';

		$apro 	=	DBSmart::DataExecute($query);

		return ($apro <> false ) ? true : false;
	}

/////////////////////////////////////////////////////////////////////////

	public static function GetObjectionsByService($serv)
	{
		
		$query 		= 	'SELECT id, name, status FROM cp_appro_resp_cancel WHERE service_id = "'.$serv.'" AND status_id = "1" ORDER BY id ASC';
		
		$service 	=	DBSmart::DBQueryAll($query);

		if($service <> false)
		{
			foreach ($service as $k => $val) { $obj[$k] = ['id' => $val['id'], 'name' => $val['name'], 'status' => $val['status_id'] ]; }

			return $obj;

		}else{ return false; }
	}

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

}