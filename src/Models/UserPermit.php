<?php
namespace App\Models;

use Model;
use App\Models\Auth;
use App\Models\Departament;
use App\Models\Status;
use App\Models\Role;
use App\Models\Team;
use App\Models\User;
use App\Models\UserRol;
use App\Models\UserPermit;

use App\Lib\DBSmart;
use App\Lib\Config;

use PDO;

class UserPermit extends Model 
{

 static $_table = 'user_permits';
	
	protected $_fillable = array('id', 'user_id', 'db', 'ident', 'ss', 'mkt', 'cc', 'ab', 'pr', 'tvpr', 'sepr', 'inpr', 'mrpr', 'vz', 'invz', 'rf', 'ac', 'nt', 'created_at');  

	public static function GetPermitByUser($id)
	{
		$query 		=	'SELECT * FROM user_permits WHERE user_id = "'.$id.'" ORDER BY id DESC';
		$per        =   DBSmart::DBQueryAll($query);		

		if($per)
		{
			foreach ($per as $p => $pe) 
			{
				$permits[$p]	=	
				[
					'id'		=>	$pe['id'],
					'user_id'	=>	$pe['user_id'],
					'user'		=>	User::GetUserById($pe['user_id'])['username'],
					'db'		=>	$pe['db'],
					'ident'		=>	$pe['ident'],
					'ss'		=>	$pe['ss'],
					'mkt'		=>	$pe['mkt'],
					'cc'		=>	$pe['cc'],
					'ab'		=>	$pe['ab'],
					'pr'		=>	$pe['pr'],
					'tvpr'		=>	$pe['tvpr'],
					'sepr'		=>	$pe['sepr'],
					'inpr'		=>	$pe['inpr'],
					'mrpr'		=>	$pe['mrpr'],
					'vz'		=>	$pe['vz'],
					'invz'		=>	$pe['invz'],
					'rf'		=>	$pe['rf'],
					'ac'		=>	$pe['ac'],
					'nt'		=>	$pe['nt'],
				];
			}
			return $permits;

		}else{ return false; }
	
	}

	public static function GetPermitByUserId($id)
	{
		$query 		=	'SELECT * FROM user_permits WHERE user_id = "'.$id.'"';
		$per        =   DBSmart::DBQuery($query);
		
		if($per)
		{
			$permits	=	
			[
				'id'		=>	$per['id'],
				'user_id'	=>	$per['user_id'],
				'user'		=>	User::GetUserById($per['user_id'])['username'],
				'db'		=>	$per['db'],
				'ident'		=>	$per['ident'],
				'ss'		=>	$per['ss'],
				'mkt'		=>	$per['mkt'],
				'cc'		=>	$per['cc'],
				'ab'		=>	$per['ab'],
				'pr'		=>	$per['pr'],
				'tvpr'		=>	$per['tvpr'],
				'sepr'		=>	$per['sepr'],
				'inpr'		=>	$per['inpr'],
				'mrpr'		=>	$per['mrpr'],
				'vz'		=>	$per['vz'],
				'invz'		=>	$per['invz'],
				'rf'		=>	$per['rf'],
				'ac'		=>	$per['ac'],
				'nt'		=>	$per['nt'],
			];

			return $permits;

		}else{ return false; }
	
	}

	public static function EditPermit($info)
	{
		$query 	=	'UPDATE user_permits SET db="'.$info['db'].'", ident="'.$info['ident'].'", ss="'.$info['ss'].'", mkt="'.$info['mkt'].'", cc="'.$info['cc'].'", ab="'.$info['ab'].'", pr="'.$info['pr'].'", tvpr="'.$info['tvpr'].'", sepr="'.$info['sepr'].'", inpr="'.$info['inpr'].'", mrpr="'.$info['mrpr'].'", vz="'.$info['vz'].'", invz="'.$info['invz'].'", rf="'.$info['rf'].'", ac="'.$info['ac'].'", nt="'.$info['nt'].'" WHERE user_id = "'.$info['id'].'"';
		$per 	=	DBSmart::DataExecute($query);

		return ($per <> false) ? true : false;
 	}

	public static function SavePermit($id)
	{

		$date       =  date('Y-m-d H:i:s', time());

		$query 	= 'INSERT INTO user_permits(user_id, db, ident, ss, mkt, cc, ab, pr, tvpr, sepr, inpr, mrpr, vz, invz, rf, ac, nt, created_at) VALUES ("'.$id.'", "on", "on", "on", "on", "on", "on", "on", "on", "on", "on", "on", "on", "on", "on", "on", "on", "'.$date.'")';
		$per 	=	DBSmart::DataExecute($query);

		return ($per <> false) ? true : false;
 	}

}
