<?php
namespace App\Models;

use Model;

use App\Models\Status;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBSmart;

class Calendar extends Model {

	static $_table = 'events';
	
	Public $_fillable = array('client_id', 'title', 'description', 'operator_id', 'color', 'process', 'notifie', 'view', 'deleted', 'start_event', 'end_event');

	public static function Events($id)
	{
		$query 	=	'SELECT * FROM events WHERE operator_id = "'.$id.'" AND process = "1" AND notifie = "0"';
		$cale 	=	DBSmart::DBQueryAll($query);

		$html 	= 	"";

		if($cale <> false)
		{
			$html.='<ul class="notification-body">';
			foreach ($cale as $c => $cal) 
			{
				$events[$c++] = array(
					'id'			=>	$cal['id'],
					'title'			=>	$cal['title'],
					'decription'	=>	$cal['description'],
					'color'			=>	$cal['color']
				);
				$html.='<li>';
				$html.='<span class="padding-10 unread">';
				$html.='<em class="badge padding-5 no-border-radius bg-color-purple txt-color-white pull-left margin-right-5"><i class="fa fa-calendar fa-fw fa-2x"></i></em>';
				$html.='<span>';
				$html.='<a href="javascript:void(0);" class="display-normal"><strong>Calendar</strong></a>: '.$cal['title'].'';
				$html.='<br>';
				$html.='<strong>Description: '.$cal['description'].'</strong><br>';
				$html.='<strong>Date: '.$cal['start_event'].'</strong><br>';
				$html.='</span>';
				$html.='</span>';
				$html.='</li>';
			}
			$html.='</ul>';

			return ['events' => $events, 'html' => $html, 'cant' => $c];
			
		}else{ return false; }
	
	}

	public static function Change($id)
	{
		$query 	=	'UPDATE events SET notifie = "1" WHERE id = "'.$id.'"';
		$dep 	=	DBSmart::DataExecute($query);
		return ($dep <> false ) ? true : false;	
	}

	public static function ChangeView($id)
	{
		$query 	=	'SELECT id, title, description, start_event FROM events WHERE operator_id = "'.$id.'" AND process = "1" AND notifie = "1"';
		$cale 	=	DBSmart::DBQueryAll($query);

		if($cale <> false)
		{
			foreach ($cale as $k => $val) 
			{
				$query 	=	'UPDATE events SET view = "1" WHERE id = "'.$val['id'].'"';
				$dep 	=	DBSmart::DataExecute($query);
			}
			return ['status' => true, 'info' => $cale];

		}else{	return ['status' => false, 'info' => $cale];	}	
	}

	public static function GetRolByUser($id)
	{

		$query 	=	'SELECT * FROM events WHERE user_id = "'.$id.'"';
		$cal 	=	DBSmart::DBQuery($query);

		if($cal <> false)
		{
			return array(
				'id'           	=> $cal['id'],
				'title'        	=> $cal['title'],
				'description'  	=> $cal['description'],
				'iconselect'	=> $cal['iconselect'],
				'priority'		=> $cal['priority'],
				'status'		=> Status::GetStatusById($cal['status_id'])['name'],
				'start_event'	=> $cal['start_event'],
				'end_event'		=> $cal['end_event']
			);

		}else{ 	return false;	}	
	}

	public static function InsertEvent($info, $user)
	{
		$_replace 	=	new Config();


		$dateI 	=	$info['dateEvent'].' '.$info['i_hora'].':'.$info['i_min'].':00';
		$dateF 	=	$info['dateEvent'].' '.$info['f_hora'].':'.$info['f_min'].':00';

		$query 	=	'INSERT INTO events(client_id, title, description, operator_id, color, process, notifie, view, deleted, start_event, end_event) VALUES ("'.$info['re_client'].'", "'.strtoupper($_replace->deleteTilde($info['title'])).'", "'.strtoupper($_replace->deleteTilde($info['description'])).'", "'.$user.'", "'.$info['priority'].'", "0", "0", "0", "0", "'.$dateI.'", "'.$dateF.'")';

		$dep 	=	DBSmart::DataExecute($query);

		return ($dep <> false ) ? true : false;
	}

	public static function ViewEvent($user)
	{
		$query 	=	'SELECT * FROM events WHERE user_id = "'.$user.'" status_id = "1"';
		$cale 	=	DBSmart::DBQueryAll($query);

		if($cale <> false)
		{		
				$calendar[] = array(
					'id'			=>	1,
					'title'			=>	"Titulo de Prueba",
					'description'	=>	"Descricion",
					'iconselect'	=>	"",
					'priority'		=>	"",
					'start'			=>	'2019-06-05 15:30:00',
					'end'			=>	'2019-06-05 15:30:00'
				);
			return $calendar;

		}else{ return false; }	
	
	}

	public static function ViewList($user)
	{
		$query 	=	'SELECT * FROM events WHERE operator_id = "'.$user.'" AND (process = "0" OR process = "1") AND view = "0" ORDER BY end_event DESC';
		$cale 	=	DBSmart::DBQueryAll($query);
		// var_dump($cale);
		// exit;

		$html 	= 	'';
		
		if($cale <> false)
		{
			$html.='<ul class="notification-body">';
			foreach ($cale as $c => $cal) 
			{
				$events[$c++] = array(
					'id'			=>	$cal['id'],
					'title'			=>	$cal['title'],
					'decription'	=>	$cal['description'],
					'color'			=>	$cal['color']
				);
				$html.='<li>';
				$html.='<span class="padding-10 unread">';
				$html.='<em class="badge padding-5 no-border-radius bg-color-purple txt-color-white pull-left margin-right-5"><i class="fa fa-calendar fa-fw fa-2x"></i></em>';
				$html.='<span>';
				$html.='<a href="javascript:void(0);" class="display-normal"><strong>Calendar</strong></a>: '.$cal['title'].'';
				$html.='<br>';
				$html.='<strong>Description: '.$cal['description'].'</strong><br>';
				$html.='<strong>Date: '.$cal['start_event'].'</strong><br>';
				$html.='</span>';
				$html.='</span>';
				$html.='</li>';
			}
			$html.='</ul>';

			return ['status' => true, 'html' => $html, 'cant' => $c];

		}else{ return ['status' => false, 'html' => '', 'cant' => '']; }
	}

	public static function GetEventsByClient($client)
	{

		$query 	=	'SELECT * FROM events WHERE client_id = "'.$client.'" ORDER BY id ASC';
		$cal 	=	DBSmart::DBQueryAll($query);

		if($cal <> false)
		{
			foreach ($cal as $k => $val) 
			{
				$resp[$k] = [
					'title'		=>	$val['title'],
					'operator'	=>	User::GetUserById($val['operator_id'])['name'],
					'status'	=>	($val['process'] == 0 ) ? 'Pending' : 'Finalized',
					'end'		=>	$val['end_event']
				];
			}
			return $resp;

		}else{	return false;	}
	}
}