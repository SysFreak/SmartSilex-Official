<?php
namespace App\Models;

use Model;

use App\Lib\Config;
use App\Lib\DBBoom;
use App\Lib\DBSmart;

use App\Models\Drives;
use App\Models\Country;
use App\Models\Town;
use App\Models\Departament;
use App\Models\Origen;
use App\Models\Medium;
use App\Models\Objetive;
use App\Models\Conversation;
use App\Models\ServiceM;
use App\Models\Campaign;
use App\Models\OrderType;
use App\Models\Package;
use App\Models\Provider;
use App\Models\Payment;
use App\Models\Referred;
use App\Models\StatusService;


use PDO;

class Drives extends Model 
{

////////////////////////////////////////////////////////////////////////
//////////////////////////// Drives Leads //////////////////////////////
////////////////////////////////////////////////////////////////////////

	////////////////////// Television Puerto Rico //////////////////////
	
	public static function RequestTVInfo($ticket)
	{

		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M", "MASCULINO", "FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, t3.name AS pais, t4.name AS pueblo, t9.name AS origen, t10.name AS medio, t11.name AS objetive, t12.name AS post, t13.name AS service, t14.name AS campaign, t18.name AS formulario, t19.name AS conversacion, t16.name AS tipo_orden, t1.tv, t1.decoder_id AS decodificador, t1.dvr_id AS dvr, t6.name AS paquete, t5.name AS proveedor_ant, t17.name AS metodo_pago, (SELECT IF(t1.ref_id = "1", "SI", "NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT IF(t1.sup_id = "1", "SI", "NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, t15.name AS status_servicio, (SELECT IF(t1.advertising_id= "1", "SI", "NO")) AS advertising, (SELECT IF(t1.ch_hd = "on", "SI", "NO")) AS hd, (SELECT IF(t1.ch_dvr = "on", "SI", "NO")) AS dvr_add, (SELECT IF(t1.ch_hbo = "on", "SI", "NO")) AS hbo, (SELECT IF(t1.ch_cinemax = "on", "SI", "NO")) AS cinemax, (SELECT IF(t1.ch_starz = "on", "SI", "NO")) AS starz, (SELECT IF(t1.ch_showtime = "on", "SI", "NO")) AS showtime, (SELECT IF(t1.gif_ent = "on", "SI", "NO")) AS gift_ent, (SELECT IF(t1.gif_choice = "on", "SI", "NO")) AS gift_choice, (SELECT IF(t1.gif_xtra = "on", "SI", "NO")) AS gift_xtra, (SELECT username FROM users WHERE id = t8.owen_id) as propietario, (SELECT departament_id FROM users WHERE id = t8.owen_id) as owen_dep, (SELECT NAME FROM data_departament WHERE id = owen_dep) as p_departamento, (SELECT username FROM users WHERE id = t8.assistan_id) as asistente, (SELECT departament_id FROM users WHERE id = t8.assistan_id) as assit_id, (SELECT NAME FROM data_departament WHERE id = assit_id) as a_departamento, t1.created_at FROM cp_service_pr_tv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_country AS t3 ON (t2.country_id = t3.id) INNER JOIN data_town AS t4 ON (t2.country_id = t4.id) INNER JOIN data_provider AS t5 ON (t1.provider_id = t5.id) INNER JOIN data_package AS t6 ON (t1.package_id = t6.id) INNER JOIN cp_serv_prop AS t7 ON (t1.ticket = t7.ticket_id) INNER JOIN cp_serv_prop AS t8 ON (t1.ticket = t8.ticket_id) INNER JOIN data_mkt_origen AS t9 ON (t2.mkt_origen_id = t9.id) INNER JOIN data_mkt_medium AS t10 ON (t2.mkt_medium_id = t10.id) INNER JOIN data_mkt_objetive AS t11 ON (t2.mkt_objective_id = t11.id) INNER JOIN data_mkt_conversation AS t12 ON (t2.mkt_post_id = t12.id) INNER JOIN data_mkt_service AS t13 ON (t2.mkt_service_id = t13.id) INNER JOIN mkt_campaign AS t14 ON (t2.mkt_campaign_id = t14.id) INNER JOIN data_status_service AS t15 ON (t1.service_id = t15.id) INNER JOIN data_type_order AS t16 ON (t1.type_order_id = t16.id) INNER JOIN data_payment AS t17 ON (t1.payment_id = t17.id) INNER JOIN data_mkt_forms AS t18 ON (t2.mkt_forms_id = t18.id) INNER JOIN data_mkt_conversation AS t19 ON (t2.mkt_conv_id = t19.id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function InsertDBTV($info)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query = 'INSERT INTO request_television(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, tipo_orden, tv, decodificador, dvr, paquete, proveedor_ant, metodo_pago, referido, referido_nombre, apoyo, apoyo_usuario, status_servicio, advertising, hd, dvr_add, hbo, cinemax, starz, showtime, gift_ent, gift_cho, gift_xtra, propietario, p_departamento, assistente, a_departament, created_at) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'",  "'.$info['nacimiento'].'",  "'.$info['edad'].'",  "'.$info['genero'].'",  "'.$info['telefono'].'",  "'.$info['proveedor'].'",  "'.$info['email'].'",  "'.$info['principal'].'",  "'.$info['postal'].'",  "'.$info['pais'].'",  "'.$info['pueblo'].'",  "'.$info['origen'].'",  "'.$info['medio'].'",  "'.$info['objetive'].'",  "'.$info['post'].'",  "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campaign'].'",  "'.$info['tipo_orden'].'",  "'.$info['tv'].'",  "'.$info['decodificador'].'",  "'.$info['dvr'].'",  "'.$info['paquete'].'",  "'.$info['proveedor_ant'].'",  "'.$info['metodo_pago'].'",  "'.$info['referido'].'",  "'.$info['ref_nombre'].'",  "'.$info['apoyo'].'",  "'.$info['apoyo_usuario'].'",  "'.$info['status_servicio'].'",  "'.$info['advertising'].'",  "'.$info['hd'].'",  "'.$info['dvr_add'].'",  "'.$info['hbo'].'",  "'.$info['cinemax'].'",  "'.$info['starz'].'",  "'.$info['showtime'].'",  "'.$info['gift_ent'].'",  "'.$info['gift_choice'].'",  "'.$info['gift_xtra'].'",  "'.$info['propietario'].'",  "'.$info['p_departamento'].'",  "'.$info['asistente'].'",  "'.$info['a_departamento'].'",  "'.$info['created_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

	//////////////////////// Security Puerto Rico ////////////////////////

	public static function RequestSecInfo($ticket)
	{

		$query 		=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M","MASCULINO","FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, t3.name AS pais, t4.name AS pueblo, t5.name AS origen, t6.name AS medio, t7.name AS objetive, t8.name AS post, t9.name AS service, t17.name AS formulario, t18.name AS conversacion, t10.name AS campana, (SELECT IF(t1.previously_id= 0,"NO","SI")) AS sist_previo, t1.equipment AS desc_equipos, t11.name AS camaras, t1.comp_equipment AS emp_monitoreo, t12.name AS dvr, t1.add_equipment AS equip_adicional, t1.password_alarm AS password, t13.name AS metodo_pago, (SELECT IF(t1.ref_id = 1,"SI","NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT IF(t1.sup_id = 1,"SI","NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, t14.name AS tipo_orden, t15.name AS status_servicio, t1.contact_1 AS contacto1, t1.contact_1_desc AS contacto1desc, t1.contact_2 AS contacto2, t1.contact_2_desc AS contacto2desc, t1.contact_3 AS contacto3, t1.contact_3_desc AS contacto3desc, (SELECT username FROM users WHERE id = t16.owen_id) as propietario, (SELECT departament_id FROM users WHERE id = t16.owen_id) as owen_dep, (SELECT NAME FROM data_departament WHERE id = owen_dep) as p_departamento, (SELECT username FROM users WHERE id = t16.assistan_id) as asistente, (SELECT departament_id FROM users WHERE id = t16.assistan_id) as assit_id, (SELECT NAME FROM data_departament WHERE id = assit_id) as a_departamento, t1.created_at FROM cp_service_pr_sec AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_country AS t3 ON (t2.country_id = t3.id) INNER JOIN data_town AS t4 ON (t2.town_id = t4.id) INNER JOIN data_mkt_origen AS t5 ON (t2.mkt_origen_id = t5.id) INNER JOIN data_mkt_medium AS t6 ON (t2.mkt_medium_id = t6.id) INNER JOIN data_mkt_objetive AS t7 ON (t2.mkt_objective_id = t7.id) INNER JOIN data_mkt_conversation AS t8 ON (t2.mkt_post_id = t8.id) INNER JOIN data_mkt_service AS t9 ON (t2.mkt_service_id = t9.id) INNER JOIN mkt_campaign AS t10 ON (t2.mkt_campaign_id = t10.id) INNER JOIN data_camera AS t11 ON (t1.cameras_id = t11.id)INNER JOIN data_dvr AS t12 ON (t1.dvr_id = t12.id) INNER JOIN data_payment AS t13 ON (t1.payment_id = t13.id) INNER JOIN data_type_order AS t14 ON (t1.type_order_id = t14.id) INNER JOIN data_status_service AS t15 ON (t1.service_id = t15.id) INNER JOIN cp_serv_prop AS t16 ON (t1.ticket = t16.ticket_id) INNER JOIN data_mkt_forms AS t17 ON (t2.mkt_forms_id = t17.id)INNER JOIN data_mkt_conversation AS t18 ON (t2.mkt_conv_id = t18.id) AND t1.ticket = "'.$ticket.'"';

		$dep 		=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertDBSEC($info)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 	'INSERT INTO request_security(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, sist_previo, desc_equipos, camaras, emp_monitoreo, dvr, equip_adicional, password, metodo_pago, referido, referido_nombre, apoyo, apoyo_usuario, tipo_orden, status_servicio, contacto1, contacto1desc, contacto2, contacto2desc, contacto3, contacto3desc, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'","'.$info['cliente'].'","'.$info['nombre'].'","'.$info['nacimiento'].'","'.$info['edad'].'","'.$info['genero'].'","'.$info['telefono'].'","'.$info['proveedor'].'","'.$info['email'].'","'.$info['principal'].'","'.$info['postal'].'","'.$info['pais'].'","'.$info['pueblo'].'","'.$info['origen'].'","'.$info['medio'].'","'.$info['objetive'].'","'.$info['post'].'","'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['sist_previo'].'","'.$info['desc_equipos'].'","'.$info['camaras'].'","'.$info['emp_monitoreo'].'","'.$info['dvr'].'","'.$info['equip_adicional'].'","'.$info['password'].'","'.$info['metodo_pago'].'","'.$info['referido'].'","'.$info['ref_nombre'].'","'.$info['apoyo'].'","'.$info['apoyo_usuario'].'","'.$info['tipo_orden'].'","'.$info['status_servicio'].'","'.$info['contacto1'].'","'.$info['contacto1desc'].'","'.$info['contacto2'].'","'.$info['contacto2desc'].'","'.$info['contacto3'].'","'.$info['contacto3desc'].'","'.$info['propietario'].'","'.$info['p_departamento'].'","'.$info['asistente'].'","'.$info['a_departamento'].'","'.$info['created_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

	//////////////////////// Internet Puerto Rico ////////////////////////

	public static function RequestIntInfo($ticket)
	{

		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M","MASCULINO","FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, t3.name AS pais, t4.name AS pueblo, t5.name AS origen, t6.name AS medio, t7.name AS objetive, t8.name AS post, t9.name AS service, t19.name AS formulario, t20.name AS conversacion, t10.name AS campana, t11.name AS int_res, t13.name AS pho_res, t12.name AS int_com, t14.name AS pho_com, (SELECT IF(t1.ref_id = "1","SI","NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_cliente, (SELECT IF(t1.sup_id = "1","SI","NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, t15.name AS metodo_pago, t16.name AS tipo_orden, t17.name AS status_servicio, (SELECT username FROM users WHERE id = t18.owen_id) as propietario, (SELECT departament_id FROM users WHERE id = t18.owen_id) as owen_dep, (SELECT NAME FROM data_departament WHERE id = owen_dep) as p_departamento, (SELECT username FROM users WHERE id = t18.assistan_id) as asistente, (SELECT departament_id FROM users WHERE id = t18.assistan_id) as assit_id, (SELECT NAME FROM data_departament WHERE id = assit_id) as a_departamento, t1.created_at FROM cp_service_pr_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_country AS t3 ON (t2.country_id = t3.id) INNER JOIN data_town AS t4 ON (t2.town_id = t4.id) INNER JOIN data_mkt_origen AS t5 ON (t2.mkt_origen_id = t5.id) INNER JOIN data_mkt_medium AS t6 ON (t2.mkt_medium_id = t6.id) INNER JOIN data_mkt_objetive AS t7 ON (t2.mkt_objective_id = t7.id) INNER JOIN data_mkt_conversation AS t8 ON (t2.mkt_post_id = t8.id) INNER JOIN data_mkt_service AS t9 ON (t2.mkt_service_id = t9.id) INNER JOIN mkt_campaign AS t10 ON (t2.mkt_campaign_id = t10.id) INNER JOIN data_internet AS t11 ON (t1.int_res_id = t11.id) INNER JOIN data_internet AS t12 ON (t1.int_com_id = t12.id) INNER JOIN data_phone AS t13 ON (t1.pho_res_id = t13.id) INNER JOIN data_phone AS t14 ON (t1.pho_com_id = t14.id) INNER JOIN data_payment AS t15 ON (t1.payment_id = t15.id) INNER JOIN data_type_order AS t16 ON (t1.type_order_id = t16.id) INNER JOIN data_status_service AS t17 ON (t1.service_id = t17.id) INNER JOIN cp_serv_prop AS t18 ON (t1.ticket = t18.ticket_id) INNER JOIN data_mkt_forms AS t19 ON (t2.mkt_forms_id = t19.id) INNER JOIN data_mkt_conversation AS t20 ON (t2.mkt_conv_id = t20.id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertDBINT($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 'INSERT INTO request_internet(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, internet_res, telefono_res, internet_com, telefono_com, referido, referido_nombre, referido_cliente, apoyo, apoyo_usuario, medoto_pago, tipo_orden, status_servicio, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['nacimiento'].'", "'.$info['edad'].'", "'.$info['genero'].'", "'.$info['telefono'].'", "'.$info['proveedor'].'", "'.$info['email'].'", "'.$info['principal'].'", "'.$info['postal'].'", "'.$info['pais'].'", "'.$info['pueblo'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objetive'].'", "'.$info['post'].'", "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['referido'].'", "'.$info['ref_nombre'].'", "'.$info['ref_cliente'].'", "'.$info['apoyo'].'", "'.$info['apoyo_usuario'].'", "'.$info['metodo_pago'].'", "'.$info['tipo_orden'].'", "'.$info['status_servicio'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['asistente'].'", "'.$info['a_departamento'].'", "'.$info['created_at'].'") ';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

//////////////////////// Internet Venezuela ////////////////////////

	public static function RequestIntVZInfo($ticket)
	{

		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M", "MASCULINO", "FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, t3.name AS pais, t4.name AS pueblo, t5.name AS origen, t6.name AS medio, t7.name AS objetive, t8.name AS post, t9.name AS service, t20.name AS formulario, t21.name AS conversacion, t10.name AS campana, t11.name AS int_res, t13.name AS pho_res, t12.name AS int_com, t14.name AS pho_com, t15.name AS metodo_pago, t16.name AS banco, t1.amount AS monto, t1.transference As transferencia, t1.email AS email_servicio, t1.trans_date AS fecha_pago, t18.name AS tipo_orden, t19.name AS status_servicio, (SELECT username FROM users WHERE id = t17.owen_id) as propietario, (SELECT departament_id FROM users WHERE id = t17.owen_id) as owen_dep, (SELECT NAME FROM data_departament WHERE id = owen_dep) as p_departamento, (SELECT username FROM users WHERE id = t17.assistan_id) as asistente, (SELECT departament_id FROM users WHERE id = t17.assistan_id) as assit_id, (SELECT NAME FROM data_departament WHERE id = assit_id) as a_departamento, t1.created_at FROM cp_service_vzla_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_country AS t3 ON (t2.country_id = t3.id) INNER JOIN data_town AS t4 ON (t2.town_id = t4.id) INNER JOIN data_mkt_origen AS t5 ON (t2.mkt_origen_id = t5.id) INNER JOIN data_mkt_medium AS t6 ON (t2.mkt_medium_id = t6.id) INNER JOIN data_mkt_objetive AS t7 ON (t2.mkt_objective_id = t7.id) INNER JOIN data_mkt_conversation AS t8 ON (t2.mkt_post_id = t8.id) INNER JOIN data_mkt_service AS t9 ON (t2.mkt_service_id = t9.id) INNER JOIN mkt_campaign AS t10 ON (t2.mkt_campaign_id = t10.id) INNER JOIN data_internet AS t11 ON (t1.int_res_id = t11.id) INNER JOIN data_internet AS t12 ON (t1.int_com_id = t12.id) INNER JOIN data_phone AS t13 ON (t1.pho_res_id = t13.id) INNER JOIN data_phone AS t14 ON (t1.pho_com_id = t14.id) INNER JOIN data_payment AS t15 ON (t1.payment_id = t15.id) INNER JOIN data_bank AS t16 ON (t1.bank_id = t16.id) INNER JOIN cp_serv_prop AS t17 ON (t1.ticket = t17.ticket_id) INNER JOIN data_type_order AS t18 ON (t1.type_order_id = t18.id) INNER JOIN data_status_service AS t19 ON (t1.service_id = t19.id) INNER JOIN data_mkt_forms AS t20 ON (t2.mkt_forms_id = t20.id) INNER JOIN data_mkt_conversation AS t21 ON (t2.mkt_conv_id = t21.id) AND t1.ticket = "'.$ticket.'"';

 		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertDBIntVZ($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 'INSERT INTO request_internet_vzla(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, internet_res, telefono_res, internet_com, telefono_com, medoto_pago, banco, monto, transferencia, email_servicio, fecha_pago, tipo_orden, status_servicio, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['nacimiento'].'", "'.$info['edad'].'", "'.$info['genero'].'", "'.$info['telefono'].'", "'.$info['proveedor'].'", "'.$info['email'].'", "'.$info['principal'].'", "'.$info['postal'].'", "'.$info['pais'].'", "'.$info['pueblo'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objetive'].'", "'.$info['post'].'", "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['metodo_pago'].'", "'.$info['banco'].'", "'.$info['monto'].'", "'.$info['transferencia'].'", "'.$info['email_servicio'].'", "'.$info['fecha_pago'].'", "'.$info['tipo_orden'].'", "'.$info['status_servicio'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['asistente'].'", "'.$info['a_departamento'].'", "'.$info['created_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

	//////////////////////// Modem Router Puerto Rico ////////////////////////

	public static function InsertDBMR($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 	'INSERT INTO request_modem_router(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, cantidad, tipo_red, ssid, pass, referido, ref_nombre, ref_cliente, apoyo, apoyo_usuario, metodo_pago, tipo_orden, status_servicio, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'","'.$info['cliente'].'", "'.$info['nombre'].'","'.$info['nacimiento'].'","'.$info['edad'].'","'.$info['genero'].'","'.$info['telefono'].'","'.$info['proveedor'].'","'.$info['email'].'","'.$info['principal'].'","'.$info['postal'].'","'.$info['pais'].'","'.$info['pueblo'].'","'.$info['origen'].'","'.$info['medio'].'","'.$info['objetive'].'","'.$info['post'].'","'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'","'.$info['cantidad'].'","'.$info['tipo_red'].'","'.$info['ssid'].'","'.$info['pass'].'","'.$info['referido'].'","'.$info['ref_nombre'].'","'.$info['ref_cliente'].'","'.$info['apoyo'].'","'.$info['apoyo_usuario'].'","'.$info['metodo_pago'].'","'.$info['tipo_orden'].'","'.$info['status_servicio'].'","'.$info['propietario'].'","'.$info['p_departamento'].'","'.$info['asistente'].'","'.$info['a_departamento'].'","'.$info['created_at'].'") ';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

	public static function RequestMRInfo($ticket)
	{

		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M", "MASCULINO", "FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, t3.name AS pais, t4.name AS pueblo, t5.name AS origen, t6.name AS medio, t7.name AS objetive, t8.name AS post, t9.name AS service, t16.name AS formulario, t17.name AS conversacion, t10.name AS campana, t1.cant AS cantidad, t11.name AS tipo_red, t1.ssid AS ssid, t1.password AS pass, (SELECT IF(t1.ref_id = "1", "SI", "NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_cliente, (SELECT IF(t1.sup_id = "1", "SI", "NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, t12.name AS metodo_pago, t13.name AS tipo_orden, t14.name AS status_servicio, (SELECT username FROM users WHERE id = t15.owen_id) as propietario, (SELECT departament_id FROM users WHERE id = t15.owen_id) as owen_dep, (SELECT NAME FROM data_departament WHERE id = owen_dep) as p_departamento, (SELECT username FROM users WHERE id = t15.assistan_id) as asistente, (SELECT departament_id FROM users WHERE id = t15.assistan_id) as assit_id, (SELECT NAME FROM data_departament WHERE id = assit_id) as a_departamento, t1.created_at FROM cp_service_pr_mr AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_country AS t3 ON (t2.country_id = t3.id) INNER JOIN data_town AS t4 ON (t2.town_id = t4.id) INNER JOIN data_mkt_origen AS t5 ON (t2.mkt_origen_id = t5.id) INNER JOIN data_mkt_medium AS t6 ON (t2.mkt_medium_id = t6.id) INNER JOIN data_mkt_objetive AS t7 ON (t2.mkt_objective_id = t7.id) INNER JOIN data_mkt_conversation AS t8 ON (t2.mkt_post_id = t8.id) INNER JOIN data_mkt_service AS t9 ON (t2.mkt_service_id = t9.id) INNER JOIN mkt_campaign AS t10 ON (t2.mkt_campaign_id = t10.id) INNER JOIN data_wireless AS t11 ON (t1.red_id = t11.id) INNER JOIN data_payment AS t12 ON (t1.payment_id = t12.id) INNER JOIN data_type_order AS t13 ON (t1.type_order_id = t13.id) INNER JOIN data_status_service AS t14 ON (t1.service_id = t14.id) INNER JOIN cp_serv_prop AS t15 ON (t1.ticket = t15.ticket_id) INNER JOIN data_mkt_forms AS t16 ON (t2.mkt_forms_id = t16.id) INNER JOIN data_mkt_conversation AS t17 ON (t2.mkt_conv_id = t17.id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	///////////////////// Request Macro Info Drive //////////////////////

	public static function ReqMacAprInfTicket($ticket)
	{
		$query 	=	'SELECT t1.ticket, t1.client_id AS cliente, t2.name AS nombre, t2.birthday AS nacimiento, t2.age AS edad,  (SELECT IF(t2.gender = "M", "MASCULINO", "FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS t_proveedor, t2.phone_alt AS alterno, (SELECT name FROM data_phone_provider WHERE id = t2.phone_alt_provider) AS a_proveedor, t2.email_main AS email, t2.add_main AS principal, t2.add_postal AS postal, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT code FROM data_zips WHERE id = t2.zip_id) AS zipcode, (SELECT name FROM data_house WHERE id = t2.h_type_id) AS tipo_casa, (SELECT name FROM data_ceiling WHERE id = t2.h_roof_id) AS tipo_techo, (SELECT name FROM data_levels WHERE id = t2.h_level_id) AS tipo_nivel, (SELECT name FROM data_mkt_origen WHERE id = t2.mkt_origen_id) AS origen, (SELECT name FROM data_mkt_medium WHERE id = t2.mkt_medium_id) AS medio, (SELECT name FROM data_mkt_objetive WHERE id = t2.mkt_objective_id) AS objectivo, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_post_id) AS post, (SELECT name FROM data_mkt_service WHERE id = t2.mkt_service_id) AS servicio, (SELECT name FROM data_mkt_forms WHERE id = t2.mkt_forms_id) AS formulario, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_conv_id) AS conversacion,(SELECT name FROM mkt_campaign WHERE id = t2.mkt_campaign_id) AS campana, (SELECT name FROM data_services WHERE id = t1.service_id) AS tipo_servicio, (CASE WHEN t1.service_id = "2" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_tv WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "3" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_sec WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "4" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1))	WHEN t1.service_id = "6" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_vzla_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) ELSE "NINGUNO" END) AS tipo_orden, (CASE WHEN t1.service_id = "2" THEN (SELECT name FROM data_leads_provider WHERE id = (SELECT origen_id AS type FROM cp_service_pr_tv WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "3" THEN (SELECT name FROM data_leads_provider WHERE id = (SELECT origen_id AS type FROM cp_service_pr_sec WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "4" THEN (SELECT name FROM data_leads_provider WHERE id = (SELECT origen_id AS type FROM cp_service_pr_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1))	WHEN t1.service_id = "6" THEN (SELECT name FROM data_leads_provider WHERE id = (SELECT origen_id AS type FROM cp_service_vzla_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) ELSE "NINGUNO" END) AS origen_lead, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS v_departamento, t1.created_at AS creado, (SELECT name FROM cp_leads_states WHERE id = (SELECT state_id FROM cp_leads_geographic WHERE client_id = t1.client_id ORDER BY id DESC LIMIT 1)) AS estado, (SELECT name FROM cp_leads_township WHERE id = (SELECT township_id FROM cp_leads_geographic WHERE client_id = t1.client_id ORDER BY id DESC LIMIT 1)) AS municipio, (SELECT name FROM cp_leads_parish WHERE id = (SELECT parish_id FROM cp_leads_geographic WHERE client_id = t1.client_id ORDER BY id DESC LIMIT 1)) AS parroquia, (SELECT name FROM cp_leads_sector WHERE id = (SELECT sector_id FROM cp_leads_geographic WHERE client_id = t1.client_id ORDER BY id DESC LIMIT 1)) AS sector FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacAprInsInfTicket($iData)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 	=	'INSERT INTO request_macro(ticket, cliente, nombre, nacimiento, edad, genero, telefono, t_proveedor, alterno, a_proveedor, email, principal, postal, pais, pueblo, zipcode, tipo_casa, tipo_techo, tipo_nivel, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, tipo_servicio, score, transactions, value, fico, tipo_orden, vendedor, v_departamento, creado, status_aprobacion, procesado, f_procesado, c_contacto, c_rason, c_operador, c_fecha, c_datos_correctos, c_datos_operador, c_datos_fecha, c_pack_editado, c_pack_operador, c_pack_fecha, c_pre_coord, c_pre_rason, c_pre_fecha, c_pre_disponibilidad, c_pre_firma_estimado, c_pre_firma_rason, c_pre_operador, c_pre_fecha_coord, c_coord, c_coord_operador, c_coord_fecha, c_cancel, c_cancel_rason, c_cancel_operador, c_cancel_fecha, c_ref, c_ref_operador, c_ref_fecha, created_at, origen_lead, estado, municipio, parroquia, sector) VALUES ("'.$iData['ticket'].'", "'.$iData['cliente'].'", "'.$iData['nombre'].'", "'.$iData['nacimiento'].'", "'.$iData['edad'].'", "'.$iData['genero'].'", "'.$iData['telefono'].'", "'.$iData['t_proveedor'].'", "'.$iData['alterno'].'", "'.$iData['a_proveedor'].'", "'.$iData['email'].'", "'.$iData['principal'].'", "'.$iData['postal'].'", "'.$iData['pais'].'", "'.$iData['pueblo'].'", "'.$iData['zipcode'].'", "'.$iData['tipo_casa'].'", "'.$iData['tipo_techo'].'", "'.$iData['tipo_nivel'].'", "'.$iData['origen'].'", "'.$iData['medio'].'", "'.$iData['objectivo'].'", "'.$iData['post'].'", "'.$iData['servicio'].'", "'.$iData['formulario'].'", "'.$iData['conversacion'].'", "'.$iData['campana'].'", "'.$iData['tipo_servicio'].'", "", "", "", "", "'.$iData['tipo_orden'].'", "'.$iData['vendedor'].'", "'.$iData['v_departamento'].'", "'.$iData['creado'].'", "PENDIENTE", "", "", "NO", "", "", "", "NO", "", "", "NO", "", "", "NO", "", "", "", "", "", "", "", "NO", "", "", "NO", "", "", "", "NO", "", "", "'.$iData['creado'].'", "'.$iData['origen_lead'].'", "'.$iData['estado'].'", "'.$iData['municipio'].'", "'.$iData['parroquia'].'", "'.$iData['sector'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////////////////////////////////////////////////////////////////
////////////////////////// END Drives Leads ////////////////////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///////////////////////// Drives Marketing /////////////////////////////
////////////////////////////////////////////////////////////////////////

//////////////////////// Asignacion de Leads MKT ///////////////////////

	public static function LeadAssigInfo($id)
	{
		$query 	=	'SELECT t1.id as mkt_id, t1.client_id AS cliente, t2.name AS nombre, t2.phone_main AS telefono, t4.name AS origen, t3.name AS interes, t5.name AS medio, t6.name AS objectivo, t7.name AS post, t8.name AS servicio, t9.name AS formulario, t10.name AS conversacion, t11.name AS campana, t12.username AS asignado, t13.username AS operador, t1.created_at FROM cp_mkt_assig_leads AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_services AS t3 ON (t1.service_i_id = t3.id) INNER JOIN data_mkt_origen AS t4 ON (t1.origen_id = t4.id) INNER JOIN data_mkt_medium AS t5 ON (t1.medium_id = t5.id) INNER JOIN data_mkt_objetive AS t6 ON (t1.objective_id = t6.id) INNER JOIN data_mkt_post AS t7 ON (t1.post_id = t7.id) INNER JOIN data_mkt_service AS t8	ON (t1.service_id = t8.id) INNER JOIN data_mkt_forms AS t9	ON (t1.form_id = t9.id) INNER JOIN data_mkt_conversation AS t10 ON (t1.conv_id = t10.id) INNER JOIN mkt_campaign AS t11 ON (t1.campaign_id = t11.id) INNER JOIN users AS t12 ON (t1.operator_id = t12.id) INNER JOIN users AS t13 ON (t1.user_id = t13.id) AND t1.client_id = "'.$id.'" ORDER BY t1.id DESC';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertLeadAssig($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 'INSERT INTO lead_assignment(mkt_id, cliente, nombre, telefono, interes, origen, medio, objectivo, post, servicio, formulario, conversaciones, campana, asignado, operador, created_at) VALUES("'.$info['mkt_id'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['telefono'].'", "'.$info['interes'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objectivo'].'", "'.$info['post'].'", "'.$info['servicio'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['asignado'].'", "'.$info['operador'].'", "'.$info['created_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

//////////////////// Respuesta Asignacion de Leads MKT /////////////////

	public static function LeadRespInfo($id)
	{

		$query 	=	'SELECT t1.mkt_id, t1.client_id as cliente, t3.name AS nombre, t3.phone_main AS telefono, t4.name AS interes, t5.name AS origen, t6.name AS medio, t7.name AS objectivo, t8.name AS post, t9.name AS servicio, t10.name AS formulario, t11.name AS conversacion, t12.name AS campana, (SELECT IF(t1.contacted = "1","SI","NO")) AS contactado, (SELECT IF(t1.tv_pr_contacted = "1","SI","NO")) AS tv_contactado, t13.name AS tv_objecion, (SELECT IF(t1.sec_pr_contacted = "1","SI","NO")) AS sec_contactado, t14.name AS sec_objecion, (SELECT IF(t1.int_pr_contacted = "1","SI","NO")) AS int_contactado, t15.name AS int_objecion, (SELECT IF(t1.wrl_pr_contacted = "1","SI","NO")) AS wrl_contactado, t16.name AS wrl_objecion, t17.username AS operador_asignado,  t1.created_at AS fecha_respuesta,  t18.username AS operador_mkt,  t2.created_at AS fecha_asignacion FROM cp_ope_assig_resp AS t1 INNER JOIN cp_mkt_assig_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN cp_leads AS t3 ON (t1.client_id = t3.client_id)INNER JOIN data_services AS t4 ON (t2.service_i_id = t4.id)INNER JOIN data_mkt_origen AS t5 ON (t2.origen_id = t5.id) INNER JOIN data_mkt_medium AS t6 ON (t2.medium_id = t6.id) INNER JOIN data_mkt_objetive AS t7 ON (t2.objective_id = t7.id) INNER JOIN data_mkt_post AS t8 ON (t2.post_id = t8.id) INNER JOIN data_mkt_service AS t9	ON (t2.service_id = t9.id) INNER JOIN data_mkt_forms AS t10	ON (t2.form_id = t10.id) INNER JOIN data_mkt_conversation AS t11 ON (t2.conv_id = t11.id) INNER JOIN mkt_campaign AS t12 ON (t2.campaign_id = t12.id) INNER JOIN data_objections AS t13 ON (t1.tv_pr_objection = t13.id) INNER JOIN data_objections AS t14 ON (t1.sec_pr_objection = t14.id) INNER JOIN data_objections AS t15 ON (t1.int_pr_objection = t15.id) INNER JOIN data_objections AS t16 ON (t1.wrl_pr_objection = t16.id) INNER JOIN users AS t17 ON (t1.created_by = t17.id)INNER JOIN users AS t18 ON (t2.user_id = t18.id) AND t1.mkt_id = t2.id AND t1.client_id = "'.$id.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}
	
	public static function InsertLeadResp($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 'INSERT INTO lead_response(mkt_id, cliente, nombre, telefono, interes, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, contactado, tv_contactado, tv_objecion, sec_contactado, sec_objecion, int_contactado, int_objecion, wrl_contactado, wrl_objecion, operador_asignado, fecha_respuesta, operador_mkt, fecha_asignacion) VALUES ("'.$info['mkt_id'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['telefono'].'", "'.$info['interes'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objectivo'].'", "'.$info['post'].'", "'.$info['servicio'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['contactado'].'", "'.$info['tv_contactado'].'", "'.$info['tv_objecion'].'", "'.$info['sec_contactado'].'", "'.$info['sec_objecion'].'", "'.$info['int_contactado'].'", "'.$info['int_objecion'].'", "'.$info['wrl_contactado'].'", "'.$info['wrl_objecion'].'", "'.$info['operador_asignado'].'", "'.$info['fecha_respuesta'].'", "'.$info['operador_mkt'].'", "'.$info['fecha_asignacion'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////////////////////////////////////////////////////////////////
//////////////////////// END Drives Marketing //////////////////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///////////////////////// Drives Approvals /////////////////////////////
////////////////////////////////////////////////////////////////////////


//////////////////////// Process Qualification ///////////////////////

	public static function GetParcialInfoAprovalsTicket($ticket)
	{
		$query 	=	'SELECT t1.created_at AS creado, t1.ticket, t1.client_id AS cliente, t2.name AS nombre, t2.phone_main AS telefono, t2.phone_alt AS alterno, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_services WHERE id = t1.service_id) AS servicio, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departamento, (CASE WHEN t1.service_id = "2" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_tv WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "3" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_sec WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "4" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1))	WHEN t1.service_id = "6" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_vzla_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) ELSE "NINGUNO" END) AS tipo_orden, (CASE WHEN (t1.status_id = "0" AND t1.cancel = "0") THEN "CANCELADO" WHEN (t1.status_id = "1" AND t1.cancel = "1") THEN "PENDIENTE" WHEN (t1.status_id = "2" AND t1.cancel = "1") THEN "PROCESADO"	ELSE "NINGUNO" END) AS status_aprobacion, (SELECT name FROM data_score WHERE id = t1.score) AS score, t1.transactions, t1.value, t1.fico, (SELECT username FROM users WHERE id = t1.finish_by) AS operador, (SELECT name FROM data_departament WHERE id = t1.finish_dep_id) AS departamento_ope, t1.finish_at AS d_fin_apro FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertParcialInfoAprovalsTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();
        $date 		= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO coordinations_data(creado, ticket, cliente, nombre, telefono, alterno, pais, pueblo, servicio, vendedor, departamento, tipo_orden, status_aprobacion, score, transactions, value, fico, operador, d_fin_apro, c_contacto, c_rason, c_datos_correctos, c_pack_editado, c_pack_operador, c_pack_fecha, c_pre_coord, c_pre_rason, c_pre_fecha, c_pre_disp, c_pre_ope, c_pre_fecha_coord, c_coord, c_coord_ope, c_coord_fecha_proc, c_cancel, c_cancel_ope, c_cancel_fecha, c_ref, c_ref_ope, c_ref_fecha, created_at) VALUES ("'.$iData['creado'].'", "'.$iData['ticket'].'", "'.$iData['cliente'].'", "'.$iData['nombre'].'", "'.$iData['telefono'].'", "'.$iData['alterno'].'", "'.$iData['pais'].'", "'.$iData['pueblo'].'", "'.$iData['servicio'].'", "'.$iData['vendedor'].'", "'.$iData['departamento'].'", "'.$iData['tipo_orden'].'", "'.$iData['status_aprobacion'].'", "'.$iData['score'].'", "'.$iData['transactions'].'", "'.$iData['value'].'", "'.$iData['fico'].'", "'.$iData['operador'].'", "'.$iData['d_fin_apro'].'","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "'.$date.'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////////////////////////////////////////////////////////////////
	
	public static function ApproRespInfo($id)
	{

		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M", "MASCULINO", "FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, t3.name AS pais, t4.name AS pueblo, t5.name AS origen, t6.name AS medio, t7.name AS objectivo, t8.name AS post, t9.name AS service, t17.name as formulario, t18.name AS conversacion, t10.name AS campana, t11.name AS tipo_servicio, t12.name AS score, t1.transactions AS transaccion, t1.value AS valor, t1.fico AS fico, (CASE WHEN t1.status_id = "0" THEN "CANCELADO" WHEN t1.status_id = "1" THEN "PENDIENTE" WHEN t1.status_id = "2" THEN "PROCESADO" END) AS status_servicio, t13.username AS creador, t15.name AS c_departamento, t1.created_at, t14.username AS finalizado, t16.name as f_departamento, t1.finish_at FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_country AS t3 ON (t2.country_id = t3.id) INNER JOIN data_town AS t4 ON (t2.country_id = t4.id) INNER JOIN data_mkt_origen AS t5 ON (t2.mkt_origen_id = t5.id) INNER JOIN data_mkt_medium AS t6 ON (t2.mkt_medium_id = t6.id) INNER JOIN data_mkt_objetive AS t7 ON (t2.mkt_objective_id = t7.id) INNER JOIN data_mkt_conversation AS t8 ON (t2.mkt_post_id = t8.id) INNER JOIN data_mkt_service AS t9 ON (t2.mkt_service_id = t9.id) INNER JOIN mkt_campaign AS t10 ON (t2.mkt_campaign_id = t10.id) INNER JOIN data_services AS t11 ON (t1.service_id = t11.id) INNER JOIN data_score AS t12 ON (t1.score = t12.id) INNER JOIN users AS t13 ON (t1.created_by = t13.id) INNER JOIN users AS t14 ON (t1.finish_by = t14.id) INNER JOIN data_departament AS t15 ON (t1.created_dep_id= t15.id) INNER JOIN data_departament AS t16 ON (t1.finish_dep_id= t16.id) INNER JOIN data_mkt_forms AS t17 ON (t2.mkt_forms_id = t17.id) INNER JOIN data_mkt_conversation AS t18 ON (t2.mkt_conv_id= t18.id) AND t1.ticket = "'.$id.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;	
	
	}

	public static function InsertApproResp($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 'INSERT INTO approvals_data(ticket, cliente, nombre, naciemiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, tipo_servicio, score, transaccion, valor, fico, status_servicio, creador, c_departamento, creado, procesado_por, p_departamento, procesado) VALUES ("'.$info['ticket'].'","'.$info['cliente'].'","'.$info['nombre'].'","'.$info['nacimiento'].'","'.$info['edad'].'","'.$info['genero'].'","'.$info['telefono'].'","'.$info['proveedor'].'","'.$info['email'].'","'.$info['principal'].'","'.$info['postal'].'","'.$info['pais'].'","'.$info['pueblo'].'","'.$info['origen'].'","'.$info['medio'].'","'.$info['objectivo'].'","'.$info['post'].'","'.$info['service'].'","'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'","'.$info['tipo_servicio'].'","'.$info['score'].'","'.$info['transaccion'].'","'.$info['valor'].'","'.$info['fico'].'","'.$info['status_servicio'].'","'.$info['creador'].'","'.$info['c_departamento'].'","'.$info['created_at'].'","'.$info['finalizado'].'","'.$info['f_departamento'].'","'.$info['finish_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

///////////////// Request Macro Process Qualification ////////////////

	public static function ReqMacAprProScoInfTicket($ticket)
	{
		$query 	=	'SELECT ticket, (SELECT username FROM users WHERE id = finish_by) AS operator, finish_at FROM cp_appro_serv WHERE ticket  = "'.$ticket.'" ORDER BY id DESC LIMIT 1';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function 	ReqMacAprUpdProScoInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();
		
		$query 		=	'UPDATE request_macro SET status_aprobacion = "PROCESADO", procesado = "'.$iData['operator'].'", f_procesado = "'.$iData['finish_at'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

///////////////// Request Macro Copy Score //////////////////////////


/////////////////// Request Macro Process Cancel ////////////////////

	public static function ReqMacAprCanScoInfTicket($ticket)
	{
		$query 	=	'SELECT ticket, (SELECT username FROM users WHERE id = finish_by) AS operator, finish_at FROM cp_appro_serv WHERE ticket  = "'.$ticket.'" ORDER BY id DESC LIMIT 1';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacAprUpdCanScoInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();
		
		$query 		=	'UPDATE request_macro SET status_aprobacion = "CANCELADO", procesado = "'.$iData['operator'].'", f_procesado = "'.$iData['finish_at'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

//////////////////////// Respuesta Aprovaciones ////////////////////////

	public static function ApproRespInfoCancel($id)
	{

		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M","MASCULINO","FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, t3.name AS pais, t4.name AS pueblo, t5.name AS origen, t6.name AS medio, t7.name AS objectivo, t8.name AS post, t9.name AS service, t17.name as formulario, t18.name AS conversacion, t10.name AS campana, t11.name AS tipo_servicio, t1.transactions AS transaccion, t1.value AS valor, t1.fico AS fico, (CASE WHEN t1.status_id = "0" THEN "CANCELADO" WHEN t1.status_id = "1" THEN "PENDIENTE" WHEN t1.status_id = "2" THEN "PROCESADO" END) AS status_servicio, t13.username AS creador, t15.name AS c_departamento, t1.created_at, t14.username AS finalizado, t16.name as f_departamento, t1.finish_at FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN data_country AS t3 ON (t2.country_id = t3.id) INNER JOIN data_town AS t4 ON (t2.country_id = t4.id) INNER JOIN data_mkt_origen AS t5 ON (t2.mkt_origen_id = t5.id) INNER JOIN data_mkt_medium AS t6 ON (t2.mkt_medium_id = t6.id) INNER JOIN data_mkt_objetive AS t7 ON (t2.mkt_objective_id = t7.id) INNER JOIN data_mkt_conversation AS t8 ON (t2.mkt_post_id = t8.id) INNER JOIN data_mkt_service AS t9 ON (t2.mkt_service_id = t9.id) INNER JOIN mkt_campaign AS t10 ON (t2.mkt_campaign_id = t10.id) INNER JOIN data_services AS t11 ON (t1.service_id = t11.id) INNER JOIN users AS t13 ON (t1.created_by = t13.id) INNER JOIN users AS t14 ON (t1.finish_by = t14.id) INNER JOIN data_departament AS t15 ON (t1.created_dep_id= t15.id) INNER JOIN data_departament AS t16 ON (t1.finish_dep_id= t16.id) INNER JOIN data_mkt_forms AS t17 ON (t2.mkt_forms_id = t17.id) INNER JOIN data_mkt_conversation AS t18 ON (t2.mkt_conv_id= t18.id) AND t1.ticket = "'.$id.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertApproRespCancel($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();

		$query 		= 'INSERT INTO approvals_data(ticket, cliente, nombre, naciemiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, tipo_servicio, score, transaccion, valor, fico, status_servicio, creador, c_departamento, creado, procesado_por, p_departamento, procesado) VALUES ("'.$info['ticket'].'","'.$info['cliente'].'","'.$info['nombre'].'","'.$info['nacimiento'].'","'.$info['edad'].'","'.$info['genero'].'","'.$info['telefono'].'","'.$info['proveedor'].'","'.$info['email'].'","'.$info['principal'].'","'.$info['postal'].'","'.$info['pais'].'","'.$info['pueblo'].'","'.$info['origen'].'","'.$info['medio'].'","'.$info['objectivo'].'","'.$info['post'].'","'.$info['service'].'","'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'","'.$info['tipo_servicio'].'","","'.$info['transaccion'].'","'.$info['valor'].'","'.$info['fico'].'","'.$info['status_servicio'].'","'.$info['creador'].'","'.$info['c_departamento'].'","'.$info['created_at'].'","'.$info['finalizado'].'","'.$info['f_departamento'].'","'.$info['finish_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}
	
//////////////// Request Macro Save Qualification /////////////////

	public static function ReqMacAprPreScoInfTicket($ticket)
	{
		$query 	=	'SELECT ticket, (SELECT name FROM data_score WHERE id = score) AS score, transactions, value, fico, (SELECT username FROM users WHERE id = operator_id) AS operator, created_at FROM cp_appro_score_tmp WHERE ticket  = "'.$ticket.'" ORDER BY id DESC LIMIT 1';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacAprUpdPreScoInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

        $_replace 	= 	new Config();
		
		$query 		=	'UPDATE request_macro SET status_aprobacion = "PENDIENTE", score = "'.$iData['score'].'", transactions = "'.$iData['transactions'].'", fico = "'.$iData['fico'].'", procesado = "'.$iData['operator'].'", f_procesado = "'.$iData['created_at'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////////////////////////////////////////////////////////////////
//////////////////////// END Drives Approvals //////////////////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
////////////////////// Drives Coordinations ////////////////////////////
////////////////////////////////////////////////////////////////////////

/////////////////// Coordinations Contact /////////////////////////

	public static function GetUpdateContactCoordTicket($ticket)
	{
		$query 	=	'SELECT ticket, (SELECT IF(cont_id = "1", "SI", "NO") FROM cp_coord_contact_tmp WHERE id = contact_id) AS c_contacto, (SELECT name FROM data_coord_contact WHERE id = (SELECT reason_id FROM cp_coord_contact_tmp WHERE id = contact_id)) AS c_rason, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_contact_tmp WHERE id = contact_id)) AS c_ope, (SELECT created_at FROM cp_coord_contact_tmp WHERE id = contact_id) AS c_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function UpdateContactCoordTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE coordinations_data SET c_contacto = "'.$iData['c_contacto'].'", c_rason = "'.$iData['c_rason'].'", c_ope = "'.$iData['c_ope'].'", c_fecha = "'.$iData['c_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

//////////////// Request Macro Save Qualification /////////////////

	public static function ReqMacCorConInfTicket($ticket)
	{
		$query 	=	'SELECT ticket, (SELECT IF(cont_id = "1", "SI", "NO") FROM cp_coord_contact_tmp WHERE id = contact_id) AS c_contacto, (SELECT name FROM data_coord_contact WHERE id = (SELECT reason_id FROM cp_coord_contact_tmp WHERE id = contact_id)) AS c_rason, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_contact_tmp WHERE id = contact_id)) AS c_operador, (SELECT created_at FROM cp_coord_contact_tmp WHERE id = contact_id) AS c_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacCorUpdConInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE request_macro SET c_contacto = "'.$iData['c_contacto'].'", c_rason = "'.$iData['c_rason'].'", c_operador = "'.$iData['c_operador'].'", c_fecha = "'.$iData['c_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

///////////////// Coordinations Data Corrects //////////////////////
	
	public static function GetUpdateViewCoordTicket($ticket)
	{
		$query 	=	'SELECT ticket, (SELECT IF(resp_id = "1", "SI", "NO") FROM cp_coord_view_tmp WHERE id = view_id) AS c_datos_correctos, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_view_tmp WHERE id = view_id)) AS c_datos_ope, (SELECT created_at FROM cp_coord_view_tmp WHERE id = view_id) AS c_datos_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function UpdateViewCoordTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE coordinations_data SET c_datos_correctos = "'.$iData['c_datos_correctos'].'", c_datos_ope = "'.$iData['c_datos_ope'].'", c_datos_fecha = "'.$iData['c_datos_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

//////////////// Request Macro Data Correct ////////////////////////
	
	public static function ReqMacCorVieInfTicket($ticket)
	{
		$query 	=	'SELECT ticket, (SELECT IF(resp_id = "1", "SI", "NO") FROM cp_coord_view_tmp WHERE id = view_id) AS c_datos_correctos, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_view_tmp WHERE id = view_id)) AS c_datos_operador, (SELECT created_at FROM cp_coord_view_tmp WHERE id = view_id) AS c_datos_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacCorUpdVieInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE request_macro SET c_datos_correctos = "'.$iData['c_datos_correctos'].'", c_datos_operador = "'.$iData['c_datos_operador'].'", c_datos_fecha = "'.$iData['c_datos_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

///////////////// Coordinations Edit Pack //////////////////////

	public static function GetUpdatePackCoordTicket($ticket)
	{

		$query 	=	'SELECT ticket, (SELECT IF(edit_id <> "0", "SI", "NO")) AS c_pack_editado, (SELECT username FROM users WHERE id = (SELECT user_id FROM cp_coord_serv_edit WHERE id = edit_id) ) as c_pack_operador, (SELECT created_at FROM cp_coord_serv_edit WHERE id = edit_id) AS c_pack_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function UpdatePackCoordTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE coordinations_data SET c_pack_editado = "'.$iData['c_pack_editado'].'", c_pack_operador = "'.$iData['c_pack_operador'].'", c_pack_fecha = "'.$iData['c_pack_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

//////////////// Request Macro Edit Pack ////////////////////////

	public static function ReqMacCorPacInfTicket($ticket)
	{

		$query 	=	'SELECT ticket, (SELECT IF(edit_id <> "0", "SI", "NO")) AS c_pack_editado, (SELECT username FROM users WHERE id = (SELECT user_id FROM cp_coord_serv_edit WHERE id = edit_id) ) as c_pack_operador, (SELECT created_at FROM cp_coord_serv_edit WHERE id = edit_id) AS c_pack_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacCorUpdPacInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE request_macro SET c_pack_editado = "'.$iData['c_pack_editado'].'", c_pack_operador = "'.$iData['c_pack_operador'].'", c_pack_fecha = "'.$iData['c_pack_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

/////////////// Coordinations Pre Coordination ///////////////////
	
	public static function GetUpdatePreCoordTicket($ticket)
	{

		$query 	=	'SELECT ticket, (SELECT IF(coord_id = "1", "SI", "NO") FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_coord, (SELECT name FROM data_coord_pre_reason WHERE id = (SELECT reason_id FROM cp_coord_pre_tmp WHERE id = pre_id)) AS c_pre_rason, (SELECT pre_date FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_fecha, (SELECT pre_disp FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_disp, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_pre_tmp WHERE id = pre_id)) AS c_pre_ope, (SELECT created_at FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_fecha_coord FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function UpdatePreCoordTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE coordinations_data SET c_pre_coord = "'.$iData['c_pre_coord'].'", c_pre_rason = "'.$iData['c_pre_rason'].'", c_pre_fecha = "'.$iData['c_pre_fecha'].'", c_pre_disp = "'.$iData['c_pre_disp'].'", c_pre_ope = "'.$iData['c_pre_ope'].'", c_pre_fecha_coord = "'.$iData['c_pre_fecha_coord'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////// Request Macro Pre Coordination ////////////////////

	public static function ReqMacCorPreInfTicket($ticket)
	{

		$query 	=	'SELECT ticket, (SELECT IF(coord_id = "1", "SI", "NO") FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_coord, (SELECT name FROM data_coord_pre_reason WHERE id = (SELECT reason_id FROM cp_coord_pre_tmp WHERE id = pre_id)) AS c_pre_rason, (SELECT pre_date FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_fecha, (SELECT pre_disp FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_disponibilidad, (SELECT name FROM data_coord_sig WHERE id = (SELECT sig_time_id FROM cp_coord_pre_tmp WHERE id = pre_id)) AS c_pre_firma_estimado, (SELECT name FROM data_coord_sig_reason WHERE id = (SELECT sig_reason_id FROM cp_coord_pre_tmp WHERE id = pre_id)) AS c_pre_firma_rason, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_pre_tmp WHERE id = pre_id)) AS c_pre_operador, (SELECT created_at FROM cp_coord_pre_tmp WHERE id = pre_id) AS c_pre_fecha_coord FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacCorUpdPreInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE request_macro SET c_pre_coord = "'.$iData['c_pre_coord'].'", c_pre_rason = "'.$iData['c_pre_rason'].'", c_pre_fecha = "'.$iData['c_pre_fecha'].'", c_pre_disponibilidad = "'.$iData['c_pre_disponibilidad'].'", c_pre_firma_estimado = "'.$iData['c_pre_firma_estimado'].'",  c_pre_firma_rason = "'.$iData['c_pre_firma_rason'].'",  c_pre_operador = "'.$iData['c_pre_operador'].'", c_pre_fecha_coord = "'.$iData['c_pre_fecha_coord'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

/////////////// Coordinations PROCESS Coordination ///////////////////

	public static function GetUpdateCoordTicket($ticket)
	{

		$query 	=	'SELECT ticket, IF(coord_id <> "0", "SI", "NO") AS c_coord, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_proc_tmp WHERE id = coord_id)) AS c_coord_ope, (SELECT created_at FROM cp_coord_proc_tmp WHERE id = coord_id) AS c_coord_fecha_proc FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function UpdateCoordTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE coordinations_data SET c_coord = "'.$iData['c_coord'].'", c_coord_ope = "'.$iData['c_coord_ope'].'", c_coord_fecha_proc = "'.$iData['c_coord_fecha_proc'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////// Request Macro PROCESS Coordination ////////////////////

	public static function ReqMacCorCorInfTicket($ticket)
	{

		$query 	=	'SELECT ticket, IF(coord_id <> "0", "SI", "NO") AS c_coord, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_proc_tmp WHERE id = coord_id)) AS c_coord_operador, (SELECT created_at FROM cp_coord_proc_tmp WHERE id = coord_id) AS c_coord_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacCorUpdCorInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE request_macro SET c_coord = "'.$iData['c_coord'].'", c_coord_operador = "'.$iData['c_coord_operador'].'", c_coord_fecha = "'.$iData['c_coord_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

/////////////// Coordinations PROCESS Referred ///////////////////

	public static function GetUpdateReferredTicket($ticket)
	{

		$query 	=	'SELECT ticket, IF(coord_ref <> "0", "SI", "NO") AS c_ref, (SELECT username FROM users WHERE id = (SELECT resp_id FROM cp_coord_ref_tmp WHERE id = coord_ref)) AS c_ref_ope, (SELECT created_at FROM cp_coord_ref_tmp WHERE id = coord_ref) AS c_ref_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function UpdateReferredTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE coordinations_data SET c_ref = "'.$iData['c_ref'].'", c_ref_ope = "'.$iData['c_ref_ope'].'", c_ref_fecha = "'.$iData['c_ref_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////// Request Macro PROCESS Referred ////////////////////

	public static function ReqMacCorRefInfTicket($ticket)
	{

		$query 	=	'SELECT ticket, IF(coord_ref <> "0", "SI", "NO") AS c_ref, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_ref_tmp WHERE id = coord_ref)) AS c_ref_operador, (SELECT created_at FROM cp_coord_ref_tmp WHERE id = coord_ref) AS c_ref_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacCorUpdRefInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE request_macro SET c_ref = "'.$iData['c_ref'].'", c_ref_operador = "'.$iData['c_ref_operador'].'", c_ref_fecha = "'.$iData['c_ref_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

/////////////////// Coordinations Cancel  ////////////////////////
	
	public static function GetUpdateCancelTicket($ticket)
	{

		$query 	=	'SELECT ticket, IF(cancel <> "0", "SI", "NO") AS c_cancel, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_coord_cancel_tmp WHERE id = cancel)) AS c_cancel_ope, (SELECT name FROM data_cancel_coord_objection WHERE id = (SELECT reason_id FROM cp_coord_cancel_tmp WHERE id = cancel)) AS c_cancel_reason, (SELECT created_at FROM cp_coord_proc_tmp WHERE id = cancel) AS c_cancel_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function UpdateCancelTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE coordinations_data SET c_cancel = "'.$iData['c_cancel'].'", c_cancel_reason = "'.$iData['c_cancel_reason'].'", c_cancel_ope = "'.$iData['c_cancel_ope'].'", c_cancel_fecha = "'.$iData['c_cancel_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

////////////////// Request Macro Cancel ////////////////////////
	
	public static function ReqMacCorUpdInfTicket($ticket)
	{

		$query 	=	'SELECT ticket, IF(cancel <> "0", "SI", "NO") AS c_cancel, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_coord_cancel_tmp WHERE id = cancel)) AS c_cancel_operador, (SELECT name FROM data_cancel_coord_objection WHERE id = (SELECT reason_id FROM cp_coord_cancel_tmp WHERE id = cancel)) AS c_cancel_rason, (SELECT created_at FROM cp_coord_proc_tmp WHERE id = cancel) AS c_cancel_fecha FROM cp_coord_serv WHERE ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function ReqMacCorUpdUpdInfTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'UPDATE request_macro SET c_cancel = "'.$iData['c_cancel'].'", c_cancel_rason = "'.$iData['c_cancel_rason'].'", c_cancel_operador = "'.$iData['c_cancel_operador'].'", c_cancel_fecha = "'.$iData['c_cancel_fecha'].'" WHERE ticket = "'.$iData['ticket'].'"';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}


////////////////////////////////////////////////////////////////////////

	public static function GetInfoCoordTicket($ticket)
	{
		$query 	=	'SELECT t3.created_at AS creado, t1.ticket, t2.client_id AS cliente, t2.name AS nombre, t2.phone_main AS telefono, t2.phone_alt AS alterno, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_services WHERE id = t3.service_id) AS servicio, (SELECT username FROM users WHERE id = t3.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t3.created_dep_id) AS departamento, (CASE WHEN t3.service_id = "2" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_tv WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t3.service_id = "3" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_sec WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t3.service_id = "4" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t3.service_id = "6" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_vzla_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) ELSE "NINGUNO" END) AS tipo_orden, (CASE WHEN (t3.status_id = "0" AND t3.cancel = "0") THEN "CANCELADO" WHEN (t3.status_id = "1" AND t3.cancel = "1") THEN "PENDIENTE" WHEN (t3.status_id = "2" AND t3.cancel = "1") THEN "PROCESADO" ELSE "NINGUNO" END ) AS status_aprobacion, (SELECT name FROM data_score WHERE id = t3.score ) AS score, t3.transactions, t3.value, t3.fico, (SELECT username FROM users WHERE id = t3.finish_by) AS operador, t3.finish_at AS d_fin_apro, (SELECT IF(cont_id = "1", "SI", "NO") FROM cp_coord_contact_tmp WHERE id = t1.contact_id) AS c_contacto, (SELECT name FROM data_coord_contact WHERE id = (SELECT reason_id FROM cp_coord_contact_tmp WHERE id = t1.contact_id)) AS c_rason, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_contact_tmp WHERE id = t1.contact_id)) AS c_ope, (SELECT created_at FROM cp_coord_contact_tmp WHERE id = t1.contact_id) AS c_fecha, (SELECT IF(resp_id = "1", "SI", "NO") FROM cp_coord_view_tmp WHERE id = t1.view_id) AS c_datos_correctos, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_view_tmp WHERE id = t1.view_id)) AS c_datos_ope, (SELECT created_at FROM cp_coord_view_tmp WHERE id = t1.view_id) AS c_datos_fecha, (SELECT IF(t1.edit_id <> "0", "SI", "NO")) AS c_pack_editado, (SELECT username FROM users WHERE id = (SELECT user_id FROM cp_coord_serv_edit WHERE id = t1.edit_id)) AS c_pack_operador, (SELECT created_at FROM cp_coord_serv_edit WHERE id = t1.edit_id) AS c_pack_fecha, (SELECT IF(coord_id = "1", "SI", "NO") FROM cp_coord_pre_tmp WHERE id = t1.pre_id) AS c_pre_coord, (SELECT name FROM data_coord_pre_reason WHERE id = (SELECT reason_id FROM cp_coord_pre_tmp WHERE id = t1.pre_id)) AS c_pre_rason, (SELECT pre_date FROM cp_coord_pre_tmp WHERE id = t1.pre_id) AS c_pre_fecha, (SELECT pre_disp FROM cp_coord_pre_tmp WHERE id = t1.pre_id) AS c_pre_disp, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_pre_tmp WHERE id = t1.pre_id)) AS c_pre_ope, (SELECT created_at FROM cp_coord_pre_tmp WHERE id = t1.pre_id) AS c_pre_fecha_coord, IF(t1.coord_id <> "0", "SI", "NO") AS c_coord, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_proc_tmp WHERE id = t1.coord_id)) AS c_coord_ope, (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.coord_id) AS c_coord_fecha_proc, (SELECT IF(resp_id = "1", "SI", "NO") FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) AS c_ref, (SELECT username FROM users WHERE id = (SELECT created_by FROM cp_coord_ref_tmp WHERE id = t1.coord_ref)) AS c_ref_ope, (SELECT created_at FROM cp_coord_ref_tmp WHERE id = t1.coord_ref) AS c_ref_fecha, IF(t1.cancel <> "0", "SI", "NO") AS c_cancel, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_coord_cancel_tmp WHERE id = t1.cancel)) AS c_cancel_ope, (SELECT name FROM data_cancel_coord_objection WHERE id = (SELECT reason_id FROM cp_coord_cancel_tmp WHERE id = t1.cancel)) AS c_cancel_reason, (SELECT created_at FROM cp_coord_proc_tmp WHERE id = t1.cancel) AS c_cancel_fecha FROM cp_coord_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN cp_appro_serv AS t3 ON (t1.ticket = t3.ticket) AND t1.ticket = "'.$ticket.'"';
			
			$dep 	=	DBSmart::DBQuery($query);

			return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertInfoCoordTicket($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();
        $date 		= 	date('Y-m-d H:i:s', time());

		$query 		=	'INSERT INTO coordinations_data_copy(creado, ticket, cliente, nombre, telefono, alterno, pais, pueblo, servicio, vendedor, departamento, tipo_orden, status_aprobacion, score, transactions, value, fico, operador, d_fin_apro, c_contacto, c_rason, c_ope, c_fecha, c_datos_correctos, c_datos_ope, c_datos_fecha, c_pack_editado, c_pack_operador, c_pack_fecha, c_pre_coord, c_pre_rason, c_pre_fecha, c_pre_disp, c_pre_ope, c_pre_fecha_coord, c_coord, c_coord_ope, c_coord_fecha_proc, c_cancel, c_cancel_ope, c_cancel_reason, c_cancel_fecha, c_ref, c_ref_ope, c_ref_fecha, created_at) VALUES ("'.$iData['creado'].'", "'.$iData['ticket'].'", "'.$iData['cliente'].'", "'.$iData['nombre'].'", "'.$iData['telefono'].'", "'.$iData['alterno'].'", "'.$iData['pais'].'", "'.$iData['pueblo'].'", "'.$iData['servicio'].'", "'.$iData['vendedor'].'", "'.$iData['departamento'].'", "'.$iData['tipo_orden'].'", "'.$iData['status_aprobacion'].'", "'.$iData['score'].'", "'.$iData['transactions'].'", "'.$iData['value'].'", "'.$iData['fico'].'", "'.$iData['operador'].'", "'.$iData['d_fin_apro'].'", "'.$iData['c_contacto'].'", "'.$iData['c_rason'].'", "'.$iData['c_ope'].'", "'.$iData['c_fecha'].'", "'.$iData['c_datos_correctos'].'", "'.$iData['c_datos_ope'].'", "'.$iData['c_datos_fecha'].'", "'.$iData['c_pack_editado'].'", "'.$iData['c_pack_operador'].'", "'.$iData['c_pack_fecha'].'", "'.$iData['c_pre_coord'].'", "'.$iData['c_pre_rason'].'", "'.$iData['c_pre_fecha'].'", "'.$iData['c_pre_disp'].'", "'.$iData['c_pre_ope'].'", "'.$iData['c_pre_fecha_coord'].'", "'.$iData['c_coord'].'", "'.$iData['c_coord_ope'].'", "'.$iData['c_coord_fecha_proc'].'", "'.$iData['c_cancel'].'", "'.$iData['c_cancel_ope'].'", "'.$iData['c_cancel_reason'].'", "'.$iData['c_cancel_fecha'].'", "'.$iData['c_ref'].'", "'.$iData['c_ref_ope'].'", "'.$iData['c_ref_fecha'].'", "'.$date.'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;

	}

	public static function GetParcialInfoAprovalsTicket2($ticket)
	{
		$query 	=	'SELECT t1.created_at AS creado, t1.ticket, t1.client_id AS cliente, t2.name AS nombre, t2.phone_main AS telefono, t2.phone_alt AS alterno, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_services WHERE id = t1.service_id) AS servicio, (SELECT username FROM users WHERE id = t1.created_by) AS vendedor, (SELECT name FROM data_departament WHERE id = t1.created_dep_id) AS departamento, (CASE WHEN t1.service_id = "2" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_tv WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "3" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_sec WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) WHEN t1.service_id = "4" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_pr_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1))	WHEN t1.service_id = "6" THEN (SELECT name FROM data_type_order WHERE id = (SELECT type_order_id AS type FROM cp_service_vzla_int WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) ELSE "NINGUNO" END) AS tipo_orden, (CASE WHEN (t1.status_id = "0" AND t1.cancel = "0") THEN "CANCELADO" WHEN (t1.status_id = "1" AND t1.cancel = "1") THEN "PENDIENTE" WHEN (t1.status_id = "2" AND t1.cancel = "1") THEN "PROCESADO"	ELSE "NINGUNO" END) AS status_aprobacion, (SELECT name FROM data_score WHERE id = t1.score) AS score, t1.transactions, t1.value, t1.fico, (SELECT username FROM users WHERE id = t1.finish_by) AS operador, (SELECT name FROM data_departament WHERE id = t1.finish_dep_id) AS departamento_ope, t1.finish_at AS d_fin_apro FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertParcialInfoAprovalsTicket2($iData)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();
        $date 		= 	date('Y-m-d H:i:s', time());

		$query 	=	'INSERT INTO coordinations_data_copy(creado, ticket, cliente, nombre, telefono, alterno, pais, pueblo, servicio, vendedor, departamento, tipo_orden, status_aprobacion, score, transactions, value, fico, operador, d_fin_apro, c_contacto, c_rason, c_datos_correctos, c_pack_editado, c_pack_operador, c_pack_fecha, c_pre_coord, c_pre_rason, c_pre_fecha, c_pre_disp, c_pre_ope, c_pre_fecha_coord, c_coord, c_coord_ope, c_coord_fecha_proc, c_cancel, c_cancel_ope, c_cancel_fecha, c_ref, c_ref_ope, c_ref_fecha, created_at) VALUES ("'.$iData['creado'].'", "'.$iData['ticket'].'", "'.$iData['cliente'].'", "'.$iData['nombre'].'", "'.$iData['telefono'].'", "'.$iData['alterno'].'", "'.$iData['pais'].'", "'.$iData['pueblo'].'", "'.$iData['servicio'].'", "'.$iData['vendedor'].'", "'.$iData['departamento'].'", "'.$iData['tipo_orden'].'", "'.$iData['status_aprobacion'].'", "'.$iData['score'].'", "'.$iData['transactions'].'", "'.$iData['value'].'", "'.$iData['fico'].'", "'.$iData['operador'].'", "'.$iData['d_fin_apro'].'","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "'.$date.'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function RequestTVInfoNew($ticket)
	{
		
		$query = 'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M","MASCULINO","FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_mkt_origen WHERE id = t2.mkt_origen_id) AS origen, (SELECT name FROM data_mkt_medium WHERE id = t2.mkt_medium_id) AS medio, (SELECT name FROM data_mkt_objetive WHERE id = t2.mkt_objective_id) AS objective, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_post_id) AS post, (SELECT name FROM data_mkt_service WHERE id = t2.mkt_service_id) AS service, (SELECT name FROM data_mkt_forms WHERE id = t2.mkt_forms_id) AS formulario, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_conv_id) AS conversacion, (SELECT name FROM mkt_campaign WHERE id = t2.mkt_campaign_id) AS campana, t1.decoder_id AS decodificador, t1.dvr_id AS dvr, (SELECT name FROM data_package WHERE id = t1.package_id) AS paquete, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS metodo_pago, (SELECT IF(t1.ref_id = "1","SI","NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT IF(t1.sup_id = "1","SI","NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS status_servicio, (SELECT IF(t1.advertising_id= "1","SI","NO")) AS advertising, (SELECT IF(t1.ch_hd = "on","SI","NO")) AS hd, (SELECT IF(t1.ch_dvr = "on","SI","NO")) AS dvr_add, (SELECT IF(t1.ch_hbo = "on","SI","NO")) AS hbo, (SELECT IF(t1.ch_cinemax = "on","SI","NO")) AS cinemax, (SELECT IF(t1.ch_starz = "on","SI","NO")) AS starz, (SELECT IF(t1.ch_showtime = "on","SI","NO")) AS showtime, (SELECT IF(t1.gif_ent = "on","SI","NO")) AS gift_ent, (SELECT IF(t1.gif_choice = "on","SI","NO")) AS gift_choice, (SELECT IF(t1.gif_xtra = "on","SI","NO")) AS gift_xtra, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_tv WHERE ticket = t1.ticket )) AS propietario, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_tv WHERE ticket = t1.ticket ))) AS p_departamento, t1.created_at, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS tipo_orden, t1.tv, (SELECT name FROM data_provider WHERE id = t1.provider_id) AS proveedor_ant FROM cp_service_pr_tv AS t1  INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;

	}

	public static function InsertDBTVNew($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		= 'INSERT INTO new_request_television(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, tipo_orden, tv, decodificador, dvr, paquete, proveedor_ant, metodo_pago, referido, referido_nombre, apoyo, apoyo_usuario, status_servicio, advertising, hd, dvr_add, hbo, cinemax, starz, showtime, gift_ent, gift_cho, gift_xtra, propietario, p_departamento, assistente, a_departament, created_at) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'",  "'.$info['nacimiento'].'",  "'.$info['edad'].'",  "'.$info['genero'].'",  "'.$info['telefono'].'",  "'.$info['proveedor'].'",  "'.$info['email'].'",  "'.$info['principal'].'",  "'.$info['postal'].'",  "'.$info['pais'].'",  "'.$info['pueblo'].'",  "'.$info['origen'].'",  "'.$info['medio'].'",  "'.$info['objective'].'",  "'.$info['post'].'",  "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'",  "'.$info['tipo_orden'].'",  "'.$info['tv'].'",  "'.$info['decodificador'].'",  "'.$info['dvr'].'",  "'.$info['paquete'].'",  "'.$info['proveedor_ant'].'",  "'.$info['metodo_pago'].'",  "'.$info['referido'].'",  "'.$info['ref_nombre'].'",  "'.$info['apoyo'].'",  "'.$info['apoyo_usuario'].'",  "'.$info['status_servicio'].'",  "'.$info['advertising'].'",  "'.$info['hd'].'",  "'.$info['dvr_add'].'",  "'.$info['hbo'].'",  "'.$info['cinemax'].'",  "'.$info['starz'].'",  "'.$info['showtime'].'",  "'.$info['gift_ent'].'",  "'.$info['gift_choice'].'",  "'.$info['gift_xtra'].'",  "'.$info['propietario'].'",  "'.$info['p_departamento'].'",  "'.$info['propietario'].'",  "'.$info['p_departamento'].'",  "'.$info['created_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;

	}

	public static function RequestSECInfoNew($ticket)
	{
		$query = 'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M","MASCULINO","FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_mkt_origen WHERE id = t2.mkt_origen_id) AS origen, (SELECT name FROM data_mkt_medium WHERE id = t2.mkt_medium_id) AS medio, (SELECT name FROM data_mkt_objetive WHERE id = t2.mkt_objective_id) AS objective, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_post_id) AS post, (SELECT name FROM data_mkt_service WHERE id = t2.mkt_service_id) AS service, (SELECT name FROM data_mkt_forms WHERE id = t2.mkt_forms_id) AS formulario, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_conv_id) AS conversacion, (SELECT name FROM mkt_campaign WHERE id = t2.mkt_campaign_id) AS campana, (SELECT IF(t1.previously_id= 0, "NO", "SI")) AS sist_previo, t1.equipment AS desc_equipos, (SELECT name FROM data_camera WHERE id = t1.cameras_id) AS camaras, t1.comp_equipment AS emp_monitoreo, t1.equipment AS desc_equipos, (SELECT name FROM data_dvr WHERE id = t1.dvr_id) AS dvr, t1.add_equipment AS equip_adicional, t1.password_alarm AS password, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS metodo_pago, (SELECT IF(t1.ref_id = 1, "SI", "NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT IF(t1.sup_id = 1, "SI", "NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, (SELECT name FROM data_type_order WHERE id = t1.service_id) AS tipo_orden, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS status_servicio, t1.contact_1 AS contacto1, t1.contact_1_desc AS contacto1desc, t1.contact_2 AS contacto2, t1.contact_2_desc AS contacto2desc, t1.contact_3 AS contacto3, t1.contact_3_desc AS contacto3desc, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_sec WHERE ticket = t1.ticket )) AS propietario, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_sec WHERE ticket = t1.ticket ))) AS p_departamento, t1.created_at FROM cp_service_pr_sec AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertDBSECNew($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		= 	'INSERT INTO new_request_security(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, sist_previo, desc_equipos, camaras, emp_monitoreo, dvr, equip_adicional, password, metodo_pago, referido, referido_nombre, apoyo, apoyo_usuario, tipo_orden, status_servicio, contacto1, contacto1desc, contacto2, contacto2desc, contacto3, contacto3desc, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'","'.$info['cliente'].'","'.$info['nombre'].'","'.$info['nacimiento'].'","'.$info['edad'].'","'.$info['genero'].'","'.$info['telefono'].'","'.$info['proveedor'].'","'.$info['email'].'","'.$info['principal'].'","'.$info['postal'].'","'.$info['pais'].'","'.$info['pueblo'].'","'.$info['origen'].'","'.$info['medio'].'","'.$info['objective'].'","'.$info['post'].'","'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['sist_previo'].'","'.$info['desc_equipos'].'","'.$info['camaras'].'","'.$info['emp_monitoreo'].'","'.$info['dvr'].'","'.$info['equip_adicional'].'","'.$info['password'].'","'.$info['metodo_pago'].'","'.$info['referido'].'","'.$info['ref_nombre'].'","'.$info['apoyo'].'","'.$info['apoyo_usuario'].'","'.$info['tipo_orden'].'","'.$info['status_servicio'].'","'.$info['contacto1'].'","'.$info['contacto1desc'].'","'.$info['contacto2'].'","'.$info['contacto2desc'].'","'.$info['contacto3'].'","'.$info['contacto3desc'].'","'.$info['propietario'].'","'.$info['p_departamento'].'","'.$info['propietario'].'","'.$info['p_departamento'].'","'.$info['created_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

	public static function RequestINTInfoNew($ticket)
	{
		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M","MASCULINO","FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_mkt_origen WHERE id = t2.mkt_origen_id) AS origen, (SELECT name FROM data_mkt_medium WHERE id = t2.mkt_medium_id) AS medio, (SELECT name FROM data_mkt_objetive WHERE id = t2.mkt_objective_id) AS objective, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_post_id) AS post, (SELECT name FROM data_mkt_service WHERE id = t2.mkt_service_id) AS service, (SELECT name FROM data_mkt_forms WHERE id = t2.mkt_forms_id) AS formulario, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_conv_id) AS conversacion, (SELECT name FROM mkt_campaign WHERE id = t2.mkt_campaign_id) AS campana, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT IF(t1.ref_id = "1","SI","NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_cliente, (SELECT IF(t1.sup_id = "1","SI","NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS metodo_pago, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS tipo_orden, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS status_servicio, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_int WHERE ticket = t1.ticket )) AS propietario, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_int WHERE ticket = t1.ticket ))) AS p_departamento, t1.created_at FROM cp_service_pr_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertDBINTNew($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		= 'INSERT INTO new_request_internet(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, internet_res, telefono_res, internet_com, telefono_com, referido, referido_nombre, referido_cliente, apoyo, apoyo_usuario, medoto_pago, tipo_orden, status_servicio, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['nacimiento'].'", "'.$info['edad'].'", "'.$info['genero'].'", "'.$info['telefono'].'", "'.$info['proveedor'].'", "'.$info['email'].'", "'.$info['principal'].'", "'.$info['postal'].'", "'.$info['pais'].'", "'.$info['pueblo'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objective'].'", "'.$info['post'].'", "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['referido'].'", "'.$info['ref_nombre'].'", "'.$info['ref_cliente'].'", "'.$info['apoyo'].'", "'.$info['apoyo_usuario'].'", "'.$info['metodo_pago'].'", "'.$info['tipo_orden'].'", "'.$info['status_servicio'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['created_at'].'") ';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

	public static function RequestIntVZInfoNew($ticket)
	{
		$query = 'SELECT t1.ticket, t2.client_id AS cliente, t2.name AS nombre, t2.birthday  AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M", "MASCULINO", "FEMENINO"))AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_mkt_origen WHERE id = t2.mkt_origen_id) AS origen, (SELECT name FROM data_mkt_medium WHERE id = t2.mkt_medium_id) AS medio, (SELECT name FROM data_mkt_objetive WHERE id = t2.mkt_objective_id) AS objective, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_post_id) AS post, (SELECT name FROM data_mkt_service WHERE id = t2.mkt_service_id) AS service, (SELECT name FROM data_mkt_forms WHERE id = t2.mkt_forms_id) AS formulario, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_conv_id) AS conversacion, (SELECT name FROM mkt_campaign WHERE id = t2.mkt_campaign_id) AS campana, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com,(SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS metodo_pago, (SELECT name FROM data_bank WHERE id = t1.bank_id) AS banco, t1.amount AS monto, t1.transference As transferencia, t1.email AS email_servicio, t1.trans_date AS fecha_pago, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS tipo_orden, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS status_servicio, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_service_vzla_int WHERE ticket = t1.ticket )) AS propietario, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM cp_service_vzla_int WHERE ticket = t1.ticket ))) AS p_departamento, t1.created_at, t2.additional FROM cp_service_vzla_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function InsertDBIntVZNew($info)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		= 'INSERT INTO new_request_internet_vzla(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, internet_res, telefono_res, internet_com, telefono_com, medoto_pago, banco, monto, transferencia, email_servicio, fecha_pago, tipo_orden, status_servicio, propietario, p_departamento, asistente, a_departamento, created_at, additional) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['nacimiento'].'", "'.$info['edad'].'", "'.$info['genero'].'", "'.$info['telefono'].'", "'.$info['proveedor'].'", "'.$info['email'].'", "'.$info['principal'].'", "'.$info['postal'].'", "'.$info['pais'].'", "'.$info['pueblo'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objective'].'", "'.$info['post'].'", "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['metodo_pago'].'", "'.$info['banco'].'", "'.$info['monto'].'", "'.$info['transferencia'].'", "'.$info['email_servicio'].'", "'.$info['fecha_pago'].'", "'.$info['tipo_orden'].'", "'.$info['status_servicio'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['created_at'].'", "'.$info['additional'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;

	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////



	public static function CopyRequestINTInfoNew($ticket)
	{
		$query 	=	'SELECT t1.ticket, t2.client_id AS cliente, t2.name as nombre, t2.birthday AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M","MASCULINO","FEMENINO")) AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_mkt_origen WHERE id = t2.mkt_origen_id) AS origen, (SELECT name FROM data_mkt_medium WHERE id = t2.mkt_medium_id) AS medio, (SELECT name FROM data_mkt_objetive WHERE id = t2.mkt_objective_id) AS objective, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_post_id) AS post, (SELECT name FROM data_mkt_service WHERE id = t2.mkt_service_id) AS service, (SELECT name FROM data_mkt_forms WHERE id = t2.mkt_forms_id) AS formulario, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_conv_id) AS conversacion, (SELECT name FROM mkt_campaign WHERE id = t2.mkt_campaign_id) AS campana, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com, (SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT IF(t1.ref_id = "1","SI","NO")) AS referido, (SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_nombre, (SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_cliente, (SELECT IF(t1.sup_id = "1","SI","NO")) AS apoyo, (SELECT username FROM users WHERE id = t1.sup_ope_id) AS apoyo_usuario, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS metodo_pago, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS tipo_orden, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS status_servicio, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_int WHERE ticket = t1.ticket )) AS propietario, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM cp_service_pr_int WHERE ticket = t1.ticket ))) AS p_departamento, t1.created_at FROM cp_service_pr_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function CopyInsertDBINTNew($info)
	{

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		= 'INSERT INTO copy_new_request_internet(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, internet_res, telefono_res, internet_com, telefono_com, referido, referido_nombre, referido_cliente, apoyo, apoyo_usuario, medoto_pago, tipo_orden, status_servicio, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['nacimiento'].'", "'.$info['edad'].'", "'.$info['genero'].'", "'.$info['telefono'].'", "'.$info['proveedor'].'", "'.$info['email'].'", "'.$info['principal'].'", "'.$info['postal'].'", "'.$info['pais'].'", "'.$info['pueblo'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objective'].'", "'.$info['post'].'", "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['referido'].'", "'.$info['ref_nombre'].'", "'.$info['ref_cliente'].'", "'.$info['apoyo'].'", "'.$info['apoyo_usuario'].'", "'.$info['metodo_pago'].'", "'.$info['tipo_orden'].'", "'.$info['status_servicio'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['created_at'].'") ';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;
	
	}

	public static function CopyRequestIntVZInfoNew($ticket)
	{
		$query = 'SELECT t1.ticket, t2.client_id AS cliente, t2.name AS nombre, t2.birthday  AS nacimiento, t2.age AS edad, (SELECT IF(t2.gender = "M", "MASCULINO", "FEMENINO"))AS genero, t2.phone_main AS telefono, (SELECT name FROM data_phone_provider WHERE id = t2.phone_main_provider) AS proveedor, t2.email_main as email, t2.add_main AS principal, t2.add_postal AS postal, (SELECT name FROM data_country WHERE id = t2.country_id) AS pais, (SELECT name FROM data_town WHERE id = t2.town_id) AS pueblo, (SELECT name FROM data_mkt_origen WHERE id = t2.mkt_origen_id) AS origen, (SELECT name FROM data_mkt_medium WHERE id = t2.mkt_medium_id) AS medio, (SELECT name FROM data_mkt_objetive WHERE id = t2.mkt_objective_id) AS objective, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_post_id) AS post, (SELECT name FROM data_mkt_service WHERE id = t2.mkt_service_id) AS service, (SELECT name FROM data_mkt_forms WHERE id = t2.mkt_forms_id) AS formulario, (SELECT name FROM data_mkt_conversation WHERE id = t2.mkt_conv_id) AS conversacion, (SELECT name FROM mkt_campaign WHERE id = t2.mkt_campaign_id) AS campana, (SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res, (SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res, (SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com,(SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com, (SELECT name FROM data_payment WHERE id = t1.payment_id) AS metodo_pago, (SELECT name FROM data_bank WHERE id = t1.bank_id) AS banco, t1.amount AS monto, t1.transference As transferencia, t1.email AS email_servicio, t1.trans_date AS fecha_pago, (SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS tipo_orden, (SELECT name FROM data_status_service WHERE id = t1.service_id) AS status_servicio, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_service_vzla_int WHERE ticket = t1.ticket )) AS propietario, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM cp_service_vzla_int WHERE ticket = t1.ticket ))) AS p_departamento, t1.created_at FROM cp_service_vzla_int AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$ticket.'"';

		$dep 	=	DBSmart::DBQuery($query);

		return ($dep <> false ) ? $dep : false;
	
	}

	public static function CopyInsertDBIntVZNew($info)
	{
		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		= 'INSERT INTO copy_new_request_internet_vzla(ticket, cliente, nombre, nacimiento, edad, genero, telefono, proveedor, email, principal, postal, pais, pueblo, origen, medio, objectivo, post, servicio, formulario, conversacion, campana, internet_res, telefono_res, internet_com, telefono_com, medoto_pago, banco, monto, transferencia, email_servicio, fecha_pago, tipo_orden, status_servicio, propietario, p_departamento, asistente, a_departamento, created_at) VALUES ("'.$info['ticket'].'", "'.$info['cliente'].'", "'.$info['nombre'].'", "'.$info['nacimiento'].'", "'.$info['edad'].'", "'.$info['genero'].'", "'.$info['telefono'].'", "'.$info['proveedor'].'", "'.$info['email'].'", "'.$info['principal'].'", "'.$info['postal'].'", "'.$info['pais'].'", "'.$info['pueblo'].'", "'.$info['origen'].'", "'.$info['medio'].'", "'.$info['objective'].'", "'.$info['post'].'", "'.$info['service'].'", "'.$info['formulario'].'", "'.$info['conversacion'].'", "'.$info['campana'].'", "'.$info['int_res'].'", "'.$info['pho_res'].'", "'.$info['int_com'].'", "'.$info['pho_com'].'", "'.$info['metodo_pago'].'", "'.$info['banco'].'", "'.$info['monto'].'", "'.$info['transferencia'].'", "'.$info['email_servicio'].'", "'.$info['fecha_pago'].'", "'.$info['tipo_orden'].'", "'.$info['status_servicio'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['propietario'].'", "'.$info['p_departamento'].'", "'.$info['created_at'].'")';

		$statement  =   $connect->prepare($query);

		return ($statement->execute()) ? true : false;

	}


}
