<?php
use Symfony\Component\HttpFoundation\Response;

//Request::setTrustedProxies(array('127.0.0.1'));

/*
|------------------------------------------------------------------------------
| Rutas de la aplicacion
|------------------------------------------------------------------------------
| aqui puedes definir rutas para la funcionalidad principal de la aplicacion
| por ahora, solo lleva hacia la pantalla principal
|
*/

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', array(
        'sidebar'   =>  True
    ));
})
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('homepage');

$app->get('/hometest', function () use ($app) {
    return $app['twig']->render('test.html.twig', array(
        'sidebar'   =>  True
    ));
})
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('hometest');

$app->get('/oldclosures', function () use ($app) {
    return $app['twig']->render('landing/closures.html.twig');
})->bind('/oldclosures');

$app->get('/closures', function () use ($app) {
    return $app['twig']->render('landing/new.html.twig');
})->bind('/closures');


/*
|------------------------------------------------------------------------------
| Rutas de Home Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/dashboard','Home.controller:index')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('dashboard');

/*
|------------------------------------------------------------------------------
| Rutas de ISP Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/isp','ISP.controller:index')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp')
    ->method('GET|POST');

$app->post  ('/isp/getinfo','ISP.controller:GetInfo')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/getinfo')
    ->method('GET|POST');

$app->post  ('/isp/getinfoid','ISP.controller:GetInfoId')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/getinfoid')
    ->method('GET|POST');

$app->post  ('/isp/ViewInfoId','ISP.controller:ViewInfoId')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/ViewInfoId')
    ->method('GET|POST');

$app->post  ('/isp/TmpFiles','ISP.controller:TmpFiles')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/TmpFiles')
    ->method('GET|POST');

$app->post  ('/isp/SaveService','ISP.controller:SaveService')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/SaveService')
    ->method('GET|POST');

$app->post  ('/isp/load','ISP.controller:Load')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/load')
    ->method('GET|POST');

$app->post  ('/isp/Notes','ISP.controller:Notes')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Notes')
    ->method('GET|POST');

$app->post  ('/isp/GetEditInfo','ISP.controller:GetEditInfo')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/GetEditInfo')
    ->method('GET|POST');

$app->post  ('/isp/UpdateServices','ISP.controller:UpdateServices')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/UpdateServices')
    ->method('GET|POST');
    
$app->post  ('/isp/ProcessService','ISP.controller:ProcessService')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/ProcessService')
    ->method('GET|POST');

$app->post  ('/isp/OpenService','ISP.controller:OpenService')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/OpenService')
    ->method('GET|POST');

$app->post  ('/isp/EmailService','ISP.controller:EmailService')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/EmailService')
    ->method('GET|POST');

$app->post  ('/isp/DeteleAdj','ISP.controller:DeteleAdj')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/DeteleAdj')
    ->method('GET|POST');

$app->post  ('/isp/DeteleTmpAdj','ISP.controller:DeteleTmpAdj')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/DeteleTmpAdj')
    ->method('GET|POST');

$app->post  ('/isp/LoadByDate','ISP.controller:LoadByDate')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/LoadByDate')
    ->method('GET|POST');

 $app->post  ('/isp/Reports','ISP.controller:Reports')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Reports')
    ->method('GET|POST'); 

$app->get  ('/isp/dashboard','ISP.controller:Dashboard')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/dashboard')
    ->method('GET|POST');

$app->POST  ('/isp/dashboard/load','ISP.controller:DashboardLoad')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/dashboard/load')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/NewCategories','ISP.controller:NewCategories')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/NewCategories')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/NewLables','ISP.controller:NewLables')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/NewLables')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/NewEmails','ISP.controller:NewEmails')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/NewEmails')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/ShowCategories','ISP.controller:ShowCategories')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/ShowCategories')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/ShowLables','ISP.controller:ShowLables')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/ShowLables')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/ShowEmails','ISP.controller:ShowEmails')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/ShowEmails')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/UpdateCategories','ISP.controller:UpdateCategories')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/UpdateCategories')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/UpdateLables','ISP.controller:UpdateLables')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/UpdateLables')
    ->method('GET|POST');

$app->POST  ('/isp/Dashboard/UpdateEmails','ISP.controller:UpdateEmails')
    ->before($app['middleware.ISPServices'])
    ->bind('/isp/Dashboard/UpdateEmails')
    ->method('GET|POST');


/*
|------------------------------------------------------------------------------
| Rutas de Home Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/Ticket','Ticket.controller:Index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket')
    ->method('GET|POST');

$app->match('/Ticket/Load','Ticket.controller:Load')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Load')
    ->method('GET|POST');

$app->match('/Ticket/Modal','Ticket.controller:ModalTicket')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Modal')
    ->method('GET|POST');

$app->match('/Ticket/Service','Ticket.controller:Services')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Service')
    ->method('GET|POST');

$app->match('/tickets/getservice','Ticket.controller:GetServices')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/getService')
    ->method('GET|POST');

$app->match('/Ticket/SubmitTicket','Ticket.controller:SubmitTicket')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/SubmitTicket')
    ->method('GET|POST');

$app->match('/tickets/submitticketsup','Ticket.controller:SubmitTicketSup')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/submitticketsup')
    ->method('GET|POST');

$app->match('/tickets/submitticketpay','Ticket.controller:SubmitTicketPay')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/submitticketpay')
    ->method('GET|POST');

$app->match('/tickets/submitticketdat','Ticket.controller:SubmitTicketDat')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/submitticketdat')
    ->method('GET|POST');

$app->match('/Ticket/View','Ticket.controller:ViewTicket')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/View')
    ->method('GET|POST');

$app->match('/Ticket/Change','Ticket.controller:ChangeTicket')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Change')
    ->method('GET|POST');

$app->match('/Ticket/Close','Ticket.controller:CloseTicket')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Close')
    ->method('GET|POST');

$app->match('/Ticket/Process','Ticket.controller:ProcessTicket')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Process')
    ->method('GET|POST');

$app->match('/Ticket/Config','Ticket.controller:Config')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Config')
    ->method('GET|POST');

$app->match('/Ticket/Edit/Departament','Ticket.controller:DepartamentEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Edit/Departament')
    ->method('GET|POST');

$app->match('/Ticket/Edit/SaveDepartament','Ticket.controller:DepartamentSave')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Edit/SaveDepartament')
    ->method('GET|POST');

$app->match('/Ticket/Edit/Service','Ticket.controller:ServiceEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Edit/Service')
    ->method('GET|POST');

$app->match('/Ticket/Edit/SaveService','Ticket.controller:ServiceSave')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/Edit/SaveService')
    ->method('GET|POST');

$app->match('/Ticket/SearchDate','Ticket.controller:SearchDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Ticket/SearchDate')
    ->method('GET|POST');


$app->match('/tickets/admins','Ticket.controller:Admins')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/admins')
    ->method('GET|POST');

$app->match('/tickets/admins/load','Ticket.controller:AdminsLoad')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/admins/load')
    ->method('GET|POST');

$app->match('/tickets/admins/SearchDate','Ticket.controller:AdminsDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/admins/SearchDate')
    ->method('GET|POST');

$app->match('/tickets/admins/info','Ticket.controller:AdminsInfo')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/admins/info')
    ->method('GET|POST');

$app->match('/tickets/admins/savenotes','Ticket.controller:AdminsSaveNotes')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/admins/savenotes')
    ->method('GET|POST');

$app->match('/tickets/admins/commands','Ticket.controller:AdminsCommands')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/admins/commands')
    ->method('GET|POST');

$app->match('/tickets/dashboard','Ticket.controller:Dashboard')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/dashboard')
    ->method('GET|POST');

$app->match('/tickets/dashboard/load','Ticket.controller:DashboardLoad')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/dashboard/load')
    ->method('GET|POST');

$app->match('/tickets/dashboard/commands','Ticket.controller:DashboardCommands')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/dashboard/commands')
    ->method('GET|POST');

$app->match('/tickets/support','Ticket.controller:DashboardSup')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/support')
    ->method('GET|POST');

$app->match('/tickets/support/info','Ticket.controller:GeneralInfo')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/support/info')
    ->method('GET|POST');

$app->match('/tickets/support/load','Ticket.controller:DashboardLoadSup')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/support/load')
    ->method('GET|POST');

$app->match('/tickets/support/searchdate','Ticket.controller:DashboardSearcDateSup')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/support/searchdate')
    ->method('GET|POST');

$app->match('/tickets/support/commands','Ticket.controller:DashboardCommandsSup')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/support/commands')
    ->method('GET|POST');

$app->match('/tickets/payment','Ticket.controller:DashboardPay')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/payment')
    ->method('GET|POST');

$app->match('/tickets/payment/info','Ticket.controller:GeneralInfo')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/payment/info')
    ->method('GET|POST');


$app->match('/tickets/payment/load','Ticket.controller:DashboardLoadPay')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/payment/load')
    ->method('GET|POST');

$app->match('/tickets/payment/searchdate','Ticket.controller:DashboardSearcDatePay')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/payment/searchdate')
    ->method('GET|POST');

$app->match('/tickets/payment/commands','Ticket.controller:DashboardCommandsPay')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/tickets/payment/commands')
    ->method('GET|POST');


$app->match('/b24','B24.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/b24')
    ->method('GET|POST');
$app->match('/b24/search','B24.controller:search')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/b24/search')
    ->method('GET|POST');

$app->match('/b24/process','B24.controller:process')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/b24/process')
    ->method('GET|POST');





/*
|------------------------------------------------------------------------------
| Rutas de Leads Controller
|------------------------------------------------------------------------------
|
*/

/////////////////////////////////////////////////////////////////////////////////
    
$app->get  ('/Leads/New','Leads.controller:LeadsNew')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/New');

$app->post  ('/Leads/NewSearch','Leads.controller:NewSearch')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/NewSearch');

$app->post  ('/Leads/NewCreate','Leads.controller:NewCreate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/NewCreate');

$app->post  ('/Leads/CreateLeads','Leads.controller:CreateLeads')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/CreateLeads');

$app->post  ('/Leads/CreateLeadNew','Leads.controller:CreateLeadNew')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/CreateLeadNew');    

$app->post  ('/Leads/Age','Leads.controller:LeadAge')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/Age');


/////////////////////////////////////////////////////////////////////////////////

$app->get  ('/Leads/Advanced','Leads.controller:Advanced')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/Advanced');

$app->get  ('/leads/view/{id}','Leads.controller:View')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/view');

$app->post  ('/Leads/Search','Leads.controller:Search')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/Search');

$app->post  ('/Leads/SearchAdvanced','Leads.controller:SearchAdvanced')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SearchAdvanced');

$app->post  ('/Leads/SearchID','Leads.controller:SearchID')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SearchID');

$app->post  ('/Leads/Marketing','Leads.controller:Marketing')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/Marketing');

$app->post  ('/Leads/CreditCard','Leads.controller:CreditCard')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/CreditCard');

$app->post  ('/Leads/AccountBank','Leads.controller:AccountBank')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/AccountBank');

$app->post  ('/Leads/Status','Leads.controller:Status')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/Status');

$app->post  ('/Leads/ChangeStatus','Leads.controller:ChangeStatus')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ChangeStatus');

$app->post  ('/Leads/Change','Leads.controller:Change')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/Change');

$app->post  ('Leads/Notes','Leads.controller:Notes')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/Notes');

$app->post  ('Leads/GetNotes','Leads.controller:GetNotes')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/GetNotes');

$app->post  ('Leads/GetReferred','Leads.controller:GetReferred')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/GetReferred');

$app->post  ('Leads/ReferredList','Leads.controller:ReferredList')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ReferredList');

$app->post  ('Leads/ReferredListVZ','Leads.controller:ReferredListVZ')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ReferredListVZ');

$app->post  ('Leads/ChangeOptions','Leads.controller:ChangeOptions')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ChangeOptions');

$app->post  ('Leads/SaveDB','Leads.controller:SaveDB')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveDB');

$app->post  ('Leads/SaveMkt','Leads.controller:SaveMkt')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveMkt');

$app->post  ('Leads/SaveCypherID','Leads.controller:SaveCypherID')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveCypherID');

$app->post  ('Leads/SaveCypherSS','Leads.controller:SaveCypherSS')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveCypherSS');

$app->post  ('Leads/SaveCypherCC','Leads.controller:SaveCypherCC')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveCypherCC');

$app->post  ('Leads/SaveCypherAB','Leads.controller:SaveCypherAB')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveCypherAB');

$app->post  ('Leads/ZipChange','Leads.controller:ZipChange')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ZipChange');

/////////////////////////////////////////////////////////////////////////////////

$app->post  ('Leads/SaveTelevision','Leads.controller:SaveTelevision')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveTelevision');

$app->post  ('Leads/ViewTelevision','Leads.controller:ViewTelevision')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewTelevision');

$app->post  ('Leads/ViewServiceTelevision','Leads.controller:ViewServiceTelevision')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewServiceTelevision');

$app->post  ('Leads/SubmitTelevision','Leads.controller:SubmitTelevision')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SubmitTelevision');

/////////////////////////////////////////////////////////////////////////////////

$app->post  ('Leads/SaveSecurity','Leads.controller:SaveSecurity')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveSecurity');

$app->post  ('Leads/ViewSecurity','Leads.controller:ViewSecurity')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewSecurity');

$app->post  ('Leads/ViewServiceSecurity','Leads.controller:ViewServiceSecurity')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewServiceSecurity');

$app->post  ('Leads/SubmitSecurity','Leads.controller:SubmitSecurity')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SubmitSecurity');

/////////////////////////////////////////////////////////////////////////////////

$app->post  ('Leads/SaveInternet','Leads.controller:SaveInternet')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveInternet');

$app->post  ('Leads/ViewInternet','Leads.controller:ViewInternet')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewInternet');

$app->post  ('Leads/ViewServiceInternet','Leads.controller:ViewServiceInternet')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewServiceInternet');

$app->post  ('Leads/SubmitInternet','Leads.controller:SubmitInternet')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SubmitInternet');

/////////////////////////////////////////////////////////////////////////////////

$app->post  ('Leads/SaveMRouter','Leads.controller:SaveMRouter')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveMRouter');

$app->post  ('Leads/ViewMRouter','Leads.controller:ViewMRouter')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewMRouter');

$app->post  ('Leads/ViewServiceMRouter','Leads.controller:ViewServiceMRouter')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewServiceMRouter');

$app->post  ('Leads/SubmitMRouter','Leads.controller:SubmitMRouter')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SubmitMRouter');

/////////////////////////////////////////////////////////////////////////////////

$app->post  ('Leads/SaveInternetVZ','Leads.controller:SaveInternetVZ')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SaveInternetVZ');

$app->post  ('Leads/ViewInternetVZ','Leads.controller:ViewInternetVZ')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewInternetVZ');

$app->post  ('Leads/ViewServiceInternetVZ','Leads.controller:ViewServiceInternetVZ')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/ViewServiceInternetVZ');

$app->post  ('Leads/SubmitInternetVZ','Leads.controller:SubmitInternetVZ')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/SubmitInternetVZ');

/////////////////////////////////////////////////////////////////////////////////

$app->post  ('Leads/LoadImg','Leads.controller:LoadImg')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Leads/LoadImg');

/////////////////////////////////////////////////////////////////////////////////

$app->match('/Leads/ServObjections','Leads.controller:ServObjections')
    ->bind('/Leads/ServObjections')
    ->method('GET|POST');

$app->match('/Leads/SubmitObjService','Leads.controller:SubmitObjService')
    ->bind('/Leads/SubmitObjService')
    ->method('GET|POST');

$app->match('/Leads/InfoVe','Leads.controller:InfoVe')
    ->bind('/Leads/InfoVe')
    ->method('GET|POST');

/////////////////////////////////////////////////////////////////////////////////

$app->match('/Leads/Geographic/Township','Leads.controller:ListTownship')
    ->bind('/Leads/Geographic/Township')
    ->method('GET|POST');

$app->match('/Leads/Geographic/Parish','Leads.controller:ListParish')
    ->bind('/Leads/Geographic/Parish')
    ->method('GET|POST');

$app->match('/Leads/Geographic/Sector','Leads.controller:ListSector')
    ->bind('/Leads/Geographic/Sector')
    ->method('GET|POST');

/////////////////////////////////////////////////////////////////////////////////

$app->match('/Leads/Geographic/SectorG','Leads.controller:ListSectorG')
    ->bind('/Leads/Geographic/SectorG')
    ->method('GET|POST');

$app->match('/Leads/Geographic/SearchSector','Leads.controller:SearchSector')
    ->bind('/Leads/Geographic/SearchSector')
    ->method('GET|POST');

$app->match('/Leads/Geographic/UpdateTicket','Leads.controller:UpdateTicket')
    ->bind('/Leads/Geographic/UpdateTicket')
    ->method('GET|POST');

/////////////////////////////////////////////////////////////////////////////////

/*
|------------------------------------------------------------------------------
| Rutas de Leaders Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/Leaders','Leaders.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Leaders')
    ->method('GET|POST');

$app->get  ('/Leaders/Load','Leaders.controller:Load')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Leaders/Load')
    ->method('GET|POST');

$app->get  ('/Leaders/FormLoad','Leaders.controller:FormLoad')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Leaders/FormLoad')
    ->method('GET|POST');

$app->get  ('/Leaders/EditUser','Leaders.controller:EditUser')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Leaders/EditUser')
    ->method('GET|POST');

$app->get  ('/Leaders/FormEditUser','Leaders.controller:FormEditUser')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Leaders/FormEditUser')
    ->method('GET|POST');

$app->post  ('/leaders/search','Leaders.controller:search')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('leaders.search');


/*
|------------------------------------------------------------------------------
| Rutas de Approvals Controller
|------------------------------------------------------------------------------
|
*/
$app->get  ('/approvals','Approvals.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/approvals');

$app->get  ('/Appro/Closures','Approvals.controller:Closures')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/Closures')
    ->method('GET|POST');

$app->get  ('/Appro/ClosuresDate','Approvals.controller:ClosuresDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/ClosuresDate')
    ->method('GET|POST');

$app->get  ('/Appro/Closures/View','Approvals.controller:ViewClosures')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/Closures/View')
    ->method('GET|POST');




$app->get  ('/Appro/Closures/Load','Approvals.controller:LoadClosures')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/Closures/Load')
    ->method('GET|POST');

$app->get  ('/Appro/Closures/NewView','Approvals.controller:NewViewClosures')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/Closures/NewView')
    ->method('GET|POST');





$app->POST  ('/Appro/Load','Approvals.controller:Load')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/Load');

$app->match('/Appro/SearchDate','Approvals.controller:SearchDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/SearchDate')
    ->method('GET|POST');

$app->match('/Appro/ViewAproPend','Approvals.controller:ViewAproPend')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/ViewAproPend')
    ->method('GET|POST');

$app->match('/Appro/Notes','Approvals.controller:Notes')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/Notes')
    ->method('GET|POST');

$app->POST  ('/Appro/NotesSave','Approvals.controller:NotesSave')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/NotesSave');

$app->match('/Appro/QualAproPend','Approvals.controller:QualAproPend')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/QualAproPend')
    ->method('GET|POST');

$app->match('/Appro/ViewCancel','Approvals.controller:ViewCancel')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/ViewCancel')
    ->method('GET|POST');

$app->match('/Appro/ProcCancel','Approvals.controller:ProcCancel')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/ProcCancel')
    ->method('GET|POST');

$app->match('/Appro/SaveQualTmp','Approvals.controller:SaveQualTmp')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/SaveQualTmp')
    ->method('GET|POST');

$app->match('/Appro/QualAproProc','Approvals.controller:QualAproProc')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/QualAproProc')
    ->method('GET|POST');

$app->match('/Appro/CopyScore','Approvals.controller:CopyScore')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Appro/CopyScore')
    ->method('GET|POST');



/*
|------------------------------------------------------------------------------
| Rutas de Coordinationss Controller
|------------------------------------------------------------------------------
|
*/
$app->get  ('/Coordinations','Coordinations.controller:index')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coordinations');

$app->POST  ('/Coord/GetInfoCoord','Coordinations.controller:GetInfoCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/GetInfoCoord');

$app->POST  ('/Coord/GetInfoCoordPend','Coordinations.controller:GetInfoCoordPend')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/GetInfoCoordPend');

$app->POST  ('/Coord/ViewContact','Coordinations.controller:ViewContact')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/ViewContact');

$app->POST  ('/Coord/InfoCoord','Coordinations.controller:InfoCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/InfoCoord');

$app->POST  ('/Coord/NotesCoord','Coordinations.controller:NotesCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/NotesCoord');

$app->POST  ('/Coord/NoteSave','Coordinations.controller:NoteSave')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/NoteSave');

$app->POST  ('/Coord/PreCoordView','Coordinations.controller:PreCoordView')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/PreCoordView');

$app->POST  ('/Coord/PreCoordSup','Coordinations.controller:PreCoordSup')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/PreCoordSup');

$app->POST  ('/Coord/PreCoord','Coordinations.controller:PreCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/PreCoord');

$app->POST  ('/Coord/ViewDBCoord','Coordinations.controller:ViewDBCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/ViewDBCoord');

$app->POST  ('/Coord/SubmitCoord','Coordinations.controller:SubmitCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/SubmitCoord');

$app->POST  ('/Coord/SubmitReferredCoord','Coordinations.controller:SubmitReferredCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/SubmitReferredCoord');

$app->POST  ('/Coord/PackageTvPR','Coordinations.controller:PackageTvPR')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/PackageTvPR');

$app->POST  ('/Coord/PackageSePR','Coordinations.controller:PackageSePR')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/PackageSePR');

$app->POST  ('/Coord/PackageInPR','Coordinations.controller:PackageInPR')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/PackageInPR');

$app->POST  ('/Coord/PackageIVEPR','Coordinations.controller:PackageIVEPR')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/PackageIVEPR');

$app->POST  ('/Coord/SubmitTelevisionPR','Coordinations.controller:SubmitTelevisionPR')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/SubmitTelevisionPR');

$app->POST  ('/Coord/SubmitSecurityPR','Coordinations.controller:SubmitSecurityPR')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/SubmitSecurityPR');

$app->POST  ('/Coord/SubmitInternetPR','Coordinations.controller:SubmitInternetPR')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/SubmitInternetPR');

$app->POST  ('/Coord/SubmitInternetVZ','Coordinations.controller:SubmitInternetVZ')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/SubmitInternetVZ');

$app->POST  ('/Coord/Load','Coordinations.controller:Load')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/Load');

$app->POST  ('/Coord/SearchDate','Coordinations.controller:SearchDate')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/SearchDate');

$app->POST  ('/Coord/ViewCancel','Coordinations.controller:ViewCancel')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/ViewCancel');

$app->POST  ('/Coord/ProcCoordCancel','Coordinations.controller:ProcCoordCancel')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/ProcCoordCancel');

$app->POST  ('/Coord/LogCoord','Coordinations.controller:LogCoord')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Coord/LogCoord');
    

/*
|------------------------------------------------------------------------------
| Rutas de Test Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/test','Test.controller:index')
// ->before($app['middleware.redirectIfNotAuthenticated'])
->bind('test');

$app->get  ('/testAuth','Test.controller:TestAuth')
// ->before($app['middleware.UserAuthorized'])
->bind('testAuth');

$app->get  ('/testChange','Test.controller:TestChange')
// ->before($app['middleware.UserAuthorized'])
->bind('testChange');



$app->get  ('/imkt','Test.controller:imkt')
// ->before($app['middleware.redirectIfNotAuthenticated'])
->bind('imkt');

/*
|------------------------------------------------------------------------------
| Rutas de Services Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/service','Service.controller:index')
    ->bind('service')
    ->method('GET|POST');

$app->post ('/ServiceForm','Service.controller:ServiceForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ServiceForm');

$app->post ('/ServiceEdit','Service.controller:ServiceEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ServiceEdit');

$app->post  ('/Service/Email','Service.controller:EmailForm')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Service/Email')
    ->method('GET|POST');

$app->post  ('/Service/EmailEdit','Service.controller:EmailEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Service/EmailEdit')
    ->method('GET|POST');

/*
|------------------------------------------------------------------------------
| Rutas de Calendar Controller
|------------------------------------------------------------------------------
|
*/

$app->get ('/calendar','Calendar.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('calendar');

$app->get  ('/Calendar/Notifie','Calendar.controller:Notifie')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Calendar/Notifie')
    ->method('GET|POST');

$app->get  ('/Calendar/ListView','Calendar.controller:ListView')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Calendar/ListView')
    ->method('GET|POST');

$app->get  ('/Calendar/Save','Calendar.controller:SaveCa')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Calendar/Save')
    ->method('GET|POST');

$app->get  ('/Calendar/Notifie/Change','Calendar.controller:Change')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Calendar/Notifie/Change')
    ->method('GET|POST');

$app->get  ('/Calendar/Notifie/ChangeView','Calendar.controller:View')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Calendar/Notifie/ChangeView')
    ->method('GET|POST');


/*
|------------------------------------------------------------------------------
| Rutas de Config Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/Config/Approvals','Config.controller:Approvals')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Config/Approvals')
    ->method('GET|POST');

$app->post  ('/Config/ScoreEdit','Config.controller:ScoreEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Config/ScoreEdit')
    ->method('GET|POST');

$app->post  ('/Config/CancelEdit','Config.controller:CancelEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Config/CancelEdit')
    ->method('GET|POST');

$app->post  ('/Config/ScoreForm','Config.controller:ScoreForm')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Config/ScoreForm')
    ->method('GET|POST');


$app->post  ('/Config/CancelForm','Config.controller:CancelForm')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Config/CancelForm')
    ->method('GET|POST');

    

/*
|------------------------------------------------------------------------------
| Rutas de Roles Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/roles','Role.controller:index')
    ->before($app['middleware.Admins'])
    ->bind('roles')
    ->method('GET|POST');

$app->post  ('/RolForm','Role.controller:RolForm')
    ->before($app['middleware.Admins'])
    ->bind('RolForm');

$app->post ('/RolEdit','Role.controller:RolEdit')
    ->before($app['middleware.Admins'])
    ->bind('RolEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Departament Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/DepartForm','Departament.controller:DepartForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('DepartForm');

$app->post ('/DepartEdit','Departament.controller:DepartEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('DepartEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Did Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/TeamForm','Teams.controller:TeamForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('TeamForm');

$app->post ('/TeamEdit','Teams.controller:TeamEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('TeamEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Did Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/DidForm','Did.controller:DidForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('DidForm');

$app->post ('/DidEdit','Did.controller:DidEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('DidEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Country Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/country','Country.controller:index')
    ->bind('country')
    ->method('GET|POST');

$app->post  ('/CountryForm','Country.controller:CountryForm')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('CountryForm');

$app->post ('/CountryEdit','Country.controller:CountryEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('CountryEdit');

$app->post ('/Country/Load','Country.controller:Load')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Load');

$app->post ('/Country/State/Show','Country.controller:StateShow')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/State/Show');

$app->post ('/Country/State/Created','Country.controller:StateCreated')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/State/Created');

$app->post ('/Country/State/Edit','Country.controller:StateEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/State/Edit');

$app->post ('/Country/State/Update','Country.controller:StateUpdate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/State/Update');


$app->post ('/Country/TownShip/Show','Country.controller:TownShipShow')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/TownShip/Show');

$app->post ('/Country/TownShip/Created','Country.controller:TownShipCreated')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/TownShip/Created');

$app->post ('/Country/TownShip/Edit','Country.controller:TownShipEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/TownShip/Edit');

$app->post ('/Country/TownShip/Update','Country.controller:TownShipUpdate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/TownShip/Update');


$app->post ('/Country/Parish/Show','Country.controller:ParishShow')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Parish/Show');

$app->post ('/Country/Parish/Created','Country.controller:ParishCreated')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Parish/Created');

$app->post ('/Country/Parish/Edit','Country.controller:ParishEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Parish/Edit');

$app->post ('/Country/Parish/Update','Country.controller:ParishUpdate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Parish/Update');


$app->post ('/Country/Sector/Show','Country.controller:SectorShow')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Sector/Show');

$app->post ('/Country/Sector/Created','Country.controller:SectorCreated')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Sector/Created');

$app->post ('/Country/Sector/Edit','Country.controller:SectorEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Sector/Edit');

$app->post ('/Country/Sector/Update','Country.controller:SectorUpdate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Country/Sector/Update');

/*
|------------------------------------------------------------------------------
| Rutas de Country Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/TownForm','Town.controller:TownForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('TownForm');

$app->post ('/TownEdit','Town.controller:TownEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('TownEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Ceiling Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/CeilingForm','Ceiling.controller:CeilingForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('CeilingForm');

$app->post ('/CeilingEdit','Ceiling.controller:CeilingEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('CeilingEdit');

/*
|------------------------------------------------------------------------------
| Rutas de House Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/house','House.controller:index')
    ->bind('house')
    ->method('GET|POST');

$app->post  ('/HouseForm','House.controller:HouseForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('HouseForm');

$app->post ('/HouseEdit','House.controller:HouseEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('HouseEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Level Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/LevelForm','Level.controller:LevelForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('LevelForm');

$app->post ('/LevelEdit','Level.controller:LevelEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('LevelEdit');


/*
|------------------------------------------------------------------------------
| Rutas de Country Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/TownForm','Town.controller:TownForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('TownForm');

$app->post ('/TownEdit','Town.controller:TownEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('TownEdit');

/*
|------------------------------------------------------------------------------
| Rutas de ZipCode Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/ZipForm','Zip.controller:ZipForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ZipForm');

$app->post ('/ZipEdit','Zip.controller:ZipEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ZipEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Bank Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/banks','Bank.controller:index')
    ->bind('banks')
    ->method('GET|POST');

$app->post  ('/BankForm','Bank.controller:BankForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('BankForm');

$app->post ('/BankEdit','Bank.controller:BankEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('BankEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Payment Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/PaymentForm','Payment.controller:PaymentForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PaymentForm');

$app->post ('/PaymentEdit','Payment.controller:PaymentEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PaymentEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Package Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/packages','Package.controller:index')
    ->bind('packages')
    ->method('GET|POST');

$app->post  ('/PackageForm','Package.controller:PackageForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PackageForm');

$app->post ('/PackageEdit','Package.controller:PackageEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PackageEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Provider Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/ProviderForm','Provider.controller:ProviderForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ProviderForm');

$app->post ('/ProviderEdit','Provider.controller:ProviderEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ProviderEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Cameras Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/cameras','Cameras.controller:index')
    ->bind('cameras')
    ->method('GET|POST');

$app->post  ('/CameraForm','Cameras.controller:CameraForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('CameraForm');

$app->post ('/CameraEdit','Cameras.controller:CameraEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('CameraEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Dvr Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/DvrForm','Dvr.controller:DvrForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('DvrForm');

$app->post ('/DvrEdit','Dvr.controller:DvrEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('DvrEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Internet Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/internet','Internet.controller:index')
    ->bind('internet')
    ->method('GET|POST');

$app->post  ('/InternetForm','Internet.controller:InternetForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('InternetForm');

$app->post ('/InternetEdit','Internet.controller:InternetEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('InternetEdit');

/*
|------------------------------------------------------------------------------
| Rutas de InternetType Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/InternetTypeForm','InternetType.controller:InternetTypeForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('InternetTypeForm');

$app->post ('/InternetTypeEdit','InternetType.controller:InternetTypeEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('InternetTypeEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Objections Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/ObjectionForm','Objection.controller:ObjectionForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ObjectionForm');

$app->post ('/ObjectionEdit','Objection.controller:ObjectionEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ObjectionEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Creative Controller
|------------------------------------------------------------------------------
|
*/

$app->match ('/creative','Creative.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('creative')
    ->method('GET|POST');

$app->post  ('Creative/Upload','Creative.controller:Upload')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Creative/Upload');

$app->post  ('Creative/Image','Creative.controller:Image')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Creative/Image');

$app->post  ('Creative/Edit','Creative.controller:Edit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Creative/Edit');

$app->post  ('Creative/EditSave','Creative.controller:EditSave')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('Creative/EditSave');

$app->match ('/campaign','Creative.controller:LoadImgCampaign')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/campaign')
    ->method('GET|POST');

$app->match ('/Creative/LoadCamp','Creative.controller:LoadCamp')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Creative/LoadCamp')
    ->method('GET|POST');

/*
|------------------------------------------------------------------------------
| Rutas de Phone Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/PhoneForm','Phone.controller:PhoneForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PhoneForm');

$app->post ('/PhoneEdit','Phone.controller:PhoneEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PhoneEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Phone Provider Controller
|------------------------------------------------------------------------------
|
*/

$app->post  ('/Phone/Provider','Phone.controller:Provider')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('/Phone/Provider');

$app->post ('/Phone/EditProvider','Phone.controller:EditProvider')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('/Phone/EditProvider');


/*
|------------------------------------------------------------------------------
| Rutas de Conf-Markeying Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/medium','Medium.controller:index')
    ->bind('medium')
    ->method('GET|POST');

$app->post ('/MediumForm','Medium.controller:MediumForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('MediumForm');

$app->post ('/MediumEdit','Medium.controller:MediumEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('MediumEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Conf-Objetive Controller
|------------------------------------------------------------------------------
|
*/

$app->post ('/ObjetiveForm','Objetive.controller:ObjetiveForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ObjetiveForm');

$app->post ('/ObjetiveEdit','Objetive.controller:ObjetiveEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ObjetiveEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Conf-Post Controller
|------------------------------------------------------------------------------
|
*/

$app->post ('/PostForm','Post.controller:PostForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PostForm');

$app->Post ('/PostEdit','Post.controller:PostEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('PostEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Conf-Service Controller
|------------------------------------------------------------------------------
|
*/

$app->post ('/ServiceMForm','ServiceM.controller:ServiceMForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ServiceMForm');

$app->post ('/ServiceMEdit','ServiceM.controller:ServiceMEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ServiceMEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Conf-Origen Controller
|------------------------------------------------------------------------------
|
*/

$app->post ('/OrigenForm','Origen.controller:OrigenForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('OrigenForm');

$app->post ('/OrigenEdit','Origen.controller:OrigenEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('OrigenEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Conf-Forms Controller
|------------------------------------------------------------------------------
|
*/

$app->post ('/FormsForm','Forms.controller:FormsForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('FormsForm');

$app->post ('/FormsEdit','Forms.controller:FormsEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('FormsEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Conf-Campaign Controller
|------------------------------------------------------------------------------
|
*/

$app->post ('/CampaignForm','Campaign.controller:CampaignForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('CampaignForm');

$app->post ('/CampaignEdit','Campaign.controller:CampaignEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('CampaignEdit');

/*
|------------------------------------------------------------------------------
| Rutas de Referred Controller
|------------------------------------------------------------------------------
|
*/

$app->post ('/ReferredForm','Referred.controller:ReferredForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ReferredForm');

$app->post ('/ReferredEdit','Referred.controller:ReferredEdit')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ReferredEdit');

$app->post ('/ReferredEditForm','Referred.controller:ReferredEditForm')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ReferredEditForm');

$app->post ('/ReferredFormInc','Referred.controller:ReferredFormInc')
->before($app['middleware.redirectIfNotAuthenticated'])
->bind('ReferredFormInc');

/*
|------------------------------------------------------------------------------
| Rutas de Marketing Controller
|------------------------------------------------------------------------------
|
*/

$app->get ('marketing','Marketing.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('marketing');

$app->post  ('/Marketing/BasicMKT','Marketing.controller:BasicMKT')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Marketing/BasicMKT')
    ->method('GET|POST');

$app->post  ('/Marketing/SearchMKT','Marketing.controller:SearchMKT')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Marketing/SearchMKT')
    ->method('GET|POST');

$app->post  ('/Marketing/NewMKT','Marketing.controller:NewMKT')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Marketing/NewMKT')
    ->method('GET|POST');

$app->post  ('/Marketing/UpdateMKT','Marketing.controller:UpdateMKT')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Marketing/UpdateMKT')
    ->method('GET|POST');

$app->post  ('/Marketing/ViewClient','Marketing.controller:ViewClient')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Marketing/ViewClient')
    ->method('GET|POST');

$app->post  ('/Marketing/Load','Marketing.controller:Load')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Marketing/Load')
    ->method('GET|POST');

$app->post  ('/Marketing/SearchDate','Marketing.controller:SearchDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Marketing/SearchDate')
    ->method('GET|POST');

/*
|---------------------------------------------------------------------Tre---------
| Rutas de Reporting Controller
|------------------------------------------------------------------------------
|
*/

$app->get ('/Reporting','Reporting.controller:index')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Reporting');

$app->post ('/ReportingCalls','Reporting.controller:ReportingCalls')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingCalls');

$app->post ('/ReportingCallsUS','Reporting.controller:ReportingCallsUS')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingCallsUS');

$app->post  ('/ReportingQueue','Reporting.controller:ReportingQueue')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingQueue')
    ->method('GET|POST');

$app->post  ('/ReportingQueueVz','Reporting.controller:ReportingQueueVz')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingQueueVz')
    ->method('GET|POST');

$app->post  ('/ReportingLastNotes','Reporting.controller:ReportingLastNotes')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingLastNotes')
    ->method('GET|POST');

$app->post ('/ReportingCallsVzla','Reporting.controller:ReportingCallsVzla')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingCallsVzla');

$app->post ('/ReportingNumb','Reporting.controller:ReportingNumb')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingNumb');

$app->post  ('/reporting/massiveleads','Reporting.controller:MassiveLeads')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/reporting/massiveleads')
    ->method('GET|POST');

$app->post  ('/reporting/massivenotes','Reporting.controller:MassiveNotes')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/reporting/massivenotes')
    ->method('GET|POST');

$app->post  ('/reporting/massivenotesmikrowisp','Reporting.controller:MassiveNotesMikrowisp')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/reporting/massivenotesmikrowisp')
    ->method('GET|POST');

$app->post  ('/reporting/conglomeratecalls','Reporting.controller:ConglomerateCalls')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/reporting/conglomeratecalls')
    ->method('GET|POST');

$app->post ('/ReportingCliSegNotes','Reporting.controller:ReportingCliSegNotes')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingCliSegNotes');

$app->post ('/reporting/VerifiePaymentTypeIMikro','Reporting.controller:VerifiePaymentTypeIMikro')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/reporting/VerifiePaymentTypeIMikro');

$app->post ('ReportingCallsOperator','Reporting.controller:ReportingCallsOperator')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingCallsOperator');

$app->post ('ReportingPhonePR','Reporting.controller:ReportingPhonePR')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ReportingPhonePR');
    


/*
|------------------------------------------------------------------------------
| Rutas de Search Controller
|------------------------------------------------------------------------------
|
*/

// $app->get  ('/Search','Search.controller:index')
//     ->before($app['middleware.redirectIfNotAuthenticated'])
//     ->bind('/Search');

$app->get  ('/Search','Search.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Search')
    ->method('GET|POST');

$app->get  ('/SearchExterno','Search.controller:SearchExterno')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/SearchExterno')
    ->method('GET|POST');

$app->get  ('/Search/CypherClient','Search.controller:SearchCypherClient')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Search/CypherClient')
    ->method('GET|POST');

$app->get  ('/Search/CypherClientType','Search.controller:SearchCypherClientType')
    ->before($app['middleware.UserAuthorized'])
    ->bind('/Search/CypherClientType')
    ->method('GET|POST');

$app->get  ('/Search/Advanced','Search.controller:Advanced')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Search/Advanced')
    ->method('GET|POST');

$app->get  ('/Search/SearchAdvanced','Search.controller:SearchAdvanced')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Search/SearchAdvanced')
    ->method('GET|POST');

$app->get  ('/Search/ViewObjections','Search.controller:ViewObjections')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Search/ViewObjections')
    ->method('GET|POST');

$app->get  ('/Search/SugarPhone','Search.controller:SugarPhone')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Search/SugarPhone')
    ->method('GET|POST');

$app->get  ('/Search/GetSugarPhone','Search.controller:GetSugarPhone')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Search/GetSugarPhone')
    ->method('GET|POST');



/*
|------------------------------------------------------------------------------
| Rutas de Users Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/users','Users.controller:index')
    ->before($app['middleware.Admins'])
    ->bind('users')
    ->method('GET|POST');

$app->post ('/ChangePasswordSubmit','Users.controller:ChangePasswordSubmit')
    ->before($app['middleware.Admins'])
    ->bind('/ChangePasswordSubmit');

$app->post ('/userInfo','Users.controller:UserInfo')
    ->before($app['middleware.Admins'])
    ->bind('userInfo');

$app->post ('UserForm','Users.controller:UserForm')
    ->before($app['middleware.Admins'])
    ->bind('UserForm');

$app->post ('PermitForm','Users.controller:PermitForm')
    ->before($app['middleware.Admins'])
    ->bind('PermitForm');

$app->post ('/ShowEdit','Users.controller:ShowEdit')
    ->before($app['middleware.Admins'])
    ->bind('ShowEdit');

$app->post ('/PermitEdit','Users.controller:PermitEdit')
    ->before($app['middleware.Admins'])
    ->bind('/PermitEdit');


/*
|------------------------------------------------------------------------------
| Rutas de Operator Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/operator','Operator.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('operator');

$app->post  ('/Operator/Assigned','Operator.controller:Assigend')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Operator/Assigned')
    ->method('GET|POST');

$app->post  ('/Operator/SearchDate','Operator.controller:SearchDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Operator/SearchDate')
    ->method('GET|POST');

$app->post  ('/Operator/RespAssigned','Operator.controller:RespAssigned')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Operator/RespAssigned')
    ->method('GET|POST');

$app->post  ('/Operator/StatusChange','Operator.controller:StatusChange')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Operator/StatusChange')
    ->method('GET|POST');

$app->post  ('/Operator/ViewStatus','Operator.controller:ViewStatus')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Operator/ViewStatus')
    ->method('GET|POST');


/*
|------------------------------------------------------------------------------
| Rutas de Sheet PR Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('SheetPR','Support.controller:index')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR');

$app->POST  ('/SheetPR/Load','Support.controller:SheetLoad')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/Load');

$app->POST  ('/SheetPR/SheetLoadDate','Support.controller:SheetLoadDate')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/SheetLoadDate');

$app->POST  ('/SheetPR/SheetCreation','Support.controller:SheetCreation')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/SheetCreation')
    ->method('GET|POST');

$app->POST  ('/SheetPR/SheetSave','Support.controller:SheetSave')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/SheetSave')
    ->method('GET|POST');

$app->POST  ('/SheetPR/CancelSheet','Support.controller:CancelSheet')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/CancelSheet')
    ->method('GET|POST');

$app->POST  ('/SheetPR/PrevSheet','Support.controller:PrevSheet')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/PrevSheet')
    ->method('GET|POST');

$app->POST  ('/SheetPR/EditSheet','Support.controller:EditSheet')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/EditSheet')
    ->method('GET|POST');

$app->POST  ('/SheetPR/SubmitEditSheet','Support.controller:SubmitEditSheet')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/SubmitEditSheet')
    ->method('GET|POST');

$app->POST  ('/SheetPR/ChangePlan','Support.controller:ChangePlan')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetPR/ChangePlan')
    ->method('GET|POST');



/*
|------------------------------------------------------------------------------
| Rutas de Sheet VE Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('SheetVE','Support.controller:indexVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE');

$app->POST  ('/SheetVE/Load','Support.controller:SheetLoadVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/Load');

$app->POST  ('/SheetVE/SheetCreation','Support.controller:SheetCreationVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/SheetCreation')
    ->method('GET|POST');

$app->POST  ('/SheetVE/ChangePlan','Support.controller:ChangePlanVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/ChangePlan')
    ->method('GET|POST');

$app->POST  ('/SheetVE/SheetSave','Support.controller:SheetSaveVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/SheetSave')
    ->method('GET|POST');

$app->POST  ('/SheetVE/EditSheet','Support.controller:EditSheetVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/EditSheet')
    ->method('GET|POST');

$app->POST  ('/SheetVE/SubmitEditSheet','Support.controller:SubmitEditSheetVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/SubmitEditSheet')
    ->method('GET|POST');

$app->POST  ('/SheetVE/SheetLoadDate','Support.controller:SheetLoadDateVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/SheetLoadDate');

$app->POST  ('/SheetVE/PrevSheet','Support.controller:PrevSheetVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/PrevSheet')
    ->method('GET|POST');

$app->POST  ('/SheetVE/CancelSheet','Support.controller:CancelSheetVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/CancelSheet')
    ->method('GET|POST');

$app->POST  ('/SheetVE/SendEmailDay','Support.controller:SendEmailDay')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/SendEmailDay')
    ->method('GET|POST');

$app->POST  ('/SheetVE/addItems','Support.controller:addItems')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/addItems')
    ->method('GET|POST');

$app->POST  ('/SheetVE/createBill','Support.controller:createBill')
    ->before($app['middleware.CustomerServices'])
    ->bind('/SheetVE/createBill')
    ->method('GET|POST');
    


    

$app->POST  ('/Support/Mikrowisp/Pre','Support.controller:MikroPre')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Support/Mikrowisp/Pre')
    ->method('GET|POST');

$app->POST  ('/Support/Mikrowisp/Load','Support.controller:MikroLoad')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Support/Mikrowisp/Load')
    ->method('GET|POST');

$app->POST  ('/Support/Mikrowisp/ViewPreCord','Support.controller:ViewPreCord')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Support/Mikrowisp/ViewPreCord')
    ->method('GET|POST');

$app->POST  ('/Support/Mikrowisp/ProcPreCord','Support.controller:ProcPreCord')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Support/Mikrowisp/ProcPreCord')
    ->method('GET|POST');


$app->POST  ('/Support/Mikrowisp/SearchDate','Support.controller:SearchDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Support/Mikrowisp/SearchDate')
    ->method('GET|POST');





$app->POST  ('/sheet/ve/fiber','Support.controller:Fiber')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/store','Support.controller:Store')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/store')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/changeplan','Support.controller:FiberChangePlan')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/changeplan')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/sheetsave','Support.controller:FiberSheetSave')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/sheetsave')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/load','Support.controller:FiberLoad')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/load')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/loadate','Support.controller:FiberLoadDate')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/loadate')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/EditFiber','Support.controller:FiberEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/EditFiber')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/submiteditsheetfib','Support.controller:SubmitFiberEdit')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/submiteditsheetfib')
    ->method('GET|POST');

$app->POST  ('/sheet/ve/fiber/cancelsheetfib','Support.controller:CancelSheetFib')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/cancelsheetfib')
    ->method('GET|POST');    

$app->POST  ('/sheet/ve/fiber/contract','Support.controller:Contract')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/sheet/ve/fiber/contract')
    ->method('GET|POST'); 


/*
|------------------------------------------------------------------------------
| Rutas de Collection Controller
|------------------------------------------------------------------------------
|
*/
$app->get  ('/Collection','Collection.controller:index')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Collection');

/*
|------------------------------------------------------------------------------
| Rutas de Collection Venezuela Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/CollectionVE','Collection.controller:indexVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/CollectionVE');

$app->POST  ('/Collection/LoadVE','Collection.controller:LoadVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/LoadVE');

$app->POST  ('/Collection/UploadVE','Collection.controller:UploadVE')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/UploadVE');

$app->POST  ('/Collection/EditAmountCode','Collection.controller:EditAmountCode')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/EditAmountCode');

// $app->POST  ('/Collection/UploadVE','Collection.controller:UploadVE')
//     ->before($app['middleware.redirectIfNotAuthenticated'])
//     ->bind('/Collection/UploadVE');

$app->POST  ('/Collection/ProcessCodeTemp','Collection.controller:ProcessCodeTemp')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ProcessCodeTemp');

$app->POST  ('/Collection/ConsolideProccess','Collection.controller:ConsolideProccess')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ConsolideProccess');

$app->GET  ('/Collection/ReportCodeTemp','Collection.controller:ReportCodeTemp')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ReportCodeTemp');

$app->POST  ('/Collection/EditConsProc','Collection.controller:EditConsProc')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/EditConsProc');

$app->POST  ('/Collection/UpdateCons','Collection.controller:UpdateCons')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/UpdateCons');

$app->POST  ('/Collection/ProcConsProc','Collection.controller:ProcConsProc')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ProcConsProc');

$app->POST  ('/Collection/AsigCode','Collection.controller:AsigCode')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/AsigCode');

$app->POST  ('/Collection/SearchMWClient','Collection.controller:SearchMWClient')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/SearchMWClient');

$app->POST  ('/Collection/UpdateAsigned','Collection.controller:UpdateAsigned')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/UpdateAsigned');

// $app->POST  ('/Collection/Reset','Collection.controller:Reset')
//     ->before($app['middleware.redirectIfNotAuthenticated'])
//     ->bind('/Collection/Reset');

$app->POST  ('/Collection/ConsolideMultiple','Collection.controller:ConsolideMultiple')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ConsolideMultiple');

$app->POST  ('/Collection/ShowInfo','Collection.controller:ShowInfo')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ShowInfo');

$app->POST  ('/Collection/AsigRepClient','Collection.controller:AsigRepClient')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/AsigRepClient');

$app->POST  ('/Collection/UpdateAsigRepClient','Collection.controller:UpdateAsigRepClient')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/UpdateAsigRepClient');

$app->POST  ('/Collection/ShowManRepClient','Collection.controller:ShowManRepClient')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ShowManRepClient');

$app->POST  ('/Collection/UpdateProcessPendAsigned','Collection.controller:UpdateProcessPendAsigned')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/UpdateProcessPendAsigned');


$app->POST  ('/Collection/AsiBillPen','Collection.controller:AsiBillPen')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/AsiBillPen');

$app->POST  ('/Collection/ShowManBillPen','Collection.controller:ShowManBillPen')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ShowManBillPen');

$app->POST  ('/Collection/ShowInfoClient','Collection.controller:ShowInfoClient')
    ->before($app['middleware.CustomerServices'])
    ->bind('/Collection/ShowInfoClient');



$app->match('/collection/ve','CollectionVE.controller:index')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve')
    ->method('GET|POST');

$app->match('/collection/ve/load','CollectionVE.controller:Load')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/load')
    ->method('GET|POST');

$app->match('/collection/ve/consolide','CollectionVE.controller:Consolide')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/consolide')
    ->method('GET|POST');

$app->match('/collection/ve/batchsuspendcode','CollectionVE.controller:BatchSuspendCode')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/batchsuspendcode')
    ->method('GET|POST');

$app->match('/collection/ve/dollar','CollectionVE.controller:DollarData')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/dollar')
    ->method('GET|POST');

$app->match('/collection/ve/dollarview','CollectionVE.controller:DollarView')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/dollarview')
    ->method('GET|POST');

$app->match('/collection/ve/uploadfile','CollectionVE.controller:UploadFile')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/uploadfile')
    ->method('GET|POST');

$app->match('/collection/ve/processcodetemp','CollectionVE.controller:ProcessCodeTemp')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/processcodetemp')
    ->method('GET|POST');
    
$app->match('/collection/ve/editconsolide','CollectionVE.controller:EditConsolide')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/editconsolide')
    ->method('GET|POST');

$app->match('/collection/ve/proccessconsolidate','CollectionVE.controller:ProcessConsolidate')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/proccessconsolidate')
    ->method('GET|POST');

$app->match('/collection/ve/consolidemultiple','CollectionVE.controller:MultipleConsolidate')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/consolidemultiple')
    ->method('GET|POST');

$app->match('/collection/ve/manualcode','CollectionVE.controller:ManualCode')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/manualcode')
    ->method('GET|POST');

$app->match('/collection/ve/suspendconsolidemultiple','CollectionVE.controller:SuspendConsolideMultiple')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/suspendconsolidemultiple')
    ->method('GET|POST');

$app->match('/collection/ve/clientreporting','CollectionVE.controller:ClientReporting')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/clientreporting')
    ->method('GET|POST');
  
$app->match('/collection/ve/anexconsolidateview','CollectionVE.controller:AnexConsolidateView')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/anexconsolidateview')
    ->method('GET|POST');

$app->match('/collection/ve/asigcodepen','CollectionVE.controller:AsigCodePending')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/asigcodepen')
    ->method('GET|POST');

$app->match('/collection/ve/viewcodes','CollectionVE.controller:ViewCodes')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/viewcodes')
    ->method('GET|POST');

$app->match('/collection/ve/asigmanualclient','CollectionVE.controller:AsigManualClient')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/asigmanualclient')
    ->method('GET|POST');

$app->match('/collection/ve/clientcodeasig','CollectionVE.controller:ClientCodeAsig')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/clientcodeasig')
    ->method('GET|POST');

$app->match('/collection/ve/vieweditcodeasig','CollectionVE.controller:ViewEditCodeAsig')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/vieweditcodeasig')
    ->method('GET|POST');

$app->match('/collection/ve/storeeditcodeasig','CollectionVE.controller:StoreEditCodeAsig')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/storeeditcodeasig')
    ->method('GET|POST');




$app->match('/collection/ve/transactions','Transactions.controller:view')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/transactions')
    ->method('GET|POST');

$app->match('/collection/ve/search','Transactions.controller:Search')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/search')
    ->method('GET|POST');

$app->match('/collection/ve/searchClient','Transactions.controller:SearchClient')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/searchClient')
    ->method('GET|POST');

$app->match('/collection/ve/searchBill','Transactions.controller:SearchBill')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/searchBill')
    ->method('GET|POST');

$app->match('/collection/ve/payment','Transactions.controller:Payment')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/payment')
    ->method('GET|POST');

$app->match('/collection/ve/preprocess','Transactions.controller:PreProcess')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/preprocess')
    ->method('GET|POST');

$app->match('/collection/ve/postprocess','Transactions.controller:PostProcess')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/postprocess')
    ->method('GET|POST');

$app->match('/collection/ve/loadReports','Transactions.controller:LoadReports')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/loadReports')
    ->method('GET|POST');

$app->match('/collection/ve/viewReport','Transactions.controller:ViewReport')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/viewReport')
    ->method('GET|POST');

$app->match('/collection/ve/suspendedReport','Transactions.controller:SuspendedReport')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/suspendedReport')
    ->method('GET|POST');

$app->match('/collection/ve/receiptPayment','Transactions.controller:ReceiptPayment')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/receiptPayment')
    ->method('GET|POST');

$app->match('/collection/ve/addItems','Transactions.controller:AddItems')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/addItems')
    ->method('GET|POST');

$app->match('/collection/ve/createBill','Transactions.controller:CreateBill')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/createBill')
    ->method('GET|POST');

$app->match('/collection/ve/importPayment','Transactions.controller:ImportPayment')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/importPayment')
    ->method('GET|POST');

$app->match('/collection/ve/printPayment','Transactions.controller:PrintPayment')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/printPayment')
    ->method('GET|POST');

$app->match('/collection/ve/calculadora','Transactions.controller:Calculator')
    ->before($app['middleware.CustomerServices'])
    ->bind('/collection/ve/calculadora')
    ->method('GET|POST');


/*
|------------------------------------------------------------------------------
| Rutas de Collection Puerto Rico Controller
|------------------------------------------------------------------------------
|
*/

$app->POST  ('/CollectioPR','Collection.controller:indexPR')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/CollectionPR');


/*
|------------------------------------------------------------------------------
| Rutas de Collection Puerto Rico Controller
|------------------------------------------------------------------------------
|
*/

$app->POST  ('/CollectioPR','Collection.controller:indexPR')
    // ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/CollectionPR');

/*
|------------------------------------------------------------------------------
| Rutas de Contratos
|------------------------------------------------------------------------------
| Permiten iniciar y cerrar sesion, ademas de cambiar el password de usuario
|
*/

$app->match('/ContractsPR','Contract.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsPR')
    ->method('GET|POST');

$app->match('/ContractsPRLoad','Contract.controller:LoadPR')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsPRLoad')
    ->method('GET|POST');

$app->match('/ContractsClientPR','Contract.controller:ContractsClientPR')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsClientPR')
    ->method('GET|POST');

$app->match('/ContractsSubmitPR','Contract.controller:ContractsSubmitPR')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsSubmitPR')
    ->method('GET|POST');

$app->match('/ContractSearchLoadPR','Contract.controller:ContractSearchLoadPR')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractSearchLoadPR')
    ->method('GET|POST');



$app->match('/ContractsVE','Contract.controller:indexVE')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsVE')
    ->method('GET|POST');

$app->match('/ContractsVELoad','Contract.controller:LoadVE')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsVELoad')
    ->method('GET|POST');

$app->match('/ContractsClientVE','Contract.controller:ContractsClientVE')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsClientVE')
    ->method('GET|POST');

$app->match('/ContractsSubmitVE','Contract.controller:ContractsSubmitVE')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsSubmitVE')
    ->method('GET|POST');

$app->match('/ContractsSubmitVETest','Contract.controller:ContractsSubmitVETest')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsSubmitVETest')
    ->method('GET|POST');

$app->match('/ContractsSubmitVEFib','Contract.controller:ContractsSubmitVEFib')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractsSubmitVEFib')
    ->method('GET|POST');


$app->match('/ContractSearchLoadVE','Contract.controller:ContractSearchLoadVE')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/ContractSearchLoadVE')
    ->method('GET|POST');


/*
|------------------------------------------------------------------------------
| Rutas de Estadisticas
|------------------------------------------------------------------------------
|
*/

$app->match('/statics/dataentry','Statistics.controller:DataEntry')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/statics/dataentry')
    ->method('GET|POST');

$app->match('/statics/dataentry/load','Statistics.controller:DataEntryLoad')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/statics/dataentry/load')
    ->method('GET|POST');


/*
|------------------------------------------------------------------------------
| Rutas de Autenticacion
|------------------------------------------------------------------------------
| Permiten iniciar y cerrar sesion, ademas de cambiar el password de usuario
|
*/

$app->match('/logout','Auth.controller:logout')
    ->bind('logout')
    ->method('GET|POST');

$app->get ('/sendemail','Auth.controller:index');

$app->get ('/login','Auth.controller:LoginForm')
->bind('login');

$app->post ('/loginForm','Auth.controller:Login')
->bind('loginForm');

$app->get ('/forgot','Auth.controller:ForgotForm');

$app->get ('/password','Auth.controller:PassForm');

$app->get ('/passrecovery/{id}','Auth.controller:PassRecovery');

$app->post ('/resetpass','Auth.controller:ResetPass')
    ->bind('resetpass');

$app->post ('/recoverypass','Auth.controller:RecoveryPass')
    ->bind('recoverypass');

/*
|------------------------------------------------------------------------------
| Rutas de Error Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/ErrorLicense','Error.controller:license')
    ->bind('ErrorLicense')
    ->method('GET|POST');

$app->get  ('/Suspended','Error.controller:Suspended')
    ->bind('Suspended')
    ->method('GET|POST');

$app->get  ('/ErrorLead','Error.controller:Lead')
    ->bind('/ErrorLead')
    ->method('GET|POST');

$app->get  ('/ErrorPermisos','Error.controller:Permisos')
    ->bind('/ErrorPermisos')
    ->method('GET|POST');


/*
|------------------------------------------------------------------------------
| Rutas de Statistics Controller
|------------------------------------------------------------------------------
|
*/

$app->get  ('/statistics','Statistics.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/Statistics');

$app->get  ('/statistics/aproserv','Statistics.controller:AproServ')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/statistics/aproserv');


/*
|------------------------------------------------------------------------------
| Rutas de Development Controller
|------------------------------------------------------------------------------
|
*/

$app->match('/development','Development.controller:index')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/development')
    ->method('GET|POST');

$app->match('/developmentCSV','Development.controller:DevCSV')
    ->before($app['middleware.redirectIfNotAuthenticated'])
    ->bind('/developmentCSV')
    ->method('GET|POST');

// $app->match('/developmentCSV','Development.controller:DevCSV')
//     ->before($app['middleware.redirectIfNotAuthenticated'])
//     ->bind('/developmentCSV')
//     ->method('GET|POST');



/*
|------------------------------------------------------------------------------
| Manejo de excepciones
|------------------------------------------------------------------------------
|
*/
$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
