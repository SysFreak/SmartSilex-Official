<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\Emails;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class ServiceController extends BaseController
{
    
    //////////////////////////////////////////////////////////////////////////////////////////////////

    public function index(Application $app, Request $request)
    {
        return $app['twig']->render('service/index.html.twig',array(
            'sidebar'   =>  true,
            'status'    =>  Status::GetStatus(),
            'services'  =>  Service::GetService(),
            'countrys'  =>  Country::GetCountry(),
            'departs'   =>  Departament::GetDepartament(),
            'emails'    =>  Emails::GetEmail()
        ));
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
        
    public static function ServiceEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'service'   => Service::ServiceById($request->get('id'))
        ));
    
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function ServiceForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type'] == 'new')
        {
            $saveDep = Service::Service($params);
                
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Service New', 'message' => 'Creacion de Servicio - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'service'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Service New', 'message' => 'Error Al Intentar Crear Servicio - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type'] == 'edit')
        {

            $saveDep = Service::Service($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Service Edit', 'message' => 'Actualizacion de Servicio - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'service'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Service Edit', 'message' => 'Error Al Intentar Actualizar informacion del Servicio - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            } 
            
        }
    
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    public static function EmailForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_e'] == 'new')
        {

            $saveDep = Emails::SaveEmail($params);
                
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Email Departament New', 'message' => 'Creacion de Email por Departamento - '.strtoupper($params['email_e']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'service'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Email Departament New', 'message' => 'Error Al Intentar Crear Email Por Departamento - '.strtoupper($params['email_e']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_e'] == 'edit')
        {

            $saveDep = Emails::SaveEmail($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Email Departament Edit', 'message' => 'Actualizacion de Email Por Departamento - '.strtoupper($params['email_e']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'service'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Email Departament Edit', 'message' => 'Error Al Intentar Actualizar Email Por Departamento - '.strtoupper($params['email_e']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            } 
            
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    public static function EmailEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'service'   => Emails::GetEmailById($request->get('id'))
        ));

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

}