<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\User;
use App\Models\Ticket;
use App\Models\TicketService;
use App\Models\TicketDepartament;
use App\Models\Departament;
use App\Models\Emails;
use App\Models\Status;

use App\Lib\SendMail;
use App\Lib\DBSmart;
use App\Lib\Config;

use PDO;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 *	Ticket Controller
 */

class TicketController extends BaseController
{

///////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function __construct(Application $app)
    {
        $curl = $app['curl']->execute(array('command' => 'license'));

        if($curl == 'expired'){ return $app['twig']->render('error/license.html.twig'); } 
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Index(Application $app, Request $request)
    {

        return $app['twig']->render('ticket/index.html.twig',array(
            'sidebar'       =>  true
        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Config(Application $app, Request $request)
    {
        $departament        =   TicketDepartament::FindDepartament();

        return $app['twig']->render('ticket/config.html.twig',array(
            'sidebar'       =>  true,
            'services'      =>  TicketService::GetService(),
            'departaments'  =>  TicketDepartament::GetDepartament(),
            'dept1'         =>  $departament['dept1'],
            'dept2'         =>  $departament['dept2'],
            'status'        =>  Status::GetStatus(),

        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function DepartamentEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'   => true, 
            'ref'      =>  TicketDepartament::GetDepartamentById($request->get('id'))
        ));  
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function DepartamentSave(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);

        $ticket = TicketDepartament::SaveDepartament($params);

        if($ticket <> false)
        { 
            $info = array('client' => '', 'channel' => 'Ticket Departament', 'message' => 'Creacion de Departamento en Ticket - '.strtoupper($params['ticket_name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   => true, 
                'web'      => '/Ticket/Config'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Ticket Departament', 'message' => 'Error Al Intentar Crear Departamento en Ticket- '.strtoupper($params['ticket_name']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Error al Intentar Actualizar Departamento de Ticket, Intente Nuevamente.'
            )); 
        } 
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ServiceEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'   => true, 
            'ref'      =>  TicketService::GetServiceById($request->get('id'))
        ));  
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ServiceSave(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);

        $ticket = TicketService::SaveService($params);

        if($ticket <> false)
        { 
            $info = array('client' => '', 'channel' => 'Ticket Servicio', 'message' => 'Creacion de Servicio en Ticket - '.strtoupper($params['ticket_serv_name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   => true, 
                'web'      => '/Ticket/Config'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Ticket Servicio', 'message' => 'Error Al Intentar Crear Servicio en Ticket- '.strtoupper($params['ticket_serv_name']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Error al Intentar Actualizar Servicio de Ticket, Intente Nuevamente.'
            )); 
        } 
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Load(Application $app, Request $request)
    {

        $info = Ticket::Load($app['session']->get('departament'));

        return $app->json(array(
            'status'    =>  true, 
            'pending'   =>  $info['pending'],
            'processed' =>  $info['processed']  
        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function SearchDate(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];

        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            $info   =   Ticket::LoadByDate($app['session']->get('departament'), "1", $dates);
        }else{
            $info   =   Ticket::LoadByDate($app['session']->get('departament'), "0", $dates);
        }

        return $app->json(array(
            'status'    =>  true, 
            'pending'   =>  $info['pending'],
            'processed' =>  $info['processed']  
        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ModalTicket(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'users'     => User::GetUsersHtmlTicket(),
            'deps'      => TicketDepartament::GetDepartamentHtml(),
        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Services(Application $app, Request $request)
    {
		return $app->json(array(
			'status'	=> true, 
			'servs'		=> TicketService::GetServiceHtml($request->get('id'))
		));
    
    }

    public static function GetServices(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'depart'    => TicketService::GetHtmlDepart(),
            'support'   => TicketService::HtmlService("1"),
            'data'      => TicketService::HtmlService("4"),
            'payment'   => TicketService::HtmlService("11"),
            'operator'  => User::HtmlTicket(),
        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function SubmitTicket(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);

        $ticket     =   Ticket::GetTicket();

        if((isset($params['tic_ope'])) AND (isset($params['tic_dep'])) AND (isset($params['tic_serv'])))
        {
            $ope    =   User::GetUserById($params['tic_ope']);
            $serv   =   TicketService::GetServiceById($params['tic_serv']);
            $dep    =   TicketDepartament::GetDepartamentByTicDep($params['tic_dep']);

            $iData  =   [
                'ticket'        =>  $ticket,
                't_dep'         =>  $dep['ticket_dep'],
                'dep'           =>  $params['tic_dep'],
                'departament'   =>  $dep['name'],
                'ope'           =>  $params['tic_ope'],
                'username'      =>  $ope['username'],
                'email'         =>  $ope['email'],
                'serv'          =>  $params['tic_serv'],
        		'service'		=>	$serv['name'],
        		'pues'			=>	strtoupper($params['tic_pues']),
        		'desc'			=>	strtoupper($params['tic_desc']),
        		'subject'		=> 	'TICKET DE SERVICIO - '.$ticket.' - '.$serv['name'].'',
        		'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/04/Conexio%CC%81n.png',
        	];

            $save 	        =	Ticket::SaveTicket($iData);

        	if($save <> false)
        	{	
        		$emailLeads =	'';

                $dEmail     =   TicketService::GetServiceById($params['tic_serv']);

                $query      =   'SELECT t3.email, UPPER(t3.name) as name FROM cp_tickets AS t1 INNER JOIN data_ticket_dep AS t2 ON (t1.dep_id =  t2.departament_id) INNER JOIN data_emails AS t3 ON (t2.dep_id = t3.departament_id) AND t1.dep_id = "'.$dEmail['dep_id'].'" GROUP BY t3.email';

                $emailLeads =   DBSmart::DBQueryAll($query);

				$leadHTML	=	Ticket::GetTicketHtml($iData);

				$refHTML	=	$supHTML = $saleHTML = $subHTML = "";
                
                $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

				$mail   	=   new SendMail();
                $email      =   $mail->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

                $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

				$mail2   	=   new SendMail();
				foreach ($emailLeads as $e => $mail) 
				{
                    $iDatas     =   ['subject'  =>  $mail['email'], 'prop_email' => $mail['email'], 'prop' => $mail['name']];

					$email2  	=   $mail2->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
				}

		        $info = array('client' => $iData['ticket'], 'channel' => 'Ticket de Servicio', 'message' => 'Ticket de Servicio - '.$iData['service'].' - Departamento - '.$iData['departament'].' Ticket - '.$iData['ticket'].' - Creado - '.strtoupper($iData['username']).' - Ejecutado Por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);


                $info   =   Ticket::GetTicketById($save);

                if($info['dep_id'] == 4)
                {
                    $iDataT  =   [
                        'id'            =>  $info['id'],
                        'ticket'        =>  $info['ticket'],
                        'status_id'     =>  1,
                        'operator_id'   =>  1,
                        'operator_at'   =>  $info['created'],
                        'deadline'      =>  "0000-00-00 00:00:00",
                        'asigned_by'    =>  1,
                        'asigned_at'    =>  ""
                    ];
                    $insert     =   Ticket::SaveDA($iDataT);
                }

				return $app->json(array(
		            'status'    => 	true,
		            'html'		=>	Ticket::TicketHtml($iData)
		        ));

        	}else{

		        $info = array('client' => '', 'channel' => 'Ticket de Servicio', 'message' => 'Error al crear Ticket de Servicio - '.$iData['service'].' Departamento - '.$iData['departament'].' - Solicitado por - '.$iData['username'].' - Ejecutado por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

				return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'No se pudo generar el ticket solicitado, Intente nuevamente.'
		        ));
        	}
        
        }else{
	        $info = array('client' => '', 'channel' => 'Ticket de Servicio', 'message' => 'Error al crear Ticket de Servicio, Faltan Datos - Ejecutado por - '.$app['session']->get('username').' - Incorrectamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'No se pudo generar el ticket solicitado, Intente nuevamente.'
	        ));
        }
    
    }

    public static function SubmitTicketSup(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);
        
        $ticket     =   Ticket::GetTicket();

        if((isset($params['tic_ope'])) AND (isset($params['ticS_serv'])) AND (isset($params['ticS_desc'])))
        {

            $ope    =   User::GetUsersID($params['tic_ope']);
            $serv   =   TicketService::ServiceID($params['ticS_serv']);
            $dep    =   TicketDepartament::DepartamentDep($params['tic_type']);
            
            $iData  =   [
                'ticket'        =>  $ticket,
                't_dep'         =>  $dep['ticket_dep'],
                'dep'           =>  $dep['ticket_dep'],
                'departament'   =>  $dep['name'],
                'ope'           =>  $params['tic_ope'],
                'username'      =>  $ope['username'],
                'email'         =>  $ope['email'],
                'serv'          =>  $params['ticS_serv'],
                'service'       =>  $serv['name'],
                'pues'          =>  '',
                'desc'          =>  strtoupper($params['ticS_desc']),
                'subject'       =>  'TICKET DE SERVICIO - '.$ticket.' - '.$serv['name'].'',
                'img'           =>  'https://boomsolutionspr.com/wp-content/uploads/2019/04/Conexio%CC%81n.png',
            ];

            $save           =   Ticket::SaveTicket($iData);
            // $save           =   true;

            if($save <> false)
            {   
                $emailLeads =   '';

                $dEmail     =   TicketService::ServiceID($params['ticS_serv']);

                $query      =   'SELECT t3.email, UPPER(t3.name) as name FROM cp_tickets AS t1 INNER JOIN data_ticket_dep AS t2 ON (t1.dep_id =  t2.departament_id) INNER JOIN data_emails AS t3 ON (t2.dep_id = t3.departament_id) AND t1.dep_id = "'.$dEmail['dep_id'].'" GROUP BY t3.email';

                $emailLeads =   DBSmart::DBQueryAll($query);

                $leadHTML   =   Ticket::TicketHtml($iData);

                $refHTML    =   $supHTML = $saleHTML = $subHTML = "";
                
                $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

                $mail       =   new SendMail();
                $email      =   $mail->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

                $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

                $mail2      =   new SendMail();
                foreach ($emailLeads as $e => $mail) 
                {
                    $iDatas     =   ['subject'  =>  $mail['email'], 'prop_email' => $mail['email'], 'prop' => $mail['name']];

                    $email2     =   $mail2->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
                }

                $info = array('client' => $iData['ticket'], 'channel' => 'Ticket de Servicio', 'message' => 'Ticket de Servicio - '.$iData['service'].' - Departamento - '.$iData['departament'].' Ticket - '.$iData['ticket'].' - Creado - '.strtoupper($iData['username']).' - Ejecutado Por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                $info   =   Ticket::GetID($save);

                $iDataT  =   [
                    'id'            =>  $info['id'],
                    'ticket'        =>  $info['ticket'],
                    'name'          =>  strip_tags($params['ticS_name']),
                    'cedula'        =>  '',
                    'phone'         =>  strip_tags($params['ticS_phone']),
                    'alternative'   =>  strip_tags($params['ticS_alter']),
                    'email'         =>  strip_tags($params['ticS_email']),
                    'type'          =>  $dep['ticket_dep'],
                    'comment'       =>  strip_tags($params['ticS_desc']),
                    'status_id'     =>  1,
                    'operator_id'   =>  1,
                    'operator_at'   =>  $info['created'],
                    'deadline'      =>  "0000-00-00 00:00:00",
                    'asigned_by'    =>  1,
                    'asigned_at'    =>  ""
                ];

                $insert     =   Ticket::SaveGen($iDataT, 'cp_ticket_support');

                return $app->json(array(
                    'status'    =>  true,
                    'html'      =>  Ticket::TicketHtml($iData)
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Ticket de Servicio', 'message' => 'Error al crear Ticket de Servicio - '.$iData['service'].' Departamento - '.$iData['departament'].' - Solicitado por - '.$iData['username'].' - Ejecutado por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'No se pudo generar el ticket solicitado, Intente nuevamente.'
                ));
            
            }

        }else{

            $info = array('client' => '', 'channel' => 'Ticket de Servicio', 'message' => 'Error al crear Ticket de Servicio, Faltan Datos - Ejecutado por - '.$app['session']->get('username').' - Incorrectamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Por favor ingrese una descripcion valida.'
            ));
       
        }

    }

    public static function SubmitTicketPay(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);
        
        $ticket     =   Ticket::GetTicket();

        if((isset($params['tic_ope'])) AND (isset($params['ticP_serv'])) AND (isset($params['ticP_desc'])))
        {

            $ope    =   User::GetUsersID($params['tic_ope']);
            $serv   =   TicketService::ServiceID($params['ticP_serv']);
            $dep    =   TicketDepartament::DepartamentDep($params['tic_type']);
           
            $iData  =   [
                'ticket'        =>  $ticket,
                't_dep'         =>  $dep['ticket_dep'],
                'dep'           =>  $dep['ticket_dep'],
                'departament'   =>  $dep['name'],
                'ope'           =>  $params['tic_ope'],
                'username'      =>  $ope['username'],
                'email'         =>  $ope['email'],
                'serv'          =>  $params['ticP_serv'],
                'service'       =>  $serv['name'],
                'pues'          =>  '',
                'desc'          =>  strtoupper($params['ticP_desc']),
                'subject'       =>  'TICKET DE SERVICIO - '.$ticket.' - '.$serv['name'].'',
                'img'           =>  'https://boomsolutionspr.com/wp-content/uploads/2019/04/Conexio%CC%81n.png',
            ];

            $save           =   Ticket::SaveTicket($iData);
            // $save           =   true;

            if($save <> false)
            {   
                $emailLeads =   '';

                $dEmail     =   TicketService::ServiceID($params['ticP_serv']);

                $query      =   'SELECT t3.email, UPPER(t3.name) as name FROM cp_tickets AS t1 INNER JOIN data_ticket_dep AS t2 ON (t1.dep_id =  t2.departament_id) INNER JOIN data_emails AS t3 ON (t2.dep_id = t3.departament_id) AND t1.dep_id = "'.$dEmail['dep_id'].'" GROUP BY t3.email';

                $emailLeads =   DBSmart::DBQueryAll($query);

                $leadHTML   =   Ticket::TicketHtml($iData);

                $refHTML    =   $supHTML = $saleHTML = $subHTML = "";
                
                $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

                $mail       =   new SendMail();
                $email      =   $mail->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

                $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

                $mail2      =   new SendMail();
                foreach ($emailLeads as $e => $mail) 
                {
                    $iDatas     =   ['subject'  =>  $mail['email'], 'prop_email' => $mail['email'], 'prop' => $mail['name']];

                    $email2     =   $mail2->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
                }

                $info = array('client' => $iData['ticket'], 'channel' => 'Ticket de Servicio', 'message' => 'Ticket de Servicio - '.$iData['service'].' - Departamento - '.$iData['departament'].' Ticket - '.$iData['ticket'].' - Creado - '.strtoupper($iData['username']).' - Ejecutado Por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                $info   =   Ticket::GetID($save);

                $iDataT  =   [
                    'id'            =>  $info['id'],
                    'ticket'        =>  $info['ticket'],
                    'name'          =>  strip_tags($params['ticP_name']),
                    'cedula'        =>  '',
                    'phone'         =>  strip_tags($params['ticP_phone']),
                    'alternative'   =>  strip_tags($params['ticP_alter']),
                    'email'         =>  strip_tags($params['ticP_email']),
                    'type'          =>  $dep['ticket_dep'],
                    'comment'       =>  strip_tags($params['ticP_desc']),
                    'status_id'     =>  1,
                    'operator_id'   =>  1,
                    'operator_at'   =>  $info['created'],
                    'deadline'      =>  "0000-00-00 00:00:00",
                    'asigned_by'    =>  1,
                    'asigned_at'    =>  ""
                ];

                $insert     =   Ticket::SaveGen($iDataT, 'cp_ticket_payment');

                return $app->json(array(
                    'status'    =>  true,
                    'html'      =>  Ticket::TicketHtml($iData)
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Ticket de Servicio', 'message' => 'Error al crear Ticket de Servicio - '.$iData['service'].' Departamento - '.$iData['departament'].' - Solicitado por - '.$iData['username'].' - Ejecutado por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'No se pudo generar el ticket solicitado, Intente nuevamente.'
                ));
            
            }

        }else{

            $info = array('client' => '', 'channel' => 'Ticket de Servicio', 'message' => 'Error al crear Ticket de Servicio, Faltan Datos - Ejecutado por - '.$app['session']->get('username').' - Incorrectamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Por favor ingrese una descripcion valida.'
            ));
       
        }
    
    }

    public static function SubmitTicketDat(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);

        var_dump($params);
        exit;
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ViewTicket(Application $app, Request $request)
    {
        $info   =   Ticket::GetTicketById($request->get('id'));

        $html   =   '<div class="row"><div class="col-lg-12">'.$info['information'].'</div></div></div>';

        return $app->json(array(
            'status'    =>  true, 
            'html'      =>  $html
        ));
   
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ProcessTicket(Application $app, Request $request)
    {
        $info       =   Ticket::GetTicketById($request->get('id'));

        return $app->json(array(
            'status'    =>  true, 
            'html'      =>  Ticket::ViewProcess($info)
        ));
   
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ChangeTicket(Application $app, Request $request)
    {
        $html   =   '<div class="row"><div class="col-md-12"><div class="form-group">
        <input name="ticket_id" id="ticket_id" value="'.$request->get('id').'" readonly="" hidden="">
        <textarea class="form-control" rows="5" required="" name="ticket_description" id="ticket_description"></textarea></div></div></div>';

        return $app->json(array(
            'status'    =>  true, 
            'html'      =>  $html
        ));
   
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function CloseTicket(Application $app, Request $request)
    {

        $params     =   [];
        parse_str($request->get('str'), $params);

        $iData  =   [
            'id'        =>  $params['ticket_id'],
            'descript'  =>  ($params['ticket_description'] <> '' ) ? strtoupper($params['ticket_description']) : '',
            'finish'    =>  $app['session']->get('id')
        ];

        $ticket     =   Ticket::GetTicketById($params['ticket_id']);

        $upgrade    =   Ticket::CloseTicket($iData);

        if($upgrade == true)
        { 

            $info = array('client' => '', 'channel' => 'Ticket Servicio', 'message' => 'Ticket # - '.strtoupper($ticket['ticket']).' - Procesado Satisfactoriamente por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            $ticket     =   Ticket::GetTicketById($params['ticket_id']);

            $ope        =   User::GetUserById($ticket['operator_id']);

            $info       =   Ticket::GetTicketById($ticket['id']);

            $serv       =   TicketService::GetServiceById($ticket['serv_id']);

            $leadHTML   =   Ticket::ViewProcess($info);

            $iData  =   [
                'email'         =>  $ope['email'],
                'username'      =>  $ope['username'],
                'f_username'    =>  $ticket['finish'],
                'ticket'        =>  $ticket['ticket'],
                'service'       =>  $ticket['ticket'],
                'img'           =>  'https://boomsolutionspr.com/wp-content/uploads/2019/04/Conexio%CC%81n.png',
                'subject'       =>  'TICKET DE SERVICIO - '.$ticket['ticket'].' - '.$serv['name'].'',
                'resp'          =>  $ticket['description'],
                'operador'      =>  $ticket['operator'],
                'finish'        =>  $ticket['finish_at']
            ];

            $query      =   'SELECT t3.email, UPPER(t3.name) as name FROM cp_tickets AS t1 INNER JOIN data_ticket_dep AS t2 ON (t1.dep_id =  t2.departament_id) INNER JOIN data_emails AS t3 ON (t2.dep_id = t3.departament_id) AND t1.dep_id = "'.$ticket['dep_id'].'" GROUP BY t3.email';

            $emailLeads =   DBSmart::DBQueryAll($query);

            $leadHTML   =   Ticket::GetTicketRespHtml($iData);

            $refHTML    =   $supHTML = $saleHTML = $subHTML = "";

            $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

            $mail       =   new SendMail();
            $email      =   $mail->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

            $iDatas     =   ['subject'  =>  $iData['email'], 'prop_email' => $iData['email'], 'prop' => $iData['username']];

            $mail2      =   new SendMail();
            foreach ($emailLeads as $e => $mail) 
            {
                $iDatas     =   ['subject'  =>  $mail['email'], 'prop_email' => $mail['email'], 'prop' => $mail['name']];

                $email2     =   $mail2->SendEmail($iDatas, $iData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
            }

            return $app->json(array(
                'status'   => true, 
                'web'      => '/Ticket/Config'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Ticket Servicio', 'message' => 'Ticket # '.strtoupper($ticket['ticket']).' - Error al intentar finalizar - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Error al Intentar Actualizar Servicio de Ticket, Intente Nuevamente.'
            )); 
        } 

        return $app->json(array(
            'status'    =>  $upgrade, 
            'title'     =>  ($upgrade == false) ? 'Error' : '',
            'content'   =>  ($upgrade == false) ? 'Se produjo un error actualizando la informacion, intente de nuevo.' : ''
        ));
    
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////
    
    public static function Admins(Application $app, Request $request)
    {
 
        if(($app['session']->get('id') == "73") OR ($app['session']->get('id') == "291") OR ($app['session']->get('id') == "2") OR ($app['session']->get('role') == "1"))
        {
            return $app['twig']->render('ticket/admins/index.html.twig',array(
                'sidebar'       =>  true,
                'users'         =>  User::GetUsersDepDA('4')
            ));

        }else{
            return $app['twig']->render('error/permisos.html.twig',array(
                'sidebar'       =>  true,
                'users'         =>  User::GetUsersDepDA('4')
            ));
        }
        
    }

    public static function AdminsLoad(Application $app, Request $request)
    {
        $tickets    =   Ticket::AdminsLoad();

        return $app->json(array(
            'status'    =>  true,
            'pending'   =>  Ticket::AdminLoadPeningHTML($tickets['pending']),
            'process'   =>  Ticket::AdminLoadProcessHTML($tickets['process'], 'TableProcess'),
            'cancel'    =>  Ticket::AdminLoadProcessHTML($tickets['cancel'], 'TableCancel'),
            'statics'   =>  Ticket::HtmlStatics($tickets['statics']),
            'gensta'    =>  $tickets['GenStatics'],
        ));

    }

    public static function AdminsDate(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];

        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            $tickets   =   Ticket::AdminsLoadDate("1", $dates);
        }else{
            $tickets   =   Ticket::AdminsLoadDate("0", $dates);
        }

        return $app->json(array(
            'status'    =>  true,
            'pending'   =>  Ticket::AdminLoadPeningHTML($tickets['pending']),
            'process'   =>  Ticket::AdminLoadProcessHTML($tickets['process'], 'TableProcess'),
            'cancel'    =>  Ticket::AdminLoadProcessHTML($tickets['cancel'], 'TableCancel'),
            'statics'   =>  Ticket::HtmlStatics($tickets['statics']),
            'gensta'    =>  $tickets['GenStatics'],
        ));

    }

    public static function AdminsInfo(Application $app, Request $request)
    {
        $tickets    =   Ticket::AdminsInfo($request->get('id'));

        return $app->json(array(
            'status'    =>  true,
            'info'      =>  $tickets['data']['information'],
            'resp'      =>  $tickets['data']['description'],
            'notes'     =>  Ticket::HtmlNotes($tickets['notes']),
            'log'       =>  Ticket::HtmlLog($tickets['logs']),
        ));

    }

    public static function AdminsSaveNotes(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        if($params['notes2'] <> "")
        {
            $tickets    =   Ticket::AdminsInfo($params['ti_no_id']);

            if($tickets <> false)
            {
                $info   =   [
                    'ticket'    =>  $tickets['data']['ticket'],
                    'note'      =>  strtoupper($params['notes2']),
                    'operator'  =>  $app['session']->get('id')
                ];

                $notes  =   Ticket::AdminsSaveNotes($info);

                if($notes <> false)
                {
                    $info   =   [
                        'ticket'    =>  $tickets['data']['ticket'],
                        'info'      =>  strtoupper('CREACION DE NOTA'),
                        'operator'  =>  $app['session']->get('id')
                    ];
                    
                    $log    =   Ticket::AdminsSaveLog($info);

                    $tickets    =   Ticket::AdminsInfo($params['ti_no_id']);

                    return $app->json(array(
                        'status'    =>  true,
                        'notes'     =>  Ticket::HtmlNotes($tickets['notes']),
                        'log'       =>  Ticket::HtmlLog($tickets['logs']),
                    ));
                }else{
                    return $app->json(array(
                        'status'    =>  false,
                        'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> No se pudo almacenar nota intente nuevamente.! </strong> </div>',
                    ));                   
                }

            }else{
                return $app->json(array(
                    'status'    =>  false,
                    'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> No se encuentra ticket asociado.! </strong> </div>',
                ));
            }

        }else{
            return $app->json(array(
                'status'    =>  false,
                'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> Debe dejar una nota valida</strong> </div>',
            ));

        }

    }

    public static function AdminsCommands(Application $app, Request $request)
    {
        $_replace   =   new Config();
        $params     =   []; 
        parse_str($request->get('str'), $params);
        $tickets    =   Ticket::AdminsInfo($params['ti_cmd_id']);
        $iData   =   [
            'id'        =>  $params['ti_cmd_id'], 
            'ope'       =>  $app['session']->get('id'), 
            'ticket'    =>  $tickets['data']['ticket'],
            'asig'      =>  $params['ti_ejec'],
            'deadline'  =>  ($params['ti_dead'] <> '') ? $_replace->DateDeadline($params['ti_dead']) : '' ];

        switch ($params['ti_sta']) 
        {

            case '1':

                $info   =   'MODIFICACION DE STATUS - NO PROCEDE';
                $upd    =   Ticket::UpdNP($iData, $info);

                return $app->json(array(
                    'status'    =>  true,
                ));
                break;
            
            case '2':

                $info   =   'MODIFICACION DE STATUS - PROCESADO';
                $upd    =   Ticket::UpdP($iData, $info);

                return $app->json(array(
                    'status'    =>  true,
                ));
                break;

            case '3':

                if($params['ti_dead'] <> '')
                {
                    $info   =   'MODIFICACION DE STATUS - ASIGNADO - '.User::GetUsersID($params['ti_ejec'])['username'];
                    $upd    =   Ticket::UpdAsig($iData, $info);

                    return $app->json(array(
                        'status'    =>  true,
                    ));

                }else{

                    return $app->json(array(
                        'status'    =>  false,
                        'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> Debe un deadline Valido.! </strong> </div>'
                    ));

                }

                break;

            case '4':

                if($params['ti_dead'] <> '')
                {
                    $info   =   'MODIFICACION DE STATUS - RE ASIGNADO - '.User::GetUsersID($params['ti_ejec'])['username'];
                    $upd    =   Ticket::UpdRAsig($iData, $info);

                    return $app->json(array(
                        'status'    =>  true,
                    ));

                }else{
                    return $app->json(array(
                        'status'    =>  false,
                        'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> Debe un deadline Valido.! </strong> </div>'
                    ));

                }
                break;

            case '5':

                $info   =   'MODIFICACION DE STATUS - CANCELADO';
                $upd    =   Ticket::UpdCancel($iData, $info);

                return $app->json(array(
                    'status'    =>  true,
                ));
                break;

            case '':

                return $app->json(array(
                    'status'    =>  '',
                ));
                break;

        }

    }

////////////////////////////////////////////////////////////////////////////////////////

    public static function Dashboard(Application $app, Request $request)
    {
        return $app['twig']->render('ticket/dashboard/dashboard.html.twig',array(
            'sidebar'       =>  true
        ));

    }

    public static function DashboardLoad(Application $app, Request $request)
    {
        $ope        =   $app['session']->get('id');

        $tickets    =   Ticket::DashboardLoad($ope);

        return $app->json(array(
            'status'    =>  true,
            'pending'   =>  Ticket::DashboardPeningHTML($tickets['pending']),
            'process'   =>  Ticket::AdminLoadProcessHTML($tickets['process'], 'TableProcess'),
            'cancel'    =>  Ticket::AdminLoadProcessHTML($tickets['cancel'], 'TableCancel'),
        ));

    }

    public static function DashboardCommands(Application $app, Request $request)
    {
        $_replace   =   new Config();
        $params     =   []; 
        parse_str($request->get('str'), $params);
        $tickets    =   Ticket::AdminsInfo($params['ti_cmd_id']);

        $iData   =   [
            'id'        =>  $params['ti_cmd_id'], 
            'ope'       =>  $app['session']->get('id'), 
            'ticket'    =>  $tickets['data']['ticket'],
            'text'      =>  strtoupper($params['ti_cmd_text'])
        ];

        switch ($params['ti_sta']) 
        {

            case '1':
                $info   =   'MODIFICACION DE STATUS - NO PROCEDE - DASHBOARD';
                $upd    =   Ticket::UpdDashNP($iData, $info);

                return $app->json(array(
                    'status'    =>  true,
                ));
                break;
            
            case '2':
                if($iData['text'] <> '')
                {
                    $info   =   'MODIFICACION DE STATUS - PROCESADO - DASHBOARD';
                    $upd    =   Ticket::UpdDashP($iData, $info);

                    return $app->json(array(
                        'status'    =>  true,
                    ));
                    break;
                }else{
                    return $app->json(array(
                        'status'    =>  false,
                        'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> Debe dejar una Nota Valida.! </strong> </div>',
                    )); 
                }

            case '5':

                $info   =   'MODIFICACION DE STATUS - CANCELADO - DASHBOARD';
                $upd    =   Ticket::UpdDashCancel($iData, $info);

                return $app->json(array(
                    'status'    =>  true,
                ));
                break;

            case '':

                return $app->json(array(
                    'status'    =>  '',
                ));
                break;

        }
    
    }

  public static function GeneralInfo(Application $app, Request $request)
    {
        switch ($request->get('tp')) 
        {
            case 'sp':
                $table      =   "cp_ticket_support";
                break;
            case 'py':
                $table      =   "cp_ticket_payment";
                break;
            case 'da':
                $table      =   "cp_ticket_admin";
                break;
        }
        $tickets    =   Ticket::GeneralInfo($request->get('id'), $table);

        return $app->json(array(
            'status'    =>  true,
            'desc'      =>  $tickets['desc'],
            'info'      =>  $tickets['data']['information'],
            'resp'      =>  $tickets['data']['description'],
            'notes'     =>  Ticket::HtmlNotes($tickets['notes']),
            'log'       =>  Ticket::HtmlLog($tickets['logs']),
        ));

    }

////////////////////////////////////////////////////////////////////////////
////////////////////////////// TICKET SUPPORT //////////////////////////////
////////////////////////////////////////////////////////////////////////////

    public static function DashboardSup(Application $app, Request $request)
    {
        return $app['twig']->render('ticket/support/index.html.twig',array(
            'sidebar'       =>  true
        ));

    }

    public static function DashboardLoadSup(Application $app, Request $request)
    {
        $ope        =   $app['session']->get('id');

        $tickets    =   Ticket::DashboardGenLoad('cp_ticket_support', '1');

        return $app->json(array(
            'status'    =>  true,
            'pending'   =>  Ticket::DashboardPeningGenHTML($tickets['pending']),
            'process'   =>  Ticket::AdminLoadHTML($tickets['process'], 'TableProcess'),
            'cancel'    =>  Ticket::AdminLoadHTML($tickets['cancel'], 'TableCancel'),
            'statics'   =>  Ticket::HtmlStaticsGen($tickets['statics']),
            'gensta'    =>  $tickets['GenStatics'],
        ));

    }

    public static function DashboardSearcDateSup(Application $app, Request $request)
    {
        $_replace   =   new Config();
        $params     =   []; 
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];

        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            $tickets   =   Ticket::DashboardGenSD("1", $dates, "1", 'cp_ticket_support');
        }else{
            $tickets   =   Ticket::DashboardGenSD("0", $dates, "1", 'cp_ticket_support');
        }

        return $app->json(array(
            'status'    =>  true,
            'pending'   =>  Ticket::DashboardPeningGenHTML($tickets['pending']),
            'process'   =>  Ticket::AdminLoadHTML($tickets['process'], 'TableProcess'),
            'cancel'    =>  Ticket::AdminLoadHTML($tickets['cancel'], 'TableCancel'),
            'statics'   =>  Ticket::HtmlStaticsGen($tickets['statics']),
            'gensta'    =>  $tickets['GenStatics'],
        ));

    }

    public static function DashboardCommandsSup(Application $app, Request $request)
    {
        $_replace   =   new Config();
        $params     =   []; 
        parse_str($request->get('str'), $params);


        $tickets    =   Ticket::GeneralInfo($params['ti_cmd_id'], 'cp_ticket_support');

        $iData   =   [
            'id'        =>  $params['ti_cmd_id'], 
            'ope'       =>  $app['session']->get('id'), 
            'ticket'    =>  $tickets['data']['ticket'],
            'text'      =>  strtoupper($params['ti_cmd_text'])
        ];

        switch ($params['ti_sta']) 
        {

            case '1':
                $info   =   'MODIFICACION DE STATUS - NO PROCEDE - DASHBOARD';
                $upd    =   Ticket::UpdGenNP($iData, $info, 'cp_ticket_support');

                return $app->json(array(
                    'status'    =>  true,
                ));

                break;
            
            case '2':

                if($iData['text'] <> '')
                {
                    $info   =   'MODIFICACION DE STATUS - PROCESADO - DASHBOARD';
                    $upd    =   Ticket::UpdGenP($iData, $info, 'cp_ticket_support');

                    return $app->json(array(
                        'status'    =>  true,
                    ));
                    break;

                }else{
                    return $app->json(array(
                        'status'    =>  false,
                        'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> Debe dejar una Nota Valida.! </strong> </div>',
                    )); 
                }

            case '5':

                $info   =   'MODIFICACION DE STATUS - CANCELADO - DASHBOARD';
                $upd    =   Ticket::UpdGenCancel($iData, $info, 'cp_ticket_support');

                return $app->json(array(
                    'status'    =>  true,
                ));
                break;

            case '':

                return $app->json(array(
                    'status'    =>  '',
                ));
                break;

        }
    
    }
    
////////////////////////////////////////////////////////////////////////////
////////////////////////////// TICKET PAYMENT //////////////////////////////
////////////////////////////////////////////////////////////////////////////

    public static function DashboardPay(Application $app, Request $request)
    {
        return $app['twig']->render('ticket/payment/index.html.twig',array(
            'sidebar'       =>  true
        ));

    }

    public static function DashboardLoadPay(Application $app, Request $request)
    {
        $ope        =   $app['session']->get('id');

        $tickets    =   Ticket::DashboardGenLoad('cp_ticket_payment', '11');

        return $app->json(array(
            'status'    =>  true,
            'pending'   =>  Ticket::DashboardPeningGenHTML($tickets['pending']),
            'process'   =>  Ticket::AdminLoadHTML($tickets['process'], 'TableProcess'),
            'cancel'    =>  Ticket::AdminLoadHTML($tickets['cancel'], 'TableCancel'),
            'statics'   =>  Ticket::HtmlStaticsGen($tickets['statics']),
            'gensta'    =>  $tickets['GenStatics'],
        ));

    }

    public static function DashboardSearcDatePay(Application $app, Request $request)
    {
        $_replace   =   new Config();
        $params     =   []; 
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];

        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            $tickets   =   Ticket::DashboardGenSD("1", $dates, "11", 'cp_ticket_payment');
        }else{
            $tickets   =   Ticket::DashboardGenSD("0", $dates, "11", 'cp_ticket_payment');
        }

        return $app->json(array(
            'status'    =>  true,
            'pending'   =>  Ticket::DashboardPeningGenHTML($tickets['pending']),
            'process'   =>  Ticket::AdminLoadProcessHTML($tickets['process'], 'TableProcess'),
            'cancel'    =>  Ticket::AdminLoadProcessHTML($tickets['cancel'], 'TableCancel'),
            'statics'   =>  Ticket::HtmlStatics($tickets['statics']),
            'gensta'    =>  $tickets['GenStatics'],
        ));

    }

    public static function DashboardCommandsPay(Application $app, Request $request)
    {
        $_replace   =   new Config();
        $params     =   []; 
        parse_str($request->get('str'), $params);


        $tickets    =   Ticket::GeneralInfo($params['ti_cmd_id'], 'cp_ticket_payment');

        $iData   =   [
            'id'        =>  $params['ti_cmd_id'], 
            'ope'       =>  $app['session']->get('id'), 
            'ticket'    =>  $tickets['data']['ticket'],
            'text'      =>  strtoupper($params['ti_cmd_text'])
        ];

        switch ($params['ti_sta']) 
        {

            case '1':
                $info   =   'MODIFICACION DE STATUS - NO PROCEDE - DASHBOARD';
                $upd    =   Ticket::UpdGenNP($iData, $info, 'cp_ticket_payment');

                return $app->json(array(
                    'status'    =>  true,
                ));

                break;
            
            case '2':

                if($iData['text'] <> '')
                {
                    $info   =   'MODIFICACION DE STATUS - PROCESADO - DASHBOARD';
                    $upd    =   Ticket::UpdGenP($iData, $info, 'cp_ticket_payment');

                    return $app->json(array(
                        'status'    =>  true,
                    ));
                    break;

                }else{
                    return $app->json(array(
                        'status'    =>  false,
                        'content'   =>  '<div class="alert alert-info fade in" style="padding: 7px;"><i class="fa-fw fa fa-info"></i><strong> Debe dejar una Nota Valida.! </strong> </div>',
                    )); 
                }

            case '5':

                $info   =   'MODIFICACION DE STATUS - CANCELADO - DASHBOARD';
                $upd    =   Ticket::UpdGenCancel($iData, $info, 'cp_ticket_payment');

                return $app->json(array(
                    'status'    =>  true,
                ));
                break;

            case '':

                return $app->json(array(
                    'status'    =>  '',
                ));
                break;

        }
    
    }


}