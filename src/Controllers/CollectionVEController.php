<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SplFileObject;
use SplTempFileObject;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;

use App\Models\CollectionVE;
use App\Models\CollectionBS;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBPbx;
use App\Lib\DBSugar;
use App\Lib\DBSmart;
use App\Lib\DBMikroVE;


use App\Lib\ApiMWVz;

/**
 * Controller Collection VE
 */

class CollectionVEController extends BaseController
{

	public static function index(Application $app, Request $request)
	{
		return $app['twig']->render('collection/ve/index.html.twig',array(
            'sidebar'   =>  true,
            'date'		=>	date("m/d/Y")
        ));
	
	}

	public static function Load(Application $app, Request $request)
	{
        return $app->json(array(
            'status'    	=> 	true,
            'pending'		=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1")),
            'reporting'		=>	CollectionBS::ClientsReporttHtml(CollectionBS::ClientsReportsPay()),
            'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1")),
            'process'		=>	CollectionBS::ConsolidateProccessHtml(CollectionBS::SearchConsolidateStatus("2")),
            'codes'			=>	CollectionBS::HtmlCodeView(CollectionBS::SearchAllCodeView()),
        ));
	
	}

	public static function Consolide(Application $app, Request $request)
	{
		$iCode 		=	CollectionBS::SearchCodeStatus('1');
		$iClient 	=	CollectionBS::ClientsReportsPay();

		if(($iCode <> false) && ($iClient <> false))
		{
			$iData 	=	CollectionBS::ConsolideReporting($iCode, $iClient);

			if($iData <> false)
			{	

				$iInsert 	=	CollectionBS::InsertConsolidate($iData, $app['session']->get('id'));

				return $app->json(array(
					'status'    	=> 	true,
					'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1")),
					'pending'		=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1")),
					'codes'			=>	CollectionBS::HtmlCodeView(CollectionBS::SearchAllCodeView()),
					'title'			=>	"Success",
					'content'		=>	"Consolidacion completada con exito."
				));

			}else{

				return $app->json(array(
					'status'    => 	false,
			        'title'		=>	"Sin Consiliacion",
			        'content'	=>	"No se encontraron coincidencias para la consiliacion."
				));

			}


		}else{
			return $app->json(array(
				'status'    => 	false,
		        'title'		=>	"Error",
		        'content'	=>	"Sin informacion para consiliar"
			));

		}
	
	}	

	public static function DollarData(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$iData 	=	[
			'base'		=>	$params['dolar_base'],
			'agregado'	=>	$params['dolar_agregado'],
			'total'		=>	($params['dolar_base'] + $params['dolar_agregado']),
			'type'		=>	$params['dolar_tipo'],
			'date'		=>	$params['dolar_date'],
			'operator'	=>	$app['session']->get('id')
		];

		$iDolar 	=	CollectionBS::InsertDollar($iData);

		if($iDolar == true)
        { 
            $info = array('client' => '', 'channel' => 'Dolar Cotizador', 'message' => 'Cotizacion - Satisfactoria - Realizado por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   	=> 	true,
                'html'		=>	CollectionBS::DolarTable(CollectionBS::SearchtDollar())
            ));

        }else{

            $info = array('client' =>  '', 'channel' => 'Dolar Cotizador', 'message' => 'Cotizacion - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false,
                'html'		=>	CollectionBS::DolarTable(CollectionBS::SearchtDollar())
            ));
        
        }

	}

	public static function DollarView(Application $app, Request $request)
	{
            return $app->json(array(
                'status'    => true,
                'html'		=>	CollectionBS::DolarTable(CollectionBS::SearchtDollar())
            ));
	
	}

	public static function UploadFile(Application $app, Request $request)
	{

		$file 	=	CollectionBS::SaverUploadFiles($_FILES, $app['session']->get('id'));

		if($file == true)
		{

			$info = array('client' => '', 'channel' => 'CollectionVE - Pre Upload File', 'message' => 'Carga de Archivo Proceado Correctamente - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status' 	=> 	true,
				'html'		=>	CollectionBS::UploadFilesHtml($file),
				'pending'	=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1"))
			));

		}else{

			$info = array('client' => '', 'channel' => 'CollectionVE - Pre Upload File - Error', 'message' => 'Error al Intentar Cargar Archivo - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status' 	=> 	false,
				'title'		=>	'Error',
				'content'	=> 	'Error al Intentar cargar el archivo, intenta nuevamente'
			));
		}
	
	}

	public static function ProcessCodeTemp(Application $app, Request $request)
	{
		$temp 	= 	CollectionBS::SearchCodesTmp();

		if($temp <> false)
		{

			foreach ($temp as $t => $tmp) 
			{
				$code 	=	CollectionBS::SearchCodeConsolidate($tmp['code']);

				if($code == false)
				{
					$cIdata 	=	CollectionBS::InsertCodeConsolidate($tmp, $app['session']->get('id'));
					
					$res[$t]	=	['Transaccion' => $tmp['code'], 'fecha' => $tmp['fecha'], 'amount' => $tmp['amount'], 'status' => "NEW"];

					$cTmp 		=	CollectionBS::InsertTmpPayment($res[$t]);

				}else{

					$res[$t]	=	['Transaccion' => $tmp['code'], 'fecha' => $tmp['fecha'], 'amount' => $tmp['amount'], 'status' => "DUPLICATE"];

					$cTmp 		=	CollectionBS::InsertTmpPayment($res[$t]);
				}

			}

			$process 	=	CollectionBS::SearchAllConsolidateStatus("1");
			$pending 	=	CollectionBS::SearchAllConsolidateStatus("0");

			$info = array('client' => '', 'channel' => 'CollectionVE - Upload File - Correcto', 'message' => 'Almacenamiento de Codigos - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	true,
				'title'		=>	'Success',
				'content'	=> 	'Procesado Correctamente',
				'cHtml'		=>	CollectionBS::ConsolidateHtml($process),
				'pHtml'		=>	CollectionBS::ConsolidateHtmlPend($pending)
			));

		}else{

			$info = array('client' => '', 'channel' => 'CollectionVE - Upload File - Error', 'message' => 'Almacenamiento de Codigos - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array( 
				'status' 	=> 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Datos Para Procesar'
			));
		
		}

	}

	public static function BatchSuspendCode(Application $app, Request $request)
	{
		$params =	[];
		parse_str($request->get('str'), $params);

		$cGen 	=	$cont 	= 	$coP 	=	$coE 	=	0;

		$iCou 	=	CollectionBS::SearchCodeStatus("1");

		if($iCou <> false)
		{
			foreach ($iCou as $c => $cal) 
			{
				$var 	=	'LotPeng'.$cal['id'].'';

				if(isset($params[$var]) == "on")
				{
					$ident[$c] 	= 	$cal['id'];
					$cGen 		= 	$cGen+1;
				}
			}

			if($cGen == 0)
			{

				return $app->json(array(
					'status'    => 	false,
					'title'		=>	'Error',
					'content'	=> 	'Debe seleccionar al menos una opcion de la lista.',
				));

			}else{

				foreach ($ident as $i => $id) 
				{

					$LotUpPen 	=	CollectionBS::UpdateCodeLotStatus($id, $app['session']->get('id'), '3');

					if($LotUpPen ==  true)
					{
						$coP = $coP + 1;
					}else{
						$coE = $coE + 1;
					}
				}

				$info = array('client' => '', 'channel' => 'CollectionBS - Suspencion Codigo - Correcto', 'message' => 'Suspencion de Codigo Correcto - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);
				
				return $app->json(array(
					'status'    => 	true,
					'html'		=>	'<div class="alert alert-info fade in">Exitoso: '.$coE.' | Error: '.$coP.'</div>',
					'pending'	=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1"))
				));

			}

		}else{

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Informaci&oacute;n Para Suspender',
			));

		}
	
	}

	public static function EditConsolide(Application $app, Request $request)
	{
		var_dump($request->get('id'));
		exit;

	}

	public static function ProcessConsolidate(Application $app, Request $request)
	{
		$_config 	=	new Config();

		$iData 		=	CollectionBS::SearchConsolidateId($request->get('id'));

		$code 		=	CollectionBS::SearchAllCode($request->get('id'));

		$mkw 		=	$_config->ApiMikroVE();

		$url 		=	$mkw['url'].'/PaidInvoice';

		$fields 	=	[
			'token'         =>  $mkw['token'],
            'idfactura'     =>  $iData['nfactura'],
            'pasarela'      =>  "Pago Electronico - BS/USD",
            'cantidad'      =>  $iData['dtotal'],
            'comision'      =>  "0",
            'idtransaccion' =>  $iData['ftransaccion'],
            'fecha'         =>  $iData['fdate']
		];

		$payment 	=	ApiMWVz::Curl($fields, $url);

        if($payment['estado'] == "exito")
        {
        	$iDat 		=	['id' => $request->get('id'), 'ope'	=> $app['session']->get('id') ];
        	$update 	=	CollectionBS::UpdateConsolidate($iDat);

        	$updateMW 	=	CollectionBS::UpdateReportMW($iData['nfactura']);

	       	foreach ($code as $c => $co) 
			{
	        	$nData 		=	[
	        		'cliente'	=>	$iData['idcliente'],
	        		'client'	=>	$iData['idcliente'],
	        		'code' 		=> 	$co['code'],
	        		'total' 	=> 	$iData['dtotal'],
	        		'fecha' 	=> 	$iData['cdate'],
	        		'factura' 	=> 	$iData['nfactura'],
	        		'dolar' 	=> 	$co['dolar'],
	        		'user' 		=>	$app['session']->get('username') 
	        	];

	        	$note 		=	CollectionBS::NoteMikrowisp($nData, "2");
			}

			$info = array('client' => $nData['cliente'], 'channel' => 'CollectionBS - Procesamiento de Conciliacion - Correcto', 'message' => 'Proceso de Conciliacion - Factura '.$nData['factura'].' - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    	=> 	true,
				'title'			=>	'Success',
				'content'		=> 	'Pago Realizado Correctamente',
            	'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1")),
            	'process'		=>	CollectionBS::ConsolidateProccessHtml(CollectionBS::SearchConsolidateStatus("2")),

			));

		}else{

			$info = array('client' => '', 'channel' => 'CollectionBS - Procesamiento de Conciliacion - Error', 'message' => 'Proceso de Conciliacion - Factura '.$iData['nfactura'].' - Solicitado Por - '.$app['session']->get('username').' - Errado '.$payment['mensaje'].'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	$payment['mensaje'],
			));
		
		}

	}

	public static function MultipleConsolidate(Application $app, Request $request)
	{
		$_config 	=	new Config();

		$params 	=	[];

		parse_str($request->get('str'), $params);

		$cGen 		=	$cont 	= 	$coP 	=	$coE 	=	0;

		$iCou 		=	CollectionBS::SearchConsolidateStatus("1");

		if($iCou <> false)
		{

			foreach ($iCou as $c => $cal) 
			{
				$var 	=	'ConPending'.$cal['id'].'';

				if(isset($params[$var]) == "on")
				{
					$ident[$c] 	= 	$cal['id'];
					$cGen 		= 	$cGen+1;
				}
			}

			if($cGen == 0)
			{
				return $app->json(array(
					'status'    => 	false,
					'title'		=>	'Error',
					'content'	=> 	'Debe seleccionar al menos una opcion de la lista.',
				));

			}else{

				$mkw 		=	$_config->ApiMikroVE();

				$url 		=	$mkw['url'].'/PaidInvoice';

				foreach ($ident as $i => $id) 
				{
					$iData 		=	CollectionBS::SearchConsolidateId($id);

					$fields 	=	[
						'token'         =>  $mkw['token'],
			            'idfactura'     =>  $iData['nfactura'],
			            'pasarela'      =>  "Pago Electronico - BS/USD",
			            'cantidad'      =>  $iData['dtotal'],
			            'comision'      =>  "0",
			            'idtransaccion' =>  $iData['ftransaccion'],
			            'fecha'         =>  $iData['fdate']
					
					];

					$payment 	=	ApiMWVz::Curl($fields, $url);

			        if($payment['estado'] == "exito")
			        {
			        	$iDat 		=	['id' => $id, 'ope'	=> $app['session']->get('id') ];

			        	$update 	=	CollectionBS::UpdateConsolidate($iDat);

			        	$updateMW 	=	CollectionBS::UpdateReportMW($iData['nfactura']);

						$code 		=	CollectionBS::SearchAllCode($id);

						foreach ($code as $c => $co) 
						{
				        	$nData 		=	[
				        		'cliente'	=>	$iData['idcliente'],
				        		'client'	=>	$iData['idcliente'],
				        		'code' 		=> 	$co['code'],
				        		'total' 	=> 	$iData['dtotal'],
				        		'fecha' 	=> 	$iData['cdate'],
				        		'factura' 	=> 	$iData['nfactura'],
				        		'dolar' 	=> 	$co['dolar'],
				        		'user' 		=>	$app['session']->get('username') 
				        	];

				        	$note 		=	CollectionBS::NoteMikrowisp($nData, "2");
						}			        	


						$info = array('client' => $nData['cliente'], 'channel' => 'CollectionBS - Procesamiento de Conciliacion - Correcto', 'message' => 'Proceso de Conciliacion - Factura '.$nData['factura'].' - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

				        $app['datalogger']->RecordLogger($info);

				        $coP 		=	$coP + 1;

					}else{

						$info = array('client' => '', 'channel' => 'CollectionBS - Procesamiento de Conciliacion - Error', 'message' => 'Proceso de Conciliacion - Factura '.$iData['nfactura'].' - Solicitado Por - '.$app['session']->get('username').' - Errado '.$payment['mensaje'].'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

				        $app['datalogger']->RecordLogger($info);

						$coE 		=	$coE + 1;
					
					}
					
				}

				return $app->json(array(
					'status'    	=> 	true,
					'html'			=>	'<div class="alert alert-info fade in">Exitoso: '.$coP.' | Error: '.$coE.'</div>',
            		'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1")),
            		'process'		=>	CollectionBS::ConsolidateProccessHtml(CollectionBS::SearchConsolidateStatus("2")),
				));

			}

		}else{
			
			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Informaci&oacute;n Para Consolidar',
			));

		}

	}

	public static function SuspendConsolideMultiple(Application $app, Request $request)
	{
		$_config 	=	new Config();

		$params 	=	[];

		parse_str($request->get('str'), $params);

		$cGen 		=	$cont 	= 	$coP 	=	$coE 	=	0;

		$iCou 		=	CollectionBS::SearchCodeStatus("1");
		
		if($iCou <> false)
		{

			foreach ($iCou as $c => $cal) 
			{
				$var 	=	'ConPending'.$cal['id'].'';

				if(isset($params[$var]) == "on")
				{
					$ident[$c] 	= 	$cal['id'];
					$cGen 		= 	$cGen+1;
				}
			}

			if($cGen == 0)
			{
				return $app->json(array(
					'status'    => 	false,
					'title'		=>	'Error',
					'content'	=> 	'Debe seleccionar al menos una opcion de la lista.',
				));

			}else{

				foreach ($ident as $i => $id) 
				{

					$iDat 		=	['id' => $id, 'ope'	=> $app['session']->get('id') ];

					$LotUpPen 	=	CollectionBS::UpdConsolidateSuspend($iDat);

					if($LotUpPen ==  true)
					{
						$coP = $coP + 1;
					}else{
						$coE = $coE + 1;
					}
				}

				$info = array('client' => '', 'channel' => 'CollectionBS - Suspencion Consolidacion - Correcto', 'message' => 'Suspencion de Condolidacion Correcto - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);
				
				return $app->json(array(
					'status'    	=> 	true,
					'html'			=>	'<div class="alert alert-info fade in">Exitoso: '.$coP.' | Error: '.$coE.'</div>',
					'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1"))
				));

			}

		}else{
			
			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Informaci&oacute;n Para Suspender',
			));

		}

	}

	public static function ManualCode(Application $app, Request $request)
	{
		$_config 	=	new Config();

		$params 	=	[];

		parse_str($request->get('str'), $params);

		$iData 		=	['date' => $_config->DateDeadline($params['mc_date']), 'code' => $params['mc_code'], 'amount' => $params['mc_amount'], 'ope' => $app['session']->get('id')  ];

		$insert 	=	CollectionBS::InsertCodeManual($iData);

		if($insert <> false)
		{
			$info = array('client' => '', 'channel' => 'CollectionBS - Registro Codigo Manual - Correcto', 'message' => 'Registro De Codigo Manual - Codigo '.$iData['code'].' - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

	    	return $app->json(array(
				'status'    => 	true,
				'title'		=>	'Success',
				'content'	=>	'Solicitud Procesada correctamente',
				'pending'	=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1")),
			));

		}else{

	    	return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=>	'Error almacenando informacion, intente de nuevo',
			));

		}

	}

	public static function ClientReporting(Application $app, Request $request)
	{
        return $app->json(array(
            'status'    	=> 	true,
            'reporting'		=>	CollectionBS::ClientsReporttHtml(CollectionBS::ClientsReportsPay()),
        ));

	}

	public static function AnexConsolidateView(Application $app, Request $request)
	{
        return $app->json(array(
            'status'    => 	true,
            'anex'		=>	CollectionBS::AnexCodeHtml(CollectionBS::SearchCodeStatus("1")),
        ));

	}

	public static function AsigCodePending(Application $app, Request $request)
	{
		$code 	=	CollectionBS::SelectCodeLotStatusId($request->get('info'));

        $iTime  =   ['date' =>  substr($code['created_at'],0, 10), 'type'   =>  CollectionBS::CheckTime(substr($code['created_at'], -8)) ];
        $dolar  =   CollectionBS::SearchDollarDateTime($iTime);

        $iData 	=	[
        	'code'				=>	$code['code'],
        	'amount'			=>	$code['amount'],
        	'cdate'				=>	$code['created_at'],
        	'dolar'				=>	$dolar['total'],
        	'dolartime'			=>	$dolar['type'],
        	'dtotal'			=>	str_replace(",","",number_format(round(($code['amount'] / $dolar['total']),2), 2)),
        	'consolidate_id'	=>	$request->get('id'),
        	'created_by'		=>	$app['session']->get('id'),
        ];

        $insert =	CollectionBS::InsertAddCode($iData);

        if($insert <> false)
        {
        	$LotUpPen 	=	CollectionBS::UpdateCodeLotStatus($request->get('info'), $app['session']->get('id'), '2');

        	$codes 		=	CollectionBS::SearchAddCode($request->get('id'));

        	$total		= 	0;

        	foreach ($codes as $c => $cod) 
        	{
        		$total 	=	($cod['dtotal'] + $total);
        	}

        	$iD 	=	['id' => $request->get('id'), 'total' => $total];

        	$update 	=	CollectionBS::UpdConsolidateTotal($iD);

	        return $app->json(array(
	            'status'    	=> 	true,
	            'id'			=>	$request->get('info'),
	            'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1")),
	            'pending'		=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1")),
	            'title'			=>	'Success',
	            'content'		=>	'Codido Anexado Correctamente'
	        ));

        }else{

			return $app->json(array(
	            'status'    	=> 	false,
	            'title'			=>	'Error',
	            'content'		=>	'Error Intente Nuevamente'
	        ));

        }
	}

	public static function ViewCodes(Application $app, Request $request)
	{

        return $app->json(array(
            'status'    => 	true,
          	'html'		=>	CollectionBS::ViewCodeHtml(CollectionBS::SearchAllCode($request->get('id')))
        ));

	}

	public static function AsigManualClient(Application $app, Request $request)
	{
		
		$check 	=	CollectionBS::SearchConsolidateClient($request->get('id'));

		if($check == false)
		{
			return $app->json(array(
	            'status'    	=> 	true,
	            'html'			=>	CollectionBS::AnexCodeClientHtml(CollectionBS::SearchCodeStatus("1")),

	        ));

		}else{
			return $app->json(array(
	            'status'    	=> 	false,
	            'title'			=>	'Error',
	            'content'		=>	'Cliente presenta una consolidacion pendiente.'
	        ));

		}

	}

	public static function ClientCodeAsig(Application $app, Request $request)
	{

		$code 	=	CollectionBS::SelectCodeLotStatusId($request->get('id'));
		$client =	CollectionBS::ClientsPay($request->get('cl'));

        $iTime  =   ['date' =>  substr($code['created_at'],0, 10), 'type'   =>  CollectionBS::CheckTime(substr($code['created_at'], -8)) ];
        $dolar  =   CollectionBS::SearchDollarDateTime($iTime);

        $iTotal =   [
            'id'        =>  $client['id'], 
            'client'    =>  $client['idcliente'],
            'asunto'    =>  $client['asunto'],
            'name'      =>  CollectionBS::GetClientsData($client['idcliente'])['nombre'],
            'fdate'     =>  $client['fReporte'],
            'dtran'     =>  $code['code'],
            'ctran'     =>  $client['transaccion'],
            'factura'   =>  $client['nfactura'],
            'total'     =>  $client['total'],
            'amount'    =>  $code['amount'],
            'status'    =>  "1",
            'dolar'     =>  $dolar['total'],
            'dolartype' =>  $dolar['type'],
            'tpay'      =>  number_format(round(($code['amount'] / $dolar['total']),2), 2)
        ];

		$iInsert 	=	CollectionBS::InsertConsolidateSingle($iTotal, $app['session']->get('id'));

		if($iInsert <> false)
		{
			return $app->json(array(
				'status'    	=> 	true,
				'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1")),
				'pending'		=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1")),
				'title'			=>	"Success",
				'content'		=>	"Consolidacion completada con exito."
			));

		}else{

			return $app->json(array(
				'status'    	=> 	false,
				'title'			=>	"Error",
				'content'		=>	"Error al Conciliar, Intente Nuevamente"
			));

		}

	}

	public static function ViewEditCodeAsig(Application $app, Request $request)
	{
		return $app->json(array(
			'status'    	=> 	true,
			'code'			=>	CollectionBS::ViewCodeAid($request->get('id')),
		));

	}

	public static function StoreEditCodeAsig(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$code 	=	CollectionBS::ViewCodeAid($params['codeid']);

		$iData 	=	[
			'id'		=>	$code['id'],
			'dolar'		=>	$params['ec_dolar'],
			'dtotal'	=>	number_format(round(($code['amount'] / $params['ec_dolar']),2), 2)
		];

		$update 	=	CollectionBS::UpdateCodeAsig($iData);

		if($update <> false)
		{
			$iTo 	=	CollectionBS::SearchAllCode($code['consolidate_id']);

			$total 	=	0;

			foreach ($iTo as $t => $to) 
			{
				$total 	=	($to['dtotal'] + $total);
			}
			$iTotal 	=	['id' => $code['consolidate_id'], 'total' => $total];

			$updateCon 	=	CollectionBS::UpdConsolidateTotal($iTotal);

			return $app->json(array(
				'status'    	=> 	true,
				'consolidate'	=>	CollectionBS::ConsolidatePendingHtml(CollectionBS::SearchConsolidateStatus("1")),
				'pending'		=>	CollectionBS::PendingCodeHtml(CollectionBS::SearchCodeStatus("1")),
				'codes'			=>	CollectionBS::HtmlCodeView(CollectionBS::SearchAllCodeView()),
			));

		}else{
			return $app->json(array(
				'status'    	=> 	false,
				'title'			=>	"Error",
				'content'		=>	"Error al intentar actualizar el valor de referencia."
			));
		}


	}
	


}