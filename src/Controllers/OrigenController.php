<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\Medium;
use App\Models\Objetive;
use App\Models\Origen;
use App\Models\Conversation;
use App\Models\ServiceM;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class OrigenController extends BaseController
{

    public static function OrigenEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'origen'    => Origen::GetOrigenById($request->get('id'))
        ));
    
    }

    public function OrigenForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_org'] == 'new')
        {
            $saveOrigen = Origen::SaveOrigen($params);
            
            if($saveOrigen <> false)
            { 
                $info = array('client' => '', 'channel' => 'Nuevo Origen', 'message' => 'Creacion de Origen MKT - '.strtoupper($params['name_org']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'medium'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Nuevo Origen', 'message' => 'Error Al Intentar Crear Origen MKT - '.strtoupper($params['name_org']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_org'] == 'edit')
        {

            $saveOrigen = Origen::SaveOrigen($params);
            
            if($saveOrigen <> false)
            {
                $info = array('client' => '', 'channel' => 'Edicion de Origen', 'message' => 'Actualizacion de Origen MKT - '.strtoupper($params['name_org']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'medium'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Edicion de Origen', 'message' => 'Error Al Intentar Actualizar Informacion del Origen MKT - '.strtoupper($params['name_org']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, Error Al Intentar Actualizar Informacion del Origen MKT, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
                            
        }
    
    }

}