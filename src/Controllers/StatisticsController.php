<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SplFileObject;
use SplTempFileObject;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;

use Carbon\Carbon;

use App\Lib\DBSmart;
use App\Lib\Config;
use App\Models\Statics;
use App\Models\User;

/**
 * 
 */
class StatisticsController extends BaseController	
{
	
	public static function index (Application $app, Request $request)
	{
		$oType 		=	OrderType::GetOrderTypeStatus();
		ddd($oType);

		$ope 		=	"33";

		$iServ[2]	=	'cp_service_pr_tv';
		$iServ[3]	=	'cp_service_pr_sec';
		$iServ[4]	=	'cp_service_pr_int';
		$iServ[6]	=	'cp_service_vzla_int';
		$aServ 		=	[2, 3, 4, 6];
		$tScore[2] 	=	[1, 2, 3];
		$tScore[3] 	=	[11];
		$tScore[4] 	=	[22];
		$tScore[6] 	=	[29];
		$tic 		=	$sc 	= 	$sn  = 	"";
		$between 	=	'created_at BETWEEN "2020-07-01 00:00:00" AND NOW()';


		foreach ($aServ as $a => $as) 
		{

			$query 		=	'SELECT COUNT(*) AS count FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$between.'';
			$sTotal 	=	DBSmart::DBQuery($query)['count'];

			$query 		=	'SELECT ticket FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$between.'';
			$sTicket 	=	DBSmart::DBQueryAll($query);

			if($sTicket <> false)
			{

				$tic.='( ';
				foreach ($sTicket as $s => $ti) { 
					$tic.='(ticket = "'.$ti['ticket'].'") OR '; 
				}
				$tic 	=	substr($tic, 0, -4).' )';

				foreach ($oType as $o => $ot) 
				{
					$query		=	'SELECT COUNT(*) AS count FROM '.$iServ[$as].' WHERE operator_id = "'.$ope.'" AND type_order_id = "'.$ot['id'].'" AND '.$tic.' AND '.$between.'';

					$t 			= 	DBSmart::DBQuery($query);
					$oTG[$o]	=	$t['count'];

				}

				$query 		=	'SELECT * FROM cp_coord_serv WHERE '.$tic.'';
				$tTC[$a]	=	DBSmart::DBQueryAll($query);
			
			}else{
				$oTG[0] = $oTG[1] = $oTG[2] = $oTG[3] = $oTG[4] = $oTG[5] = $oTG[6] = $oTG[7] =	0;
			}


			$tSub 	=	($oTG[0] + $oTG[1] + $oTG[2] + $oTG[3] + $oTG[4] + $oTG[6]);

			$tCierres[$a] 	=	[
				'Operador'				=>	'ANABEL.SL305',
				''.$iServ[$as].''		=> 	$sTotal,
				''.$oType[0]['name'].''	=>	$oTG[0],
				''.$oType[1]['name'].''	=>	$oTG[1],
				''.$oType[2]['name'].''	=>	$oTG[2],
				''.$oType[3]['name'].''	=>	$oTG[3],
				''.$oType[4]['name'].''	=>	$oTG[4],
				''.$oType[5]['name'].''	=>	$oTG[5],
				''.$oType[6]['name'].''	=>	$oTG[6],
				''.$oType[7]['name'].''	=>	$oTG[7],
				'tEfect'				=>	$tSub,
				'Efectividad'			=>	($tSub <> 0 ) ? number_format( (($tSub * 100) / $sTotal),2 ).' %' : '0 %'
			];
			

			$sc.='( ';
			$sn.='( ';

			foreach ($tScore[$as] as $ts) 
			{
				$sc.='(score = '.$ts.') OR ';
				$sn.='(score != '.$ts.') AND ';
			
			}

			$sc 	=	substr($sc, 0, -4).' )';
			$sn 	=	substr($sn, 0, -5).' )';

			$query 	=	'SELECT
				(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$between.' ) AS cTotal,
				(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$sc.' AND '.$between.') AS aTotal,
				(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$sn.' AND '.$between.') AS other';

			$iApro[$a] 	=	DBSmart::DBQuery($query);

			$tic = $sc = $sn = "";

			// foreach ($tTC as $t => $tC) 
			// {

			// 	ddd($tC);
			// }


			$tProcentaje[$a]	= [
				'Efec_Cierres'	=>	$tCierres[$a]['Efectividad'],
				'Efec_AproTV'	=>	($iApro[$a]['aTotal'] <> 0) ? number_format((($iApro[$a]['aTotal'] * 100) / $tCierres[$a]['tEfect']),2)."%" : "0%",
			];

		}

		// ddd($iApro, $tProcentaje);

		// ddd("Termino");
		
		ddd($tCierres, $iApro, $tProcentaje, $tTC);


		foreach ($aServ as $a => $as) 
		{
			$sc.='( ';
			$sn.='( ';

			foreach ($tScore[$as] as $ts) 
			{
				$sc.='(score = '.$ts.') OR ';
				$sn.='(score != '.$ts.') AND ';
			}

			$sc 	=	substr($sc, 0, -4).' )';
			$sn 	=	substr($sn, 0, -5).' )';

			$query 	=	'SELECT
			(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$between.' ) AS cTotal,
			(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$sc.' AND '.$between.') AS aTotal,
			(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$sn.' AND '.$between.') AS other
			';

			$info[$a] 	=	DBSmart::DBQuery($query);

			// d($sc, $sn);
			$sc = $sn = "";
		}









		foreach ($aServ as $i => $iS) 
		{

			$query 		=	'SELECT COUNT(*) AS count FROM cp_appro_serv WHERE operator_id = "'.$ope.'" AND '.$between.'';
			$sTotal 	=	DBSmart::DBQuery($query)['count'];
			
			foreach ($oType as $o => $oT) 
			{
				$query 	=	'SELECT COUNT(*) AS count FROM '.$iS.' WHERE operator_id = "'.$ope.'" AND type_order_id = "'.$oT['id'].'" AND '.$between.'';
				$t 		= 	DBSmart::DBQuery($query);

				$oTG[$o] =	$t['count'];

			}

			$tSub 	=	($oTG[0] + $oTG[1] + $oTG[2] + $oTG[3] + $oTG[4] + $oTG[6]);

			$tCierres[$i] 	=	[
				'Operador'				=>	'LAIONAL.SL328',
				''.$iS.''				=> 	$sTotal,
				''.$oType[0]['name'].''	=>	$oTG[0],
				''.$oType[1]['name'].''	=>	$oTG[1],
				''.$oType[2]['name'].''	=>	$oTG[2],
				''.$oType[3]['name'].''	=>	$oTG[3],
				''.$oType[4]['name'].''	=>	$oTG[4],
				''.$oType[5]['name'].''	=>	$oTG[5],
				''.$oType[6]['name'].''	=>	$oTG[6],
				''.$oType[7]['name'].''	=>	$oTG[7],
				'tEfect'				=>	$tSub,
				'Efectividad'			=>	($tSub <> 0 ) ? number_format( (($tSub * 100) / $sTotal),2 ).' %' : '0 %'
			];

		}

		foreach ($aServ as $a => $as) 
		{
			$sc.='( ';
			$sn.='( ';

			foreach ($tScore[$as] as $ts) 
			{
				$sc.='(score = '.$ts.') OR ';
				$sn.='(score != '.$ts.') AND ';
			}

			$sc 	=	substr($sc, 0, -4).' )';
			$sn 	=	substr($sn, 0, -5).' )';

			$query 	=	'SELECT
			(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$between.' ) AS cTotal,
			(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$sc.' AND '.$between.') AS aTotal,
			(SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND service_id = "'.$as.'" AND '.$sn.' AND '.$between.') AS other
			';

			$info[$a] 	=	DBSmart::DBQuery($query);

			// d($sc, $sn);
			$sc = $sn = "";
		}
		

		$query 	=	'SELECT (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND '.$between.' AND service_id = "2") AS cTotal, (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND (score = "1" OR score = "2" OR score = "3") AND '.$between.' AND service_id = "2") AS aTotal, (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND score = "1" AND '.$between.' AND service_id = "2") AS score1, (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND score = "2" AND '.$between.' AND service_id = "2") AS score2, (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND score = "3" AND '.$between.' AND service_id = "2") AS score3, (SELECT COUNT(*) FROM cp_appro_serv WHERE created_by = "'.$ope.'" AND (score != "1" AND score != "2" AND score != "3") AND '.$between.' AND service_id = "2") AS otras';
		$taRes 	=	DBSmart::DBQuery($query);

		$tApro	= [
			'Efec_Cierres'		=>	$tCierres[0]['Efectividad'],
			'Efec_Aprobadas'	=>	number_format((($taRes['aTotal'] * 100) / $tCierres[0]['tEfect']),2)
		];

		ddd($tCierres, $info);

		$pSub 	=	number_format( (($tSub * 100) / $tvTotal),2 ).' %';


		ddd($tvTotal, $oTG, $total, $pSub);
	
	}

	public static function AproServ(Application $app, Request $request)
	{
		$service	=	Service::GetService();
		ddd($service);
		$between 	=	'created_at BETWEEN "2020-07-01 00:00:00" AND NOW()';
		$cont 		=	$cont2 	=	1;

		foreach ($service as $s => $se) 
		{
			if($se['status_id'] == 1)
			{
				$query 	=	'SELECT COUNT(1) AS T FROM (SELECT * FROM cp_appro_serv WHERE service_id = "'.$se['id'].'" AND '.$between.' GROUP BY client_id HAVING COUNT(1) > 1 ) t';
				$ser	=	DBSmart::DBQuery($query);

				$tServ[$s]	=	['Servicio' => $se['name'], 'TOTAL' => $ser['T'] ];
				$query 	=	'SELECT client_id FROM (SELECT * FROM cp_appro_serv WHERE service_id = "'.$se['id'].'" AND '.$between.' GROUP BY client_id HAVING COUNT(1) > 1 ) t';
				$cli 	= 	DBSmart::DBQueryAll($query);

				if($cli <> false)
				{
					foreach ($cli as $c => $cl) 
					{
						$query 	=	'SELECT COUNT(*) AS T FROM cp_appro_serv WHERE client_id = "'.$cl['client_id'].'" AND service_id = "'.$se['id'].'" AND '.$between.'';
						$clie 	=	DBSmart::DBQuery($query);

						$iClie[$cont++] 	= 	[
							'Servicio'	=>	$se['name'], 
							'Cliente' 	=>	$cl['client_id'], 
							'TOTAL'		=>	$clie['T'] 
						];

						$query 	=	'SELECT ticket AS T FROM cp_appro_serv WHERE client_id = "'.$cl['client_id'].'" AND service_id = "'.$se['id'].'" AND '.$between.'';
						$ticket	=	DBSmart::DBQueryAll($query);

						foreach ($ticket as $t => $ti) 
						{
							$query 	= 	'SELECT t1.client_id, t1.ticket, t2.name, t2.phone_main, (SELECT name FROM data_score WHERE id = t1.score) AS score, t3.username AS Sale, t4.name AS Sale_Dep, t1.created_at AS Sale_Date, t5.username AS Operator, t6.name AS Operator_Dep, t1.finish_at AS Operator_Date, t1.status_id, t1.cancel, t1.cancel FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) INNER JOIN users AS t3 ON (t1.created_by = t3.id) INNER JOIN data_departament AS t4 ON (t1.created_dep_id = t4.id) INNER JOIN users AS t5 ON (t1.finish_by = t5.id) INNER JOIN data_departament AS t6 ON (t1.finish_dep_id = t6.id) AND t1.ticket = "'.$ti['T'].'"';
							$tick 	=	DBSmart::DBQuery($query);

							if( ($tick['status_id'] == "1")	 	AND ($tick['cancel'] == "1") )
							{	$status 	=	"PENDIENTE"; 
								$ope 		=	""; 	$ope_dep = "";	
							}
							elseif( ($tick['status_id'] == "2")	AND ($tick['cancel'] == "1") )
							{	$status 	=	"PROCESADO"; 
								$ope 		=	$tick['Operator']; $ope_dep = $tick['Operator_Dep'];
							}
							elseif( ($tick['status_id'] == "0") AND	($tick['cancel'] == "0") )
							{	$status 	=	"CANCELADO";
								$ope 		=	$tick['Operator']; $ope_dep = $tick['Operator_Dep'];
							}
							else
							{ 
								$status 	=	"NINGUNO"; 
								$ope 		=	""; 	$ope_dep = "";	
							}

							$iTicket[$cont2++] 	=	[
								'Cliente'		=>	$tick['client_id'],
								'Ticket'		=>	$tick['ticket'],
								'Nombre'		=>	$tick['name'],
								'Telefono'		=>	$tick['phone_main'],
								'Score'			=>	$tick['score'],
								'Vendedor'		=>	$tick['Sale'],
								'VendeorDep'	=>	$tick['Sale_Dep'],
								'FechaVenta'	=>	$tick['Sale_Date'],
								'Status'		=>	$status,
								'Operador'		=>	$ope,
								'OperadorDep'	=>	$ope_dep,
								'OperadorVenta'	=>	$tick['Operator_Date'],
							];
							$status 	=	"";
						}
					}
				}

			}
		}

				ddd($tServ, $iClie, $iTicket);

		$service[0] 	=	['id' 	=> 	'2', 'name'	=>	'TELEVISION PR'];
		$service[1] 	=	['id' 	=> 	'3', 'name'	=>	'TELEVISION PR'];
		$service[2] 	=	['id' 	=> 	'4', 'name'	=>	'TELEVISION PR'];
		$service[3] 	=	['id' 	=> 	'6', 'name'	=>	'TELEVISION PR'];


		ddd("Approvals Statistcs");
	
	}

	public static function DataEntry(Application $app, Request $request)
	{
        return $app['twig']->render('statics/dataentry/index.html.twig',array(
            'sidebar'       =>  true,
        ));
	
	}

	public static function DataEntryLoad(Application $app, Request $request)
	{

		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		if(isset($params['dateIni']) <> false)
		{
			$dates   =   [
			    'dateIni'   =>  ($params['dateIni'] <> '') ? $_replace->ChangeDate($params['dateIni']) : '',
			    'dateEnd'   =>  ($params['dateEnd'] <> '') ? $_replace->ChangeDate($params['dateEnd']) : ''
			];
		}else{
			$dates 	=	'';
		}

		$statics 	=	Statics::GetStatics($dates);
		$TV 		=	Statics::LoadStaticsDataEntry(2, $dates);
		$INTPR 		=	Statics::LoadStaticsDataEntry(4, $dates);
		$INTVE 		=	Statics::LoadStaticsDataEntry(6, $dates);

		$user 		=	Statics::LoadStaticsDEUsers($dates);

        return $app->json(array(
            'status'    =>  true,
            'statics'	=>	$statics['cData'],
            'topVE'		=>	Statics::TopTable($statics['TopVE'], "VENEZUELA"),
            'topPR'		=>	Statics::TopTable($statics['TopPR'], "PUERTO RICO"),
            'TV'		=>	$TV,
            'TvSta'		=>	Statics::HTMLStaticsDataEntry($TV),
            'TvStaG'	=>	$TV['score'],
            'INTPR'		=>	$INTPR,
            'IntPRSta'	=>	Statics::HTMLStaticsDataEntry($INTPR),
            'IntPRStaG'	=>	$INTPR['score'],
            'INTVE'		=>	$INTVE,
            'IntVESta'	=>	Statics::HTMLStaticsDataEntry($INTVE),
            'IntVEStaG'	=>	$INTVE['score'],
            'uPRHtml'	=>	Statics::HTMLStaticsDEUsers($user['userPR']),
            'uVEHtml'	=>	Statics::HTMLStaticsDEUsers($user['userVE'])
        ));
		
	}

}