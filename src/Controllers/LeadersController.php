<?php
namespace App\Controllers;
require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Leader;
use App\Models\Did;

use PDO;

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
* 
*/
class LeadersController extends BaseController
{
	public function index(Application $app)
	{

		return $app['twig']->render('leaders/index.html.twig',array(
            'sidebar'       =>  true,
            'users'			=>	User::GetUsersByTeamLeaders($app['session']->get('team'))
        ));
	}

	public function view(Application $app, Request $request)
	{
		return $app['twig']->render('leaders/index.html.twig',array(
            'sidebar'              =>  true
        ));
	}

	public function Load(Application $app, Request $request)
	{
		$html 	=	Leader::GetHtmlUserInfo(User::GetUsersByTeamLeaders( $app['session']->get('team')));

		return $app->json(array(
            'html'    => 	$html
        ));
	}

	public function EditUser(Application $app, Request $request)
	{
		return $app->json(array(
			'statu'		=>	true,
            'user'    	=> 	User::GetUserById($request->get('id'))
        ));
	}

	public function FormEditUser(Application $app, Request $request)
	{
		$params     =   [];
        parse_str($request->get('str'), $params);

        $user 		= 	User::EditUserLeader($params);
       	
       	return $app->json(array(
			'status'		=>	($user == true) ? true : false
        ));
	}

	public function FormLoad(Application $app, Request $request)
	{
        $params     =   [];
        parse_str($request->get('str'), $params);

        $data 	=	Leader::GetInfoByOperator($params);
       	$html 	=	Leader::GetHtmlInfo($data['iTotal']);
       	$hSta 	=	Leader::GetHtmlInfoStatics($data['iStatics']);

		return $app->json(array(
            'html'    	=> 	$html,
            'hsta'		=>	$hSta
        ));
	}


}