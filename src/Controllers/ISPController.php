<?php

namespace App\Controllers;

require '../vendor/autoload.php';


use Silex\Application;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

use App\Models\Auth;
use App\Models\User;
use App\Models\IspType;
use App\Models\IspStatus;
use App\Models\IspLabel;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\DBMikroVE;
use App\Lib\SendMail;

/**
 * 
 */
class ISPController extends BaseController
{

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function index(Application $app, Request $request)
    {
        return $app['twig']->render('ISP/index.html.twig',array(
            'sidebar'       =>  true,
            'status'        =>  IspStatus::GetAllByStatus(1),
            'type'          =>  IspType::GetAllByStatus(1),
            'label'         =>  IspLabel::GetAllByStatus(1),
        ));
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function GetInfo(Application $app, Request $request)
    {
        
        $_replace   =   new Config();
        
        parse_str($request->get('str'), $params);

        switch ($params['new_option']) 
        {
            case '1':   $tQquery = "t1.id";         $type = "id";           break;
            case '2':   $tQquery = "t1.cedula";     $type = "cedula";       break;
            case '3':   $tQquery = "t1.telefono";   $type = "telefono";     break;
            case '4':   $tQquery = "t1.correo";     $type = "correo";       break;
            case '5':   $tQquery = "t1.nombre";     $type = "nombre";       break;
            default:    $tQquery = "t1.id";         $type = "id";           break;
        }

        $query  =   'SELECT COUNT(*) AS T FROM usuarios WHERE '.$type.' LIKE "%'.$params['new_id'].'%"';
        $T      =   DBMikroVE::DBQuery($query);

        if($T['T'] == 0)
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  'Client not found, try another medium. !'
            ));
        }
        elseif ($T['T'] == 1) 
        {
            $query  =   'SELECT id, nombre, telefono, correo, direccion_principal, estado FROM usuarios WHERE '.$type.' LIKE "%'.$params['new_id'].'%"';
            $iUser  =   DBMikroVE::DBQuery($query);

            $query  =   'SELECT idcliente, descripcion, coordenadas, instalado, costo, ipap, nodo, emisor FROM tblservicios WHERE idcliente LIKE "%'.$iUser['id'].'%"';
            $iServ  =   DBMikroVE::DBQuery($query);

            $query  =   'SELECT nodo FROM server WHERE id = "'.$iServ['nodo'].'"';
            $iNodo  =   DBMikroVE::DBQuery($query);

            $query  =   'SELECT nombre FROM emisores WHERE id = "'.$iServ['emisor'].'"';
            $iEmi   =   DBMikroVE::DBQuery($query);

            $iData  =   [
                'id'            =>  $iUser['id'],
                'nombre'        =>  $iUser['nombre'],
                'telefono'      =>  $iUser['telefono'],
                'correo'        =>  $iUser['correo'],
                'direccion'     =>  $iUser['direccion_principal'],
                'estado'        =>  $iUser['estado'],
                'plan'          =>  $iServ['descripcion'],
                'coordenadas'   =>  $iServ['coordenadas'],
                'instalacion'   =>  $iServ['instalado'],
                'costo'         =>  $iServ['costo'],    
                'ip'            =>  $iServ['ipap'],
                'nodo'          =>  $iNodo['nodo'],
                'emisor'        =>  $iEmi['nombre']
            ];

            return $app->json(array(
                'status'    =>  true, 
                'data'      =>  $iData,
                'html'      =>  '',
                'count'     =>  $T['T']
            ));

        }
        else
        {

            $query      =   'SELECT id, nombre, telefono, correo, direccion_principal, estado FROM usuarios WHERE '.$type.' LIKE "%'.$params['new_id'].'%"';
            $iClient    =   DBMikroVE::DBQueryAll($query);
            $html       =   '';

            foreach ($iClient as $key => $val) 
            {
                
                // $query  =   'SELECT idcliente, descripcion, coordenadas, instalado, costo, ipap, nodo, emisor FROM tblservicios WHERE idcliente LIKE "%'.$val['id'].'%"';
                // $iServ  =   DBMikroVE::DBQuery($query);

                // // var_dump($iServ);exit;

                // $query  =   'SELECT nodo FROM server WHERE id = "'.$iServ['nodo'].'"';
                // $iNodo  =   DBMikroVE::DBQuery($query);

                // $query  =   'SELECT nombre FROM emisores WHERE id = "'.$iServ['emisor'].'"';
                // $iEmi   =   DBMikroVE::DBQuery($query);

                    $html.='<tr>';
                    $html.='<td style="width:10%;">'.$val['id'].'</td>';
                    $html.='<td style="width:80%;">'.$val['nombre'].'</td>';
                    $html.='<td style="width:10%;"><a href="javascript:void(0);" onclick="SelectService()"><i class="fa fa-check"></i></a></td>';
                    $html.='</tr>';

                $iData[$key]  =   [
                    'id'            =>  $val['id'],
                    'nombre'        =>  $val['nombre'],
                    // 'telefono'      =>  $val['telefono'],
                    // 'correo'        =>  $val['correo'],
                    // 'estado'        =>  $val['estado'],
                    // 'direccion'     =>  $val['direccion_principal'],
                    // 'plan'          =>  $iServ['descripcion'],
                    // 'coordenadas'   =>  $iServ['coordenadas'],
                    // 'instalacion'   =>  $iServ['instalado'],
                    // 'costo'         =>  $iServ['ipap'],
                    // 'ip'            =>  $iServ['ipap'],
                    // 'nodo'          =>  $iNodo['nodo'],
                    // 'emisor'        =>  $iEmi['nombre']
                ];

            }

            return $app->json(array(
                'status'        =>  true, 
                'data'          =>  $iData,
                'htmlClient'    =>  $html,
                'count'         =>  $T['T']
            ));

        }
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function GetInfoId(Application $app, Request $request)
    {
       
        $_replace   =   new Config();

        $query  =   'SELECT id, nombre, telefono, correo, direccion_principal, estado FROM usuarios WHERE id LIKE "%'.$request->get('id').'%"';
        $iUser  =   DBMikroVE::DBQuery($query);

        $query  =   'SELECT idcliente, descripcion, coordenadas, instalado, costo, ipap, nodo, emisor FROM tblservicios WHERE idcliente LIKE "%'.$request->get('id').'%"';
        $iServ  =   DBMikroVE::DBQuery($query);

        $query  =   'SELECT nodo FROM server WHERE id = "'.$iServ['nodo'].'"';
        $iNodo  =   DBMikroVE::DBQuery($query);

        $query  =   'SELECT nombre FROM emisores WHERE id = "'.$iServ['emisor'].'"';
        $iEmi   =   DBMikroVE::DBQuery($query);

        $iData  =   [
            'id'            =>  $iUser['id'],
            'nombre'        =>  $iUser['nombre'],
            'telefono'      =>  $iUser['telefono'],
            'correo'        =>  $iUser['correo'],
            'direccion'     =>  $iUser['direccion_principal'],
            'estado'        =>  $iUser['estado'],
            'plan'          =>  $iServ['descripcion'],
            'coordenadas'   =>  $iServ['coordenadas'],
            'instalacion'   =>  $iServ['instalado'],
            'costo'         =>  $iServ['costo'],
            'ip'            =>  $iServ['ipap'],
            'nodo'          =>  $iNodo['nodo'],
            'emisor'        =>  $iEmi['nombre']
        ];

        return $app->json(array(
            'status'    =>  true, 
            'data'      =>  $iData,
        ));

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function ViewInfoId(Application $app, Request $request)
    {
        
        $_replace   =   new Config();
        $htmlLabel  =   $htmlCat    =   $htmlAdj    =   $htmlNotes   =   '';

        $query      =   'SELECT ticket, forms, id_mw, title,category_id, message, labels FROM cp_tickets_isp WHERE id = "'.$request->get('id').'"';
        $ticket     =   DBSmart::DBQuery($query);

        $adj        =   DBSmart::DBQueryAll('SELECT * FROM cp_isp_files_tmp WHERE forms = "'.$ticket['forms'].'" AND status_id = "1" ORDER BY id ASC');

        if($adj)
        {
            foreach ($adj as $key => $value) 
            {
                $htmlAdj.='<tr>';
                $htmlAdj.='<td><a href="assets/upload/'.$value['name'].'" target="_blank">'.$value['name'].'</a></td>';
                $htmlAdj.='</tr">';
            }

        }else{
            $htmlAdj.='<tr>';
            $htmlAdj.='<td>SIN ARCHIVOS ADJUNTOS</td>';
            $htmlAdj.='</tr">';
        }

        $ilabels    =   explode(",", $ticket['labels']);
        $type       =   IspType::GetAllByStatus(1);
        
        foreach ($ilabels as $l => $lab) 
        {
            $query  =   DBSmart::DBQuery('SELECT name FROM data_isp_labels WHERE id = "'.$lab.'"');
            $htmlLabel.='<a href=""><span class="badge badge-info">'.$query['name'].'</span></a> ';
        }

        $htmlCat.='<select class="form-control" id="view_cat_serv" name="view_cat_serv" readonly>';
        foreach ($type as $t => $ty) 
        {
            $select     =   ($ticket['category_id'] == $ty['id']) ? 'selected' : '';
            $htmlCat.='<option value="'.$ty['id'].'" '.$select.'>'.$ty['name'].'</option>';
        }
        $htmlCat.='</select>';

        $query      =   'SELECT id, note, user_id, img, type, created_at FROM cp_isp_notes WHERE ticket = "'.$request->get('id').'" ORDER BY id DESC';
        $not        =   DBSmart::DBQueryAll($query);
        
        if($not <> false)
        {
            $htmlNotes.='<ul>';
            foreach ($not as $k => $val) 
            {
                $htmlNotes.='<li class="message">';
                $htmlNotes.='<div class="message-text" style="text-align: justify;">';
                $htmlNotes.='<h3><strong><time>'.$_replace->ShowDateAll($val['created_at']).'</time></strong></h3>';
                $htmlNotes.='<a class="username">'.User::GetUserById($val['user_id'])['username'].'</a>';
                $htmlNotes.=''.utf8_encode($val['note']).'';
                if($val['img'] <> "")
                {
                    $htmlNotes.='<p class="chat-file row">';
                    $htmlNotes.='<b class="pull-left col-sm-6"> <i class="fa fa-file"></i>Attachment</b>';
                    $htmlNotes.='<span class="col-sm-6 pull-right"> <a target="_blank" href="'.$val['img'].'" class="btn btn-xs btn-success">View</a></span>';
                    $htmlNotes.='</p>';
                }
                $htmlNotes.='</div>';
                $htmlNotes.='</li>';
            }
            $htmlNotes.='</ul>';
        }

        $query      =   'SELECT id, nombre, telefono, correo, direccion_principal, estado FROM usuarios WHERE id LIKE "%'.$ticket['id_mw'].'%"';
        $iUser      =   DBMikroVE::DBQuery($query);

        $query      =   'SELECT idcliente, descripcion, coordenadas, instalado, costo, ipap, nodo, emisor FROM tblservicios WHERE idcliente LIKE "%'.$request->get('id').'%"';
        $iServ      =   DBMikroVE::DBQuery($query);

        $query      =   'SELECT nodo FROM server WHERE id = "'.$iServ['nodo'].'"';
        $iNodo      =   DBMikroVE::DBQuery($query);

        $query      =   'SELECT nombre FROM emisores WHERE id = "'.$iServ['emisor'].'"';
        $iEmi       =   DBMikroVE::DBQuery($query);

        $iData  =   [
            'id'            =>  $iUser['id'],
            'nombre'        =>  $iUser['nombre'],
            'telefono'      =>  $iUser['telefono'],
            'correo'        =>  $iUser['correo'],
            'direccion'     =>  $iUser['direccion_principal'],
            'estado'        =>  $iUser['estado'],
            'plan'          =>  $iServ['descripcion'],
            'coordenadas'   =>  $iServ['coordenadas'],
            'instalacion'   =>  $iServ['instalado'],
            'costo'         =>  $iServ['costo'],
            'ip'            =>  $iServ['ipap'],
            'nodo'          =>  $iNodo['nodo'],
            'emisor'        =>  $iEmi['nombre']
        ];

        return $app->json(array(
            'status'    =>  true, 
            'data'      =>  $iData,
            'lab'       =>  $htmlLabel,
            'cat'       =>  $htmlCat,
            'adj'       =>  $htmlAdj,
            'notes'     =>  $htmlNotes,
            'info'      =>  $ticket,

        ));

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function TmpFiles(Application $app, Request $request)
    {
        $_info  =   new Config();

        $ale    =   $_info->Ramdom(10);

        switch ($_FILES["csv"]['type']) {
            case 'image/jpeg':
                $type   =   '.jpeg';
                break;
            case 'image/png':
                $type   =   '.png';
                break;
            case 'image/jpg':
                $type   =   '.jpg';
                break;            
            default:
                $type   =   '.jpeg';
                break;
        }


        if(!$_FILES["csv"]['name'] == "")
        {

            $forms  =   (isset($_POST['id_forms'])) ? 'forms' : 'edit';
            $file    =   [
                'forms' =>  (isset($_POST['id_forms'])) ? $_POST['id_forms'] : $_POST['Edit_id_forms'],
                'name'  =>  $ale.$type,
                'size'  =>  $_FILES["csv"]['size'],
                'type'  =>  $_FILES["csv"]["type"],
                'sta'   =>  1,
                'dir'   =>  "assets/upload/".$ale.$type,
            ];

            $img            =   move_uploaded_file($_FILES["csv"]["tmp_name"], $app['upload'].$file['name']);

            if($img == true)
            {
                
                $query      =   'INSERT INTO cp_isp_files_tmp (forms, name, img, type, size, status_id, created_at) VALUES("'.$file['forms'].'", "'.$file['name'].'", "'.$file['dir'].'", "'.$file['type'].'", "'.$file['size'].'", "1", NOW())';
                $insert     =   DBSmart::DataExecute($query);

                $iFiles     =   DBSmart::DBQueryAll('SELECT * FROM cp_isp_files_tmp WHERE forms = "'.$file['forms'].'" AND status_id = "1" ORDER BY id ASC');

                $html   =   '';
                foreach ($iFiles as $key => $value) {
                    $html.='<tr>';
                    $html.='<td><a href="assets/upload/'.$value['name'].'"target="_blank">'.$value['name'].'</a></td>';
                    $html.='<td><a href="javascript:void(0);" onclick="DelTmpAdj('.(int)$value['id'].')"><i class="fa fa-times"></i></a></td>';
                    $html.='</tr">';
                }

                return $app->json(array(
                    'status'    =>  true, 
                    'forms'     =>  $forms,
                    'html'      =>  $html
                ));

            }else{
                return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("Error al intentar guardar archivo adjunto, intente nuevamente si el problema persiste contacte a su administrador de sistemas.", true)
                )); 
            }

        }else{

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("No se pudo procesar la solicitud, debe dejar una nota valida", true)
            )); 

        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function SaveService(Application $app, Request $request)
    {
        $_replace   =   new Config();
        parse_str($request->get('title'),   $title);
        parse_str($request->get('text'),    $text);
        parse_str($request->get('label'),   $label);

        if(trim($title['name_serv']) == "")
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Debe introducir un título válido.",
            ));   
        }

        if(trim($text['text_serv']) == "")
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "El campo información de servicio no puede estar vacío.",
            ));   
        }

        if(!isset($label['labels_serv']))
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Debe seleccionar al menos una etiqueta válida.",
            ));   
        }
        
        $l  =   "";
        foreach($label as $k => $val) { foreach ($val as $y => $yal) { $l.=$yal.","; } }
        $labels     =   substr($l, 0, (strlen($l) -1 ));

        $ticket     =   DBSmart::DBQuery('SELECT ticket FROM cp_tickets_isp ORDER BY id DESC limit 1');

        $ticket     =   (isset($ticket['ticket'])) ? $ticket['ticket'] : 0;
        $tTicket    =   $ticket+1;

        $query      =   'INSERT INTO cp_tickets_isp (ticket, forms, id_mw, title, category_id, message, labels, status_id, operator_id, created_at) VALUES ("'.$tTicket.'", "'.$title['id_form'].'", "'.$title['id_serv'].'", "'.$title['name_serv'].'", "'.$title['category_serv'].'", "'.$text['text_serv'].'", "'.$labels.'", "1", "'.$app['session']->get('id').'", "'.date("Y-m-d").'")';

        $insert     =   DBSmart::DataExecuteLastID($query);

        if($insert)
        {
            
            $tic        =   $insert;
            $client     =   DBSmart::DBQuery('SELECT ticket, title, message, id_mw FROM cp_tickets_isp WHERE ticket = "'.$tic.'"');

            $query  =   'SELECT id, nombre, telefono, correo, direccion_principal, estado FROM usuarios WHERE id LIKE "%'.$client['id_mw'].'%"';
            $iUser  =   DBMikroVE::DBQuery($query);

            $query  =   'SELECT idcliente, descripcion, coordenadas, instalado, costo, ipap, nodo, emisor FROM tblservicios WHERE idcliente LIKE "%'.$iUser['id'].'%"';
            $iServ  =   DBMikroVE::DBQuery($query);

            $query  =   'SELECT nodo FROM server WHERE id = "'.$iServ['nodo'].'"';
            $iNodo  =   DBMikroVE::DBQuery($query);

            $query  =   'SELECT nombre FROM emisores WHERE id = "'.$iServ['emisor'].'"';
            $iEmi   =   DBMikroVE::DBQuery($query);

            $iData  =   [
                'id'            =>  $iUser['id'],
                'nombre'        =>  $iUser['nombre'],
                'telefono'      =>  $iUser['telefono'],
                'correo'        =>  $iUser['correo'],
                'direccion'     =>  $iUser['direccion_principal'],
                'estado'        =>  $iUser['estado'],
                'plan'          =>  $iServ['descripcion'],
                'coordenadas'   =>  $iServ['coordenadas'],
                'instalacion'   =>  $iServ['instalado'],
                'costo'         =>  $iServ['costo'],    
                'ip'            =>  $iServ['ipap'],
                'nodo'          =>  $iNodo['nodo'],
                'emisor'        =>  $iEmi['nombre']
            ];

            $email      =   DBSmart::DBQueryAll('SELECT email FROM data_isp_emails WHERE status_id = "1" ORDER BY id ASC');

            if($email <> false ) { foreach ($email as $e => $em) { $ccs[$e] = $em['email']; } }else{ $ccs = ''; }

            $html   =   '';
            $html   =   '<table><tbody><tr><td>ID:</td><td>'.$iData['id'].'</td></tr><tr><td>Cliente:</td><td>'.$iData['nombre'].'</td></tr><tr><td>Direcci&oacute;n:</td><td>'.$iData['direccion'].'</td></tr><tr><td>Tel&eacute;fonos:</td><td>'.$iData['telefono'].'</td></tr><tr><td>Emails:</td><td>'.$iData['correo'].'</td></tr><tr><td>Servicio:</td><td>'.$iData['plan'].'</td></tr><tr><td>IP:</td><td>'.$iData['ip'].'</td></tr><tr><td>Router:</td><td>'.$iData['nodo'].'</td></tr><tr><td>Estatus:</td><td>'.$iData['estado'].'</td></tr><tr><td>Instalaci&oacute;n</td><td>2019-09-26</td></tr></tbody></table>';


            $email      =   new SendMail();
            $body       =   'T&iacute;tulo: <strong>'.$client['title'].'</strong>. <br><br>

            Descriptci&oacute;n: <strong>'.$client['message'].'</strong>. <br><br>

            Categoria: <strong>Modificaci&Oacuten del Servicio.</strong>. <br><br>

            Informaci&oacute;n: <strong>'.$html.'</strong>

            <br><br>';

            $iData      =   [
                'emails'    =>  $ccs,
                'ccs'       =>  '',
                'subject'   =>  "TICKET ISP".$client['ticket']." - ".$client['title'],
                'body'      =>  $body,
                'att'       =>  ''
            ];


            $sendEmail  =   $email->SendIPSTicket($iData);

            return $app->json(array(
                'status'    =>  true, 
            ));

        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Error en almacenamiento de informacion, intentelo nuevamente.",
            ));
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function Load(Application $app, Request $request)
    {
        $html = "";
        $actives    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "1" ORDER BY ID DESC');
        $process    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "2" ORDER BY ID DESC');
        $suspend    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "3" ORDER BY ID DESC');
        $cancel     =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "4" ORDER BY ID DESC');

        $act        =   ($actives <> false) ?   ISPController::HtmlLoad($actives, 1)   : '';
        $pro        =   ($process <> false) ?   ISPController::HtmlLoad($process, 2)   : '';
        $sus        =   ($suspend <> false) ?   ISPController::HtmlLoad($suspend, 3)   : '';
        $can        =   ($cancel <> false) ?    ISPController::HtmlLoad($cancel, 4)    : '';
        
        return $app->json(array(
            'status'    =>  true, 
            'actives'   =>  $act,
            'process'   =>  $pro,
            'suspend'   =>  $sus,
            'cancel'    =>  $can,
        ));

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function Notes(Application $app, Request $request)
    {
        $_info  =   new Config();

        $ale    =   $_info->Ramdom(10);

        if(!$_POST['notes'] == "")
        {
            if(!$_FILES['file']['name'] == "")
            {
                switch ($_FILES["file"]['type']) {
                    case 'image/jpeg':
                        $type   =   '.jpeg';
                        break;
                    case 'image/png':
                        $type   =   '.png';
                        break;
                    case 'image/jpg':
                        $type   =   '.jpg';
                        break;            
                    default:
                        $type   =   '.jpeg';
                        break;
                }

                $file    =   [
                    'name'  =>  $ale.$type,
                    'size'  =>  $_FILES["file"]['size'],
                    'type'  =>  $_FILES["file"]["type"],
                    'sta'   =>  1,
                    'dir'   =>  "assets/upload/".$ale.$type,
                ];

                $img            =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$file['name']);

                if($img == true)
                {
                    $note       =   strtoupper($_info->deleteTilde($_POST['notes']));

                    $query      =   'INSERT INTO cp_isp_notes(ticket, note, img, type, size, user_id, created_at) VALUES ("'.$_POST['view_note_serv'].'", "'.$note.'", "'.$file['dir'].'", "'.$file['type'].'", "'.$file['size'].'", "'.$app['session']->get('id').'", NOW())';

                    $not        =   DBSmart::DataExecuteLastID($query);

                    $nTicket    =   DBSmart::DBQuery('SELECT ticket FROM cp_isp_notes WHERE id = "'.$not.'"');
                    $idTicket   =   DBSmart::DBQuery('SELECT ticket, id_mw FROM cp_tickets_isp WHERE ticket = "'.$nTicket['ticket'].'"');

                    $in         =  '&lt;p&gt;';
                    $fn         =  '&lt;/p&gt;';

                    $asunto     =   "NOTA TICKET ISP".$idTicket['ticket'];                       
                    $mensaje    =   ''.$in.' '.$note.' '.$fn.'';

                    $query      =   'INSERT INTO notas(idcliente, asunto, mensaje, fecha, adjuntos, idoperador, imagen, tipo, portal) VALUES ("'.$idTicket['id_mw'].'", "'.$asunto.'", "'.$mensaje.'", NOW(), "", "1", "", "0", "")';

                    $mwNote     =   DBMikroVE::DataExecute($query);

                    if($note == true)
                    { 
                        $_replace   =   new Config();
                        $query      =   'SELECT id, ticket, note, user_id, img, type, created_at FROM cp_isp_notes WHERE ticket = "'.$_POST['view_note_serv'].'" ORDER BY id DESC';
                        $not        =   DBSmart::DBQueryAll($query);
                        $html       =   "";
                        
                        if($not <> false)
                        {
                            $html.='<ul>';
                            foreach ($not as $k => $val) 
                            {
                                $html.='<li class="message">';
                                $html.='<div class="message-text" style="text-align: justify;">';
                                $html.='<h3><strong><time>'.$_replace->ShowDateAll($val['created_at']).'</time></strong></h3>';
                                $html.='<a class="username">'.User::GetUserById($val['user_id'])['username'].'</a>';
                                $html.=''.utf8_encode($val['note']).'';
                                if($val['img'] <> "")
                                {
                                    $html.='<p class="chat-file row">';
                                    $html.='<b class="pull-left col-sm-6"> <i class="fa fa-file"></i>Attachment</b>';
                                    $html.='<span class="col-sm-6 pull-right"> <a target="_blank" href="'.$file['dir'].'" class="btn btn-xs btn-success">View</a></span>';
                                    $html.='</p>';
                                }
                                $html.='</div>';
                                $html.='</li>';
                            }
                            $html.='</ul>';
                        }

                        return $app->json(array(
                            'status'    => true, 
                            'notes'     => $html
                        ));

                    }else{

                        return $app->json(array(
                            'status'    => false, 
                            'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                        ));
                    
                    }

                }else{
                    return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Error al intentar guardar archivo adjunto, intente nuevamente si el problema persiste contacte a su administrador de sistemas.", true)
                    )); 
                }
            
            }else{
                $_replace   =   new Config();

                $note       =   strtoupper($_replace->deleteTilde($_POST['notes']));

                $query      =   'INSERT INTO cp_isp_notes(ticket, note, img, type, size, user_id, created_at) VALUES ("'.$_POST['view_note_serv'].'", "'.$note.'", "", "", "", "'.$app['session']->get('id').'", NOW())';

                $not       =   DBSmart::DataExecuteLastID($query);

                $nTicket    =   DBSmart::DBQuery('SELECT ticket FROM cp_isp_notes WHERE id = "'.$not.'"');
                $idTicket   =   DBSmart::DBQuery('SELECT ticket, id_mw FROM cp_tickets_isp WHERE ticket = "'.$nTicket['ticket'].'"');

                $in         =  '&lt;p&gt;';
                $fn         =  '&lt;/p&gt;';

                $asunto     =   "NOTA TICKET ISP".$idTicket['ticket'];                       
                $mensaje    =   ''.$in.' '.$note.' '.$fn.'';

                $query      =   'INSERT INTO notas(idcliente, asunto, mensaje, fecha, adjuntos, idoperador, imagen, tipo, portal) VALUES ("'.$idTicket['id_mw'].'", "'.$asunto.'", "'.$mensaje.'", NOW(), "", "1", "", "0", "")';

                $mwNote     =   DBMikroVE::DataExecute($query);

                if($not == true)
                { 

                    $_replace   =   new Config();
                    $query      =   'SELECT id, note, user_id, img, type, created_at FROM cp_isp_notes WHERE ticket = "'.$_POST['view_note_serv'].'" ORDER BY id DESC';
                    $not        =   DBSmart::DBQueryAll($query);
                    $html       =   "";


                    
                    if($not <> false)
                    {
                        $html.='<ul>';
                        foreach ($not as $k => $val) 
                        {
                            $html.='<li class="message">';
                            $html.='<div class="message-text" style="text-align: justify;">';
                            $html.='<h3><strong><time>'.$_replace->ShowDateAll($val['created_at']).'</time></strong></h3>';
                            $html.='<a class="username">'.User::GetUserById($val['user_id'])['username'].'</a>';
                            $html.=''.utf8_encode($val['note']).'';
                            if($val['img'] <> "")
                            {
                                $html.='<p class="chat-file row">';
                                $html.='<b class="pull-left col-sm-6"> <i class="fa fa-file"></i>Attachment</b>';
                                $html.='<span class="col-sm-6 pull-right"> <a target="_blank" href="../../'.$val['img'].'" class="btn btn-xs btn-success">View</a></span>';
                                $html.='</p>';
                            }
                            $html.='</div>';
                            $html.='</li>';
                        }
                        $html.='</ul>';
                    }

                    return $app->json(array(
                        'status'    =>  true, 
                        'notes'     =>  $html
                    ));

                }else{


                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                    ));
                }
            }
        
        }else{

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("No se pudo procesar la solicitud, debe dejar una nota valida", true)
            )); 

        }   
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function GetEditInfo(Application $app, Request $request)
    {

        $_replace   =   new Config();

        $htmlLabel  =   $htmlCat    =   $htmlAdj    =   $htmlNotes   =   '';

        $query      =   'SELECT ticket, forms, title,category_id, message, labels FROM cp_tickets_isp WHERE id = "'.$request->get('id').'"';
        $ticket     =   DBSmart::DBQuery($query);

        $adj        =   DBSmart::DBQueryAll('SELECT * FROM cp_isp_files_tmp WHERE forms = "'.$ticket['forms'].'" AND status_id = "1" ORDER BY id ASC');

        if($adj)
        {
            foreach ($adj as $key => $value) 
            {
                $htmlAdj.='<tr>';
                $htmlAdj.='<td><a href="assets/upload/'.$value['name'].'" target="_blank">'.$value['name'].'</a></td>';
                $htmlAdj.='<td><a href="javascript:void(0);" onclick="DelEditAdj('.(int)$value['id'].')"><i class="fa fa-times"></i></a></td>';
                $htmlAdj.='</tr">';
            }

        }else{
            $htmlAdj.='<tr>';
            $htmlAdj.='<td>SIN ARCHIVOS ADJUNTOS</td>';
            $htmlAdj.='</tr">';
        }

        $ilabels    =   explode(",", $ticket['labels']);
        $type       =   IspType::GetAllByStatus(1);
        $label      =   IspLabel::GetAllByStatus(1);

        $htmlLabel.='<select multiple="" class="form-control custom-scroll" name="Edit_labels_serv[]" title="Seleccione algunas cateogiras">';

        foreach ($label as $l => $lab) 
        {
            foreach ($ilabels as $i => $ila) 
            {
                if($lab['id'] == $ila){ $select = 'selected'; goto s1; }else{ $select = '';}
            }
            s1:
                $htmlLabel.='<option value="'.$lab['id'].'" '.$select.'>'.$lab['name'].'</option>';
        }
        $htmlLabel.='</select>';

        $htmlCat.='<select class="form-control" id="Edit_cat_serv" name="Edit_cat_serv">';
        foreach ($type as $t => $ty) 
        {
            $select     =   ($ticket['category_id'] == $ty['id']) ? 'selected' : '';
            $htmlCat.='<option value="'.$ty['id'].'" '.$select.'>'.$ty['name'].'</option>';
        }
        $htmlCat.='</select>';

        $query  =   'SELECT id, nombre, telefono, correo, direccion_principal, estado FROM usuarios WHERE id LIKE "%'.$request->get('id').'%"';
        $iUser  =   DBMikroVE::DBQuery($query);

        $query  =   'SELECT idcliente, descripcion, coordenadas, instalado, costo, ipap, nodo, emisor FROM tblservicios WHERE idcliente LIKE "%'.$request->get('id').'%"';
        $iServ  =   DBMikroVE::DBQuery($query);

        $query  =   'SELECT nodo FROM server WHERE id = "'.$iServ['nodo'].'"';
        $iNodo  =   DBMikroVE::DBQuery($query);

        $query  =   'SELECT nombre FROM emisores WHERE id = "'.$iServ['emisor'].'"';
        $iEmi   =   DBMikroVE::DBQuery($query);

        $iData  =   [
            'id'            =>  $iUser['id'],
            'nombre'        =>  $iUser['nombre'],
            'telefono'      =>  $iUser['telefono'],
            'correo'        =>  $iUser['correo'],
            'direccion'     =>  $iUser['direccion_principal'],
            'estado'        =>  $iUser['estado'],
            'plan'          =>  $iServ['descripcion'],
            'coordenadas'   =>  $iServ['coordenadas'],
            'instalacion'   =>  $iServ['instalado'],
            'costo'         =>  $iServ['costo'],
            'ip'            =>  $iServ['ipap'],
            'nodo'          =>  $iNodo['nodo'],
            'emisor'        =>  $iEmi['nombre']
        ];

        return $app->json(array(
            'status'    =>  true, 
            'data'      =>  $iData,
            'lab'       =>  $htmlLabel,
            'cat'       =>  $htmlCat,
            'adj'       =>  $htmlAdj,
            'notes'     =>  $htmlNotes,
            'info'      =>  $ticket,

        ));
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function UpdateServices(Application $app, Request $request)
    {
        
        $_replace   =   new Config();
        parse_str($request->get('title'),   $title);
        parse_str($request->get('text'),    $text);
        parse_str($request->get('label'),   $label);

        if(trim($title['Edit_nam_serv']) == "")
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Debe introducir un título válido.",
            ));   
        }

        if(trim($text['Edit_tex_serv']) == "")
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "El campo información de servicio no puede estar vacío.",
            ));   
        }

        if(!isset($label['Edit_labels_serv']))
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Debe seleccionar al menos una etiqueta válida.",
            ));   
        }

        $l  =   "";
        foreach($label as $k => $val) { foreach ($val as $y => $yal) { $l.=$yal.","; } }
        $labels     =   substr($l, 0, (strlen($l) -1 ));

        $update     =   DBSmart::DataExecute('UPDATE cp_tickets_isp SET title = "'.$title['Edit_nam_serv'].'", message = "'.$text['Edit_tex_serv'].'", category_id = "'.$title['Edit_cat_serv'].'", labels = "'.$labels.'" WHERE ticket = "'.$title['Edit_id_serv'].'"');

        if($update)
        {
            return $app->json(array(
                'status'    =>  true, 
            ));
        }else{
            return $app->json(array(
                'status'    =>  false,
                'message'   =>  "Error, Imposible procesar tu solicitud."
            ));
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function ProcessService(Application $app, Request $request)
    {
        $update     =   DBSmart::DataExecute('UPDATE cp_tickets_isp SET status_id= "'.$request->get('type').'", finish_at = "'.date("Y-m-d").'" WHERE id = "'.$request->get('id').'"');

        if($update)
        {
            return $app->json(array(
                'status'    =>  true
            )); 
        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Intente nuevamente.!",
            )); 
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function OpenService(Application $app, Request $request)
    {
        $update     =   DBSmart::DataExecute('UPDATE cp_tickets_isp SET status_id= "'.$request->get('type').'", finish_at = "" WHERE id = "'.$request->get('id').'"');

        if($update)
        {
            return $app->json(array(
                'status'    =>  true
            )); 
        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Intente nuevamente.!",
            )); 
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function EmailService(Application $app, Request $request)
    {
        
        $correo     =   $ccs    =   [];
        $client     =   DBSmart::DBQuery('SELECT ticket, title, message, id_mw FROM cp_tickets_isp WHERE ticket = "'.$request->get('id').'"');
        $iUser      =   DBMikroVE::DBQuery('SELECT id, nombre, telefono, correo, direccion_principal, estado FROM usuarios WHERE id LIKE "%'.$client['id_mw'].'%"');
        $email      =   DBSmart::DBQueryAll('SELECT email FROM data_isp_emails WHERE status_id = "1" ORDER BY id ASC');

        if($email <> false ) { foreach ($email as $e => $em) { $ccs[$e] = $em['email']; } }else{ $ccs = ''; }

        switch ($request->get('type')) {
            case '1':
                $email      =   new SendMail();
                $body       =   'Estimado cliente <strong>'.$iUser['nombre'].'</strong>. <br><br>

                El presente es para informar que hemos recibido su comunicación, para efectos de documentación se generó Ticket: <strong>ISP'.$client['ticket'].'</strong>, por medio del cual podrá darle seguimiento a su caso. <br><br>

                Al tener mayor información se la haremos llegar. <br><br>

                Saludos. <br><br>';
            break;
            case '2':
                $email      =   new SendMail();
                $body       =   'Estimado cliente <strong>'.$iUser['nombre'].'</strong>. <br><br>

                El presente es para informar que el personal de <strong>'.$iUser['nombre'].'</strong> nos confirma operatividad, por lo cual se procede al cierre del ticket <strong>ISP'.$client['ticket'].'</strong>. <br><br>

                Saludos. <br><br>';
            break;
            case '3':
                $email      =   new SendMail();
                $body       =   'Estimado cliente <strong>'.$iUser['nombre'].'</strong>. <br><br>

                El presente es para informar que debido a inactividad en el caso, se procede al cierre del ticket <strong>ISP'.$client['ticket'].'</strong>. <br><br>

                Saludos. <br><br>';
            break;
            
            default:
                $email      =   new SendMail();
                $body       =   'Estimado cliente <strong>'.$iUser['nombre'].'</strong>. <br><br>

                El presente es para informar que hemos recibido su comunicación, para efectos de documentación se generó Ticket: <strong>ISP'.$client['ticket'].'</strong>, por medio del cual podrá darle seguimiento a su caso. <br><br>

                Al tener mayor información se la haremos llegar. <br><br>

                Saludos. <br><br>';
            break;
        }
        
        if($iUser['correo'] <> '')
        {
            $emails     =   explode(",", $iUser['correo']);

            foreach ($emails as $e => $em) { $correo[$e]     =   $em; }

            $iData      =   [
                'emails'    =>  $correo,
                'ccs'       =>  $ccs,
                'subject'   =>  "TICKET ISP".$client['ticket']." - ".$client['title'],
                'body'      =>  $body,
                'att'       =>  ''
            ];

            $sendEmail  =   $email->SendIPS($iData);

            if($sendEmail['error'] == false)
            {
                return $app->json(array(
                    'status'    =>  true, 
                    'message'   =>  "Email Enviados Satisfactoriamente",
                ));             

            }else{
                return $app->json(array(
                    'status'    =>  false, 
                    'message'   =>  "Se produjo un error enviando el email asociado, intente nuevamente.",
                ));                             
            }            

        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Cliente no posee emails asociados",
            )); 
        }
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function DeteleAdj(Application $app, Request $request)
    {
        $htmlAdj    =   '';
        $iAdj       =   DBSmart::DataExecute('UPDATE cp_isp_files_tmp SET status_id = "2" WHERE id = "'.$request->get('id').'"');
        
        if($iAdj <> false)
        {
            $info   =   DBSmart::DBQuery('SELECT forms FROM cp_isp_files_tmp WHERE id = "'.$request->get('id').'"');

            if($info <> false)
            {
                $adj        =   DBSmart::DBQueryAll('SELECT * FROM cp_isp_files_tmp WHERE forms = "'.$info['forms'].'" AND status_id = "1" ORDER BY id ASC');

                if($adj)
                {
                    foreach ($adj as $key => $value) 
                    {
                        $htmlAdj.='<tr>';
                        $htmlAdj.='<td><a href="assets/upload/'.$value['name'].'" target="_blank">'.$value['name'].'</a></td>';
                        $htmlAdj.='<td><a href="javascript:void(0);" onclick="DelAdj('.(int)$value['id'].')"><i class="fa fa-times"></i></a></td>';
                        $htmlAdj.='</tr">';
                    }

                }else{
                    $htmlAdj.='<tr>';
                    $htmlAdj.='<td>SIN ARCHIVOS ADJUNTOS</td>';
                    $htmlAdj.='</tr">';
                }

            }else{
                $htmlAdj.='<tr>';
                $htmlAdj.='<td>SIN ARCHIVOS ADJUNTOS</td>';
                $htmlAdj.='</tr">';
            }

            return $app->json(array(
                'status'    =>  true, 
                'adj'       =>  $htmlAdj,
            )); 

        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "No se pudo eliminar el archivo, intente nuevamente",
            )); 
        }
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function DeteleTmpAdj(Application $app, Request $request)
    {
        
        $htmlAdj    =   '';
        $iAdj       =   DBSmart::DataExecute('UPDATE cp_isp_files_tmp SET status_id = "2" WHERE id = "'.$request->get('id').'"');
        
        if($iAdj <> false)
        {
            $info   =   DBSmart::DBQuery('SELECT forms FROM cp_isp_files_tmp WHERE id = "'.$request->get('id').'"');

            $adj        =   DBSmart::DBQueryAll('SELECT * FROM cp_isp_files_tmp WHERE forms = "'.$request->get('form').'" AND status_id = "1" ORDER BY id ASC');

            if($adj)
            {
                foreach ($adj as $key => $value) 
                {
                    $htmlAdj.='<tr>';
                    $htmlAdj.='<td><a href="assets/upload/'.$value['name'].'" target="_blank">'.$value['name'].'</a></td>';
                    $htmlAdj.='<td><a href="javascript:void(0);" onclick="DelAdj('.(int)$value['id'].')"><i class="fa fa-times"></i></a></td>';
                    $htmlAdj.='</tr">';
                }

            }else{
                $htmlAdj.='<tr>';
                $htmlAdj.='<td>SIN ARCHIVOS ADJUNTOS</td>';
                $htmlAdj.='</tr">';
            
            }

            return $app->json(array(
                'status'    =>  true, 
                'adj'       =>  $htmlAdj,
            )); 

        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "No se pudo eliminar el archivo, intente nuevamente",
            )); 
        }
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function HtmlLoad($data, $status)
    {
        $html = '';
        if($data)
        {
            foreach ($data as $a => $act) 
            {
                $iActives[$a]   =   [
                    'ticket'    =>  'ISP'.$act['ticket'],
                    'mw'        =>  $act['id_mw'],
                    'title'     =>  strtoupper($act['title']),
                    'category'  =>  DBSmart::DBQuery('SELECT name FROM data_isp_type WHERE id = "'.$act['category_id'].'"')['name'],
                    'status'    =>  DBSmart::DBQuery('SELECT name FROM data_isp_status WHERE id = "'.$act['status_id'].'"')['name'],
                    'operator'  =>  DBSmart::DBQuery('SELECT username FROM users WHERE id = "'.$act['operator_id'].'"')['username'],
                    'created'   =>  $act['created_at'],
                    'finish'    =>  $act['finish_at'],
                ];

                if($status == 1)
                {

                    $html.='<tr>';
                    $html.='<td style="width:5%;">'.$iActives[$a]['mw'].'</td>';
                    $html.='<td style="width:5%;"">'.$iActives[$a]['ticket'].'</td>';
                    $html.='<td style="width:45%;"><a href="javascript:void(0);" onclick="ViewOpenService('.(int)$act['ticket'].')">'.$iActives[$a]['title'].'</a></td>';
                    $html.='<td style="width:5%;"><span class="label label-success">'.$iActives[$a]['category'].'</span></td>';
                    $html.='<td style="width:5%;"><span class="label label-warning">'.$iActives[$a]['status'].'</span></td>';
                    $html.='<td style="width:10%;">'.$iActives[$a]['operator'].'</td>';
                    $html.='<td style="width:7%;">'.$iActives[$a]['created'].'</td>';
                    $html.='<td style="width:7%;">';

                    $html.='<div class="btn-group btn-group-sm">';
                    $html.='<a class="btn btn-info" href="javascript:void(0);">Acci&oacute;n</a>';
                    $html.='<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>';
                    $html.='<ul class="dropdown-menu">';
                    $html.='<li><a href="javascript:void(0);" onclick="EditServices('.(int)$act['ticket'].');">Editar</a></li>';
                    $html.='<li><a href="javascript:void(0);" onclick="ProcessService('.(int)$act['ticket'].');">Solucionado</a></li>';
                    $html.='<li><a href="javascript:void(0);" onclick="SuspendedServices('.(int)$act['ticket'].');">Inactividad</a></li>';
                    $html.='<li><a href="javascript:void(0);" onclick="CancelServices('.(int)$act['ticket'].');">Cancelar</a></li>';
                    $html.='<li><a href="javascript:void(0);" onclick="SendEmail('.(int)$act['ticket'].');">Emails</a></li>';
                    $html.='</ul>';
                    $html.='</div>';
                    $html.='</td>';

                }

                if( ($status == 2) || ($status == 3) || ($status == 4) )
                {
                    switch ($status) 
                    {
                        case '2': $em = '<li><a href="javascript:void(0);" onclick="SendEmailPro('.(int)$act['ticket'].');">Emails</a></li>'; break;
                        case '3': $em = '<li><a href="javascript:void(0);" onclick="SendEmailSus('.(int)$act['ticket'].');">Emails</a></li>'; break;
                        case '4': $em = ''; break;
                        default:  $em = '<li><a href="javascript:void(0);" onclick="SendEmailPro('.(int)$act['ticket'].');">Emails</a></li>'; break;
                    }

                    $html.='<tr>';
                    $html.='<td style="width:5%;">'.$iActives[$a]['mw'].'</td>';
                    $html.='<td style="width:10%;">'.$iActives[$a]['ticket'].'</td>';
                    $html.='<td style="width:35%;"><a href="javascript:void(0);" onclick="ViewOpenService('.(int)$act['ticket'].')">'.$iActives[$a]['title'].'</a></td>';
                    $html.='<td style="width:10%;"><span class="label label-success">'.$iActives[$a]['category'].'</span></td>';
                    $html.='<td style="width:10%;">'.$iActives[$a]['operator'].'</td>';
                    $html.='<td style="width:10%;">'.$iActives[$a]['created'].'</td>';
                    $html.='<td style="width:10%;">'.$iActives[$a]['finish'].'</td>';
                    $html.='<td style="width:10%;">';

                    $html.='<div class="btn-group btn-group-sm">';
                    $html.='<a class="btn btn-info" href="javascript:void(0);">Acci&oacute;n</a>';
                    $html.='<a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><span class="caret"></span></a>';
                    $html.='<ul class="dropdown-menu">';
                    $html.='<li><a href="javascript:void(0);" onclick="ReOpen('.(int)$act['ticket'].');">Open</a></li>';
                    $html.=''.$em.'';
                    $html.='</ul>';
                    $html.='</div>';
                    $html.='</td>';
                }

                $html.='</tr>';
            }
        }

        return $html;
    
    }

    public function HtmlDashLoad($data,$ty)
    {
        $html = '';
        if($data)
        {
            foreach ($data as $a => $act) 
            {
                
                switch ($ty) 
                {
                    case '1': $btn = '<a href="javascript:void(0);" onclick="EditCat('.(int)$act['id'].')"><i class="fa fa-edit"></i></a>'; break;
                    case '2': $btn = '<a href="javascript:void(0);" onclick="EditLab('.(int)$act['id'].')"><i class="fa fa-edit"></i></a>'; break;
                    case '3': $btn = '<a href="javascript:void(0);" onclick="EditEma('.(int)$act['id'].')"><i class="fa fa-edit"></i></a>'; break;
                    default: $btn = '<a href="javascript:void(0);" onclick="EditCat('.(int)$act['id'].')"><i class="fa fa-edit"></i></a>'; break;
                }

                $status     =   DBSmart::DBQuery('SELECT name FROM data_isp_status WHERE id = "'.$act['status_id'].'"');
                $spSta      =   ($act['status_id'] == 1) ? 'label-success' : 'label-warning';
                $name       =   (isset($act['name'])) ? $act['name'] : $act['email'];
                $html.='<tr>';
                $html.='<td style="text-align: center; width:10%;">'.$act['id'].'</td>';
                $html.='<td style="width:70%;"">'.strtoupper($name).'</td>';
                $html.='<td style="text-align: center; width:10%;"><span class="label '.$spSta.'">'.$status['name'].'</span></td>';
                $html.='<td style="text-align: center; width:10%;">'.$btn.'</td>';
                $html.='</tr>';
            }
        }

        return $html;
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function LoadByDate(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];

        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            $html = "";
            $actives    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "1" AND created_at BETWEEN "'.$dates['dateIni'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');
            $process    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "2" AND finish_at BETWEEN "'.$dates['dateIni'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');
            $suspend    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "3" AND finish_at BETWEEN "'.$dates['dateIni'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');
            $cancel     =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "4" AND finish_at BETWEEN "'.$dates['dateIni'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');

            $act        =   ($actives <> false) ?   ISPController::HtmlLoad($actives, 1)   : '';
            $pro        =   ($process <> false) ?   ISPController::HtmlLoad($process, 2)   : '';
            $sus        =   ($suspend <> false) ?   ISPController::HtmlLoad($suspend, 3)   : '';
            $can        =   ($cancel <> false) ?    ISPController::HtmlLoad($cancel, 4)    : '';
            
            return $app->json(array(
                'status'    =>  true, 
                'actives'   =>  $act,
                'process'   =>  $pro,
                'suspend'   =>  $sus,
                'cancel'    =>  $can,
            ));
        }else{

            $html = "";
            $actives    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "1" AND created_at BETWEEN "'.$dates['dateEnd'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');
            $process    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "2" AND finish_at BETWEEN "'.$dates['dateEnd'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');
            $suspend    =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "3" AND finish_at BETWEEN "'.$dates['dateEnd'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');
            $cancel     =   DBSmart::DBQueryAll('SELECT ticket, id_mw, title, category_id, status_id, operator_id, created_at, finish_at FROM cp_tickets_isp WHERE status_id = "4" AND finish_at BETWEEN "'.$dates['dateEnd'].'" AND "'.$dates['dateEnd'].'" ORDER BY ID DESC');

            $act        =   ($actives <> false) ?   ISPController::HtmlLoad($actives, 1)   : '';
            $pro        =   ($process <> false) ?   ISPController::HtmlLoad($process, 2)   : '';
            $sus        =   ($suspend <> false) ?   ISPController::HtmlLoad($suspend, 3)   : '';
            $can        =   ($cancel <> false) ?    ISPController::HtmlLoad($cancel, 4)    : '';
            
            return $app->json(array(
                'status'    =>  true, 
                'actives'   =>  $act,
                'process'   =>  $pro,
                'suspend'   =>  $sus,
                'cancel'    =>  $can,
            ));
        }

        return $app->json(array(
            'status'    =>  true, 
            'pending'   =>  $info['pending'],
            'processed' =>  $info['processed'],
            'cancel'    =>  $info['cancel'],
            'errorTV'   =>  $info['errortv']
        ));
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function Reports(Application $app, Request $request)
    {
        $_replace   =   new Config();
        
        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($_GET['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($_GET['dateEnd'])
        ];

        $res  =   DBSmart::DBQueryAll('SELECT CONCAT("ISP",t1.ticket) AS ticket, t1.id_mw AS Mikrowisp, t1.title AS Titulo, t1.message AS Cuerpo, t3.name AS Categoria, t4.name AS Status, t2.username AS Operador, t1.created_at AS Creacion, t1.finish_at AS Finalizacion FROM cp_tickets_isp t1 INNER JOIN users t2 ON (t1.operator_id = t2.id) INNER JOIN data_isp_type t3 ON (t1.category_id = t3.id) INNER JOIN data_isp_status t4 ON (t1.status_id = t4.id) AND t1.finish_at BETWEEN "'.$dates['dateIni'].'" AND "'.$dates['dateEnd'].'"');

        $columnNames = array();

        if(!empty($res))
        {
            $firstRow = $res[0];
            foreach($firstRow as $colName => $val){
                $columnNames[] = $colName;
            }
        }

        $fileName = 'Reporting - ISP TICKET - '.date("m-d-Y H:m:s").'.csv';

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
         
        $fp = fopen('php://output', 'w');
         
        fputcsv($fp, $columnNames);

        foreach ($res as $row) { fputcsv($fp, $row);   }
         
        fclose($fp);

        return $app->json(array('status'    => true));

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function Dashboard(Application $app, Request $request)
    {
        return $app['twig']->render('ISP/dashboard.html.twig',array(
            'sidebar'   =>  true,
            'status'    =>  DBSmart::DBQueryAll('SELECT id, name FROM data_isp_status ORDER BY id ASC')
        ));
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function DashboardLoad(Application $app, Request $request)
    {
        $html = "";
        $categories =   DBSmart::DBQueryAll('SELECT id, name, status_id FROM data_isp_type      ORDER BY id ASC');
        $labels     =   DBSmart::DBQueryAll('SELECT id, name, status_id FROM data_isp_labels    ORDER BY id ASC');
        $emails     =   DBSmart::DBQueryAll('SELECT id, email, status_id FROM data_isp_emails   ORDER BY id ASC');

        $cat        =   ($categories <> false) ?    ISPController::HtmlDashLoad($categories, 1) : '';
        $lab        =   ($labels <> false) ?        ISPController::HtmlDashLoad($labels, 2)     : '';
        $ema        =   ($emails <> false) ?        ISPController::HtmlDashLoad($emails, 3)     : '';
        
        return $app->json(array(
            'status'        =>  true, 
            'categories'    =>  $cat,
            'labels'        =>  $lab,
            'emails'        =>  $ema,
        ));

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function NewCategories(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        $insert =   DBSmart::DataExecute('INSERT INTO data_isp_type (name, status_id, created_at) VALUES("'.strtoupper($_replace->Clean($params['cat_name'])).'", "'.$params['cat_status'].'", "'.date("Y-m-d").'")');        
        if($insert)
        {
            return $app->json(array(
                'status'        =>  true, 
            ));

        }else{
            return $app->json(array(
                'status'        =>  false, 
                'message'       =>  "Al intentar almacenar la Categoria."
            ));            
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function NewLables(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        $insert =   DBSmart::DataExecute('INSERT INTO data_isp_labels (name, status_id, created_at) VALUES("'.strtoupper($_replace->Clean($params['lab_name'])).'", "'.$params['lab_status'].'", "'.date("Y-m-d").'")');        
        if($insert)
        {
            return $app->json(array(
                'status'        =>  true, 
            ));

        }else{
            return $app->json(array(
                'status'        =>  false, 
                'message'       =>  "Al intentar almacenar la Etiqueta."
            ));            
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function NewEmails(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        $insert =   DBSmart::DataExecute('INSERT INTO data_isp_emails (email, status_id, created_at) VALUES("'.strtoupper($_replace->Clean($params['email_name'])).'", "'.$params['email_status'].'", "'.date("Y-m-d").'")');        
        if($insert)
        {
            return $app->json(array(
                'status'        =>  true, 
            ));

        }else{
            return $app->json(array(
                'status'        =>  false, 
                'message'       =>  "Al intentar almacenar Email."
            ));            
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function ShowCategories(Application $app, Request $request)
    {
        $search     =   DBSmart::DBQuery('SELECT id, name FROM data_isp_type WHERE id = "'.$request->get('id').'"');
        if($search)
        {
            return $app->json(array(
                'status'    =>  true, 
                'id'        =>  $search['id'],
                'name'      =>  $search['name'],
            ));

        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'       =>  "No se encontro Categoria Seleccionada"
            ));

        }
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function ShowLables(Application $app, Request $request)
    {
        $search     =   DBSmart::DBQuery('SELECT id, name FROM data_isp_labels WHERE id = "'.$request->get('id').'"');
        if($search)
        {
            return $app->json(array(
                'status'    =>  true, 
                'id'        =>  $search['id'],
                'name'      =>  $search['name'],
            ));

        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'       =>  "No se encontro Etiqueta Seleccionada"
            ));

        }
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function ShowEmails(Application $app, Request $request)
    {
        $search     =   DBSmart::DBQuery('SELECT id, email FROM data_isp_emails WHERE id = "'.$request->get('id').'"');
        if($search)
        {
            return $app->json(array(
                'status'    =>  true, 
                'id'        =>  $search['id'],
                'email'     =>  $search['email'],
            ));

        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'       =>  "No se encontro Email Seleccionado"
            ));

        }
    
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function UpdateCategories(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        $update     =   DBSmart::DataExecute('UPDATE data_isp_type SET name = "'.strtoupper($_replace->Clean($params['cat_e_name'])).'", status_id = "'.$params['cat_e_status'].'" WHERE id = "'.$params['cat_e_id'].'" '); 
        if($update)
        {
            return $app->json(array(
                'status'        =>  true, 
            ));

        }else{
            return $app->json(array(
                'status'        =>  false, 
                'message'       =>  "Al intentar Actualizar la Categoria."
            ));            
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function UpdateLables(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        $update     =   DBSmart::DataExecute('UPDATE data_isp_labels SET name = "'.strtoupper($_replace->Clean($params['lab_e_name'])).'", status_id = "'.$params['lab_e_status'].'" WHERE id = "'.$params['lab_e_id'].'" ');        
        if($update)
        {
            return $app->json(array(
                'status'        =>  true, 
            ));

        }else{
            return $app->json(array(
                'status'        =>  false, 
                'message'       =>  "Al intentar almacenar la Etiqueta."
            ));            
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function UpdateEmails(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        $update     =   DBSmart::DataExecute('UPDATE data_isp_emails SET email = "'.strtolower($_replace->Clean($params['email_e_name'])).'", status_id = "'.$params['email_e_status'].'" WHERE id = "'.$params['email_e_id'].'" ');        
        if($update)
        {
            return $app->json(array(
                'status'        =>  true, 
            ));

        }else{
            return $app->json(array(
                'status'        =>  false, 
                'message'       =>  "Al intentar Actualizar el Email."
            ));            
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////



}