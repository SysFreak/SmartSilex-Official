<?php
namespace App\Controllers;
require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\Score;
use App\Models\ApprovalsRespCancel;

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
* 
*/
class ConfigController extends BaseController
{
	// public static function index(Application $app)
	// {	

 //     	return $app['twig']->render('config/index.html.twig',array(
 //            'sidebar'   	    =>  true,
 //            'users'		        =>  User::GetUsers(),
 //            'departaments'    =>  Departament::GetDepartament(),
 //            'roles'	          =>  Role::GetRole(),
 //            'status'          =>  Status::GetStatus(),
 //            'teams'           =>  Team::GetTeams(),
 //            'dids'		        =>  Did::GetDid(),
 //            'towns'           =>  Town::GetTown(),
 //            'zips'            =>  ZipCode::GetZipCode(),
 //            'countrys'        =>  Country::GetCountry(),
 //            'houses'          =>  House::GetHouse(),
 //            'levels'          =>  Level::GetLevel(),
 //            'ceilings'        =>  Ceiling::GetCeiling(),
 //            'banks'           =>  Bank::GetBank(),
 //            'payments'        =>  Payment::GetPayment(),
 //            'services'        =>  Service::GetService(),
 //            'providers'       =>  Provider::GetProvider(),
 //            'packages'        =>  Package::GetPackage(),
 //            'dvrs'            =>  Dvr::GetDvr(),
 //            'cameras'         =>  Camera::GetCamera(),
 //            'internets'       =>  Internet::GetInternet(),
 //            'phones'          =>  Phone::GetPhone(),
 //            'inttypes'        =>  InternetType::GetInternetType(),
 //            'objections'      =>  Objection::GetObjection()
 //      ));
	
 //      }

//////////////////////////////////////////////////////////////////////////////////////////

      public static function Approvals(Application $app, Request $request)
      {
            return $app['twig']->render('config/approvals.html.twig',array(
                  'sidebar'   =>    true,
                  'scores'    =>    Score::GetScore(),
                  'cancels'   =>    ApprovalsRespCancel::GetApproCancels(),
                  'services'  =>    Service::GetService(),
                  'countrys'  =>    Country::GetCountry(),
                  'status'    =>    Status::GetStatus(),
            ));

      }

//////////////////////////////////////////////////////////////////////////////////////////

      public static function ScoreEdit(Application $app, Request $request)
      {
        return $app->json(array(
            'status'    => true, 
            'score'     => Score::GetScoreId($request->get('id'))
        ));

      }

//////////////////////////////////////////////////////////////////////////////////////////

      public static function ScoreForm(Application $app, Request $request)
      {
            $params     =   [];

            parse_str($request->get('value'), $params);

            if($params['type_sc'] == 'new')
            {
                  $saveDep = Score::SaveService($params);
                      
                  if($saveDep == true)
                  { 
                      $info = array('client' => '', 'channel' => 'Score Nuevo', 'message' => 'Creacion de Nuevo Score - '.strtoupper($params['name_sc']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                      $app['datalogger']->RecordLogger($info);

                      return $app->json(array(
                          'status'   => true, 
                          'web'      => 'service'
                      ));

                  }else{

                      $info = array('client' => '', 'channel' => 'Score Nuevo', 'message' => 'Error Al Intentar Crear Nuevo Score - '.strtoupper($params['name_sc']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                      $app['datalogger']->RecordLogger($info);

                      return $app->json(array(
                          'status'    => false, 
                          'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                      )); 
                  }

            }
            elseif($params['type_sc'] == 'edit')
            {

            $saveDep = Score::SaveService($params);

            if($saveDep == true)
            {
                $info = array('client' => '', 'channel' => 'Score Edicion', 'message' => 'Actualizacion de Score - '.strtoupper($params['name_sc']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'service'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Score Edicion', 'message' => 'Error Al Intentar Actualizar informacion del Score - '.strtoupper($params['name_sc']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            } 

            }
      
      }

//////////////////////////////////////////////////////////////////////////////////////////

      public static function CancelEdit(Application $app, Request $request)
      {
        return $app->json(array(
            'status'    => true, 
            'cancel'    => ApprovalsRespCancel::GetAproCancelById($request->get('id'))
        ));

      }

//////////////////////////////////////////////////////////////////////////////////////////

      public static function CancelForm(Application $app, Request $request)
      {
            $params     =   [];

            parse_str($request->get('value'), $params);

            if($params['type_can'] == 'new')
            {
                  $saveDep = ApprovalsRespCancel::SaveService($params);
                      
                  if($saveDep == true)
                  { 
                      $info = array('client' => '', 'channel' => 'Objecion de Cancelacion Nuevo', 'message' => 'Creacion de Nueva Objecion de Cancelacion - '.strtoupper($params['name_can']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                      $app['datalogger']->RecordLogger($info);

                      return $app->json(array(
                          'status'   => true
                      ));

                  }else{

                      $info = array('client' => '', 'channel' => 'Objecion de Cancelacion Nuevo', 'message' => 'Error Al Intentar Crear Objecion de Cancelacion - '.strtoupper($params['name_can']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                      $app['datalogger']->RecordLogger($info);

                      return $app->json(array(
                          'status'    => false, 
                          'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                      )); 
                  }

            }
            elseif($params['type_can'] == 'edit')
            {

            $saveDep = ApprovalsRespCancel::SaveService($params);

            if($saveDep == true)
            {
                $info = array('client' => '', 'channel' => 'Objecion de Cancelacion Edicion', 'message' => 'Actualizacion de Objecion de Cancelacion - '.strtoupper($params['name_can']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Objecion de Cancelacion Edicion', 'message' => 'Error Al Intentar Actualizar Objecion de Cancelacion - '.strtoupper($params['name_can']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            } 

            }
      
      }


//////////////////////////////////////////////////////////////////////////////////////////

}