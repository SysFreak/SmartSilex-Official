<?php

namespace App\Controllers;
require '../vendor/autoload.php';


use App\Models\Auth;
use App\Models\User;
use App\Models\UserRol;
use App\Models\Status;
use App\Models\Departament;
use App\Models\Service;
use App\Models\Role;
use App\Models\Did;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

class RoleController extends BaseController
{

    public function index(Application $app, Request $request)
    {
        return $app['twig']->render('rols/index.html.twig',array(
            'sidebar'       =>  true,
            'departaments'  =>  Departament::GetDepartament(),
            'status'        =>  Status::GetStatus(),
            'dids'          =>  Did::GetDid(),
            'roles'         =>  Role::GetRole(),
            'services'      =>  Service::GetService(),

        ));
    }

    public function RolEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'rol'       =>  Role::GetRolById($request->get('id')),
            'roles'     =>  UserRol::GetRolsByRol($request->get('id'))
        ));
    
    }

    public function RolForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_r'] == 'new')
        {
            $saveDep = Role::SaveRol($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Rol New', 'message' => 'Creacion de Rol - '.strtoupper($params['name_r']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);
                
                $iData =    [
                    'rol'           =>   $saveDep,
                    'type'          =>   $params['type_r'],
                    'calendar'      =>   (isset($params['calendar_r']))       ? $params['calendar_r']       : 'off',
                    'operator'      =>   (isset($params['operator_r']))       ? $params['operator_r']       : 'off',
                    'lead'          =>   (isset($params['lead_r']))           ? $params['lead_r']           : 'off',
                    'leader'        =>   (isset($params['leaders_r']))        ? $params['leaders_r']        : 'off',
                    'marketing'     =>   (isset($params['marketing_r']))      ? $params['marketing_r']      : 'off',
                    'creative'      =>   (isset($params['creative_r']))       ? $params['creative_r']       : 'off',
                    'reporting'     =>   (isset($params['reporting_r']))      ? $params['reporting_r']      : 'off',
                    'config'        =>   (isset($params['config_r']))         ? $params['config_r']         : 'off',
                    'data_entry'    =>   (isset($params['data_entry_r']))     ? $params['data_entry_r']     : 'off',
                    'approvals'     =>   (isset($params['approvals_r']))      ? $params['approvals_r']      : 'off',
                    'coordinations' =>   (isset($params['coordinations_r']))  ? $params['coordinations_r']  : 'off',
                    'ticket'        =>   (isset($params['ticket_r']))         ? $params['ticket_r']         : 'off',
                    'support'       =>   (isset($params['support_r']))        ? $params['support_r']        : 'off',
                    'collection'    =>   (isset($params['collection_r']))     ? $params['collection_r']     : 'off',
                    'contracts'     =>   (isset($params['contracts_r']))      ? $params['contracts_r']      : 'off',
                ];

                $SaveRol    =   UserRol::SaveUserRol($iData);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'roles'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Rol New', 'message' => 'Error Al Intentar Crear Rol - '.strtoupper($params['name_r']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            
            }
        
        }
        elseif($params['type_r'] == 'edit')
        {

            $saveDep = Role::SaveRol($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Rol Edit', 'message' => 'Actualizacion de Rol - '.strtoupper($params['name_r']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                $iData =    [
                    'id_r'          =>   $params['id_r'],
                    'rol'           =>   $params['id_r'],
                    'type'          =>   $params['type_r'],
                    'calendar'      =>   (isset($params['calendar_r']))       ? $params['calendar_r']       : 'off',
                    'operator'      =>   (isset($params['operator_r']))       ? $params['operator_r']       : 'off',
                    'lead'          =>   (isset($params['lead_r']))           ? $params['lead_r']           : 'off',
                    'leader'        =>   (isset($params['leaders_r']))        ? $params['leaders_r']        : 'off',
                    'marketing'     =>   (isset($params['marketing_r']))      ? $params['marketing_r']      : 'off',
                    'creative'      =>   (isset($params['creative_r']))       ? $params['creative_r']       : 'off',
                    'reporting'     =>   (isset($params['reporting_r']))      ? $params['reporting_r']      : 'off',
                    'config'        =>   (isset($params['config_r']))         ? $params['config_r']         : 'off',
                    'data_entry'    =>   (isset($params['data_entry_r']))     ? $params['data_entry_r']     : 'off',
                    'approvals'     =>   (isset($params['approvals_r']))      ? $params['approvals_r']      : 'off',
                    'coordinations' =>   (isset($params['coordinations_r']))  ? $params['coordinations_r']  : 'off',
                    'ticket'        =>   (isset($params['ticket_r']))         ? $params['ticket_r']         : 'off',
                    'support'       =>   (isset($params['support_r']))        ? $params['support_r']        : 'off',
                    'collection'    =>   (isset($params['collection_r']))     ? $params['collection_r']     : 'off',
                    'contracts'     =>   (isset($params['contracts_r']))      ? $params['contracts_r']      : 'off',
                ];

                $SaveRol    =   UserRol::SaveUserRol($iData);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'roles'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Rol Edit', 'message' => 'Error Al Intentar Actualizar informacion del Rol - '.strtoupper($params['name_r']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }   
        }    
    }
}