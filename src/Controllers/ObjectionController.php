<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Country;
use App\Models\Service;
use App\Models\Status;
use App\Models\Objection;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class ObjectionController extends BaseController
{
    
    public static function ObjectionEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'        => true, 
            'objection'     => Objection::GetObjectionById($request->get('id'))
        ));
    
    }

    public function ObjectionForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_obj'] == 'new')
        {
            
            $saveDep = Objection::SaveObjection($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Objection New', 'message' => 'Creacion de Objeciones - '.strtoupper($params['name_obj']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'packages'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Objection New', 'message' => 'Error Al Intentar Crear Objeciones - '.strtoupper($params['name_obj']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_obj'] == 'edit')
        {

            $saveDep = Objection::SaveObjection($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Objection Edit', 'message' => 'Actualizacion de Objeciones - '.strtoupper($params['name_obj']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'packages'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Objection Edit', 'message' => 'Error Al Intentar Actualizar informacion del Objeciones - '.strtoupper($params['name_obj']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
            
        }
    
    }

}