<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Provider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class ProviderController extends BaseController
{
    
    public static function ProviderEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'provider'  => Provider::GetProviderById($request->get('id'))
        ));
    
    }

    public function ProviderForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_pr'] == 'new')
        {
            $saveDep = Provider::SaveProvider($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Provider New', 'message' => 'Creacion de Paquete - '.strtoupper($params['name_pr']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'packages'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Provider New', 'message' => 'Error Al Intentar Crear Paquete - '.$params['username'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }        
        }
        elseif($params['type_pr'] == 'edit')
        {
            $saveDep = Provider::SaveProvider($params);
        
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Provider Edit', 'message' => 'Actualizacion de Paquete - '.strtoupper($params['name_pr']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'packages'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Provider Edit', 'message' => 'Error Al Intentar Actualizar informacion del Paquete - '.strtoupper($params['name_pr']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, <strong>Datos Duplicados</strong>, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
            
        }
    
    }

}