<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SplFileObject;
use SplTempFileObject;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;

use App\Models\CollectionVE;
use App\Models\User;

use App\Lib\Config;
use App\Lib\DBPbx;
use App\Lib\DBSugar;
use App\Lib\DBSmart;
use App\Lib\DBMikroVE;


use App\Lib\ApiMWVz;

/**
 * 
 */
class CollectionController extends BaseController
{
	
	public static function index(Application $app, Request $request)
	{
		return $app['twig']->render('collection/index.html.twig',array(
            'sidebar'   =>  true,
            'date'		=>	date("m/d/Y")
        ));
	}

///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// VENEZUELA //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

	public static function indexVE(Application $app, Request $request)
	{

		return $app['twig']->render('collection/VeCollection.html.twig',array(
            'sidebar'   =>  true,
            'date'		=>	date("m/d/Y")
        ));
	}

	public static function LoadVE(Application $app, Request $request)
	{
		$clients 	=	CollectionVE::GetClientsMW();
		$pending 	=	CollectionVE::SearchAllConsolidateStatus("0");
		$consolide 	=	CollectionVE::SearchAllConsolidateStatus("1");
		$proccess 	=	CollectionVE::SearchAllConsolidateStatus("2");

		$bills 		=	CollectionVE::GetBillsMw();
		$reports 	=	CollectionVE::ClientsReportsPay();

		return $app->json(array(
	        'status'    => 	true, 
	        'clAct'		=>	CollectionVE::ClientsMWtHtml($clients['active']),
	        'clSus'		=>	CollectionVE::ClientsMWtHtml($clients['suspendidos']),
	        'coCon'		=>	CollectionVE::ConsolidateHtml($consolide),
	        'coPen'		=>	CollectionVE::ConsolidateHtmlPend($pending),
	        'coPro'		=>	CollectionVE::ProcessHtml($proccess),
	        'clPen'		=>	CollectionVe::ClientsReporttHtml($reports),
	        'blPen'		=>	CollectionVe::BIllsMWtHtmlRes($bills),

		));
	
	}

	public static function UploadVE(Application $app, Request $request)
	{
		$file 	=	CollectionVE::SaverUploadFiles($_FILES);

		if($file == true)
		{
			$info = array('client' => '', 'channel' => 'CollectionVE - Pre Upload File', 'message' => 'Carga de Archivo Proceado Correctamente - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status' 	=> 	true,
				'html'		=>	CollectionVE::UploadFilesHtml()
			));

		}else{

			$info = array('client' => '', 'channel' => 'CollectionVE - Pre Upload File - Error', 'message' => 'Error al Intentar Cargar Archivo - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status' 	=> 	false,
				'title'		=>	'Error',
				'content'	=> 	'Error al Intentar cargar el archivo, intenta nuevamente'
			));
		}
	
	}

	public static function EditAmountCode(Application $app, Request $request)
	{

		$iData 	=	['id' => $request->get('id'), 'amount' => $request->get('amount')];

		$update	=	CollectionVE::EditAmountCode($iData);

		if($update == true)
		{
			return $app->json(array(
				'status' 	=> 	true,
				'title'		=>	'Success',
				'content'	=> 	'Monto Actualizado Correctamente'
			));
		}else{
			return $app->json(array(
				'status' 	=> 	false,
				'title'		=>	'Error',
				'content'	=> 	'Error al Intentar actualizar el monto, por favor intente nuevamente'
			));
		}
	}

	public static function ProcessCodeTemp(Application $app, Request $request)
	{
		$temp 	= 	CollectionVE::SearchCodesTmp();

		if($temp <> false)
		{

			foreach ($temp as $t => $tmp) 
			{
				$code 	=	CollectionVE::SearchCodeConsolidate($tmp['code']);

				if($code == false)
				{
					$cIdata 	=	CollectionVE::InsertCodeConsolidate($tmp, $app['session']->get('id'));
					
					$res[$t]	=	['Transaccion' => $tmp['code'], 'fecha' => $tmp['fecha'], 'amount' => $tmp['amount'], 'status' => "NEW"];

					$cTmp 		=	CollectionVE::InsertTmpPayment($res[$t]);

				}else{

					$res[$t]	=	['Transaccion' => $tmp['code'], 'fecha' => $tmp['fecha'], 'amount' => $tmp['amount'], 'status' => "DUPLICATE"];

					$cTmp 		=	CollectionVE::InsertTmpPayment($res[$t]);
				}

			}

			$process 	=	CollectionVE::SearchAllConsolidateStatus("1");
			$pending 	=	CollectionVE::SearchAllConsolidateStatus("0");

			$info = array('client' => '', 'channel' => 'CollectionVE - Upload File - Correcto', 'message' => 'Almacenamiento de Codigos - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	true,
				'title'		=>	'Success',
				'content'	=> 	'Procesado Correctamente',
				'cHtml'		=>	CollectionVE::ConsolidateHtml($process),
				'pHtml'		=>	CollectionVE::ConsolidateHtmlPend($pending)
			));

		}else{

			$info = array('client' => '', 'channel' => 'CollectionVE - Upload File - Error', 'message' => 'Almacenamiento de Codigos - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array( 
				'status' 	=> 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Datos Para Procesar'
			));
		}
	
	}

	public static function ReportCodeTemp(Application $app, Request $request)
	{
	
		$res 	= 	CollectionVE::SearchTmpPayment();

		$columnNames = array();

        if(!empty($res))
        {
            $firstRow = $res[0];
            foreach($firstRow as $colName => $val){
                $columnNames[] = $colName;
            }

	        $query      = "TRUNCATE TABLE cp_mw_payment_tmp";
	        $truncate   = DBSmart::DataExecute($query);

	        $query      = "TRUNCATE TABLE cp_mw_payment_tmp2";
	        $truncate   = DBSmart::DataExecute($query);
        
        }

        $fileName = 'Reporting - Upload File - '.date("m-d-Y H:m:s").'.csv';

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
         
        $fp = fopen('php://output', 'w');
         
        fputcsv($fp, $columnNames);

        foreach ($res as $row) { fputcsv($fp, $row);   }
         
        fclose($fp);

        return $app->json(array('status'    => true));

	}

	public static function ConsolideProccess(Application $app, Request $request)
	{
		$fClient 	=	CollectionVE::ClientsReportsPay();

		$tPayment 	=	CollectionVE::SearchAllConsolidate();

		if(($fClient <> false) && ($tPayment <> false))
		{
	        $fClient    =   CollectionVE::ClientsReportsPay();

	        $tPayment   =   CollectionVE::SearchAllConsolidate();

	        foreach ($fClient as $f => $fc) 
	        {
	            foreach ($tPayment as $t => $tp) 
	            {
	                if(strpos(substr(strtoupper($fc['transaccion']), -5), substr(strtoupper($tp['code']), -5) ) !== false)
	                {
						$tTotal[$f]	=	[
							'id' 		=>	$tp['id'], 
							'client'	=>	$fc['idcliente'],
							'asunto'	=>	$fc['asunto'],
							'name'		=>	CollectionVE::GetClientsData($fc['idcliente'])['nombre'],
							'fdate'		=>	$fc['fecha'],
							'code' 		=> 	$tp['code'],
							'factura'	=>	$fc['nfactura'],
							'total'		=>	$fc['total'],
							'amount'	=>	$tp['amount'],
							'status'	=>	"1"
						];

						$process 	=	CollectionVE::UpdateCodeConsolidateGen($tTotal[$f]);

					}
				}
			}

			$process 	=	CollectionVE::SearchAllConsolidateStatus("1");
			$pending 	=	CollectionVE::SearchAllConsolidateStatus("0");

			$info = array('client' => '', 'channel' => 'CollectionVE - Conciliacion - Correcto', 'message' => 'Proceso de Conciliacion - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	true,
				'title'		=>	'Success',
				'content'	=> 	'Consolidacion Completa',
				'cHtml'		=>	CollectionVE::ConsolidateHtml($process),
				'pHtml'		=>	CollectionVE::ConsolidateHtmlPend($pending)
			));

		}else{

			$info = array('client' => '', 'channel' => 'CollectionVE - Conciliacion - Error', 'message' => 'Proceso de Conciliacion - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Informaci&oacute;n para Consiliar',
			));
		}

	
	}

	public static function EditConsProc(Application $app, Request $request)
	{
		$iData 	=	CollectionVE::SearchIdConsolidate($request->get('id'));

		return $app->json(array(
			'status'    => 	true,
			'html'		=>	CollectionVE::EditConsolidateHTML($iData),
		));
	}

	public static function UpdateCons(Application $app, Request $request)
	{
		$iData 	=	['id'	=>	$request->get('id'), 'amount' 	=>	$request->get('amount')];

		$iRes 	=	CollectionVE::UpdateConsolidateStatus($iData);
		
		$Data 	=	CollectionVE::SearchIdConsolidate($request->get('id'));

		if($iRes <> false)
		{
			$process 	=	CollectionVE::SearchAllConsolidateStatus("1");
			$pending 	=	CollectionVE::SearchAllConsolidateStatus("0");

			$info = array('client' => $Data['idcliente'], 'channel' => 'CollectionVE - Conciliacion Edicion - Correcto - Code '.$Data['code'].'', 'message' => 'Proceso de Edicion de Conciliacion - Codigo: '.$Data['code'].' - Client '.$Data['idcliente'].' - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	true,
				'title'		=>	'Success',
				'content'	=> 	'Actualizaci&oacute;n Procesada',
				'cHtml'		=>	CollectionVE::ConsolidateHtml($process),
				'pHtml'		=>	CollectionVE::ConsolidateHtmlPend($pending)
			));

		}else{
			
			$info = array('client' => $Data['idcliente'], 'channel' => 'CollectionVE - Conciliacion Edicion - Error - - Code '.$Data['code'].'', 'message' => 'Proceso de Edicion de Conciliacion - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Error en Proceso, Intente Nuevamente',
			));
		}
	}

	public static function ProcConsProc(Application $app, Request $request)
	{

		$iData 	=	CollectionVE::SearchIdConsolidate($request->get('id'));

		$data 	=	new Config();

		$mkw    =   $data->ApiMikroVE();

		$url 	=	$mkw['url'].'/PaidInvoice';

        $fields     =   [
            'token'         =>  $mkw['token'],
            'idfactura'     =>  $iData['nfactura'],
            'pasarela'      =>  "Pago Electronico",
            'cantidad'      =>  $iData['amount'],
            'comision'      =>  "0",
            'idtransaccion' =>  $iData['code'],
            'fecha'         =>  $iData['cdate']
        ];

        $payment 	=	ApiMWVz::Curl($fields, $url);

        if($payment['estado'] == "exito")
        {
        	$update 	=	CollectionVE::UpdateConsolidateIdPayment($request->get('id'));
        	$updateMW 	=	CollectionVE::UpdateReportMW($iData['nfactura']);

        	$nData 		=	[
        		'cliente'	=>	$iData['idcliente'],
        		'client'	=>	$iData['idcliente'],
        		'code' 		=> 	$iData['code'],
        		'total' 	=> 	$iData['amount'],
        		'fecha' 	=> 	$iData['cdate'],
        		'factura' 	=> 	$iData['nfactura'],
        		'user' 		=>	$app['session']->get('username') 
        	];

        	$note 		=	CollectionVE::NoteMikrowisp($nData, "2");

			$info = array('client' => $nData['cliente'], 'channel' => 'CollectionVE - Procesamiento de Conciliacion - Correcto', 'message' => 'Proceso de Conciliacion - Factura '.$nData['factura'].' - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	true,
				'title'		=>	'Success',
				'content'	=> 	'Pago Realizado Correctamente'
			));

		}else{

			$info = array('client' => '', 'channel' => 'CollectionVE - Procesamiento de Conciliacion - Error', 'message' => 'Proceso de Conciliacion - Factura '.$iData['nfactura'].' - Solicitado Por - '.$app['session']->get('username').' - Errado '.$payment['mensaje'].'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	$payment['mensaje'],
			));
		
		}
	
	}


	public static function AsigCode(Application $app, Request $request)
	{
		$pending 	=	CollectionVE::SearchIdConsolidate($request->get('id'));

		return $app->json(array(
			'status'    => 	true,
	        'html'		=>	CollectionVE::ConsolidateHtmlPendAsig($pending),
		));

	}

	public static function SearchMWClient(Application $app, Request $request)
	{
		$client 	=	$request->get('client');
		$query 		=	'SELECT id, nombre FROM usuarios WHERE id = "'.$client.'"';
		$result 	=	DBMikroVE::DBQuery($query);

		if($result <> false)
		{
			$query 	=	'SELECT id FROM facturas WHERE idcliente = "'.$client.'" AND estado = "No pagado"';
			$bills 	=	DBMikroVE::DBQueryAll($query);

			$html 	=	'';
			if($bills <> false)
			{
				$html.='<label><strong> Facturas</strong></label>';
	        	$html.='<select class="form-control" id="mw_bills" name="mw_bills" required="">';

				foreach ($bills as $k => $bil) 
				{
			        $html.='<option value="'.$bil['id'].'">'.$bil['id'].'</option>';
				}

        		$html.='</select>';
			}else{
				$html.='<label><strong> Facturas</strong></label>';
				$html.='<select class="form-control" id="mw_bills" name="mw_bills" required="" readonly>';
				$html.='<option value="">FACTURAS</option>';
				$html.='</select>';
			}

			return $app->json(array(
		        'status'    => 	true, 
		        'id'		=>	$result['id'],
		        'name'		=>	$result['nombre'],
		        'bills'		=>	$html
			)); 
		}else{
			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"No se encuentra cliente en Mikrowisp, por favor intente nuevamente."
			)); 

		}
	}

	public static function UpdateAsigned(Application $app, Request $request)
	{

		if(!$_FILES['file']['name'] == "")
		{

			$_replace 	= 	new Config();

			$filename 	=   $_FILES["file"]["name"];
       		$img        =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$filename);

			$fileAdd    =   [
				'name'  =>  $filename,
				'size'  =>  $_FILES["file"]['size'],
				'type'  =>  $_FILES["file"]["type"],
				'dir'   =>  "assets/upload/".$filename
			];

			$query 	=	'SELECT * FROM facturas WHERE id = "'.$_POST['mw_bills'].'"';
			$bills 	=	DBMikroVE::DBQuery($query);
			
			$conso 	=	CollectionVE::SearchCodeCons($_POST['mw_code']);

			if($conso <> false)
			{
				$iData =	[
					'id'			=>	$conso['id'],
					'client'		=>	$bills['idcliente'],
					'name'			=>	$_POST['mw_name'],
					'asunto'		=>	"Pago de Factura",
					'factura'		=>	$bills['id'],
					'total'			=>	$bills['total'],
					'fdate'			=>	$bills['emitido'],
					'code'			=>	$conso['code'],
					'fecha'			=>	$conso['cdate'],
					'status'		=>	"1",
					'user'			=>	$app['session']->get('username')
				];

				$update 	=	CollectionVE::UpdateCodeConsolidate($iData, $fileAdd);

				if($update <> false)
				{
					
					$note 	=	CollectionVE::NoteMikrowisp($iData, "0");

					$info 	= 	array('client' => $iData['client'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura', 'message' => 'Asignacion de Codigo '.$iData['code'].' a Factura '.$iData['factura'].' - Realizado Correctamente - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

			        $app['datalogger']->RecordLogger($info);

					return $app->json(array(
						'status'    => 	true,
						'title'		=>	'Success',
						'content'	=> 	'Asignacion Realizada Correctamente',
					));
				
				}else{

					$info = array('client' => $iData['client'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura - Error', 'message' => 'Error en Asignacion de Codigo '.$iData['code'].' a Factura '.$iData['factura'].' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

			        $app['datalogger']->RecordLogger($info);

					return $app->json(array(
				        'status'    => 	false, 
				        'title'		=>	'Error',
				    	'content'   => 	"Error en el Proceso, Intente nuevamente"
					)); 
				
				}
			
			}else{

				$info = array('client' => $bills['idcliente'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura - Error', 'message' => 'Error en Asignacion de Codigo '.$params['mw_code'].' a Factura '.$params['mw_bills'].' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Codigo No Encontrado"
				)); 
			}

		}else{

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Debe Contener un Adjunto que valide la asignacion, Intente nuevamente.'
			));

		}

		// $params =	[];
		// parse_str($request->get('str'), $params);

		$query 	=	'SELECT * FROM facturas WHERE id = "'.$params['mw_bills'].'"';
		$bills 	=	DBMikroVE::DBQuery($query);

		$conso 	=	CollectionVE::SearchCodeCons($params['mw_code']);

		if($conso <> false)
		{
			$iData =	[
				'id'			=>	$conso['id'],
				'client'		=>	$bills['idcliente'],
				'name'			=>	$params['mw_name'],
				'asunto'		=>	"Pago de Factura",
				'factura'		=>	$bills['id'],
				'total'			=>	$bills['total'],
				'fdate'			=>	$bills['emitido'],
				'code'			=>	$conso['code'],
				'status'		=>	"1"
			];

			$update 	=	CollectionVE::UpdateCodeConsolidate($iData);

			if($update <> false)
			{
				
				$info = array('client' => $iData['client'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura', 'message' => 'Asignacion de Codigo '.$iData['code'].' a Factura '.$iData['factura'].' - Realizado Correctamente - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

				return $app->json(array(
					'status'    => 	true,
					'title'		=>	'Success',
					'content'	=> 	'Asignacion Realizada Correctamente',
				));
			}else{

				$info = array('client' => $iData['client'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura - Error', 'message' => 'Error en Asignacion de Codigo '.$iData['code'].' a Factura '.$iData['factura'].' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Error en el Proceso, Intente nuevamente"
				)); 
			}
		
		}else{
				$info = array('client' => $bills['idcliente'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura - Error', 'message' => 'Error en Asignacion de Codigo '.$params['mw_code'].' a Factura '.$params['mw_bills'].' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Codigo No Encontrado"
			)); 
		}


	}


	public static function ConsolideMultiple(Application $app, Request $request)
	{
		$params =	[];
		parse_str($request->get('str'), $params);

		$cGen 	=	$cont 	= 	$coP 	=	$coE 	=	0;

		$query 	=	'SELECT id FROM cp_mw_consolidate WHERE status_id = "1" ORDER BY id ASC';
		$iCou 	=	DBSmart::DBQueryAll($query);

		if($iCou <> false)
		{
			foreach ($iCou as $c => $cal) 
			{
				$var 	=	'CheckConso'.$cal['id'].'';

				if(isset($params[$var]) == "on")
				{
					$ident[$c] 	= 	$cal['id'];
					$cGen 		= 	$cGen+1;
				}
			}

			if($cGen == 0)
			{
				return $app->json(array(
					'status'    => 	false,
					'title'		=>	'Error',
					'content'	=> 	'Debe seleccionar al menos una opcion de la lista.',
				));

			}else{

				foreach ($ident as $i => $id) 
				{
					
					$iData 	=	CollectionVE::SearchIdConsolidate($id);

					$query 	=	'SELECT total FROM facturas WHERE id ="'.$iData['nfactura'].'"';
					$BillT 	=	DBMikroVE::DBQuery($query);

					if($iData['total'] <> $BillT['total'])
					{
						$query  =   'UPDATE cp_mw_consolidate SET total="'.$BillT['total'].'" WHERE nfactura = "'.$iData['nfactura'].'"';

        				$update =	DBSmart::DataExecute($query);
					}

					$iData 	=	CollectionVE::SearchIdConsolidate($id);

					$data 	=	new Config();

					$mkw    =   $data->ApiMikroVE();

					$url 	=	$mkw['url'].'/PaidInvoice';

			        $fields     =   [
			            'token'         =>  $mkw['token'],
			            'idfactura'     =>  $iData['nfactura'],
			            'pasarela'      =>  "Pago Electronico",
			            'cantidad'      =>  $iData['amount'],
			            'comision'      =>  "0",
			            'idtransaccion' =>  $iData['code'],
			            'fecha'         =>  $iData['cdate']
			        ];

			        $payment 	=	ApiMWVz::Curl($fields, $url);

			        if($payment['estado'] == "exito")
			        {
			        	$coP 		=	$coP + 1;
			        	$update 	=	CollectionVE::UpdateConsolidateIdPayment($id);
			        	$updateMW 	=	CollectionVE::UpdateReportMW($iData['nfactura']);

				        $nData      =   [
				            'cliente'   =>  $iData['idcliente'],
				            'client'    =>  $iData['idcliente'],
				            'code'      =>  $iData['code'],
				            'total'     =>  $iData['amount'],
				            'fecha'     =>  date("Y-m-d H:m:s"),
				            'factura'   =>  $iData['nfactura'],
				            'user'      =>  $app['session']->get('username') 
				        ];

				        $note       =   CollectionVE::NoteMikrowisp($nData, "2");
			
						$info = array('client' => $iData['idcliente'], 'channel' => 'CollectionVE - Procesamiento de Conciliacion - Correcto', 'message' => 'Proceso de Conciliacion - Factura '.$iData['nfactura'].' - Realizado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

				        $app['datalogger']->RecordLogger($info);

					}else{
						$coE 		=	$coE + 1;				
					}

				}

					return $app->json(array(
					'status'    => 	true,
					'html'		=>	'<div class="alert alert-info fade in">Exitoso: '.$coP.' | Error: '.$coE.'</div>'
				));
			
			}
		}else{

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Informaci&oacute;n Para Consolidar',
			));
		}

	}

	public static function ShowInfo(Application $app, Request $request)
	{
		$iData 	=	CollectionVE::SearchAllConsolidateStatus("1");

		if($iData <> false)
		{
			foreach ($iData as $v => $val) 
			{
				$query 	=	'SELECT total FROM facturas WHERE id ="'.$val['nfactura'].'"';

				$BillT 	=	DBMikroVE::DBQuery($query);

				if($val['total'] <> $BillT['total'])
				{
					$query  =   'UPDATE cp_mw_consolidate SET total="'.$BillT['total'].'" WHERE nfactura = "'.$val['nfactura'].'"';

					$update =	DBSmart::DataExecute($query);
				}
			}			
		}

		$clients 	=	CollectionVE::GetClientsMW();
		$pending 	=	CollectionVE::SearchAllConsolidateStatus("0");
		$consolide 	=	CollectionVE::SearchAllConsolidateStatus("1");
		$proccess 	=	CollectionVE::SearchAllConsolidateStatus("2");

		$bills 		=	CollectionVE::GetBillsMw();
		$reports 	=	CollectionVE::ClientsReportsPay();

		return $app->json(array(
			'status'    => 	true,
	        'clAct'		=>	CollectionVE::ClientsMWtHtml($clients['active']),
	        'clSus'		=>	CollectionVE::ClientsMWtHtml($clients['suspendidos']),
	        'coCon'		=>	CollectionVE::ConsolidateHtml($consolide),
	        'coPen'		=>	CollectionVE::ConsolidateHtmlPend($pending),
	        'coPro'		=>	CollectionVE::ProcessHtml($proccess),
	        'clPen'		=>	CollectionVe::ClientsReporttHtml($reports),
	        'blPen'		=>	CollectionVe::BIllsMWtHtmlRes($bills),
		));

	}

	public static function AsigRepClient(Application $app, Request $request)
	{
		$id 		=	$request->get('id');

		$client 	=	CollectionVE::ClientReportId($id);
		$pending 	=	CollectionVE::SearchAllConsolidateStatus("0");

		if($pending <> false)
		{
			return $app->json(array(
				'status'    => 	true,
		        'html'		=>	CollectionVE::AsigRepClientHtml($client, $pending)
			));

		}else{
			return $app->json(array(
				'status'    => 	false,
		        'title'		=>	"Error",
		        'content'	=>	"No existe informacion con que conciliar."
			));
		}

	}

	public static function UpdateAsigRepClient(Application $app, Request $request)
	{

		if(!$_FILES['file']['name'] == "")
		{

			$_replace 	= 	new Config();

			$filename 	=   $_FILES["file"]["name"];
       		$img        =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$filename);

			$fileAdd    =   [
				'name'  =>  $filename,
				'size'  =>  $_FILES["file"]['size'],
				'type'  =>  $_FILES["file"]["type"],
				'dir'   =>  "assets/upload/".$filename
			];


			$cGen 	=	$cont 	= 	$coP 	=	$coE 	=	0;

			$query 	=	'SELECT id, cdate FROM cp_mw_consolidate WHERE status_id = "0" ORDER BY id ASC';
			$iCou 	=	DBSmart::DBQueryAll($query);

			if($iCou <> false)
			{
				foreach ($iCou as $c => $cal) 
				{
					$var 	=	'CheckAsigRep'.$cal['id'].'';

					if(isset($_POST[$var]) == "on")
					{
						$ident 		= 	$cal['id'];
						$cGen 		= 	$cGen+1;
						$cdate 		=	$cal['cdate'];
					}
				}

				if($cGen == 0)
				{
					return $app->json(array(
						'status'    => 	false,
						'title'		=>	'Error',
						'content'	=> 	'Debe seleccionar al menos una opcion de la lista.',
					));

				}
				elseif($cGen >= 2) 
				{
					return $app->json(array(
						'status'    => 	false,
						'title'		=>	'Error',
						'content'	=> 	'Debe seleccionar Solo una opcion de la lista',
					));

				}else{

					$query 		=	'SELECT * FROM facturas WHERE id = "'.$_POST['mw_bill'].'"';
					$client 	=	DBMikroVE::DBQuery($query);

					$info 		=	CollectionVE::SearchIdConsolidate($ident);

					$iData 	=	[
						'client' 		=>	$client['idcliente'],
						'name'			=>	$_POST['mw_name'],
						'asunto'		=>	"Pago de Factura",
						'factura'		=>	$client['id'],
						'total'			=>	$client['total'],
						'fdate'			=>	$client['emitido'],
						'fecha'			=>	$cdate,
						'code'			=>	$info['code'],
						'status'		=>	"1",
						'id'			=>	$ident,
						'user'			=>	$app['session']->get('username')
					];

					$update 	=	CollectionVE::UpdateCodeConsolidate($iData, $fileAdd);

					if($update <> false)
					{

						$note 	=	CollectionVE::NoteMikrowisp($iData, "0");

						$info = array('client' => $iData['client'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura', 'message' => 'Asignacion de Codigo '.$iData['code'].' a Factura '.$iData['factura'].' - Realizado Correctamente - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

				        $app['datalogger']->RecordLogger($info);

						return $app->json(array(
							'status'    => 	true,
							'title'		=>	'Success',
							'content'	=> 	'Codigo asignado a factura, realizado correctamente'
						));
					}else{

						$info = array('client' => $iData['client'], 'channel' => 'CollectionVE - Asignacion de Codigo a Factura', 'message' => 'Asignacion de Codigo '.$iData['code'].' a Factura '.$iData['factura'].' - Realizado Correctamente - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

				        $app['datalogger']->RecordLogger($info);


						return $app->json(array(
							'status'    => 	false,
							'title'		=>	'Error',
							'content'	=> 	'Error al asignar codigo, Intente nuevamente'
						));
					}		
				}
			
			}else{
				return $app->json(array(
					'status'    => 	false,
					'title'		=>	'Error',
					'content'	=> 	'Sin Informaci&oacute;n Para Consolidar',
				));
			
			}
		
		}else{
			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Debe Contener un Adjunto que valide la asignacion, Intente nuevamente.'
			));
			
		}
	}

	public static function ShowManRepClient(Application $app, Request $request)
	{
		$info 		=	CollectionVE::ClientReportId($request->get('id'));

		if($info <> false)
		{
			return $app->json(array(
				'status'    =>  true,
				'html'		=>	CollectionVE::ShowManRepClientHtml($info),
			));			
		}else{

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'No se encuentra Informaci&oacute;n, intente nuevamente',
			));

		}
	}


	public static function UpdateProcessPendAsigned(Application $app, Request $request)
	{

		if(!$_FILES['file']['name'] == "")
		{

			$filename       =   $_FILES["file"]["name"];
       		$img            =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$filename);

			$fileAdd    =   [
				'name'  =>  $filename,
				'size'  =>  $_FILES["file"]['size'],
				'type'  =>  $_FILES["file"]["type"],
				'dir'   =>  "assets/upload/".$filename
			];

			$_replace 	= 	new Config();

			$query 	= 	'SELECT * FROM facturas WHERE id="'.$_POST['mw_bills'].'" AND estado="No pagado"';
			$bill 	=	DBMikroVE::DBQuery($query);

			$info	=	CollectionVE::ClientReportClient($_POST['mw_id']);

			if($bill <> false)
			{
				$iData 	=	[
					'cliente'		=>	$bill['idcliente'],
					'client'		=>	$bill['idcliente'],
					'nombre'		=>	$_POST['mw_name'],
					'asunto'		=>	'Pago Factura #'.$bill['id'],
					'factura'		=>	$bill['id'],
					'total'			=>	$bill['total'],
					'emitido'		=>	$bill['emitido'],
					'transaccion'	=>	$_POST['mw_code'],
					'code'			=>	$_POST['mw_code'],
					'amount'		=>	$_POST['mw_amount'],
					'fecha'			=>	$_replace->ChangeDate($_POST['mw_date']),
					'status'		=>	"1",
					'user'			=>	$app['session']->get('username')
				];

				$insert 	=	CollectionVE::InsertCodeConsolidateGen($iData, $fileAdd, $app['session']->get('id'));

				if($insert <> false)
				{
					
					$note 	=	CollectionVE::NoteMikrowisp($iData, "1");

					$info = array('client' => $iData['client'], 'channel' => 'CollectionVE - Asignacion Manual de Codigo a Factura', 'message' => 'Asignacion Manual de Codigo '.$iData['code'].' a Factura '.$iData['factura'].' - Realizado Correctamente - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

			        $app['datalogger']->RecordLogger($info);

					return $app->json(array(
						'status'    => 	true,
						'title'		=>	'Success',
						'content'	=> 	'Informacion Procesada Correctamente',
					));	

				}else{
					
					return $app->json(array(
						'status'    => 	false,
						'title'		=>	'Error',
						'content'	=> 	'Se produjo un error al intentar insertar la informacion, intente nueamente.',
					));				
				}

			}else{

				return $app->json(array(
					'status'    => 	false,
					'title'		=>	'Error',
					'content'	=> 	'Factura No se encuentra o esta en estado Pagada, Por favor verificar.',
				));
			
			}
		
		}else{
			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Debe Contener un Adjunto que valide la asignacion, Intente nuevamente.'
			));
		}


		// $params =	[];
		// parse_str($request->get('str'), $params);

	}

	public static function AsiBillPen(Application $app, Request $request)
	{
		$id 		=	$request->get('id');

		$query 		=	'SELECT id AS nfactura, (SELECT nombre FROM usuarios WHERE id=idcliente) AS nombre, idcliente FROM facturas WHERE id = "'.$id.'"';
		$client		=	DBMikroVE::DBQuery($query);

		$pending 	=	CollectionVE::SearchAllConsolidateStatus("0");

		if($pending <> false)
		{
			return $app->json(array(
				'status'    => 	true,
		        'html'		=>	CollectionVE::AsigRepClientHtml($client, $pending)
			));
			
		}else{
			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'Sin Informacion para Consiliar'
			));
		}


	}

	public static function ShowManBillPen(Application $app, Request $request)
	{
		$id 		=	$request->get('id');

		$query 		=	'SELECT id, id AS nfactura, (SELECT nombre FROM usuarios WHERE id=idcliente) AS nombre, idcliente FROM facturas WHERE id = "'.$id.'"';
		$info		=	DBMikroVE::DBQuery($query);

		if($info <> false)
		{
			return $app->json(array(
				'status'    =>  true,
				'html'		=>	CollectionVE::ShowManRepClientHtml($info),
			));			
		}else{

			return $app->json(array(
				'status'    => 	false,
				'title'		=>	'Error',
				'content'	=> 	'No se encuentra Informaci&oacute;n, intente nuevamente',
			));

		}

	}

	public static function ShowInfoClient(Application $app, Request $request)
	{
		$id 	=	$request->get('id');

		$query 	= 	'SELECT id, nombre, direccion_principal, telefono, movil, correo FROM usuarios WHERE id = "'.$id.'"';
		$info 	=	DBMikroVE::DBQuery($query);

		$query 	= 	'SELECT id, descripcion AS plan, costo, ip FROM tblservicios WHERE idcliente = "'.$id.'"';
		$serv 	=	DBMikroVE::DBQueryAll($query);

		$query 	= 	'SELECT id AS factura, legal AS fiscal, emitido, estado, total, pago FROM facturas WHERE idcliente = "'.$id.'"';
		$bills 	=	DBMikroVE::DBQueryAll($query);

		return $app->json(array(
			'status'    => 	true,
			'html'		=>	CollectionVE::ShowInfoClientHml($info, $serv, $bills),
		));

	}


///////////////////////////////////////////////////////////////////////////////
//////////////////////////////// PUERTO RICO //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

	public static function indexPR(Application $app, Request $request)
	{
		return $app['twig']->render('collection/PrCollection.html.twig',array(
            'sidebar'   =>  true,
            'date'		=>	date("m/d/Y")
        ));
	
	}

	public static function SheetCreation(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		$iData 	=	['client' => $request->get('id'), 'date' => $_replace->ChangeDate($request->get('date'))];

		$iSheet	=	SheetRecogido::SearchSheet($iData);

		if($iSheet == false)
		{
			$iMikro 	=	Mikrowisp::ClientSearch($iData);

			if($iMikro <> false)
			{
				$iClient 	=	[
					'id'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['idcliente'])))),
					'name'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['nombre'])))),
					'add'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['Direccion_Fisica'])))),
					'phone'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['telefono'])))),
					'coord'	=>	utf8_encode(trim($iMikro['coordenadas'])),
					'date'	=>	$request->get('date')
				];

				$town 	=	Town::GetTownByCountry('1');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	SheetRecogido::SheetBodyHTML($iClient, $town)
				)); 

			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp, por favor intente nuevamente."
				)); 

			}

			var_dump($iMikro);
			exit;
		}else{

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Existe una hoja previa de servicio para la fecha seleccionada, por favor intente en otro dia."
			)); 
		}

		var_dump($iSheet);
		exit;

	}
}