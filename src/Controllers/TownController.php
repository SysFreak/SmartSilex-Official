<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Country;
use App\Models\Town;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class TownController extends BaseController
{
    
    public static function TownEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'town'      => Town::GetTownById($request->get('id'))
        ));
    
    }

    public function TownForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_town'] == 'new')
        {
            
            $saveDep = Town::SaveTown($params);
            
            if($saveDep <>false)
            { 
                $info = array('client' => '', 'channel' => 'Town New', 'message' => 'Creacion de Ciudad - '.strtoupper($params['name_town']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'country'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Town New', 'message' => 'Error Al Intentar Crear Ciudad - '.strtoupper($params['name_town']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_town'] == 'edit')
        {

            $saveDep = Town::SaveTown($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Town Edit', 'message' => 'Actualizacion de Ciudad - '.strtoupper($params['name_town']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'country'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Town Edit', 'message' => 'Error Al Intentar Actualizar informacion del Ciudad - '.strtoupper($params['name_town']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }            
        }    
    }
}