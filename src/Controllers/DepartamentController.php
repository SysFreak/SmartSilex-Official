<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class DepartamentController extends BaseController
{
    
    public static function DepartEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'        => true, 
            'departament'   => Departament::GetDepById($request->get('id'))
        ));
    
    }

    public function DepartForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_d'] == 'new')
        {
            $saveDep = Departament::SaveDepartament($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Departament New', 'message' => 'Creacion de Paquete - '.strtoupper($params['name_d']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'users'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Departament New', 'message' => 'Error Al Intentar Crear Paquete - '.strtoupper($params['name_d']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        }
        elseif($params['type_d'] == 'edit')
        {
            $saveDep = Departament::SaveDepartament($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Departament Edit', 'message' => 'Actualizacion de Departamento - '.strtoupper($params['name_d']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'users'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Departament Edit', 'message' => 'Error Al Intentar Actualizar informacion del Departamento - '.strtoupper($params['name_d']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        }
    
    }

}