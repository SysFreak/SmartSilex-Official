<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Did; 
use App\Models\Reporting;
use App\Models\Leads;
use App\Models\CollectionVE;
use App\Models\CollectionPR;

use App\Lib\Config;
use App\Lib\DBMikroVE;
use App\Lib\DBMikro;
use App\Lib\DBPbx;
use App\Lib\DBPbxVz;
use App\Lib\DBPbxVzla;
use App\Lib\DBSugar;
use App\Lib\DBSmart;
use App\Lib\DBCalls;

use App\Lib\Conexion;

use Carbon\Carbon;

use PDO;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class ReportingController extends BaseController
{

    public function __construct(Application $app)
    {
       
        $curl = $app['curl']->execute(array('command' => 'license'));
        
        if($curl == 'expired'){ return $app['twig']->render('error/license.html.twig'); } 
    
    }

    public function index (Application $app, Request $request)
    {
        return $app['twig']->render('reporting/index.html.twig',array(
            'sidebar'       =>  true,
            'dids'          =>  Did::GetDid()
        ));
    
    }

    public static function ReportingCalls(Application $app, Request $request)
    {

        $query  =   'SELECT id FROM data_services WHERE status_id = "1" AND country_id = "1"';
        $serv   =   DBSmart::DBQueryAll($query);

        $hServ  =   '';
        $cont    =   0;

        foreach ($serv as $k => $val) 
        {
            $hServ.='(';
            $hServ.='services_id = "'.$val['id'].'") OR ';
        }
            $hServ = substr($hServ, 0, -4);

        $query  =   'SELECT inbound, outbound FROM data_did WHERE status_id = "1" AND ('.$hServ.') ORDER BY id DESC';
        // ddd($query);
        $did    =   DBSmart::DBQueryAll($query);

        if($did <> false)
        {
            $con        =   New DBPbx;

            $connect    =   $con->Conexion();

            foreach ($did as $d => $di) 
            {

                $query      =   'SELECT calldate, src, dst as queue, did FROM cdr WHERE did = "'.$di['inbound'].'" AND calldate BETWEEN "'.$_POST['dateTime_in'].' 00:00:00" AND "'.$_POST['dateTime_out'].' 23:59:59" GROUP BY src ORDER BY calldate DESC';

                $statement  =   $connect->prepare($query);
                $statement->execute();
                $result     =   $statement->fetchAll();

                if($result)
                {
                    foreach ($result as $r => $res) 
                    {
                        $lead   =   "";

                        $phone  = (strlen($res['src']) > 10)    ? mb_substr(trim(substr($res['src'], 0,strlen($res['src']))), -10)  :   $res['src'];

                        $query  =   'SELECT t1.name, t1.phone_main, t1.phone_alt, t1.phone_other, t2.username AS creator, t1.created_at FROM cp_leads AS t1 INNER JOIN users AS t2 ON (t1.created_by = t2.id) AND (t1.phone_main LIKE "%'.$phone.'%" OR t1.phone_alt LIKE "%'.$phone.'%") ORDER BY t1.id DESC LIMIT 1';

                        $lead   =   DBSmart::DBQuery($query);

                        $rows[$cont]  =   [
                            'calldate'  =>  $res['calldate'],
                            'src'       =>  $phone,
                            'alt'       =>  $lead['phone_alt'],
                            'other'     =>  $lead['phone_other'],
                            'queue'     =>  $res['queue'],
                            'did'       =>  $res['did'],
                            'client'    =>  (isset($lead['name']))          ?   $lead['name']       : "",
                            'creator'   =>  (isset($lead['creator']))       ?   $lead['creator']    : "",
                            'date'      =>  (isset($lead['created_at']))    ?   $lead['created_at'] : "" 
                        ];
                        $cont   =   $cont + 1;
                        $lead   =   "";

                    }
                
                }

            }

            $columnNames = array();
            if(!empty($rows)){
                $firstRow = $rows[0];
                foreach($firstRow as $colName => $val){
                    $columnNames[] = $colName;
                }
            }

            $fileName = 'Reporting-DIDS-PR - '.date("Y-d-m").'.csv';

            header('Content-Type: application/excel');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
             
            $fp = fopen('php://output', 'w');
             
            fputcsv($fp, $columnNames);

            foreach ($rows as $row) { fputcsv($fp, $row);   }
             
            fclose($fp);
        
        }

        return $app->json(array('status'    => true));

    }

    public static function ReportingCallsUS(Application $app, Request $request)
    {

        $query  =   'SELECT id FROM data_services WHERE status_id = "1" AND country_id = "2"';
        $serv   =   DBSmart::DBQueryAll($query);

        $hServ  =   '';
        $cont    =   0;

        foreach ($serv as $k => $val) 
        {
            $hServ.='(';
            $hServ.='services_id = "'.$val['id'].'") OR ';
        
        }
            $hServ = substr($hServ, 0, -4);

        $query  =   'SELECT inbound, outbound FROM data_did WHERE status_id = "1" AND ('.$hServ.') ORDER BY id DESC';
        // ddd($query);
        $did    =   DBSmart::DBQueryAll($query);

        if($did <> false)
        {
            $con        =   New DBPbx;

            $connect    =   $con->Conexion();

            foreach ($did as $d => $di) 
            {

                $query      =   'SELECT calldate, src, dst as queue, did FROM cdr WHERE did = "'.$di['inbound'].'" AND calldate BETWEEN "'.$_POST['dateTimeUS_in'].' 00:00:00" AND "'.$_POST['dateTimeUS_out'].' 23:59:59" GROUP BY src ORDER BY calldate DESC';

                $statement  =   $connect->prepare($query);
                $statement->execute();
                $result     =   $statement->fetchAll();

                if($result)
                {
                    foreach ($result as $r => $res) 
                    {
                        $lead   =   "";

                        $phone  = (strlen($res['src']) > 10)    ? mb_substr(trim(substr($res['src'], 0,strlen($res['src']))), -10)  :   $res['src'];

                        $query  =   'SELECT t1.name, t1.phone_main, t1.phone_alt, t1.phone_other, t2.username AS creator, t1.created_at FROM cp_leads AS t1 INNER JOIN users AS t2 ON (t1.created_by = t2.id) AND (t1.phone_main LIKE "%'.$phone.'%" OR t1.phone_alt LIKE "%'.$phone.'%" OR t1.phone_other LIKE "%'.$phone.'%") ORDER BY t1.id DESC LIMIT 1';

                        $lead   =   DBSmart::DBQuery($query);

                        $rows[$cont]  =   [
                            'calldate'  =>  $res['calldate'],
                            'src'       =>  $phone,
                            'alt'       =>  $lead['phone_alt'],
                            'other'     =>  $lead['phone_other'],
                            'queue'     =>  $res['queue'],
                            'did'       =>  $res['did'],
                            'client'    =>  (isset($lead['name']))          ?   $lead['name']       : "",
                            'creator'   =>  (isset($lead['creator']))       ?   $lead['creator']    : "",
                            'date'      =>  (isset($lead['created_at']))    ?   $lead['created_at'] : "" 
                        ];
                        $cont   =   $cont + 1;
                        $lead   =   "";

                    }
                
                }else{

                    $rows[$cont]  =   [
                        'calldate'  =>  '',
                        'src'       =>  '',
                        'alt'       =>  '',
                        'other'     =>  '',
                        'queue'     =>  '',
                        'did'       =>  $di['inbound'],
                        'client'    =>  '',
                        'creator'   =>  '',
                        'date'      =>  ''
                    ];
                    $cont   =   $cont + 1;
                    $lead   =   "";
                
                }

            }

            $columnNames = array();
            if(!empty($rows)){
                $firstRow = $rows[0];
                foreach($firstRow as $colName => $val){
                    $columnNames[] = $colName;
                }
            }

            $fileName = 'Reporting-DIDS-US - '.date("Y-d-m").'.csv';

            header('Content-Type: application/excel');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
             
            $fp = fopen('php://output', 'w');
             
            fputcsv($fp, $columnNames);

            foreach ($rows as $row) { fputcsv($fp, $row);   }
             
            fclose($fp);
        
        }

        return $app->json(array('status'    => true));

    }

    public static function ReportingCallsVzla(Application $app, Request $request)
    {

        $query      =   'SELECT calldate, src, dst as queue, did FROM cdr WHERE did LIKE "%'.$_POST['did'].'%" AND calldate BETWEEN "'.$_POST['dateTime_in_vzla'].' 00:00:00" AND "'.$_POST['dateTime_out_vzla'].' 23:59:59" GROUP BY src ORDER BY calldate DESC';

        $result     =   DBPbxVzla::DBQueryAll($query);

        if($result)
        {

            foreach ($result as $key => $val) 
            {

                $lead   =   "";

                $phone  =   (strlen($val['src']) > 10) ? mb_substr(trim(substr($val['src'], 0,strlen($val['src']))), -10) : $val['src'];

                $SQL    =   'SELECT name, (SELECT username FROM users WHERE id = created_by) AS creator, created_at, phone_main, phone_alt, phone_other FROM cp_leads WHERE phone_main LIKE "%'.$phone.'%" OR phone_alt LIKE "%'.$phone.'%"';

                $lead   =   DBSmart::DBQuery($SQL);

                if($lead <> false)
                {
                    $rows[$key]  =   [
                        'calldate'  =>  $val['calldate'],
                        'src'       =>  $phone,
                        'alt'       =>  $lead['phone_alt'],
                        'other'     =>  $lead['phone_other'],
                        'queue'     =>  '',
                        'did'       =>  $val['did'],
                        'client'    =>  (isset($lead['name']))          ? $lead['name']         : "",
                        'creator'   =>  (isset($lead['creator']))       ? $lead['creator']      : "",
                        'date'      =>  (isset($lead['created_at']))    ? $lead['created_at']   : "" 
                    ];
                }else{

                    $rows[$key]  =   [
                        'calldate'  =>  $val['calldate'],
                        'src'       =>  $phone,
                        'alt'       =>  '',
                        'other'     =>  '',
                        'queue'     =>  '',
                        'did'       =>  $val['did'],
                        'client'    =>  '',
                        'creator'   =>  '',
                        'date'      =>  ''
                    ];
                }
            
            }

            $columnNames = array();
            if(!empty($rows)){
                $firstRow = $rows[0];
                foreach($firstRow as $colName => $val){
                    $columnNames[] = $colName;
                }
            }

            $fileName = 'Reporting-DID-VZLA - '.$phone.' - '.date("Y-d-m").'.csv';

            header('Content-Type: application/excel');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
             
            $fp = fopen('php://output', 'w');
             
            fputcsv($fp, $columnNames);

            foreach ($rows as $row) { fputcsv($fp, $row);   }
             
            fclose($fp);

        }

        return $app->json(array('status'    => true));

    }

    public static function ReportingQueue(Application $app, Request $request)
    {

        $con        =   New DBPbx;

        $connect    =   $con->Conexion();    

        $csv = [];

        if(!$_FILES['file']['name'] == "")
        {
            $name       = $_FILES['file']['name'];
            $ext        = 'csv';
            $type       = $_FILES['file']['type'];
            $tmpName    = $_FILES['file']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                $row = 0;
                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $row++;
                    $query      =   'SELECT calldate, src, dst, duration, billsec, disposition, cnum, outbound_cnum FROM cdr where dst like "%'.$data[0].'%" AND calldate BETWEEN "'.$_POST['dateTime'].' 00:00:00" AND "'.$_POST['dateTime'].' 23:59:59" AND billsec > '.$_POST['criterion'].' order by calldate DESC';

                    $statement  =   $connect->prepare($query);

                    $statement->execute();

                    $result     =   $statement->fetch();

                    if($result)
                    {
                        $rows[$row] =    [ 
                            'number'    =>  $data[0],
                            'date'      =>  $result['calldate'],
                            'ext'       =>  $result['src'],
                            'numero'    =>  $result['dst'],
                            'total'     =>  $result['duration'],
                            'hablado'   =>  $result['billsec'],
                            'status'    =>  $result['disposition']
                        ];
                    }else{
                        $rows[$row] =    [ 
                            'number'    =>  $data[0],
                            'date'      =>  '',
                            'ext'       =>  '',
                            'numero'    =>  '',
                            'total'     =>  '',
                            'hablado'   =>  '',
                            'status'    =>  ''
                        ];
                    }
                    
                }

            }
               
                $columnNames = array();
                if(!empty($csv)){
                    $firstRow = $csv[0];
                    foreach($firstRow as $colName => $val){
                        $columnNames[] = $colName;
                    }
                }

                $fileName = 'Reporting-Queue - Criterion - '.$_POST['criterion'].' -'.$_POST['dateTime'].'.csv';

                header('Content-Type: application/excel');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                 
                $fp = fopen('php://output', 'w');
                 
                fputcsv($fp, $columnNames);

                foreach ($rows as $row) { fputcsv($fp, $row);   }
                 
                fclose($fp);

        }

        return $app->json(array('status'    => true));

    }

    public static function ReportingQueueVz(Application $app, Request $request)
    {

        $con        =   New DBPbxVz;

        $connect    =   $con->Conexion();    

        $csv = [];

        if(!$_FILES['file']['name'] == "")
        {
            $name       = $_FILES['file']['name'];
            $ext        = 'csv';
            $type       = $_FILES['file']['type'];
            $tmpName    = $_FILES['file']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                $row = 0;
                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $row++;
                    $query      =   'SELECT calldate, src, dst, duration, billsec, disposition, cnum, outbound_cnum FROM cdr where src like "%'.$data[0].'%" AND calldate BETWEEN "'.$_POST['dateTimevz'].' 00:00:00" AND "'.$_POST['dateTimevz'].' 23:59:59" AND billsec > '.$_POST['criterionvz'].' AND disposition = "ANSWERED" order by calldate DESC';

                    $statement  =   $connect->prepare($query);

                    $statement->execute();

                    $result     =   $statement->fetch();

                    if($result)
                    {
                        $rows[$row] =    [ 
                            'number'    =>  $data[0],
                            'date'      =>  $result['calldate'],
                            'ext'       =>  $result['src'],
                            'numero'    =>  $result['dst'],
                            'total'     =>  $result['duration'],
                            'hablado'   =>  $result['billsec'],
                            'status'    =>  $result['disposition']
                        ];
                    }else{
                        $rows[$row] =    [ 
                            'number'    =>  $data[0],
                            'date'      =>  '',
                            'ext'       =>  '',
                            'numero'    =>  '',
                            'total'     =>  '',
                            'hablado'   =>  '',
                            'status'    =>  ''
                        ];
                    }
                    
                }

            }
               
                $columnNames = array();
                if(!empty($csv)){
                    $firstRow = $csv[0];
                    foreach($firstRow as $colName => $val){
                        $columnNames[] = $colName;
                    }
                }

                $fileName = 'Reporting-Queue - Criterion - '.$_POST['criterionvz'].' -'.$_POST['dateTimevz'].'.csv';

                header('Content-Type: application/excel');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                 
                $fp = fopen('php://output', 'w');
                 
                fputcsv($fp, $columnNames);

                foreach ($rows as $row) { fputcsv($fp, $row);   }
                 
                fclose($fp);

        }

        return $app->json(array('status'    => true));

    }

    public static function ReportingNumb(Application $app, Request $request)
    {

        $csv = [];

        if(!$_FILES['file']['name'] == "")
        {
            $name       = $_FILES['file']['name'];
            $ext        = 'csv';
            $type       = $_FILES['file']['type'];
            $tmpName    = $_FILES['file']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                $row = 0;
                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $row++;

                    $query      =   'SELECT * FROM cp_leads WHERE phone_main = "'.$data[0].'" OR phone_alt = "'.$data[0].'" ORDER BY id DESC LIMIT 1';
                    $result     =   DBSmart::DBQuery($query);

                    if($result <> false)
                    {
                        $rows[$row] =    [ 
                            'id'        =>  $result['client_id'],
                            'nombre'    =>  $result['name'],
                            'principal' =>  $result['phone_main'],
                            'alterno'   =>  $result['phone_alt'],
                            'creado'    =>  $result['created_at']
                        ];

                    }else{
                        $rows[$row] =    [ 
                            'id'        =>  '',
                            'nombre'    =>  'NO Registrado',
                            'principal' =>  $data[0],
                            'alterno'   =>  '',
                            'creado'    =>  ''
                        ];
                    }
                    
                }

            }
               
                $columnNames = array();
                if(!empty($csv)){
                    $firstRow = $csv[0];
                    foreach($firstRow as $colName => $val){
                        $columnNames[] = $colName;
                    }
                }

                $fileName = 'Reporting-Numero Telefonico - Registrados - '.date("Y-m-d").'.csv';

                header('Content-Type: application/excel');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                 
                $fp = fopen('php://output', 'w');
                 
                fputcsv($fp, $columnNames);

                foreach ($rows as $row) { fputcsv($fp, $row);   }
                 
                fclose($fp);

        }

        return $app->json(array('status'    => true));

    }

    public static function ReportingLastNotes(Application $app, Request $request)
    {

        $conn       =   New DBSugar;

        $connect    =   $conn->Conexion();

        $query      =   'SELECT t3.first_name, t3.last_name, t3.phone_home, t2.description FROM leads_notes_1_c AS t1 INNER JOIN notes AS t2 ON (t1.leads_notes_1notes_idb = t2.id) INNER JOIN leads AS t3 ON (t1.leads_notes_1leads_ida = t3.id) AND t1.date_modified BETWEEN "'.$_POST['dateIni_ln'].' 00:00:00" AND "'.$_POST['dateEnd_ln'].' 23:59:59" ORDER BY t1.date_modified DESC';

        $statement  =   $connect->prepare($query);

        $statement->execute();

        $result     =   $statement->fetchAll(PDO::FETCH_ASSOC);

        if($result)
        {
            foreach ($result as $key => $val) 
            {
                $rows[$key]    =   [
                    'name'      =>  (isset($val['first_name'])) ? $val['first_name'].' '.$val['last_name'] : "",
                    'phone'     =>  (isset($val['phone_home'])) ? $val['phone_home'] : "",
                    'note'      =>  (isset($val['description'])) ? $val['description'] : "",
                ];
            }

            $columnNames = array();
            if(!empty($rows)){
                $firstRow = $rows[0];
                foreach($firstRow as $colName => $val){
                    $columnNames[] = $colName;
                }
            }

            $fileName = 'Reporting-LastNote - '.date("Y-d-m h:m:s").'.csv';

            header('Content-Type: application/excel');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
             
            $fp = fopen('php://output', 'w');
             
            fputcsv($fp, $columnNames);

            foreach ($rows as $row) { fputcsv($fp, $row);   }
             
            fclose($fp);
        }

        return $app->json(array('status'    => true));
    
    }

    public static function MassiveLeads(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $csv = [];
        
        if($_FILES['file_lead']['name'] <> "")
        {
            $name       = $_FILES['file_lead']['name'];
            $ext        = 'csv';
            $type       = $_FILES['file_lead']['type'];
            $tmpName    = $_FILES['file_lead']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                $row = 0;
                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $row++;
                    $id     =   "";
                    $id     =   Leads::LastClientID()['client'];
                    $year   =   date("Y-m-d",strtotime(date("Y-m-d")."- 18 year"));
                    $age    =   $_replace->AgeClient($year);

                    if(strtoupper($data[3]) == "PR") 
                    { 
                        $country    =   "1";    $town  =   "1";     $zip   =   "1";     $level  =   "1";    $roof   =   "1";    $type   =   "1";
                    }
                    elseif (strtoupper($data[3]) == "USA") 
                    { 
                        $country    =   "2";    $town  =   "91";    $zip    =   "199";  $level  =   "5";    $roof   =   "4";    $type   =   "6";
                    }
                    elseif (strtoupper($data[3]) == "VE") 
                    { 
                        $country    =   "4";    $town  =   "128";   $zip    =   "176";  $level  =   "14";    $roof   =   "10";    $type   =   "16";
                    }
                    else
                    { 
                        $country = "1";     $town  =   "1";     $zip   =   "1";     $level  =   "1";    $roof   =   "1";    $type   =   "1";
                    }

                    $iData  =   [
                        'client_id'             => $id,
                        'referred_id'           => "0",
                        'name'                  => strtoupper($_replace->deleteTilde(trim($data[1]))),
                        'birthday'              => $year,
                        'age'                   => $age,
                        'repre'                 => strtoupper($_replace->deleteTilde(trim($data[1]))),
                        'type'                  => "1",
                        'phone_main'            => $data[0],
                        'phone_main_owner'      => strtoupper($_replace->deleteTilde(trim($data[1]))),
                        'phone_main_provider'   => "1",
                        'country_id'            => $country,
                        'town_id'               => $town,
                        'zip_id'                => $zip,
                        'h_type_id'             => $type,
                        'h_roof_id'             => $roof,
                        'h_level_id'            => $level,
                        'ident_id'              => "" ,
                        'ident_exp'             => "" ,
                        'ss_id'                 => "" ,
                        'ss_exp'                => "" ,
                    ];


                    $query      =   'SELECT * FROM cp_leads WHERE phone_main = "'.$iData['phone_main'].'" OR phone_alt = "'.$iData['phone_main'].'" OR phone_other = "'.$iData['phone_main'].'" ORDER BY id DESC LIMIT 1';
                    $result     =   DBSmart::DBQuery($query);

                    if($result == false)
                    {


                        $query =    'INSERT INTO cp_leads(client_id, referred_id, name, birthday, gender, age, repre_legal, type_acc, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_by, created_at) VALUES ("'.$iData['client_id'].'", "'.$iData['referred_id'].'", "'.$iData['name'].'", "'.$iData['birthday'].'", "M", "'.$iData['age'].'", "'.$iData['repre'].'", "'.$iData['type'].'", "'.$iData['phone_main'].'", "'.$iData['phone_main_owner'].'", "'.$iData['phone_main_provider'].'", "", "", "1", "", "", "1", "", "", "", "", "", "", "'.$iData['country_id'].'", "'.$iData['town_id'].'", "'.$iData['zip_id'].'", "'.$iData['h_type_id'].'", "'.$iData['h_roof_id'].'", "'.$iData['h_level_id'].'", "'.$iData['ident_id'].'", "'.$iData['ident_exp'].'", "'.$iData['ss_id'].'", "'.$iData['ss_exp'].'", "1", "1", "1", "1", "1", "1", "1", "1", "", "1", NOW())';

                        $lead       =   DBSmart::DataExecute($query);

                        if($lead <> false)
                        {
                            $note       =   strtoupper($_replace->deleteTilde($data[2]));

                            $query      =   'INSERT INTO cp_notes(client_id, note, img, type, size, user_id, created_at) VALUES ("'.$iData['client_id'].'", "'.$note.'", "", "",  "0", "1", NOW())';

                            $note       =   DBSmart::DataExecute($query);

                            $rows[$row] =    [ 
                                'id'        =>  $iData['client_id'],
                                'nombre'    =>  $iData['name'],
                                'principal' =>  $iData['phone_main'],
                                'status'    =>  'TRUE'
                            ];
                       
                        }else{

                            $rows[$row] =    [ 
                                'id'        =>  '',
                                'nombre'    =>  $iData['name'],
                                'principal' =>  $iData['phone_main'],
                                'status'    =>  'FALSE'
                            ];
                        
                        }

                    }else{
                        $rows[$row] =    [ 
                            'id'        =>  '',
                            'nombre'    =>  $iData['name'],
                            'principal' =>  $iData['phone_main'],
                            'status'    =>  'DUPLICATED'
                        ];
                    
                    }

                    $iData  =   [];

                }

                $columnNames = array();
                if(!empty($csv)){
                    $firstRow = $csv[0];
                    foreach($firstRow as $colName => $val){
                        $columnNames[] = $colName;
                    }
                }

                $fileName = 'CREACION MASSIVA - '.date("Y-m-d").'.csv';

                header('Content-Type: application/excel');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                 
                $fp = fopen('php://output', 'w');
                 
                fputcsv($fp, $columnNames);

                foreach ($rows as $row) { fputcsv($fp, $row);   }
                 
                fclose($fp);
            }

        }
        
        return $app->json(array('status'    => true));

    }

    public static function MassiveNotes(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $csv = [];
        
        if($_FILES['file_notes']['name'] <> "")
        {
            $name       = $_FILES['file_notes']['name'];
            $ext        = 'csv';
            $type       = $_FILES['file_notes']['type'];
            $tmpName    = $_FILES['file_notes']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                $row = 0;
                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $row++;

                    if($data[0] <> '')
                    {
                        $iData  =   ['client'   => $data[0], 'note'   =>    strtoupper($_replace->deleteTilde($data[1])) ];

                        $query      =   'INSERT INTO cp_notes(client_id, note, img, type, size, user_id, created_at) VALUES ("'.$iData['client'].'", "'.$iData['note'].'", "", "",  "0", "1", NOW())';

                        $insert     =   DBSmart::DataExecute($query);

                        if($insert <> false)
                        {

                            $rows[$row] =    [ 
                                'client'    =>  $data[0],
                                'status'    =>  'TRUE'
                            ];
                       
                        }else{
                            $rows[$row] =    [ 
                                'client'    =>  $data[0],
                                'status'    =>  'FALSE'
                            ];
                        
                        }

                    }
                }

                $columnNames = array();
                if(!empty($csv)){
                    $firstRow = $csv[0];
                    foreach($firstRow as $colName => $val){
                        $columnNames[] = $colName;
                    }
                }

                $fileName = 'CREACION MASSIVA NOTAS - '.date("Y-m-d").'.csv';

                header('Content-Type: application/excel');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                 
                $fp = fopen('php://output', 'w');
                 
                fputcsv($fp, $columnNames);

                foreach ($rows as $row) { fputcsv($fp, $row);   }
                 
                fclose($fp);
            }

        }
        
        return $app->json(array('status'    => true));

    }

    public static function MassiveNotesMikrowisp(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $csv = [];
        
        if($_FILES['file_mw']['name'] <> "")
        {
            $name       = $_FILES['file_mw']['name'];
            $ext        = 'csv';
            $type       = $_FILES['file_mw']['type'];
            $tmpName    = $_FILES['file_mw']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                $row = 0;
                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $row++;

                    if( ($data[0] <> '') AND ($data[1] <> '') AND ($data[2] <> '') AND ($data[3] <> '') )
                    {
                        if(strtoupper($data[3]) == "VE")
                        {
                            $iData      =   ['client'   => $data[0], 'title'   =>    strtoupper($_replace->deleteTilde($data[1])), 'note'   =>    strtoupper($_replace->deleteTilde($data[1])) ];

                            $insert       =   CollectionVE::NoteMassive($iData);

                            // ddd($insert);

                            if($insert <> false)
                            {
                                $rows[$row] =    [ 
                                    'client'    =>  $iData['client'],
                                    'title'     =>  $iData['title'],
                                    'note'      =>  $iData['note'],
                                    'country'   =>  "VENEZUELA",
                                    'status'    =>  'true'
                                ];
                           
                            }else{
                                $rows[$row] =    [ 
                                    'client'    =>  $iData['client'],
                                    'title'     =>  $iData['title'],
                                    'note'      =>  $iData['note'],
                                    'country'   =>  "VENEZUELA",
                                    'status'    =>  'false'
                                ];
                            
                            }

                        }
                        elseif(strtoupper($data[3]) == "PR")
                        {
                            $iData      =   ['client'   => $data[0], 'title'   =>    strtoupper($_replace->deleteTilde($data[1])), 'note'   =>    strtoupper($_replace->deleteTilde($data[1])) ];

                            $insert       =   CollectionPR::NoteMassive($iData);                           

                            if($insert <> false)
                            {
                                $rows[$row] =    [ 
                                    'client'    =>  $iData['client'],
                                    'title'     =>  $iData['title'],
                                    'note'      =>  $iData['note'],
                                    'country'   =>  "PUERTO RICO",
                                    'status'    =>  'true'
                                ];
                           
                            }else{
                                $rows[$row] =    [ 
                                    'client'    =>  $iData['client'],
                                    'title'     =>  $iData['title'],
                                    'note'      =>  $iData['note'],
                                    'country'   =>  "PUERTO RICO",
                                    'status'    =>  'false'
                                ];
                            
                            }
                        }

                    }
                }

                $columnNames = array();
                if(!empty($csv)){
                    $firstRow = $csv[0];
                    foreach($firstRow as $colName => $val){
                        $columnNames[] = $colName;
                    }
                }

                $fileName = 'CREACION MASSIVA NOTAS - MIKROWISP - '.date("Y-m-d").'.csv';

                header('Content-Type: application/excel');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                 
                $fp = fopen('php://output', 'w');
                 
                fputcsv($fp, $columnNames);

                foreach ($rows as $row) { fputcsv($fp, $row);   }
                 
                fclose($fp);
            }

        }
        
        return $app->json(array('status'    => true));

    }

    static public function ConglomerateCalls(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $objPHPExcel    =   new \PHPExcel();


        if($_POST['date_cong_in'] <> $_POST['date_cong_out'])
        {
            $date   =   [ 'dateIn'   =>  $_POST['date_cong_in'], 'dateEnd'   =>  $_POST['date_cong_out'] ];
            $betw   =   'BETWEEN "'.$date['dateIn'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';
        }else{
            $betw   =   'LIKE "'.$_POST['date_cong_in'].'%"';
        }

        switch ($_POST['dep']) 
        {
            case '1':
                $departa    =   '(departament_id = "1" OR departament_id = "28")';
                break;

            case '2':
                $departa    =   '(departament_id = "2")';
                break;

            case '5':
                $departa    =   '(departament_id = "5")';
                break;

            case '6':
                $departa    =   '(departament_id = "6")';
                break;

            case '11':
                $departa    =   '(departament_id = "11")';
                break;

            default:
                $departa    =   '(departament_id = "2")';
                break;
        }



        $query  =   'SELECT id, username, ext, ext_ve, (SELECT name FROM data_departament WHERE id = departament_id) AS departament FROM users WHERE '.$departa.' AND status_id = "1" ORDER BY id ASC';

        $users  =   DBSmart::DBQueryAll($query);

         foreach ($users as $u => $us) 
         {
            $query      =   'SELECT (SELECT SUM(inb) FROM cp_time_calls_pr_new WHERE ext = "'.$us['ext'].'" AND created_at '.$betw.') AS CInbPR,  (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(inbt))) FROM cp_time_calls_pr_new WHERE ext = "'.$us['ext'].'" AND created_at '.$betw.') AS TInbPR, (SELECT SUM(oubto)+SUM(ouna) FROM cp_time_calls_pr_new WHERE ext = "'.$us['ext'].'" AND created_at '.$betw.') AS COutPR, (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(oubtot))) FROM cp_time_calls_pr_new WHERE ext = "'.$us['ext'].'" AND created_at '.$betw.') AS TOutPR, (SELECT SUM(cacalls) FROM cp_time_calls_pr_new WHERE ext = "'.$us['ext'].'" AND created_at '.$betw.') AS CTotalPR, (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(ticalls))) FROM cp_time_calls_pr_new WHERE ext = "'.$us['ext'].'" AND created_at '.$betw.') AS TTotalPR, (SELECT SUM(inb) FROM cp_time_calls_vz_new WHERE ext = "'.$us['ext_ve'].'" AND created_at '.$betw.') AS CInbVE,  (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(inbt))) FROM cp_time_calls_vz_new WHERE ext = "'.$us['ext_ve'].'" AND created_at '.$betw.') AS TInbVE, (SELECT SUM(oubto)+SUM(ouna) FROM cp_time_calls_vz_new WHERE ext = "'.$us['ext_ve'].'" AND created_at '.$betw.') AS COutVE, (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(oubtot))) FROM cp_time_calls_vz_new WHERE ext = "'.$us['ext_ve'].'" AND created_at '.$betw.') AS TOutVE, (SELECT SUM(cacalls) FROM cp_time_calls_vz_new WHERE ext = "'.$us['ext_ve'].'" AND created_at '.$betw.') AS CTotalVE, (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(ticalls))) FROM cp_time_calls_vz_new WHERE ext = "'.$us['ext_ve'].'" AND created_at '.$betw.') AS TTotalVE, (SELECT CInbPR + CInbVE ) AS CInbT,  (SELECT SEC_TO_TIME( SUM(TIME_TO_SEC(TInbPR) + TIME_TO_SEC(TInbVE))) ) AS TInbT,  (SELECT COutPR + COutVE ) AS COutT,  (SELECT SEC_TO_TIME( SUM(TIME_TO_SEC(TOutPR) + TIME_TO_SEC(TOutVE))) ) AS TOutT,  (SELECT CTotalPR + CTotalVE ) AS CTotalT,  (SELECT SEC_TO_TIME( SUM(TIME_TO_SEC(TTotalPR) + TIME_TO_SEC(TTotalVE))) ) AS TTotalT';

            $iData  =   DBSmart::DBQuery($query);

            $statics[$u]    =   [
                'username'      =>  $us['username'],
                'departament'   =>  $us['departament'],
                'CInbPR'        =>  ( (isset($iData['CInbPR']))     ? $iData['CInbPR']      : '0' ),
                'TInbPR'        =>  ( (isset($iData['TInbPR']))     ? $iData['TInbPR']      : '00:00:00' ),
                'COutPR'        =>  ( (isset($iData['COutPR']))     ? $iData['COutPR']      : '0' ),
                'TOutPR'        =>  ( (isset($iData['TOutPR']))     ? $iData['TOutPR']      : '00:00:00' ),
                'CTotalPR'      =>  ( (isset($iData['CTotalPR']))   ? $iData['CTotalPR']    : '0' ),
                'TTotalPR'      =>  ( (isset($iData['TTotalPR']))   ? $iData['TTotalPR']    : '00:00:00' ),
                'CInbVE'        =>  ( (isset($iData['CInbVE']))     ? $iData['CInbVE']      : '0' ),
                'TInbVE'        =>  ( (isset($iData['TInbVE']))     ? $iData['TInbVE']      : '00:00:00' ),
                'COutVE'        =>  ( (isset($iData['COutVE']))     ? $iData['COutVE']      : '0' ),
                'TOutVE'        =>  ( (isset($iData['TOutVE']))     ? $iData['TOutVE']      : '00:00:00' ),
                'CTotalVE'      =>  ( (isset($iData['CTotalVE']))   ? $iData['CTotalVE']    : '0' ),
                'TTotalVE'      =>  ( (isset($iData['TTotalVE']))   ? $iData['TTotalVE']    : '00:00:00' ),
                'CInbT'         =>  ( (isset($iData['CInbT']))      ? $iData['CInbT']       : '0' ),
                'TInbT'         =>  ( (isset($iData['TInbT']))      ? $iData['TInbT']       : '00:00:00' ),
                'COutT'         =>  ( (isset($iData['COutT']))      ? $iData['COutT']       : '0' ),
                'TOutT'         =>  ( (isset($iData['TOutT']))      ? $iData['TOutT']       : '00:00:00' ),
                'CTotalT'       =>  ( (isset($iData['CTotalT']))    ? $iData['CTotalT']     : '0' ),
                'TTotalT'       =>  ( (isset($iData['TTotalT']))    ? $iData['TTotalT']     : '00:00:00' ),
            ];

            
         }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Operador')
            ->setCellValue('B1', 'Departamento')
            ->setCellValue('C1', 'PR - Inbound - Cantidad')
            ->setCellValue('D1', 'PR - Inbound - Total')
            ->setCellValue('E1', 'PR - Outbound - Cantidad')
            ->setCellValue('F1', 'PR - Outbound - Total')
            ->setCellValue('G1', 'PR - Cantidad - Total')
            ->setCellValue('H1', 'PR - Tiempo - Total')
            ->setCellValue('I1', 'VE - Inbound - Cantidad')
            ->setCellValue('J1', 'VE - Inbound - Total')
            ->setCellValue('K1', 'VE - Outbound - Cantidad')
            ->setCellValue('L1', 'VE - Outbound - Total')
            ->setCellValue('M1', 'VE - Cantidad - Total')
            ->setCellValue('N1', 'VE - Tiempo - Total')
            ->setCellValue('O1', 'Inbound - Cantidad')
            ->setCellValue('P1', 'Inbound - Total')
            ->setCellValue('Q1', 'Outbound - Cantidad')
            ->setCellValue('R1', 'Outbound - Total')
            ->setCellValue('S1', 'Cantidad - Total')
            ->setCellValue('T1', 'Tiempo - Total');

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);  
        $cel=2;

        foreach ($statics as $s => $sta) 
        {
            $a1     =   $sta['username'];
            $b1     =   $sta['departament'];
            $c1     =   $sta['CInbPR'];
            $d1     =   $sta['TInbPR'];
            $e1     =   $sta['COutPR'];
            $f1     =   $sta['TOutPR'];
            $g1     =   $sta['CTotalPR'];
            $h1     =   $sta['TTotalPR'];
            $i1     =   $sta['CInbVE'];
            $j1     =   $sta['TInbVE'];
            $k1     =   $sta['COutVE'];
            $l1     =   $sta['TOutVE'];
            $m1     =   $sta['CTotalVE'];
            $n1     =   $sta['TTotalVE'];
            $o1     =   $sta['CInbT'];
            $p1     =   $sta['TInbT'];
            $q1     =   $sta['COutT'];
            $r1     =   $sta['TOutT'];
            $s1     =   $sta['CTotalT'];
            $t1     =   $sta['TTotalT'];

            $a      =   "A".$cel;
            $b      =   "B".$cel;
            $c      =   "C".$cel;
            $d      =   "D".$cel;
            $e      =   "E".$cel;
            $f      =   "F".$cel;
            $g      =   "G".$cel;
            $h      =   "H".$cel;
            $i      =   "I".$cel;
            $j      =   "J".$cel;
            $k      =   "K".$cel;
            $l      =   "L".$cel;
            $m      =   "M".$cel;
            $n      =   "N".$cel;
            $o      =   "O".$cel;
            $p      =   "P".$cel;
            $q      =   "Q".$cel;
            $r      =   "R".$cel;
            $s      =   "S".$cel;
            $t      =   "T".$cel;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($a, $a1)
                ->setCellValue($b, $b1)
                ->setCellValue($c, $c1)
                ->setCellValue($d, $d1)
                ->setCellValue($e, $e1)
                ->setCellValue($f, $f1)
                ->setCellValue($g, $g1)
                ->setCellValue($h, $h1)
                ->setCellValue($i, $i1)
                ->setCellValue($j, $j1)
                ->setCellValue($k, $k1)
                ->setCellValue($l, $l1)
                ->setCellValue($m, $m1)
                ->setCellValue($n, $n1)
                ->setCellValue($o, $o1)
                ->setCellValue($p, $p1)
                ->setCellValue($q, $q1)
                ->setCellValue($r, $r1)
                ->setCellValue($s, $s1)
                ->setCellValue($t, $t1);
            $cel+=1;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Time Calls');

        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');

        header('Content-Disposition: attachment;filename="TimeCalls - '.$users[0]['departament'].'".xls"');

        header('Cache-Control: max-age=0');

        $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $writer->save('php://output');

        return $app->json(array('status'    => true));
    
    }

    public static function ReportingCliSegNotes(Application $app, Request $request)
    {
        if($_POST['dateCSN_in'] <> $_POST['dateCSN_out'])
        {
            $date   =   [ 'dateIn'   =>  $_POST['dateCSN_in'], 'dateEnd'   =>  $_POST['dateCSN_out'] ];
            $betw   =   'BETWEEN "'.$date['dateIn'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';
        }else{
            $betw   =   'LIKE "'.$_POST['dateCSN_in'].'%"';
        }

        $query  =   'SELECT id, client_id, name, phone_main, phone_alt FROM cp_leads WHERE country_id = "'.$_POST['CSN_country'].'" AND created_at '.$betw.' GROUP BY phone_main ORDER BY client_id ASC';
        $leads  =   DBSmart::DBQueryAll($query);
        $c      =   $c2     =   $cn1    =   $cn2     =   $row   =   0;
        $dNow   =   Carbon::now()->toDateTimeString();

        foreach ($leads as $l => $lea) 
        {
            $query  =   'SELECT ticket FROM cp_appro_serv WHERE client_id = "'.$lea['client_id'].'"';
            $iRes   =   DBSmart::DBQuery($query);

            if($iRes == false)
            {   
                $iD[$c=$c+1]        =   [ 'client_id' => $lea['client_id'] ];
            }
        
        }

        foreach ($iD as $i => $id) 
        {
            $query  =   'SELECT client_id, created_at FROM cp_notes WHERE client_id = "'.$id['client_id'].'" ORDER BY created_at DESC LIMIT 1';
            $note   =   DBSmart::DBQuery($query);      

            if($note <> false)
            {
                $diff   =   ( (strtotime($dNow) - strtotime($note['created_at'])) / 86400);

                if($diff >= 1)
                {
                    $query  =   'SELECT t1.client_id AS CLIENTE, t2.name AS NOMBRE, t2.phone_main AS PRINCIPAL, t2.phone_alt AS ALTERNO, t2.phone_other AS OTRO, t2.created_at AS F_CLIENT, t1.note AS NOTA, (SELECT username FROM users WHERE id = t1.user_id) AS OPERADOR, t1.created_at AS F_NOTE, (SELECT name FROM data_country WHERE id = t2.country_id) AS COUNTRY FROM cp_notes AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.client_id = "'.$id['client_id'].'" ORDER BY t1.created_at DESC LIMIT 3';
                    $notes   =   DBSmart::DBQueryAll($query);

                    if($notes <> false)
                    {
                        foreach ($notes as $n => $not) 
                        {
                            $rows[$row]  =   [
                                'CLIENTE'   =>   $not['CLIENTE'],
                                'NOMBRE'    =>   $not['NOMBRE'],
                                'PRINCIPAL' =>   $not['PRINCIPAL'],
                                'ALTERNO'   =>   $not['ALTERNO'],
                                'OTRO'      =>   $not['OTRO'],
                                'F_CLIENT'  =>   $not['F_CLIENT'],
                                'NOTA'      =>   $not['NOTA'],
                                'OPERADOR'  =>   $not['OPERADOR'],
                                'F_NOTE'    =>   $not['F_NOTE'],
                                'COUNTRY'   =>   $not['COUNTRY']
                            ];
                            $row    =   $row + 1;
                        }
                    }
                }

            }

        }

        // ddd($rows);
        $columnNames = array();
        if(!empty($rows)){
            $firstRow = $rows[0];
            foreach($firstRow as $colName => $val){
                $columnNames[] = $colName;
            }
        }

        switch ($_POST['CSN_country']) 
        {
            case '1':
                $country    =   'PUERTO-RICO';
                break;
            
            case '2':
                $country    =   'ESTADOS-UNIDO';
                break;

            case '4':
                $country    =   'VENZUELA';
                break;
        }

        $fileName = 'Reporting-Client-Seguiment-Notes-'.$country.'.csv';

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
         
        $fp = fopen('php://output', 'w');
         
        fputcsv($fp, $columnNames);

        foreach ($rows as $row) { fputcsv($fp, $row);   }
         
        fclose($fp);

        return $app->json(array('status'    => true));
    
    }

    public static function VerifiePaymentTypeIMikro(Application $app, Request $request)
    {
        $query      = "TRUNCATE TABLE cp_mw_payment_tmp2";
        $truncate   = DBSmart::DataExecute($query);

        $file   =   $_FILES;

        $csv    =   [];
        $iData  =   [];
        $st     =   0;


        if($file['file_vptm']['type'] == 'application/vnd.ms-excel')
        {
            $name       =   $file['file_vptm']['name'];
            $ext        =   'csv';
            $type       =   $file['file_vptm']['type'];
            $tmpName    =   $file['file_vptm']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                set_time_limit(0);
                $row = 0;

                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $info   =   $data[0];

                    if( $info == "Date")
                    {
                        $st = 1;
                    }elseif( $info == "Details" ){
                        $st = 2;  
                    }

                    if($st == 1)
                    {
                        if($row <> 0)
                        {

                            $csv[$row]['Date']          =   ($data[0] <> '')    ? (date("Y-m-d", strtotime($data[0]))) : '';
                            $csv[$row]['Description']   =   ($data[1] <> '')    ? $data[1] : '';
                            $csv[$row]['Amount']        =   ($data[2] <> '')    ? $data[2] : '';
                            $iData[$row]    =   [
                                'date'          =>  ($data[0] <> '')    ? (date("Y-m-d", strtotime($data[0]))) : '',
                                'description'   =>  trim(substr($data[1], (strpos($data[1], '#')+1), (strpos($data[1], ';') - (strpos($data[1], '#')+1)))),
                                'amount'        =>  ($data[2] <> '')    ? $data[2] : ''
                            ];

                            $query      =   'INSERT INTO cp_mw_payment_tmp2(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';
                            $insert     =   DBSmart::DataExecute($query);
                        }

                        $row++;

                    }
                    elseif($st == 2)
                    {
                        if($row <> 0)
                        {
                            $csv[$row]['Details']           =   ($data[0] <> '')    ? $data[0] : '';
                            $csv[$row]['Posting Date']      =   ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '';
                            $csv[$row]['Description']       =   ($data[2] <> '')    ? $data[2] : '';
                            $csv[$row]['Amount']            =   ($data[3] <> '')    ? $data[3] : '';
                            $csv[$row]['Type']              =   ($data[4] <> '')    ? $data[4] : '';

                            $csv[$row]['Check or Slip #']   =   ($data[5] <> '')    ? $data[5] : '';

                            if($csv[$row]['Type'] == "PARTNERFI_TO_CHASE")
                            {
                                $iData[$row]    =   [
                                    'date'          =>  ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '',
                                    'description'   =>  (substr($data[2],-9)),
                                    'amount'        =>  ($data[3] <> '')    ? $data[3] : ''
                                ];

                                $query  =   'INSERT INTO cp_mw_payment_tmp2(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';

                                $insert     =   DBSmart::DataExecute($query);
                            }

                            if($csv[$row]['Type'] == "QUICKPAY_CREDIT")
                            {
                                $iData[$row]    =   [
                                    'date'          =>  ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '',
                                    'description'   =>  (substr($data[2],-10)),
                                    'amount'        =>  ($data[3] <> '')    ? $data[3] : ''
                                ];

                                $query  =   'INSERT INTO cp_mw_payment_tmp2(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';

                                $insert     =   DBSmart::DataExecute($query);
                            
                            }

                            if($csv[$row]['Type'] == "ACH_CREDIT")
                            {
                                $iData[$row]    =   [
                                    'date'          =>  ($data[1] <> '')    ? (date("Y-m-d", strtotime($data[1]))) : '',
                                    'description'   =>  (substr($data[2],-9)),
                                    'amount'        =>  ($data[3] <> '')    ? $data[3] : ''
                                ];

                                $query  =   'INSERT INTO cp_mw_payment_tmp2(fecha, code, amount) VALUES ("'.$iData[$row]['date'].'", "'.$iData[$row]['description'].'", "'.$iData[$row]['amount'].'")';

                                $insert     =   DBSmart::DataExecute($query);
                            }

                        }

                        $row++;

                    }elseif($st == 0){
                        return false;
                    }

                }
                
                $query  =   'SELECT * FROM cp_mw_payment_tmp2 ORDER BY id ASC';
                $iTot   =   DBSmart::DBQueryAll($query);
                $c      =   0;

                foreach ($iTot as $t => $tot)
                {
                    $query  =   'SELECT t2.nombre, t2.telefono, t2.movil, t2.correo, t2.estado, t1.nfactura, t1.fecha_pago, t1.transaccion FROM operaciones AS t1 INNER JOIN usuarios AS t2 ON (t1.idcliente = t2.id) AND t1.transaccion LIKE "%'.$tot['code'].'%"';
                    $iMik   =   DBMikroVE::DBQuery($query);

                    if($iMik <> false)
                    {
                    
                        $rows[$c]  =   [
                            'NOMBRE'        =>  $iMik['nombre'],
                            'TELEFONO'      =>  $iMik['telefono'],
                            'ALTERNO'       =>  $iMik['movil'],
                            'EMAIL'         =>  $iMik['correo'],
                            'ESTADO'        =>  $iMik['estado'],
                            'FACTURA'       =>  $iMik['nfactura'],
                            'FECHA_PAGO'    =>  $iMik['fecha_pago'],
                            'CODIGO'        =>  $iMik['transaccion'],
                        ];
                        $c=$c+1;
                    }
                    
                }

                $columnNames = array();
                if(!empty($rows)){
                    $firstRow = $rows[0];
                    foreach($firstRow as $colName => $val){
                        $columnNames[] = $colName;
                    }
                }

                $fileName = 'Reporting-Check-Code-Transsaccion.csv';

                header('Content-Type: application/excel');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                 
                $fp = fopen('php://output', 'w');
                 
                fputcsv($fp, $columnNames);

                foreach ($rows as $row) { fputcsv($fp, $row);   }
                 
                fclose($fp);

                return $app->json(array('status'    => true));
            
            }else{

                return false;

            }
        
        }else{
            
            return false;
        }
    }

    public static function ReportingCallsOperator(Application $app, Request $request)
    {
        if($_POST['dateCO_in'] <> $_POST['dateCO_out'])
        {
            $date   =   [ 'dateIn'   =>  $_POST['dateCO_in'], 'dateEnd'   =>  $_POST['dateCO_out'] ];
            $betw   =   'BETWEEN "'.$date['dateIn'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';

            $mIn  =   substr($_POST['dateCO_in'], 5,2);
            $mOu  =   substr($_POST['dateCO_out'], 5,2);

            $yIn  =   substr($_POST['dateCO_in'], 2,2);
            $yOu  =   substr($_POST['dateCO_out'], 2,2);

            if($mIn == $mOu)
            {   
                switch ($_POST['CO_country']) 
                {
                    case '1':
                        $t  =  'pr_calls_operator_'.$yIn.$mIn;
                        $cu =   'Puerto-Rico';
                        break;
                    
                    case '2':
                        $t  =  'us_calls_operator_'.$yIn.$mIn;
                        $cu =   'Estados Unidos';
                        break;

                    case '4':
                        $t  =  'vz_calls_operator_'.$yIn.$mIn;
                        $cu =   'Venezuela';
                        break;

                    default:
                        $t  =  'vz_calls_operator_'.$yIn.$mIn;
                        $cu =   'Venezuela';
                        break;
                }

                $query  =   'SELECT * FROM '.$t.' WHERE calldate '.$betw.'';
                $rows   =   DBCalls::DBQueryAll($query);

                $objPHPExcel    =   new \PHPExcel();

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'id')
                    ->setCellValue('B1', 'calldate')
                    ->setCellValue('C1', 'src')
                    ->setCellValue('D1', 'did')
                    ->setCellValue('E1', 'dst')
                    ->setCellValue('F1', 'operator')
                    ->setCellValue('G1', 'ext')
                    ->setCellValue('H1', 'departament')
                    ->setCellValue('I1', 'team')
                    ->setCellValue('J1', 'disposition')
                    ->setCellValue('K1', 'time')
                    ->setCellValue('L1', 'online')
                    ->setCellValue('M1', 'type')
                    ->setCellValue('N1', 'created_at');

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                $cel=2;

                foreach ($rows as $s => $sta) 
                {
                    $a1     =   $sta['id'];
                    $b1     =   $sta['calldate'];
                    $c1     =   $sta['src'];
                    $d1     =   $sta['did'];
                    $e1     =   $sta['dst'];
                    $f1     =   $sta['operator'];
                    $g1     =   $sta['ext'];
                    $h1     =   $sta['departament'];
                    $i1     =   $sta['team'];
                    $j1     =   $sta['disposition'];
                    $k1     =   $sta['time'];
                    $l1     =   $sta['online'];
                    $m1     =   $sta['type'];
                    $n1     =   $sta['created_at'];
                    
                    $a      =   "A".$cel;
                    $b      =   "B".$cel;
                    $c      =   "C".$cel;
                    $d      =   "D".$cel;
                    $e      =   "E".$cel;
                    $f      =   "F".$cel;
                    $g      =   "G".$cel;
                    $h      =   "H".$cel;
                    $i      =   "I".$cel;
                    $j      =   "J".$cel;
                    $k      =   "K".$cel;
                    $l      =   "L".$cel;
                    $m      =   "M".$cel;
                    $n      =   "N".$cel;
                    
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($a, $a1)
                        ->setCellValue($b, $b1)
                        ->setCellValue($c, $c1)
                        ->setCellValue($d, $d1)
                        ->setCellValue($e, $e1)
                        ->setCellValue($f, $f1)
                        ->setCellValue($g, $g1)
                        ->setCellValue($h, $h1)
                        ->setCellValue($i, $i1)
                        ->setCellValue($j, $j1)
                        ->setCellValue($k, $k1)
                        ->setCellValue($l, $l1)
                        ->setCellValue($m, $m1)
                        ->setCellValue($n, $n1);
                    $cel+=1;
                    
                }

                $objPHPExcel->getActiveSheet()->setTitle('Calls Operator');

                $objPHPExcel->setActiveSheetIndex(0);

                header('Content-Type: application/vnd.ms-excel');

                header('Content-Disposition: attachment;filename="Calls Operator-'.$cu.'.xls"');
                header('Cache-Control: max-age=0');

                $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

                $writer->save('php://output');

                return $app->json(array('status'    => true));

            }else{

                $bw1    =   'BETWEEN "'.$date['dateIn'].' 00:00:00" AND "'.date("Y").'-'.$mIn.'-31 23:59:59"';
                $bw2    =   'BETWEEN "'.date("Y").'-'.$mOu.'-01 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';

                // ddd($bw1, $bw2);

                switch ($_POST['CO_country']) 
                {
                    case '1':
                        $t1  =  'pr_calls_operator_'.date("y").$mIn;
                        $t2  =  'pr_calls_operator_'.date("y").$mOu;
                        $cu  =  'Puerto-Rico';
                        break;
                    
                    case '2':
                        $t1  =  'us_calls_operator_'.date("y").$mIn;
                        $t2  =  'us_calls_operator_'.date("y").$mOu;
                        $cu  =  'Estados-Unidos';
                        break;

                    case '4':
                        $t1  =  'vz_calls_operator_'.date("y").$mIn;
                        $t2  =  'vz_calls_operator_'.date("y").$mOu;
                        $cu  =  'Venezuela';
                        break;

                    default:
                        $t1  =  'vz_calls_operator_'.date("y").$mIn;
                        $t2  =  'vz_calls_operator_'.date("y").$mOu;
                        $cu  =  'Venezuela';
                        break;
                }

                $query  =   'SELECT * FROM '.$t1.' WHERE calldate '.$bw1.'';
                $rows   =   DBCalls::DBQueryAll($query);
                 
                $query  =   'SELECT * FROM '.$t2.' WHERE calldate '.$bw2.'';
                $rows2   =   DBCalls::DBQueryAll($query);

                $objPHPExcel    =   new \PHPExcel();

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'id')
                    ->setCellValue('B1', 'calldate')
                    ->setCellValue('C1', 'src')
                    ->setCellValue('D1', 'did')
                    ->setCellValue('E1', 'dst')
                    ->setCellValue('F1', 'operator')
                    ->setCellValue('G1', 'ext')
                    ->setCellValue('H1', 'departament')
                    ->setCellValue('I1', 'team')
                    ->setCellValue('J1', 'disposition')
                    ->setCellValue('K1', 'time')
                    ->setCellValue('L1', 'online')
                    ->setCellValue('M1', 'type')
                    ->setCellValue('N1', 'created_at');

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                $cel=2;

                foreach ($rows as $s => $sta) 
                {
                    $a1     =   $sta['id'];
                    $b1     =   $sta['calldate'];
                    $c1     =   $sta['src'];
                    $d1     =   $sta['did'];
                    $e1     =   $sta['dst'];
                    $f1     =   $sta['operator'];
                    $g1     =   $sta['ext'];
                    $h1     =   $sta['departament'];
                    $i1     =   $sta['team'];
                    $j1     =   $sta['disposition'];
                    $k1     =   $sta['time'];
                    $l1     =   $sta['online'];
                    $m1     =   $sta['type'];
                    $n1     =   $sta['created_at'];
                    
                    $a      =   "A".$cel;
                    $b      =   "B".$cel;
                    $c      =   "C".$cel;
                    $d      =   "D".$cel;
                    $e      =   "E".$cel;
                    $f      =   "F".$cel;
                    $g      =   "G".$cel;
                    $h      =   "H".$cel;
                    $i      =   "I".$cel;
                    $j      =   "J".$cel;
                    $k      =   "K".$cel;
                    $l      =   "L".$cel;
                    $m      =   "M".$cel;
                    $n      =   "N".$cel;
                    
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($a, $a1)
                        ->setCellValue($b, $b1)
                        ->setCellValue($c, $c1)
                        ->setCellValue($d, $d1)
                        ->setCellValue($e, $e1)
                        ->setCellValue($f, $f1)
                        ->setCellValue($g, $g1)
                        ->setCellValue($h, $h1)
                        ->setCellValue($i, $i1)
                        ->setCellValue($j, $j1)
                        ->setCellValue($k, $k1)
                        ->setCellValue($l, $l1)
                        ->setCellValue($m, $m1)
                        ->setCellValue($n, $n1);
                    $cel+=1;
                    
                }

                foreach ($rows2 as $s => $sta) 
                {
                    $a1     =   $sta['id'];
                    $b1     =   $sta['calldate'];
                    $c1     =   $sta['src'];
                    $d1     =   $sta['did'];
                    $e1     =   $sta['dst'];
                    $f1     =   $sta['operator'];
                    $g1     =   $sta['ext'];
                    $h1     =   $sta['departament'];
                    $i1     =   $sta['team'];
                    $j1     =   $sta['disposition'];
                    $k1     =   $sta['time'];
                    $l1     =   $sta['online'];
                    $m1     =   $sta['type'];
                    $n1     =   $sta['created_at'];
                    
                    $a      =   "A".$cel;
                    $b      =   "B".$cel;
                    $c      =   "C".$cel;
                    $d      =   "D".$cel;
                    $e      =   "E".$cel;
                    $f      =   "F".$cel;
                    $g      =   "G".$cel;
                    $h      =   "H".$cel;
                    $i      =   "I".$cel;
                    $j      =   "J".$cel;
                    $k      =   "K".$cel;
                    $l      =   "L".$cel;
                    $m      =   "M".$cel;
                    $n      =   "N".$cel;
                    
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($a, $a1)
                        ->setCellValue($b, $b1)
                        ->setCellValue($c, $c1)
                        ->setCellValue($d, $d1)
                        ->setCellValue($e, $e1)
                        ->setCellValue($f, $f1)
                        ->setCellValue($g, $g1)
                        ->setCellValue($h, $h1)
                        ->setCellValue($i, $i1)
                        ->setCellValue($j, $j1)
                        ->setCellValue($k, $k1)
                        ->setCellValue($l, $l1)
                        ->setCellValue($m, $m1)
                        ->setCellValue($n, $n1);
                    $cel+=1;
                }

                $objPHPExcel->getActiveSheet()->setTitle('Calls Operator');

                $objPHPExcel->setActiveSheetIndex(0);

                header('Content-Type: application/vnd.ms-excel');

                header('Content-Disposition: attachment;filename="Calls Operator-'.$cu.'.xls"');
                header('Cache-Control: max-age=0');

                $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

                $writer->save('php://output');

                return $app->json(array('status'    => true));

            }

        }else{
            
            $date   =   [ 'dateIn'   =>  $_POST['dateCO_in'], 'dateEnd'   =>  $_POST['dateCO_out'] ];
            $betw   =   'BETWEEN "'.$date['dateIn'].' 00:00:00" AND "'.$date['dateEnd'].' 23:59:59"';

            $mIn  =   substr($_POST['dateCO_in'], 5,2);
            $mOu  =   substr($_POST['dateCO_out'], 5,2);

            $yIn  =   substr($_POST['dateCO_in'], 2,2);
            $yOu  =   substr($_POST['dateCO_out'], 2,2);

            switch ($_POST['CO_country']) 
            {
                case '1':
                    $t  =  'pr_calls_operator_'.$yIn.$mIn;
                    $cu =   'Puerto-Rico';
                    break;
                
                case '2':
                    $t  =  'us_calls_operator_'.$yIn.$mIn;
                    $cu =   'Estados Unidos';
                    break;

                case '4':
                    $t  =  'vz_calls_operator_'.$yIn.$mIn;
                    $cu =   'Venezuela';
                    break;

                default:
                    $t  =  'vz_calls_operator_'.$yIn.$mIn;
                    $cu =   'Venezuela';
                    break;
            }

            $query  =   'SELECT * FROM '.$t.' WHERE calldate '.$betw.'';
            $rows   =   DBCalls::DBQueryAll($query);
        
            $objPHPExcel    =   new \PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'id')
                ->setCellValue('B1', 'calldate')
                ->setCellValue('C1', 'src')
                ->setCellValue('D1', 'did')
                ->setCellValue('E1', 'dst')
                ->setCellValue('F1', 'operator')
                ->setCellValue('G1', 'ext')
                ->setCellValue('H1', 'departament')
                ->setCellValue('I1', 'team')
                ->setCellValue('J1', 'disposition')
                ->setCellValue('K1', 'time')
                ->setCellValue('L1', 'online')
                ->setCellValue('M1', 'type')
                ->setCellValue('N1', 'created_at');

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
            $cel=2;

            foreach ($rows as $s => $sta) 
            {
                $a1     =   $sta['id'];
                $b1     =   $sta['calldate'];
                $c1     =   $sta['src'];
                $d1     =   $sta['did'];
                $e1     =   $sta['dst'];
                $f1     =   $sta['operator'];
                $g1     =   $sta['ext'];
                $h1     =   $sta['departament'];
                $i1     =   $sta['team'];
                $j1     =   $sta['disposition'];
                $k1     =   $sta['time'];
                $l1     =   $sta['online'];
                $m1     =   $sta['type'];
                $n1     =   $sta['created_at'];
                
                $a      =   "A".$cel;
                $b      =   "B".$cel;
                $c      =   "C".$cel;
                $d      =   "D".$cel;
                $e      =   "E".$cel;
                $f      =   "F".$cel;
                $g      =   "G".$cel;
                $h      =   "H".$cel;
                $i      =   "I".$cel;
                $j      =   "J".$cel;
                $k      =   "K".$cel;
                $l      =   "L".$cel;
                $m      =   "M".$cel;
                $n      =   "N".$cel;
                
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($a, $a1)
                    ->setCellValue($b, $b1)
                    ->setCellValue($c, $c1)
                    ->setCellValue($d, $d1)
                    ->setCellValue($e, $e1)
                    ->setCellValue($f, $f1)
                    ->setCellValue($g, $g1)
                    ->setCellValue($h, $h1)
                    ->setCellValue($i, $i1)
                    ->setCellValue($j, $j1)
                    ->setCellValue($k, $k1)
                    ->setCellValue($l, $l1)
                    ->setCellValue($m, $m1)
                    ->setCellValue($n, $n1);
                $cel+=1;
                
            }

            $objPHPExcel->getActiveSheet()->setTitle('Calls Operator');

            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="Calls Operator-'.$cu.'.xls"');
            header('Cache-Control: max-age=0');

            $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            $writer->save('php://output');

            return $app->json(array('status'    => true));
        
        }

        ddd("ReportingCallsOperator");
    }

    public static function ReportingPhonePR(Application $app, Request $request)
    {
        $c      =   $ddd  =   0;
        $sum    =   $sumGen     =   0;
        if( ($request->get('dateRPPR_in') == "") || ($request->get('dateRPPR_out') == "") )
        {
            return $app->json(array('status'    => false));
        }else{

            if ( 
                ( substr($_POST['dateRPPR_in'], 5,2) <> substr($_POST['dateRPPR_out'], 5,2) ) || 
                ( substr($_POST['dateRPPR_in'], 2,2) <> substr($_POST['dateRPPR_out'], 2,2) )
            ){
             
                $year   =   [substr($_POST['dateRPPR_in'], 2,2), substr($_POST['dateRPPR_out'], 2,2)];
                $month  =   [substr($_POST['dateRPPR_in'], 5,2), substr($_POST['dateRPPR_out'], 5,2)];
                $mMenor =   min($month);
                $yMenor =   min($year);
                $mMayor =   max($month);
                $yMayor =   max($year);
                
                $iData  =   [];

                if($yMenor <> $yMayor)
                {
                    for ($i=substr($_POST['dateRPPR_in'], 5,2); $i < 13 ; $i++) 
                    { 
                        
                        if( (substr($_POST['dateRPPR_in'], 5,2) <= 13) && (substr($_POST['dateRPPR_in'], 5,2) <= $i) )
                        {
                            $iData[$c++]    =   ['in' => "20".$yMenor."-".(($i<10) ? '0'.(int)$i : $i)."-01", 'fn' => "20".$yMenor."-".(($i<10) ? '0'.(int)$i : $i)."-31"];
                        }
                    }

                    for ($i=1; $i <= substr($_POST['dateRPPR_out'], 5,2) ; $i++) 
                    { 
                        if( (substr($_POST['dateRPPR_out'], 5,2) <= 13) && (substr($_POST['dateRPPR_out'], 5,2) >= $i) )
                        {
                            $iData[$c++]    =   ['in' => "20".$yMayor."-".(($i<10) ? '0'.(int)$i : $i)."-01", 'fn' => "20".$yMayor."-".(($i<10) ? '0'.(int)$i : $i)."-31"];;
                        }
                    }

                }else{
                    for ($i=$mMenor; $i <= $mMayor ; $i++) 
                    { 
                        $iData[$c++]    =   ['in' => "20".$yMenor."-".(($i<10) ? '0'.(int)$i : $i)."-01", 'fn' => "20".$yMenor."-".(($i<10) ? '0'.(int)$i : $i)."-31"];
                    }                    
                }

            }else{
                $year   =   [substr($_POST['dateRPPR_in'], 2,2), substr($_POST['dateRPPR_out'], 2,2)];
                $month  =   [substr($_POST['dateRPPR_in'], 5,2), substr($_POST['dateRPPR_out'], 5,2)];
                $iData[1]  =   [
                    'in' => "20".substr($_POST['dateRPPR_in'], 2,2).'-'.substr($_POST['dateRPPR_in'], 5,2).'-01', 
                    'fn' => "20".substr($_POST['dateRPPR_out'], 2,2).'-'.substr($_POST['dateRPPR_out'], 5,2).'-31'
                ];
            }

            $objPHPExcel    =   new \PHPExcel();
            $cel=2;
            
            foreach ($iData as $k => $val) 
            {
                if($request->get('RPPR_country') == 1)
                {
                    $info     =   DBMikro::DBQueryAll('SELECT UPPER(fi.descripcion) Paquetes, SUM(fi.cantidad) AS Total FROM facturas f INNER JOIN facturaitems fi ON (f.id = fi.idfactura) AND f.pago BETWEEN "'.$val['in'].'" AND "'.$val['fn'].'" AND fi.descripcion LIKE "%telefo%" GROUP BY fi.descripcion');

                }
                elseif($request->get('RPPR_country') == 4)
                {
                    $info     =   DBMikroVE::DBQueryAll('SELECT UPPER(fi.descripcion) Paquetes, SUM(fi.cantidad) AS Total FROM facturas f INNER JOIN facturaitems fi ON (f.id = fi.idfactura) AND f.pago BETWEEN "'.$val['in'].'" AND "'.$val['fn'].'" AND fi.descripcion LIKE "%telefo%" GROUP BY fi.descripcion');
                }

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', '#')
                    ->setCellValue('B1', 'FECHA')
                    ->setCellValue('C1', 'CONCEPTO')
                    ->setCellValue('D1', 'TOTAL');

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);


                foreach ($info as $s => $sta) 
                {
                    $a1     =   $ddd++;
                    $b1     =   $val['in']." - ".$val['fn'];
                    $c1     =   $sta['Paquetes'];
                    $d1     =   $sta['Total'];

                    $a      =   "A".$cel;
                    $b      =   "B".$cel;
                    $c      =   "C".$cel;
                    $d      =   "D".$cel;
                   
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($a, $a1)
                        ->setCellValue($b, $b1)
                        ->setCellValue($c, $c1)
                        ->setCellValue($d, $d1);
                    $cel+=1;
                    $sum    =   $sum + $sta['Total'];
                }
                
                $sumGen =   $sumGen + $sum;

                $a1     =   "";
                $b1     =   "";
                $c1     =   "TOTAL DE MES";
                $d1     =   $sum;

                $a      =   "A".$cel;
                $b      =   "B".$cel;
                $c      =   "C".$cel;
                $d      =   "D".$cel;
               
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($a, $a1)
                    ->setCellValue($b, $b1)
                    ->setCellValue($c, $c1)
                    ->setCellValue($d, $d1);
                $cel+=1;
                $cel+=1;
                $sum  = 0;

            }
                $cel+=1;
                
                $a1     =   "";
                $b1     =   "";
                $c1     =   "TOTAL GENERAL";
                $d1     =   $sumGen;

                $a      =   "A".$cel;
                $b      =   "B".$cel;
                $c      =   "C".$cel;
                $d      =   "D".$cel;
               
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($a, $a1)
                    ->setCellValue($b, $b1)
                    ->setCellValue($c, $c1)
                    ->setCellValue($d, $d1);
                $cel+=1;


            $objPHPExcel->getActiveSheet()->setTitle('Conceptos de Telefonia');

            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="Conceptos-Telefonia-'.date("Ymdhms").'.xls"');
            header('Cache-Control: max-age=0');

            $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            $writer->save('php://output');



            ddd($iData);



            ddd("Reporting Phone PUERTO RICO");

        }

    } 

}