<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\Medium;
use App\Models\Objetive;
use App\Models\Conversation;
use App\Models\ServiceM;
use App\Models\Campaign;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class CampaignController extends BaseController
{

    public static function CampaignEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'campaign'  => Campaign::GetCampaignById($request->get('id'))
        ));
    
    }

    public function CampaignForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_camp'] == 'new')
        {
            $saveCampaign = Campaign::SaveCampaign($params);
            
            if($saveCampaign <> false)
            { 
                $info = array('client' => '', 'channel' => 'Campaign New', 'message' => 'Creacion de Campana MKT - '.strtoupper($params['name_camp']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'creative'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Campaign New', 'message' => 'Error Al Intentar Crear Campana MKT - '.strtoupper($params['name_camp']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_camp'] == 'edit')
        {

            $saveCampaign = Campaign::SaveCampaign($params);
            
            if($saveCampaign <> false)
            {
                $info = array('client' => '', 'channel' => 'Campaign Edit', 'message' => 'Actualizacion de Campana MKT - '.strtoupper($params['name_camp']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'creative'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Campaign Edit', 'message' => 'Error Al Intentar Actualizar Informacion de la Campana MKT - '.strtoupper($params['name_camp']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, Error Al Intentar Actualizar Informacion del Campana MKT, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
                            
        }
    
    }

}