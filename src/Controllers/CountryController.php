<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\LeadStates;
use App\Models\LeadTownShip;
use App\Models\LeadParish;
use App\Models\LeadSector;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class CountryController extends BaseController
{
    
    //////////////////////////////////////////////////////////////////////////////////////////////////

    public static function index(Application $app, Request $request)
    {
        return $app['twig']->render('countrys/index.html.twig',array(
            'sidebar'  =>  true,
            'status'   =>  Status::GetStatus(),
            'countrys' =>  Country::GetCountry(),
            'towns'    =>  Town::GetTown(),
            'zips'     =>  ZipCode::GetZipCode()
        ));
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static function Load(Application $app, Request $request)
    {
        $states     =   LeadStates::GetStates();
        $township   =   LeadTownShip::GetTownShip();
        $parish     =   LeadParish::GetParish();
        $sector     =   LeadSector::GetSector();

        return $app->json(array(
            'status'    =>  true, 
            'states'    =>  LeadStates::GetStatesHtml($states),
            'township'  =>  LeadTownShip::GetTownShipHtml($township),
            'parish'    =>  LeadParish::GetParishHtml($parish),
            'sector'    =>  LeadSector::GetSectorHtml($sector)            
        ));
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////
        
    public static function CountryEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'country'      => Country::GetCountryById($request->get('id'))
        ));
    
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function CountryForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_c'] == 'new')
        {
            $saveDep = Country::SaveCountry($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Country New', 'message' => 'Creacion de Country - '.strtoupper($params['name_c']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'country'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Country New', 'message' => 'Error Al Intentar Crear Paquete - '.strtoupper($params['name_c']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }        
        }
        elseif($params['type_c'] == 'edit')
        {

            $saveDep = Country::SaveCountry($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Country Edit', 'message' => 'Actualizacion de Paquete - '.strtoupper($params['name_c']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'country'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Country Edit', 'message' => 'Error Al Intentar Actualizar informacion del Paquete - '.strtoupper($params['name_c']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            } 
            
        }
    
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    public function StateShow(Application $app, Request $request)
    {
        $country    =   Country::GetCountry();
        $status     =   Status::GetStatus();
        
        return $app->json(array(
            'status'    => true, 
            'html'      => LeadStates::GetShowHtml($country, $status)
        ));

    }

    public function StateCreated(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $insert     =    LeadStates::InsertStates($params);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'State Created', 'message' => 'Creacion de Estado - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => true,
                'title'     =>  'Success',
                'content'   => 'Estado Actualizado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'State Created', 'message' => 'Error Al Intentar Crear Estado - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'title'     =>  'Error',
                'content'   => 'Imposible actualizar Estado, Intente Nuevamente.'
            )); 
        }

    }

    public function StateEdit(Application $app, Request $request)
    {

        $id         =   $request->get('id');

        $iData      =   LeadStates::GetStatesById($id);
        $country    =   Country::GetCountry();
        $status     =   Status::GetStatus();

        return $app->json(array(
            'status'    => true, 
            'html'      => LeadStates::GetStatesEditHtml($iData, $country, $status)
        ));

    }
    
    public function StateUpdate(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $id     =   $request->get('id');

        $insert     =    LeadStates::UpdateStates($params, $id);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'State Update', 'message' => 'Actualizacion de Estado - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => true,
                'title'     =>  'Success',
                'content'   => 'Estado Actualizado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'State Update', 'message' => 'Error Al Intentar Actualizar Estado - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'title'     =>  'Error',
                'content'   => 'Imposible actualizar Estado, Intente Nuevamente.'
            )); 
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function TownShipShow(Application $app, Request $request)
    {
        $country    =   LeadStates::GetStates();
        $status     =   Status::GetStatus();
        
        return $app->json(array(
            'status'    => true, 
            'html'      => LeadTownShip::GetShowTownShipHtml($country, $status)
        ));

    }

    public function TownShipCreated(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $insert     =    LeadTownShip::InsertTownShip($params);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'State Created', 'message' => 'Creacion de Estado - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => true,
                'title'     =>  'Success',
                'content'   => 'Estado Actualizado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'State Created', 'message' => 'Error Al Intentar Crear Estado - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'title'     =>  'Error',
                'content'   => 'Imposible actualizar Estado, Intente Nuevamente.'
            )); 
        }

    }

    public function TownShipEdit(Application $app, Request $request)
    {

        $id         =   $request->get('id');

        $iData      =   LeadTownShip::GetTownShipById($id);
        $country    =   LeadStates::GetStates();
        $status     =   Status::GetStatus();

        return $app->json(array(
            'status'    => true, 
            'html'      => LeadTownShip::GetTownShipEditHtml($iData, $country, $status)
        ));

    }
    
    public function TownShipUpdate(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $id     =   $request->get('id');

        $insert     =    LeadTownShip::UpdateTownShip($params, $id);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'State Update', 'message' => 'Actualizacion de Estado - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => true,
                'title'     =>  'Success',
                'content'   => 'Estado Actualizado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'State Update', 'message' => 'Error Al Intentar Actualizar Estado - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'title'     =>  'Error',
                'content'   => 'Imposible actualizar Estado, Intente Nuevamente.'
            )); 
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function ParishShow(Application $app, Request $request)
    {
        $country    =   LeadTownShip::GetTownShip();
        $status     =   Status::GetStatus();
        
        return $app->json(array(
            'status'    => true, 
            'html'      => LeadParish::GetShowParishHtml($country, $status)
        ));

    }

    public function ParishCreated(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $insert     =    LeadParish::InsertParish($params);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'Parroquia Created', 'message' => 'Creacion de Parroquia - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => true,
                'title'     =>  'Success',
                'content'   => 'Parroquia Creado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Parroquia Created', 'message' => 'Error Al Intentar Crear Parroquia - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'title'     =>  'Error',
                'content'   => 'Imposible actualizar Parroquia, Intente Nuevamente.'
            )); 
        }

    }

    public function ParishEdit(Application $app, Request $request)
    {

        $id         =   $request->get('id');

        $iData      =   LeadParish::GetParishById($id);
        $country    =   LeadTownShip::GetTownShip();
        $status     =   Status::GetStatus();

        return $app->json(array(
            'status'    => true, 
            'html'      => LeadParish::GetParishEditHtml($iData, $country, $status)
        ));

    }
    
    public function ParishUpdate(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $id     =   $request->get('id');

        $insert     =    LeadParish::UpdateParish($params, $id);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'Parroquia Update', 'message' => 'Actualizacion de Parroquia - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => true,
                'title'     =>  'Success',
                'content'   => 'Parroquia Actualizado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Parroquia Update', 'message' => 'Error Al Intentar Actualizar Parroquia - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'title'     =>  'Error',
                'content'   => 'Imposible actualizar Parroquia, Intente Nuevamente.'
            )); 
        }

    }


//////////////////////////////////////////////////////////////////////////////////////////////////

    public function SectorShow(Application $app, Request $request)
    {
        $country    =   LeadParish::GetParish();
        $status     =   Status::GetStatus();
        
        return $app->json(array(
            'status'    => true, 
            'html'      => LeadSector::GetShowSectorHtml($country, $status)
        ));

    }

    public function SectorCreated(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $insert     =    leadSector::InsertSector($params);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'Sector Created', 'message' => 'Creacion de Sector - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  true,
                'title'     =>  'Success',
                'content'   =>  'Sector Creado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Sector Created', 'message' => 'Error Al Intentar Crear Sector - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false, 
                'title'     =>  'Error',
                'content'   =>  'Imposible actualizar Sector, Intente Nuevamente.'
            )); 
        }

    }

    public function SectorEdit(Application $app, Request $request)
    {

        $id         =   $request->get('id');

        $iData      =   leadSector::GetSectorById($id);
        $country    =   LeadParish::GetParish();
        $status     =   Status::GetStatus();

        return $app->json(array(
            'status'    => true, 
            'html'      => leadSector::GetSectorEditHtml($iData, $country, $status)
        ));

    }
    
    public function SectorUpdate(Application $app, Request $request)
    {

        $params     =   []; 
        parse_str($request->get('str'), $params);

        $id     =   $request->get('id');

        $insert     =    leadSector::UpdateSector($params, $id);
        
        if($insert <> false)
        { 
            $info = array('client' => '', 'channel' => 'Sector Update', 'message' => 'Actualizacion de Sector - '.strtoupper($params['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  true,
                'title'     =>  'Success',
                'content'   =>  'Sector Actualizado Correctamente'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Sector Update', 'message' => 'Error Al Intentar Actualizar Sector - '.strtoupper($params['name_c']).' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false, 
                'title'     =>  'Error',
                'content'   =>  'Imposible actualizar Sector, Intente Nuevamente.'
            )); 
        }

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

}