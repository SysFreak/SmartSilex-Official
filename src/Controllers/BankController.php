<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class BankController extends BaseController
{
    public function index(Application $app, Request $request)
    {
        return $app['twig']->render('banks/index.html.twig',array(
            'sidebar'       =>  true,
            'banks'         =>  Bank::GetBank(),
            'payments'      =>  Payment::GetPayment(),
            'countrys'      =>  Country::GetCountry(),
            'services'      =>  Service::GetService(),
            'status'        =>  Status::GetStatus()
        ));
    }

    public static function BankEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'bank'      => Bank::GetBankById($request->get('id'))
        ));
    
    }

    public function BankForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_ba'] == 'new')
        {
            $saveBank = Bank::SaveBank($params);
            
            if($saveBank <> false)
            { 
                $info = array('client' => '', 'channel' => 'Bank New', 'message' => 'Creacion de Banco - '.strtoupper($params['name_ba']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'banks'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Bank New', 'message' => 'Error Al Intentar Crear Banco - '.strtoupper($params['name_ba']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_ba'] == 'edit')
        {

            $saveBank = Bank::SaveBank($params);
            
            if($saveBank <> false)
            {
                $info = array('client' => '', 'channel' => 'Bank Edit', 'message' => 'Actualizacion de Banco - '.strtoupper($params['name_ba']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'banks'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Bank Edit', 'message' => 'Error Al Intentar Actualizar Informacion del Banco - '.strtoupper($params['name_ba']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, Error Al Intentar Actualizar Informacion del Banco, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
                            
        }
    
    }

}