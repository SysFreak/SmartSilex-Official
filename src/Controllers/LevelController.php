<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Level;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class LevelController extends BaseController
{
    
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static function LevelEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'level'     => Level::GetLevelById($request->get('id'))
        ));
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////

    public function LevelForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_lev'] == 'new')
        {
            $saveDep = Level::SaveLevel($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Level New', 'message' => 'Creacion de Nivel - '.strtoupper($params['name_lev']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'house'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Level New', 'message' => 'Error Al Intentar Crear Nivel - '.strtoupper($params['name_lev']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_lev'] == 'edit')
        {

            $saveDep = Level::SaveLevel($params);
        
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Level Edit', 'message' => 'Actualizacion de Paquete - '.strtoupper($params['name_lev']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'house'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Level Edit', 'message' => 'Error Al Intentar Actualizar informacion del Level - '.strtoupper($params['name_lev']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }          
        }    
    }
}