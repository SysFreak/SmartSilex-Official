<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\User;
use App\Models\Notes;
use App\Models\Coordination;
use App\Models\CoordContactTmp;
use App\Models\CoordPreTmp;
use App\Models\CoordViewTmp;
use App\Models\CoordProcessTmp;
use App\Models\CoordReferredTmp;
use App\Models\CoordPackageTmp;
use App\Models\CoordCancel;
use App\Models\CoordContactSel;
use App\Models\CoordPreReason;

use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\ServiceIntVZ;
use App\Models\ApprovalsScore;

use App\Models\Country;
use App\Models\Bank;
use App\Models\Package;
use App\Models\Provider;
use App\Models\Payment;
use App\Models\Camera;
use App\Models\Dvr;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\PhoneProvider;

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use PDO;

/**
* date('Y-m-d H:i:s'
*/
class CoordinationsController extends BaseController
{

////////////////////////////////////////////////////////////////////////////////

    public static function index(Application $app)
    {

        return $app['twig']->render('coordination/index.html.twig',array(
            'sidebar'       =>  true,
            'countrys'      =>  Country::GetCountry(),
            'banks'         =>  Bank::GetBank(),
            'packages'      =>  Package::GetPackage(),
            'providers'     =>  Provider::GetProvider(),
            'users'         =>  User::GetUsers(),
            'payments'      =>  Payment::GetPayment(),
            'cameras'       =>  Camera::GetCamera(),
            'dvrs'          =>  Dvr::GetDvr(),
            'intres'        =>  Internet::GetType('1','1'),
            'intcom'        =>  Internet::GetType('2','1'),
            'phoneres'      =>  Phone::GetType('1','1'),
            'phonecom'      =>  Phone::GetType('2','1'),
            'intresvz'      =>  Internet::GetType('1','4'),
            'intcomvz'      =>  Internet::GetType('2','4'),
            'phoneresvz'    =>  Phone::GetType('1','4'),
            'phonecomvz'    =>  Phone::GetType('2','4'),
            'phonepro'      =>  PhoneProvider::GetProvider(),
            'contactlist'   =>  CoordContactSel::GetContactSelByStatus(),
            'prereason'     =>  CoordPreReason::GetPreReasonByStatus(),

        ));
    
    }

////////////////////////////////////////////////////////////////////////////////

    public static function Load(Application $app, Request $request)
    {
        $info = Coordination::Load();

        return $app->json(array(
            'status'    =>  true, 
            'pending'   =>  $info
        ));
    }

////////////////////////////////////////////////////////////////////////////////

    public static function GetInfoCoord(Application $app, Request $request)
    {
        return $app->json(array(
            'status' =>  true, 
            'info'   =>  Coordination::ViewCoord($request->get('id_c'))
        ));
    }

////////////////////////////////////////////////////////////////////////////////

    public function ViewContact(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('str'), $params);

        $cIData     =   Coordination::ViewCoord($params['id_c_id']);

        $iData      =   [
            'ticket'    =>  $cIData['ticket'],
            'client'    =>  $cIData['client_id'],
            'contact'   =>  $params['c_contact'],
            'reason'    =>  $params['c_reason'],
            'created'   =>  $app['session']->get('id')
        ];

        $record         =   CoordContactTmp::SaveContact($iData);

        if($record <> false)
        { 
            $coord      =   Coordination::UpdateTicket($iData, $record, 'CONTACT');

            if($coord <> false)
            {
                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Contacto', 'message' => 'Contacto de Coordinacion de Cliente - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  true,
                    'title'     =>  'Success',
                    'content'   =>  'Datos Actualizados Correctamente'
                ));

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Contacto ', 'message' => 'Contacto de Coordinacion de Cliente - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                ));
            }
            
        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Contacto ', 'message' => 'Contacto de Coordinacion de Cliente - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));
        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function InfoCoord(Application $app, Request $request)
    {
        $cIData     =   Coordination::ViewCoord($request->get('id_c'));

        $ticket     =   $cIData['ticket'];

        $score      =   ApprovalsScore::GetScoreByTicket($cIData['ticket']);

        $type       =   Coordination::DetecTicket(substr($ticket,0,2), $ticket);

        $info       =   Coordination::GetInfo($type, $ticket);

        $client     =   Coordination::ObtData($info['client'], $ticket);

        $referred   =   Coordination::ObtData($info['referred'], $ticket);

        $htmlCli    =   ($client['status'] == true)     ? Coordination::InfoViewClient($client['data'], 'client', $score)       :   '';

        $htmlRef    =   ($referred['status'] == true)   ? Coordination::InfoViewClient($referred['data'], 'referred', $score)   :   '';
        
        switch ($type) {
            case 'TVPR':
                $htmlServ   =   Coordination::InfoServTvPR($info['service']);
                break;
            case 'SECPR':
                $htmlServ   =   Coordination::InfoServSecPR($info['service']);
                break;
            case 'INTPR':
                $htmlServ   =   Coordination::InfoServIntPR($info['service'], $info['client']);
                break;
            case 'IVE':
                $htmlServ   =   Coordination::InfoServIntVZ($info['service'], $info['client']);
                break;
        }

        return $app->json(array(
            'status'    =>  true,
            'client'    =>  $htmlCli,
            'referred'  =>  $htmlRef,
            'service'   =>  $htmlServ
        ));
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function NotesCoord(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    =>  true,
            'notes'     =>  Notes::GetNotesHtml($request->get('id_c'))
        ));

    }

////////////////////////////////////////////////////////////////////////////////
    
    public function NoteSave(Application $app, Request $request)
    {

        $notes  =   [
            'client_id_n'   =>  $_POST['id_c_n_coord'],
            'notes'         =>  $_POST['notes'],
            'operator'      =>  $app['session']->get('id')
        ];

        if(!$_POST['notes'] == "")
        {
            if(!$_FILES['file']['name'] == "")
            {
                $filename       =   $_FILES["file"]["name"];
                $img            =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$filename);

                if($img == true)
                {
                    $fileAdd    =   [
                        'name'  =>  $filename,
                        'size'  =>  $_FILES["file"]['size'],
                        'type'  =>  $_FILES["file"]["type"],
                        'dir'   =>  "assets/upload/".$filename
                    ];

                    $notes = Notes::SaveNoteCF($notes, $fileAdd, $app['session']->get('id'));

                    if($notes == true)
                    { 
                        $info = array('client' => $_POST['id_c_n_coord'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Satisfactoria - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                        $app['datalogger']->RecordLogger($info);

                        return $app->json(array(
                            'status'    => true, 
                            'notes'     =>  Notes::GetNotesHtml($notes['client_id_n'])
                        ));

                    }else{

                        $info = array('client' =>  $_POST['id_c_n_coord'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                        $app['datalogger']->RecordLogger($info);

                        return $app->json(array(
                            'status'    => false, 
                            'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                        ));
                    
                    }

                }else{
                    return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Error al intentar guardar archivo adjunto, intente nuevamente si el problema persiste contacte a su administrador de sistemas.", true)
                    )); 
                }
            }else{

                $notes = Notes::SaveNoteSF($notes, $app['session']->get('id'));

                if($notes == true)
                { 
                    $info = array('client' => $_POST['id_c_n_coord'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Satisfactoria - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => true, 
                        'notes'     =>  Notes::GetNotesHtml($_POST['id_c_n_coord'])
                    ));

                }else{

                    $info = array('client' =>  $_POST['id_c_n_coord'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                    ));
                }
            }
        
        }else{

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("No se pudo procesar la solicitud, debe dejar una nota valida", true)
            )); 

        }

    }

////////////////////////////////////////////////////////////////////////////////

    public function PreCoord(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];

        parse_str($request->get('str'), $params);

        $cIData     =   Coordination::ViewCoord($params['id_c_p_coord']);

        $iData  =   [
            'ticket'    =>  $cIData['ticket'],
            'client'    =>  $cIData['client_id'],
            'coord'     =>  $params['c_p_contact'],
            'reason'    =>  $params['c_p_reazon'],
            'p_date'    =>  $_replace->ChangeDate($params['c_p_date']),
            'p_disp'    =>  $params['c_p_disp'],
            'created'   =>  $app['session']->get('id')
        ];

        $record     =   CoordPreTmp::SavePreCoord($iData);

        if($record <> false)
        { 

            $coord  =   Coordination::UpdateTicket($iData, $record, 'PRE');

            if($coord <> false)
            {
                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - PreCoordinacion', 'message' => 'Contacto de PreCoordinacion de Cliente - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  true,
                    'title'     =>  'Success',
                    'content'   =>  'Datos Actualizados Correctamente'
                ));

            }else{
                
                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - PreCoordinacion ', 'message' => 'Contacto de PreCoordinacion de Cliente - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                ));
            }

        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - PreCoordinacion ', 'message' => 'Contacto de PreCoordinacion de Cliente - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));
        }
        
    }

////////////////////////////////////////////////////////////////////////////////

    public function ViewDBCoord(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('str'), $params);

        $cIData     =   Coordination::ViewCoord($params['id_c_i_coord']);

        $iData =    [
            'ticket'    =>  $cIData['ticket'],
            'client'    =>  $cIData['client_id'],
            'view'      =>  $params['c_i_correct'],
            'created'   =>  $app['session']->get('id')
        ];

        $record         =   CoordViewTmp::SaveViewData($iData);

        if($record <> false)
        { 

            $coord  =   Coordination::UpdateTicket($iData, $record, 'VIEW');

            if($coord <> false)
            {
                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Datos Correctos', 'message' => 'Datos Correctos de Cliente - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  true,
                    'title'     =>  'Success',
                    'content'   =>  'Datos Actualizados Correctamente'
                ));

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Datos Correctos', 'message' => 'Datos Correctos de Cliente - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                ));
                
            }

        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Datos Correctos', 'message' => 'Datos Correctos de Cliente - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));
        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function SubmitCoord(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('str'), $params);

        $preSub     =   Coordination::GetCoordInfoById($params['t_co_p_coord']);

        $iData =    [
            'ticket'    =>  $preSub['ticket'],
            'client'    =>  $preSub['client_id'],
            'answered'  =>  $params['co_answered'],
            'created'   =>  $app['session']->get('id'),
            'type'      =>  substr($preSub['ticket'], 0,2)
        ];

        $mens   =   '';

        if(($preSub['contact'] == "NO") OR ($preSub['contact'] == null)) 
        {
            $mens   =   'Imposible procesar una orden sin Contacto Previo.!';
        }
        elseif(($preSub['view'] == "NO") OR ($preSub['view'] == null)) 
        {
            $mens   =   'Imposible procesar una orden sin verificaci&oacute;n de Datos.!';
        }
        elseif(($preSub['pre'] == "NO") OR ($preSub['pre'] == null)) 
        {
            $mens   =   'Imposible procesar una orden sin una PreCordinacion Previa!';
        }

        if($mens <> "")
        {   return $app->json(array(    'status'    =>  false,  'title'     =>  'Error',    'content'   =>  $mens));    }

        $record         =   CoordProcessTmp::SaveProcessCoord($iData);

        if($record <> false)
        { 

            $coord  =   Coordination::UpdateTicket($iData, $record, 'C_ID');

            if($coord <> false)
            {
                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Procesamiento de Orden', 'message' => 'Procesamiento de Orden - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  true,
                    'title'     =>  'Success',
                    'content'   =>  'Datos Actualizados Correctamente'
                ));

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Procesamiento de Orden', 'message' => 'Procesamiento de Orden - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                ));
                
            }

        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Procesamiento de Orden', 'message' => 'Procesamiento de Orden - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));
        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function SubmitReferredCoord(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('str'), $params);


        $preSub     =   Coordination::GetCoordInfoById($params['t_co_pr_coord']);

        $iData =    [
            'ticket'    =>  $preSub['ticket'],
            'client'    =>  $preSub['client_id'],
            'answered'  =>  $params['cr_answered'],
            'created'   =>  $app['session']->get('id'),
            'type'      =>  substr($preSub['ticket'], 0,2)
        ];

        $record         =   CoordReferredTmp::SaveReferredCoord($iData);

        if($record <> false)
        { 

            $coord  =   Coordination::UpdateTicket($iData, $record, 'C_RE');

            if($coord <> false)
            {
                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Procesamiento de Orden - Referido', 'message' => 'Procesamiento de Orden - Referido - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  true,
                    'title'     =>  'Success',
                    'content'   =>  'Datos Actualizados Correctamente'
                ));

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Procesamiento de Orden - Referido', 'message' => 'Procesamiento de Orden - Referido - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                ));
                
            }

        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Procesamiento de Orden - Referido', 'message' => 'Procesamiento de Orden - Referido - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));
        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function PackageTvPR(Application $app, Request $request)
    {
        $ticket     =   $request->get('t_c');
        $id         =   $request->get('id_c');

        $serv       =   ServiceTvPr::GetServiceByTicket($ticket);

        if($serv <> false)
        {
            return $app->json(array(
                'status'    =>  true,
                'type'      =>  'TV',
                'tv'        =>  $serv
            )); 

        }else{

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Ticket - '.$ticket.' - No Encontrado, Por favor intente de nuevo o contacte a su administrador de sistema.'
            ));

        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function PackageSePR(Application $app, Request $request)
    {

        $ticket     =   $request->get('t_c');
        $id         =   $request->get('id_c');

        $serv       =   ServiceSecPr::GetServiceByTicket($ticket);

        if($serv <> false)
        {
            return $app->json(array(
                'status'    =>  true,
                'type'      =>  'SE',
                'sec'       =>  $serv
            )); 

        }else{

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Ticket - '.$ticket.' - No Encontrado, Por favor intente de nuevo o contacte a su administrador de sistema.'
            ));

        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function PackageInPR(Application $app, Request $request)
    {
        $ticket     =   $request->get('t_c');
        $client     =   $request->get('id_c');

        $serv       =   ServiceIntPr::GetServiceByTicket($ticket);

        if($serv <> false)
        {
            return $app->json(array(
                'status'    =>  true,
                'type'      =>  'IN',
                'int'       =>  $serv
            )); 

        }else{

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Ticket - '.$ticket.' - No Encontrado, Por favor intente de nuevo o contacte a su administrador de sistema.'
            ));

        } 
    }

////////////////////////////////////////////////////////////////////////////////

    public function PackageIVEPR(Application $app, Request $request)
    {
        $ticket     =   $request->get('t_c');
        $client     =   $request->get('id_c');

        $serv       =   ServiceIntVZ::GetServiceByTicket($ticket);

        if($serv <> false)
        {
            return $app->json(array(
                'status'    =>  true,
                'type'      =>  'IV',
                'ive'       =>  $serv
            )); 

        }else{

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Ticket - '.$ticket.' - No Encontrado, Por favor intente de nuevo o contacte a su administrador de sistema.'
            ));

        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function SubmitTelevisionPR(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];

        parse_str($request->get('str'), $params);  

        $iData  =   [
            'ticket'        =>  $params['t_t_s_coord'],
            'client'        =>  $params['id_t_scoord'],
            'tv'            =>  $params['pr_tv_co_tv'],
            'package'       =>  $params['pr_tv_co_packages'],
            'provider'      =>  $params['pr_tv_co_providers'],
            'decoders'      =>  $params['pr_tv_co_decoder'],
            'dvrs'          =>  $params['pr_tv_co_dvrs'],
            'payment'       =>  $params['pr_tv_co_payment'],
            'hd'            =>  (isset($params['pr_tv_co_hd']))        ? 'on' : 'off',
            'dvr'           =>  (isset($params['pr_tv_co_dvr']))       ? 'on' : 'off',
            'hbo'           =>  (isset($params['pr_tv_co_hbo']))       ? 'on' : 'off',
            'cinemax'       =>  (isset($params['pr_tv_co_cinemax']))   ? 'on' : 'off',
            'starz'         =>  (isset($params['pr_tv_co_startz']))    ? 'on' : 'off',
            'showtime'      =>  (isset($params['pr_tv_co_showtime']))  ? 'on' : 'off',
            'gift_ent'      =>  (isset($params['pr_tv_co_gif_ent']))   ? 'on' : 'off',
            'gift_cho'      =>  (isset($params['pr_tv_co_gif_cho']))   ? 'on' : 'off',
            'gift_ext'      =>  (isset($params['pr_tv_co_gif_ext']))   ? 'on' : 'off',
            'service'       =>  '2',
            'operator'      =>  $app['session']->get('id'),
            'date'          =>  date('Y-m-d H:i:s', time())
        ];

        $serv   =   ServiceTvPr::UpdateServiceCoord($iData);

        if($serv == true)
        {
            $upgrade    =   CoordPackageTmp::SaveEditCoord($iData);

            if($upgrade <> false)
            {
                $coord      =   Coordination::UpdateTicket($iData, $upgrade, 'EDIT');

                if($coord <> false)
                {
                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Televivion PR Paquete', 'message' => 'Actualizacion de Paquete - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  true,
                        'title'     =>  'Success',
                        'content'   =>  'Datos Actualizados Correctamente'
                    ));

                }else{

                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Televivion PR Paquete ', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  false,
                        'title'     =>  'Error',
                        'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                    ));
                }

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Televivion PR Paquete ', 'message' => 'Actualizacion de Paquete - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Packete Actualizo, pero no se modifico en la tabla de coordinacion. Intente nuevamente, si el problema perfiste contacte al administrador de sistemas.'
                ));
            }

        }else{
            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Televivion PR Paquete', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));

        }
    }

////////////////////////////////////////////////////////////////////////////////

    public function SubmitSecurityPR(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];

        parse_str($request->get('str'), $params);

        $iData  =   [
            'ticket'        =>  $params['t_s_s_coord'],
            'client'        =>  $params['id_s_scoord'],
            'previosly'     =>  $params['pr_sec_co_previously_co'],
            'equipment'     =>  $params['pr_sec_co_equipment'],
            'cameras'       =>  $params['pr_sec_co_cameras'],
            'comp_equip'    =>  $params['pr_sec_co_company'],
            'dvr'           =>  $params['pr_sec_co_dvr'],
            'add_equip'     =>  $params['pr_sec_co_add'],
            'pass_alarm'    =>  $params['pr_sec_co_password'],
            'payment'       =>  $params['pr_sec_co_payment'],
            'cont1'         =>  $params['pr_sec_co_cont_1'],
            'cont1desc'     =>  $params['pr_sec_co_cont_1_desc'],
            'cont2'         =>  $params['pr_sec_co_cont_2'],
            'cont2desc'     =>  $params['pr_sec_co_cont_2_desc'],
            'cont3'         =>  $params['pr_sec_co_cont_3'],
            'cont3desc'     =>  $params['pr_sec_co_cont_3_desc'],
            'service'       =>  '3',
            'operator'      =>  $app['session']->get('id'),
            'date'          =>  date('Y-m-d H:i:s', time())
        ];

        $serv   =   ServiceSecPr::UpdateServiceCoord($iData); 

        if($serv == true)
        {
            $upgrade    =   CoordPackageTmp::SaveEditCoord($iData);

            if($upgrade <> false)
            {
                $coord      =   Coordination::UpdateTicket($iData, $upgrade, 'EDIT');

                if($coord <> false)
                {
                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Security PR Paquete', 'message' => 'Actualizacion de Paquete - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  true,
                        'title'     =>  'Success',
                        'content'   =>  'Datos Actualizados Correctamente'
                    ));

                }else{

                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Security PR Paquete ', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  false,
                        'title'     =>  'Error',
                        'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                    ));
                }

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Security PR Paquete ', 'message' => 'Actualizacion de Paquete - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Packete Actualizo, pero no se modifico en la tabla de coordinacion. Intente nuevamente, si el problema perfiste contacte al administrador de sistemas.'
                ));
            }

        }else{
            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Security PR Paquete', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));

        }
    
    }

 ////////////////////////////////////////////////////////////////////////////////
    
    public function SubmitInternetPR(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];

        parse_str($request->get('str'), $params);

        $iData  =   [
            'ticket'        =>  $params['t_i_s_coord'],
            'client'        =>  $params['id_i_scoord'],
            'int_res'       =>  $params['pr_int_co_res'],
            'pho_res'       =>  $params['pr_int_co_res_pho'],
            'int_com'       =>  $params['pr_int_co_trade'],
            'pho_com'       =>  $params['pr_int_co_trade_pho'],
            'payment'       =>  $params['pr_int_co_payment'],
            'service'       =>  '4',
            'operator'      =>  $app['session']->get('id'),
            'date'          =>  date('Y-m-d H:i:s', time())
        ];

        $serv   =   ServiceIntPr::UpdateServiceCoord($iData); 

        if($serv == true)
        {
            $upgrade    =   CoordPackageTmp::SaveEditCoord($iData);

            if($upgrade <> false)
            {
                $coord      =   Coordination::UpdateTicket($iData, $upgrade, 'EDIT');

                if($coord <> false)
                {
                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet PR Paquete', 'message' => 'Actualizacion de Paquete - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  true,
                        'title'     =>  'Success',
                        'content'   =>  'Datos Actualizados Correctamente'
                    ));

                }else{

                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet PR Paquete ', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  false,
                        'title'     =>  'Error',
                        'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                    ));
                }

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet PR Paquete ', 'message' => 'Actualizacion de Paquete - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Packete Actualizo, pero no se modifico en la tabla de coordinacion. Intente nuevamente, si el problema perfiste contacte al administrador de sistemas.'
                ));
            }

        }else{
            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet PR Paquete', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));

        }
    
    }

////////////////////////////////////////////////////////////////////////////////

    public function SubmitInternetVZ(Application $app, Request $request)
    {
        $_replace   =   new Config();

        $params     =   [];

        parse_str($request->get('str'), $params);

        $iData  =   [
            'ticket'        =>  $params['t_iv_s_coord'],
            'client'        =>  $params['id_iv_scoord'],
            'int_res'       =>  $params['vzla_int_co_res'],
            'pho_res'       =>  $params['vzla_int_co_res_pho'],
            'int_com'       =>  $params['vzla_int_co_trade'],
            'pho_com'       =>  $params['vzla_int_co_trade_pho'],
            'payment'       =>  $params['vzla_int_co_payment'],
            'bank'          =>  $params['vzla_int_co_bank'],
            'amount'        =>  $params['vzla_int_co_amount'],
            'transf'        =>  $params['vzla_int_co_trans'],
            'transf_date'   =>  $params['vzla_int_co_date'],
            'email'         =>  $params['vzla_int_co_email'],
            'service'       =>  '6',
            'operator'      =>  $app['session']->get('id'),
            'date'          =>  date('Y-m-d H:i:s', time())
        ];

        $serv   =   ServiceIntVZ::UpdateServiceCoord($iData); 

        if($serv == true)
        {
            $upgrade    =   CoordPackageTmp::SaveEditCoord($iData);

            if($upgrade <> false)
            {
                $coord      =   Coordination::UpdateTicket($iData, $upgrade, 'EDIT');

                if($coord <> false)
                {
                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet VZLA Paquete', 'message' => 'Actualizacion de Paquete - EXITOSO - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  true,
                        'title'     =>  'Success',
                        'content'   =>  'Datos Actualizados Correctamente'
                    ));

                }else{

                    $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet VZLA Paquete ', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  false,
                        'title'     =>  'Error',
                        'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
                    ));
                }

            }else{

                $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet VZLA Paquete ', 'message' => 'Actualizacion de Paquete - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Packete Actualizo, pero no se modifico en la tabla de coordinacion. Intente nuevamente, si el problema perfiste contacte al administrador de sistemas.'
                ));
            }

        }else{
            $info = array('client' => $iData['client'], 'channel' => 'Coordinacion - Internet VZLA Paquete', 'message' => 'Actualizacion de Paquete - NINGUNO - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se pudo completar la operacion, intente nuevamente.'
            ));

        }
    
    }


////////////////////////////////////////////////////////////////////////////////

    public static function ViewCancel(Application $app, Request $request)
    {

        $cancels    =   CoordCancel::GetCoordCancels();
        
        $html       =   Coordination::ViewCancel($cancels, $request->get('id'));
        
        return $app->json(array(
            'status'    =>  true,
            'html'      =>  $html
        ));
    }

////////////////////////////////////////////////////////////////////////////////

    public Static function ProcCoordCancel(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('str'), $params);

        $select     =   (isset($params['cancel_sel_c'])) ? true : false;

        $cInfo      =   Coordination::ViewCoord($params['id_can_coord']);

        if($select == true)
        {

            if($params['cancel_note_c'] <> "")
            {

                $date       =   date('Y-m-d H:i:s', time());

                $serv       =   substr($cInfo['ticket'],0,2);

                switch ($serv) 
                {
                    case 'TV':
                        $service = 2;
                        break;
                    case 'SE':
                        $service = 3;
                        break;
                    case 'IN':
                        $service = 4;
                        break;
                    case 'IV':
                        $service = 6;
                        break;
                }

                $cData      =   [
                    'id'        => $params['id_can_coord'],
                    'ticket'    => $cInfo['ticket'], 
                    'client'    => $cInfo['client_id'], 
                    'service'   => $service, 
                    'notes'     => $params['cancel_note_c'],
                    'operator'  => $app['session']->get('id'),
                    'depart'    => $app['session']->get('departament'),
                    'date'      => $date
                ];

                $cancel     =   Coordination::CoordCancel($cData, $params);

                if($cancel == true)
                {
                    $info = array('client' => $cData['client'], 'channel' => 'Cancelacion de Coordinacion', 'message' => 'Cancelacion de Coordinacion - Satisfactoria - Ticket - '.$cData['ticket'].' - Realizado Por - '.$app['session']->get('username').' - Realizada Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    // $infoDB     =   Drives::ApproRespInfoCancel($cData['ticket']);
                    // $APBoom     =   Drives::InsertApproRespCancel($infoDB);

                    return $app->json(array(
                        'status'    =>  true,
                        'title'     =>  'Success',
                        'content'   =>  'Cancelacion Procesada Correctamente'
                    ));

                }else{

                    $info = array('client' =>  $cData['client'], 'channel' => 'Cancelacion de Coordinacion', 'message' => 'Cancelacion de Coordinacion - Error - Ticket - '.$cData['ticket'].' - Solicitado Por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));


                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  false,
                        'title'     =>  'Error',
                        'content'   =>  'Cancelacion de Coordinacion - Error - Ticket - '.$cData['ticket'].' - Realizado Por - '.$app['session']->get('username').''

                    ));
                }

            }else{

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Debe Dejar una nota validad para procesar la solicitud.'
                ));

            }
        
        }else{

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Debe seleccionar una opcion validad para procesar la solicitud.'
            ));
        }
    }

////////////////////////////////////////////////////////////////////////////////

    public static function LogCoord(Application $app, Request $request)
    {
        $cIData     =   Coordination::ViewCoord($request->get('id'));
        
        return $app->json(array(
            'status'    =>  true,
            'html'      =>  Coordination::LogCoord($cIData)
        ));

    }

////////////////////////////////////////////////////////////////////////////////
}