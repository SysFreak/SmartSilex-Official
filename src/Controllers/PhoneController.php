<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Phone;
use App\Models\PhoneProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class PhoneController extends BaseController
{
    
    public static function PhoneEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'phone'      => Phone::GetPhoneById($request->get('id'))
        ));
    
    }

    public function PhoneForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_pho'] == 'new')
        {
            $saveDep = Phone::SavePhone($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Phone New', 'message' => 'Creacion de Phone - '.$params['name_pho'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Phone New', 'message' => 'Error Al Intentar Crear Plan Telefonico - '.$params['name_pho'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                ));
            }        
        }
        elseif($params['type_pho'] == 'edit')
        {
            $saveDep = Phone::SavePhone($params);
           
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Phone Edit', 'message' => 'Actualizacion de Phone - '.$params['name_pho'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Phone New', 'message' => 'Error Al Intentar Actualizar Plan Telefonico - '.$params['name_pho'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
       
        }    
    
    }

//////////////////////////////////////////////////////////////////////////

    public static function EditProvider(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'provider'  => PhoneProvider::GetProviderById($request->get('id'))
        ));
    }

    public static function Provider(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_prov_t'] == 'new')
        {

            $saveDep = PhoneProvider::SaveProvider($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Phone Provider New', 'message' => 'Creacion de Proveedor de Telefonia - '.$params['name_prov_t'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Phone Provider New', 'message' => 'Error Al Intentar Crear Proveedor de Telefonia - '.$params['name_prov_t'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                ));
            }        
        }
        elseif($params['type_prov_t'] == 'edit')
        {
            $saveDep = PhoneProvider::SaveProvider($params);
           
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Phone Provider Edit', 'message' => 'Actualizacion de Proveedor de Telefonia - '.$params['name_prov_t'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Phone Provider Edit', 'message' => 'Error Al Intentar Actualizar Proveedor de Servicio - '.$params['name_prov_t'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
       
        }  
    }


}