<?php
namespace App\Controllers;
require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\UserPermit;
use App\Models\Departament;
use App\Models\Team;
use App\Models\Status;
use App\Models\Role;
use App\Models\Perfil;
use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
* 
*/
class UsersController extends BaseController
{

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function index(Application $app, Request $request)
    {
        return $app['twig']->render('users/index.html.twig',array(
            'sidebar'       =>  true,
            'users'         =>  User::GetUsers(),
            'departaments'  =>  Departament::GetDepartament(),
            'teams'         =>  Team::GetTeams(),
            'roles'         =>  Role::GetRole(),
            'status'        =>  Status::GetStatus(),
        ));
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function ShowEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'        => true, 
            'user'          => User::GetUserById($request->get('id'))
        ));
    }

//////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function ChangePasswordSubmit(Application $app, Request $request)
    {

        $params     =   [];

        parse_str($request->get('str'), $params);

        if($params['pass1'] == $params['pass2'])
        {
            $pass   =   md5($params['pass1']);

            $change =   User::UserPasswordUpdate($pass, $app['session']->get('id'));
      
            if($change  <> false)
            {
                $info = array('client' => '', 'channel' => 'Password Change', 'message' => 'Actualizacion de Password - '.$app['session']->get('username').' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array('status' => true));
            }else{
                
                $info = array('client' => '', 'channel' => 'Password Change', 'message' => 'Error Actualizando Password - '.$app['session']->get('username').' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array('status' => false));
            }

        }else{
            $info = array('client' => '', 'channel' => 'Password Change', 'message' => 'Error Actualizando Password - '.$app['session']->get('username').' - Solicitado por - '.$app['session']->get('username').' - Password No Coinciden.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array('status' => false));
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function PermitEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'        => true, 
            'permit'        => UserPermit::GetPermitByUserId($request->get('id'))
        ));
    }

//////////////////////////////////////////////////////////////////////////////////////////////////

    public function UserForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type'] == 'edit')
        {
            if(($params['password'] == "") AND ($params['cpassword'] == ""))
            {
                $userSave = User::SaveUser($params, "");
                
                if($userSave <> false)
                {

                    $info = array('client' => '', 'channel' => 'User Edit', 'message' => 'Actualizacion de informacion de Usuario - '.$params['username'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'   => true, 
                        'web'      => 'users'
                    ));

                }else{

                    $info = array('client' => '', 'channel' => 'User Edit', 'message' => 'Error Al Intentar Actualizar informacion del Usuario - '.$params['username'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                    )); 
                }
            
            }
            elseif( $params['password'] == $params['cpassword'] )
            {
                
                $pass = md5($params['password']);
                
                $userSave = User::SaveUser($params, $pass);
                
                if($userSave <> false)
                {

                    $info = array('client' => '', 'channel' => 'User Edit', 'message' => 'Actualizacion de informacion de Usuario - '.$params['username'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'   => true, 
                        'web'      => 'users'
                    ));

                }else{

                    $info = array('client' => '', 'channel' => 'User Edit', 'message' => 'Error Al Intentar Actualizar informacion del Usuario - '.$params['username'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                    )); 
                }

            }else{

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Password No Coinciden Intente Nuevamente", true)
                ));
            
            }

        }elseif ($params['type'] == 'new') {

            if( ($params['password'] == "") AND ($params['cpassword'] == "") )
            {
                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Debe Asignar Un Password Para el Usuario.", true)
                ));
            }
            elseif ($params['password'] == $params['cpassword']) 
            {
                $pass       = md5($params['password']);

                $userSave   = User::SaveUser($params, $pass);
                
                if($userSave <> false)
                {
                    $savePerm   =   UserPermit::SavePermit($userSave);

                    $info = array('client' => '', 'channel' => 'User New', 'message' => 'Creacion de Usuario - '.$params['username'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'   => true, 
                        'web'      => 'users'
                    ));

                }else{

                    $info = array('client' => '', 'channel' => 'User New', 'message' => 'Error Al Intentar Crear Usuario - '.$params['username'].' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                    )); 

                }

            }else{

               return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Password No Coinciden Intente Nuevamente", true)
                )); 
            }
        
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////

    public static function PermitForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params); 

        $iData  =   [
            'id'    =>  $params['id_p'],
            'db'    =>  (isset($params['db']))      ? "on"   :   "off" ,
            'ident' =>  (isset($params['ident']))   ? "on"   :   "off" ,
            'ss'    =>  (isset($params['ss']))      ? "on"   :   "off" ,
            'mkt'   =>  (isset($params['mkt']))     ? "on"   :   "off" ,
            'cc'    =>  (isset($params['cc']))      ? "on"   :   "off" ,
            'ab'    =>  (isset($params['ab']))      ? "on"   :   "off" ,
            'rf'    =>  (isset($params['rf']))      ? "on"   :   "off" ,
            'ac'    =>  (isset($params['ac']))      ? "on"   :   "off" ,
            'pr'    =>  (isset($params['pr']))      ? "on"   :   "off" ,
            'tvpr'  =>  (isset($params['tvpr']))    ? "on"   :   "off" ,
            'sepr'  =>  (isset($params['sepr']))    ? "on"   :   "off" ,
            'inpr'  =>  (isset($params['inpr']))    ? "on"   :   "off" ,
            'mrpr'  =>  (isset($params['mrpr']))    ? "on"   :   "off" ,
            'vz'    =>  (isset($params['vz']))      ? "on"   :   "off" ,
            'invz'  =>  (isset($params['invz']))    ? "on"   :   "off" ,
            'nt'    =>  (isset($params['nt']))      ? "on"   :   "off" ,
            'db'    =>  (isset($params['db']))      ? "on"   :   "off" ,
        ];

        $savePerm       =   UserPermit::EditPermit($iData);

        if($savePerm <> false)
        {
            $info = array('client' => '', 'channel' => 'Usuarios Permisos Lead', 'message' => 'Edicion de Permisos de Lead de Usuario - '.User::GetUserById($iData['id'])['username'].' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   => true, 
                'web'      => 'users'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Usuarios Permisos Lead', 'message' => 'Error Al Intentar Editar Permisos de Lead al Usuario - '.User::GetUserById($iData['id'])['username'].' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
            )); 

        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////

}