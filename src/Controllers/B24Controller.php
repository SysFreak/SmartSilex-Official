<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use App\Lib\Config;
use App\Lib\DBSmart;
use App\Lib\DBMikroVE;

/**
 * 
 */
class B24Controller extends BaseController
{
    
    public function index(Application $app, Request $request)
    {

        return $app['twig']->render('B24/index.html.twig',array(
            'sidebar'       =>  true,
        ));
    }

    public function search(Application $app, Request $request)
    {
        $_replace   =   new Config();

        parse_str($request->get('str'), $params);

        $fiels      =   ['id', 'nombre', 'correo', 'telefono', 'movil'];
        $iFielsG    =   'COUNT(*) T';

        $iFiels =   ''; foreach ($fiels as $f => $fi) { $iFiels.=$fi.', '; } $iFiels    =   substr($iFiels, 0, -2).'';
        $table  =   'usuarios';

        switch ($params['b24_type']) {
            case '1':
                $qu     =   '((telefono LIKE "%'.trim($params['b24_input']).'%") || (movil LIKE "%'.trim($params['b24_input']).'%"))';
                break;
            case '2':
                $qu     =   'name LIKE "%'.trim($params['b24_input']).'%"';
                break;
            case '3':
                $qu     =   'correo LIKE "%'.trim($params['b24_input']).'%"';
                break;
            default:
                $qu     =   '((telefono LIKE "%'.trim($params['b24_input']).'%") || (movil LIKE "%'.trim($params['b24_input']).'%"))';
                break;
        };

        $res    =   DBMikroVE::DBQuery('SELECT '.$iFielsG.' FROM '.$table.' WHERE '.$qu);

        if($res['T'] == '0')
        {
            return $app->json(array(
                'status'    =>  true, 
                'type'      =>  's',
                'message'   =>  'Cliente NO encontrado, desea continuar?'
            ));

        }
        else if($res['T'] == '1')
        {
            $res    =   DBMikroVE::DBQuery('SELECT '.$iFiels.' FROM '.$table.' WHERE '.$qu);

            $html   =   '';
            $html.='<table class="table table-striped table-bordered table-hover"><tbody><tr><td>Check</td><td>Mikrowisp;</td><td>Cliente</td><td>Telefono</td></tr><tr>';
            $html.='<td style="text-align: center;"><input type="checkbox" name="CheckCli'.$res['id'].'" id="CheckCli'.$res['id'].'"></td>';
            $html.='<td>'.$res['id'].'</td>';
            $html.='<td>'.$res['nombre'].'</td>';
            $html.='<td>'.preg_replace('([^0-9 \._\-@])','', $res['telefono']).'</td>';
            $html.='</tr></tbody></table>';

            return $app->json(array(
                'status'    =>  true, 
                'html'      =>  $html,
                'type'      =>  's',
                'message'   =>  'Cliente encontrado, desea continuar?'
            ));    

        }else{
            $res    =   DBMikroVE::DBQueryAll('SELECT '.$iFiels.' FROM '.$table.' WHERE '.$qu);
            $html   =   '';
            $html.='<table class="table table-striped table-bordered table-hover"><tbody><tr><td>Check</td><td>Mikrowisp;</td><td>Cliente</td><td>Telefono</td></tr>';
            foreach ($res as $r => $re) 
            {
                $html.='<tr>';
                $html.='<td style="text-align: center;"><input type="checkbox" name="CheckCli'.$re['id'].'" id="CheckCli'.$re['id'].'"></td>';
                $html.='<td>'.$re['id'].'</td>';
                $html.='<td>'.$re['nombre'].'</td>';
                $html.='<td>'.preg_replace('([^0-9 \._\-@])','', $re['telefono']).'</td>';
                $html.='</tr>';
            }
            $html.='</tbody></table>';


            return $app->json(array(
                'status'    =>  true, 
                'html'      =>  $html,
                'type'      =>  's',
                'message'   =>  'Cliente Cliente posee mas de una cuenta, desea continuar?'
            ));    
        
        }
        
    }

    public function process(Application $app, Request $request)
    {
        $_replace   =   new Config();

        parse_str($request->get('str'), $params);

        $fiels      =   ['id', 'nombre', 'correo', 'telefono', 'movil'];
        $iFielsG    =   'COUNT(*) T';

        $iFiels =   ''; foreach ($fiels as $f => $fi) { $iFiels.=$fi.', '; } $iFiels    =   substr($iFiels, 0, -2).'';
        $table  =   'usuarios';

        switch ($params['b24_type']) {
            case '1':
                $qu     =   '((telefono LIKE "%'.trim($params['b24_input']).'%") || (movil LIKE "%'.trim($params['b24_input']).'%"))';
                break;
            case '2':
                $qu     =   'name LIKE "%'.trim($params['b24_input']).'%"';
                break;
            case '3':
                $qu     =   'correo LIKE "%'.trim($params['b24_input']).'%"';
                break;
            default:
                $qu     =   '((telefono LIKE "%'.trim($params['b24_input']).'%") || (movil LIKE "%'.trim($params['b24_input']).'%"))';
                break;
        };

        $res    =   DBMikroVE::DBQueryAll('SELECT '.$iFiels.' FROM '.$table.' WHERE '.$qu);

        $cGen   =   0;
        if($res <> false)
        {
            foreach ($res as $c => $cal) 
            {
                $var    =   'CheckCli'.$cal['id'].'';

                if(isset($params[$var]) == "on")
                {
                    $ident[$c]  =   $cal['id'];
                    $cGen       =   $cGen+1;
                }
            }

        }else{

        }

        var_dump($params);exit;
    }

}
