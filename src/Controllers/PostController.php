<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\Medium;
use App\Models\Objetive;
use App\Models\Conversation;
use App\Models\ServiceM;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class PostController extends BaseController
{

    public static function PostEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'post'      => Conversation::GetPostById($request->get('id'))
        ));
    
    }

    public function PostForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_po'] == 'new')
        {
            $savePost = Conversation::SavePost($params);
            
            if($savePost <> false)
            { 
                $info = array('client' => '', 'channel' => 'Post New', 'message' => 'Creacion de Post MKT - '.strtoupper($params['name_po']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'medium'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Post New', 'message' => 'Error Al Intentar Crear Post MKT - '.strtoupper($params['name_po']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_po'] == 'edit')
        {

            $savePost = Conversation::SavePost($params);
            
            if($savePost <> false)
            {
                $info = array('client' => '', 'channel' => 'Post Edit', 'message' => 'Actualizacion de Post MKT - '.strtoupper($params['name_po']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'medium'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Post Edit', 'message' => 'Error Al Intentar Actualizar Informacion del Post MKT - '.strtoupper($params['name_po']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, Error Al Intentar Actualizar Informacion del Post MKT, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
                            
        }
    
    }

}