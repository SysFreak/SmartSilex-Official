<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Calendar;


use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
* 
*/
class CalendarController extends BaseController
{
	public static function index(Application $app)
	{
		$events 	= Calendar::Events($app['session']->get('id'));

		return $app['twig']->render('calendar/index.html.twig',array(
            'sidebar'              =>  true
        ));
	
	}

	public static function store(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('value'), $params);

        $insert = Calendar::InsertEvent($params, $app['session']->get('id'));

        if($insert == true)
        {
        	return json_encode(Calendar::ViewEvent($app['session']->get('id'))); 

        }else{

        	return false;
        }
	
	}

	public static function Notifie(Application $app, Request $request)
	{
		$events 	= Calendar::Events($app['session']->get('id'));

		if($events <> false)
		{
	        return $app->json(array(
	            'status'   	=> 	true, 
	            'events'   	=> 	$events['events']
	        ));
			
		}else{
			return $app->json(array('status'   => 	false));
		}
	
	}

	public static function Change(Application $app, Request $request)
	{
		$events 	= Calendar::Change($request->get('id'));

		if($events <> false)
		{
	        return $app->json(array('status'   => 	true));
			
		}else{

			return $app->json(array('status'   => 	false));
		}
	
	}

	public static function View(Application $app, Request $request)
	{

		$view = Calendar::ChangeView($app['session']->get('id'));

		$html = "";

		if($view['status'] <> false)
		{
			$html.='<ul class="notification-body">';

			if($view['status'] == false)
			{
				return $app->json(array('status' => true, $html => $html));
			}
			else
			{
				foreach ($view['info'] as $c => $cal) 
				{
					$html.='<li>';
					$html.='<span class="padding-10 unread">';
					$html.='<em class="badge padding-5 no-border-radius bg-color-purple txt-color-white pull-left margin-right-5"><i class="fa fa-calendar fa-fw fa-2x"></i></em>';
					$html.='<span>';
					$html.='<a href="javascript:void(0);" class="display-normal"><strong>Calendar</strong></a>: '.$cal['title'].'';
					$html.='<br>';
					$html.='<strong>Description: '.$cal['description'].'</strong><br>';
					$html.='<strong>Date: '.$cal['start_event'].'</strong><br>';
					$html.='</span>';
					$html.='</span>';
					$html.='</li>';
				}
				$html.='</ul>';
				return $app->json(array('status'   => 	true, 'html' => $html));
			}
		}
		else
		{
			return $app->json(array('status' => true, $html => $html));
		}	
	}

	public static function ListView(Application $app, Request $request)
	{
		$cal =	Calendar::ViewList($app['session']->get('id'));
		return $app->json(array('status' =>	$cal['status'], 'html' => $cal['html'], 'cant' => $cal['cant']));
	}

	public static function SaveCa(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('value'), $params);

        if($params['s_support'] == 0)
        {
	        return Calendar::InsertEvent($params, $app['session']->get('id'));

        }else{
        	return Calendar::InsertEvent($params, $params['o_support']);
        }

	
	}

}