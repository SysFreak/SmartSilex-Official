<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Team;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class TeamsController extends BaseController
{
    
    public static function TeamEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'team'      => Team::GetTeamById($request->get('id'))
        ));
    
    }

    public function TeamForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_t'] == 'new')
        {
            $saveDep = Team::SaveTeam($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Team New', 'message' => 'Creacion de Team - '.strtoupper($params['name_t']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'users'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Team New', 'message' => 'Error Al Intentar Crear Team - '.strtoupper($params['name_t']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        }
        elseif($params['type_t'] == 'edit')
        {

            $saveDep = Team::SaveTeam($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Team Edit', 'message' => 'Actualizacion de Team - '.strtoupper($params['name_t']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'users'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Team Edit', 'message' => 'Error Al Intentar Actualizar informacion del Team - '.strtoupper($params['name_t']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }   
        }    
    }
}