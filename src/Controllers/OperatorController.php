<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;


use App\Models\OperatorAssigned;
use App\Models\OperatorAssignedResp;
use App\Models\Objection;
use App\Models\Drives;
use App\Models\StatusChange;

use App\Lib\Config;

/**
*	Class Operator Controller 
*/
class OperatorController extends BaseController
{


	public static function index(Application $app, Request $request)
	{

		return $app['twig']->render('operator/index.html.twig',array(
            'sidebar'       =>  true,
            'operator'		=>	$app['session']->get('id'),
            'objections'	=>	Objection::GetObjectionAll()
        ));
	
	}

	public static function Assigend(Application $app, Request $request)
	{
		$operator 	=	$request->get('operator');

		$date 		=	date("Y-m-d"); 

		$data 		=	OperatorAssigned::GetAssigned($operator, $date);

		$html 		=	OperatorAssigned::GetTableAssigned($data['panel']);

		$sts 		=	OperatorAssigned::GetTableStatic($data['assig'], $data['contact'], $data['close']);
		
        return $app->json(array(
            'html'      => 	$html,
            'sts'		=>	$sts
        ));
	
	}
	
	public static function SearchDate(Application $app, Request $request)
	{
		$params     =   [];
		parse_str($request->get('str'), $params);

		$_replace   =   new Config();
		
		$info 	=	[
			'id'		=>	$request->get('ope'),
			'dateIni'	=>	$_replace->ChangeDate($params['dateIni']),
			'dateEnd'	=>	$_replace->ChangeDate($params['dateEnd'])
		];

		if($params['dateIni'] <> $params['dateEnd'])
		{
			$showInfo 	=	OperatorAssigned::GetAssignedByDate($info, "1");

		}else{

			$showInfo 	=	OperatorAssigned::GetAssignedByDate($info, "0");
		}

		$html 			=	OperatorAssigned::GetTableAssigned($showInfo['panel']);

		$sts 			=	OperatorAssigned::GetTableStatic($showInfo['assig'], $showInfo['contact'], $showInfo['close']);

        return $app->json(array(
            'html'      => 	$html,
            'sts'		=>	$sts

        ));
	
	}

	public static function RespAssigned(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('str'), $params);

		$date 		=	date('Y-m-d h:i:s', time());
		$client 	=	OperatorAssigned::GetAssignedById($params['assig_id'])['client'];
		$info 		=	[
			'operator'	=>	$app['session']->get('id'),
			'client'	=>	$client,
			'mkt'		=>	$params['assig_id'],
			'contacted'	=>	$params['contacted'],
			'tv_pr_c'	=>	$params['tv_pr_contacted'],
			'tv_pr_o'	=>	$params['tv_pr_objection'],
			'sec_pr_c'	=>	$params['sec_pr_contacted'],
			'sec_pr_o'	=>	$params['sec_pr_objection'],
			'int_pr_c'	=>	$params['int_pr_contacted'],
			'int_pr_o'	=>	$params['int_pr_objection'],
			'wrl_pr_c'	=>	$params['wrl_pr_contacted'],
			'wrl_pr_o'	=>	$params['wrl_pr_objection'],
			'date'		=>	$date
		];

		$infor 	=	OperatorAssignedResp::SaveResp($info);

		if($info <> false)
		{
			$resp 				=	OperatorAssigned::where('id', $params['assig_id'])->find_one();
			$resp->data_id 		=	$infor;
			$resp->process_at 	=	$date;
			$resp->status_id 	=	"1";
			$rsave 				=	($resp->save()) ? true : false;
			if($rsave)
			{
				$info = array('client' => $client, 'channel' => 'Asignacion Leads', 'message' => 'Respuesta de Asignacion de Leads - '.strtoupper($client).' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        	$app['datalogger']->RecordLogger($info);

	        	$infoDB     =   Drives::LeadRespInfo($client);
                $LeadBoom   =   Drives::InsertLeadResp($infoDB);


				return $app->json(array('status'    => 	true));

			}else{

				$info = array('client' => $client, 'channel' => 'Asignacion Leads', 'message' => 'Error en Respuesta de Asignacion de Leads - '.strtoupper($client).' - Ejecutado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        	$app['datalogger']->RecordLogger($info);

				return $app->json(array('status'    => 	true));
			}
		}else{

			$info = array('client' => $client, 'channel' => 'Asignacion Leads', 'message' => 'Error en Respuesta de Asignacion de Leads - '.strtoupper($client).' - Ejecutado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

        	$app['datalogger']->RecordLogger($info);

			return $app->json(array('status'    => 	true));
		}

	}

	public static function ViewStatus(Application $app, Request $request)
	{
		$id 	=	$request->get('id');
		$res 	=	StatusChange::GetStatusChangeByUser($id);

		return $app->json(array('status' => $res));
	}

	public static function StatusChange(Application $app, Request $request)
	{
		$id 	=	$request->get('id');
		$res 	=	StatusChange::GetStatusChangeByUser($id);

		if($res == 1)
		{

			$sta =	(StatusChange::SaveStatus($id, "0") == true) ? '0' : '1';
			return 	$app->json(array('status' => $sta));
		}else{
			$sta =	(StatusChange::SaveStatus($id, "1") == true) ? '1' : '0';
			return 	$app->json(array('status' => $sta));
		}
	}


}