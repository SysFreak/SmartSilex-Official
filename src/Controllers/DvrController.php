<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Dvr;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class DvrController extends BaseController
{
    
    public static function DvrEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'dvr'  => Dvr::GetDvrById($request->get('id'))
        ));
    
    }

    public function DvrForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_dvr'] == 'new')
        {
            $saveDep = Dvr::SaveDvr($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Dvr New', 'message' => 'Creacion de Dvr - '.strtoupper($params['name_dvr']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'cameras'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Dvr New', 'message' => 'Error Al Intentar Crear Paquete - '.strtoupper($params['name_dvr']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_dvr'] == 'edit')
        {
            $saveDep = Dvr::SaveDvr($params);
        
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Dvr Edit', 'message' => 'Actualizacion de Paquete - '.strtoupper($params['name_dvr']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'cameras'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Dvr Edit', 'message' => 'Error Al Intentar Actualizar informacion del Paquete - '.strtoupper($params['name_dvr']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }        
        }  
    }
}