<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Payment;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class PaymentController extends BaseController
{
    
    public static function PaymentEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'payment'      => Payment::GetPaymentById($request->get('id'))
        ));
    
    }

    public function PaymentForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_pa'] == 'new')
        {
            $savePay = Payment::SavePayment($params);
            
            if($savePay <> false)
            { 
                $info = array('client' => '', 'channel' => 'Payment New', 'message' => 'Creacion de Paquete - '.strtoupper($params['name_pa']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'banks'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Payment New', 'message' => 'Error Al Intentar Crear Paquete - '.strtoupper($params['name_pa']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_pa'] == 'edit')
        {

            $savePay = Payment::SavePayment($params);
            
            if($savePay <> false)
            {
                $info = array('client' => '', 'channel' => 'Payment Edit', 'message' => 'Actualizacion de Paquete - '.strtoupper($params['name_pa']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'banks'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Payment Edit', 'message' => 'Error Al Intentar Actualizar informacion del Paquete - '.strtoupper($params['name_pa']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        }    
    }
}