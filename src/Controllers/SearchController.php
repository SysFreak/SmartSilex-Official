<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Leads;
use App\Models\Country;
use App\Models\Bank;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Ceiling;
use App\Models\House;
use App\Models\Level;
use App\Models\Package;
use App\Models\Provider;
use App\Models\Payment;
use App\Models\Camera;
use App\Models\Dvr;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\StatusService;
use App\Models\Search;
use App\Models\Score;
use App\Models\Objection;
use App\Models\PhoneProvider;
use App\Models\StatusClient;
use App\Models\OrderType;
use App\Models\LeadsProvider;

use App\Models\IDType;
use App\Models\SSType;

use App\Lib\Config;
use App\Lib\Functions;
use App\Lib\DBPbx;
use App\Lib\Conexion;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use App\Lib\SugarDB;
use App\Lib\DataCypher;

use PDO;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class SearchController extends BaseController
{

//////////////////////////////////////////////////////////////////////////////////////////

    public function index(Application $app, Request $request)
    {
        if(isset($_POST['search-type']) == "simpled")
        {
            return $app['twig']->render('/search/index.html.twig',array(
                'sidebar'       =>  true,
                'leads'         =>  Leads::SearchSimpled($_POST)
            ));

        }
        else
        {
            return $app['twig']->render('/search/index.html.twig',array(
                'sidebar'       =>  true,
                'leads'         =>  Leads::AllClient(),
            ));
        }
        
    }


    public function SearchExterno(Application $app, Request $request)
    {

        if(is_numeric($_POST['search-fld']))
        {
            return $app['twig']->render('/search/index.html.twig',array(
                'sidebar'       =>  true,
                'leads'         =>  Leads::SearchExterno($_POST)
            ));

        }else{

            return $app['twig']->render('error/SearchExterno.html.twig',array(
                'sidebar'              =>  true
            ));

        }
    }

    public function SearchCypherClient(Application $app, Request $request)
    {
        $cypher =   New UnCypher();

        $data   =   $request->get('str');   
        $query  =   'SELECT client_id, name, phone_main, phone_alt, ident_id, ident_exp, ss_id, ss_exp FROM cp_leads WHERE phone_main LIKE "%'.$data.'%" ORDER BY id DESC';
        $inf    =   DBSmart::DBQueryAll($query);

        $html   =   '';

        if($inf <> false)
        {
            foreach ($inf as $k => $val) 
            {
                
                $info   =   $cypher->GetInfoCypher($val['client_id'], 'ID');
                $ide    =   $info['infoCard'];

                if($ide <> false) 
                    { $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = IDType::GetIdTypeById($info['info']['type_d'])['name'];}
                    // { $ident = $ide['id']; $ident_exp = $ide['exp']; $ident_type = "";}
                else 
                    { $ident = ""; $ident_exp = ""; $ident_type = ""; }

                $info   =   $cypher->GetInfoCypher($val['client_id'], 'SS');
                $sss    =   $info['infoCard'];

                if($sss <> false) 
                    { $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = SSType::GetSSTypeById($info['info']['type_d'])['name'];}
                    // { $ss = $sss['ss']; $ss_exp = $sss['exp']; $ss_type = "";}
                else 
                    { $ss = ""; $ss_exp = ""; $ss_type = ""; }

                $info   =   $cypher->GetInfoCypher($val['client_id'], 'CC');

                $cards  =   $info['infoCard'];


                if($cards <> false) 
                    { $cardName = $info['infoCard']['name']; $card = $cards['card']; $card_exp = $cards['month']."/".$cards['year']; }
                else 
                    { $cardName = ""; $card = ""; $card_exp = ""; }

                $info   =   $cypher->GetInfoCypher($val['client_id'], 'AB');

                $ABS    =   $info['infoCard'];

                if($ABS <> false) 
                    { 
                        $ab_nam = $ABS['name']; $ab_num = $ABS['account']; $ab_ban = Bank::GetBankById($ABS['bank'])['name']; $ab_typ = $ABS['type'];
                    }
                else 
                    { $ab_nam = ""; $ab_num = ""; $ab_ban = ""; $ab_typ = ""; }
                
                $html.='<table class="table table-bordered">';
                $html.='<tbody>';
                $html.='<tr>';
                $html.='<td>NAME</td>';
                $html.='<td>'.$val['name'].'</td>';
                $html.='<td>CLIENT</td>';
                $html.='<td>'.$val['client_id'].'</td>';
                $html.='<td>PHONE</td>';
                $html.='<td>'.$val['phone_main'].'</td>';
                $html.='<td>ALTER</td>';
                $html.='<td>'.$val['phone_alt'].'</td>';
                $html.='<td></td>';
                $html.='<td></td>';
                $html.='</tr>';
                $html.='<tr>';
                $html.='<td>ID</td>';
                $html.='<td>'.$ident.'</td>';
                $html.='<td>EXP</td>';
                $html.='<td>'.$ident_exp.'</td>';
                $html.='<td>SS</td>';
                $html.='<td>'.$ss.'</td>';
                $html.='<td>EXP</td>';
                $html.='<td>'.$ss_exp.'</td>';
                $html.='<td></td>';
                $html.='<td></td>';
                $html.='</tr>';
                $html.='<tr>';
                $html.='<td>CC</td>';
                $html.='<td>'.$card.'</td>';
                $html.='<td>EXP</td>';
                $html.='<td>'.$card_exp.'</td>';
                $html.='<td>NAME</td>';
                $html.='<td>'.$cardName.'</td>';
                $html.='<td>TYPE</td>';
                $html.='<td></td>';
                $html.='<td>CVV</td>';
                $html.='<td>'.$cards['cvv'].'</td>';
                $html.='</tr>';
                $html.='<tr>';
                $html.='<td>AB</td>';
                $html.='<td>'.$ab_num.'</td>';
                $html.='<td>BANCO</td>';
                $html.='<td>'.$ab_ban.'</td>';
                $html.='<td>TYPE</td>';
                $html.='<td>'.$ab_typ.'</td>';
                $html.='<td>NAME</td>';
                $html.='<td>'.$ab_nam.'</td>';
                $html.='<td></td>';
                $html.='<td></td>';
                $html.='</tr>';
                $html.='</tbody>';
                $html.='</table>';
                $html.='<br>';

            }

            return $app->json(array(
                'status'    =>  true,
                'html'      =>  $html,
            ));

        }else{

            $html = '<div class="alert alert-info fade in"><i class="fa-fw fa fa-info"></i><strong>Info!</strong> Sin Informacion para mostrar.</div>';

            return $app->json(array(
                'status'    =>  false,
                'html'      =>  $html,
            ));
        }
    
    }

    public function SearchCypherClientType(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);
        $html = '';

        $info = new Config();

        $user = $info->Curl()['user'];
        $pass = $info->Curl()['pass'];
        $host = $info->Curl()['host'];

        $url    = $host;

        $query      =   'SELECT id, cypher, key_id, type, c_type, last, exp, country_id FROM cp_cypherdata WHERE client_id = "'.$params['search-client'].'" AND c_type = "'.$params['search-type'].'" ORDER BY id DESC';
        $cypher     =   DBSmart::DBQueryAll($query);

        if($cypher <> false)
        {
            foreach ($cypher as $k => $val) 
            {
                $postfields = array(
                    'command'   =>  'infocard',
                    'id'        =>  $val['key_id']
                );

                array_push($postfields, $user, $pass);

                $ch     = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);        
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

                $output = curl_exec($ch);

                $appp     = new DataCypher($info->Ramdom(32));
                $infoCard = $appp->Uncypher($val['cypher'], $output);

                if($params['search-type'] == "SS")
                {
                    $html.='<table class="table table-bordered">';
                    $html.='<tbody>';
                    $html.='<tr>';
                    $html.='<td>SS</td>';
                    $html.='<td>'.$infoCard['ss'].'</td>';
                    $html.='<td>Exp</td>';
                    $html.='<td>'.$infoCard['exp'].'</td>';
                    $html.='</tr>';
                    $html.='</tbody>';
                    $html.='</table>';
                }
                elseif($params['search-type'] == "ID")
                {
                    $html.='<table class="table table-bordered">';
                    $html.='<tbody>';
                    $html.='<tr>';
                    $html.='<td>ID</td>';
                    $html.='<td>'.$infoCard['id'].'</td>';
                    $html.='<td>Exp</td>';
                    $html.='<td>'.$infoCard['exp'].'</td>';
                    $html.='</tr>';
                    $html.='</tbody>';
                    $html.='</table>';
                }
                elseif($params['search-type'] == "CC")
                {
                    $html.='<table class="table table-bordered">';
                    $html.='<tbody>';
                    $html.='<tr>';
                    $html.='<td>Nombre</td>';
                    $html.='<td>'.$infoCard['name'].'</td>';
                    $html.='<td>Tarjeta</td>';
                    $html.='<td>'.$infoCard['card'].'</td>';
                    $html.='<td>Vencimiento</td>';
                    $html.='<td>'.$infoCard['month'].'/'.$infoCard['year'].'</td>';
                    $html.='</tr>';
                    $html.='</tbody>';
                    $html.='</table>';
                }
                elseif($params['search-type'] == "AB")
                {
                    $html.='<table class="table table-bordered">';
                    $html.='<tbody>';
                    $html.='<tr>';
                    $html.='<td>Nombre</td>';
                    $html.='<td>'.$infoCard['name'].'</td>';
                    $html.='<td>Cuenta</td>';
                    $html.='<td>'.$infoCard['account'].'</td>';
                    $html.='<td>Banco</td>';
                    $html.='<td>'.Bank::GetBankById($infoCard['bank'])['name'].'</td>';
                    $html.='<td>Tipo</td>';
                    $html.='<td>'.$infoCard['type'].'</td>';
                    $html.='</tr>';
                    $html.='</tbody>';
                    $html.='</table>';

                }
                else
                {  
                    $html = '';    
                }

            }

            return $app->json(array(
                'status'    =>  true,
                'html'      =>  $html,
            ));

        }else{

            $html = '<div class="alert alert-info fade in"><i class="fa-fw fa fa-info"></i><strong>Info!</strong> Sin Informacion para mostrar.</div>';

            return $app->json(array(
                'status'    =>  false,
                'html'      =>  $html,
            ));
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////

    public static function Advanced(Application $app, Request $request)
    {

       return $app['twig']->render('/search/advanced.html.twig',array(
            'sidebar'       =>  true,
            'countrys'      =>  Country::GetCountry(),
            'packages'      =>  Package::GetPackage(),
            'providers'     =>  Provider::GetProvider(),
            'payments'      =>  Payment::GetPayment(),
            'cameras'       =>  Camera::GetCamera(),
            'dvrs'          =>  Dvr::GetDvr(),
            'intres'        =>  Internet::GetType('1','1'),
            'intcom'        =>  Internet::GetType('2','1'),
            'phoneres'      =>  Phone::GetType('1','1'),
            'phonecom'      =>  Phone::GetType('2','1'),
            'statusserv'    =>  StatusService::GetStatus(),
            'towns'         =>  Town::GetTown(),
            'zips'          =>  ZipCode::GetZipCode(),
            'houses'        =>  House::GetHouse(),
            'ceilings'      =>  Ceiling::GetCeiling(),
            'levels'        =>  Level::GetLevel(),
            'scores'        =>  Score::GetScore(),
            'objections'    =>  Objection::GetObjectionAll(),
            'cameras'       =>  Camera::GetCamera(),
            'dvrs'          =>  Dvr::GetDvr(),
            'ordertype'     =>  OrderType::GetOrderTypeStatus(),
            'intresvz'      =>  Internet::GetType('1','4'),
            'intcomvz'      =>  Internet::GetType('2','4'),
            'phoneresvz'    =>  Phone::GetType('1','4'),
            'phonecomvz'    =>  Phone::GetType('2','4'),
            'leadpro'       =>  LeadsProvider::GetLeadsProvider(),
        ));
   
    }

    public static function SearchAdvanced(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);

        $sType =   (isset($params['search-type'])) ? $params['search-type'] : false;

        switch ($sType) 
        {
            case "DataBasic":
                $rLeads     =   Search::GetDBQuery($params);
                $hLeads     =   Search::HtmlDBQuery($rLeads);
                break;
            case "basic_tvpr":
                $rLeads     =   Search::BasicTvPR($params);
                $hLeads     =   Search::HtmlBasicTvPR($rLeads);
                break;
            case "basic_secpr":
                $rLeads     =   Search::BasicSecPR($params);
                $hLeads     =   Search::HtmlBasicSecPR($rLeads);
                break;
            case "basic_netpr":
                $rLeads     =   Search::BasicNetPR($params);
                $hLeads     =   Search::HtmlBasicNetPR($rLeads);
                break;
            case "basic_netvz":
                $rLeads     =   Search::BasicNetVz($params);
                $hLeads     =   Search::HtmlBasicNetPR($rLeads);
                break;
            case "score_tvpr":
                $rLeads     =   Search::ScorePR($params);
                $hLeads     =   Search::HtmlInfoPR($rLeads);
                break;
            case "score_secpr":
                $rLeads     =   Search::ScorePR($params);
                $hLeads     =   Search::HtmlInfoPR($rLeads);
                break;
            case "score_netpr":
                $rLeads     =   Search::ScorePR($params);
                $hLeads     =   Search::HtmlInfoPR($rLeads);
                break;
            case "score_netvz":
                $rLeads     =   Search::ScorePR($params);
                $hLeads     =   Search::HtmlInfoPR($rLeads);
                break;
            case "objection_tvpr":
                $rLeads     =   Search::ObjectionPR($params);
                $hLeads     =   Search::HtmlObjectionPr($rLeads);
                break;
            case "objection_secpr":
                $rLeads     =   Search::ObjectionPR($params);
                $hLeads     =   Search::HtmlObjectionPr($rLeads);
                break;
            case "objection_netpr":
                $rLeads     =   Search::ObjectionPR($params);
                $hLeads     =   Search::HtmlObjectionPr($rLeads);
                break;
            case "objection_netvz":
                $rLeads     =   Search::ObjectionPR($params);
                $hLeads     =   Search::HtmlObjectionPr($rLeads);
                break;
                
            case false:
                $rLeads = Leads::AllClient();
                break;
        }

        return $app->json(array(
            'status'    =>  true,
            'html'      =>  $hLeads,
        ));
    
    }

//////////////////////////////////////////////////////////////////////////////////////////

    public static function ViewObjections(Application $app, Request $request)
    {
        $info       =   ['client'   =>  $request->get('id'), 'service'  => $request->get('serv')];
        $search     =   Search::SearchObjections($info);
        $html       =   Search::HtmlObjection($search);

        return $app->json(array(
            'status'    =>  true,
            'html'      =>  $html,
        ));
    
    }

//////////////////////////////////////////////////////////////////////////////////////////

    public static function SugarPhone(Application $app, Request $request)
    {

        return $app['twig']->render('/search/sugarphone.html.twig',array(
            'sidebar'       =>  true
        ));
    
    }

//////////////////////////////////////////////////////////////////////////////////////////

    public static function GetSugarPhone(Application $app, Request $request)
    {

        $_replace   =   new Config();

        $func   =   new Functions();

        $query  =   'SELECT id FROM leads WHERE phone_home LIKE "%'.$request->get('phone').'%" OR phone_other LIKE "%'.$request->get('phone').'%"';
        
        $id     =   SugarDB::DBQueryAll($query);

        if($id <> false)
        {

            foreach ($id as $v => $val) 
            {
                $query     =   'SELECT t2.id_c, UPPER(CONCAT(t1.first_name," ", t1.last_name)) AS name, (t1.birthdate), t1.phone_home AS phone_main, UPPER(t2.provider_phone_home_c) AS phone_main_provider, t1.phone_other AS phone_alt, UPPER(t2.provider_phone_other_c) AS phone_alt_provider, (SELECT t2.email_address_caps AS email FROM email_addr_bean_rel AS t1 INNER JOIN email_addresses AS t2 ON (t1.email_address_id = t2.id) AND t1.bean_id = "'.$val['id'].'" LIMIT 1) AS email_main, UPPER(t1.primary_address_postalcode) AS add_main, UPPER(t1.alt_address_postalcode) AS add_postal, UPPER(t2.town_list_c) AS town_id, UPPER(t2.housing_tenure_c) AS h_type_id, UPPER(t2.techo_internet_c) AS h_roof_id, UPPER(t1.description) AS description, (SELECT id_s FROM users_su_sm WHERE id = t1.created_by) AS user_id, (SELECT user_name FROM users WHERE id = t1.created_by) AS username, t1.date_entered FROM leads AS t1 INNER JOIN leads_cstm AS t2 ON (t1.id = t2.id_c) AND t1.id = "'.$val['id'].'"';

                $leadR     =   SugarDB::DBQuery($query);

                if($leadR <> false)
                {
                    $query  =   'SELECT client_id FROM cp_leads ORDER BY id DESC';

                    $tic    =   DBSmart::DBQuery($query);

                    $cli    =   (isset($tic['client_id'])) ? ($tic['client_id'] + 1) : "10001";

                    

                    $d      =   $func->LeadsSmart($leadR, $cli);

                    $query  =   'INSERT INTO cp_leads_copy1(client_id, referred_id, name, birthday, gender, age, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_by, created_at) VALUES ("'.$d['client_id'].'", "0", "'.$d['name'].'", "'.$d['birthday'].'", "'.$d['gender'].'", "'.$d['age'].'", "'.$d['phone_main'].'", "'.$d['phone_main_owner'].'", "'.$d['phone_main_provider'].'", "'.$d['phone_alt'].'", "'.$d['phone_alt_owner'].'", "'.$d['phone_alt_provider'].'", "'.$d['phone_other'].'", "'.$d['phone_other_owner'].'", "'.$d['phone_other_provider'].'", "'.$d['email_main'].'", "'.$d['email_other'].'", "'.$d['add_main'].'", "'.$d['add_postal'].'", "'.$d['coor_lati'].'", "'.$d['coor_long'].'", "'.$d['country_id'].'", "'.$d['town_id'].'", "'.$d['zip_id'].'", "'.$d['h_type_id'].'", "'.$d['h_roof_id'].'", "'.$d['h_level_id'].'", "'.$d['ident_id'].'", "'.$d['ident_exp'].'", "'.$d['ss_id'].'", "'.$d['ss_exp'].'", "'.$d['mkt_origen_id'].'", "'.$d['mkt_medium_id'].'", "'.$d['mkt_objective_id'].'", "'.$d['mkt_post_id'].'", "'.$d['mkt_forms_id'].'", "'.$d['mkt_conv_id'].'", "'.$d['mkt_service_id'].'", "'.$d['mkt_campaign_id'].'", "'.$d['additional'].'", "'.$d['created_by'].'", "'.$d['created_at'].'")';

                    $insert =   DBSmart::DataExecute($query);

                    if($insert <> false)
                    {
                        $query  =   'SELECT (t2.created_by) as suite_u_id, (SELECT id_s FROM users_su_sm WHERE id = t2.created_by) AS id_s, t2.description as nota, t2.date_entered  FROM leads_notes_1_c AS t1 INNER JOIN notes AS t2 ON (t1.leads_notes_1notes_idb = t2.id) AND t1.leads_notes_1leads_ida = "'.$val['id'].'"';

                        $notes  =   SugarDB::DBQueryAll($query);

                        if($notes <> false)
                        {
                            foreach ($notes as $n => $not) 
                            {
                                $query  =   'INSERT INTO cp_notes(client_id, note, img, type, size, user_id, created_at) VALUES ("'.$cli.'", "'.$_replace->deleteTilde($not['nota']).'", "", "", "", "'.$not['id_s'].'","'.$not['date_entered'].'")';

                                $insert =   DBSmart::DataExecute($query);

                                if($insert == false){ $nIns             =   $nIns++; $nInsert[$l]   =   array('id'  =>  $d['id_c']); }
                            }
                        }                    
                    
                    }

                    $char   =   $func->TownChar();

                    foreach ($char as $c => $ch) 
                    {
                        $query  =   'UPDATE cp_leads_copy1 SET town_id = REPLACE(town_id, "'.$ch['name'].'", "'.$ch['change'].'")';

                        $upd    =   DBSmart::DataExecute($query);
                    }

                    $town   =   $func->IdTown();

                    foreach ($town as $t => $tow) 
                    {
                        $query  =   'UPDATE cp_leads_copy1 SET town_id = REPLACE(town_id, "'.$tow['town'].'", "'.$tow['id'].'")';

                        $upd    =   DBSmart::DataExecute($query);
                    }   


                    $dChar  =   $func->DelCharAll();

                    foreach ($dChar as $d => $dCh) 
                    {
                        $query  =   'UPDATE cp_leads_copy1 SET phone_main = REPLACE(phone_main, "'.$dCh['name'].'", "'.$dCh['change'].'"), phone_alt = REPLACE(phone_alt, "'.$dCh['name'].'", "'.$dCh['change'].'"), phone_other = REPLACE(phone_other, "'.$dCh['name'].'", "'.$dCh['change'].'")';

                        $upd    =   DBSmart::DataExecute($query);
                    }


                    $query  =   'UPDATE cp_leads_copy1 SET phone_main = SUBSTR(phone_main, 1,10), phone_alt = SUBSTR(phone_alt, 1,10), phone_other = SUBSTR(phone_other, 1,10)';
                    $upd    =   DBSmart::DataExecute($query);      

                    $opt    =   $func->OptProvider();

                    foreach ($opt as $o => $ot) 
                    {
                        $query  =   'UPDATE cp_leads_copy1 SET phone_main_provider = REPLACE(phone_main_provider, "'.$ot['name'].'", "'.$ot['id'].'"), phone_alt_provider = REPLACE(phone_alt_provider, "'.$ot['name'].'", "'.$ot['id'].'"), phone_other_provider = REPLACE(phone_other_provider, "'.$ot['name'].'", "'.$ot['id'].'")';

                        $upd    =   DBSmart::DataExecute($query);

                    }

                        $query  =   'UPDATE cp_leads_copy1 SET phone_main_provider = REPLACE(phone_main_provider, "BOOS4", "8"), phone_alt_provider = REPLACE(phone_alt_provider, "BOOS4", "8"), phone_other_provider = REPLACE(phone_other_provider, "BOOS4", "8")';
                        $upd    =   DBSmart::DataExecute($query);


                    $cTH    =   $func->OptType();

                    foreach ($cTH as $t => $th) 
                    {
                        $query  =   'UPDATE cp_leads_copy1 SET h_type_id = REPLACE(h_type_id, "'.$th['name'].'", "'.$th['id'].'")';

                        $upd    =   DBSmart::DataExecute($query);

                    }


                    $cRoof  =   $func->OptRoof();

                    foreach ($cRoof as $r => $roo) 
                    {
                        $query  =   'UPDATE cp_leads_copy1 SET h_roof_id = REPLACE(h_roof_id, "'.$roo['name'].'", "'.$roo['id'].'")';

                        $upd    =   DBSmart::DataExecute($query);

                    }

                    $query      =   'SELECT * FROM cp_leads_copy1 WHERE client_id = "'.$cli.'"';

                    $dLead      =   DBSmart::DBQuery($query);

                    $query      =   'INSERT INTO cp_leads(client_id, referred_id, name, birthday, gender, age, phone_main, phone_main_owner, phone_main_provider, phone_alt, phone_alt_owner, phone_alt_provider, phone_other, phone_other_owner, phone_other_provider, email_main, email_other, add_main, add_postal, coor_lati, coor_long, country_id, town_id, zip_id, h_type_id, h_roof_id, h_level_id, ident_id, ident_exp, ss_id, ss_exp, mkt_origen_id, mkt_medium_id, mkt_objective_id, mkt_post_id, mkt_forms_id, mkt_conv_id, mkt_service_id, mkt_campaign_id, additional, created_by, created_at) VALUES ("'.$dLead['client_id'].'","'.$dLead['referred_id'].'","'.$dLead['name'].'","'.$dLead['birthday'].'","'.$dLead['gender'].'","'.$dLead['age'].'","'.$dLead['phone_main'].'","'.$dLead['phone_main_owner'].'","'.$dLead['phone_main_provider'].'","'.$dLead['phone_alt'].'","'.$dLead['phone_alt_owner'].'","'.$dLead['phone_alt_provider'].'","'.$dLead['phone_other'].'","'.$dLead['phone_other_owner'].'","'.$dLead['phone_other_provider'].'","'.$dLead['email_main'].'","'.$dLead['email_other'].'","'.$dLead['add_main'].'","'.$dLead['add_postal'].'","'.$dLead['coor_lati'].'","'.$dLead['coor_long'].'","1","1","1","1","1","1","","","","","1","1","1","1","1","1","1","1","'.$_replace->deleteTilde($dLead['additional']).'","'.$dLead['created_by'].'","'.$dLead['created_at'].'")';

                    $insertLead =   DBSmart::DataExecute($query);

                return $app->json(array(
                    'status'    =>  true,
                    'title'     =>  'success',
                    'content'   =>  'Datos procesados Correctamente'
                )); 




                }

                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'Sin Datos encontrados del cliente'
                ));   
            }

        }else{

            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se Encuentra Numero Telefonico asignado por favor verificar nuevamente.'
            ));    
        }

        var_dump($id);
        exit;
    
    }

//////////////////////////////////////////////////////////////////////////////////////////

}