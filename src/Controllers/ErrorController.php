<?php
namespace App\Controllers;
require '../vendor/autoload.php';

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
* 
*/
class ErrorController extends BaseController
{
	public static function License(Application $app)
	{
		return $app['twig']->render('error/license.html.twig');
	}

	public static function Lead(Application $app)
	{
		return $app['twig']->render('error/lead.html.twig');
	}
	
	public static function Permisos(Application $app)
	{
		return $app['twig']->render('error/permisos.html.twig');
	}

	public static function Suspended(Application $app)
	{
		return $app['twig']->render('error/temporable.html.twig');
	}
}