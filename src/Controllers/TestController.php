<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use SplFileObject;
use SplTempFileObject;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;

use App\Models\User;
use App\Models\UserPermit;
use App\Models\Town;
use App\Models\Status;
use App\Models\Departament;
use App\Models\Team;
use App\Models\Role;
use App\Models\CypherData;
use App\Models\Approvals;
use App\Models\ApprovalsRespCancel;
use App\Models\ApprovalsScore;
use App\Models\ApprovalsScoreTmp;
use App\Models\MarketingAssigned;
use App\Models\OperatorAssigned;
use App\Models\Cdr;
use App\Models\CallsOperator;
use App\Models\LeadSuite;
use App\Models\Drives;
use App\Models\Coordination;
use App\Models\Leads;
use App\Models\Score;
use App\Models\StatusLead;
use App\Models\CollectionVE;
use App\Models\CollectionBS;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\ServiceIntVZTmp;
use App\Models\Country;
use App\Models\ZipCode;
use App\Models\Ceiling;
use App\Models\Level;
use App\Models\House;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Ticket;
use App\Models\Transactions;

use App\Lib\DBMikroVE;
use App\Lib\DBMikro;
use App\Lib\DBPayment;
use App\Lib\Smart;
use App\Lib\Config;
use App\Lib\DBPbx;
use App\Lib\DBSugar;
use App\Lib\DBUber;
use App\Lib\DBSmart;
use App\Lib\DBSupport;
use App\Lib\DBBoom;
use App\Lib\DBBoom2;
use App\Lib\DBSugarTest;
use App\Lib\MultiLogger;
use App\Lib\SendMail;
use App\Lib\UnCypher;
use App\Lib\DBWebVe;
use App\Lib\Curl;


use App\Lib\ApiMWVz;

use Carbon\Carbon;
use \Datetime;
// use App\Emailer\SendEmail;


/**
* 
*/

class TestController extends BaseController	
{

///////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function index (Application $app, Request $request)
	{  
        $dI     =   "2022-08-01";
        $dF     =   "2022-08-31";
        $c      =   $tFact  =   $tRep   =   0;

        $data   =   DBMikroVE::DBQueryAll('SELECT TIPO, COUNT(*) T FROM reporte WHERE fecha2 BETWEEN "'.$dI.' 00:00:00" AND "'.$dF.' 23:59:59" GROUP BY tipo');

        foreach ($data as $k => $da) 
        {

            // if( ($da['TIPO'] == "Acuerdo Comercial") || ($da['TIPO'] == "Pago - Mixto") || ($da['TIPO'] == "Pago - PayPal"))
            // {
            //     $info   =   DBMikroVE::DBQueryAll('SELECT r.idcliente, r.nfactura, f.emitido, f.total, f.cobrado, f.estado, r.asunto, r.transaccion, r.total totalrepor, r.fecha2, r.tipo FROM reporte r INNER JOIN facturas f ON (r.nfactura = f.id) AND r.tipo = "'.$da['TIPO'].'" AND r.fecha2 BETWEEN "'.$dI.' 00:00:00" AND "'.$dF.' 23:59:59" ORDER BY f.id ASC');

            //     if($info <> false)
            //     {
            //         foreach ($info as $i => $inf) 
            //         {
            //             $tFact          =   ($tFact + $inf['cobrado']);
            //             $tRep           =   ($tRep  + $inf['totalrepor']);

            //             $iData[$c++]    =   [
            //                 'id'            =>  $inf['idcliente'],
            //                 'factura'       =>  $inf['nfactura'],
            //                 'emitido'       =>  $inf['emitido'],
            //                 'total'         =>  $inf['total'],
            //                 'cobrado'       =>  $inf['cobrado'],
            //                 'estado'        =>  $inf['estado'],
            //                 'asunto'        =>  $inf['asunto'],
            //                 'transaccion'   =>  $inf['transaccion'],
            //                 'reportado'     =>  $inf['totalrepor'],
            //                 'fecha'         =>  $inf['fecha2'],
            //                 'tipo'          =>  $inf['tipo'],
            //             ];
            //         }

            //         $tGeneral[$k]   =   [
            //             'tipo'          =>  $da['TIPO'],
            //             'facturado'     =>  $tFact,
            //             'Reportado'     =>  $tRep,
            //         ];
            //         $tFact  =   $tRep   =   0;
            //     }

            // }

            $info   =   DBMikroVE::DBQueryAll('SELECT r.idcliente, r.nfactura, f.emitido, f.total, f.cobrado, f.estado, r.asunto, r.transaccion, r.total totalrepor, r.fecha2, r.tipo FROM reporte r INNER JOIN facturas f ON (r.nfactura = f.id) AND r.tipo = "'.$da['TIPO'].'" AND r.fecha2 BETWEEN "'.$dI.' 00:00:00" AND "'.$dF.' 23:59:59" ORDER BY f.id ASC');

            if($info <> false)
            {
                foreach ($info as $i => $inf) 
                {
                    $tFact          =   ($tFact + $inf['cobrado']);
                    $tRep           =   ($tRep  + $inf['totalrepor']);

                    $iData[$c++]    =   [
                        'id'            =>  $inf['idcliente'],
                        'factura'       =>  $inf['nfactura'],
                        'emitido'       =>  $inf['emitido'],
                        'total'         =>  $inf['total'],
                        'cobrado'       =>  $inf['cobrado'],
                        'estado'        =>  $inf['estado'],
                        'asunto'        =>  $inf['asunto'],
                        'transaccion'   =>  $inf['transaccion'],
                        'reportado'     =>  $inf['totalrepor'],
                        'fecha'         =>  $inf['fecha2'],
                        'tipo'          =>  $inf['tipo'],
                    ];
                }

                $tGeneral[$k]   =   [
                    'tipo'          =>  $da['TIPO'],
                    'facturado'     =>  $tFact,
                    'Reportado'     =>  $tRep,
                ];
                $tFact  =   $tRep   =   0;
            
            }




        }
            ddd($data, $iData, $tGeneral);

        // ddd($data);

        ddd("Test Controller"); 
    
    }
	
///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function TestAuth (Application $app, Request $request)
    {
        $_conf      =   New Config;
        $objPHPExcel    =   new \PHPExcel();
            
        $query      =   'SELECT client_id FROM cp_leads WHERE country_id = "1" ORDER BY client_id DESC';
        $iDatVE     =   DBSmart::DBQueryAll($query);

        $query      =   'SELECT COUNT(*) AS T FROM cp_leads WHERE country_id = "1" ORDER BY client_id DESC';
        $iTotal     =   DBSmart::DBQuery($query);

        echo "Obteniendo Informacion Solicitada"."\n";

        foreach ($iDatVE as $iD => $dVE) 
        {
            $query  =   'SELECT client_id FROM data_cypher_pr WHERE client_id = "'.$dVE['client_id'].'"';
            $up     =   DBSmart::DBQuery($query);

            if($up == false)
            {
                $info   =   Coordination::GetInfoGen($dVE['client_id']);
                $iDat   =   Coordination::ObtDataGen($info['client']);
                $client =   $iDat['data'];


                $query  =   'INSERT INTO data_cypher_pr (client_id, name, birthday, latitude, longitude, referred, phone, provider1, alternative, provider2, other, provider3, town, zip, country, ceiling, level, rif, rif_exp, rif_type, cedula, cedula_exp, cedula_type, creditcard, creditcard_exp, bank, bank_num, bank_type, address, address_postal, email, additional) VALUES ("'.$client['client'].'", "'.$_conf->Clean($client['name']).'", "'.$client['birthday'].'", "'.$client['coord_la'].'", "'.$client['coord_lo'].'", "0", "'.$client['phone'].'", "'.$client['p_provider'].'", "'.$client['p_alter'].'", "'.$client['a_provider'].'", "'.$client['p_other'].'", "'.$client['o_provider'].'", "'.$client['town'].'", "3001", "'.$client['country'].'", "'.$client['ceiling'].'", "'.$client['level'].'", "'.$client['ss'].'", "'.$client['ss_exp'].'", "'.$client['ss_type'].'", "'.$client['id'].'", "'.$client['id_exp'].'", "'.$client['id_type'].'", "'.$client['cc'].'", "'.$client['cc_exp'].'", "'.$client['ab'].'", "'.$client['ab_num'].'", "'.$client['ab_type'].'", "'.$_conf->Clean($client['add']).'", "'.$_conf->Clean($client['add_postal']).'", "'.$_conf->Clean($client['email']).'", "'.$_conf->Clean($client['additional']).'")';

                $iData[$iD]    =   [
                    'client'    =>  $client['client'],
                    'Name'      =>  $client['name'],
                    'status'    =>  DBSmart::DataExecute($query)
                ];

                echo $client['client']." - ".$client['name']."\n";

            }


        }

        ddd("Termino");   
    
    } 

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function TestForms (Application $app, Request $request)
    {

        ddd("Form Test");
        return $app['twig']->render('test/index.html.twig',array(
            'sidebar'              =>  true
        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function TestChange (Application $app, Request $request)
    {
        $query  =   'SELECT id, username FROM users_copy WHERE (departament_id = "1" OR departament_id = "11") AND status_id = "1"';
        $iOpe   =   DBSmart::DBQueryAll($query);

        foreach ($iOpe as $o => $ope) 
        {
            $sub    =   substr($ope['username'], -5,2);

            if($sub == "SP" OR $sub == "CL")
            {
                $username   =   substr($ope['username'],0, (strlen($ope['username']) -6 )).".CS".substr($ope['username'], -3);

                $query      =   'UPDATE users_copy SET username="'.$username.'" WHERE id = "'.$ope['id'].'"';
                $iUpd       =   DBSmart::DataExecute($query);

                d($iUpd, $username);

            }
        }

        ddd("Termino");
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function CalTimeIVA($data)
    {
        $a  =   new DateTime("2021-11-01");
        $b  =   new DateTime($data);

        if( $b > $a )
        {
            return 1;
        }
        elseif($b == $a)
        {
            return 0;
        }
        elseif($b < $a)
        {
            return -1;
        }

        return ($b > $a) ? "mayor" : (($a < $b) ? "menor" : "igual o menor");

       //  ddd($res);

       // ddd(new DateTime('2000-01-01'));
       //  $date   =  "2021-11-01";
       //  $diff   =   ((strtotime($data) - strtotime($date))/86400);
       //  ddd($diff);
       //  return  (($diff < 0) ? false : true);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////



}