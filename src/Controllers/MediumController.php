<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\Medium;
use App\Models\Objetive;
use App\Models\Origen;
use App\Models\Forms;
use App\Models\Conversation;
use App\Models\ServiceM;
use App\Models\Campaign;
use App\Models\Creative;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class MediumController extends BaseController
{
    public function index(Application $app, Request $request)
    {

        return $app['twig']->render('marketing/config.html.twig',array(
            'sidebar'       =>  true,
            'mediums'       =>  Medium::GetMedium(),
            'objetives'     =>  Objetive::GetObjetive(),
            'origens'       =>  Origen::GetOrigen(),
            'formss'        =>  Forms::GetForms(),
            'posts'         =>  Conversation::GetPost(),
            'servicesM'     =>  ServiceM::GetServiceM(),
            'countrys'      =>  Country::GetCountry(),
            'services'      =>  Service::GetService(),
            'creatives'     =>  Creative::GetImgs(),
            'status'        =>  Status::GetStatus()
        ));
    }

    public static function MediumEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'medium'    => Medium::GetMediumById($request->get('id'))
        ));
    
    }

    public function MediumForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_me'] == 'new')
        {
            $saveMedium = Medium::SaveMedium($params);
            
            if($saveMedium <> false)
            { 
                $info = array('client' => '', 'channel' => 'Medium New', 'message' => 'Creacion de Medio MKT - '.strtoupper($params['name_me']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'medium'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Medium New', 'message' => 'Error Al Intentar Crear Medio MKT - '.strtoupper($params['name_me']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_me'] == 'edit')
        {

            $saveMedium = Medium::SaveMedium($params);
            
            if($saveMedium <> false)
            {
                $info = array('client' => '', 'channel' => 'Medium Edit', 'message' => 'Actualizacion de Medio MKT - '.strtoupper($params['name_me']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'medium'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Medium Edit', 'message' => 'Error Al Intentar Actualizar Informacion del Medio MKT - '.strtoupper($params['name_me']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, Error Al Intentar Actualizar Informacion del Medio MKT, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
                            
        }
    
    }

}