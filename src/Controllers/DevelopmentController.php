<?php
namespace App\Controllers;

require '../vendor/autoload.php';


use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SplFileObject;
use SplTempFileObject;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;

use App\Models\User;
use App\Models\UserPermit;
use App\Models\Town;
use App\Models\Status;
use App\Models\Departament;
use App\Models\Team;
use App\Models\Role;
use App\Models\CypherData;
use App\Models\Approvals;
use App\Models\ApprovalsRespCancel;
use App\Models\ApprovalsScoreTmp;
use App\Models\MarketingAssigned;
use App\Models\OperatorAssigned;
use App\Models\Cdr;
use App\Models\CallsOperator;
use App\Models\LeadSuite;
use App\Models\Drives;
use App\Models\Coordination;
use App\Models\Leads;
use App\Models\Score;
use App\Models\StatusLead;
use App\Models\CollectionVE;
use App\Models\CollectionBS;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\ServiceIntVZTmp;
use App\Models\Country;
use App\Models\ZipCode;
use App\Models\Ceiling;
use App\Models\Level;
use App\Models\House;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Ticket;

use App\Lib\DBMikroVE;
use App\Lib\DBMikro;
use App\Lib\DBPayment;
use App\Lib\Smart;
use App\Lib\Config;
use App\Lib\DBPbx;
use App\Lib\DBSugar;
use App\Lib\DBSmart;
use App\Lib\DBSupport;
use App\Lib\DBBoom;
use App\Lib\DBBoom2;
use App\Lib\MultiLogger;
use App\Lib\SendMail;
use App\Lib\UnCypher;
use App\Lib\DBWebVe;
use App\Lib\Curl;


use App\Lib\ApiMWVz;

use Carbon\Carbon;
// use App\Emailer\SendEmail;

// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

/**
* 
*/

class DevelopmentController extends BaseController	
{

///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function index (Application $app, Request $request)
	{
        return $app['twig']->render('development/index.html.twig',array(
            'sidebar'       =>  true,
        ));
    }    
	
///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function DevCSV (Application $app, Request $request)
    {

        if(!$_FILES['file']['name'] == "")
        {
            $name       = $_FILES['file']['name'];
            $ext        = 'csv';
            $type       = $_FILES['file']['type'];
            $tmpName    = $_FILES['file']['tmp_name'];

            if(($handle = fopen($tmpName, 'r')) !== FALSE) 
            {
                $row = 0;
                while(($data = fgetcsv($handle, 1000, ',')) !== FALSE) 
                {
                    $row++;

                    $T      =   strlen($data[1]);
                    $iD     =   $data[1];


                    if($T == 8)
                    {
                        $d1      =   substr($iD, 0,1);
                        if($d1 < 10){ $d = "0".$d1; }else{ $d = $d1; }
                        $m1     =   substr($iD, 2,1);
                        if($m1 < 10){ $m = "0".$m1; }else{ $m = $m1; }
                        $y  =   substr($iD, 4,4);
                        $iDate  =   $y."-".$m."-".$d;

                    }
                    elseif($T == 9)
                    {
                        $d1      =   substr($iD, 0,2);
                        if($d1 < 10){ $d = "0".$d1; }else{ $d = $d1; }
                        $m1     =   substr($iD, 3,1);
                        if($m1 < 10){ $m = "0".$m1; }else{ $m = $m1; }
                        $y  =   substr($iD, 5,4);
                        $iDate  =   $y."-".$m."-".$d;

                    }else{
                        $d1      =   substr($iD, 0,2);
                        if($d1 < 10){ $d = "0".$d1; }else{ $d = $d1; }
                        $m1     =   substr($iD, 3,2);
                        if($m1 < 10){ $m = "0".$m1; }else{ $m = $m1; }
                        $y  =   substr($iD, 6,4);
                        $iDate  =   $y."-".$m."-".$d;

                    }


                    // ddd($iDate);

                    $query      =   'INSERT INTO email_birthday (name, email, birthday) VALUES("'.$data[0].'", "'.strtolower($data[2]).'", "'.strtolower($iDate).'")';
                    $insert     =   DBSmart::DataExecute($query);
                    // $insert     =   true;

                    if ($insert) 
                    {

                        $rows[$row]     =   [
                            'name'      =>  $data[0],
                            'email'     =>  $data[2],
                            'birthday'  =>  $iDate,
                            'status'    =>  true
                        ];
                    }else{
                        $rows[$row]     =   [
                            'name'      =>  $data[0],
                            'email'     =>  $data[2],
                            'birthday'  =>  $iDate,
                            'status'    =>  false
                        ];

                    }                    
                }

            }

        }

        ddd($rows);

        return $app->json(array('status'    => true));


    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

}