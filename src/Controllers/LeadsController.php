<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Leads;
use App\Models\LeadsTmp;

use App\Models\Marketing;
use App\Models\MarketingTmp;

use App\Models\Emails;
use App\Models\Approvals;
use App\Models\Coordination;
use App\Models\Calendar;
use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\PhoneProvider;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\ReferredData;
use App\Models\Referred;
use App\Models\Notes;
use App\Models\StatusLead;
use App\Models\StatusService;
use App\Models\Medium;
use App\Models\Conversation;
use App\Models\Objetive;
use App\Models\Origen;
use App\Models\Forms;
use App\Models\ServiceM;
use App\Models\ServiceTvPr;
use App\Models\ServiceTvPrTmp;
use App\Models\ServiceSecPr;
use App\Models\ServiceSecPrTmp;
use App\Models\ServiceIntPr;
use App\Models\ServiceIntPrTmp;
use App\Models\ServiceMRouterPr;
use App\Models\ServiceIntVZ;
use App\Models\ServiceIntVZTmp;
use App\Models\ServiceMRouterPrTmp;
use App\Models\ServicePropietary;
use App\Models\LeadsServObjection;
use App\Models\StatusClient;
use App\Models\LeadsProvider;
use App\Models\LeadsCompTV;
use App\Models\LeadsCompSEC;
use App\Models\LeadsCompINT;
use App\Models\LeadsCompINTVZLA;
use App\Models\IDType;
use App\Models\SSType;

use App\Models\LeadTownShip;
use App\Models\LeadStates;
use App\Models\LeadSector;
use App\Models\LeadParish;
use App\Models\LeadGeographic;

use App\Models\Campaign;
use App\Models\CypherData;
use App\Models\OrderType;
use App\Models\Drives;

use Silex\Application;
use App\Lib\MultiLogger;
use App\Lib\Config;
use App\Lib\Conexion;
use App\Lib\DBSmart;
use App\Lib\UnCypher;
use App\Lib\InfoVE;
use App\Lib\DBBoom;


use App\Lib\SendMail;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;
/**
* 
*/
class LeadsController extends BaseController
{

///////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function index(Application $app, Request $request)
	{

		return $app['twig']->render('leads/index.html.twig',array(
            'sidebar'       =>  true
        ));
	
	}

///////////////////////////////////////////////////////////////////////////////////////////////

	public static function LeadAge(Application $app, Request $request)
	{
		$tiempo = strtotime($request->get('date')); 
        $ahora  = time(); 
        $edad   = ($ahora-$tiempo)/(60*60*24*365.25);  
        return 	$app->json(floor($edad));   

	}

///////////////////////////////////////////////////////////////////////////////////////////////

	public static function View(Application $app, Request $request)
	{
		$lead 		= 	Leads::GetLeadByClient($request->get('id'));

		if($lead == false)
		{

			return $app['twig']->render('error/lead.html.twig',array('sidebar' => true));

		}else{

			return $app['twig']->render('leads/index.html.twig',array(
	            'sidebar'       =>  true,
	            'ope'			=>	$app['session']->get('id'),
	            'lead'			=>	$lead,
				'client'		=>	$lead['client'],
				'county'		=>	$lead['country'],
	            'countrys'		=>	Country::GetCountry(),
	            'banks'			=>	Bank::GetBank(),
	            'dataRefs'		=>	ReferredData::GetReferred(),
	            'referreds'		=>	Referred::GetReferredByClient($request->get('id')),
	            'packages'		=>	Package::GetPackage(),
	            'providers'		=>	Provider::GetProvider(),
	            'users'			=>	User::GetUsers(),
	            'payments'		=>	Payment::GetPayment(),
	            'cameras'		=>	Camera::GetCamera(),
	            'dvrs'			=>	Dvr::GetDvr(),
	            'intres'		=>	Internet::GetType('1','1'),
	            'intcom'		=>	Internet::GetType('2','1'),
				'phoneres'		=>	Phone::GetType('1','1'),
	            'phonecom'		=>	Phone::GetType('2','1'),
	            'intresvz'		=>	Internet::GetTypeVzRes('1','4'),
	            'intcomvz'		=>	Internet::GetTypeVzCom('2','4'),
				'phoneresvz'	=>	Phone::GetType('1','4'),
	            'phonecomvz'	=>	Phone::GetType('2','4'),
	            'phonepro'		=>	PhoneProvider::GetProvider(),
	            'events'		=>	Calendar::GetEventsByClient($request->get('id')),
	            'statusserv'	=>	StatusService::GetStatus(),
	            'ordertypes'	=>	OrderType::GetOrderType(),
	            'client_id'		=>	$request->get('id'),
	            'leadpro'		=> 	LeadsProvider::GetLeadsProvider(),
	            'idType'		=>	IDType::GetIdType(),
	            'ssType'		=>	SSType::GetSSType(),
	            'township'		=>	LeadTownShip::GetTownShip(),
	            'sector'		=>	LeadSector::GetSector(),
	            'states'		=>	LeadStates::GetStates(),
	            'parish'		=>	LeadParish::GetParish(),

	        ));
			
		}

	}

///////////////////////////////////////////////////////////////////////////////////////////////

	public static function Status(Application $app, Request $request)
	{

		$lead = StatusLead::StatusLead($request->get('id'));

		$_replace 	= 	New Config();

		if($lead['status'] == 'online')
		{
			return $app->json(array('status'    => 	true, 'username' => $lead['ope'], 'online' => $_replace->ShowDateAll($lead['online'])));

		}else{
			return $app->json(array('status'    => 	false));

		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ChangeStatus(Application $app, Request $request)
	{
		$_replace 	= 	New Config();
        $id 		= 	$request->get('id');
        $client 	= 	$request->get('client');

        $lead 		=	StatusLead::StatusLead($client);

        $ope 		=	StatusLead::StatusOperator($app['session']->get('id'));

        // var_dump($lead);
        // var_dump($ope);
        // exit;

        if($lead['status'] == 'offline')
        {
        	if($ope['status'] == 'offline')
        	{
 				$iData 		=	['client' => $client, 'user' => $app['session']->get('id'), 'status'	=> '2'];
		        $online 	=	StatusLead::OnlineStatus($iData);

				$info 	= 	array('client' => $client, 'channel' => 'Online Lead', 'message' => 'Modificacion de Estatus de Llamada del Cliente - '.strtoupper($client).' - Realizada por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

				$app['datalogger']->RecordLogger($info);

				return $app->json(array(
					'status'    => 	true,
					'username'	=>	$app['session']->get('username'),
					'online'	=>  date("m-d-Y H:i:s")
				));

        	}
        	elseif($ope['status'] == 'online')
        	{
        		if( $lead['client'] == $ope['client'] )
        		{

					$iData 		=	['client' => $client, 'user' => $app['session']->get('id'), 'status'	=> '2'];
			        $offline 	=	StatusLead::OfflineStatus($iData);

					$info = array('client' => $client, 'channel' => 'Online Lead', 'message' => 'Modificacion de Estatus de Llamada del Cliente - '.strtoupper($client).' - Realizada por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

					$app['datalogger']->RecordLogger($info);

					return $app->json(array(
						'status'    => 	false,
						'username'	=>	$app['session']->get('username')
					));

        		}
        		elseif( $lead['client'] <> $ope['client'] )
        		{
					return $app->json(array(
						'status'    => 	'error',
						'title'		=>	'Status - Ocupado',
						'content'	=>	'Operador se encuentra en linea con Cliente - '.$ope['client']
					));

        		}
        	
        	}

        }
        elseif($lead['status'] == 'online')
        {
        	if( $ope['status'] == 'offline' )
        	{
				return $app->json(array(
					'status'    => 	'error',
					'title'		=>	'Status - Ocupado',
					'content'	=>	'Cliente se encuentra Online con el Operador - '.$lead['ope']
				));

        	}
        	elseif( $ope['status'] == 'online' )
        	{
        		if( $lead['client'] == $ope['client'] )
        		{

					$iData 		=	['client' => $client, 'user' => $app['session']->get('id'), 'status'	=> '2'];
			        $offline 	=	StatusLead::OfflineStatus($iData);

					$info = array('client' => $client, 'channel' => 'Online Lead', 'message' => 'Modificacion de Estatus de Llamada del Cliente - '.strtoupper($client).' - Realizada por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

					$app['datalogger']->RecordLogger($info);

					return $app->json(array(
						'status'    => 	false,
						'username'	=>	$app['session']->get('username')
					));

        		}
        		elseif( $lead['client'] <> $ope['client'] )
        		{
					return $app->json(array(
						'status'    => 	'error',
						'title'		=>	'Status - Ocupado',
						'content'	=>	'Operador se encuentra en linea con Cliente - '.$ope['client']
					));

        		}
        	
        	}
        
        }
	
		return $app->json(array('status' => 'false'));

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function Search(Application $app, Request $request)
	{

		$lead = Leads::GetLeadByClient($request->get('client'));

		if($lead <> false)
		{
			return $app->json([
				'status' 		=>	true, 
				'lead' 			=> 	$lead,
				'town' 			=> 	Town::GetTownByCountry($lead['country']),
				'zip'			=>	ZipCode::GetZipByCountry($lead['country']),
				'house'			=>	House::GetHouseByCountry($lead['country']),
				'roof'			=>	Ceiling::GetCeilingByCountry($lead['country']),
				'level'			=>	Level::GetLevelByCountry($lead['country']),
				'geographic'	=>	LeadGeographic::GetGeographicByClient($request->get('client'))
			]);

		}else{
			return $app->json(array('status' => 'false', 'web' => '/ErrorLead'));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ZipChange(Application $app, Request $request)
	{

		return $app->json([
			'status' 	=>	true, 
			'zip'		=>	ZipCode::GetZipByTown($request->get('zip'))
		]);
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SearchID(Application $app, Request $request)
	{

		$lead = Leads::GetLeadByClient($request->get('client'));

		if($lead)
		{
			$SS = CypherData::GetCypherClientType($request->get('client'), "SS");

			$ID = CypherData::GetCypherClientType($request->get('client'), "ID");

			return $app->json(['status' => true, 'SS' => $SS, 'ID' => $ID]);
		}else{
			return $app->json(['status' => false, 'SS' => '', 'ID' => '']);
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ChangeOptions(Application $app, Request $request)
	{
		$cou = $request->get('country');

		return $app->json([	
			'status' 	=>	true, 
			'town' 		=> 	Town::GetTownByCountry($cou),
			'zip'		=>	ZipCode::GetZipByCountry($cou),
			'house'		=>	House::GetHouseByCountry($cou),
			'roof'		=>	Ceiling::GetCeilingByCountry($cou),
			'level'		=>	Level::GetLevelByCountry($cou),
			'medium'	=> 	Medium::GetMediumByCountry($cou),
			'objective'	=>	Objetive::GetObjectiveByCountry($cou),
			'origen'	=>	Origen::GetOrigenByCountry($cou),
			'post'		=>	Conversation::GetPostByCountry($cou),
			'service'	=>	ServiceM::GetServiceMByCountry($cou),
			'campaign'	=>	Campaign::GetCampaignByCountry($cou)

		]);
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function LoadImg(Application $app, Request $request)
	{

		$campaign = Campaign::GetCampaignById($request->get('id'));

		return $app->json([	
			'status' 	=>	true, 
			'campaign'	=> 	$campaign
		]);

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function Marketing(Application $app, Request $request)
	{

		$lead = Leads::GetLeadByClient($request->get('client'));

		return $app->json([	
			'status' 	=>	true, 
			'lead' 		=> 	$lead,
			'medium'	=> 	Medium::GetMediumByCountry($lead['country']),
			'objective'	=>	Objetive::GetObjectiveByCountry($lead['country']),
			'origen'	=>	Origen::GetOrigenByCountry($lead['country']),
			'post'		=>	Conversation::GetPostByCountry($lead['country']),
			'forms'		=>	Forms::GetFormsByCountry($lead['country']),
			'service'	=>	ServiceM::GetServiceMByCountry($lead['country']),
			'campaign'	=>	Campaign::GetCampaignByCountry($lead['country']),
			'users'		=>	User::GetUsers()
		]);

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function CreditCard(Application $app, Request $request)
	{

		$lead = Leads::GetLeadByClient($request->get('client'));

		$card = CypherData::GetCypherClientType($lead['client'], 'CC');

		if($card['c_type'] == "CC")
		{
			$infoC = New Config();

        	$exists = @get_headers( $infoC->Curl()['host']);

	        if(strpos($exists[0],'404') === false)
	        {
	            $curl = $app['curl']->execute(array('command' => 'license'));

	            if($curl == 'expired')
	            { 
	                return $app->json(['status'	=> false]);
	            }
	            else
	            {
	                $card = CypherData::GetCypherClientType($lead['client'], 'CC');

	                $keygen = $app['curl']->execute(array(
	                    'command'   =>  'infocard',
	                    'id'        =>  $card['key']
	                ));

	                if($keygen <> false)
	                {
	                    
	                    $infoCard = $app['cypher']->Uncypher($card['cypher'], $keygen);

	                    $iCard = [
	                    	'name'		=>	$infoCard['name'],
	                    	'CC'		=>	$card['CC'],
	                    	'month'		=>	$infoCard['month'],
	                    	'year'		=>	$infoCard['year'],
	                    	'cvv'		=>	(($infoCard['cvv'] <> null) or ($infoCard['cvv'] <> '')) ? "XX".substr($infoCard['cvv'], -1) : "000",
	                    	'country'	=>	$card['country'],
	                    	'type'		=>	$card['type']
	                    ];

                    	return $app->json(['status'	=> true, 'card' => $iCard]);

	                }else{

	                   return $app->json(['status'	=> false]);
	                }
	            }
	        }
	        else
	        {
				return $app->json(['status'	=> false]);
	        }

		}else{
			return $app->json(['status'	=> false]);
		}
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function AccountBank(Application $app, Request $request)
	{

		$lead = Leads::GetLeadByClient($request->get('client'));

		$account = CypherData::GetCypherClientType($lead['client'], 'AB');

		if($account['c_type'] == "AB")
		{
			$infoC = New Config();

        	$exists = @get_headers( $infoC->Curl()['host']);

	        if(strpos($exists[0],'404') === false)
	        {
	            $curl = $app['curl']->execute(array('command' => 'license'));

	            if($curl == 'expired')
	            { 
	                return $app->json(['status'	=> false]);
	            }
	            else
	            {
	                $account = CypherData::GetCypherClientType($lead['client'], 'AB');

	                $keygen = $app['curl']->execute(array(
	                    'command'   =>  'infocard',
	                    'id'        =>  $account['key']
	                ));

	                if($keygen <> false)
	                {
	                    
	                    $infoAccount = $app['cypher']->Uncypher($account['cypher'], $keygen);

	                    $iAccount = [
	                    	'name'		=>	$infoAccount['name'],
	                    	'account'	=>	"XXXX".mb_substr(trim(substr($infoAccount['account'], 0,strlen($infoAccount['account']))), -4),
	                    	'type'		=>	$infoAccount['type'],
	                    	'bank'		=>	$infoAccount['bank'],
	                    	'country'	=>	$account['country']
	                    ];

                    	return $app->json(['status'	=> true, 'account' => $iAccount]);

	                }else{

	                   return $app->json(['status'	=> false]);
	                }
	            }
	        }
	        else
	        {
				return $app->json(['status'	=> false]);
	        }

		}else{
			return $app->json(['status'	=> false]);
		}
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function Change(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('str'), $params);

		$inline = (isset($params['inline']))  ? "ONLINE"  : 'OFFLINE';

		$insert = StatusLead::InsertStatus($params['idClient'], $inline, $app['session']->get('username'));

		return $app->json(StatusLead::StatusLead($params['idClient'], $app['session']->get('username')));
		
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function Notes(Application $app, Request $request)
	{

		if(!$_POST['notes'] == "")
		{
			if(!$_FILES['file']['name'] == "")
			{
				$filename       =   $_FILES["file"]["name"];
        		$img            =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$filename);

        		if($img == true)
        		{
		        	$fileAdd    =   [
		                'name'  =>  $filename,
		                'size'  =>  $_FILES["file"]['size'],
		                'type'  =>  $_FILES["file"]["type"],
		                'dir'   =>  "assets/upload/".$filename
		            ];

        			$notes = Notes::SaveNoteCF($_POST, $fileAdd, $app['session']->get('id'));

					if($notes == true)
	                { 
	                    $info = array('client' => $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Satisfactoria - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	                    $app['datalogger']->RecordLogger($info);

	                    return $app->json(array(
	                        'status'   	=> true, 
	                        'notes'		=>	Notes::GetNotesHtml($_POST['client_id_n'])
	                    ));

	                }else{

	                    $info = array('client' =>  $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	                    $app['datalogger']->RecordLogger($info);

	                    return $app->json(array(
	                        'status'    => false, 
	                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
	                    ));
	                
	                }

        		}else{
        			return $app->json(array(
	                'status'    => false, 
                	'html'      => Auth::Notification("Error al intentar guardar archivo adjunto, intente nuevamente si el problema persiste contacte a su administrador de sistemas.", true)
            		)); 
        		}
			}else{
				$notes = Notes::SaveNoteSF($_POST, $app['session']->get('id'));

				if($notes == true)
                { 
                    $info = array('client' => $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Satisfactoria - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'   	=> true, 
                        'notes'		=>	Notes::GetNotesHtml($_POST['client_id_n'])
                    ));

                }else{

                    $info = array('client' =>  $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                    ));
                }
			}
		
		}else{

			return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("No se pudo procesar la solicitud, debe dejar una nota valida", true)
            )); 

		}	
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function GetNotes(Application $app, Request $request)
	{
		return $app->json(array(
            'notes'  => Notes::GetNotesHtml($request->get('client'))
        ));
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function GetReferred(Application $app, Request $request)
	{
		return $app->json(array(
            'referred'  => Referred::GetReferredByClient($request->get('client'))
        ));
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ReferredList(Application $app, Request $request)
	{
		return $app->json(array(
            'html'  => Referred::GetReferredClientHtml($request->get('id'), "1", $request->get('type'), $request->get('code'))
        ));
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ReferredListVZ(Application $app, Request $request)
	{
		return $app->json(array(
            'html'  => Referred::GetReferredClientVZHtml($request->get('id'), "1", $request->get('type'), $request->get('code'))
        ));
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveDB(Application $app, Request $request)
	{
        $params     =   $geographic 	=	[];

        parse_str($request->get('value'), $params);
        parse_str($request->get('geo'), $geographic);

        if(strlen($params['c_name']) > 2)
        {
    		if(strlen($params['c_phone']) >= 10)
    		{

        		if(strlen($params['c_phone_ow']) > 2)
        		{
        			if((isset($params['c_zip'])))
        			{
				        $Ltemp 	=	LeadsTmp::SaveDB($params, 'UPDATE', $app['session']->get('id'));

				        if($Ltemp <> false)
				        {
					        $leads 	= 	Leads::SaveDB($params);

					        if($leads <> false)
					        {

					        	if($params['c_country'] == "4")
					        	{
					        		$rGeo 	=	LeadGeographic::InsertGeographic($params['c_client'], $geographic, $app['session']->get('id'));

					        		if($rGeo <> false)
					        		{
										$info = array('client' => $params['c_client'], 'channel' => 'Lead Geographic VE', 'message' => 'Actualizacion de Geografia Venezuela - '.strtoupper($params['c_client']).' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

								        // $app['datalogger']->RecordLogger($info);
					        		}else{
										$info = array('client' => $params['c_client'], 'channel' => 'Lead Geographic VE', 'message' => 'Error Actualizacion en Geografia Venezuela - '.strtoupper($params['c_client']).' - Solicitado por - '.$app['session']->get('username'), 'time' => $app['date'], 'username' => $app['session']->get('username'));

								        // $app['datalogger']->RecordLogger($info);
					        		}
					        	}

								$info = array('client' => $params['c_client'], 'channel' => 'Lead Update', 'message' => 'Actualizacion de Lead - '.strtoupper($params['c_client']).' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

						        $app['datalogger']->RecordLogger($info);

								return $app->json(array(
					                'status'    => 	true,
					                'title'		=>	'Success',
					                'content'   => 	'Data Stored Correctly'
					            ));

					        }else{
								
								$info = array('client' => $params['c_client'], 'channel' => 'Lead Update', 'message' => 'Error Al Actualizar Lead - '.strtoupper($params['c_client']).' - Realizado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

						        $app['datalogger']->RecordLogger($info);

					    		return $app->json(array(
					                'status'    => false,
					                'title'		=>	'Error',
					                'content'   => 	'Error Attempting to Update the basic data of the Client Try again and if the error persists contact the System Administrator.'
					            ));
					        }

				        }else{

				        	$info = array('client' => $params['c_client'], 'channel' => 'Lead Update', 'message' => 'Error Al Actualizar Lead - '.strtoupper($params['c_client']).' - Realizado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

					        $app['datalogger']->RecordLogger($info);

				    		return $app->json(array(
				                'status'    => false,
				                'title'		=>	'Error',
				                'content'   => 	'Error Attempting to Update the basic data of the Client Try again and if the error persists contact the System Administrator.'
				            ));
				        
				        }
        			}else{
        				return $app->json(array(
				            'status'    => 	false,
				            'title'		=>	'Error',
				            'content'   => 	'Debe seleccionar un ZipCode Valido.'
			        	));
        			}
        		}else{

	        		return $app->json(array(
			            'status'    => 	false,
			            'title'		=>	'Error',
			            'content'   => 	'El propietario del telefono principal debe contener al menos dos caracteres.'
			        ));

        		}

			return $app->json(array(
	            'status'    => 	success,
	            'title'		=>	'success',
	            'content'   => 	'funciono.'
	        ));

    		}else{

        		return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'Numero telefonico principal debe Contener 10 Digitos.'
		        ));

    		}

        }else{
			
			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'El nombre debe tener mas de dos caracteres.'
	        ));
        }
        


	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveCypherID(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('value'), $params);

        $cypherID = CypherData::GetCypherClientType($params['client_ident'], "ID");

        if(($params['ident_client'] == "") ||  ($params['ident_exp'] == ""))
        {
        	
        	$info = array('client' => $params['client_ident'], 'channel' => 'Identificacion - Actualizacion', 'message' => 'Identificacion - No Actualizada - Datos Faltantes '.strtoupper($params['ident_client']).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

	  		return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Debe Completar Todos los Parametros.'
	        ));

        }
        elseif ($params['ident_client'] <> $cypherID['ID'] )
        {

			$infoU      =   New UnCypher();

        	$_replace 	= 	new Config();
			$exp 		=	$_replace->ChangeDate($params['ident_exp']);

			$cData = [
                'id'  	=>  strtoupper($params['ident_client']),
                'exp'  	=>  $exp
            ];

            $iData 	=	[
            	'client'	=>	$params['client_ident'],
            	'operator'	=>	$app['session']->get('id'),
            	'type'		=>	'ID',
            	'type_l'	=>	'ID',
            	'country'	=>	$params['ident_country'],
            	'exp'		=>	$exp,
            	'type_d'	=>	$params['ident_type']
            ];

			$SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

			if($SaveCypher == true)
			{
				$info = array('client' => $params['client_ident'], 'channel' => 'Identificacion', 'message' => 'Identificacion - Creada - Procesada Por - '.strtoupper($params['client_ident']).' - Realizado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

		        $id     =   CypherData::GetCypherClientType($params['client_ident'], 'ID');

		        $lead 	=	Leads::UpdateID($params['client_ident'], $id);

		        return $app->json($SaveCypher);

			}else{
				$info = array('client' => $params['client_ident'], 'channel' => 'Identificacion - Error', 'message' => 'Identificacion - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

		        return $app->json($SaveCypher);
			}        
        }
        else
        {
        	$info = array('client' => $params['client_ident'], 'channel' => 'Identificacion - Actualizacion', 'message' => 'Identificacion - Nada Que Actualizar - '.strtoupper($params['client_ident']).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);
           	
           	return $app->json(array(
                'status'    => 	true,
                'title'		=>	'Success',
                'content'   => 	'Nada Que Actualizar.'
        	));
        }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveCypherSS(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('value'), $params);

        $cypherSS = CypherData::GetCypherClientType($params['client_ident'], "SS");

        if(($params['ss_client'] == "") ||  ($params['ss_exp'] == ""))
        {
        	
        	$info = array('client' => $params['client_ident'], 'channel' => 'Seguro Social - Actualizacion', 'message' => 'Seguro Social - No Actualizada - Datos Faltantes - '.strtoupper($params['client_ident']).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

	  		return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Debe Completar Todos los Parametros.'
	        ));

        }
        elseif ($params['ss_client'] <> $cypherSS['SS'] )
        {

			$infoU      =   New UnCypher();

			$_replace 	= 	new Config();
			$exp 		=	$_replace->ChangeDate($params['ss_exp']);

			$cData = [
                'ss'  	=>  strtoupper($params['ss_client']),
                'exp'  	=>  $exp
            ];

            $iData 	=	[
            	'client'	=>	$params['client_ident'],
            	'operator'	=>	$app['session']->get('id'),
            	'type'		=>	'SS',
            	'type_l'	=>	'SS',
            	'country'	=>	$params['ss_country'],
            	'exp'		=>	$exp,
            	'type_d'	=>	$params['ss_type']
            ];

			$SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

			if($SaveCypher == true)
			{
				$info = array('client' => $params['client_ident'], 'channel' => 'Seguro Social', 'message' => 'Seguro Social - Creado - '.strtoupper($params['client_ident']).' - Realizado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

        		$ss     =   CypherData::GetCypherClientType($params['client_ident'], 'SS');

		        $lead 	=	Leads::UpdateSS($params['client_ident'], $ss);

		        return $app->json($SaveCypher);

			}else{
				$info = array('client' => $params['client_ident'], 'channel' => 'Seguro Social - Error', 'message' => 'Seguro Social - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

		        return $app->json($SaveCypher);
			}       
        }
        else
        {
        	$info = array('client' => $params['client_ident'], 'channel' => 'Seguro Social - Actualizacion', 'message' => 'Nada Que Actualizar - '.strtoupper($params['client_ident']).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);
           	
           	return $app->json(array(
                'status'    => 	true,
                'title'		=>	'Success',
                'content'   => 	'Nada que Actualizar.'
        	));
        }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveCypherCC(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('value'), $params);

        $cypherCC = CypherData::GetCypherClientType($params['cc_client'], "CC");

        if(($params['cc_holder'] == "") || ($params['cc_number'] == "") || ($params['cc_exp'] == ""))
        {
        	$info = array('client' => $params['cc_client'], 'channel' => 'Tarjeta de Credito - Actualizacion', 'message' => 'Tarjeta de Credito - No Actualizada - Datos Faltantes '.strtoupper($params['cc_client']).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

	  		return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Debe Completar todos los Parametros.'
	        ));

        }
        elseif ( $params['cc_number'] <> $cypherCC['CC'] )
        {


			$infoU      =   New UnCypher();

			$exp 		=	$params['cc_exp'];

			$cData = [
                'name'  =>  strtoupper($params['cc_holder']),
                'card'  =>  $params['cc_number'],
                'month' =>  $params['cc_month'],
                'year'  =>  $exp,
                'cvv'   =>  $params['cc_cvv'],                    
            ];

            $iData 	=	[
            	'client'	=>	$params['cc_client'],
            	'operator'	=>	$app['session']->get('id'),
            	'type'		=>	'CC',
            	'type_l'	=>	$params['cc_type'],
            	'country'	=>	$params['cc_country'],
            	'exp'		=>	$exp,
            	'type_d'    => 	"1"
            ];

			$SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

			if($SaveCypher == true)
			{
				$info = array('client' => $params['cc_client'], 'channel' => 'Tarjeta de Credito', 'message' => 'Tarjeta de Credito Creada Correctamente - Nombre - '.strtoupper($params['cc_client']).' - Realizado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

		        return $app->json($SaveCypher);

			}else{
				$info = array('client' => $params['cc_client'], 'channel' => 'Tarjeta de Credito - Error', 'message' => 'Error en Tarjeta de Credito - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

		        return $app->json($SaveCypher);
			}
        
        }
        else
        {
        	$info = array('client' => $params['cc_client'], 'channel' => 'Tarjeta de Credito - Actualizacion', 'message' => 'Nada que Actualizar - '.strtoupper($params['cc_client']).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);
           	
           	return $app->json(array(
                'status'    => 	true,
                'title'		=>	'Success',
                'content'   => 	'Nothing to modify'
        	));
        }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveCypherAB(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('value'), $params);

        $cypherAB = CypherData::GetCypherClientType($params['ab_client'], "AB");

        if(($params['ab_holder'] == "") || ($params['ab_number'] == ""))
        {
        	$info = array('client' => $params['ab_client'], 'channel' => 'Cuenta de Banco - Actualizacion', 'message' => 'Cuenta de Banco - No Actualizada - Datos Faltantes '.strtoupper($params['ab_client']).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

	  		return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Debe Completar Todos Los Parametros.'
	        ));

        }
        elseif ( $params['ab_number'] <> $cypherAB['AB'] )
        {

			$infoU      =   New UnCypher();
        	
	        $cData = [
	            'name'  =>  strtoupper($params['ab_holder']),
	            'account'   =>  $params['ab_number'],
	            'type'      =>  $params['ab_type'],
	            'c_type'    =>  'AB',
	            'bank'      =>  $params['ab_bank']                 
	        ];

	        $iData  =   [
	            'client'    =>  $params['ab_client'],
	            'operator'  =>  $app['session']->get('id'),
	            'type'      =>  'AB',
	            'type_l'    =>  $params['ab_type'],
	            'country'   =>  $params['ab_country'],
	            'exp'       =>  date("Y"),
	            'type_d'	=>	"1"
	        ];

			$SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

			if($SaveCypher == true)
			{
				$info = array('client' => $params['ab_client'], 'channel' => 'Cuenta de Banco', 'message' => 'Cuenta de Banco Creada Correctamente - Nombre - '.strtoupper($params['ab_client']).' - Realizado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

		        return $app->json($SaveCypher);
			}else{
				$info = array('client' => $params['cc_client'], 'channel' => 'Cuenta de Banco - Error', 'message' => 'Cuenta de Banco - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

		        return $app->json($SaveCypher);
			}       
        }
        else
        {
        	$info = array('client' => $params['ab_client'], 'channel' => 'Cuenta de Banco - Actualizacion', 'message' => 'Nada que Actualizar - '.strtoupper($params['ab_client']).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);
           	
           	return $app->json(array(
                'status'    => 	true,
                'title'		=>	'Satisfactorio',
                'content'   => 	'Nada Que Modificar'
        	));
        }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveMkt(Application $app, Request $request)
	{
        $params     =   [];

        parse_str($request->get('value'), $params);

        $leads 	= 	Leads::SaveMkt($params);

        $Mtemp 	=	MarketingTmp::SaveDB($params, 'UPDATE', $app['session']->get('id'));

        if($leads){

	        $info = array('client' => $params['mkt_client'], 'channel' => 'Lead Marketing Update', 'message' => 'Actualizacion Marketing Lead - '.strtoupper($params['mkt_client']).' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
                'status'    => 	true,
                'title'		=>	'Success',
                'content'   => 	'Data Stored Correctly'
            ));

        }else{
	        $info = array('client' => $params['mkt_client'], 'channel' => 'Lead Marketing Update', 'message' => 'Error Al Actualizar Marketing Lead - '.strtoupper($params['mkt_client']).' - Realizado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

    		return $app->json(array(
                'status'    => false,
                'title'		=>	'Error',
                'content'   => 	'Error Attempting to Update the basic data of the Client Try again and if the error persists contact the System Administrator.'
            ));
        }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// Service Television /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveTelevision(Application $app, Request $request)
	{
	    $params     =   [];

	    parse_str($request->get('value'), $params);
	    
	    $info 	=	[
	    	'client'		=>	(isset($params['pr_tv_client']))	? $params['pr_tv_client']		: $params['pr_tv_client'],	
	    	'tv'			=>	(isset($params['pr_tv_tv'])) 		? $params['pr_tv_tv'] 			: "0",
	    	'packages'		=>	(isset($params['pr_tv_packages'])) 	? $params['pr_tv_packages'] 	: "1",
	    	'providers'		=>	(isset($params['pr_tv_providers'])) ? $params['pr_tv_providers'] 	: "1",
	    	'decoders'		=>	(isset($params['pr_tv_decoder'])) 	? $params['pr_tv_decoder'] 		: "0",
	    	'dvrs'			=>	(isset($params['pr_tv_dvrs'])) 		? $params['pr_tv_dvrs'] 		: "0",
	    	'payment'		=>	(isset($params['pr_tv_payment'])) 	? $params['pr_tv_payment'] 		: "1",
	    	'referred'		=>	(isset($params['pr_tv_referred'])) 	? $params['pr_tv_referred']		: "0",
	    	'ref_list'		=>	(isset($params['pr_tv_ref_list'])) 	? $params['pr_tv_ref_list']		: "0",
	    	'sup'			=>	(isset($params['pr_tv_support'])) 	? $params['pr_tv_support']		: "0",
	    	'sup_list'		=>	(isset($params['pr_tv_ope_list'])) 	? $params['pr_tv_ope_list']		: "1",
	    	'service'		=>	(isset($params['pr_tv_status'])) 	? $params['pr_tv_status']		: "1",
	    	'advertising'	=>	(isset($params['pr_tv_adv'])) 		? $params['pr_tv_adv']			: "0",
	    	'type_order'	=>	(isset($params['pr_tv_type_order']))? $params['pr_tv_type_order']	: "1",
	    	'origen'		=>	(isset($params['pr_tv_origen']))	? $params['pr_tv_origen']		: "1",
	    	'hd'			=>	(isset($params['pr_tv_hd'])) 		? $params['pr_tv_hd']			: "off",
	    	'dvr'			=>	(isset($params['pr_tv_dvr'])) 		? $params['pr_tv_dvr']			: "off",
	    	'hbo'			=>	(isset($params['pr_tv_hbo'])) 		? $params['pr_tv_hbo']			: "off",
	    	'cinemax'		=>	(isset($params['pr_tv_cinemax'])) 	? $params['pr_tv_cinemax']		: "off",
	    	'starz'			=>	(isset($params['pr_tv_startz'])) 	? $params['pr_tv_startz']		: "off",
	    	'showtime'		=>	(isset($params['pr_tv_showtime'])) 	? $params['pr_tv_showtime']		: "off",
	    	'gif_ent'		=>	(isset($params['pr_tv_gif_ent'])) 	? $params['pr_tv_gif_ent']		: "off",
	    	'gif_cho'		=>	(isset($params['pr_tv_gif_cho'])) 	? $params['pr_tv_gif_cho']		: "off",
	    	'gif_xtra'		=>	(isset($params['pr_tv_gif_ext'])) 	? $params['pr_tv_gif_ext']		: "off",
	    	'operator'		=>	$app['session']->get('id')
	    ];

	    $save 	=	ServiceTvPrTmp::SaveService($info);

		if($save)
		{

	        $info = array('client' => $info['client'], 'channel' => 'Lead Service Tv Update', 'message' => 'Update Service Tv PR- '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	true,
	            'title'		=>	'Success',
	            'content'   => 	'Data Stored Correctly'
	        ));

	    }else{
	        $info = array('client' => $info['client'], 'channel' => 'Lead Service Tv Update', 'message' => 'Error Update Service Tv PR - '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Error while trying to update the tv service. If the error persists, contact the system Administrator.'
	        ));
	    }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewTelevision(Application $app, Request $request)
	{
		$servTv 	= 	ServiceTvPrTmp::ServiceTv($request->get('id'));
		$servInf 	=	Approvals::GetInfoService($request->get('id'), '2');
		$servCoo 	=	Coordination::GetInfoService($servInf['ticket']);

		if($servTv)
		{
			return $app->json([
				'status' 	=>	true, 
				'tv' 		=> 	$servTv,
				'inf'		=>	$servInf,
				'coor'		=>	$servCoo
			]);

		}else{
			return $app->json(array('status' => false));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewServiceTelevision(Application $app, Request $request)
	{
		$service = ServiceTvPrTmp::ServiceTv($request->get('id'));

		if($servTv)
		{
			return $app->json([
				'status' 	=>	true, 
				'tv' 		=> 	$servTv
			]);

		}else{
			return $app->json(array('status' => false));
		}
		
		exit;

	}


////////////////////////////////////////////////////////////////////////////////////////////////////

public static function SubmitTelevision(Application $app, Request $request)
{
	// $date 		= 	date('Y-m-d H:i:s', time());

	// $params     =   [];

	// $info 		=	"";

	// parse_str($request->get('value'), $params);

	// $client 	=	$params['pr_tv_client'];

	// $ticket 	= 	ServiceTvPr::GetTicket($client);

	// $serv 		=	ServiceTvPrTmp::ServiceTv($client);

	// if($serv['status'] <> false)
	// {
	// 	$saveServ 	=	ServiceTvPr::SaveService($ticket, $serv, $date);

	// 	if($saveServ == true)
	// 	{

	//         $info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Servicio de Television PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Enviado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 		$app['datalogger']->RecordLogger($info);

	// 		if($params['pr_tv_support'] == 0)
	// 		{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'),  'client' => $params['pr_tv_client'], 'created' => $serv['created']]; }
	// 		else
	// 		{ $user 	=	['owen'	=>	$params['pr_tv_ope_list'],  'assist' => $app['session']->get('id'), 'client' => $params['pr_tv_client'], 'created' => $serv['created']]; }

	// 		$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

	// 		$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

	// 		if($saveApro == true)
	// 		{
	// 			$query 	=	'SELECT 
	// 				t1.client_id,
	// 				t2.name,
	// 				t2.birthday,
	// 				t2.phone_main,
	// 				t2.phone_alt,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
	// 				t2.coor_lati,
	// 				t2.coor_long,
	// 				(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
	// 				t2.add_main,
	// 				t2.add_postal,
	// 				t2.email_main,
	// 				t2.email_other,
	// 				t2.additional,
	// 				t1.tv,
	// 				t1.decoder_id AS decoder,
	// 				(SELECT name FROM data_package WHERE id = t1.package_id) AS package,
	// 				t1.dvr_id AS dvrs,
	// 				(SELECT name FROM data_provider WHERE id = t1.provider_id) AS provider,
	// 				(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
	// 				(IF(t1.ch_hd = "on", "SI", "NO")) AS hd,
	// 				(IF(t1.ch_dvr = "on", "SI", "NO")) AS dvr,
	// 				(IF(t1.ch_hbo = "on", "SI", "NO")) AS hbo,
	// 				(IF(t1.ch_cinemax = "on", "SI", "NO")) AS cinemax,
	// 				(IF(t1.ch_starz = "on", "SI", "NO")) AS starz,
	// 				(IF(t1.ch_showtime = "on", "SI", "NO")) AS showtime,
	// 				(IF(t1.gif_ent = "on", "SI", "NO")) AS gift_ent,
	// 				(IF(t1.gif_choice = "on", "SI", "NO")) AS gift_cho,
	// 				(IF(t1.gif_xtra = "on", "SI", "NO")) AS gift_xtra,
	// 				(IF(t1.advertising_id = "1", "SI", "NO")) AS adv,
	// 				(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
	// 				(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
	// 				(IF(t1.ref_id = "1","SI","NO")) AS ref,
	// 				(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
	// 				(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
	// 				(IF(t1.sup_id = "1","SI","NO")) AS sup,
	// 				(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
	// 				(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
	// 				(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
	// 				(SELECT email FROM users WHERE id = prop_id) AS prop_email,
	// 				(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
	// 				(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
	// 				(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
	// 				(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
	// 				t1.created_at AS created
	// 				FROM cp_service_pr_tv AS t1 
	// 				INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
	// 				INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
	// 				AND t1.ticket =  "'.$ticket.'"';

	// 			$iData	=	DBSmart::DBQuery($query);

	// 			if($iData <> false)
	// 			{
	// 				$pData 	=	[
	// 					'username'		=>	$iData['prop'],
	// 					'departament'	=>	$iData['prop_depart'],
	// 					'subject'		=>	'TELEVISION - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/TV-icon.png',
	// 					'service'		=> 	'TELEVISION',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];

	// 				$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
	// 				$refHTML	=	Leads::GetReferredHtml($iData);
	// 				$supHTML	=	Leads::GetSupportHtml($iData);

	// 				$saleHTML	=	Leads::GetLeadSaleHtml($iData);
	// 				$subHTML	=	Leads::GetSubTvHtml($iData);

	// 				$mail   	=   new SendMail();
	// 				$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 				$pData 	=	[
	// 					'username'		=>	'DATA ENTRY',
	// 					'departament'	=>	'DATA ENTRY',
	// 					'subject'		=>	'TELEVISION - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/TV-icon.png',
	// 					'service'		=> 	'TELEVISION',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];
	// 				$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];

	// 				$mail   	=   new SendMail();
	// 		 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 		        $query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
	// 		        $emails =   DBSmart::DBQueryAll($query);

	// 		        if($emails <> false)
	// 		        {
	// 		        	foreach ($emails as $e => $mail) 
	// 		        	{
	// 		        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
	// 		        		$pData 	=	[
	// 							'username'		=>	strtoupper($mail['name']),
	// 							'departament'	=>	$mail['dept'],
	// 							'subject'		=>	'TELEVISION - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 							'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/TV-icon.png',
	// 							'service'		=> 	'TELEVISION',
	// 							'ticket'		=>	$ticket,
	// 							'created'		=>	$iData['created']
	// 						];
	// 						$mail   	=   new SendMail();
	// 						$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
	// 		        	}
	// 		        }

	// 		        $html = "";
	// 		        $html =	'<div class="row">
	// 							<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
	// 								<div class="panel panel-darken">
	// 									<div class="panel-heading" style="text-align: center;">
	// 										<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-desktop" style="font-size: 50px;"></i>
	// 										<h3 class="panel-title" style="margin-top: 10px;">TELEVISION PR</h3>
	// 									</div>
	// 									<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
	// 								</div>
	// 							</div>
	// 						</div>';

	// 			    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
	// 			    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

	// 			    $iTicket 	=	Drives::RequestTVInfoNew($ticket);
	// 				$iResult 	=	Drives::InsertDBTVNew($iTicket);

	// 				return $app->json(array(
	// 		            'status'    => 	true,
	// 		            'title'		=>	'Success',
	// 		            'content'   => 	'Datos Enviados Correctamente',
	// 		            'html'		=> 	$html
	// 		        ));

	// 			}else{

	// 				$info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error Enviando Servicio Television PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 				$app['datalogger']->RecordLogger($info);

	// 				return $app->json(array(
	// 			        'status'    => 	false,
	// 			        'title'		=>	'Error',
	// 			        'content'   => 	'Error tratando de enviar el servicio de Television PR. Si el error persiste contactar al administrador de sistema.'
	// 			    ));
	// 			}
	// 		}else{

	// 			$info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error Enviando Servicio Television PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 			$app['datalogger']->RecordLogger($info);

	// 			return $app->json(array(
	// 		        'status'    => 	false,
	// 		        'title'		=>	'Error',
	// 		        'content'   => 	'Error tratando de enviar el servicio de Television PR. Si el error persiste contactar al administrador de sistema.'
	// 		    ));
	// 		}

	// 	}else{

	// 		$info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error Enviando Servicio Television PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 		$app['datalogger']->RecordLogger($info);

	// 		return $app->json(array(
	// 	        'status'    => 	false,
	// 	        'title'		=>	'Error',
	// 	        'content'   => 	'Error tratando de enviar el servicio de Television PR. Si el error persiste contactar al administrador de sistema.'
	// 	    ));

	// 	}
		
	// }else{

	// 	$info2 = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error - Lead Television PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//     $app['datalogger']->RecordLogger($info2);

	// 	return $app->json(array(
	//         'status'    => 	false,
	//         'title'		=>	'Error',
	//         'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio de Television PR registrado en Sistema, si es un error notifique al administrador del sistema.'
	//     ));
	
	// }

	$date = date('Y-m-d H:i:s', time());

	$params     =   [];

	$info 		=	"";

	parse_str($request->get('value'), $params);

	$iClient 	=	Leads::GetLeadByClient($params['pr_tv_client']);

	$client 	=	$params['pr_tv_client'];

	$serv 		=	ServiceTvPrTmp::ServiceTv($client);
	
	if($iClient['country'] == "1")
	{
        if(strlen($iClient['nameClient']) > 2)
        {
        	if($iClient['birthday'] <> "")
        	{
        		if(strlen($iClient['phone_main']) >= 10)
        		{
	        		if(strlen($iClient['add_main']) > 5)
	        		{
	        			if(strlen($iClient['add_postal']) > 5)
	        			{
							if($iClient['email_main'] <> "")
		        			{
		        				if($serv['origen'] <> "1")
		        				{

									if($serv['status'] <> false)
									{
										$ticket 	= 	ServiceTvPr::GetTicket($client);
										
										$saveServ 	=	ServiceTvPr::SaveService($ticket, $serv, $date);

										if($saveServ == true)
										{

									        $info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Servicio de Television PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Enviado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

											$app['datalogger']->RecordLogger($info);

											if($params['pr_tv_support'] == 0)
											{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'),  'client' => $params['pr_tv_client'], 'created' => $serv['created']]; }
											else
											{ $user 	=	['owen'	=>	$params['pr_tv_ope_list'],  'assist' => $app['session']->get('id'), 'client' => $params['pr_tv_client'], 'created' => $serv['created']]; }

											$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

											$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

											if($saveApro == true)
											{
												$query 	=	'SELECT 
													t1.client_id,
													t2.name,
													t2.birthday,
													t2.phone_main,
													t2.phone_alt,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
													t2.coor_lati,
													t2.coor_long,
													(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
													t2.add_main,
													t2.add_postal,
													t2.email_main,
													t2.email_other,
													t2.additional,
													t1.tv,
													t1.decoder_id AS decoder,
													(SELECT name FROM data_package WHERE id = t1.package_id) AS package,
													t1.dvr_id AS dvrs,
													(SELECT name FROM data_provider WHERE id = t1.provider_id) AS provider,
													(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
													(IF(t1.ch_hd = "on", "SI", "NO")) AS hd,
													(IF(t1.ch_dvr = "on", "SI", "NO")) AS dvr,
													(IF(t1.ch_hbo = "on", "SI", "NO")) AS hbo,
													(IF(t1.ch_cinemax = "on", "SI", "NO")) AS cinemax,
													(IF(t1.ch_starz = "on", "SI", "NO")) AS starz,
													(IF(t1.ch_showtime = "on", "SI", "NO")) AS showtime,
													(IF(t1.gif_ent = "on", "SI", "NO")) AS gift_ent,
													(IF(t1.gif_choice = "on", "SI", "NO")) AS gift_cho,
													(IF(t1.gif_xtra = "on", "SI", "NO")) AS gift_xtra,
													(IF(t1.advertising_id = "1", "SI", "NO")) AS adv,
													(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
													(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
													(IF(t1.ref_id = "1","SI","NO")) AS ref,
													(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
													(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
													(IF(t1.sup_id = "1","SI","NO")) AS sup,
													(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
													(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
													(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
													(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
													(SELECT email FROM users WHERE id = prop_id) AS prop_email,
													(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
													(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
													(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
													(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
													(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
													t1.created_at AS created
													FROM cp_service_pr_tv AS t1 
													INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
													INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
													AND t1.ticket =  "'.$ticket.'"';

												$iData	=	DBSmart::DBQuery($query);

												if($iData <> false)
												{
													$pData 	=	[
														'username'		=>	$iData['prop'],
														'departament'	=>	$iData['prop_depart'],
														'subject'		=>	'TELEVISION - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/TV-icon.png',
														'service'		=> 	'TELEVISION',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];

													$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
													$refHTML	=	Leads::GetReferredHtml($iData);
													$supHTML	=	Leads::GetSupportHtml($iData);

													$saleHTML	=	Leads::GetLeadSaleHtml($iData);
													$subHTML	=	Leads::GetSubTvHtml($iData);

													$mail   	=   new SendMail();
													$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

													$pData 	=	[
														'username'		=>	'DATA ENTRY',
														'departament'	=>	'DATA ENTRY',
														'subject'		=>	'TELEVISION - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/TV-icon.png',
														'service'		=> 	'TELEVISION',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];
													$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];

													$mail   	=   new SendMail();
											 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

											        $query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
											        $emails =   DBSmart::DBQueryAll($query);

											        if($emails <> false)
											        {
											        	foreach ($emails as $e => $mail) 
											        	{
											        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
											        		$pData 	=	[
																'username'		=>	strtoupper($mail['name']),
																'departament'	=>	$mail['dept'],
																'subject'		=>	'TELEVISION - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
																'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/TV-icon.png',
																'service'		=> 	'TELEVISION',
																'ticket'		=>	$ticket,
																'created'		=>	$iData['created']
															];
															$mail   	=   new SendMail();
															$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
											        	}
											        }

											        $html = "";
											        $html =	'<div class="row">
																<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
																	<div class="panel panel-darken">
																		<div class="panel-heading" style="text-align: center;">
																			<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-desktop" style="font-size: 50px;"></i>
																			<h3 class="panel-title" style="margin-top: 10px;">TELEVISION PR</h3>
																		</div>
																		<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
																	</div>
																</div>
															</div>';

												    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
												    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

												    $iTicket 	=	Drives::RequestTVInfoNew($ticket);
													$iResult 	=	Drives::InsertDBTVNew($iTicket);

													return $app->json(array(
											            'status'    => 	true,
											            'title'		=>	'Success',
											            'content'   => 	'Datos Enviados Correctamente',
											            'html'		=> 	$html
											        ));

												}else{

													$info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error Enviando Servicio Television PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

													$app['datalogger']->RecordLogger($info);

													return $app->json(array(
												        'status'    => 	false,
												        'title'		=>	'Error',
												        'content'   => 	'Error tratando de enviar el servicio de Television PR. Si el error persiste contactar al administrador de sistema.'
												    ));
												}
											}else{

												$info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error Enviando Servicio Television PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

												$app['datalogger']->RecordLogger($info);

												return $app->json(array(
											        'status'    => 	false,
											        'title'		=>	'Error',
											        'content'   => 	'Error tratando de enviar el servicio de Television PR. Si el error persiste contactar al administrador de sistema.'
											    ));
											}

										}else{

											$info = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error Enviando Servicio Television PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

											$app['datalogger']->RecordLogger($info);

											return $app->json(array(
										        'status'    => 	false,
										        'title'		=>	'Error',
										        'content'   => 	'Error tratando de enviar el servicio de Television PR. Si el error persiste contactar al administrador de sistema.'
										    ));

										}
										
									}else{

										$info2 = array('client' => $client, 'channel' => 'Lead Television PR', 'message' => 'Error - Lead Television PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

									    $app['datalogger']->RecordLogger($info2);

										return $app->json(array(
									        'status'    => 	false,
									        'title'		=>	'Error',
									        'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio de Television PR registrado en Sistema, si es un error notifique al administrador del sistema.'
									    ));
									
									}

		        				}else{

			        				return $app->json(array(
							            'status'    => 	false,
							            'title'		=>	'Error',
							            'content'   => 	'Deber contener un origen de la venta valido.'
						        	));
		        				}
		        			}else{
		        				return $app->json(array(
						            'status'    => 	false,
						            'title'		=>	'Error',
						            'content'   => 	'Debe contener una direccion de email valido.'
					        	));
		        			}
	        			}else{
	        				return $app->json(array(
					            'status'    => 	false,
					            'title'		=>	'Error',
					            'content'   => 	'La direccion postal debe contener al menos cinco caracteres.'
				        	));
	        			}	        		
	        		}else{
	        			return $app->json(array(
				            'status'    => 	false,
				            'title'		=>	'Error',
				            'content'   => 	'La direccion principal debe contener al menos cinco caracteres.'
				        ));
	        		}
        		}else{

        			return $app->json(array(
			            'status'    => 	false,
			            'title'		=>	'Error',
			            'content'   => 	'Numero telefonico principal debe Contener 10 Digitos.'
			        ));
        		}
        	}else{

				return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'Debe ingresar una fecha de nacimiento valida.'
		        ));
        	}
        }else{
			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'El nombre debe tener mas de dos caracteres.'
	        ));
        }

		return $app->json(array(
	        'status'    => 	success,
	        'title'		=>	'success',
	        'content'   => 	'Si Funciono....'
	    ));

	}else{
		return $app->json(array(
	        'status'    => 	false,
	        'title'		=>	'Error',
	        'content'   => 	'Imposible enviar la solicutd, no corresponde al pais seleccionado para el cliente.'
	    ));
	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Service Security /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveSecurity(Application $app, Request $request)
	{

		$params     =   [];

		parse_str($request->get('value'), $params);
	        
	    $info 	=	[
	    	'client'		=>	(isset($params['pr_sec_client']))		? $params['pr_sec_client']		: $params['pr_sec_client'],
	    	'previosly'		=>	(isset($params['pr_sec_previously']))	? $params['pr_sec_previously']	: "0",
	    	'equipment'		=>	(isset($params['pr_sec_equipment'])) 	? $params['pr_sec_equipment'] 	: "",
	    	'cameras'		=>	(isset($params['pr_sec_cameras'])) 		? $params['pr_sec_cameras'] 	: "1",
	    	'comp_equip'	=>	(isset($params['pr_sec_company'])) 		? $params['pr_sec_company']		: "",
	    	'dvr'			=>	(isset($params['pr_sec_dvr'])) 			? $params['pr_sec_dvr'] 		: "1",
	    	'add_equip'		=>	(isset($params['pr_sec_add'])) 			? $params['pr_sec_add'] 		: "",
	    	'pass_alarm'	=>	(isset($params['pr_sec_password'])) 	? $params['pr_sec_password'] 	: "",
	    	'payment'		=>	(isset($params['pr_sec_payment'])) 		? $params['pr_sec_payment']		: "1",
	    	'ref_id'		=>	(isset($params['pr_sec_referred'])) 	? $params['pr_sec_referred']	: "0",
	    	'ref_list'		=>	(isset($params['pr_sec_ref_list'])) 	? $params['pr_sec_ref_list']	: "0",
	    	'sup'			=>	(isset($params['pr_sec_sup'])) 			? $params['pr_sec_sup']			: "0",
	    	'sup_list'		=>	(isset($params['pr_sec_ope_list'])) 	? $params['pr_sec_ope_list']	: "1",
	    	'cont1'			=>	(isset($params['pr_sec_cont_1'])) 		? $params['pr_sec_cont_1']		: "",
	    	'cont1desc'		=>	(isset($params['pr_sec_cont_1_desc'])) 	? $params['pr_sec_cont_1_desc']	: "",
	    	'cont2'			=>	(isset($params['pr_sec_cont_2'])) 		? $params['pr_sec_cont_2']		: "",
	    	'cont2desc'		=>	(isset($params['pr_sec_cont_2_desc'])) 	? $params['pr_sec_cont_2_desc']	: "",
	    	'cont3'			=>	(isset($params['pr_sec_cont_3'])) 		? $params['pr_sec_cont_3']		: "",
	    	'cont3desc'		=>	(isset($params['pr_sec_cont_3_desc'])) 	? $params['pr_sec_cont_3_desc']	: "",
	    	'service'		=>	(isset($params['pr_sec_status'])) 		? $params['pr_sec_status']		: "1",
	    	'order_type'	=>	(isset($params['pr_sec_type_order'])) 	? $params['pr_sec_type_order']	: "1",
	    	'origen'		=>	(isset($params['pr_sec_origen'])) 		? $params['pr_sec_origen']		: "1",
	    	'operator'		=>	$app['session']->get('id')	
	    ];

		$save 	=	ServiceSecPrTmp::SaveService($info);

		if($save)
		{

	        $info = array('client' => $info['client'], 'channel' => 'Lead Service Sec Update', 'message' => 'Update Service Sec PR- '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	true,
	            'title'		=>	'Success',
	            'content'   => 	'Data Stored Correctly'
	        ));

	    }else{
	        $info = array('client' => $info['client'], 'channel' => 'Lead Service Sec Update', 'message' => 'Error Update Service Sec PR - '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Error while trying to update the tv service. If the error persists, contact the system Administrator.'
	        ));
	    }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewSecurity(Application $app, Request $request)
	{
		$servSec 	= 	ServiceSecPrTmp::ServiceSec($request->get('id'));
		$servInf 	=	Approvals::GetInfoService($request->get('id'), '3');
		$servCoo 	=	Coordination::GetInfoService($servInf['ticket']);

		if($servSec)
		{
			return $app->json([
				'status' 	=>	true, 
				'sec' 		=> 	$servSec,
				'inf'		=>	$servInf,
				'coo'		=>	$servCoo
			]);

		}else{
			return $app->json(array('status' => false));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewServiceSecurity(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);

		$client =	$params['pr_sec_client'];

		return $app->json(array('status' => false));

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

public static function SubmitSecurity(Application $app, Request $request)
{
	// $date = date('Y-m-d H:i:s', time());

	// $params     =   [];

	// $info 		=	"";

	// parse_str($request->get('value'), $params);

	// $client 	=	$params['pr_sec_client'];

	// $ticket 	= 	ServiceSecPr::GetTicket($client);

	// $serv 		=	ServiceSecPrTmp::ServiceSec($client);

	// if($serv['status'] <> false)
	// {
	// 	$saveServ 	=	ServiceSecPr::SaveService($ticket, $serv);

	// 	if($saveServ == true)
	// 	{
	//         $info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Servicio de Seguridad PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Enviado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//         $app['datalogger']->RecordLogger($info);

	// 		if($params['pr_sec_sup'] == 0)
	// 		{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
	// 		else
	// 		{ $user 	=	['owen'	=>	$params['pr_sec_ope_list'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }

	// 		$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

	// 		$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

	// 		if($saveApro == true)
	// 		{
	// 			$query 	=	'SELECT t1.client_id,
	// 				t2.name,
	// 				t2.birthday,
	// 				t2.phone_main,
	// 				t2.phone_alt,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
	// 				t2.coor_lati,
	// 				t2.coor_long,
	// 				(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
	// 				t2.add_main,
	// 				t2.add_postal,
	// 				t2.email_main,
	// 				t2.email_other,
	// 				t2.additional,
	// 				(IF(t1.previously_id = "1","SI","NO")) AS previosly,
	// 				t1.comp_equipment AS companay,
	// 				(SELECT name FROM data_camera WHERE id = t1.cameras_id) AS cameras,
	// 				(SELECT name FROM data_dvr WHERE id = t1.dvr_id) AS dvrs,
	// 				t1.password_alarm AS pass_alarm,
	// 				(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
	// 				t1.equipment,
	// 				t1.add_equipment AS add_equip,
	// 				t1.contact_1 AS cont1,
	// 				t1.contact_1_desc AS cont1des,
	// 				t1.contact_2 AS cont2,
	// 				t1.contact_2_desc AS cont2des,
	// 				t1.contact_3 AS cont3,
	// 				t1.contact_3_desc AS cont3des,
	// 				(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
	// 				(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
	// 				(IF(t1.ref_id = "1","SI","NO")) AS ref,
	// 				(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
	// 				(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
	// 				(IF(t1.sup_id = "1","SI","NO")) AS sup,
	// 				 (SELECT username FROM users WHERE id = t3.owen_id) AS prop,
	// 				(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
	// 				(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
	// 				(SELECT email FROM users WHERE id = prop_id) AS prop_email,
	// 				(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
	// 				(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
	// 				(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
	// 				(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
	// 				t1.created_at AS created
	// 				FROM cp_service_pr_sec AS t1 
	// 				INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
	// 				INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
	// 				AND t1.ticket = "'.$ticket.'"';

	// 			$iData	=	DBSmart::DBQuery($query);

	// 			if($iData <> false)
	// 			{
	// 				$pData 	=	[
	// 					'username'		=>	$iData['prop'],
	// 					'departament'	=>	$iData['prop_depart'],
	// 					'subject'		=>	'SEGURIDAD - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-security-icon.png',
	// 					'service'		=> 	'SEGURIDAD',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];

	// 				$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
	// 				$refHTML	=	Leads::GetReferredHtml($iData);
	// 				$supHTML	=	Leads::GetSupportHtml($iData);

	// 				$saleHTML	=	Leads::GetLeadSaleHtml($iData);
	// 				$subHTML	=	Leads::GetSubSecHtml($iData);

	// 				$mail   	=   new SendMail();
	// 				$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 				$pData 	=	[
	// 					'username'		=>	'DATA ENTRY',
	// 					'departament'	=>	'DATA ENTRY',
	// 					'subject'		=>	'SEGURIDAD - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-security-icon.png',
	// 					'service'		=> 	'SEGURIDAD',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];
	// 				$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
	// 				$mail   	=   new SendMail();
	// 		 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 				$query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
	// 		        $emails =   DBSmart::DBQueryAll($query);

	// 		        if($emails <> false)
	// 		        {
	// 		        	foreach ($emails as $e => $mail) 
	// 		        	{
	// 		        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
	// 		        		$pData 	=	[
	// 							'username'		=>	strtoupper($mail['name']),
	// 							'departament'	=>	$mail['dept'],
	// 							'subject'		=>	'SEGURIDAD - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 							'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-security-icon.png',
	// 							'service'		=> 	'SEGURIDAD',
	// 							'ticket'		=>	$ticket,
	// 							'created'		=>	$iData['created']
	// 						];
	// 						$mail   	=   new SendMail();
	// 						$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
	// 		        	}
	// 		        }

	// 		        $html = "";
	// 		        $html =	'<div class="row">
	// 							<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
	// 								<div class="panel panel-darken">
	// 									<div class="panel-heading" style="text-align: center;">
	// 										<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-lock" style="font-size: 50px;"></i>
	// 										<h3 class="panel-title" style="margin-top: 10px;">SEGURIDAD PR</h3>
	// 									</div>
	// 									<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
	// 								</div>
	// 							</div>
	// 						</div>';
				    
	// 			    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
	// 			    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

	// 			    $iTicket 	=	Drives::RequestSECInfoNew($ticket);
	// 				$iResult 	=	Drives::InsertDBSECNew($iTicket);

	// 				return $app->json(array(
	// 		            'status'    => 	true,
	// 		            'title'		=>	'Success',
	// 		            'content'   => 	'Datos Enviados Correctamente',
	// 		            'html'		=> 	$html
	// 		        ));

	// 			}else{

	// 				$info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error Enviando Servicio Seguridad PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 				$app['datalogger']->RecordLogger($info);

	// 				return $app->json(array(
	// 			        'status'    => 	false,
	// 			        'title'		=>	'Error',
	// 			        'content'   => 	'Error tratando de enviar el servicio de Seguridad PR. Si el error persiste contactar al administrador de sistema.'
	// 			    ));
	// 			}
	// 		}else{

	// 			$info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error Enviando Servicio Seguridad PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 			$app['datalogger']->RecordLogger($info);

	// 			return $app->json(array(
	// 		        'status'    => 	false,
	// 		        'title'		=>	'Error',
	// 		        'content'   => 	'Error tratando de enviar el servicio de Seguridad PR. Si el error persiste contactar al administrador de sistema.'
	// 		    ));
	// 		}

	// 	}else{

	// 		$info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error Enviando Servicio Seguridad PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 		$app['datalogger']->RecordLogger($info);

	// 		return $app->json(array(
	// 	        'status'    => 	false,
	// 	        'title'		=>	'Error',
	// 	        'content'   => 	'Error tratando de enviar el servicio de Seguridad PR. Si el error persiste contactar al administrador de sistema.'
	// 	    ));
		
	// 	}

	// }else{
	// 	$info2 = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error - Lead Seguridad PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//     $app['datalogger']->RecordLogger($info2);

	// 	return $app->json(array(
	//         'status'    => 	false,
	//         'title'		=>	'Error',
	//         'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio de Seguridad PR registrado en Sistema, si es un error notifique al administrador del sistema.'
	//     ));
	
	// }


	$date = date('Y-m-d H:i:s', time());

	$params     =   [];

	$info 		=	"";

	parse_str($request->get('value'), $params);

	$client 	=	$params['pr_sec_client'];

	$iClient 	=	Leads::GetLeadByClient($client);

	$serv 		=	ServiceSecPrTmp::ServiceSec($client);

	if($iClient['country'] == "1")
	{
        if(strlen($iClient['nameClient']) > 2)
        {
        	if($iClient['birthday'] <> "")
        	{
        		if(strlen($iClient['phone_main']) >= 10)
        		{
	        		if(strlen($iClient['add_main']) > 5)
	        		{
	        			if(strlen($iClient['add_postal']) > 5)
	        			{
							if($iClient['email_main'] <> "")
		        			{
		        				if($serv['origen'] <> "1")
		        				{
									if($serv['status'] <> false)
									{
										$ticket 	= 	ServiceSecPr::GetTicket($client);

										$saveServ 	=	ServiceSecPr::SaveService($ticket, $serv);

										if($saveServ == true)
										{
									        $info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Servicio de Seguridad PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Enviado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

									        $app['datalogger']->RecordLogger($info);

											if($params['pr_sec_sup'] == 0)
											{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
											else
											{ $user 	=	['owen'	=>	$params['pr_sec_ope_list'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }

											$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

											$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

											if($saveApro == true)
											{
												$query 	=	'SELECT t1.client_id,
													t2.name,
													t2.birthday,
													t2.phone_main,
													t2.phone_alt,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
													t2.coor_lati,
													t2.coor_long,
													(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
													t2.add_main,
													t2.add_postal,
													t2.email_main,
													t2.email_other,
													t2.additional,
													(IF(t1.previously_id = "1","SI","NO")) AS previosly,
													t1.comp_equipment AS companay,
													(SELECT name FROM data_camera WHERE id = t1.cameras_id) AS cameras,
													(SELECT name FROM data_dvr WHERE id = t1.dvr_id) AS dvrs,
													t1.password_alarm AS pass_alarm,
													(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
													t1.equipment,
													t1.add_equipment AS add_equip,
													t1.contact_1 AS cont1,
													t1.contact_1_desc AS cont1des,
													t1.contact_2 AS cont2,
													t1.contact_2_desc AS cont2des,
													t1.contact_3 AS cont3,
													t1.contact_3_desc AS cont3des,
													(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
													(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
													(IF(t1.ref_id = "1","SI","NO")) AS ref,
													(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
													(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
													(IF(t1.sup_id = "1","SI","NO")) AS sup,
													 (SELECT username FROM users WHERE id = t3.owen_id) AS prop,
													(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
													(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
													(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
													(SELECT email FROM users WHERE id = prop_id) AS prop_email,
													(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
													(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
													(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
													(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
													(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
													t1.created_at AS created
													FROM cp_service_pr_sec AS t1 
													INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
													INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
													AND t1.ticket = "'.$ticket.'"';

												$iData	=	DBSmart::DBQuery($query);

												if($iData <> false)
												{
													$pData 	=	[
														'username'		=>	$iData['prop'],
														'departament'	=>	$iData['prop_depart'],
														'subject'		=>	'SEGURIDAD - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-security-icon.png',
														'service'		=> 	'SEGURIDAD',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];

													$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
													$refHTML	=	Leads::GetReferredHtml($iData);
													$supHTML	=	Leads::GetSupportHtml($iData);

													$saleHTML	=	Leads::GetLeadSaleHtml($iData);
													$subHTML	=	Leads::GetSubSecHtml($iData);

													$mail   	=   new SendMail();
													$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

													$pData 	=	[
														'username'		=>	'DATA ENTRY',
														'departament'	=>	'DATA ENTRY',
														'subject'		=>	'SEGURIDAD - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-security-icon.png',
														'service'		=> 	'SEGURIDAD',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];
													$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
													$mail   	=   new SendMail();
											 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

													$query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
											        $emails =   DBSmart::DBQueryAll($query);

											        if($emails <> false)
											        {
											        	foreach ($emails as $e => $mail) 
											        	{
											        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
											        		$pData 	=	[
																'username'		=>	strtoupper($mail['name']),
																'departament'	=>	$mail['dept'],
																'subject'		=>	'SEGURIDAD - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
																'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-security-icon.png',
																'service'		=> 	'SEGURIDAD',
																'ticket'		=>	$ticket,
																'created'		=>	$iData['created']
															];
															$mail   	=   new SendMail();
															$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
											        	}
											        }

											        $html = "";
											        $html =	'<div class="row">
																<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
																	<div class="panel panel-darken">
																		<div class="panel-heading" style="text-align: center;">
																			<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-lock" style="font-size: 50px;"></i>
																			<h3 class="panel-title" style="margin-top: 10px;">SEGURIDAD PR</h3>
																		</div>
																		<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
																	</div>
																</div>
															</div>';
												    
												    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
												    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

												    $iTicket 	=	Drives::RequestSECInfoNew($ticket);
													$iResult 	=	Drives::InsertDBSECNew($iTicket);

													return $app->json(array(
											            'status'    => 	true,
											            'title'		=>	'Success',
											            'content'   => 	'Datos Enviados Correctamente',
											            'html'		=> 	$html
											        ));

												}else{

													$info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error Enviando Servicio Seguridad PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

													$app['datalogger']->RecordLogger($info);

													return $app->json(array(
												        'status'    => 	false,
												        'title'		=>	'Error',
												        'content'   => 	'Error tratando de enviar el servicio de Seguridad PR. Si el error persiste contactar al administrador de sistema.'
												    ));
												}
											}else{

												$info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error Enviando Servicio Seguridad PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

												$app['datalogger']->RecordLogger($info);

												return $app->json(array(
											        'status'    => 	false,
											        'title'		=>	'Error',
											        'content'   => 	'Error tratando de enviar el servicio de Seguridad PR. Si el error persiste contactar al administrador de sistema.'
											    ));
											}

										}else{

											$info = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error Enviando Servicio Seguridad PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

											$app['datalogger']->RecordLogger($info);

											return $app->json(array(
										        'status'    => 	false,
										        'title'		=>	'Error',
										        'content'   => 	'Error tratando de enviar el servicio de Seguridad PR. Si el error persiste contactar al administrador de sistema.'
										    ));
										
										}

									}else{
										$info2 = array('client' => $client, 'channel' => 'Lead Seguridad PR', 'message' => 'Error - Lead Seguridad PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

									    $app['datalogger']->RecordLogger($info2);

										return $app->json(array(
									        'status'    => 	false,
									        'title'		=>	'Error',
									        'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio de Seguridad PR registrado en Sistema, si es un error notifique al administrador del sistema.'
									    ));
									
									}

		        				}else{

			        				return $app->json(array(
							            'status'    => 	false,
							            'title'		=>	'Error',
							            'content'   => 	'Deber contener un origen de la venta valido.'
						        	));
		        				}
		        			}else{
		        				return $app->json(array(
						            'status'    => 	false,
						            'title'		=>	'Error',
						            'content'   => 	'Debe contener una direccion de email valido.'
					        	));
		        			}
	        			}else{
	        				return $app->json(array(
					            'status'    => 	false,
					            'title'		=>	'Error',
					            'content'   => 	'La direccion postal debe contener al menos cinco caracteres.'
				        	));
	        			}	        		
	        		}else{
	        			return $app->json(array(
				            'status'    => 	false,
				            'title'		=>	'Error',
				            'content'   => 	'La direccion principal debe contener al menos cinco caracteres.'
				        ));
	        		}
        		}else{

        			return $app->json(array(
			            'status'    => 	false,
			            'title'		=>	'Error',
			            'content'   => 	'Numero telefonico principal debe Contener 10 Digitos.'
			        ));
        		}
        	}else{

				return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'Debe ingresar una fecha de nacimiento valida.'
		        ));
        	}
        }else{
			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'El nombre debe tener mas de dos caracteres.'
	        ));
        }

		return $app->json(array(
	        'status'    => 	success,
	        'title'		=>	'success',
	        'content'   => 	'Si Funciono....'
	    ));

	}else{
		return $app->json(array(
	        'status'    => 	false,
	        'title'		=>	'Error',
	        'content'   => 	'Imposible enviar la solicutd, no corresponde al pais seleccionado para el cliente.'
	    ));
	
	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Service Internet /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveInternet(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);
	        
	    $info 	=	[
	    	'client'		=>	(isset($params['pr_int_client']))		? $params['pr_int_client']			: "0",
	    	'int_res'		=>	(isset($params['pr_int_res']))			? $params['pr_int_res']				: "1",	
	    	'pho_res'		=>	(isset($params['pr_int_res_pho'])) 		? $params['pr_int_res_pho'] 		: "1",
	    	'int_com'		=>	(isset($params['pr_int_trade'])) 		? $params['pr_int_trade'] 			: "6",
	    	'pho_com'		=>	(isset($params['pr_int_trade_pho'])) 	? $params['pr_int_trade_pho']		: "5",
	    	'ref'			=>	(isset($params['pr_int_referred'])) 	? $params['pr_int_referred'] 		: "0",
	    	'ref_list'		=>	(isset($params['pr_int_ref_list']))		? $params['pr_int_ref_list']		: "0",
	    	'sup'			=>	(isset($params['pr_int_support'])) 		? $params['pr_int_support']			: "0",
	    	'sup_list'		=>	(isset($params['pr_int_ope'])) 			? $params['pr_int_ope']				: "1",
	    	'payment'		=>	(isset($params['pr_int_payment'])) 		? $params['pr_int_payment']			: "1",
	    	'service'		=>	(isset($params['pr_int_status'])) 		? $params['pr_int_status']			: "1",    	
	    	'order_type'	=>	(isset($params['pr_int_type_order'])) 	? $params['pr_int_type_order']		: "1",
	    	'origen'		=>	(isset($params['pr_int_origen'])) 		? $params['pr_int_origen']			: "1",
	    	'operator'		=>	$app['session']->get('id')	
	    ];

		$save 	=	ServiceIntPrTmp::SaveService($info);

		if($save == true)
		{

	        $info = array('client' => $info['client'], 'channel' => 'Lead Service Int Update', 'message' => 'Update Service Int PR- '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	true,
	            'title'		=>	'Success',
	            'content'   => 	'Data Stored Correctly'
	        ));

	    }else{
	        $info = array('client' => $info['client'], 'channel' => 'Lead Service Int Update', 'message' => 'Error Update Service Int PR - '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Error while trying to update the tv service. If the error persists, contact the system Administrator.'
	        ));
	    }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewInternet(Application $app, Request $request)
	{
		$servInt 	= 	ServiceIntPrTmp::ServiceInt($request->get('id'));
		$servInf 	=	Approvals::GetInfoService($request->get('id'), '4');
		$servCoo 	=	Coordination::GetInfoService($servInf['ticket']);

		if($servInt)
		{
			return $app->json([
				'status' 	=>	true, 
				'int' 		=> 	$servInt,
				'inf'		=>	$servInf,
				'coo'		=> 	$servCoo
			]);

		}else{
			return $app->json(array('status' => false));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewServiceInternet(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);

		$client =	$params['pr_int_client'];

		return $app->json(array('status' => false));

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

public static function SubmitInternet(Application $app, Request $request)
{

	// $date = date('Y-m-d H:i:s', time());

	// $params     =   [];

	// $info 		=	"";

	// parse_str($request->get('value'), $params);

	// $client 	=	$params['pr_int_client'];

	// $ticket 	= 	ServiceIntPr::GetTicket($client);

	// $serv 		=	ServiceIntPrTmp::ServiceInt($client);

	// if($serv['status'] <> false)
	// {
	// 	$saveServ 	=	ServiceIntPr::SaveService($ticket, $serv, $date);

	// 	if($saveServ)
	// 	{

	// 		$info 		= 	array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

 //    		$app['datalogger']->RecordLogger($info);

	// 		if($params['pr_int_support'] == 0)
	// 		{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
	// 		else
	// 		{ $user 	=	['owen'	=>	$params['pr_int_ope'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
			
	// 		$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

	// 		$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

	// 		if($saveApro == true)
	// 		{
	// 			$query	=	'SELECT 
	// 				t1.client_id,
	// 				t2.name,
	// 				t2.birthday,
	// 				t2.phone_main,
	// 				t2.phone_alt,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
	// 				t2.coor_lati,
	// 				t2.coor_long,
	// 				(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
	// 				t2.add_main,
	// 				t2.add_postal,
	// 				t2.email_main,
	// 				t2.email_other,
	// 				t2.additional,
	// 				(SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res,
	// 				(SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res,
	// 				(SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com,
	// 				(SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com,
	// 				(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
	// 				(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
	// 				(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
	// 				(IF(t1.ref_id = "1","SI","NO")) AS ref,
	// 				(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
	// 				(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
	// 				(IF(t1.sup_id = "1","SI","NO")) AS sup,
	// 				(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
	// 				(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
	// 				(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
	// 				(SELECT email FROM users WHERE id = prop_id) AS prop_email,
	// 				(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
	// 				(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
	// 				(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
	// 				(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
	// 				t1.created_at AS created
	// 				FROM cp_service_pr_int AS t1 
	// 				INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
	// 				INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
	// 				AND t1.ticket = "'.$ticket.'"';

	// 			$iData	=	DBSmart::DBQuery($query);

	// 			if($iData <> false)
	// 			{
	// 				$pData 	=	[
	// 					'username'		=>	$iData['prop'],
	// 					'departament'	=>	$iData['prop_depart'],
	// 					'subject'		=>	'INTERNET - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 					'service'		=> 	'INTERNET',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];

	// 				$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
	// 				$refHTML	=	Leads::GetReferredHtml($iData);
	// 				$supHTML	=	Leads::GetSupportHtml($iData);

	// 				$saleHTML	=	Leads::GetLeadSaleHtml($iData);
	// 				$subHTML	=	Leads::GetSubIntHtml($iData);

	// 				$mail   	=   new SendMail();
	// 				$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 				$pData 	=	[
	// 					'username'		=>	'DATA ENTRY',
	// 					'departament'	=>	'DATA ENTRY',
	// 					'subject'		=>	'INTERNET - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 					'service'		=> 	'INTERNET',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];
	// 				$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
	// 				$mail   	=   new SendMail();
	// 		 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 				$query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
	// 		        $emails =   DBSmart::DBQueryAll($query);

	// 		        if($emails <> false)
	// 		        {
	// 		        	foreach ($emails as $e => $mail) 
	// 		        	{
	// 		        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
	// 		        		$pData 	=	[
	// 							'username'		=>	strtoupper($mail['name']),
	// 							'departament'	=>	$mail['dept'],
	// 							'subject'		=>	'INTERNET - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 							'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 							'service'		=> 	'INTERNET',
	// 							'ticket'		=>	$ticket,
	// 							'created'		=>	$iData['created']
	// 						];
	// 						$mail   	=   new SendMail();
	// 						$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
	// 		        	}
	// 		        }

	// 		        $html = "";
	// 		        $html =	'<div class="row">
	// 							<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
	// 		                		<div class="panel panel-darken">
	// 		                    		<div class="panel-heading" style="text-align: center;">
	// 		                        		<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-globe" style="font-size: 50px;"></i>
	// 		                        		<h3 class="panel-title" style="margin-top: 10px;">INTERNET PR</h3>
	// 		                    		</div>
	// 		                    	<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
	// 		                	</div>
	// 						</div>';

	// 			    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
	// 			    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

	// 			    $iTicket 	=	Drives::RequestINTInfoNew($ticket);
	// 				$iResult 	=	Drives::InsertDBINTNew($iTicket);

	// 				return $app->json(array(
	// 		            'status'    => 	true,
	// 		            'title'		=>	'Success',
	// 		            'content'   => 	'Datos Enviados Correctamente',
	// 		            'html'		=> 	$html
	// 		        ));
	// 			}else{

	// 				$info = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error en envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 		        $app['datalogger']->RecordLogger($info);

	// 				return $app->json(array(
	// 		            'status'    => 	false,
	// 		            'title'		=>	'Error',
	// 		            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
	// 		        ));
	// 			}
	// 		}else{

	// 			$info = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error en envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 	        $app['datalogger']->RecordLogger($info);

	// 			return $app->json(array(
	// 	            'status'    => 	false,
	// 	            'title'		=>	'Error',
	// 	            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
	// 	        ));
	// 		}

	// 	}else{

	// 		$info = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error en envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//         $app['datalogger']->RecordLogger($info);

	// 		return $app->json(array(
	//             'status'    => 	false,
	//             'title'		=>	'Error',
	//             'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
	//         ));

	// 	}

	// }else{
		
	// 	$info2 = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error - Lead Internet PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//     $app['datalogger']->RecordLogger($info2);

	// 	return $app->json(array(
	//         'status'    => 	false,
	//         'title'		=>	'Error',
	//         'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio Internet PR registrado en Sistema, si es un error notifique al administrador del sistema.'
	//     ));
	
	// }

	$date = date('Y-m-d H:i:s', time());

	$params     =   [];

	$info 		=	"";

	parse_str($request->get('value'), $params);

	$iClient 	=	Leads::GetLeadByClient($params['pr_int_client']);

	$client 	=	$params['pr_int_client'];

	$serv 		=	ServiceIntPrTmp::ServiceInt($client);

	if($iClient['country'] == "1")
	{
        if(strlen($iClient['nameClient']) > 2)
        {
        	if($iClient['birthday'] <> "")
        	{
        		if(strlen($iClient['phone_main']) >= 10)
        		{
	        		if(strlen($iClient['add_main']) > 5)
	        		{
	        			if(strlen($iClient['add_postal']) > 5)
	        			{
							if($iClient['email_main'] <> "")
		        			{
		        				if($serv['origen'] <> "1")
		        				{
									if($serv['status'] <> false)
									{
										$ticket 	= 	ServiceIntPr::GetTicket($client);
										
										$saveServ 	=	ServiceIntPr::SaveService($ticket, $serv, $date);

										if($saveServ)
										{

											$info 		= 	array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

								    		$app['datalogger']->RecordLogger($info);

											if($params['pr_int_support'] == 0)
											{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
											else
											{ $user 	=	['owen'	=>	$params['pr_int_ope'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
											
											$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

											$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

											if($saveApro == true)
											{
												$query	=	'SELECT 
													t1.client_id,
													t2.name,
													t2.birthday,
													t2.phone_main,
													t2.phone_alt,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
													t2.coor_lati,
													t2.coor_long,
													(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
													t2.add_main,
													t2.add_postal,
													t2.email_main,
													t2.email_other,
													t2.additional,
													(SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res,
													(SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res,
													(SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com,
													(SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com,
													(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
													(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
													(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
													(IF(t1.ref_id = "1","SI","NO")) AS ref,
													(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
													(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
													(IF(t1.sup_id = "1","SI","NO")) AS sup,
													(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
													(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
													(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
													(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
													(SELECT email FROM users WHERE id = prop_id) AS prop_email,
													(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
													(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
													(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
													(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
													(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
													t1.created_at AS created
													FROM cp_service_pr_int AS t1 
													INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
													INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
													AND t1.ticket = "'.$ticket.'"';

												$iData	=	DBSmart::DBQuery($query);

												if($iData <> false)
												{
													$pData 	=	[
														'username'		=>	$iData['prop'],
														'departament'	=>	$iData['prop_depart'],
														'subject'		=>	'INTERNET - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
														'service'		=> 	'INTERNET',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];

													$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
													$refHTML	=	Leads::GetReferredHtml($iData);
													$supHTML	=	Leads::GetSupportHtml($iData);

													$saleHTML	=	Leads::GetLeadSaleHtml($iData);
													$subHTML	=	Leads::GetSubIntHtml($iData);

													$mail   	=   new SendMail();
													$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

													$pData 	=	[
														'username'		=>	'DATA ENTRY',
														'departament'	=>	'DATA ENTRY',
														'subject'		=>	'INTERNET - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
														'service'		=> 	'INTERNET',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];
													$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
													$mail   	=   new SendMail();
											 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

													$query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
											        $emails =   DBSmart::DBQueryAll($query);

											        if($emails <> false)
											        {
											        	foreach ($emails as $e => $mail) 
											        	{
											        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
											        		$pData 	=	[
																'username'		=>	strtoupper($mail['name']),
																'departament'	=>	$mail['dept'],
																'subject'		=>	'INTERNET - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
																'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
																'service'		=> 	'INTERNET',
																'ticket'		=>	$ticket,
																'created'		=>	$iData['created']
															];
															$mail   	=   new SendMail();
															$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
											        	}
											        }

											        $html = "";
											        $html =	'<div class="row">
																<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
											                		<div class="panel panel-darken">
											                    		<div class="panel-heading" style="text-align: center;">
											                        		<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-globe" style="font-size: 50px;"></i>
											                        		<h3 class="panel-title" style="margin-top: 10px;">INTERNET PR</h3>
											                    		</div>
											                    	<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
											                	</div>
															</div>';

												    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
												    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

												    $iTicket 	=	Drives::RequestINTInfoNew($ticket);
													$iResult 	=	Drives::InsertDBINTNew($iTicket);

													return $app->json(array(
											            'status'    => 	true,
											            'title'		=>	'Success',
											            'content'   => 	'Datos Enviados Correctamente',
											            'html'		=> 	$html
											        ));
												}else{

													$info = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error en envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

											        $app['datalogger']->RecordLogger($info);

													return $app->json(array(
											            'status'    => 	false,
											            'title'		=>	'Error',
											            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
											        ));
												}
											}else{

												$info = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error en envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

										        $app['datalogger']->RecordLogger($info);

												return $app->json(array(
										            'status'    => 	false,
										            'title'		=>	'Error',
										            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
										        ));
											}

										}else{

											$info = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error en envio de Servicio Internet PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

									        $app['datalogger']->RecordLogger($info);

											return $app->json(array(
									            'status'    => 	false,
									            'title'		=>	'Error',
									            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
									        ));

										}

									}else{
										
										$info2 = array('client' => $client, 'channel' => 'Lead Internet PR', 'message' => 'Error - Lead Internet PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

									    $app['datalogger']->RecordLogger($info2);

										return $app->json(array(
									        'status'    => 	false,
									        'title'		=>	'Error',
									        'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio Internet PR registrado en Sistema, si es un error notifique al administrador del sistema.'
									    ));
									
									}

		        				}else{

			        				return $app->json(array(
							            'status'    => 	false,
							            'title'		=>	'Error',
							            'content'   => 	'Deber contener un origen de la venta valido.'
						        	));
		        				}
		        			}else{
		        				return $app->json(array(
						            'status'    => 	false,
						            'title'		=>	'Error',
						            'content'   => 	'Debe contener una direccion de email valido.'
					        	));
		        			}
	        			}else{
	        				return $app->json(array(
					            'status'    => 	false,
					            'title'		=>	'Error',
					            'content'   => 	'La direccion postal debe contener al menos cinco caracteres.'
				        	));
	        			}	        		
	        		}else{
	        			return $app->json(array(
				            'status'    => 	false,
				            'title'		=>	'Error',
				            'content'   => 	'La direccion principal debe contener al menos cinco caracteres.'
				        ));
	        		}
        		}else{

        			return $app->json(array(
			            'status'    => 	false,
			            'title'		=>	'Error',
			            'content'   => 	'Numero telefonico principal debe Contener 10 Digitos.'
			        ));
        		}
        	}else{

				return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'Debe ingresar una fecha de nacimiento valida.'
		        ));
        	}
        }else{
			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'El nombre debe tener mas de dos caracteres.'
	        ));
        }

		return $app->json(array(
	        'status'    => 	success,
	        'title'		=>	'success',
	        'content'   => 	'Si Funciono....'
	    ));

	}else{
		return $app->json(array(
	        'status'    => 	false,
	        'title'		=>	'Error',
	        'content'   => 	'Imposible enviar la solicutd, no corresponde al pais seleccionado para el cliente.'
	    ));
	
	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Service Internet Vzla/////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveInternetVZ(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);

		if( isset($params['vzla_int_origen']) )
		{
			$origen = $params['vzla_int_origen'];
		}else{
			$oDat 	=	ServiceIntVZTmp::ServiceInt($params['vzla_int_client']);

			if($oDat <> false)
			{
				$origen = $oDat['origen'];
			}else{
				$origen = "14";
			}
		}
	        
	    $info 	=	[
	    	'client'		=>	(isset($params['vzla_int_client']))		? $params['vzla_int_client']		: $params['vzla_int_client'],
	    	'int_res'		=>	(isset($params['vzla_int_res']))		? $params['vzla_int_res']			: "14",	
	    	'int_com'		=>	(isset($params['vzla_int_trade'])) 		? $params['vzla_int_trade'] 		: "20",
	    	'pho_res'		=>	(isset($params['vzla_int_res_pho'])) 	? $params['vzla_int_res_pho'] 		: "9",
	    	'pho_com'		=>	(isset($params['vzla_int_trade_pho'])) 	? $params['vzla_int_trade_pho']		: "13",
	    	'ref'			=>	(isset($params['vzla_int_referred'])) 	? $params['vzla_int_referred'] 		: "0",
	    	'ref_list'		=>	(isset($params['vzla_int_ref_list']))	? $params['vzla_int_ref_list']		: "0",
	    	'sup'			=>	(isset($params['vzla_int_support'])) 	? $params['vzla_int_support']		: "0",
	    	'sup_list'		=>	(isset($params['vzla_int_ope'])) 		? $params['vzla_int_ope']			: "1",
	    	'bank'			=>	(isset($params['vzla_int_bank'])) 		? $params['vzla_int_bank']			: "115",
	    	'payment'		=>	(isset($params['vzla_int_payment'])) 	? $params['vzla_int_payment']		: "1",
	    	'amount'		=>	(isset($params['vzla_int_amount'])) 	? $params['vzla_int_amount']		: "",
	    	'trans'			=>	(isset($params['vzla_int_trans'])) 		? $params['vzla_int_trans']			: "",
	    	'trans_date'	=>	(isset($params['vzla_int_date'])) 		? $params['vzla_int_date']			: "",
	    	'email'			=>	(isset($params['vzla_int_email'])) 		? $params['vzla_int_email']			: "",
	    	'service'		=>	(isset($params['vzla_int_status'])) 	? $params['vzla_int_status']		: "1",    	
	    	'order_type'	=>	(isset($params['vzla_int_type_order'])) ? $params['vzla_int_type_order']	: "1",
	    	'origen'		=>	$origen,
	    	'additional'	=>	(isset($params['vzla_int_additional'])) ? $params['vzla_int_additional']	: "",
	    	'operator'		=>	$app['session']->get('id')	
	    ];

		$save 	=	ServiceIntVZTmp::SaveService($info);

		if($save == true)
		{

	        $info = array('client' => $info['client'], 'channel' => 'Actualizacion de Servicio Internet Venezuela', 'message' => 'Actualizacion de Servicio de Int VZLA - '.strtoupper($info['client']).' - Operador - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	true,
	            'title'		=>	'Success',
	            'content'   => 	'Data Stored Correctly'
	        ));

	    }else{
	        $info = array('client' => $info['client'], 'channel' => 'Actualizacion de Servicio Internet Venezuela', 'message' => 'Error al Actualizar Servicio Int VZLA - '.strtoupper($info['client']).' - Operador - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Error al intentar actualizar el Servicio de Internet VZLA. Si el Error persiste contactar al administrador de Sistema.'
	        ));
	    }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewInternetVZ(Application $app, Request $request)
	{
		$servInt 	= 	ServiceIntVZTmp::ServiceInt($request->get('id'));
		$servInf 	=	Approvals::GetInfoService($request->get('id'), '6');
		$servCoo 	=	Coordination::GetInfoService($servInf['ticket']);

		if($servInt)
		{
			return $app->json([
				'status' 	=>	true, 
				'int' 		=> 	$servInt,
				'inf'		=>	$servInf,
				'coo'		=>	$servCoo
			]);

		}else{
			return $app->json(array('status' => false));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

public static function SubmitInternetVZ(Application $app, Request $request)
{
	// $date = date('Y-m-d H:i:s', time());

	// $params     =   [];

	// $info 		=	"";

	// parse_str($request->get('value'), $params);

	// $client 	=	$params['vzla_int_client'];

	// $ticket 	= 	ServiceIntVZ::GetTicket($client);

	// $serv 		=	ServiceIntVZTmp::ServiceInt($client);

	// if($serv['status'] <> false)
	// {
	// 	$saveServ 	=	ServiceIntVZ::SaveService($ticket, $serv, $date);

	// 	if($saveServ == true)
	// 	{
	// 		$info 		= 	array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Envio de Servicio Internet Vzla - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//         $app['datalogger']->RecordLogger($info);

	// 		if($params['vzla_int_support'] == 0)
	// 		{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
	// 		else
	// 		{ $user 	=	['owen'	=>	$params['vzla_int_ope'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
			
	// 		$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

	// 		$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

	// 		if($saveApro == true)
	// 		{
	// 			$query 	=	'SELECT 
	// 				t1.client_id,
	// 				t2.name,
	// 				t2.birthday,
	// 				t2.phone_main,
	// 				t2.phone_alt,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
	// 				t2.coor_lati,
	// 				t2.coor_long,
	// 				(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
	// 				t2.add_main,
	// 				t2.add_postal,
	// 				t2.email_main,
	// 				t2.email_other,
	// 				t2.additional,
	// 				(SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res,
	// 				(SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res,
	// 				(SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com,
	// 				(SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com,
	// 				(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
	// 				(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
	// 				(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,				
	// 				(SELECT name FROM data_bank WHERE id = t1.bank_id) AS bank,
	// 				t1.amount,
	// 				t1.transference,
	// 				t1.trans_date,
	// 				t1.email,
	// 				(IF(t1.ref_id = "1","SI","NO")) AS ref,
	// 				(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
	// 				(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
	// 				(IF(t1.sup_id = "1","SI","NO")) AS sup,
	// 				(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
	// 				(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
	// 				(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
	// 				(SELECT email FROM users WHERE id = prop_id) AS prop_email,
	// 				(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
	// 				(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
	// 				(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
	// 				(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
	// 				t1.created_at AS created
	// 				FROM cp_service_vzla_int AS t1 
	// 				INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
	// 				INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
	// 				AND t1.ticket = "'.$ticket.'"';

	// 			$iData	=	DBSmart::DBQuery($query);

	// 			if($iData <> false)
	// 			{
	// 				$pData 	=	[
	// 					'username'		=>	$iData['prop'],
	// 					'departament'	=>	$iData['prop_depart'],
	// 					'subject'		=>	'INTERNET VZLA - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 					'service'		=> 	'INTERNET VZLA',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];

	// 				$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
	// 				$refHTML	=	Leads::GetReferredHtml($iData);
	// 				$supHTML	=	Leads::GetSupportHtml($iData);

	// 				$saleHTML	=	Leads::GetLeadSaleHtml($iData);
	// 				$subHTML	=	Leads::GetSubIntVZHtml($iData);

	// 				$mail   	=   new SendMail();
	// 				$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 				$pData 	=	[
	// 					'username'		=>	'DATA ENTRY',
	// 					'departament'	=>	'DATA ENTRY',
	// 					'subject'		=>	'INTERNET VZLA - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 					'service'		=> 	'INTERNET VZLA',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];
	// 				$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
	// 				$mail   	=   new SendMail();
	// 		 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 		        $query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
	// 		        $emails =   DBSmart::DBQueryAll($query);

	// 		        if($emails <> false)
	// 		        {
	// 		        	foreach ($emails as $e => $mail) 
	// 		        	{
	// 		        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
	// 		        		$pData 	=	[
	// 							'username'		=>	strtoupper($mail['name']),
	// 							'departament'	=>	$mail['dept'],
	// 							'subject'		=>	'INTERNET VZLA - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 							'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 							'service'		=> 	'INTERNET VZLA',
	// 							'ticket'		=>	$ticket,
	// 							'created'		=>	$iData['created']
	// 						];
	// 						$mail   	=   new SendMail();
	// 						$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
	// 		        	}
	// 		        }

	// 		        $html = "";
	// 		        $html =	'<div class="row">
	// 							<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
	// 		                		<div class="panel panel-darken">
	// 		                    		<div class="panel-heading" style="text-align: center;">
	// 		                        		<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-globe" style="font-size: 50px;"></i>
	// 		                        		<h3 class="panel-title" style="margin-top: 10px;">INTERNET VZLA</h3>
	// 		                    		</div>
	// 		                    	<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
	// 		                	</div>
	// 						</div>';

	// 			    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
	// 			    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

	// 			    $iTicket 	=	Drives::RequestIntVZInfoNew($ticket);
	// 				$iResult 	=	Drives::InsertDBIntVZNew($iTicket);

	// 				return $app->json(array(
	// 		            'status'    => 	true,
	// 		            'title'		=>	'Success',
	// 		            'content'   => 	'Datos Enviados Correctamente',
	// 		            'html'		=> 	$html
	// 		        ));

	// 			}else{

	// 				$info = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error en envio de Servicio Internet Vzla - '.strtoupper($ticket).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 		        $app['datalogger']->RecordLogger($info);

	// 				return $app->json(array(
	// 		            'status'    => 	false,
	// 		            'title'		=>	'Error',
	// 		            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
	// 		        ));
	// 			}
			
	// 		}else{

	// 			$info = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error en envio de Servicio Internet Vzla - '.strtoupper($ticket).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 	        $app['datalogger']->RecordLogger($info);

	// 			return $app->json(array(
	// 	            'status'    => 	false,
	// 	            'title'		=>	'Error',
	// 	            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
	// 	        ));
			
	// 		}

	// 	}else{

	// 		$info = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error en envio de Servicio Internet Vzla - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//         $app['datalogger']->RecordLogger($info);

	// 		return $app->json(array(
	//             'status'    => 	false,
	//             'title'		=>	'Error',
	//             'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
	//         ));
		
	// 	}
	
	// }else{

	// 	$info2 = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error - Lead Internet VZLA - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//     $app['datalogger']->RecordLogger($info2);

	// 	return $app->json(array(
	//         'status'    => 	false,
	//         'title'		=>	'Error',
	//         'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio de Internet VZLA registrado en Sistema, si es un error notifique al administrador del sistema.'
	//     ));

	// }

	$date = date('Y-m-d H:i:s', time());

	$params     =   [];

	$info 		=	"";

	parse_str($request->get('value'), $params);

	$iClient 	=	Leads::GetLeadByClient($params['vzla_int_client']);

	$client 	=	$params['vzla_int_client'];

	$serv 		=	ServiceIntVZTmp::ServiceInt($client);

	if($iClient['country'] == "4")
	{
        if(strlen($iClient['nameClient']) > 2)
        {
        	if($iClient['birthday'] <> "")
        	{
        		if(strlen($iClient['phone_main']) >= 10)
        		{
	        		if(strlen($iClient['add_main']) > 5)
	        		{
						if($iClient['email_main'] <> "")
		        		{
        					if($serv['origen'] <> "1")
        					{
								if($serv['status'] <> false)
								{
									$ticket 	= 	ServiceIntVZ::GetTicket($client);

									$saveServ 	=	ServiceIntVZ::SaveService($ticket, $serv, $date);

									if($saveServ == true)
									{
										$info 		= 	array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Envio de Servicio Internet Vzla - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

								        $app['datalogger']->RecordLogger($info);

										if($params['vzla_int_support'] == 0)
										{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
										else
										{ $user 	=	['owen'	=>	$params['vzla_int_ope'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
										
										$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

										$saveApro 	=	Approvals::SaveAprovals($ticket, $serv, $date);

										if($saveApro == true)
										{
											$query 	=	'SELECT 
												t1.client_id,
												t2.name,
												t2.birthday,
												t2.phone_main,
												t2.phone_alt,
												CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
												(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
												CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
												(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
												t2.coor_lati,
												t2.coor_long,
												(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
												t2.add_main,
												t2.add_postal,
												t2.email_main,
												t2.email_other,
												t2.additional,
												(SELECT name FROM data_internet WHERE id = t1.int_res_id) AS int_res,
												(SELECT name FROM data_phone WHERE id = t1.pho_res_id) AS pho_res,
												(SELECT name FROM data_internet WHERE id = t1.int_com_id) AS int_com,
												(SELECT name FROM data_phone WHERE id = t1.pho_com_id) AS pho_com,
												(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
												(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
												(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,				
												(SELECT name FROM data_bank WHERE id = t1.bank_id) AS bank,
												t1.amount,
												t1.transference,
												t1.trans_date,
												t1.email,
												(IF(t1.ref_id = "1","SI","NO")) AS ref,
												(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
												(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
												(IF(t1.sup_id = "1","SI","NO")) AS sup,
												(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
												(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
												(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
												(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
												(SELECT email FROM users WHERE id = prop_id) AS prop_email,
												(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
												(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
												(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
												(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
												(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
												t1.created_at AS created
												FROM cp_service_vzla_int AS t1 
												INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
												INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
												AND t1.ticket = "'.$ticket.'"';

											$iData	=	DBSmart::DBQuery($query);

											if($iData <> false)
											{
												$pData 	=	[
													'username'		=>	$iData['prop'],
													'departament'	=>	$iData['prop_depart'],
													'subject'		=>	'INTERNET VZLA - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
													'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
													'service'		=> 	'INTERNET VZLA',
													'ticket'		=>	$ticket,
													'created'		=>	$iData['created']
												];

												$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
												$refHTML	=	Leads::GetReferredHtml($iData);
												$supHTML	=	Leads::GetSupportHtml($iData);

												$saleHTML	=	Leads::GetLeadSaleHtml($iData);
												$subHTML	=	Leads::GetSubIntVZHtml($iData);

												$mail   	=   new SendMail();
												$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

												$pData 	=	[
													'username'		=>	'DATA ENTRY',
													'departament'	=>	'DATA ENTRY',
													'subject'		=>	'INTERNET VZLA - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
													'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
													'service'		=> 	'INTERNET VZLA',
													'ticket'		=>	$ticket,
													'created'		=>	$iData['created']
												];
												$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
												$mail   	=   new SendMail();
										 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

										        $query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
										        $emails =   DBSmart::DBQueryAll($query);

										        if($emails <> false)
										        {
										        	foreach ($emails as $e => $mail) 
										        	{
										        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
										        		$pData 	=	[
															'username'		=>	strtoupper($mail['name']),
															'departament'	=>	$mail['dept'],
															'subject'		=>	'INTERNET VZLA - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
															'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
															'service'		=> 	'INTERNET VZLA',
															'ticket'		=>	$ticket,
															'created'		=>	$iData['created']
														];
														$mail   	=   new SendMail();
														$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
										        	}
										        }

										        $html = "";
										        $html =	'<div class="row">
															<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
										                		<div class="panel panel-darken">
										                    		<div class="panel-heading" style="text-align: center;">
										                        		<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-globe" style="font-size: 50px;"></i>
										                        		<h3 class="panel-title" style="margin-top: 10px;">INTERNET VZLA</h3>
										                    		</div>
										                    	<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
										                	</div>
														</div>';

											    $iTicket    =   Drives::ReqMacAprInfTicket($ticket);
											    $iDriver    =   Drives::ReqMacAprInsInfTicket($iTicket);

											    $iTicket 	=	Drives::RequestIntVZInfoNew($ticket);
												$iResult 	=	Drives::InsertDBIntVZNew($iTicket);

												return $app->json(array(
										            'status'    => 	true,
										            'title'		=>	'Success',
										            'content'   => 	'Datos Enviados Correctamente',
										            'html'		=> 	$html
										        ));

											}else{

												$info = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error en envio de Servicio Internet Vzla - '.strtoupper($ticket).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

										        $app['datalogger']->RecordLogger($info);

												return $app->json(array(
										            'status'    => 	false,
										            'title'		=>	'Error',
										            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
										        ));
											}
										
										}else{

											$info = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error en envio de Servicio Internet Vzla - '.strtoupper($ticket).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

									        $app['datalogger']->RecordLogger($info);

											return $app->json(array(
									            'status'    => 	false,
									            'title'		=>	'Error',
									            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
									        ));
										
										}

									}else{

										$info = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error en envio de Servicio Internet Vzla - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

								        $app['datalogger']->RecordLogger($info);

										return $app->json(array(
								            'status'    => 	false,
								            'title'		=>	'Error',
								            'content'   => 	'Error intentando almacenar la solicitud de servicio. Si el error persiste, contactar al administrador de sistemas.'
								        ));
									
									}
								
								}else{

									$info2 = array('client' => $client, 'channel' => 'Lead Internet VZLA', 'message' => 'Error - Lead Internet VZLA - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

								    $app['datalogger']->RecordLogger($info2);

									return $app->json(array(
								        'status'    => 	false,
								        'title'		=>	'Error',
								        'content'   => 	'Cliente - '.$client.' - No tiene un Pre Servicio de Internet VZLA registrado en Sistema, si es un error notifique al administrador del sistema.'
								    ));

								}

        					}else{
	        					return $app->json(array(
						            'status'    => 	false,
						            'title'		=>	'Error',
						            'content'   => 	'Deber contener un origen de la venta valido.'
					        	));	
        					}
	        			}else{
	        				return $app->json(array(
					            'status'    => 	false,
					            'title'		=>	'Error',
					            'content'   => 	'Debe contener una direccion de email valido.'
				        	));
	        			}	        		
	        		}else{
	        			return $app->json(array(
				            'status'    => 	false,
				            'title'		=>	'Error',
				            'content'   => 	'La direccion principal debe contener al menos cinco caracteres.'
				        ));
	        		}
        		}else{

        			return $app->json(array(
			            'status'    => 	false,
			            'title'		=>	'Error',
			            'content'   => 	'Numero telefonico principal debe Contener 10 Digitos.'
			        ));
        		}
        	}else{

				return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'Debe ingresar una fecha de nacimiento valida.'
		        ));
        	}
        }else{
			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'El nombre debe tener mas de dos caracteres.'
	        ));
        }

		return $app->json(array(
	        'status'    => 	success,
	        'title'		=>	'success',
	        'content'   => 	'Si Funciono....'
	    ));

	}else{
		return $app->json(array(
	        'status'    => 	false,
	        'title'		=>	'Error',
	        'content'   => 	'Imposible enviar la solicutd, no corresponde al pais seleccionado para el cliente.'
	    ));
	
	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// Service Modem Router ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SaveMRouter(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);
	        
	    $info 	=	[
	    	'client'		=>	(isset($params['pr_mr_client']))		? $params['pr_mr_client']		: $params['pr_mr_client'],
	    	'cant'			=>	(isset($params['pr_mr_cant']))			? $params['pr_mr_cant']			: "0",
	    	'red'			=>	(isset($params['pr_mr_red'])) 			? $params['pr_mr_red']			: "0",
	    	'ssid'			=>	(isset($params['pr_mr_ssid'])) 			? $params['pr_mr_ssid'] 		: "",
	    	'password'		=>	(isset($params['pr_mr_pass'])) 			? $params['pr_mr_pass'] 		: "",
	    	'payment'		=>	(isset($params['pr_mr_payment'])) 		? $params['pr_mr_payment']		: "1",
	    	'sup'			=>	(isset($params['pr_mr_support'])) 		? $params['pr_mr_support']		: "0",
	    	'sup_list'		=>	(isset($params['pr_mr_ope_list'])) 		? $params['pr_mr_ope_list']		: "1",
			'ref'			=>	(isset($params['pr_mr_referred'])) 		? $params['pr_mr_referred']		: "0",
	    	'ref_list'		=>	(isset($params['pr_mr_ref_list']))		? $params['pr_mr_ref_list']		: "0",
	    	'service'		=>	(isset($params['pr_mr_status'])) 		? $params['pr_mr_status']		: "1",
	    	'order_type'	=>	(isset($params['pr_mr_type_order'])) 	? $params['pr_mr_type_order']	: "1",
	    	'origen'		=>	(isset($params['pr_mr_origen'])) 		? $params['pr_mr_origen']		: "1",
			'operator'		=>	$app['session']->get('id')	
	    ];

		$save 	=	ServiceMRouterPrTmp::SaveMRouter($info);

		if($save == true)
		{

	        $info = array('client' => $info['client'], 'channel' => 'Lead Product Modem Router Update', 'message' => 'Update Product Modem Router PR- '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	true,
	            'title'		=>	'Success',
	            'content'   => 	'Data Stored Correctly'
	        ));

	    }else{

	        $info = array('client' => $info['client'], 'channel' => 'Lead Product Modem Router Update', 'message' => 'Error Update Product Modem Router PR - '.strtoupper($info['client']).' - Execute by - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Error while trying to update the Modem Router Product. If the error persists, contact the system Administrator.'
	        ));
	    }

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewMRouter(Application $app, Request $request)
	{
		$servMr = ServiceMRouterPrTmp::ServiceMRouter($request->get('id'));

		if($servMr)
		{
			return $app->json([
				'status' 	=>	true, 
				'mr' 		=> 	$servMr
			]);

		}else{
			return $app->json(array('status' => false));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewServiceMRouter(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);

		$client =	$params['pr_mr_client'];

		return $app->json(array('status' => false));

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

public static function SubmitMRouter(Application $app, Request $request)
{

	// $date = date('Y-m-d H:i:s', time());

	// $params     =   [];

	// $info 		=	"";

	// parse_str($request->get('value'), $params);

	// $client 	=	$params['pr_mr_client'];

	// $ticket 	= 	ServiceMRouterPr::GetTicket();

	// $serv 		=	ServiceMRouterPrTmp::ServiceMRouter($client);

	// if($serv['status'] <> false)
	// {

	// 	$saveServ 	=	ServiceMRouterPr::SaveMRouter($ticket, $serv, $date);
	
	// 	if($saveServ == true)
	// 	{
	// 		$info 		= 	array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Solicitud Producto Modem Router PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

 //    		$app['datalogger']->RecordLogger($info);

	// 		if($params['pr_mr_support'] == 0)
	// 		{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
	// 		else
	// 		{ $user 	=	['owen'	=>	$params['pr_mr_ope_list'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
			
	// 		$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

	// 		if($saveServ == true)
	// 		{
	// 			$query 	=	'SELECT 
	// 				t1.client_id,
	// 				t2.name,
	// 				t2.birthday,
	// 				t2.phone_main,
	// 				t2.phone_alt,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
	// 				CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
	// 				(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
	// 				t2.coor_lati,
	// 				t2.coor_long,
	// 				(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
	// 				t2.add_main,
	// 				t2.add_postal,
	// 				t2.email_main,
	// 				t2.email_other,
	// 				t2.additional,
	// 				t1.cant,
	// 				(SELECT name FROM data_wireless WHERE id = t1.red_id) AS red,
	// 				t1.ssid,
	// 				t1.password,
	// 				(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
	// 				(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
	// 				(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
	// 				(IF(t1.ref_id = "1","SI","NO")) AS ref,
	// 				(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
	// 				(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
	// 				(IF(t1.sup_id = "1","SI", "NO")) AS sup,
	// 				(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
	// 				(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
	// 				(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
	// 				(SELECT email FROM users WHERE id = prop_id) AS prop_email,
	// 				(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
	// 				(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
	// 				(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
	// 				(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
	// 				(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
	// 				t1.created_at AS created
	// 				FROM cp_service_pr_mr AS t1
	// 				INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
	// 				INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
	// 				AND t1.ticket =  "'.$ticket.'"';

	// 			$iData	=	DBSmart::DBQuery($query);

	// 			if($iData <> false)
	// 			{
	// 				$pData 	=	[
	// 					'username'		=>	$iData['prop'],
	// 					'departament'	=>	$iData['prop_depart'],
	// 					'subject'		=> 	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket,
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 					'service'		=> 	'MODEM ROUTER',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];

	// 				$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
	// 				$refHTML	=	Leads::GetReferredHtml($iData);
	// 				$supHTML	=	Leads::GetSupportHtml($iData);

	// 				$saleHTML	=	Leads::GetLeadSaleHtml($iData);
	// 				$subHTML	=	Leads::GetSubMRouterHtml($iData);

	// 				$mail   	=   new SendMail();
	// 				$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 				$pData 	=	[
	// 					'username'		=>	'DATA ENTRY',
	// 					'departament'	=>	'DATA ENTRY',
	// 					'subject'		=>	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 					'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 					'service'		=> 	'MODEM ROUTER',
	// 					'ticket'		=>	$ticket,
	// 					'created'		=>	$iData['created']
	// 				];
					
	// 				$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
	// 				$mail   	=   new SendMail();
	// 		 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

	// 		        $query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
	// 		        $emails =   DBSmart::DBQueryAll($query);

	// 		        if($emails <> false)
	// 		        {
	// 		        	foreach ($emails as $e => $mail) 
	// 		        	{
	// 		        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
	// 		        		$pData 	=	[
	// 							'username'		=>	strtoupper($mail['name']),
	// 							'departament'	=>	$mail['dept'],
	// 							'subject'		=>	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 							'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 							'service'		=> 	'MODEM ROUTER',
	// 							'ticket'		=>	$ticket,
	// 							'created'		=>	$iData['created']
	// 						];
	// 						$mail   	=   new SendMail();
	// 						$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
	// 		        	}
	// 		        }

	// 		        $emails[1]	=	['email' => 'carlos.alberti@boomsolutionspr.com', 	'name' => strtoupper("Carlos Alberti")];
	// 		        $emails[2]	=	['email' => 'vibelka.cruz@boomsolutionspr.com', 	'name' => strtoupper("Vibelka Cruz")];
	// 		        $emails[3]	=	['email' => 'juan.rivera@boomsolutionspr.com,', 	'name' => strtoupper("Juan Rivera")];

	// 				if($emails <> false)
	// 		        {
	// 		        	foreach ($emails as $e => $mail) 
	// 		        	{
	// 		        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
	// 		        		$pData 	=	[
	// 							'username'		=>	strtoupper($mail['name']),
	// 							'departament'	=>	$mail['dept'],
	// 							'subject'		=>	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
	// 							'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
	// 							'service'		=> 	'MODEM ROUTER',
	// 							'ticket'		=>	$ticket,
	// 							'created'		=>	$iData['created']
	// 						];
	// 						$mail   	=   new SendMail();
	// 						$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
	// 		        	}
	// 		        }

	// 		        $html = "";
	// 		        $html =	'<div class="row">
	// 							<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
	// 		                		<div class="panel panel-darken">
	// 		                    		<div class="panel-heading" style="text-align: center;">
	// 		                        		<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-rss" style="font-size: 50px;"></i>
	// 		                        		<h3 class="panel-title" style="margin-top: 10px;">MODEM ROUTER PR</h3>
	// 		                    		</div>
	// 		                    	<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
	// 		                	</div>
	// 						</div>';

	// 		        $iTicket    =   Drives::RequestMRInfo($ticket);
	// 		        $iDriver    =   Drives::InsertDBMR($iTicket);

	// 				return $app->json(array(
	// 		            'status'    => 	true,
	// 		            'title'		=>	'Success',
	// 		            'content'   => 	'Datos Enviados Correctamente',
	// 		            'html'		=> 	$html
	// 		        ));		

	// 			}else{

	// 				$info = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error Enviando Producto Modem Router PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 		        $app['datalogger']->RecordLogger($info);

	// 				return $app->json(array(
	// 		            'status'    => 	false,
	// 		            'title'		=>	'Error',
	// 		            'content'   => 	'Error intentando almacenar la solicitud de producto Modem Router PR. Si el error persiste, contactar al administrador de sistemas'
	// 		        ));
	// 			}

	// 		}else{

	// 			$info = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error Enviando Producto Modem Router PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	// 	        $app['datalogger']->RecordLogger($info);

	// 			return $app->json(array(
	// 	            'status'    => 	false,
	// 	            'title'		=>	'Error',
	// 	            'content'   => 	'Error intentando almacenar la solicitud de producto Modem Router PR. Si el error persiste, contactar al administrador de sistemas'
	// 	        ));

	// 		}

	// 	}else{
			
	// 		$info = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error Enviando Producto Modem Router PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//         $app['datalogger']->RecordLogger($info);

	// 		return $app->json(array(
	//             'status'    => 	false,
	//             'title'		=>	'Error',
	//             'content'   => 	'Error intentando almacenar la solicitud de producto Modem Router PR. Si el error persiste, contactar al administrador de sistemas'
	//         ));
		
	// 	}

	// }else{
	// 	$info2 = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error - Producto Modem Router PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	//     $app['datalogger']->RecordLogger($info2);

	// 	return $app->json(array(
	//         'status'    => 	false,
	//         'title'		=>	'Error',
	//         'content'   => 	'Cliente - '.$client.' - No tiene una Pre Orden de Modem Router PR registrado en Sistema, si es un error notifique al administrador del sistema.'
	//     ));
	
	// }

	$date = date('Y-m-d H:i:s', time());

	$params     =   [];

	$info 		=	"";

	parse_str($request->get('value'), $params);

	$iClient 	=	Leads::GetLeadByClient($params['pr_mr_client']);

	$client 	=	$params['pr_mr_client'];

	$serv 		=	ServiceMRouterPrTmp::ServiceMRouter($client);

	if($iClient['country'] == "1")
	{
        if(strlen($iClient['nameClient']) > 2)
        {
        	if($iClient['birthday'] <> "")
        	{
        		if(strlen($iClient['phone_main']) >= 10)
        		{
	        		if(strlen($iClient['add_main']) > 5)
	        		{
	        			if(strlen($iClient['add_postal']) > 5)
	        			{
							if($iClient['email_main'] <> "")
		        			{
		        				if($serv['origen'] <> "1")
		        				{

									if($serv['status'] <> false)
									{
										$ticket 	= 	ServiceMRouterPr::GetTicket();

										$saveServ 	=	ServiceMRouterPr::SaveMRouter($ticket, $serv, $date);
									
										if($saveServ == true)
										{
											$info 		= 	array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Solicitud Producto Modem Router PR - '.strtoupper($client).' - Solicitado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

								    		$app['datalogger']->RecordLogger($info);

											if($params['pr_mr_support'] == 0)
											{ $user 	=	['owen'	=>	$app['session']->get('id'), 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
											else
											{ $user 	=	['owen'	=>	$params['pr_mr_ope_list'], 'assist' => $app['session']->get('id'), 'client' => $client, 'created' => $serv['created']]; }
											
											$saveProp 	=	ServicePropietary::SavePropietary($ticket, $user, $date);

											if($saveServ == true)
											{
												$query 	=	'SELECT 
													t1.client_id,
													t2.name,
													t2.birthday,
													t2.phone_main,
													t2.phone_alt,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1)) AS ss,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "SS" ORDER BY id DESC LIMIT 1) AS ss_exp,
													CONCAT("XXXX",(SELECT last FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1)) AS id,
													(SELECT exp FROM cp_cypherdata WHERE client_id = t1.client_id AND c_type = "ID" ORDER BY id DESC LIMIT 1) AS id_exp,
													t2.coor_lati,
													t2.coor_long,
													(SELECT name FROM data_town WHERE id = t2.town_id) AS town,
													t2.add_main,
													t2.add_postal,
													t2.email_main,
													t2.email_other,
													t2.additional,
													t1.cant,
													(SELECT name FROM data_wireless WHERE id = t1.red_id) AS red,
													t1.ssid,
													t1.password,
													(SELECT name FROM data_payment WHERE id = t1.payment_id) AS payment,
													(SELECT name FROM data_type_order WHERE id = t1.type_order_id) AS type_order,
													(SELECT name FROM data_status_service WHERE id = t1.service_id) AS type_service,
													(IF(t1.ref_id = "1","SI","NO")) AS ref,
													(SELECT name FROM cp_referred WHERE id = t1.ref_list_id) AS ref_name,
													(SELECT client_id FROM cp_referred WHERE id = t1.ref_list_id) AS ref_client,
													(IF(t1.sup_id = "1","SI", "NO")) AS sup,
													(SELECT username FROM users WHERE id = t3.owen_id) AS prop,
													(SELECT id FROM users WHERE id = t3.owen_id) AS prop_id,
													(SELECT departament_id FROM users WHERE id = t3.owen_id) AS prop_id_dep,
													(SELECT name FROM data_departament WHERE id = prop_id_dep) AS prop_depart,
													(SELECT email FROM users WHERE id = prop_id) AS prop_email,
													(SELECT username FROM users WHERE id = t3.assistan_id) AS assit,
													(SELECT id FROM users WHERE id = assistan_id) AS assit_id,
													(SELECT departament_id FROM users WHERE id = assistan_id) AS assit_id_dep,
													(SELECT name FROM data_departament WHERE id = assit_id_dep) AS assit_depart,
													(SELECT email FROM users WHERE id = t3.assistan_id) AS assit_email,
													t1.created_at AS created
													FROM cp_service_pr_mr AS t1
													INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id)
													INNER JOIN cp_serv_prop AS t3 ON (t1.ticket = t3.ticket_id)
													AND t1.ticket =  "'.$ticket.'"';

												$iData	=	DBSmart::DBQuery($query);

												if($iData <> false)
												{
													$pData 	=	[
														'username'		=>	$iData['prop'],
														'departament'	=>	$iData['prop_depart'],
														'subject'		=> 	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket,
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
														'service'		=> 	'MODEM ROUTER',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];

													$leadHTML	=	Leads::GetLeadSubmitHtml($iData);
													$refHTML	=	Leads::GetReferredHtml($iData);
													$supHTML	=	Leads::GetSupportHtml($iData);

													$saleHTML	=	Leads::GetLeadSaleHtml($iData);
													$subHTML	=	Leads::GetSubMRouterHtml($iData);

													$mail   	=   new SendMail();
													$email  	=   $mail->SendEmail($iData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

													$pData 	=	[
														'username'		=>	'DATA ENTRY',
														'departament'	=>	'DATA ENTRY',
														'subject'		=>	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
														'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
														'service'		=> 	'MODEM ROUTER',
														'ticket'		=>	$ticket,
														'created'		=>	$iData['created']
													];
													
													$EData 	=	['prop_email' => "aprobaciones@boomdish.com", 'prop' => 'DATA ENTRY'];
													$mail   	=   new SendMail();
											 		$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);

											        $query  =   'SELECT email, name, (SELECT name FROM data_departament WHERE id = departament_id) AS dept FROM data_emails WHERE departament_id = "'.$app['session']->get('departament').'" AND status_id = "1"';
											        $emails =   DBSmart::DBQueryAll($query);


											        if($emails <> false)
											        {
											        	foreach ($emails as $e => $maill) 
											        	{

											        		$EData 	=	['prop_email' => $maill['email'], 'prop' => strtoupper($maill['name'])];
											        		$pData 	=	[
																'username'		=>	strtoupper($maill['name']),
																'departament'	=>	$maill['dept'],
																'subject'		=>	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
																'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
																'service'		=> 	'MODEM ROUTER',
																'ticket'		=>	$ticket,
																'created'		=>	$iData['created']
															];
															$mail   	=   new SendMail();
															$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
											        	}
											        }

											        $emails[1]	=	['email' => 'carlos.alberti@boomsolutionspr.com', 	'name' => strtoupper("Carlos Alberti")];
											        $emails[2]	=	['email' => 'vibelka.cruz@boomsolutionspr.com', 	'name' => strtoupper("Vibelka Cruz")];
											        $emails[3]	=	['email' => 'juan.rivera@boomsolutionspr.com,', 	'name' => strtoupper("Juan Rivera")];

													if($emails <> false)
											        {
											        	foreach ($emails as $e => $mail) 
											        	{
											        		$EData 	=	['prop_email' => $mail['email'], 'prop' => strtoupper($mail['name'])];
											        		$pData 	=	[
																'username'		=>	strtoupper($mail['name']),
																'departament'	=>	'',
																'subject'		=>	'MODEM ROUTER - TICKET DE SERVICIO '.$ticket.' - '.$iData['phone_main'].'',
																'img'			=> 	'https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png',
																'service'		=> 	'MODEM ROUTER',
																'ticket'		=>	$ticket,
																'created'		=>	$iData['created']
															];
															$mail   	=   new SendMail();
															$email  	=   $mail->SendEmail($EData, $pData, $leadHTML, $refHTML, $supHTML, $saleHTML, $subHTML);
											        	}
											        }

											        $html = "";
											        $html =	'<div class="row">
																<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-3">
											                		<div class="panel panel-darken">
											                    		<div class="panel-heading" style="text-align: center;">
											                        		<i class="icon-append fa fa-lg fa-fw fa fa-lg fa-rss" style="font-size: 50px;"></i>
											                        		<h3 class="panel-title" style="margin-top: 10px;">MODEM ROUTER PR</h3>
											                    		</div>
											                    	<div class="panel-footer text-align-center"><h1>'.$ticket.'</h1></div>
											                	</div>
															</div>';

											        $iTicket    =   Drives::RequestMRInfo($ticket);
											        $iDriver    =   Drives::InsertDBMR($iTicket);

													return $app->json(array(
											            'status'    => 	true,
											            'title'		=>	'Success',
											            'content'   => 	'Datos Enviados Correctamente',
											            'html'		=> 	$html
											        ));		

												}else{

													$info = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error Enviando Producto Modem Router PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

											        $app['datalogger']->RecordLogger($info);

													return $app->json(array(
											            'status'    => 	false,
											            'title'		=>	'Error',
											            'content'   => 	'Error intentando almacenar la solicitud de producto Modem Router PR. Si el error persiste, contactar al administrador de sistemas'
											        ));
												}

											}else{

												$info = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error Enviando Producto Modem Router PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

										        $app['datalogger']->RecordLogger($info);

												return $app->json(array(
										            'status'    => 	false,
										            'title'		=>	'Error',
										            'content'   => 	'Error intentando almacenar la solicitud de producto Modem Router PR. Si el error persiste, contactar al administrador de sistemas'
										        ));

											}

										}else{
											
											$info = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error Enviando Producto Modem Router PR - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

									        $app['datalogger']->RecordLogger($info);

											return $app->json(array(
									            'status'    => 	false,
									            'title'		=>	'Error',
									            'content'   => 	'Error intentando almacenar la solicitud de producto Modem Router PR. Si el error persiste, contactar al administrador de sistemas'
									        ));
										
										}

									}else{
										$info2 = array('client' => $client, 'channel' => 'Producto Modem Router PR', 'message' => 'Error - Producto Modem Router PR - Submit - '.strtoupper($client).' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

									    $app['datalogger']->RecordLogger($info2);

										return $app->json(array(
									        'status'    => 	false,
									        'title'		=>	'Error',
									        'content'   => 	'Cliente - '.$client.' - No tiene una Pre Orden de Modem Router PR registrado en Sistema, si es un error notifique al administrador del sistema.'
									    ));
									
									}

		        				}else{

			        				return $app->json(array(
							            'status'    => 	false,
							            'title'		=>	'Error',
							            'content'   => 	'Deber contener un origen de la venta valido.'
						        	));
		        				}
		        			}else{
		        				return $app->json(array(
						            'status'    => 	false,
						            'title'		=>	'Error',
						            'content'   => 	'Debe contener una direccion de email valido.'
					        	));
		        			}
	        			}else{
	        				return $app->json(array(
					            'status'    => 	false,
					            'title'		=>	'Error',
					            'content'   => 	'La direccion postal debe contener al menos cinco caracteres.'
				        	));
	        			}	        		
	        		}else{
	        			return $app->json(array(
				            'status'    => 	false,
				            'title'		=>	'Error',
				            'content'   => 	'La direccion principal debe contener al menos cinco caracteres.'
				        ));
	        		}
        		}else{

        			return $app->json(array(
			            'status'    => 	false,
			            'title'		=>	'Error',
			            'content'   => 	'Numero telefonico principal debe Contener 10 Digitos.'
			        ));
        		}
        	}else{

				return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'Debe ingresar una fecha de nacimiento valida.'
		        ));
        	}
        }else{
			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'El nombre debe tener mas de dos caracteres.'
	        ));
        }

		return $app->json(array(
	        'status'    => 	success,
	        'title'		=>	'success',
	        'content'   => 	'Si Funciono....'
	    ));

	}else{
		return $app->json(array(
	        'status'    => 	false,
	        'title'		=>	'Error',
	        'content'   => 	'Imposible enviar la solicutd, no corresponde al pais seleccionado para el cliente.'
	    ));
	
	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function LeadsNew(Application $app, Request $request)
	{

		return $app['twig']->render('leads/new.html.twig',array(
            'sidebar'       =>  true,
            'countrys'		=>	Country::GetCountry()
        ));
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function NewSearch(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);

		$leads = (Leads::SearchSimpled($params) == false) ? false : true;

		return $app->json(array(
            'status'    => 	false,
            'info'		=>	$leads
        ));
	
	}

	public static function NewCreate(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('str'), $params);

		if(($params['c_name'] == "") OR ($params['search-fld'] == ""))
		{
			return $app->json(array(
		        'status'    => 	false,
		        'title'		=>	'Error',
		        'content'   => 	'Faltan Datos para la creacion del cliente, por favor verificar'
        	));

		}else{

			$leads = Leads::SearchCreate($params);

			if($leads['status'] == true)
			{
				return $app->json(array(
			        'status'    => 	true,
			        'title'		=>	'html',
			        'html'		=>	$leads['html']
	        	));

			}else{
				
				return $app->json(array(
			        'status'    => 	false,
			        'title'		=>	'html',
			        'html'		=>	''
	        	));
			}
		}	
	
	}

	public static function CreateLeadNew(Application $app, Request $request)
	{

		$params     =   [];

		parse_str($request->get('str'), $params);

		$query 	=	'SELECT client_id, type_acc FROM cp_leads WHERE ( (phone_main LIKE "%'.$params['search-fld'].'%") OR (phone_alt LIKE "%'.$params['search-fld'].'%") OR (phone_other LIKE "%'.$params['search-fld'].'%")) AND (country_id = "'.$params['c_country'].'") ORDER BY id DESC LIMIT 1';
		$iData 	=	DBSmart::DBQuery($query);

		$type 	=	$iData['type_acc'] + 1;

		$newLeads 	=	Leads::CreateSimple($params, $type, $app['session']->get('id'));

		if($newLeads['status'] == true)
		{
			$info = array('client' => $newLeads['client'], 'channel' => 'Create Leads', 'message' => 'Create Leads ID - '.$newLeads['client'].' - Execute by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        // $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	true,
	            'title'		=>	'Success',
	            'content'   => 	'Datos Enviados Correctamente',
	            'web'		=>	'/leads/view/'.$newLeads['client']
	        ));

		}else{

			$info = array('client' => $newLeads['client'], 'channel' => 'Create Leads', 'message' => 'Error Create Leads ID - '.$newLeads['client'].' - Execute by - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        // $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Error while trying to update the tv service. If the error persists, contact the system Administrator.'
	        ));
		}


		return $app->json(array(
            'status'    => 	false,
            'info'		=>	$leads
        ));
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function CreateLeads(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);

		$query 	=	'SELECT client_id, type_acc FROM cp_leads WHERE phone_main LIKE "%'.$params['search-fld'].'%" OR phone_alt LIKE "%'.$params['search-fld'].'%" ORDER BY id DESC LIMIT 1';
		$iData 	=	DBSmart::DBQuery($query);
		
		$type 	=	$iData['type_acc'] + 1;

		$newLeads 	=	Leads::CreateSimple($params, $type, $app['session']->get('id'));

		if($newLeads['status'] == true)
		{
			$info = array('client' => $newLeads['client'], 'channel' => 'Create Leads', 'message' => 'Create Leads ID - '.$newLeads['client'].' - Execute by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	true,
	            'title'		=>	'Success',
	            'content'   => 	'Datos Enviados Correctamente',
	            'web'		=>	'/leads/view/'.$newLeads['client']
	        ));

		}else{

			$info = array('client' => $newLeads['client'], 'channel' => 'Create Leads', 'message' => 'Error Create Leads ID - '.$newLeads['client'].' - Execute by - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Error while trying to update the tv service. If the error persists, contact the system Administrator.'
	        ));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ServObjections(Application $app, Request $request)
	{
		$serv 		= 	$request->get('serv');
		$client 	= 	$request->get('id');
		$lead 		=	Leads::GetLeadById($client);

		switch ($request->get('serv')) 
		{
			case $request->get('serv') == "tv":
				$serv = "2";
				break;

			case $request->get('serv') == "sec":
				$serv = "3";
				break;

			case $request->get('serv') == "int":
				$serv = "4";
				break;

			case $request->get('serv') == "wrl":
				$serv = "5";
				break;

			case $request->get('serv') == "ive":
				$serv = "6";
				break;
		}

		$info 		=	[
			'service'	=>	$serv,
			'country'	=>	$lead['country'],
			'status'	=>	"1"
		];

		$objections =	Objection::GetObjectionByService($info);

		return $app->json(array(
            'html'    => 	Leads::GetServObjectionHtml($objections, $serv, $client)
        ));

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SubmitObjService(Application $app, Request $request)
	{
		$params     =   [];

		parse_str($request->get('value'), $params);

		if($params['cancel_service'] == "2"){ $serv = "TELEVISION PR"; }
		elseif($params['cancel_service'] == "3") { $serv = "SEGURIDAD PR"; }
		elseif($params['cancel_service'] == "4") { $serv = "INTERNET PR"; }
		elseif($params['cancel_service'] == "5") { $serv = "WIRELESS PR"; }
		elseif($params['cancel_service'] == "6") { $serv = "INTERNET VZLA"; }
		else{ $serv = ""; }

		if(isset($params['objection_sel']))
		{

			foreach ($params['objection_sel'] as $p => $par) 
			{
				$iData = [
					'client'	=>	$params['cancel_client'],
					'objection'	=>	$par,
					'service'	=>	$params['cancel_service'],
					'operator'	=>	$app['session']->get('id'),
					'date'		=>	date('Y-m-d H:i:s', time())
				];

				$obj = LeadsServObjection::SaveObjections($iData);

			}

			if($obj == true)
			{
	 			$info = array('client' => $params['cancel_client'], 'channel' => 'Objeciones De Servicio - '.$serv, 'message' => 'Las objeciones por el servicio de - '.$serv .' se almacenaron Correctamente - realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info);

				return $app->json(array(
	                'status'    => 	true,
	                'title'		=>	'Success',
	                'content'   => 	'Informacion Almacenada Correctamente'
	            ));
			}else{


				$info = array('client' => $params['cancel_client'], 'channel' => 'Objeciones De Servicio - '.$serv, 'message' => 'Error al intentar almacenar objeciones para el servicio de - '.$serv .' - realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

			    $app['datalogger']->RecordLogger($info);

	        	return $app->json(array(
	                'status'    => 	false,
	                'title'		=>	'Error',
	                'content'   => 	'Ocurrio un error al intentar almacenar las objeciones, si persiste el error contacte al administrador de sistemas.'
	            ));
			}

		}else{

			$info = array('client' => $params['cancel_client'], 'channel' => 'Objeciones De Servicio - '.$serv, 'message' => 'No se seleccionaron objeciones para el servicio de - '.$ser .' - realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

		    $app['datalogger']->RecordLogger($info);

        	return $app->json(array(
                'status'    => 	false,
                'title'		=>	'Error',
                'content'   => 	'Debe seleccionar al menos una opcion para procesar la solicitud'
            ));
		}

	}

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function InfoVe(Application $app, Request $request)
	{
		$html = '';

		if($request->get('ced') <> "")
		{
	        $cne    =   new InfoVE;
	        $cli    =   json_decode($cne->CiudadanoVE($request->get('typ'), $request->get('ced')),true);

	        // var_dump($cli);
	        // exit;
             
	        if($cli['error'] == true)
	        {
		        $html = '';
				$html.='<table class=" table table-bordered" style="font-size: 10px; text-align: center;">';
				$html.='<tbody>';
				$html.='<tr>';
				$html.='<td>CLIENTE</td>';
				$html.='<td>'.((isset($cli['nombre'])) ? strtoupper($cli['nombre']) : "").'</td>';
				$html.='</tr>';
				$html.='<tr>';
				$html.='<td>CEDULA</td>';
				$html.='<td>'.((isset($cli['cedula'])) ? strtoupper($cli['cedula']) : "").'</td>';
				$html.='</tr>';
				$html.='<tr>';
				$html.='<td>ESTADO</td>';
				$html.='<td>'.((isset($cli['estado'])) ? strtoupper($cli['estado']) : "").'</td>';
				$html.='</tr>';
				$html.='<tr>';
				$html.='<td>MUNICIPIO</td>';
				$html.='<td>'.((isset($cli['municipio'])) ? strtoupper($cli['municipio']) : "").'</td>';
				$html.='</tr>';
				$html.='<tr>';
				$html.='<td>PARROQUIA</td>';
				$html.='<td>'.((isset($cli['parroquia'])) ? strtoupper($cli['parroquia']) : "").'</td>';
				$html.='</tr>';
				$html.='</tbody>';
				$html.='</table>';
				return $app->json(array('status' => true, 'html' => $html));
	        
	        }else{
		        $html = '';
				$html.='<table class=" table table-bordered" style="font-size: 10px; text-align: center;">';
				$html.='<tbody>';
				$html.='<tr>';
				$html.='<td>'.strtoupper($cli['descripcion']).'</td>';
				$html.='</tr>';
				$html.='</tbody>';
				$html.='</table>';
	        	return $app->json(array('status' => false, 'html' => $html));
	        }

		}else{
			$html = '';
			$html.='<table class=" table table-bordered" style="font-size: 10px; text-align: center;">';
			$html.='<tbody>';
			$html.='<tr>';
			$html.='<td>SIN INFORMACION PARA MOSTRAR</td>';
			$html.='</tr>';
			$html.='</tbody>';
			$html.='</table>';
			return $app->json(array('status' => false, 'html' => $html));
		}
	}


////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ListTownship(Application $app, Request $request)
	{
		$id 		=	$request->get('id');

		$township 	=	LeadTownShip::GetListByTownship($id);
		$parish 	=	LeadParish::GetListByParishSelect();
		$sector 	=	LeadSector::GetListBySectorSelect();

		return $app->json(array(
			'status' 	=> 	true,
			'township'	=>	$township,
			'parish'	=>	$parish,
			'sector'	=>	$sector
		));
	}

	public static function ListParish(Application $app, Request $request)
	{
		$id 		=	$request->get('id');

		$parish 	=	LeadParish::GetListByParish($id);

		return $app->json(array(
			'status' 	=> 	true,
			'parish'	=>	$parish
		));
	}

	public static function ListSector(Application $app, Request $request)
	{
		$id 		=	$request->get('id');

		$sector 	=	LeadSector::GetListBySector($id);

		return $app->json(array(
			'status' 	=> 	true,
			'sector'	=>	$sector
		));
	}

	public static function ListSectorG(Application $app, Request $request)
	{
		$id 		=	$request->get('id');


        $pID    =   LeadSector::GetSectorById($id)['states'];
        $tID    =   LeadParish::GetParishById($pID)['states'];
        $sID    =   LeadTownShip::GetTownShipByID($tID)['states'];
        $sFID   =   LeadStates::GetStatesByID($sID)['id'];

        $pHTML  =   LeadSector::GetListBySectorGen($pID, $id);
        $tHTML  =   LeadParish::GetListByParishGen($tID, $pID);
        $sHTML  =   LeadTownShip::GetListTownshipGen($sID, $tID);
        $sFHTML =   LeadStates::GetListStatesGen($sFID);

		return $app->json(array(
			'status' 	=> 	true,
			'sector'	=>	$pHTML,
			'parroquia'	=>	$tHTML,
			'municipio'	=>	$sHTML,
			'estado'	=>	$sFHTML

		));
	}


	public static function SearchSector(Application $app, Request $request)
	{
		$sector =	$request->get('sector');

		$res 	=	LeadSector::SearchSector($sector);

		return $app->json(array(
			'status' 	=> 	true,
			'html'		=>	LeadSector::SearchSectorHTML($res)

		));

	}

	public static function UpdateTicket(Application $app, Request $request)
	{
		$ticket =	$request->get('ticket');

		$con        =	New DBBoom;
        $connect    =   $con->Conexion();

		$query 		=	'SELECT ticket FROM request_macro WHERE ticket = "'.$ticket.'"';
     
        $statement  =   $connect->prepare($query);

        $statement->execute();

        $result     =   $statement->fetch(PDO::FETCH_ASSOC);

        if($result)
        {
			$query 	=	'SELECT (SELECT name FROM cp_leads_states WHERE id = state_id) AS state, (SELECT name FROM cp_leads_township WHERE id = township_id) AS township, (SELECT name FROM cp_leads_parish WHERE id = parish_id) AS parish, (SELECT name FROM cp_leads_sector WHERE id = sector_id) AS sector FROM cp_leads_geographic WHERE client_id = (SELECT client_id FROM cp_appro_serv WHERE ticket = "'.$ticket.'") ORDER BY id DESC LIMIT 1';

			$geo 	= 	DBSmart::DBQuery($query);

			$query 	=	'UPDATE request_macro SET estado = "'.$geo['state'].'", municipio = "'.$geo['township'].'", parroquia = "'.$geo['parish'].'", sector = "'.$geo['sector'].'" WHERE ticket = "'.$ticket.'"';

			$statement  =   $connect->prepare($query);

			$UpdGeo 	=	($statement->execute()) ? true : false;

			if($UpdGeo <> false)
			{

				return $app->json(array(
		            'status'    => 	true,
		            'title'		=>	'Success',
		            'content'   => 	'Datos Actualizados Correctamente.'
				));

			}else{

				return $app->json(array(
		            'status'    => 	false,
		            'title'		=>	'Error',
		            'content'   => 	'Imposible Realizar la Actualizacion, Intente Nuevamente.'
				));

			}

        }else{

			return $app->json(array(
	            'status'    => 	false,
	            'title'		=>	'Error',
	            'content'   => 	'Ticket No Encontrado'
			));
        }
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////
}