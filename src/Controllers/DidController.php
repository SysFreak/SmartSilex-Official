<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Did;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * 
 */
class DidController extends BaseController
{
    
    public function DidEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'did'       => Did::GetDidById($request->get('id'))
        ));
    
    }

    public function DidForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_di'] == 'new')
        {
            $saveDep = Did::SaveDid($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Did New', 'message' => 'Creacion de Did - '.strtoupper($params['name_di']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'roles'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Did New', 'message' => 'Error Al Intentar Crear Did - '.strtoupper($params['name_di']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                ));  
            }
        
        }
        elseif($params['type_di'] == 'edit')
        {

            $saveDep = Did::SaveDid($params);
                
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Did Edit', 'message' => 'Actualizacion de Paque - '.strtoupper($params['name_di']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'roles'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Did Edit', 'message' => 'Error Al Intentar Actualizar informacion del Did - '.strtoupper($params['name_di']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, <strong>Datos Duplicados</strong>, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
                
        }
    
    }

}