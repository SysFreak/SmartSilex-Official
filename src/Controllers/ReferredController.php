<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\InternetType;
use App\Models\Objection;
use App\Models\Medium;
use App\Models\Objetive;
use App\Models\Referred;
use App\Models\ReferredTmp;
use App\Models\ServiceM;
use App\Models\CypherData;
use App\Models\Leads;

use App\Lib\Config;
use App\Lib\UnCypher;
use App\Lib\DBSmart;

use Silex\Application;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class ReferredController extends BaseController
{

////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ReferredEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'referred'  => Referred::GetReferredById($request->get('id'))
        ));
    
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ReferredEditForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        $ref    =   Referred::UpdateReferred($params, $app['session']->get('id'));
        $ref2   =   ReferredTmp::UpdateReferred($params, $app['session']->get('id'));

        return $app->json(array('status'    => true));
    
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function ReferredForm(Application $app, Request $request)
    {

        $_replace   =   new Config();

        $params     =   [];

        parse_str($request->get('value'), $params);

        $refClient      =   Leads::LastClientID();  

        if(($params['re_ss'] <> "") || ($params['re_ss_exp'] <> ""))
        {

            $infoU      =   New UnCypher();

            $_replace   =   new Config();
            $exp        =   $_replace->ChangeDate($params['re_ss_exp']);

            $cData = [
                'ss'    =>  strtoupper($params['re_ss']),
                'exp'   =>  $exp
            ];

            $iData  =   [
                'client'    =>  $refClient['client'],
                'operator'  =>  $app['session']->get('id'),
                'type'      =>  'SS',
                'type_l'    =>  'SS',
                'country'   =>  $params['re_country_ss'],
                'exp'       =>  $exp,
                'type_d'    =>  $params['re_type_ss']
            ];

            $SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

            if($SaveCypher == true)
            {
                $info = array('client' => $refClient['client'], 'channel' => 'Seguro Social', 'message' => 'Seguro Social  - Creado - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

            }else{

                $info = array('client' => $refClient['client'], 'channel' => 'Seguro Social - Error', 'message' => 'Seguro Social - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json($SaveCypher);

            }       

        }else{
            $ss = "";
        }

        if(($params['re_ident'] <> "") || ($params['re_ident_exp'] <> ""))
        {

            $_replace   =   new Config();
            $exp        =   $_replace->ChangeDate($params['re_ident_exp']);

            $cData = [
                'id'    =>  strtoupper($params['re_ident']),
                'exp'   =>  $exp
            ];

            $iData  =   [
                'client'    =>  $refClient['client'],
                'operator'  =>  $app['session']->get('id'),
                'type'      =>  'ID',
                'type_l'    =>  'ID',
                'country'   =>  $params['re_country_ident'],
                'exp'       =>  $exp,
                'type_d'    =>  $params['re_type_ident']
            ];

            $SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

            if($SaveCypher == true)
            {
                $info = array('client' => $refClient['client'], 'channel' => 'Identificacion', 'message' => 'Identificacion - Creada - Procesada Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

            }else{
                $info = array('client' => $refClient['client'], 'channel' => 'Identificacion - Error', 'message' => 'Identificacion - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json($SaveCypher);
            } 

        }else{
            $id = "";
        }

        $ss     =   CypherData::GetCypherClientType($refClient['client'], 'SS');
        $id     =   CypherData::GetCypherClientType($refClient['client'], 'ID');

        $birth  =   ($params['re_birthday'] <> '' ) ? $_replace->ChangeDate($params['re_birthday']) : '';

        if($birth <> '')
        {
            $tiempo = strtotime($birth); 
            $ahora  = time(); 
            $edad   = ($ahora-$tiempo)/(60*60*24*365.25);  
            $age    = floor($edad);
            
        }else{  $age = 18; }


        $info   =   [
            'name'      =>  strtoupper($_replace->deleteTilde($params['re_name'])),
            'birthday'  =>  $birth,
            'phone'     =>  $params['re_phone'],
            'client'    =>  $refClient['client'],
            'referred'  =>  $params['re_client'],
            'ss'        =>  ($ss <> "") ? $ss['id'] : '',
            'id'        =>  ($id <> "") ? $id['id'] : '',
            'country'   =>  $params['re_country'],
            'type'      =>  $params['re_type'],
            'user'      =>  $app['session']->get('id')
        ];

        $lead   =   [
            'client_id'     =>  $refClient['client'],
            'referred_id'   =>  $params['re_client'],
            'c_name'        =>  strtoupper($_replace->deleteTilde($params['re_name'])),
            'c_birthday'    =>  $birth,
            'age'           =>  $age,
            'c_phone'       =>  $params['re_phone'],
            'c_phone_ow'    =>  strtoupper($_replace->deleteTilde($params['re_name'])),
            'c_country'     =>  $params['re_country'],
            'ss_id'         =>  ($ss <> "" ) ? $ss['id'] : '',
            'ss_exp'        =>  ($ss <> "" ) ? $_replace->ChangeDate($params['re_ss_exp']) : '',
            'ident_id'      =>  ($id <> "" ) ? $id['id'] : '',
            'ident_exp'     =>  ($id <> "" ) ? $_replace->ChangeDate($params['re_ident_exp']) : '',
            'user'          =>  $app['session']->get('id')
        ];

        $ref    =   Referred::SaveReferred($info);

        if($ref == true)
        {
            $lead = Leads::SaveLeadRef($lead);

            if($app['session']->get('departament') == "28")
            {
                
                $query  =   'INSERT INTO cp_service_vzla_int_tmp(client_id, int_res_id, pho_res_id, int_com_id, pho_com_id, ref_id, ref_list_id, sup_id, sup_ope_id, payment_id, service_id, type_order_id, origen_id, owen_id, assit_id, created_at) VALUES ("'.$refClient['client'].'", "'.Internet::GetFirst("1", "4")['id'].'", "'.Phone::GetFirst("1", "4")['id'].'", "'.Internet::GetFirst("2", "4")['id'].'", "'.Phone::GetFirst("2", "4")['id'].'", "0", "0", "1", "'.$app['session']->get('id').'", "'.Payment::GetFirst("6", "4")['id'].'", "1", "1", "13", "'.$app['session']->get('id').'", "'.$app['session']->get('id').'", NOW())';

                $serv   =   DBSmart::DataExecute($query);
        
            }
            
            $info = array('client' => $refClient['client'], 'channel' => 'Referido', 'message' => 'Referido  - Creado - ID - '.$refClient['client'].' - Solicitado Por - '.$app['session']->get('username').' - Correctamente', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  true,
                'title'     =>  'Success',
                'content'   =>  'Data Stored Correctly'
            ));

        }else{
            $info = array('client' => $refClient['client'], 'channel' => 'Referido', 'message' => 'Referido - Error - ID - '.$refClient['client'].' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false,
                'title'     =>  'Error',
                'content'   =>  'Verificar que el cliente fue creado en sistema, en caso contrario Contactar al administrador de Sistemas.'
            ));
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////


    public function ReferredFormInc(Application $app, Request $request)
    {

        $_replace   =   new Config();

        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['re_cl_in'] <> "")
        {
            $lead   =   Leads::GetLeadById($params['re_cl_in']);

            if($lead <> false)
            {
                $iData  =   [
                    'orig'      =>  (isset($params['re_cl']))       ?   strtoupper($_replace->deleteTilde($params['re_cl']))    : "" ,
                    'name'      =>  (isset($lead['name']))          ?   strtoupper($_replace->deleteTilde($lead['name']))       : "" ,
                    'phone'     =>  (isset($lead['phone_main']))    ?   strtoupper($_replace->deleteTilde($lead['phone_main'])) : "" ,
                    'birth'     =>  ($lead['birthday'] <> "")       ?   $lead['birthday'] : (date("Y-m-d",strtotime(date("Y-m-d")."- 18 year"))),
                    'age'       =>  $_replace->AgeClient( ($lead['age'] <> "")  ?   $lead['age'] : (date("Y-m-d",strtotime(date("Y-m-d")."- 18 year")))),
                    'c_ph'      =>  (isset($lead['phone_main']))    ?   strtoupper($_replace->deleteTilde( $lead['phone_main'] ) ) : "",
                    'c_pho'     =>  (isset($lead['name']))          ?   strtoupper($_replace->deleteTilde($lead['name'])) : "",
                    'country'   =>  $lead['country'],
                    'type'      =>  "2",
                    'user'      =>  $app['session']->get('id'),
                    'client'    =>  $lead['client'],
                ];

                if($iData['age'] < 18)
                {
                    return $app->json(array(
                        'status'    =>  false,
                        'title'     =>  'Error',
                        'content'   =>  'El Cliente Referido no cumple con la mayoria de edad, por favor verificar la informacion.'
                    ));
                }

                $referred    =   Referred::SaveReferredLead($iData);

                if($referred <> false)
                {
                    $info = array('client' => $params['re_cl'], 'channel' => 'Referido', 'message' => 'Referido  - Asignado Satisfactoriamente - Solicitado Por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  true,
                        'title'     =>  'success',
                        'content'   =>  'Informacion almacenada correctamente'
                    ));

                }else{
                    $info = array('client' => $params['re_cl'], 'channel' => 'Referido', 'message' => 'Referido  - Error al intentar asignar referido - Solicitado Por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  false,
                        'title'     =>  'error',
                        'content'   =>  'Error al actualizar referido, intente nuevamente'
                    ));

                }

            }else{
                return $app->json(array(
                    'status'    =>  false,
                    'title'     =>  'Error',
                    'content'   =>  'El id no se encuentra registrado en sistema, intente nuevamente.'
                ));      
            }

        }else{
            return $app->json(array(
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'Debe Introducir un id valido'
            ));

        }

        var_dump($params);
        exit;

        // $refClient      =   Leads::LastClientID();     

        // if(($params['re_ss'] <> "") || ($params['re_ss_exp'] <> ""))
        // {

        //     $infoU      =   New UnCypher();

        //     $_replace   =   new Config();
        //     $exp        =   $_replace->ChangeDate($params['re_ss_exp']);

        //     $cData = [
        //         'ss'    =>  strtoupper($params['re_ss']),
        //         'exp'   =>  $exp
        //     ];

        //     $iData  =   [
        //         'client'    =>  $refClient['client'],
        //         'operator'  =>  $app['session']->get('id'),
        //         'type'      =>  'SS',
        //         'type_l'    =>  'SS',
        //         'country'   =>  $params['re_country_ss'],
        //         'exp'       =>  $exp,
        //         'type_d'    =>  $params['re_type_ss']
        //     ];

        //     $SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

        //     if($SaveCypher == true)
        //     {
        //         $info = array('client' => $refClient['client'], 'channel' => 'Seguro Social', 'message' => 'Seguro Social  - Creado - Procesado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

        //         $app['datalogger']->RecordLogger($info);

        //     }else{

        //         $info = array('client' => $refClient['client'], 'channel' => 'Seguro Social - Error', 'message' => 'Seguro Social - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

        //         $app['datalogger']->RecordLogger($info);

        //         return $app->json($SaveCypher);

        //     }       

        // }else{
        //     $ss = "";
        // }

        // if(($params['re_ident'] <> "") || ($params['re_ident_exp'] <> ""))
        // {

        //     $_replace   =   new Config();
        //     $exp        =   $_replace->ChangeDate($params['re_ident_exp']);

        //     $cData = [
        //         'id'    =>  strtoupper($params['re_ident']),
        //         'exp'   =>  $exp
        //     ];

        //     $iData  =   [
        //         'client'    =>  $refClient['client'],
        //         'operator'  =>  $app['session']->get('id'),
        //         'type'      =>  'ID',
        //         'type_l'    =>  'ID',
        //         'country'   =>  $params['re_country_ident'],
        //         'exp'       =>  $exp,
        //         'type_d'    =>  $params['re_type_ident']
        //     ];

        //     $SaveCypher =  $infoU->CypherDataInfo($cData, $iData);

        //     if($SaveCypher == true)
        //     {
        //         $info = array('client' => $refClient['client'], 'channel' => 'Identificacion', 'message' => 'Identificacion - Creada - Procesada Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

        //         $app['datalogger']->RecordLogger($info);

        //     }else{
        //         $info = array('client' => $refClient['client'], 'channel' => 'Identificacion - Error', 'message' => 'Identificacion - No Creada - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

        //         $app['datalogger']->RecordLogger($info);

        //         return $app->json($SaveCypher);
        //     } 

        // }else{
        //     $id = "";
        // }

        // $ss     =   CypherData::GetCypherClientType($refClient['client'], 'SS');
        // $id     =   CypherData::GetCypherClientType($refClient['client'], 'ID');

        // $birth  =   ($params['re_birthday'] <> '' ) ? $_replace->ChangeDate($params['re_birthday']) : '';

        // if($birth <> '')
        // {
        //     $tiempo = strtotime($birth); 
        //     $ahora  = time(); 
        //     $edad   = ($ahora-$tiempo)/(60*60*24*365.25);  
        //     $age    = floor($edad);
            
        // }else{  $age = 0; }


        // $info   =   [
        //     'name'      =>  strtoupper($_replace->deleteTilde($params['re_name'])),
        //     'birthday'  =>  $birth,
        //     'phone'     =>  $params['re_phone'],
        //     'client'    =>  $refClient['client'],
        //     'referred'  =>  $params['re_client'],
        //     'ss'        =>  ($ss <> "") ? $ss['id'] : '',
        //     'id'        =>  ($id <> "") ? $id['id'] : '',
        //     'country'   =>  $params['re_country'],
        //     'type'      =>  $params['re_type'],
        //     'user'      =>  $app['session']->get('id')
        // ];

        // $lead   =   [
        //     'client_id'     =>  $refClient['client'],
        //     'referred_id'   =>  $params['re_client'],
        //     'c_name'        =>  strtoupper($_replace->deleteTilde($params['re_name'])),
        //     'c_birthday'    =>  $birth,
        //     'age'           =>  $age,
        //     'c_phone'       =>  $params['re_phone'],
        //     'c_phone_ow'    =>  strtoupper($_replace->deleteTilde($params['re_name'])),
        //     'c_country'     =>  $params['re_country'],
        //     'ss_id'         =>  ($ss <> "" ) ? $ss['id'] : '',
        //     'ss_exp'        =>  ($ss <> "" ) ? $_replace->ChangeDate($params['re_ss_exp']) : '',
        //     'ident_id'      =>  ($id <> "" ) ? $id['id'] : '',
        //     'ident_exp'     =>  ($id <> "" ) ? $_replace->ChangeDate($params['re_ident_exp']) : '',
        //     'user'          =>  $app['session']->get('id')
        // ];

        // $ref    =   Referred::SaveReferred($info);

        // if($ref == true)
        // {
        //     $lead = Leads::SaveLeadRef($lead);
            
        //     $info = array('client' => $refClient['client'], 'channel' => 'Referido', 'message' => 'Referido  - Creado - ID - '.$refClient['client'].' - Solicitado Por - '.$app['session']->get('username').' - Correctamente', 'time' => $app['date'], 'username' => $app['session']->get('username'));

        //     $app['datalogger']->RecordLogger($info);

        //     return $app->json(array(
        //         'status'    =>  true,
        //         'title'     =>  'Success',
        //         'content'   =>  'Data Stored Correctly'
        //     ));

        // }else{
        //     $info = array('client' => $refClient['client'], 'channel' => 'Referido', 'message' => 'Referido - Error - ID - '.$refClient['client'].' - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

        //     $app['datalogger']->RecordLogger($info);

        //     return $app->json(array(
        //         'status'    => false,
        //         'title'     =>  'Error',
        //         'content'   =>  'Verificar que el cliente fue creado en sistema, en caso contrario Contactar al administrador de Sistemas.'
        //     ));
        // }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

}
