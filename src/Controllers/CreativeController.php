<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use App\Models\Auth;
use App\Models\Status;
use App\Models\Creative;
use App\Models\Campaign;
use App\Models\Country;
use App\Models\Service;
use App\Models\Objetive;

use App\Lib\Config;

/**
* 
*/
class CreativeController extends BaseController
{
    public static function index(Application $app)
    {
        return $app['twig']->render('creative/index.html.twig',array(
            'sidebar'   =>  true,
            'status'    =>  Status::GetStatus(),
            'imgs'      =>  Creative::GetImgs(),
            'countrys'  =>  Country::GetCountry(),
            'services'  =>  Service::GetService(),
            'campaigns' =>  Campaign::GetCampaign(),
            'creatives' =>  Creative::GetImgs(),
            'objetives' =>  Objetive::GetObjetive(),
            'status'    =>  Status::GetStatus()
        ));
    }

    public static function LoadCamp(Application $app, Request $request)
    {
        $country    =   Country::GetCountry();
        $html       =   '';

        foreach ($country as $k => $val) 
        {
            $camp[$k]   =  Campaign::GetCampaignActive($val['id']);
        }

        $counPR     =   ['name'     =>  'PUERTO RICO',     'sig' => 'PR'];
        $counUS     =   ['name'     =>  'ESTADOS UNIDOS',  'sig' => 'US'];
        $counVE     =   ['name'     =>  'VENEZUELA',       'sig' => 'VE'];

        return $app->json(array(
            'status'   =>   true, 
            'htmlPR'     =>   Campaign::HtmlCampaign($camp[0], $counPR),
            'htmlUS'     =>   Campaign::HtmlCampaign($camp[1], $counUS),
            'htmlVE'     =>   Campaign::HtmlCampaign($camp[3], $counVE),
        ));

    }

    public static function LoadImgCampaign(Application $app, Request $request)
    {

        $country    =   Country::GetCountry();
        $html       =   '';

        foreach ($country as $k => $val) 
        {
            $camp[$k]   =  Campaign::GetCampaignActive($val['id']);
        }

        $counPR     =   ['name'     =>  'PUERTO RICO',     'sig' => 'PR'];
        $counUS     =   ['name'     =>  'ESTADOS UNIDOS',  'sig' => 'US'];
        $counVE     =   ['name'     =>  'VENEZUELA',       'sig' => 'VE'];

        return $app->json(array(
            'status'   =>   true, 
            'htmlPR'     =>   Campaign::HtmlCampaign($camp[0], $counPR),
            'htmlUS'     =>   Campaign::HtmlCampaign($camp[1], $counUS),
            'htmlVE'     =>   Campaign::HtmlCampaign($camp[3], $counVE),
        ));
        return $app['twig']->render('landing/campaign_pr.html.twig',array(
            'sidebar'   =>  true
        ));
    }


    public static function Image(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'img'       => Creative::GetImgId($request->get('id'))
        ));
    
    }

    public static function Upload(Application $app, Request $request)
    {
        
        $info   =   array(
            'name'      =>  $_POST['name'],
            'user'      =>  $app['session']->get('id'),
            'status'    =>  $_POST['status_id'],
            'country'   =>  $_POST['country_id_cr']
        );

        $filename       =   $_FILES["file"]["name"];
        $img            =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$filename);

        if($img == true)
        {
            $fileAdd    =   [
                'name'  =>  $filename,
                'size'  =>  $_FILES["file"]['size'],
                'type'  =>  $_FILES["file"]["type"],
                'dir'   =>  "assets/upload/".$filename
            ];

            $file    =   Creative::SaveImg($info, $fileAdd);

            if($file == true)
            {
                
                $info = array('client' => '', 'channel' => 'Creative Imagen', 'message' => 'Creacion de Imagen - '.strtoupper($_POST['name']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'creative'
                ));

            }else{
                
                $info = array('client' => '', 'channel' => 'Creative Imagen', 'message' => 'Error al Crear Imagen - '.$filename.' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Error al Intentar Crear la Imagen, intente nuevamente y si el problema persiste contacte a su administrador de sistemas.", true)
                ));  
            }


        }else{
            $info = array('client' => '', 'channel' => 'Creative Imagen', 'message' => 'Error al Cargar Imagen - '.$filename.' - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("Imposible Cargar el archivo al servidor, intente nuevamente y si el problema persiste contacte a su administrador de sistemas.", true)
            ));   
        }
    }

    public static function Edit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'img'       => Creative::GetImgIds($request->get('id'))
        ));
    
    }

    public static function EditSave(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        $infos = [
            'id'        =>  $params['id_cr'],
            'name'      =>  $params['name_cr'],
            'country'   =>  $params['country_id_cr'],
            'status'    =>  $params['status_id_cr'],
            'update'    =>  $app['session']->get('id')
        ];

        $inf = Creative::EditSave($infos);

        if($inf == true)
        { 
            $info = array('client' => '', 'channel' => 'Creative Edit', 'message' => 'Actualizacion de Imagen - '.strtoupper($params['name_cr']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   => true, 
                'web'      => 'creative'
            ));

        }else{

            $info = array('client' => '', 'channel' => 'Creative Edit', 'message' => 'Error Al Intentar Editar Imagen - '.strtoupper($params['name_cr']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
            )); 
        }
   
    }
}