<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\User;
use App\Models\Approvals;
use App\Models\ApprovalsScore;
use App\Models\ApprovalsScoreTmp;
use App\Models\Service;
use App\Models\Score;
use App\Models\ServiceTvPr;
use App\Models\ServiceSecPr;
use App\Models\ServiceIntPr;
use App\Models\ServiceIntVZ;
use App\Models\Notes;
use App\Models\Users;
use App\Models\Departament;
use App\Models\Emails;
use App\Models\Drives;
use App\Models\Coordination;

use App\Lib\Config;
use App\Lib\UnCypher;
use App\Lib\SendMail;
use App\Lib\DBSmart;

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
* 
*/
class ApprovalsController extends BaseController
{

////////////////////////////// Index View //////////////////////////////

	public static function index(Application $app)
	{
		return $app['twig']->render('approvals/index.html.twig',array(
            'sidebar'              =>  true
        ));
	
	}

////////////////////////////// View Load //////////////////////////////

	public static function Load(Application $app, Request $request)
	{

		$info =	Approvals::Load();

        return $app->json(array(
            'status'    => 	true, 
            'pending'	=> 	$info['pending'],
            'processed'	=>	$info['processed'],
            'cancel'	=>	$info['cancel'],
            'errorTV'	=>	$info['errortv']
        ));

	}

////////////////////////////// View Load //////////////////////////////

	public static function Closures(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  ($params['dateIniC'] <> '') ? $_replace->ChangeDate($params['dateIniC']) : '',
            'dateEnd'   =>  ($params['dateEndC'] <> '') ? $_replace->ChangeDate($params['dateEndC']) : ''
        ];

		$info =	Approvals::Closures($dates);

        return $app->json(array(
            'status'    => 	($info <> false) ? true : false, 
            'info'		=>	$info
        ));

	}

	public static function ViewClosures(Application $app, Request $request)
	{
		$info 	=	Approvals::ViewClosures();

        return $app->json(array(
            'status'    => 	($info == "") ? false : true, 
            'html'		=>	($info == "") ? '' : $info, 
        ));

	}

	public static function ClosuresDate(Application $app, Request $request)
	{

		$_replace 	= 	new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  ($params['dateIniC'] <> '') ? $_replace->ChangeDate($params['dateIniC']) : '',
            'dateEnd'   =>  ($params['dateEndC'] <> '') ? $_replace->ChangeDate($params['dateEndC']) : ''
        ];

		$info 	=	Approvals::ViewClosuresSearch($dates);

        return $app->json(array(
            'status'    => 	($info == "") ? false : true, 
            'html'		=>	($info == "") ? '' : $info, 
        ));

	}

////////////////////////////// Closures //////////////////////////////

	public static function LoadClosures(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        if($request->get('str') <> null)
        {
	        $dates   =   [
	            'dateIni'   =>  ($params['dateIniC'] <> '') ? $_replace->ChangeDate($params['dateIniC']) : '',
	            'dateEnd'   =>  ($params['dateEndC'] <> '') ? $_replace->ChangeDate($params['dateEndC']) : ''
	        ];
        
        }else{
        	$dates 	=	false;
        
        }

		$TV 		=	Approvals::LoadClosures(2, $dates);
		$Inter 		=	Approvals::LoadClosures(4, $dates);
		$IntVE 		=	Approvals::LoadClosures(6, $dates);
		$iData 		=	Approvals::LoadClosuresData($dates);

		// var_dump($iData);
		// exit;

		return $app->json(array(
			'status'	=>	true,
			'TV'		=>	$TV,
			'TVPRHtml' 	=>	Approvals::ClosuresNewTable($TV['sta']),
			'INTPR'		=>	$Inter,
			'INTPRHtml' =>	Approvals::ClosuresNewTable($Inter['sta']),
			'INTVE'		=>	$IntVE,
			'INTVEHtml' =>	Approvals::ClosuresNewTable($IntVE['sta']),
			'info'		=>	$iData
		));

	}

	public static function NewViewClosures(Application $app, Request $request)
	{
		$info 	=	Approvals::ViewClosures();

        return $app->json(array(
            'status'    => 	($info == "") ? false : true, 
            'html'		=>	($info == "") ? '' : $info, 
        ));

	}

	public static function NewClosuresDate(Application $app, Request $request)
	{

		$_replace 	= 	new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  ($params['dateIniC'] <> '') ? $_replace->ChangeDate($params['dateIniC']) : '',
            'dateEnd'   =>  ($params['dateEndC'] <> '') ? $_replace->ChangeDate($params['dateEndC']) : ''
        ];

		$info 	=	Approvals::ViewClosuresSearch($dates);

        return $app->json(array(
            'status'    => 	($info == "") ? false : true, 
            'html'		=>	($info == "") ? '' : $info, 
        ));

	}

//////////////////////////// Search Date ////////////////////////////

	public static function SearchDate(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];
        
        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            $info 	=	Approvals::LoadByDate("1", $dates);
        }else{
			$info 	=	Approvals::LoadByDate("0", $dates);
        }

        return $app->json(array(
            'status'    => 	true, 
            'pending'	=> 	$info['pending'],
            'processed'	=>	$info['processed'],
            'cancel'	=>	$info['cancel'],
            'errorTV'	=>	$info['errortv']
        ));
	
	}

//////////////////////////// View Pending ////////////////////////////

	public static function ViewAproPend(Application $app, Request $request)
	{

		$cIData 	=	Approvals::GetServById($request->get('id'));
		
		$ticket 	=	$cIData['ticket'];

		$score      =   ApprovalsScore::GetScoreByTicket($cIData['ticket']);

		$type 		=	Approvals::DetecTicket(substr($ticket,0,2), $ticket);

		$info 		=	Approvals::GetInfo($type, $ticket);

		$client 	=	Approvals::ObtData($info['client'], $ticket);

		$referred 	=	Approvals::ObtData($info['referred'], $ticket);

		$htmlCli 	=	($client['status'] == true)  	? Approvals::InfoViewClient($client['data'], 'client', $score) 		: 	'';

		$htmlRef 	=	($referred['status'] == true) 	? Approvals::InfoViewClient($referred['data'], 'referred', $score)	:	'';

		switch ($type) {
			case 'TVPR':
				$htmlServ 	=	Approvals::InfoServTvPR($info['service']);
				break;
			case 'SECPR':
				$htmlServ 	=	Approvals::InfoServSecPR($info['service']);
				break;
			case 'INTPR':
				$htmlServ 	=	Approvals::InfoServIntPR($info['service'], $info['client']);
				break;
			case 'IVE':
				$htmlServ 	=	Approvals::InfoServIntVZ($info['service'], $info['client']);
				break;
		}

		return $app->json(array(
            'status'    => 	true,
            'client'	=>	$htmlCli,
            'referred' 	=> 	$htmlRef,
            'service'  	=> 	$htmlServ
        ));	
	
	}

//////////////////////////// View Notes ////////////////////////////

	public static function Notes(Application $app, Request $request)
	{
		
		$service 	=	Approvals::GetServById($request->get('id'));

		$notes 		= 	Notes::GetNotesHtml($service['client']);

        return $app->json(array(

            'status'    => 	true, 
            'notes'		=> 	$notes,
            'client'	=>	$service['client']
        ));
	
	}

//////////////////////////// Save Notes ////////////////////////////

	public static function NotesSave(Application $app, Request $request)
	{

		if(!$_POST['notes'] == "")
		{
			if(!$_FILES['file']['name'] == "")
			{
				$filename       =   $_FILES["file"]["name"];
        		$img            =   move_uploaded_file($_FILES["file"]["tmp_name"], $app['upload'].$filename);

        		if($img == true)
        		{
		        	$fileAdd    =   [
		                'name'  =>  $filename,
		                'size'  =>  $_FILES["file"]['size'],
		                'type'  =>  $_FILES["file"]["type"],
		                'dir'   =>  "assets/upload/".$filename
		            ];

        			$notes = Notes::SaveNoteCF($_POST, $fileAdd, $app['session']->get('id'));

					if($notes == true)
	                { 
	                    $info = array('client' => $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Satisfactoria - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	                    $app['datalogger']->RecordLogger($info);

	                    return $app->json(array(
	                        'status'   	=> true, 
	                        'notes'		=>	Notes::GetNotesHtml($_POST['client_id_n'])
	                    ));

	                }else{

	                    $info = array('client' =>  $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	                    $app['datalogger']->RecordLogger($info);

	                    return $app->json(array(
	                        'status'    => false, 
	                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
	                    ));
	                
	                }

        		}else{
        			return $app->json(array(
	                'status'    => false, 
                	'html'      => Auth::Notification("Error al intentar guardar archivo adjunto, intente nuevamente si el problema persiste contacte a su administrador de sistemas.", true)
            		)); 
        		}
			}else{
				$notes = Notes::SaveNoteSF($_POST, $app['session']->get('id'));

				if($notes == true)
                { 
                    $info = array('client' => $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Satisfactoria - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'   	=> true, 
                        'notes'		=>	Notes::GetNotesHtml($_POST['client_id_n'])
                    ));

                }else{

                    $info = array('client' =>  $_POST['client_id_n'], 'channel' => 'Note New', 'message' => 'Creacion de Nota - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                    ));
                }
			}
		}else{

			return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("No se pudo procesar la solicitud, debe dejar una nota valida", true)
            )); 

		}	
	
	}

///////////////////////// View Qualification ///////////////////////

	public static function QualAproPend(Application $app, Request $request)
	{
		$info 		=	Approvals::GetServById($request->get('id'));

		$cli 		=	ApprovalsScore::GetLastScore($info);

		$dataClient	=	($cli <> false) ? $cli : false;

		$service 	=	$info['service'];

	    return $app->json(array(
            'status'    => 	true, 
            'qual'		=> 	Approvals::QualHtml($dataClient, $service, $request->get('id'))
        ));

	}

///////////////////////// Process Qualification ///////////////////////

	public static function QualAproProc(Application $app, Request $request)
	{
		$info 		=	Approvals::GetServById($request->get('id'));

		$data 		=	ApprovalsScoreTmp::GetLastScoreTicket($info['ticket']);

		$serv 		=	Service::ServiceById($info['service'])['name'];

		if($data['status'] <> false)
		{
			
			if($data == true)
			{
				$iData 	=	[
					'ticket'		=>	$data['ticket'],
					'client'		=>	$data['client'],
					'appro'			=>	$info['id'],
					'service'		=>	$info['service'],
					'serviceF'		=>	Service::ServiceById($info['service'])['name'],
					'score'			=>	$data['score'],
					'scoreF'		=>	Score::GetScoreId($data['score'])['name'],
					'transaction'	=>	$data['transactions'],
					'value'			=>	$data['value'],
					'fico'			=>	$data['fico'],
					'ope_a'			=>	$data['operator'],
					'operatorA'		=>	User::GetUserById($data['operator'])['username'],
					'ope_c'			=>	$info['created'],
					'operatorC'		=>	User::GetUserById($info['created'])['username'],
				];

				$order 		=	Approvals::SaveQualification($iData);

				if($order <> false)
				{
					if(($iData['score'] == "1") || ($iData['score'] == "2") || ($iData['score'] == "11") || ($iData['score'] == "22") || ($iData['score'] == "29"))
					{
						$coord 	=	Coordination::SaveCoord($iData);

					}else{	

						$coord = false;	
					}

					$info2 = array('client' => $info['client'], 'channel' => 'Aprobaciones - '.$serv, 'message' => 'Aprobaciones Score - Ticket - '.$info['ticket'].' - Solicitado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

			        $app['datalogger']->RecordLogger($info2);

					$iTicket  	=   Drives::ReqMacAprProScoInfTicket($iData['ticket']);
					$iDriver 	=	Drives::ReqMacAprUpdProScoInfTicket($iTicket);


					return $app->json(array(
		                'status'    => 	true,
		                'title'		=>	'Success',
		                'content'   => 	'Data Stored Correctly'
		            ));

				}else{

					$info2 = array('client' => $info['client'], 'channel' => 'Aprobaciones - '.$serv, 'message' => 'Error - Aprobaciones Score - Ticket - '.$info['ticket'].' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

			        $app['datalogger']->RecordLogger($info2);

		    		return $app->json(array(
		                'status'    => 	false,
		                'title'		=>	'Error',
		                'content'   => 	'Ticket - '.$info['ticket'].' - No tiene una aprobación previa registrada, si es un error notifique al administrador del sistema.'
		            ));

				}

	        }else{

		        $info2 = array('client' => $info['client'], 'channel' => 'Aprobaciones - '.$serv, 'message' => 'Error - Aprobaciones Score - Ticket - '.$info['ticket'].' - Solicitado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

		        $app['datalogger']->RecordLogger($info2);

	    		return $app->json(array(
	                'status'    => 	false,
	                'title'		=>	'Error',
	                'content'   => 	'Ticket - '.$info['ticket'].' - No tiene una aprobación previa registrada, si es un error notifique al administrador del sistema.'
	            ));
	        
	        }

		}else{

			$info2 = array('client' => $info['client'], 'channel' => 'Aprobaciones - '.$serv, 'message' => 'Error - Aprobaciones - Ticket - '.$info['ticket'].' - Sin previo registro de Score - Solicitado Por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info2);

    		return $app->json(array(
                'status'    => 	false,
                'title'		=>	'Error',
                'content'   => 	'Ticket - '.$info['ticket'].' - No tiene una aprobación previa registrada, si es un error notifique al administrador del sistema.'
            ));
		}

	}

///////////////////////// Copy Qualification ///////////////////////

	public static function CopyScore(Application $app, Request $request)
	{
		$serv 	=	ApprovalsScore::GetScoreById($request->get('id')); 

		$info 	=	Approvals::where('id', $request->get('cl'))->find_one();

		if($serv)
		{
			$cData	=	[
				'id'			=>	$info->id,
				'appro'			=>	$info->id,
				'ticket'		=>	$info->ticket,
				'client'		=>	$info->client_id,
				'service'		=>	$serv['service'],
				'score'			=>	$serv['score'],
				'transaction'	=>	$serv['transaction'],
				'value'			=>	$serv['value'],
				'fico'			=>	$serv['fico'],
				'operator'		=>	$app['session']->get('id'),
				'ope_a'			=>	$app['session']->get('id'),
				'created'		=>	$serv['created']
			];

			$tscore 	=	ApprovalsScoreTmp::SaveScoreCopy($cData);

			if($tscore)
			{
				$score 	=	ApprovalsScore::SaveScoreCopy($cData);
				
				if($score)
				{
					$sco 	=	Approvals::where('id', $cData['id'])->find_one();

					$sco->score_id 		=	$score;
					$sco->score 		=	$cData['score'];
					$sco->transactions 	=	$cData['transaction'];
					$sco->value 		=	$cData['value'];
					$sco->fico 	 		=	$cData['fico'];
					$sco->status_id		=	"2";
					$sco->cancel 		=	"1";
					$sco->finish_by		=	$cData['operator'];
					$sco->finish_at		=	date('Y-m-d H:i:s', time());
					$fin 				=	($sco->save()) ? true : false;

					if($fin)
					{
						$iTicket  	=   Drives::ReqMacAprProScoInfTicket($cData['ticket']);
						$iDriver 	=	Drives::ReqMacAprUpdProScoInfTicket($iTicket);

						$info = array('client' => $cData['client'], 'channel' => 'Approvals Copy', 'message' => 'Approvals Copy - Satisfactory - Ticket - '.$cData['ticket'].' - Performed by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

		                $app['datalogger']->RecordLogger($info);


		                return $app->json(array(
			                'status'    => 	true,
			                'title'		=>	'Success',
			                'content'   => 	'Data Stored Correctly'
		                ));

					}else{

						$info = array('client' =>  $cData['client'], 'channel' => 'Approvals Copy', 'message' => 'Approvals Copy - Error - Performed by - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));


	                	$app['datalogger']->RecordLogger($info);

		                return $app->json(array(
		                    'status'   	=> 	false,
		                    'title'		=>	'Error',
		                    'content'	=>	'Approvals Cancel - Error - Ticket - '.$cData['ticket'].' - Execute by - '.$app['session']->get('username').''

		                ));
					}

				}else{

					return $app->json(array(
		                'status'    => 	false,
		                'title'		=>	'Error',
		                'content'   => 	'Error while trying to copy previous approval, try again, if the problem persists contact your system administrator.'
	            	));
				}

			}
			else
			{
				return $app->json(array(
	                'status'    => 	false,
	                'title'		=>	'Error',
	                'content'   => 	'Error while trying to copy previous approval, try again, if the problem persists contact your system administrator.'
            	));

			}

		}else{
    		return $app->json(array(
                'status'    => 	false,
                'title'		=>	'Error',
                'content'   => 	'Error while trying to copy previous approval, try again, if the problem persists contact your system administrator.'
            ));
		}
	
	}

//////////////////////////// View Cancel ////////////////////////////

	public static function ViewCancel(Application $app, Request $request)
	{

		$service 	=	Approvals::GetServById($request->get('id')); 

		$servTV 	=	$servSec 	=	$servInt 	=	$html 	=	"";

		if($service <> false)
		{
			$html = Approvals::ViewCancel($service['service'], $request->get('id'));
		}else{
			return $app->json(array(
	            'status'    => 	false	        
	        ));
		}

			return $app->json(array(
	            'status'    => 	true,
	            'html'		=>	$html
	        ));

	}

	public static function ProcCancel(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('str'), $params);

        $select 	=	(isset($params['cancel_sel'])) ? true : false;

        if(isset($params['cancel_sel']))
        {

        	if($params['cancel_note'] <> "")
        	{

				$date		= 	date('Y-m-d H:i:s', time());

		        $serv		=	Approvals::GetServById($params['cancel_id']);

		        $cData 		=	[
		        	'id'		=> $params['cancel_id'],
		        	'ticket' 	=> $serv['ticket'], 
		        	'client' 	=> $serv['client'], 
		        	'service' 	=> $serv['service'], 
		        	'notes'		=> $params['cancel_note'],
		        	'operator' 	=> $app['session']->get('id'), 
		        	'depart'	=> $app['session']->get('departament'),
		        	'date' 		=> $date
		    	];

		        $cancel 	=	Approvals::ApprovalsCancel($params, $cData);

		        if($cancel == true)
		        {
			        $iTicket    =   Drives::ReqMacAprCanScoInfTicket($cData['ticket']);
			        $iDriver    =   Drives::ReqMacAprUpdCanScoInfTicket($iTicket);

					$info = array('client' => $cData['client'], 'channel' => 'Cancelacion de Aprobacion', 'message' => 'Cancelacion de Aprobacion - Satisfactoria - Ticket - '.$cData['ticket'].' - Por - '.$app['session']->get('username').' - Realizada Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	                $app['datalogger']->RecordLogger($info);

	                return $app->json(array(
		                'status'    => 	true,
		                'title'		=>	'Success',
		                'content'   => 	'Cancelacion Procesada Correctamente'
	                ));

		        }else{

                    $info = array('client' =>  $cData['client'], 'channel' => 'Cancelacion de Aprobacion', 'message' => 'Cancelacion de Aprobacion - Error -Por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));


	                $app['datalogger']->RecordLogger($info);

	                return $app->json(array(
	                    'status'   	=> 	false,
	                    'title'		=>	'Error',
	                    'content'	=>	'Cancelacion de Aprobacion - Error - Ticket - '.$cData['ticket'].' - Realizado Por - '.$app['session']->get('username').''

	                ));
		        }

        	}else{

        		return $app->json(array(
                	'status'    => 	false,
                	'title'		=>	'Error',
                	'content'   => 	'Debe Dejar una nota validad para procesar la solicitud.'
            	));

        	}
        
        }else{

        	return $app->json(array(
                'status'    => 	false,
                'title'		=>	'Error',
                'content'   => 	'Debe seleccionar una opcion validad para procesar la solicitud.'
            ));
        }

	}

///////////////////////// Save Qualifications Tmp /////////////////////////

	public static function SaveQualTmp(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('str'), $params);

		$service 	=	Approvals::GetServById($params['qual_id']);

		$serv 		=	Service::ServiceById($service['service'])['name'];

		$apro 		=	Approvals::SaveQualTmp($params, $service, $app['session']->get('id'));

		if($apro == true)
		{
			$iTicket    =   Drives::ReqMacAprPreScoInfTicket($service['ticket']);
        	$iDriver    =   Drives::ReqMacAprUpdPreScoInfTicket($iTicket);

	        $info = array('client' => $service['client'], 'channel' => 'Approvals Previous Service - '.$serv, 'message' => 'Approvals Previous Score - Ticket - '.$service['ticket'].' - Execute by - '.$app['session']->get('username').' - Done Correctly.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

			$app['datalogger']->RecordLogger($info);

			if(($params['qual_score'] == "4") || ($params['qual_score'] == "5") || ($params['qual_score'] == "6") || ($params['qual_score'] == "7") || ($params['qual_score'] == "42"))
			{
				$query =	'SELECT t1.ticket, t1.client_id, t2.name, t2.phone_main, (SELECT username FROM users WHERE id = t1.created_by) AS sale, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = t1.created_by) ) AS departament, t1.created_at, (SELECT name FROM data_score WHERE id = (SELECT score FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) ) AS score, (SELECT username FROM users WHERE id = (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) AS operador, (SELECT name FROM data_departament WHERE id = (SELECT departament_id FROM users WHERE id = (SELECT operator_id FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1)) ) AS OpeDep, (SELECT created_at FROM cp_appro_score_tmp WHERE ticket = t1.ticket ORDER BY id DESC LIMIT 1) AS DateProc, (SELECT email FROM users WHERE id = t1.created_by) AS email FROM cp_appro_serv AS t1 INNER JOIN cp_leads AS t2 ON (t1.client_id = t2.client_id) AND t1.ticket = "'.$service['ticket'].'"';
				$iInfo 	=	DBSmart::DBQuery($query);

				$html="";
				$html.='<h3 style="margin-top: 20px; text-align: center;">INFORMACION DE TICKET </h3>';
				$html.='<table class="head-wrap" style="background: #ecf8ff">';
					$html.='<tbody>';
						$html.='<tr>';
							$html.='<td><strong>ID</strong></td>';
							$html.='<td>'.$iInfo['client_id'].'</td>';
							$html.='<td><strong>CLIENTE</strong></td>';
							$html.='<td>'.$iInfo['name'].'</td>';
							$html.='<td><strong>TELEFONO</strong></td>';
							$html.='<td>'.$iInfo['phone_main'].'</td>';
						$html.='</tr>';
						$html.='<tr>';
							$html.='<td><strong>FECHA</strong></td>';
							$html.='<td>'.$iInfo['created_at'].'</td>';
							$html.='<td><strong>VENDEDOR</strong></td>';
							$html.='<td>'.$iInfo['sale'].'</td>';
							$html.='<td><strong>DEPARTAMENTO</strong></td>';
							$html.='<td>'.$iInfo['departament'].'</td>';
						$html.='</tr>';
						$html.='<tr>';
							$html.='<td><strong>SCORE</strong></td>';
							$html.='<td>'.$iInfo['score'].'</td>';
							$html.='<td><strong>OPERADOR</strong></td>';
							$html.='<td>'.$iInfo['operador'].'</td>';
							$html.='<td><strong>FECHA</strong></td>';
							$html.='<td>'.$iInfo['DateProc'].'</td>';
						$html.='</tr>';
					$html.='</tbody>';
				$html.='</table>';
				$html.='<br/>';
				$html.='<br/>';

				$mail   	=   new SendMail();

				$email  	=   $mail->SendEMailScoreError($iInfo, $html);
			
			}

			return $app->json(array(
                'status'    => 	true,
                'title'		=>	'Success',
                'content'   => 	'Data Stored Correctly'
            ));

        }else{
	        $info = array('client' => $params['c_client'], 'channel' => 'Approvals Previous Service - '.$serv, 'message' => 'Error creating Approvals Previous Score - Ticket - '.$service['ticket'].' - Made By - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

	        $app['datalogger']->RecordLogger($info);

    		return $app->json(array(
                'status'    => false,
                'title'		=>	'Error',
                'content'   => 	'Error while trying to create preapproval, if error persists contact your system administrator.'
            ));
        }

	}

///////////////////////// Save Qualifications Tmp /////////////////////////


}