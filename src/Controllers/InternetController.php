<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Departament;
use App\Models\Role;
use App\Models\Status;
use App\Models\Team;
use App\Models\Did;
use App\Models\Town;
use App\Models\ZipCode;
use App\Models\Country;
use App\Models\House;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Provider;
use App\Models\Package;
use App\Models\Dvr;
use App\Models\Camera;
use App\Models\Internet;
use App\Models\Phone;
use App\Models\PhoneProvider;
use App\Models\InternetType;
use App\Models\Objection;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class InternetController extends BaseController
{
    
//////////////////////////////////////////////////////////////////////////////////////////////////

    public function index(Application $app, Request $request)
    {
        return $app['twig']->render('internet/index.html.twig',array(
            'sidebar'       =>  true,
            'status'        =>  Status::GetStatus(),
            'countrys'      =>  Country::GetCountry(),
            'internets'     =>  Internet::GetInternet(),
            'phones'        =>  Phone::GetPhone(),
            'providers'     =>  PhoneProvider::GetProvider(),          
            'inttypes'      =>  InternetType::GetInternetType()
        ));
    }

    public static function InternetEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'internet'      => Internet::GetInternetById($request->get('id'))
        ));
    
    }

    public function InternetForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_int'] == 'new')
        {
            $saveDep = Internet::SaveInternet($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Internet New', 'message' => 'Creacion de Paquete - '.strtoupper($params['name_int']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Internet New', 'message' => 'Error Al Intentar Crear Paquete - '.strtoupper($params['name_int']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }        
        }
        elseif($params['type_int'] == 'edit')
        {

            $saveDep = Internet::SaveInternet($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Internet Edit', 'message' => 'Actualizacion de Paquete - '.strtoupper($params['name_int']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Internet Edit', 'message' => 'Error Al Intentar Actualizar informacion del Paquete - '.strtoupper($params['name_int']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        }
    
    }

}