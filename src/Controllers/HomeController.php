<?php
namespace App\Controllers;
require '../vendor/autoload.php';

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
* 
*/
class HomeController extends BaseController
{
	public static function index(Application $app)
	{

		return $app['twig']->render('index.html.twig',array(
            'sidebar'              =>  true
        ));
	}
}