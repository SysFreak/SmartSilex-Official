<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Ceiling;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class CeilingController extends BaseController
{
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static function CeilingEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'ceiling'      => Ceiling::GetCeilingById($request->get('id'))
        ));
    
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function CeilingForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_ce'] == 'new')
        {
            $saveDep = Ceiling::SaveCeiling($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Ceiling New', 'message' => 'Creacion de Paquete - '.strtoupper($params['name_ce']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'house'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Ceiling New', 'message' => 'Error Al Intentar Crear Paquete - '.strtoupper($params['name_ce']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, <strong>Datos Duplicados</strong>, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_ce'] == 'edit')
        {

            $saveDep = Ceiling::SaveCeiling($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Ceiling Edit', 'message' => 'Actualizacion de Ceiling - '.strtoupper($params['name_ce']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'house'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Ceiling Edit', 'message' => 'Error Al Intentar Actualizar informacion del Paquete - '.strtoupper($params['name_ce']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        }
    
    }

}