<?php

namespace App\Controllers;

use App\Models\User;
use App\Models\UserRol;
use App\Models\UserPermit;
use App\Models\Auth;
use App\Models\Departament;
use App\Models\Status;
use App\Models\Team;
use App\Models\Role;
use App\Lib\MultiLogger;
use App\Lib\DBSmart;
use App\Lib\SendMail;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
* 
*/
session_start();

class AuthController extends BaseController
{

    public function index(Application $app)
    {
        // $email = New SendMail();
        // ddd($email->mail());
        ddd($app['mail']->Mail());
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function LoginForm(Application $app)
    {
        return $app['twig']->render('login.html.twig', array());
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function ForgotForm(Application $app)
    {
        return $app['twig']->render('forgot.html.twig', array());
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function PassForm(Application $app)
    {
        return $app['twig']->render('newpassword.html.twig', array());
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function Login(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        $getUser = Auth::GetUser($params);

        if($getUser And $getUser['status_id'] == 1)
        {
            $valPass =  Auth::ValPass(md5($params['password']), $getUser['password']);

            if($valPass == true)
            {

                $_SESSION['id'] = $getUser['id'];

                $app['session']->set('id',          $getUser['id']);
                $app['session']->set('email',       $getUser['email']);
                $app['session']->set('name',        $getUser['name']);
                $app['session']->set('username',    $getUser['username']);
                $app['session']->set('departament', $getUser['departament_id']);
                $app['session']->set('role',        $getUser['role_id']);
                $app['session']->set('status',      $getUser['status_id']);
                $app['session']->set('team',        $getUser['team_id']);
                $app['session']->set('roles',       UserRol::GetRolsByRol($getUser['role_id']));
                $app['session']->set('permits',     UserPermit::GetPermitByUser($getUser['id']));

                $info = array('client' => '', 'channel' => 'LogIn', 'message' => 'Login Usuario - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));                

                $app['datalogger']->RecordLogger($info);

                $web    =   ($getUser['departament_id'] == "1")     ?   'SheetVE'  :   'Coordinations';

                return $app->json(array(
                    'status'   => true, 
                    'web'      => $web
                ));

            }else{

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Password No Coinciden", true)
                ));
            }

        }else{

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("Usuario no existe o se encuentra Inhabilitado", true)
            ));
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function logout(Application $app)
    {

        $info = array('client' => '', 'channel' => 'LogOut', 'message' => 'Logout Usuario - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));                
        $app['datalogger']->RecordLogger($info);

        session_destroy();

        $app['session_manager']->destroy();
        return $this->redirectToRoute('login');

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function ResetPass(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        $getUser = Auth::GetUser($params);

        if($getUser And (strtolower($getUser['email']) == $params['email']) )
        {
            return $app->json(array(
                'status'    => true, 
                'web'       => 'passrecovery/'.$getUser['username']
            ));

        }else{

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("Datos No Coinciden, Intente Nuevamente", true)
            ));

        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function PassRecovery(Application $app, Request $request)
    {
        return $app['twig']->render('newpassword.html.twig', array('username' => $request->get('id')));
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public function RecoveryPass(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        $username   =   strtoupper($params['username']);

        if($params['password'] <> $params['cpassword'])
        {

            return $app->json(array(
                'status'    => false, 
                'html'      => Auth::Notification("Password No Coinciden, Intente Nuevamente", true)
            ));  

        }else{

            $query      =   'SELECT username FROM users WHERE username = "'.$username.'"';
            $res        =   DBSmart::DBQuery($query);

            if($res <> false)
            {
                $query  =   'UPDATE users SET password = "'.md5($params['password']).'" WHERE username = "'.$username.'"';
                $result =   DBSmart::DataExecute($query);

                if($result <> false)
                {
                    $info = array('client' => '', 'channel' => 'PassChange', 'message' => 'Password de usuario - '.$params['username'].' - Modificado Correctamente.', 'time' => $app['date'], 'username' => $params['username']);                
                    $app['datalogger']->RecordLogger($info);


                    return $app->json(array(
                        'status'    => true, 
                        'web'       => '../login'
                    ));
                }else{

                    $info = array('client' => '', 'channel' => 'PassChange', 'message' => 'Intento de Modificacion de Password de usuario - '.$params['username'].' - Incorrecto.', 'time' => $app['date'], 'username' => $params['username']);  

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    => false, 
                        'html'      => Auth::Notification("Error al actualizar password, si persiste el problema contactar al administrador de sistema", true)
                    ));
                }


            }else{
                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("Usuario no encontrado", true)
                ));
            }
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////
}