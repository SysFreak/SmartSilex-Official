<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use SplFileObject;
use SplTempFileObject;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;

use App\Models\User;
use App\Models\UserPermit;
use App\Models\Transactions;

use App\Lib\DBMikroVE;
use App\Lib\DBForms;
use App\Lib\DBSmart;
use App\Lib\ApiMWVz;
use App\Lib\Config;

use Carbon\Carbon;


/**
* 
*/

class TransactionsController extends BaseController	
{

///////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function view (Application $app, Request $request)
    {
        return $app['twig']->render('transactions/index.html.twig',array(
            'sidebar'   =>  true,
            'data'      =>  Transactions::Load()
        ));
   
    }
	
///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Search (Application $app, Request $request)
    {
        if( !(strlen(trim($request->get('value'))) == 0) )
        {
            $cli    =   trim($request->get('value'));

            $query  =   'SELECT mw, name, estado FROM cp_mw_clients WHERE name LIKE "%'.$cli.'%" OR phone_prin LIKE "%'.$cli.'%" OR username LIKE "%'.$cli.'%" order by id ASC';
            $res    =   DBSmart::DBQueryAll($query);

            if($res)
            {
                return $app->json(array(
                    'status'    =>  true, 
                    'html'      =>  Transactions::HtmlSearch($res),
                )); 

            }else{
                return $app->json(array(
                    'status'    =>  false, 
                    'message'   =>  'Busqueda sin resultados.'
                ));                
            }
        
        }else{
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  'Necesita enviar un valor exacto.'
            ));
        
        }

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function SearchClient (Application $app, Request $request)
    {
        $cli    =   trim($request->get('value'));

        $query  =   'SELECT mw id, UPPER(name) if002, estado if001, phone_prin if003, phone_alt if004, UPPER(address) if005, emails if006, username if007, password if008 FROM cp_mw_clients WHERE mw = "'.$request->get('value').'"';
        $res    =   DBSmart::DBQuery($query);

        $facturas   =   'SELECT f_id, total FROM cp_mw_invoices WHERE mw = "'.$request->get('value').'" AND estado = "No pagado" ORDER BY id ASC';
        $fac    =   DBSmart::DBQueryAll($facturas);

        $fact   =   false;
        $c      =   $total  =   0;

        if($fac <> false) 
        { 
            foreach ($fac as $f => $fa)  
            { 
                $fact[$f]   =   ['id' => $fa['f_id'], 'total' => $fa['f_id']." - Total: $".$fa['total'] ]; 
                $c          =   $c + 1;
                $total      =   $total + $fa['total'];
            } 
        }

        return $app->json(array(
            'status'    =>  true, 
            'client'    =>  $res,
            'bills'     =>  $fact,
            'c'         =>  $c,
            'total'     =>  $total
        ));

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function SearchBill (Application $app, Request $request)
    {
        $cli    =   trim($request->get('value'));

        $query  =   'SELECT f_id, total, vencimiento FROM cp_mw_invoices WHERE f_id = "'.$cli.'"';
        $res    =   DBSmart::DBQuery($query);

        $bill   =   Transactions::GetBills($cli);
        $alert  =   "";
        $total  =   0;

        if($res <> false)
        {
            return $app->json(array(
                'status'    =>  true, 
                'bill'      =>  $res,
                'reg'       =>  $bill,
                'alert'     =>  ($bill['status'] <> false) ? Transactions::HtmlBills($bill) : '',
                'pending'   =>  $bill['pending']
            ));
        }else{
            return $app->json(array(
                'status'    =>  false, 
                'bill'      =>  $res,
                'reg'       =>  false,
                'alert'     =>  '',
                'pending'   =>  '0'
            ));
        }

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Payment (Application $app, Request $request)
    {
        $_replace   =   new Config();
        
        parse_str($request->get('general'),     $gen);
        parse_str($request->get('complet'),     $com);
        parse_str($request->get('partial'),     $par);
        parse_str($request->get('transfer'),    $tra);
        parse_str($request->get('movil'),       $mov);
        parse_str($request->get('paypal'),      $pay);
        parse_str($request->get('zelle'),       $zel);

        foreach ($gen as $g => $ge) 
        {
            if($gen[$g] == "")
            {
                return $app->json(array(
                    'status'    =>  false, 
                    'message'   =>  "Debe seleccionar todas las opciones disponibles. Selecciones."
                ));
            }
        }

        $op     =   false;

        switch ($gen['rp002']) 
        {
            case '1':   $op     =   Transactions::CheckFiels($gen['rp001'], $gen['rp002'], $com, true);     break;
            case '2':   $op     =   Transactions::CheckFiels($gen['rp001'], $gen['rp002'], $com, $tra);     break;
            case '3':   $op     =   Transactions::CheckFiels($gen['rp001'], $gen['rp002'], $com, $mov);     break;
            case '4':   $op     =   Transactions::CheckFiels($gen['rp001'], $gen['rp002'], $com, $pay);     break;
            case '5':   $op     =   Transactions::CheckFiels($gen['rp001'], $gen['rp002'], $com, $zel);     break;
        }

        if($op == false)
        {
            return $app->json(array(
                'status'    =>  false, 
                'message'   =>  "Debe seleccionar todas las opciones disponibles. Informacion."
            ));
        }

        $bills  =   ['001' => $gen['rp004'], '002' => $gen['rp001'], '003' => $gen['rp002'], '004' => $gen['rp005'], '005' => $gen['rp006'], '006' => $gen['rp007'] ];

        switch ($gen['rp002']) 
        {
            case '1':   $sql     =   Transactions::GetSQL($gen['rp001'], $gen['rp002'], $com, true, $bills, $app['session']->get('id'));    break;
            case '2':   $sql     =   Transactions::GetSQL($gen['rp001'], $gen['rp002'], $com, $tra, $bills, $app['session']->get('id'));    break;
            case '3':   $sql     =   Transactions::GetSQL($gen['rp001'], $gen['rp002'], $com, $mov, $bills, $app['session']->get('id'));    break;
            case '4':   $sql     =   Transactions::GetSQL($gen['rp001'], $gen['rp002'], $com, $pay, $bills, $app['session']->get('id'));    break;
            case '5':   $sql     =   Transactions::GetSQL($gen['rp001'], $gen['rp002'], $com, $zel, $bills, $app['session']->get('id'));    break;
        }

        $iBill      =   Transactions::InsFielsInvo($gen['rp004'], ['type' => $sql['typeMW'], 'amount' => $com['com003'] ] );

        $insert     =   DBSmart::DataExecute($sql['query']);

        $bill       =   Transactions::GetBills($gen['rp004']);

        $message    =   ($insert <> false) 
                        ?   'Factura #'.$gen['rp004'].' - Agregar Pago: Success - Usuario: '.$app['session']->get('username').' - Fecha: '.date("Y-m-d H:m:s").''
                        :   'Factura #'.$gen['rp004'].' - Agregar Pago: Error - Usuario: '.$app['session']->get('username').' - Fecha: '.date("Y-m-d H:m:s").'';
        $iLog       =   [
            'f_id'      =>  $gen['rp004'],
            'message'   =>  $message,
            'operator'  =>  $app['session']->get('username'),
        ];

        $log        =   Transactions::RegLog($iLog);
    
        return $app->json(array(
            'status'    =>  (($insert == true) ? true : false), 
            'alert'     =>  Transactions::HtmlBills($bill),
            'state'     =>  0,
            'amount'    =>  $bill['pending'],
            'bill'      =>  Transactions::GetListBills($gen['rp009']),
            'id'        =>  DBSmart::DBQuery('SELECT id FROM cp_mw_payments WHERE bills = "'.$gen['rp004'].'" ORDER BY id DESC LIMIT 1')['id']

        ));
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function PreProcess(Application $app, Request $request)
    {
        
        $_replace   =   new Config();
        
        parse_str($request->get('general'),     $general);

        $bill   =   Transactions::GetBills($general['rp004']);

        if($bill['pending'] == 0)
        {
            $process    =   Transactions::ProcessBill($general);

            $message    =   ($process <> false) 
                            ?   'Factura #'.$general['rp004'].' - Pago de Factura: Success - Usuario: '.$app['session']->get('username').' - Fecha: '.date("Y-m-d H:m:s").''
                            :   'Factura #'.$general['rp004'].' - Pago de Factura: Error - Usuario: '.$app['session']->get('username').' - Fecha: '.date("Y-m-d H:m:s").'';
            $iLog       =   [
                'f_id'      =>  $general['rp004'],
                'message'   =>  $message,
                'operator'  =>  $app['session']->get('username'),
            ];

            $log        =   Transactions::RegLog($iLog);

            $bill   =   Transactions::GetBills($general['rp004']);

            return $app->json(array(
                'status'    =>  $process['status'],
                'message'   =>  $process['message'],
                'pending'   =>  $bill['pending'],
                'id'        =>  $general['rp004']
            ));

        }
        elseif( ($bill['pending'] < 0 ) || ($bill['pending'] > 0 ) )
        {
            return $app->json(array(
                'status'    =>  false,
                'pending'   =>  $bill['pending']
            ));
        }

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function PostProcess(Application $app, Request $request)
    {
        
        $_replace   =   new Config();
        
        parse_str($request->get('general'),     $gen);

        $bill   =   Transactions::ProcessBill($gen);

        if($bill['status'] <> false)
        {
            return $app->json(array(
                'status'    =>  true,
                'bills'     =>  Transactions::GetListBills($gen['rp009']),
                'id'        =>  $bill['bill']
            ));

        }

        return $app->json(array(
            'status'    =>  false,
            'message'   =>  $bill['message'],
            'id'        =>  $bill['bill']
        ));
        
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function LoadReports(Application $app, Request $request)
    {
        $_replace   =   new Config();
        
        parse_str($request->get('date'), $date);
        
        $dat = ( ($date['da001'] <> "") && ($date['da002'] <> "") ) ? ['in' => $date['da001'], 'en' => $date['da002'] ] : ['in' => date("Y-m-d"), 'en' => date("Y-m-d")];

        $iData  =   [
            'data'      =>  Transactions::Load(),
            'created'   =>  'created_at BETWEEN "'.$dat['in'].' 00:00:00" AND "'.$dat['en'].' 23:59:59"',
            'operator'  =>  'operator_id = "'.$app['session']->get('id').'"',
            'status'    =>  '(status_id = "0" OR status_id = "1")'
        ];

        $sta    =   Transactions::OpeStatics($iData);

        return $app->json(array(
            'status'    =>  true,
            'data'      =>  $sta['info'],
            'statics'   =>  Transactions::HtmlStatics($sta['statics'])
        ));

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ViewReport(Application $app, Request $request)
    {
        $view   =   Transactions::ViewReport($request->get('id'));

        return $app->json(array(
            'status'    =>  true,
            'html'      =>  $view['html'],
            'sta'       =>  $view['status']
        ));
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function SuspendedReport(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    =>  Transactions::SuspendedReport($request->get('id'))
        ));
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ReceiptPayment(Application $app, Request $request)
    {

        return $app->json(array(
            'status'    =>  true,
            'html'      =>  Transactions::ReceiptPayment($app['session']->get('id'))
        ));
    }    

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function AddItems(Application $app, Request $request)
    {
        $iItems =   DBSmart::DBQuery('SELECT * FROM data_mw_fact_items WHERE id = "'.$request->get('id').'"');

        return $app->json(array(
            'status'    =>  true, 
            'items'     =>  $iItems,
        ));
    }


///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function CreateBill(Application $app, Request $request)
    {
        $_replace   =   new Config();
        
        parse_str($request->get('str'), $params);

        $id      =   $params['mw_modal_id'];

        $info   =   DBSmart::DBQuery('SELECT * FROM cp_mw_invoices WHERE f_id = "'.$id.'"');
        
        $gen    =   ['rp004' => $info['mw']];

        if( isset($params['mw_modal_id']) )
        {
            $total  =   $c  =   0;

            for ($i=1; $i < 10 ; $i++) 
            { 
                if(isset($params['cant_'.$i]))
                {
                    $iItems[$c++] = [
                        'id'        =>  $id,
                        'content'   =>  DBSmart::DBQuery('SELECT content FROM data_mw_fact_items WHERE id = "'.$i.'"')['content'],
                        'cost'      =>  $params['cost_'.$i],
                        'cantidad'  =>  $params['cant_'.$i],
                        'iva'       =>  $params['iva_'.$i],
                        'total'     =>  $params['cost_'.$i]
                    ];

                    $total  =   $total + $params['total_'.$i];

                }           
            }

            if($id <> false)
            {
                foreach ($iItems as $it => $itm) 
                {
                    $fITems =   DBMikroVE::DataExecute('INSERT INTO facturaitems (idfactura, descripcion, cantidad, idalmacen, unidades, impuesto, block, impuesto911, clave_invoice, tipoitem, montodescuento) VALUES("'.$id.'", "'.$itm['content'].'", "'.$itm['total'].'", "0", "'.$itm['cantidad'].'", "'.$itm['iva'].'", "0", "0.00", "", "0", "0.00")');

                    $sItems =   DBSmart::DataExecute('INSERT INTO cp_mw_invoices_manual (mw, bills, descriptions, price, quantity, iva, total, operator_id,created_at)  VALUES("'.$info['mw'].'", "'.$id.'", "'.$itm['content'].'", "'.$itm['cost'].'", "'.$itm['cantidad'].'", "'.$itm['iva'].'", "'.$total.'", "'.$app['session']->get('id').'", "'.date("Y-m-d H:m:s").'")');
                }

                $fact  =   DBMikroVE::DBQueryAll('SELECT * FROM facturaitems WHERE idfactura = "'.$id.'" ORDER BY id ASC');

                $tSin   =   ['iva' => 0, 'cost' => 0];
                $iva    =   $tTotal     =   0;

                foreach ($fact as $f => $fa) 
                {   
                    
                    $iva    =   ($fa['impuesto'] <> 0) ? (($fa['cantidad'] * $fa['unidades']) * ($fa['impuesto']/100) ) : 0;

                    $tSin   =   [
                        'iva'   =>  $iva + $tSin['iva'],
                        'cost'  =>  ($fa['cantidad'] * $fa['unidades']) + $tSin['cost'],
                    ];

                    $iva =  0;

                }

                $tTotal     =   ($tSin['cost'] + $tSin['iva']) + $tTotal;


                $upd    =   DBMikroVE::DataExecute('UPDATE facturas SET total = "'.$tTotal.'", iva_igv = "'.$tSin['iva'].'", sub_total = "'.$tSin['cost'].'" WHERE id = "'.$id.'"'); 

                $updS   =   DBSmart::DataExecute('UPDATE cp_mw_invoices SET total = "'.$tTotal.'", iva_igv = "'.$tSin['iva'].'", sub_total = "'.$tSin['cost'].'" WHERE f_id = "'.$id.'"'); 

                $inst   =   DBSmart::DBQuery('SELECT * FROM cp_mw_payments WHERE bills = "'.$id.'"');

                $up     =   DBSmart::DataExecute('UPDATE cp_mw_payments SET pay_amount = "'.($inst['pay_amount'] + $total).'", total_df = "'.($inst['total_df'] + $total).'" WHERE bills = "'.$id.'" ');

                $facturas   =   'SELECT f_id, total FROM cp_mw_invoices WHERE mw = "'.$info['mw'].'" AND estado = "No pagado" ORDER BY id ASC';
                $fac    =   DBSmart::DBQueryAll($facturas);

                $fact   =   false;
                $c      =   $total  =   0;

                if($fac <> false) 
                { 
                    foreach ($fac as $f => $fa)  
                    { 
                        $fact[$f]   =   ['id' => $fa['f_id'], 'total' => $fa['f_id']." - Total: $".$fa['total'] ]; 
                        $c          =   $c + 1;
                        $total      =   $total + $fa['total'];
                    } 
                }

                return $app->json(array(
                    'status'    =>  true,
                    'bills'     =>  $fact,
                ));

            }else{
                return $app->json(array(
                    'status'    =>  false 
                )); 

            }

        }else{
            return $app->json(array(
                'status'    =>  false 
            )); 
        }


    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function ImportPayment(Application $app, Request $request)
    {
        $id     =   $request->get('id');
        $iInfo  =   DBSmart::DBQuery('SELECT * FROM cp_mw_invoices WHERE f_id = "'.$id.'"');

        $con    =   "COMPENSACION POR PAGO EN DIVISAS";
        $por    =   0.03;

        $total  =   round( (($iInfo['sub_total'] + $iInfo['iva_igv']) * $por),2);

        $fITems =   DBMikroVE::DataExecute('INSERT INTO facturaitems (idfactura, descripcion, cantidad, idalmacen, unidades, impuesto, block, impuesto911, clave_invoice, tipoitem, montodescuento) VALUES("'.$id.'", "'.$con.'", "'.$total.'", "0", "1", "0", "0", "0.00", "", "0", "0.00")');

        $tTotal =   ( ($iInfo['sub_total'] + $iInfo['iva_igv']) + $total);

        $sTotal =   ($iInfo['sub_total'] + $total);

        $upd    =   DBMikroVE::DataExecute('UPDATE facturas SET total = "'.$tTotal.'", sub_total = "'.$sTotal.'" WHERE id = "'.$id.'"'); 

        $updS   =   DBSmart::DataExecute('UPDATE cp_mw_invoices SET total = "'.$tTotal.'", sub_total = "'.$sTotal.'" WHERE f_id = "'.$id.'"'); 

        $inst   =   DBSmart::DBQuery('SELECT * FROM cp_mw_payments WHERE bills = "'.$id.'"');

        $up     =   DBSmart::DataExecute('UPDATE cp_mw_payments SET pay_amount = "'.$tTotal.'", total_df = "'.($inst['total_df'] + $total).'" WHERE bills = "'.$id.'" ');

        $facturas   =   'SELECT f_id, total FROM cp_mw_invoices WHERE mw = "'.$iInfo['mw'].'" AND estado = "No pagado" ORDER BY id ASC';

        $fac    =   DBSmart::DBQueryAll($facturas);

        $fact   =   false;
        $c      =   $total  =   0;

        if($fac <> false) 
        { 
            foreach ($fac as $f => $fa)  
            { 
                $fact[$f]   =   ['id' => $fa['f_id'], 'total' => $fa['f_id']." - Total: $".$fa['total'] ]; 
                $c          =   $c + 1;
                $total      =   $total + $fa['total'];
            } 
        }

        return $app->json(array(
            'status'    =>  true,
            'bills'     =>  $fact,
        ));


    }


///////////////////////////////////////////////////////////////////////////////////////////////////////


    public static function PrintPayment(Application $app, Request $request)
    {
        $id     =   $request->get('id');

        $iInfo  =   DBSmart::DBQuery('SELECT status_id FROM cp_mw_payments WHERE bills = "'.$id.'"');
        // ddd($iInfo);

        if($iInfo <> false)
        {
            if($iInfo['status_id'] <> 0)
            {
                return $app->json(array(
                    'status'    =>  true 
                )); 
            }else{
                return $app->json(array(
                    'status'    =>  false,
                    'message'   =>  'Debe Procesar la factura para imprimir el recibo de pago'
                )); 
            }
        }else{
            return $app->json(array(
                'status'    =>  false,
                'message'   =>  'Debe procesar la factura antes de imprimir el recibo de pago'
            )); 
        }
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Calculator(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    =>  true,
            'data'      =>  DBForms::DBQuery('SELECT * FROM scrapers ORDER BY id DESC limit 1')
        )); 
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////

}