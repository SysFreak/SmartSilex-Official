<?php
namespace App\Controllers;

require '../vendor/autoload.php';

use Silex\Application;
use App\Lib\MultiLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use App\Models\Leads;
use App\Models\Marketing;
use App\Models\MarketingTmp;
use App\Models\MarketingAssigned;
use App\Models\OperatorAssigned;

use App\Models\Origen;
use App\Models\Medium;
use App\Models\Objetive;
use App\Models\Conversation;
use App\Models\Forms;
use App\Models\Service;
use App\Models\ServiceM;
use App\Models\Campaign;
use App\Models\Country;
use App\Models\Notes;
use App\Models\User;
use App\Models\Drives;

use App\Lib\Config;

/**
*	Class Marketing Controller
*/
class MarketingController extends BaseController
{

////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static function index(Application $app, Request $request)
	{
		
        return $app['twig']->render('marketing/index.html.twig',array(
            'sidebar'              =>  true,
        ));
	
	}

////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function Load(Application $app, Request $request)
    {
        $load   =   User::GetUserMkt('2', '5', '1');
        $static =   Marketing::DataOperator($load);
        $html   =   Marketing::LoadHtml($static);
        return  $app->json(array('html' => $html ));
    
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function BasicMKT(Application $app, Request $request)
	{
		
		return $app->json(array('html' => Marketing::BasicMKT(Country::GetCountry()) ));

	}

////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function SearchDate(Application $app, Request $request)
    {
        $params     =   [];
        parse_str($request->get('str'), $params);

        $_replace   =   new Config();

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];
     
        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            
            $load   =   User::GetUserMkt('2', '5', '1');
            $static =   Marketing::DataOperatorByDate($load, $dates, "0");

        }else{

            $load   =   User::GetUserMkt('2', '5', '1');
            $static =   Marketing::DataOperatorByDate($load, $dates, "1");

        }

        $html   =   Marketing::LoadHtml($static);
        return  $app->json(array('html' => $html ));
    
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function SearchMKT(Application $app, Request $request)
	{
		$params     =   [];

        parse_str($request->get('str'), $params);

        $leads 	=	Leads::GetLeadsByPhone($params['phone']);

    	$data 	=	[
            'origen'    =>  Origen::GetOrigenByCountryID($params['country_id']),
    		'medium'	=>	Medium::GetMediumByCountryID($params['country_id']),
    		'objetive'	=>	Objetive::GetObjectiveByCountryID($params['country_id']),
    		'post'		=>	Conversation::GetPostByCountryID($params['country_id']),
            'form'      =>  Forms::GetFormsByCountryID($params['country_id']),
    		'service'	=>	ServiceM::GetServiceMByCountryID($params['country_id']),
    		'campaign'	=>	Campaign::GetCampaignByCountryID($params['country_id']),
            'serviceI'  =>  Service::GetServiceByCountry($params['country_id']),
    		'user'		=>	User::GetUsers()
    	];

        if($leads['cont'] >= 2)
        {
        	$html 	=	Marketing::GetHtmlRegMultiple($leads['lead']);
        	return $app->json(array(
	            'status'    => 	true,
	            'html'		=>	$html
	        ));

        }else{
            $notes      =   Notes::GetNotesHtml($leads['lead'][0]['client']);
        	$html 		=	Marketing::GetHtmlRegSingle($leads['lead'][0], $data, $params['phone'], $notes, $params['country_id']);
        	return $app->json(array(
	            'status'    => 	true,
	            'html'		=>	$html
	        ));
        }	
	
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function NewMKT(Application $app, Request $request)
    { 
        $params     =   [];

        parse_str($request->get('str'), $params);

        $leads  =   Leads::CreateMKT($params, $app['session']->get('id'));

        if($leads == true)
        {
            $info = array('client' => $leads['client'], 'channel' => 'Creacion de Leads', 'message' => 'Creacion de Lead ID - '.$leads['client'].' - Realizado por - '.$app['session']->get('username').' - Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            if($params['lead_assig'] == 1)
            {
                $info   =   [
                    'client'    =>  $leads['client'],
                    'serviceI'  =>  $params['serviceI_id'],
                    'origen'    =>  $params['origen_id'],
                    'medium'    =>  $params['medium_id'],
                    'objective' =>  $params['objetive_id'],
                    'post'      =>  $params['post_id'],
                    'form'      =>  $params['form_id'],
                    'conv'      =>  $params['conv_id'],
                    'service'   =>  $params['service_id'],
                    'campaign'  =>  $params['campaign_id'],
                    'date'      =>  date('Y-m-d H:i:s', time())
                ];

                $users  =   [
                    'user'      =>  $app['session']->get('id'),
                    'operator'  =>  $params['operator_id']
                ];

                $assig  =   MarketingAssigned::SaveMktAssig($info,$users);

                if($assig)
                {
                    $assOpe =   OperatorAssigned::SaveAssig($info, $users, $assig);
                    
                    if($assOpe)
                    {
                        $info = array('client' => $info['client'], 'channel' => 'Asignacion de Leads', 'message' => 'Asignacion de Leads Realizada Correctamente - Cliente '.strtoupper($info['client']).' - al Operador - '.User::GetUserById($users['operator'])['username'].' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                        $app['datalogger']->RecordLogger($info);

                        $infoDB     =   Drives::LeadAssigInfo($info['client']);
                        $LeadBoom   =   Drives::InsertLeadAssig($infoDB);

                        return $app->json(array(
                            'status'    =>  'process',
                            'title'     =>  'Success',
                            'content'   =>  'Data Procesada Correctamente',
                            'color'     =>  "#739E73"

                        ));

                    }else{

                        $info = array('client' => $info['client'], 'channel' => 'Asignacion de Leads', 'message' => 'Error en Asignacion de Leads - Cliente '.strtoupper($info['client']).' - al Operador - '.User::GetUserById($users['operator'])['username'].' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                        $app['datalogger']->RecordLogger($info);

                        return $app->json(array(
                            'status'    =>  'process',
                            'title'     =>  'Error',
                            'content'   =>  'Error en Asignacion de Leads',
                            'color'     =>  '#C46A69'
                        ));
                    
                    }

                }else{
                    
                    $info = array('client' => $info['client'], 'channel' => 'Asignacion de Leads', 'message' => 'Error en Asignacion de Leads - Cliente '.strtoupper($info['client']).' - al Operador - '.User::GetUserById($users['operator'])['username'].' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                    $app['datalogger']->RecordLogger($info);

                    return $app->json(array(
                        'status'    =>  'process',
                        'title'     =>  'Error',
                        'content'   =>  'Error en Asignacion de Leads',
                        'color'     =>  '#C46A69'
                    ));
                }
            }else{

                $info = array('client' => $info['client'], 'channel' => 'Asignacion de Leads', 'message' => 'No se realizo asignacion de Leads - Cliente '.strtoupper($info['client']).' - Ejecutado por - '.$app['session']->get('username').'' , 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  'process',
                    'title'     =>  'Success',
                    'content'   =>  'Data Procesada Correctamente',
                    'color'     =>  "#739E73"
                )); 
            }         
            
        }else{
            
            $info = array('client' => '', 'channel' => 'Creacion de Leads', 'message' => 'Error en Creacion de Lead - Solicitado Por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'    =>  'process',
                'title'     =>  'Error',
                'content'   =>  'Error en Creacion de Leads',
                'color'     =>  '#C46A69'
            ));
        }  

    }

////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function UpdateMKT(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('str'), $params);

        $infoG = [
            'mkt_client'    =>  $params['m_client'],
            'mkt_origen'    =>  $params['origen_id'],
            'mkt_medium'    =>  $params['medium_id'],
            'mkt_form'      =>  $params['form_id'],
            'mkt_objective' =>  $params['objetive_id'],
            'mkt_conv'      =>  $params['conv_id'],
            'mkt_post'      =>  $params['post_id'],
            'mkt_service'   =>  $params['service_id'],
            'mkt_campaign'  =>  $params['campaign_id']
        ];

        if($params['lead_assig'] == 0)
        {
            $mktTmp = MarketingTmp::SaveDB($infoG, "UPDATE", $app['session']->get('id'));

            $mktUpd = Leads::SaveMkt($infoG);

            if($mktUpd)
            {
                $info = array('client' => $info['mkt_client'], 'channel' => 'Actualizacion de Leads', 'message' => 'Actualizacion de Leads - Cliente '.strtoupper($info['mkt_client']).' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  'process',
                    'title'     =>  'Success',
                    'content'   =>  'Data Procesada Correctamente',
                    'color'     =>  "#739E73"

                ));

            }else{

                $info = array('client' => $info['mkt_client'], 'channel' => 'Actualizacion de Leads', 'message' => 'Error en Actualizacion de Leads - Cliente '.strtoupper($info['mkt_client']).' - Ejecutado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  'process',
                    'title'     =>  'Error',
                    'content'   =>  'Error en Asignacion de Leads',
                    'color'     =>  '#C46A69'
                ));
            }

        }else{

            $info2 = [
                'client'    =>  $params['m_client'],
                'serviceI'  =>  $params['serviceI_id'],
                'origen'    =>  $params['origen_id'],
                'medium'    =>  $params['medium_id'],
                'form'      =>  $params['form_id'],
                'objective' =>  $params['objetive_id'],
                'conv'      =>  $params['conv_id'],
                'post'      =>  $params['post_id'],
                'service'   =>  $params['service_id'],
                'campaign'  =>  $params['campaign_id'],
                'operator'  =>  $params['operator_id'],
                'user'      =>  $app['session']->get('id'),
                'date'      =>  date('Y-m-d H:i:s', time())
            ];


            $mktTmp     = MarketingTmp::SaveDB($infoG, "UPDATE", $app['session']->get('id'));
            $mktUpd     = Leads::SaveMkt($infoG);
            $mktAsig    = MarketingAssigned::SaveMktAssig($info2, $info2);
            $mktOpe     = OperatorAssigned::SaveAssig($info2, $info2, $mktAsig);

            if($mktOpe)
            {
                $info = array('client' => $info2['client'], 'channel' => 'Asignacion de Leads', 'message' => 'Asignacion de Leads Realizada Correctamente - Cliente '.strtoupper($info2['client']).' - al Operador - '.User::GetUserById($info2['operator'])['username'].' - Ejecutado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                $infoDB     =   Drives::LeadAssigInfo($info2['client']);
                $LeadBoom   =   Drives::InsertLeadAssig($infoDB);

                return $app->json(array(
                    'status'    =>  'process',
                    'title'     =>  'Success',
                    'content'   =>  'Data Procesada Correctamente',
                    'color'     =>  "#739E73"

                ));

            }else{

                $info = array('client' => $info2['client'], 'channel' => 'Asignacion de Leads', 'message' => 'Error en Asignacion de Leads - Cliente '.strtoupper($info2['client']).' - al Operador - '.User::GetUserById($info2['operator'])['username'].' - Ejecutado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    =>  'process',
                    'title'     =>  'Error',
                    'content'   =>  'Error en Asignacion de Leads',
                    'color'     =>  '#C46A69'
                ));
            
            }

        }
    
    }

////////////////////////////////////////////////////////////////////////////////////////////////////

	public static function ViewClient(Application $app, Request $request)
	{
        $Leads = Leads::GetLeadByClient($request->get('id'));

        $lead  =    [
            'client'    =>  $Leads['client'],
            'name'      =>  $Leads['name'],
            'phone'     =>  $Leads['phone_main'],
            'country'   =>  $Leads['country'],
            'medium'    =>  $Leads['mkt_medium'],
            'objetive'  =>  $Leads['mkt_objective'],
            'origen'    =>  $Leads['mkt_origen'],
            'post'      =>  $Leads['mkt_post'],
            'form'      =>  $Leads['mkt_post'],
            'service'   =>  $Leads['mkt_service'],
            'campaign'  =>  $Leads['mkt_campaign']
        ];

        $data   =   [
            'origen'    =>  Origen::GetOrigenByCountryID($lead['country']),
            'medium'    =>  Medium::GetMediumByCountryID($lead['country']),
            'objetive'  =>  Objetive::GetObjectiveByCountryID($lead['country']),
            'post'      =>  Conversation::GetPostByCountryID($lead['country']),
            'form'      =>  Forms::GetFormsByCountryID($lead['country']),
            'service'   =>  ServiceM::GetServiceMByCountryID($lead['country']),
            'campaign'  =>  Campaign::GetCampaignByCountryID($lead['country']),
            'serviceI'  =>  Service::GetServiceByCountry($lead['country']),
            'user'      =>  User::GetUsers()
        ];

        $notes =  Notes::GetNotesHtml($lead['client']);

		$html  =  Marketing::GetHtmlRegSingle($lead, $data, "", $notes, $lead['country']);

        return $app->json(array(
            'status'    =>  true,
            'html'      =>  $html
        ));
	
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
}