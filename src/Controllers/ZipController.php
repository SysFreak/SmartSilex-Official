<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\Country;
use App\Models\ZipCode;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class ZipController extends BaseController
{
    
    public static function ZipEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'zip'       => ZipCode::GetZipById($request->get('id'))
        ));
    
    }

    public function ZipForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_z'] == 'new')
        {
            
            $saveDep = ZipCode::SaveZip($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Zip New', 'message' => 'Creacion de ZipCode - '.strtoupper($params['code_z']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'country'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Zip New', 'message' => 'Error Al Intentar Crear ZipCode - '.strtoupper($params['code_z']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_z'] == 'edit')
        {

            $saveDep = ZipCode::SaveZip($params);
            
            if($saveDep <> false)
            {
                $info = array('client' => '', 'channel' => 'Zip Edit', 'message' => 'Actualizacion de ZipCode - '.strtoupper($params['code_z']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'country'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Zip Edit', 'message' => 'Error Al Intentar Actualizar informacion del ZipCode - '.strtoupper($params['code_z']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
            
        }
    
    }

}