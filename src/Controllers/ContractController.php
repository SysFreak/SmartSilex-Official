<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\ContractPR;
use App\Models\ContractVE;
use App\Models\User;

use App\Lib\Config;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class ContractController extends BaseController
{
    
	public static function index(Application $app)
	{
		return $app['twig']->render('contracts/index.html.twig',array(
            'sidebar'              =>  true
        ));
	
	}

	public static function indexVE(Application $app)
	{
		return $app['twig']->render('contracts/indexve.html.twig',array(
            'sidebar'              =>  true
        ));
	
	}

    public static function LoadPR(Application $app, Request $request)
    {
        $info = ContractPR::LoadPR();

        return $app->json(array(
            'status'    =>  true, 
            'htmlRes'  	=>  $info['res'],
            'htmlCom'  	=>  $info['com'],
			'users'		=>	User::GetUsersHtmlContracts()
        ));
    
    }

	public static function ContractSearchLoadPR(Application $app, Request $request)
	{

		$_replace 	= 	new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        
        $dates   =   [
            'dateIni'   =>  ($params['pr_dateIni'] <> '') ? $_replace->ChangeDate($params['pr_dateIni']) : '',
            'dateEnd'   =>  ($params['pr_dateEnd'] <> '') ? $_replace->ChangeDate($params['pr_dateEnd']) : ''
        ];

        $dateIni 	=	strtotime($dates['dateIni']);
        $dateEnd 	=	strtotime($dates['dateEnd']);

        if($dateIni == $dateEnd)
        {
        	$info = ContractPR::SearchSingleLoadPR($dates['dateIni']);

        	return $app->json(array(
        	    'status'    =>  true, 
        	    'htmlRes'  	=>  $info['res'],
        	    'htmlCom'  	=>  $info['com'],

        	));
        }else{
             $info = ContractPR::SearchBetweenLoadPR($dates);

        	return $app->json(array(
        	    'status'    =>  true, 
        	    'htmlRes'  	=>  $info['res'],
        	    'htmlCom'  	=>  $info['com'],

        	));
        }
	
	}

    public static function LoadVE(Application $app, Request $request)
    {
        $info 	= 	ContractVE::LoadVE();
        $fibra 	=	ContractVE::LoadVEFib();
        $gpon 	=	ContractVE::LoadVEGpon();

        return $app->json(array(
            'status'    =>  true, 
            'htmlRes'  		=>  $info['res'],
            'htmlCom'  		=>  $info['com'],
            'htmlFibRes'  	=>  $fibra['fib'],
            'htmlGpon'  	=>  $gpon['gpon'],
            'users'			=>	User::GetUsersHtmlContracts()

        ));
    
    }

	public static function ContractSearchLoadVE(Application $app, Request $request)
	{

		$_replace 	= 	new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);
        
        $dates   =   [
            'dateIni'   =>  ($params['ve_dateIni'] <> '') ? $_replace->ChangeDate($params['ve_dateIni']) : '',
            'dateEnd'   =>  ($params['ve_dateEnd'] <> '') ? $_replace->ChangeDate($params['ve_dateEnd']) : ''
        ];

        $dateIni 	=	strtotime($dates['dateIni']);
        $dateEnd 	=	strtotime($dates['dateEnd']);

        if($dateIni == $dateEnd)
        {
        	$info 	=	ContractVE::SearchSingleLoadVE($dates['dateIni']);

        	return $app->json(array(
        	    'status'    =>  true, 
        	    'htmlRes'  		=>  $info['res'],
        	    'htmlCom'  		=>  $info['com'],
        	    'htmlResFib'  	=>  $info['fib'],

        	));
        }else{
             $info = ContractVE::SearchBetweenLoadVE($dates);

        	return $app->json(array(
        	    'status'    =>  true, 
        	    'htmlRes'  		=>  $info['res'],
        	    'htmlCom'  		=>  $info['com'],
        	    'htmlResFib'  	=>  $info['fib'],

        	));
        }
	
	}

	public static function ContractsClientPR(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		parse_str($request->get('str'), $params);

		$client 	=	$params['cli_id'];

		if($params['cli_type'] == 1)
		{
			$iCoord 	=	ContractPR::GetInfoCoordClient($client);

	        if($iCoord <> false)
	        {
	        	$iClient 	=	ContractPR::GetInfoClient($client);

				$iPack 		=	ContractPR::GetInfoPack($iCoord['ticket']);
		
				$iInfo 		=	['pre_date' => $iCoord['pre_date'], 'price' => $iPack['price'] ];

				$iPro 		=	ContractPR::GetProrateo($iInfo);

				$iTotal		=	[
					'name'		=>	$iClient['name'],
					'phone'		=>	$iClient['phone_main'],
					'ticket'	=>	$iCoord['ticket'],
					'pack'		=>	$iPack['pack'],
					'date'		=>	$_replace->ShowDate($iCoord['pre_date']),
					'price'		=>	$iPack['price'],
					'pro'		=>	substr(round($iPro,4), 0, 4),
					'month'		=>	$params['cli_mon'],
					'type'		=>	$iPack['type'],
					'telf'		=>	$iPack['telf'],
					'teltype'	=>	$iPack['teltype'],
					'telprice'	=>	$iPack['telprice'],
					'inst'		=>	'149.99',
					'sale'		=>	$iCoord['sale'],
					'sale_date'	=>	$_replace->ShowDate($iCoord['sale_date'])
				];

		      	return $app->json(array(
		            'status'    => 	true,
		            'iTotal'		=>	$iTotal
		        ));
	        
	        }else{
		      	return $app->json(array(
		            'status'    => 	false
		        ));
	        }
		}
		elseif($params['cli_type'] == 2)
		{
			return $app->json(array(
	            'status'    => 	'edit',
	            'html'		=>	"Edit Template"
	        ));

		}else{
			return $app->json(array(
	            'status'    => 	false
	        ));
	    }	
	
	}

	public static function ContractsClientVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		parse_str($request->get('str'), $params);

		$client 	=	$params['cli_ve_id'];

		if($params['cli_ve_type'] == 1)
		{
			$iCoord 	=	ContractVE::GetInfoCoordClient($client);

	        if($iCoord <> false)
	        {
	        	$iClient 	=	ContractVE::GetInfoClient($client);

				$iPack 		=	ContractVE::GetInfoPack($iCoord['ticket']);
		
				$iInfo 		=	['pre_date' => $iCoord['pre_date'], 'price' => $iPack['price'] ];

				$iPro 		=	ContractVE::GetProrateo($iInfo);

				$iTotal		=	[
					'name'		=>	$iClient['name'],
					'phone'		=>	$iClient['phone_main'],
					'ticket'	=>	$iCoord['ticket'],
					'pack'		=>	$iPack['pack'],
					'date'		=>	$_replace->ShowDate($iCoord['pre_date']),
					'price'		=>	$iPack['price'],
					'pro'		=>	substr(round($iPro,4), 0, 4),
					'month'		=>	$params['cli_ve_mon'],
					'type'		=>	$iPack['type'],
					'telf'		=>	$iPack['telf'],
					'teltype'	=>	$iPack['teltype'],
					'telprice'	=>	$iPack['telprice'],
					'inst'		=>	'149.99',
					'sale'		=>	$iCoord['sale'],
					'sale_date'	=>	$_replace->ShowDate($iCoord['sale_date'])
				];
		      	return $app->json(array(
		            'status'    => 	true,
		            'iTotal'		=>	$iTotal
		        ));
	        
	        }else{
		      	return $app->json(array(
		            'status'    => 	false
		        ));
	        }
		}
		elseif($params['cli_type'] == 2)
		{
			return $app->json(array(
	            'status'    => 	'edit',
	            'html'		=>	"Edit Template"
	        ));

		}else{
			return $app->json(array(
	            'status'    => 	false
	        ));
	    }	
	
	}

	public static function ContractsSubmitPR(Application $app, Request $request)
	{
		parse_str($request->get('str'), $params);

		$client 	=	$request->get('cli');

		$iClient 	=	ContractPR::GetInfoClient($client);

		$iCypher 	=	ContractPR::GetCypherClient($client);

		$iPro 		=	ContractPR::GetProrateo2($params);

		$eDate 		=	ContractPR::GetDateEnd($params['cli_date'], $params['cli_month']);

		$iData 	=	[
		  	'ticket' 		=>	$params['cli_ticket'],
			'client'		=>	$client,
			'name' 			=>	$params['cli_name'],
			'add_main'		=>	$iClient['add_main'],
			'add_postal'	=>	$iClient['add_postal'],
		  	'phone_main'	=>	$params['cli_phone'],
		  	'phone_alt'		=>	$iClient['phone_alt'],
		  	'email'			=>	$iClient['email_main'],
  			'nivel'			=>	$iClient['nivel'],
  			'techo'			=>	$iClient['techo'],
  			'house'			=>	$iClient['house'],
  			'coor_lati'		=>	$iClient['coor_lati'],
  			'coor_long'		=>	$iClient['coor_long'],
		  	'package' 		=>	$params['cli_package'],
		  	'type'	 		=>	$params['cli_type_c'],
		  	'pre_date'		=>	$params['cli_date'],
		  	'last_date'		=>	$eDate,
		  	'date_res'		=>	$iPro['dRes'],
		  	'price' 		=>	$params['cli_price'],
		  	'prorateo'		=>	($params['cli_pro'] <> 0 ) ? (substr(round($iPro['total'],4), 0, 4)) : "0.00", 
		  	'mes' 			=>	$params['cli_month'],
		  	'instalation'	=>	$params['cli_inst'],
		  	'telephone'		=>	$params['cli_telf'],
		  	'tel_type'		=>	$params['cli_telf_type'],
		  	'tel_price'		=>	$params['cli_telf_prec'],
		  	'cc_last'		=>	$iCypher['cc']['last'],
		  	'cc_exp'		=>	$iCypher['cc']['exp'],
		  	'ab_last'		=>	$iCypher['ab']['last'],
		  	'ab_exp'		=>	$iCypher['ab']['exp'],
		  	'ab_bank'		=>	$iCypher['ab']['bank'],
		  	'id_last'		=>	$iCypher['id']['last'],
		  	'id_exp'		=>	$iCypher['id']['exp'],
		  	'ss_last'		=>	$iCypher['ss']['last'],
		  	'ss_exp'		=>	$iCypher['ss']['exp'],
		  	'contract'		=>	date('ymdHis'),
		  	'country'		=>	"PUERTO RICO",
		  	'sale'			=>	$params['cli_sale'],
		  	'repre'			=>	$iClient['repre_legal'],
		  	'created_by'	=>	$app['session']->get('username')
  		];


  		$inInfo 	=	ContractPR::InsertInfoContract($iData);

		if($inInfo == true)
        { 
            $info = array('client' => $iData['client'], 'channel' => 'Contract', 'message' => 'Creacion de Contrato Tipo - '.$iData['type'].' - Numero '.$iData['contract'].' - Realizado por - '.$app['session']->get('username').' - Status Satisfactorio.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   	=> 	true, 
                'type'		=>	$iData['type'],
                'contract'	=>	$iData['contract']
            ));

        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Contract', 'message' => 'Creacion de Contrato Tipo - '.$iData['type'].' - Solicitado por - '.$app['session']->get('username').' - Status Error.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   	=> 	false
            ));
        
        }
	
	}

	public static function ContractsSubmitVE(Application $app, Request $request)
	{
		parse_str($request->get('str'), $params);

		$client 	=	$request->get('cli');

		$iClient 	=	ContractVE::GetInfoClient($client);
		
		$iCypher 	=	ContractVE::GetCypherClient($client);
		
		$iPro 		=	ContractVE::GetProrateo2($params);

		$eDate 		=	ContractVE::GetDateEnd($params['cli_ve_date'], $params['cli_ve_month']);

		$iData 	=	[
		  	'ticket' 		=>	$params['cli_ve_ticket'],
			'client'		=>	$client,
			'name' 			=>	$params['cli_ve_name'],
			'add_main'		=>	$iClient['add_main'],
			'add_postal'	=>	$iClient['add_postal'],
		  	'phone_main'	=>	$params['cli_ve_phone'],
		  	'phone_alt'		=>	$iClient['phone_alt'],
		  	'phone_other'	=>	$iClient['phone_other'],
		  	'email'			=>	$iClient['email_main'],
  			'nivel'			=>	$iClient['nivel'],
  			'techo'			=>	$iClient['techo'],
  			'house'			=>	$iClient['house'],
  			'coor_lati'		=>	$iClient['coor_lati'],
  			'coor_long'		=>	$iClient['coor_long'],
		  	'package' 		=>	$params['cli_ve_package'],
		  	'type'	 		=>	$params['cli_ve_type_c'],
		  	'pre_date'		=>	$params['cli_ve_date'],
		  	'last_date'		=>	$eDate,
		  	'date_res'		=>	$iPro['dRes'],
		  	'price' 		=>	$params['cli_ve_price'],
		  	'prorateo'		=>	($params['cli_ve_pro'] <> 0 ) ? $iPro['total'] : "0.00",
		  	'mes' 			=>	$params['cli_ve_month'],
		  	'instalation'	=>	$params['cli_ve_inst'],
		  	'telephone'		=>	$params['cli_ve_telf'],
		  	'tel_type'		=>	$params['cli_ve_telf_type'],
		  	'tel_price'		=>	$params['cli_ve_telf_prec'],
		  	'cc_last'		=>	$iCypher['cc']['last'],
		  	'cc_exp'		=>	$iCypher['cc']['exp'],
		  	'ab_last'		=>	$iCypher['ab']['last'],
		  	'ab_exp'		=>	$iCypher['ab']['exp'],
		  	'ab_bank'		=>	$iCypher['ab']['bank'],
		  	'id_last'		=>	$iCypher['id']['last'],
		  	'id_exp'		=>	$iCypher['id']['exp'],
		  	'ss_last'		=>	$iCypher['ss']['last'],
		  	'ss_exp'		=>	$iCypher['ss']['exp'],
		  	'contract'		=>	date('ymdHis'),
		  	'country'		=>	"VENEZUELA",
		  	'sale'			=>	$params['cli_ve_sale'],
		  	'repre'			=>	$iClient['repre_legal'],
		  	'tp'			=>	$request->get('ty'),
		  	'created_by'	=>	$app['session']->get('username')
  		];

  		switch ($iData['tp']) 
  		{
  			case '3':
  				$inInfo 	=	ContractVE::SaveContractFib($iData);
  				break;
  			case '7':
  				$inInfo 	=	ContractVE::SaveContractGpon($iData);
  				break;
  			default:
  				$inInfo 	=	ContractVE::InsertInfoContract($iData);
  				break;
  		}

  		// if($iData['tp'] == "3")
  		// {
  		// 	$inInfo 	=	ContractVE::SaveContractFib($iData);
  			
  		// }else{
  		// 	$inInfo 	=	ContractVE::InsertInfoContract($iData);

  		// }

		if($inInfo == true)
        { 
            $info = array('client' => $iData['client'], 'channel' => 'Contract', 'message' => 'Creacion de Contrato Tipo - '.$iData['type'].' - Numero '.$iData['contract'].' - Realizado por - '.$app['session']->get('username').' - Status Satisfactorio.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   	=> 	true, 
                'type'		=>	$iData['type'],
                'contract'	=>	$iData['contract'],
                'ty'		=>	$request->get('ty')
            ));

        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Contract', 'message' => 'Creacion de Contrato Tipo - '.$iData['type'].' - Solicitado por - '.$app['session']->get('username').' - Status Error.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   	=> 	false
            ));
        
        }
	
	}

	public static function ContractsSubmitVETest(Application $app, Request $request)
	{
		parse_str($request->get('str'), $params);

		$client 	=	$request->get('cli');

		if($params['cli_ve_type_c'] == "RESIDENCIAL")
		{
            return $app->json(array(
                'status'   	=> 	true, 
                'type'		=>	'RESIDENCIAL',
                'contract'	=>	'210130113342',
                'ty'		=>	$request->get('ty')
            ));
		}
		elseif ($params['cli_ve_type_c'] == "COMERCIAL") 
		{
            return $app->json(array(
                'status'   	=> 	true, 
                'type'		=>	'COMERCIAL',
                'contract'	=>	'210201193613',
                'ty'		=>	$request->get('ty')
            ));
		}


		$iClient 	=	ContractVE::GetInfoClient($client);
		
		$iCypher 	=	ContractVE::GetCypherClient($client);
		
		$iPro 		=	ContractVE::GetProrateo2($params);

		$eDate 		=	ContractVE::GetDateEnd($params['cli_ve_date'], $params['cli_ve_month']);

		$iData 	=	[
		  	'ticket' 		=>	$params['cli_ve_ticket'],
			'client'		=>	$client,
			'name' 			=>	$params['cli_ve_name'],
			'add_main'		=>	$iClient['add_main'],
			'add_postal'	=>	$iClient['add_postal'],
		  	'phone_main'	=>	$params['cli_ve_phone'],
		  	'phone_alt'		=>	$iClient['phone_alt'],
		  	'email'			=>	$iClient['email_main'],
  			'nivel'			=>	$iClient['nivel'],
  			'techo'			=>	$iClient['techo'],
  			'house'			=>	$iClient['house'],
  			'coor_lati'		=>	$iClient['coor_lati'],
  			'coor_long'		=>	$iClient['coor_long'],
		  	'package' 		=>	$params['cli_ve_package'],
		  	'type'	 		=>	$params['cli_ve_type_c'],
		  	'pre_date'		=>	$params['cli_ve_date'],
		  	'last_date'		=>	$eDate,
		  	'date_res'		=>	$iPro['dRes'],
		  	'price' 		=>	$params['cli_ve_price'],
		  	'prorateo'		=>	$iPro['total'],
		  	'mes' 			=>	$params['cli_ve_month'],
		  	'instalation'	=>	$params['cli_ve_inst'],
		  	'telephone'		=>	$params['cli_ve_telf'],
		  	'tel_type'		=>	$params['cli_ve_telf_type'],
		  	'tel_price'		=>	$params['cli_ve_telf_prec'],
		  	'cc_last'		=>	$iCypher['cc']['last'],
		  	'cc_exp'		=>	$iCypher['cc']['exp'],
		  	'ab_last'		=>	$iCypher['ab']['last'],
		  	'ab_exp'		=>	$iCypher['ab']['exp'],
		  	'ab_bank'		=>	$iCypher['ab']['bank'],
		  	'id_last'		=>	$iCypher['id']['last'],
		  	'id_exp'		=>	$iCypher['id']['exp'],
		  	'ss_last'		=>	$iCypher['ss']['last'],
		  	'ss_exp'		=>	$iCypher['ss']['exp'],
		  	'contract'		=>	date('ymdHis'),
		  	'country'		=>	"VENEZUELA",
		  	'sale'			=>	$params['cli_ve_sale'],
		  	'repre'			=>	$iClient['repre_legal'],
		  	'tp'			=>	$request->get('ty'),
		  	'created_by'	=>	$app['session']->get('username')
  		];

  		if($iData['tp'] == "3")
  		{
  			$inInfo 	=	ContractVE::SaveContractFib($iData);
  			
  		}else{
  			$inInfo 	=	ContractVE::InsertInfoContract($iData);

  		}

		if($inInfo == true)
        { 
            $info = array('client' => $iData['client'], 'channel' => 'Contract', 'message' => 'Creacion de Contrato Tipo - '.$iData['type'].' - Numero '.$iData['contract'].' - Realizado por - '.$app['session']->get('username').' - Status Satisfactorio.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   	=> 	true, 
                'type'		=>	$iData['type'],
                'contract'	=>	$iData['contract'],
                'ty'		=>	$request->get('ty')
            ));

        }else{

            $info = array('client' => $iData['client'], 'channel' => 'Contract', 'message' => 'Creacion de Contrato Tipo - '.$iData['type'].' - Solicitado por - '.$app['session']->get('username').' - Status Error.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

            return $app->json(array(
                'status'   	=> 	false
            ));
        
        }
	
	}

	public static function ContractsSubmitVEFib(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		parse_str($request->get('str'), $params);
		
		var_dump($params);
		exit;


	}

}