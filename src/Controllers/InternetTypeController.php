<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use App\Models\Auth;
use App\Models\User;
use App\Models\InternetType;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use JasonGrimes\Paginator;

/**
 * 
 */
class InternetTypeController extends BaseController
{
    
    public static function InternetTypeEdit(Application $app, Request $request)
    {
        return $app->json(array(
            'status'    => true, 
            'internettype'      => InternetType::GetInternetTypeById($request->get('id'))
        ));
    
    }

    public function InternetTypeForm(Application $app, Request $request)
    {
        $params     =   [];

        parse_str($request->get('value'), $params);

        if($params['type_int_t'] == 'new')
        {
            $saveDep = InternetType::SaveInternetType($params);
            
            if($saveDep <> false)
            { 
                $info = array('client' => '', 'channel' => 'Internet Type New', 'message' => 'Creacion de Paquete - '.strtoupper($params['name_int_t']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Internet Type New', 'message' => 'Error Al Intentar Crear Paquete - '.strtoupper($params['name_int_t']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }
        
        }
        elseif($params['type_int_t'] == 'edit')
        {
            $saveDep = InternetType::SaveInternetType($params);
            
            if($saveDep <> false)
            {

                $info = array('client' => '', 'channel' => 'Internet Type Edit', 'message' => 'Actualizacion de Paquete - '.strtoupper($params['name_int_t']).' - Realizado por - '.$app['session']->get('username').' - Realizado Correctamente.', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'   => true, 
                    'web'      => 'internet'
                ));

            }else{

                $info = array('client' => '', 'channel' => 'Internet Type Edit', 'message' => 'Error Al Intentar Actualizar Paquete - '.strtoupper($params['name_int_t']).' - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

                $app['datalogger']->RecordLogger($info);

                return $app->json(array(
                    'status'    => false, 
                    'html'      => Auth::Notification("No se pudo procesar la solicitud, intente nuevamente, si el problema persiste, comuniquese con su administrador de sistema.", true)
                )); 
            }          
        }    
    }
}