<?php

namespace App\Controllers;

require '../vendor/autoload.php';

use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use League\Csv\Writer;
use SplFileObject;
use SplTempFileObject;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use \PDO;

use App\Models\Mikrowisp;
use App\Models\MikrowispVe;
use App\Models\ServiceSheet;
use App\Models\ServiceSheetVe;
use App\Models\Town;
use App\Models\SupPreCord;
use App\Models\Coordination;
use App\Models\ApprovalsScore;
use App\Models\Internet;
use App\Models\Level;
use App\Models\Ceiling;
use App\Models\House;

use App\Lib\Config;
use App\Lib\DBPbx;
use App\Lib\DBSugar;
use App\Lib\DBMikroVE;
use App\Lib\DBSmart;
use App\Lib\SendMail;

/**
 * 
 */
class SupportController extends BaseController
{

/**
 * ******************************
 ** Hoja de Servicio Puerto Rico
 ********************************
 */

	public static function index(Application $app, Request $request)
	{
		return $app['twig']->render('Support/index.html.twig',array(
            'sidebar'   =>  true,
            'date'		=>	date("m/d/Y")
        ));
	
	}

	public static function SheetLoad(Application $app, Request $request)
	{
		$date 	= 	date("Y-m-d");

		return $app->json(array(
	        'status'    => 	true, 
	    	'html'      => 	ServiceSheet::LoadSheet($date),
	    	'date'		=>	date("m/d/Y")
		)); 
	
	}

	public static function SheetLoadDate(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  ($params['dateIni'] <> '') ? $_replace->ChangeDate($params['dateIni']) : '',
            'dateEnd'   =>  ($params['dateEnd'] <> '') ? $_replace->ChangeDate($params['dateEnd']) : ''
        ];

 

		return $app->json(array(
	        'status'    	=> 	true, 
	    	'html'      	=> 	ServiceSheet::LoadSheetDate($dates),
	    	'dateStreet'	=>	$_replace->ShowDateGen($params['dateEnd'])
		)); 
	
	}

	public static function SheetCreation(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		$iData 	=	['client' => $request->get('id'), 'date' => $_replace->ChangeDate($request->get('date'))];
		
		$iSheet	=	ServiceSheet::SearchSheet($iData);

		if($iSheet == false)
		{
			$iMikro 	=	Mikrowisp::ClientSearch($iData);

			if($iMikro <> false)
			{

				$iAp 	=	Mikrowisp::ClientAP($iData);
				$iEmi 	=	Mikrowisp::ClientPlans($iData);

				$iClient 	=	[
					'id'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['idcliente'])))),
					'name'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['nombre'])))),
					'add'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['Direccion_Fisica'])))),
					'phone'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['telefono'])))),
					'movil'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['movil'])))),
					'coord'	=>	utf8_encode(trim($iMikro['coordenadas'])),
					'date'	=>	$request->get('date')
				];

				$iPlan 		=	[
					'ip'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['ip'])))),
					'contr'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['contrato'])))),
					'plan'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['plan'])))),
					'ap'	=>	utf8_encode(trim($iAp))
				];

				$town 	=	Town::GetTownByCountry('1');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	ServiceSheet::SheetBodyHTML($iClient, $iPlan, $iEmi, $town)
				)); 

			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp, por favor intente nuevamente."
				)); 
			}

		}else{

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Existe una hoja previa de servicio para la fecha seleccionada, por favor intente en otro dia."
			)); 
		}
	
	}

	public static function SheetSave(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$iData 	=	[
			'client'	=>	$params['s_client'],
			'name'		=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($params['s_name'])))),
			'phone'		=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($params['s_phone'])))),
			'date'		=>	$_replace->ChangeDate($params['s_calendar']),
			'town'		=>	$params['s_town'],
			'coord'		=>	utf8_encode(trim($params['s_coord'])),
			'add'		=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($params['s_add'])))),
			'plan'		=>	utf8_encode(strtoupper(trim($params['s_plan_s']))),
			'ip'		=>	utf8_encode(trim($params['s_ip_a'])),
			'ip_p'		=>	utf8_encode(trim($params['s_ip_p'])),
			'signal'	=>	utf8_encode(trim($params['s_signal'])),
			'transf'	=>	utf8_encode(trim($params['s_trans'])),
			'sppedrx'	=>	utf8_encode(trim($params['s_speedrx'])),
			'sppedtx'	=>	utf8_encode(trim($params['s_speedtx'])),
			'ap'		=>	utf8_encode(trim($params['s_ap'])),
			'motive'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($params['s_motive'])))),
			'tecnical'	=>	'1',
			'operator'	=>	$app['session']->get('id')
		];
		
		$iResp 	=	ServiceSheet::SaveSheet($iData);

		if($iResp <> false)
		{
		    $info = array('client' =>  '', 'channel' => 'Hoja de Servicio', 'message' => 'Creacion de Hoja de Servicio - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Success', 
	        	'content'   =>  'Hoja de Servicio Creada Satisfactoriamente.',
	        	'dataID'	=>	$iResp
	    	));
		}else{

		    $info = array('client' =>  '', 'channel' => 'Hoja de Servicio', 'message' => 'Creacion de Hoja de Servicio - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible crear hoja de servicio, intente nuevamente."
	    	));
		}
	
	}

	public static function CancelSheet(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		if($params['res_can'] == "0")
		{
			return $app->json(array(
		        'status'    => 	false, 
	        	'title'		=>	'Error',
		    	'content'   => 	"Sin Ninguna Accion a Ejecutar."
			)); 
		}else{
			$iData =	['status' => $params['res_can'], 'id' => $params['mikro_id_can'] ];

			$res 	=	ServiceSheet::CancelSheet($iData);

			if($res <> false)
			{
			    $info = array('client' =>  '', 'channel' => 'Cancelacion Hoja de Servicio', 'message' => 'Cancelacion de Hoja de Servicio - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	true, 
			        'title'		=>	'Success',
			    	'content'   => 	"Datos Actualizados Correctamente"
				)); 
			}else{

			    $info = array('client' =>  '', 'channel' => 'Cancelacion Hoja de Servicio', 'message' => 'Cancelacion de Hoja de Servicio - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
				)); 
			}


		}	
	
	}

	public static function PrevSheet(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		if($params['res_prev'] == "0")
		{
			return $app->json(array(
		        'status'    => 	false, 
	        	'title'		=>	'Error',
		    	'content'   => 	"Sin Ninguna Accion a Ejecutar."
			)); 
		}else{
			$iData =	['status' => $params['res_prev'], 'id' => $params['mikro_id_prev'] ];

			$res 	=	ServiceSheet::PrevSheet($iData);

			if($res <> false)
			{
			    $info = array('client' =>  '', 'channel' => 'Servicio en Prevista', 'message' => 'Servicio en Prevista - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	true, 
			        'title'		=>	'Success',
			    	'content'   => 	"Datos Actualizados Correctamente"
				)); 
			}else{

			    $info = array('client' =>  '', 'channel' => 'Servicio en Prevista', 'message' => 'Servicio en Prevista - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
				)); 
			}


		}
	
	}

	public static function EditSheet(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		$iData 		=	['id' => $request->get('id')];
		$iSheet 	=	ServiceSheet::SearchSheetById($iData);

		if($iSheet <> false)
		{
			$iCli 	=	['client' => $iSheet['client_id'] ];
			$iMikro =	Mikrowisp::ClientSearch($iCli);

			if($iMikro <> false)
			{
				$iAp 	=	Mikrowisp::ClientAP($iCli);
				$iEmi 	=	Mikrowisp::ClientPlans($iCli);

				$iClient 	=	[
					'id'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['client_id'])))),
					'name'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['name'])))),
					'add'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['add_main'])))),
					'phone'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['phone_main'])))),
					'coord'	=>	utf8_encode(trim($iSheet['coordinations'])),
					'date'	=>	$_replace->ShowDate($iSheet['visit_date']),
					'town'	=>	$iSheet['town_id']
				];

				$iPlan 		=	[
					'ip'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_current'])))),
					'ip_p'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_public'])))),
					'contr'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'plan'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'ap'	=>	utf8_encode(trim($iSheet['ap_current'])),
					'rx'	=>	utf8_encode(trim($iSheet['speedtest_rx'])),
					'tx'	=>	utf8_encode(trim($iSheet['speedtest_tx'])),
					'sig'	=>	utf8_encode(trim($iSheet['signal_r'])),
					'trans'	=>	utf8_encode(trim($iSheet['transmitions'])),
					'reas'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['reason'])))),
				];

				$town 	=	Town::GetTownCountry('1');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	ServiceSheet::SheetBodyEditHTML($iClient, $iPlan, $iEmi, $town)
				)); 
			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp, por favor intente nuevamente."
				)); 
			}
		}else{
			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
			)); 
		}

		$iMikro 	=	Mikrowisp::ClientSearch($iData);
	
	}

	public static function SubmitEditSheet(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$iData 	=	[
			'id'		=>	strtoupper(trim($params['id_mikro'])),
			'date'		=>	$_replace->ChangeDate(trim($params['s_calendar'])),
			'phone'		=>	strtoupper(trim($params['s_phone'])),
			'name'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_name']))),
			'coord'		=>	strtoupper(trim($params['s_coord'])),
			'town'		=>	strtoupper(trim($params['s_town'])),
			'add'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_add']))),
			'plan'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_plan_s']))),
			'ip'		=>	strtoupper(trim($params['s_ip_a'])),
			'ip_p'		=>	strtoupper(trim($params['s_ip_p'])),
			'signal'	=>	strtoupper(trim($params['s_signal'])),
			'trans'		=>	strtoupper(trim($params['s_trans'])),
			'ap'		=>	trim($params['s_ap']),
			'rx'		=>	strtoupper(trim($params['s_speedrx'])),
			'tx'		=>	strtoupper(trim($params['s_speedtx'])),
			'reason'	=>	$_replace->deleteTilde((trim($params['s_motive']))),
			'operator'	=>	$app['session']->get('id')
		];

		$iRes 	=	ServiceSheet::UploadSheet($iData);

		if($iRes <> false)
		{
		    $info = array('client' =>  '', 'channel' => 'Actualizacion Hoja de Servicio', 'message' => 'Actualizacion de Hoja de Servicio - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Success', 
	        	'content'   =>  'Hoja de Servicio Actualizada Satisfactoriamente.',
	        	'dataID'	=>	$iRes
	    	));
		}else{
		    $info = array('client' =>  '', 'channel' => 'Actualizacion Hoja de Servicio', 'message' => 'Actualizacion de Hoja de Servicio - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Error', 
	        	'content'   =>  'Imposible Actualizar Hoja de Servicio, Intente Nuevamente.'
	    	));
		}

	}

	public static function ChangePlan(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		$iData 		=	['plan' => $request->get('plan') ];
		
		$iRes 		=	Mikrowisp::GetPlan($iData);

		if($iRes['emisor'] <> 0)
		{
			$iInfo 	=	['emisor'	=>	$iRes['emisor'] ];

			$iEmi 	=	Mikrowisp::GetEmisor($iInfo);

			$ap 	=	($iEmi <> false) ? $iEmi['nombre'] : "SIN AP ASOCIADO";
		}else{
			$ap 	=	"SIN AP ASOCIADO";
		}

		$iPlan = [
            'ip'                =>  $_replace->deleteTilde(strtoupper(trim($iRes['ip']))),
            'contrato'          =>  $_replace->deleteTilde(strtoupper(trim($iRes['id']))),
            'plan'              =>  $_replace->deleteTilde(strtoupper(trim($iRes['plan']))),
            'ap'                =>  $ap
        ];

		return $app->json(array(
        	'status'	=>	true,
        	'info'		=>	$iPlan
    	));
	
	}


/**
 * ******************************
 ** Hoja de Servicio Venezuela
 ********************************
 */

	public static function indexVE(Application $app, Request $request)
	{
		return $app['twig']->render('Support/VeIndex.html.twig',array(
            'sidebar'   =>  true,
            'date'		=>	date("m/d/Y")
        ));
	
	}

	public static function SheetLoadVE(Application $app, Request $request)
	{
		$date 	= 	date("Y-m-d");
		$iItems =	DBSmart::DBQueryAll('SELECT id, content FROM data_mw_fact_items WHERE status_id = "1" ORDER BY id ASC');

		return $app->json(array(
	        'status'    => 	true, 
	    	'html'      => 	ServiceSheetVe::LoadSheet($date),
	    	'items'		=>	$iItems,
	    	'date'		=>	date("m/d/Y")
		)); 
	
	}

	public static function addItems(Application $app, Request $request)
	{
		$iItems =	DBSmart::DBQuery('SELECT * FROM data_mw_fact_items WHERE id = "'.$request->get('id').'"');

		return $app->json(array(
	        'status'    => 	true, 
	    	'items'		=>	$iItems,
		)); 
	
	}

	public static function createBill(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		// var_dump($params);exit;

		// $id 		=	$params['mikro_id_items'];
		$id 		=	16;

		if( isset($params['mikro_id_items']) )
		{
			$total 	= 	$c 	= 	0;

			for ($i=1; $i < 10 ; $i++) 
			{ 
				if(isset($params['cant_'.$i]))
				{
					$iItems[$c++] =	[
						// 'id'		=>	$params['mikro_id_items'],
						'id'		=>	$id,
						'content'	=>	DBSmart::DBQuery('SELECT content FROM data_mw_fact_items WHERE id = "'.$i.'"')['content'],
						'cost'		=>	$params['cost_'.$i],
						'cantidad'	=>	$params['cant_'.$i],
						'iva'		=>	$params['iva_'.$i],
						'total'		=> 	$params['cost_'.$i]
					];

					$total 	=	$total + $params['total_'.$i];

				}			
			}

			$bFree 	=	DBMikroVE::DataExecuteLastID('INSERT INTO facturas (legal, idcliente, emitido, vencimiento, pago, total, forma, estado, tipo, cobrado, iva_igv, sub_total, impuesto, mp_id, mp_estado, payu_id, payu_pdf, payu_tipo, payu_urlpdf, oxxo_id, oxxo_referencia, total_khipu, id_khipu, url_khipu, barcode_cobro_digital, data_cobro_digital, otros_impuestos, siro, hashsiro, siroconcepto, barcode_siro, percepcion_afip, saldo, moneda ) VALUES("0", "'.$iItems[0]['id'].'", "'.date("Y-m-d").'", "'.date("Y-m-d").'", "0000-00-00", "'.$total.'", "", "No pagado", "1", "0.00", "0.00","'.$total.'", "SI", "", "", "", "", "", "", "", "", "0.00", "", "", "", "", "0;0;0", "0", "", "0", "", "0.00", "0.00", "1")');

			if($bFree <> false)
			{
				foreach ($iItems as $it => $itm) 
				{
			        $fITems =   DBMikroVE::DataExecute('INSERT INTO facturaitems (idfactura, descripcion, cantidad, idalmacen, unidades, impuesto, block, impuesto911, clave_invoice, tipoitem, montodescuento) VALUES("'.$bFree.'", "'.$itm['content'].'", "'.$itm['total'].'", "0", "'.$itm['cantidad'].'", "'.$itm['iva'].'", "0", "0.00", "", "0", "0.00")');

					$sItems =	DBSmart::DataExecute('INSERT INTO cp_mw_invoices_manual (mw, bills, descriptions, price, quantity, iva, total, operator_id,created_at)  VALUES("'.$id.'", "'.$bFree.'", "'.$itm['content'].'", "'.$itm['cost'].'", "'.$itm['cantidad'].'", "'.$itm['iva'].'", "'.$total.'", "'.$app['session']->get('id').'", "'.date("Y-m-d H:m:s").'")');
				}

				return $app->json(array(
			        'status'    => 	true 
				)); 

			}else{
				return $app->json(array(
			        'status'    => 	false 
				)); 

			}

		}else{
			return $app->json(array(
		        'status'    => 	false 
			)); 
		}


	}

	public static function SheetCreationVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		$iData 	=	['client' => $request->get('id'), 'date' => $_replace->ChangeDate($request->get('date'))];

		$iSheet	=	ServiceSheetVe::SearchSheet($iData);


		if($iSheet == false)
		{
			$iMikro 	=	MikrowispVe::ClientSearch($iData);


			if($iMikro <> false)
			{

				$iAp 	=	MikrowispVe::ClientAP($iData);
				$iEmi 	=	MikrowispVe::ClientPlans($iData);

				$iClient 	=	[
					'id'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['idcliente'])))),
					'name'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['nombre'])))),
					'add'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['direccion_principal'])))),
					'phone'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['telefono'])))),
					'movil'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['movil'])))),
					'coord'	=>	trim(utf8_encode($iMikro['coordenadas'])),
					'date'	=>	$request->get('date')
				];

				// var_dump($iClient);
				// exit;

				$iPlan 		=	[
					'ip'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['ip'])))),
					'contr'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['contrato'])))),
					'plan'	=>	$_replace->deleteTilde(strtoupper(trim(utf8_encode($iMikro['plan'])))),
					'ap'	=>	trim(utf8_encode($iAp))
				];

				$town 	=	Town::GetTownByCountry('4');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	ServiceSheetVE::SheetBodyHTML($iClient, $iPlan, $iEmi, $town)
				)); 

			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp o NO posee Servicios Asociados, por favor intente nuevamente."
				)); 
			}

		}else{

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Existe una hoja previa de servicio para la fecha seleccionada, por favor intente en otro dia."
			)); 
		}
	
	}

	public static function ChangePlanVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		$iData 		=	['plan' => $request->get('plan') ];
		
		$iRes 		=	MikrowispVe::GetPlan($iData);

		if($iRes['emisor'] <> 0)
		{
			$iInfo 	=	['emisor'	=>	$iRes['emisor'] ];

			$iEmi 	=	MikrowispVe::GetEmisor($iInfo);

			$ap 	=	($iEmi <> false) ? $iEmi['nombre'] : "SIN AP ASOCIADO";
		}else{
			$ap 	=	"SIN AP ASOCIADO";
		}

		$iPlan = [
            'ip'                =>  $_replace->deleteTilde(strtoupper(trim($iRes['ip']))),
            'contrato'          =>  $_replace->deleteTilde(strtoupper(trim($iRes['id']))),
            'plan'              =>  $_replace->deleteTilde(strtoupper(trim($iRes['plan']))),
            'ap'                =>  $ap
        ];

		return $app->json(array(
        	'status'	=>	true,
        	'info'		=>	$iPlan
    	));
	
	}

	public static function SheetSaveVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$iData 	=	[
			'client'	=>	$params['s_client'],
			'name'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_name']))),
			'phone'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_phone']))),
			'date'		=>	$_replace->ChangeDate($params['s_calendar']),
			'town'		=>	$params['s_town'],
			'coord'		=>	trim($params['s_coord']),
			'add'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_add']))),
			'plan'		=>	strtoupper(trim($params['s_plan_s'])),
			'ip'		=>	trim($params['s_ip_a']),
			'ip_p'		=>	trim($params['s_ip_p']),
			'signal'	=>	trim($params['s_signal']),
			'transf'	=>	trim($params['s_trans']),
			'sppedrx'	=>	trim($params['s_speedrx']),
			'sppedtx'	=>	trim($params['s_speedtx']),
			'ap'		=>	trim($params['s_ap']),
			'motive'	=>	$_replace->deleteTilde(strtoupper(trim($params['s_motive']))),
			'tecnical'	=>	'1',
			'operator'	=>	$app['session']->get('id')
		];
		
		$iResp 	=	ServiceSheetVe::SaveSheet($iData);

		if($iResp <> false)
		{
		    $info = array('client' =>  '', 'channel' => 'Hoja de Servicio', 'message' => 'Creacion de Hoja de Servicio - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Success', 
	        	'content'   =>  'Hoja de Servicio Creada Satisfactoriamente.',
	        	'dataID'	=>	$iResp
	    	));
		}else{

		    $info = array('client' =>  '', 'channel' => 'Hoja de Servicio', 'message' => 'Creacion de Hoja de Servicio - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible crear hoja de servicio, intente nuevamente."
	    	));
		}
	
	}

	public static function EditSheetVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		$iData 		=	['id' => $request->get('id')];
		$iSheet 	=	ServiceSheetVe::SearchSheetById($iData);

		if($iSheet <> false)
		{
			$iCli 	=	['client' => $iSheet['client_id'] ];
			$iMikro =	MikrowispVe::ClientSearch($iCli);

			if($iMikro <> false)
			{
				$iAp 	=	MikrowispVe::ClientAP($iCli);
				$iEmi 	=	MikrowispVe::ClientPlans($iCli);

				$iClient 	=	[
					'id'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['client_id'])))),
					'name'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['name'])))),
					'add'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['add_main'])))),
					'phone'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['phone_main'])))),
					'coord'	=>	utf8_encode(trim($iSheet['coordinations'])),
					'date'	=>	$_replace->ShowDate($iSheet['visit_date']),
					'town'	=>	$iSheet['town_id']
				];

				$iPlan 		=	[
					'ip'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_current'])))),
					'ip_p'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_public'])))),
					'contr'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'plan'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'ap'	=>	utf8_encode(trim($iSheet['ap_current'])),
					'rx'	=>	utf8_encode(trim($iSheet['speedtest_rx'])),
					'tx'	=>	utf8_encode(trim($iSheet['speedtest_tx'])),
					'sig'	=>	utf8_encode(trim($iSheet['signal_r'])),
					'trans'	=>	utf8_encode(trim($iSheet['transmitions'])),
					'reas'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['reason'])))),
				];

				$town 	=	Town::GetTownCountry('4');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	ServiceSheetVe::SheetBodyEditHTML($iClient, $iPlan, $iEmi, $town)
				)); 
			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp, por favor intente nuevamente."
				)); 
			}
		}else{
			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
			)); 
		}

		$iMikro 	=	MikrowispVe::ClientSearch($iData);
	
	}

	public static function SubmitEditSheetVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$iData 	=	[
			'id'		=>	strtoupper(trim($params['id_mikro'])),
			'date'		=>	$_replace->ChangeDate(trim($params['s_calendar'])),
			'phone'		=>	strtoupper(trim($params['s_phone'])),
			'name'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_name']))),
			'coord'		=>	strtoupper(trim($params['s_coord'])),
			'town'		=>	strtoupper(trim($params['s_town'])),
			'add'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_add']))),
			'plan'		=>	$_replace->deleteTilde(strtoupper(trim($params['s_plan_s']))),
			'ip'		=>	strtoupper(trim($params['s_ip_a'])),
			'ip_p'		=>	strtoupper(trim($params['s_ip_p'])),
			'signal'	=>	strtoupper(trim($params['s_signal'])),
			'trans'		=>	strtoupper(trim($params['s_trans'])),
			'ap'		=>	trim($params['s_ap']),
			'rx'		=>	strtoupper(trim($params['s_speedrx'])),
			'tx'		=>	strtoupper(trim($params['s_speedtx'])),
			'reason'	=>	strtoupper($_replace->deleteTilde((trim($params['s_motive'])))),
			'operator'	=>	$app['session']->get('id')
		];

		$iRes 	=	ServiceSheetVe::UploadSheet($iData);

		if($iRes <> false)
		{
		    $info = array('client' =>  '', 'channel' => 'Actualizacion Hoja de Servicio', 'message' => 'Actualizacion de Hoja de Servicio - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Success', 
	        	'content'   =>  'Hoja de Servicio Actualizada Satisfactoriamente.',
	        	'dataID'	=>	$iRes
	    	));
		}else{
		    $info = array('client' =>  '', 'channel' => 'Actualizacion Hoja de Servicio', 'message' => 'Actualizacion de Hoja de Servicio - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Error', 
	        	'content'   =>  'Imposible Actualizar Hoja de Servicio, Intente Nuevamente.'
	    	));
		}

	}

	public static function SheetLoadDateVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  ($params['dateIni'] <> '') ? $_replace->ChangeDate($params['dateIni']) : '',
            'dateEnd'   =>  ($params['dateEnd'] <> '') ? $_replace->ChangeDate($params['dateEnd']) : ''
        ];

		return $app->json(array(
	        'status'    	=> 	true, 
	    	'html'      	=> 	ServiceSheetVe::LoadSheetDate($dates),
	    	'dateStreet'	=>	$_replace->ShowDateGen($params['dateEnd'])
		)); 
	
	}

	public static function EditSheeVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();

		$iData 		=	['id' => $request->get('id')];
		$iSheet 	=	ServiceSheetVe::SearchSheetById($iData);

		if($iSheet <> false)
		{
			$iCli 	=	['client' => $iSheet['client_id'] ];
			$iMikro =	MikrowispVe::ClientSearch($iCli);

			if($iMikro <> false)
			{
				$iAp 	=	MikrowispVe::ClientAP($iCli);
				$iEmi 	=	MikrowispVe::ClientPlans($iCli);

				$iClient 	=	[
					'id'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['client_id'])))),
					'name'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['name'])))),
					'add'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['add_main'])))),
					'phone'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['phone_main'])))),
					'coord'	=>	utf8_encode(trim($iSheet['coordinations'])),
					'date'	=>	$_replace->ShowDate($iSheet['visit_date']),
					'town'	=>	$iSheet['town_id']
				];

				$iPlan 		=	[
					'ip'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_current'])))),
					'ip_p'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_public'])))),
					'contr'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'plan'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'ap'	=>	utf8_encode(trim($iSheet['ap_current'])),
					'rx'	=>	utf8_encode(trim($iSheet['speedtest_rx'])),
					'tx'	=>	utf8_encode(trim($iSheet['speedtest_tx'])),
					'sig'	=>	utf8_encode(trim($iSheet['signal_r'])),
					'trans'	=>	utf8_encode(trim($iSheet['transmitions'])),
					'reas'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['reason'])))),
				];

				$town 	=	Town::GetTownCountry('4');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	ServiceSheetVe::SheetBodyEditHTML($iClient, $iPlan, $iEmi, $town)
				)); 
			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp, por favor intente nuevamente."
				)); 
			}
		}else{
			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
			)); 
		}

		$iMikro 	=	MikrowispVe::ClientSearch($iData);
	
	}

	public static function PrevSheetVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		if($params['res_prev'] == "0")
		{
			return $app->json(array(
		        'status'    => 	false, 
	        	'title'		=>	'Error',
		    	'content'   => 	"Sin Ninguna Accion a Ejecutar."
			)); 
		}else{
			$iData =	['status' => $params['res_prev'], 'id' => $params['mikro_id_prev'] ];

			$res 	=	ServiceSheetVe::PrevSheet($iData);

			if($res <> false)
			{
			    $info = array('client' =>  '', 'channel' => 'Servicio en Prevista', 'message' => 'Servicio en Prevista - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	true, 
			        'title'		=>	'Success',
			    	'content'   => 	"Datos Actualizados Correctamente"
				)); 
			}else{

			    $info = array('client' =>  '', 'channel' => 'Servicio en Prevista', 'message' => 'Servicio en Prevista - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
				)); 
			}


		}
	
	}

	public static function CancelSheetVE(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		if($params['res_can'] == "0")
		{
			return $app->json(array(
		        'status'    => 	false, 
	        	'title'		=>	'Error',
		    	'content'   => 	"Sin Ninguna Accion a Ejecutar."
			)); 
		}else{
			$iData =	['status' => $params['res_can'], 'id' => $params['mikro_id_can'] ];

			$res 	=	ServiceSheetVe::CancelSheet($iData);

			if($res <> false)
			{
			    $info = array('client' =>  '', 'channel' => 'Cancelacion Hoja de Servicio', 'message' => 'Cancelacion de Hoja de Servicio - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	true, 
			        'title'		=>	'Success',
			    	'content'   => 	"Datos Actualizados Correctamente"
				)); 
			}else{

			    $info = array('client' =>  '', 'channel' => 'Cancelacion Hoja de Servicio', 'message' => 'Cancelacion de Hoja de Servicio - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
				)); 
			}


		}	
	
	}

	public static function MikroPre(Application $app, Request $request)
	{
		return $app['twig']->render('Support/PreCoord.html.twig',array(
            'sidebar'   =>  true,
        ));
	
	}

	public static function MikroLoad(Application $app, Request $request)
	{
		$iData 	=	SupPreCord::GetSupPreCord();

		return $app->json(array(
	        'status'    => 	true, 
	    	'html'      => 	SupPreCord::GetPreCoordHTML($iData)
		)); 
	
	}

	public static function ViewPreCord(Application $app, Request $request)
	{
		$cIData     =   SupPreCord::GetSupPreCordById($request->get('id'));

		$ticket 	=	$cIData['ticket'];

		$score      =   ApprovalsScore::GetScoreByTicket($cIData['ticket']);

		$type       =   SupPreCord::DetecTicket(substr($ticket,0,2), $ticket);

		$info       =   SupPreCord::GetInfo($type, $ticket);

        $client     =   SupPreCord::ObtData($info['client'], $ticket);

        $referred   =   SupPreCord::ObtData($info['referred'], $ticket);

        $htmlCli    =   ($client['status'] == true)     ? Coordination::InfoViewClient($client['data'], 'client', $score)       :   '';

        $htmlRef    =   ($referred['status'] == true)   ? Coordination::InfoViewClient($referred['data'], 'referred', $score)   :   '';

		switch ($type)
        {
            case 'INTPR':
                $htmlServ   =   Coordination::InfoServIntPR($info['service'], $info['client']);
                break;
            case 'IVE':
                $htmlServ   =   Coordination::InfoServIntVZ($info['service'], $info['client']);
                break;
        }

        return $app->json(array(
            'status'    =>  true,
            'client'    =>  $htmlCli,
            'referred'  =>  $htmlRef,
            'service'   =>  $htmlServ
        ));
	
	}

	public static function ProcPreCord(Application $app, Request $request)
	{
		$id 	=	$request->get('id');

		$update = 	SupPreCord::UpdatePreCord($id);

		if($update <> false)
		{
			return $app->json(array(
		        'status'    => 	true, 
		        'title'		=>	'Success',
		    	'content'   => 	"Datos Procesados Correctamente."
			)); 
		}else{
			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
			)); 
		}

	}

	public static function SearchDate(Application $app, Request $request)
	{

        $_replace   =   new Config();

        $params     =   [];
        parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  $_replace->ChangeDate($params['dateIni']),
            'dateEnd'   =>  $_replace->ChangeDate($params['dateEnd'])
        ];
        
        if($dates['dateIni'] <> $dates['dateEnd'])
        {
            $info   =   SupPreCord::LoadByDate("1", $dates);
        }else{
            $info   =   SupPreCord::LoadByDate("0", $dates);
        }

        return $app->json(array(
            'status'    =>  true, 
            'html'   	=>  SupPreCord::GetPreCoordHTML($info)
        ));

	}

/**
 * ******************************
 ** Hoja de Servicio Fibra Inalambrica
 ********************************
 */

	public static function Fiber(Application $app, Request $request)
	{
		return $app['twig']->render('Support/fiber/index.html.twig',array(
            'sidebar'   =>  true,
            'date'		=>	date("m/d/Y"),
            'ceiling'	=>	Ceiling::GetCeilingByCountryID("4"),
            'level'		=>	Level::GetLevelByCountryID("4"),
            'house'		=>	House::GetHouseByCountryID("4"),
        ));

	}

	public static function Store(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$_replace 	= 	new Config();

		$iData 	=	['client' => $params['sh_id'], 'date' => $_replace->ChangeDate($params['sh_dt'])];

		$iSheet	=	ServiceSheetVe::SearchSheetFib($iData);

		if($iSheet == false)
		{
			$iMikro 	=	MikrowispVe::ClientSearch($iData);


			if($iMikro <> false)
			{

				$iAp 	=	MikrowispVe::ClientAP($iData);
				$iEmi 	=	MikrowispVe::ClientPlans($iData);
				$iPack 	=	Internet::GetTypeVzFib("4");

				$iClient 	=	[
					'id'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['idcliente'])))),
					'name'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['nombre'])))),
					'add'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['direccion_principal'])))),
					'phone'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['telefono'])))),
					'coord'	=>	utf8_encode(trim($iMikro['coordenadas'])),
					'date'	=>	$params['sh_dt']
				];

				$iPlan 		=	[
					'ip'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['ip'])))),
					'contr'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['contrato'])))),
					'plan'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iMikro['plan'])))),
					'ap'	=>	utf8_encode(trim($iAp))
				];

				$town 	=	Town::GetTownByCountry('4');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	ServiceSheetVE::SheetFiberEdit($iClient, $iPlan, $iEmi, $town, $iPack)
				)); 

			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp o NO posee Servicios Asociados, por favor intente nuevamente."
				)); 
			}

		}else{

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Existe una hoja previa de servicio para la fecha seleccionada, por favor intente en otro dia."
			)); 
		}

	}

	public static function FiberChangePlan(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		$iData 		=	['plan' => $request->get('plan') ];
		
		$iRes 		=	MikrowispVe::GetPlan($iData);

		if($iRes['emisor'] <> 0)
		{
			$iInfo 	=	['emisor'	=>	$iRes['emisor'] ];

			$iEmi 	=	MikrowispVe::GetEmisor($iInfo);

			$ap 	=	($iEmi <> false) ? $iEmi['nombre'] : "SIN AP ASOCIADO";
		}else{
			$ap 	=	"SIN AP ASOCIADO";
		}

		$iPlan = [
            'ip'                =>  $_replace->deleteTilde(strtoupper(trim($iRes['ip']))),
            'contrato'          =>  $_replace->deleteTilde(strtoupper(trim($iRes['id']))),
            'plan'              =>  $_replace->deleteTilde(strtoupper(trim($iRes['plan']))),
            'ap'                =>  $ap
        ];

		return $app->json(array(
        	'status'	=>	true,
        	'info'		=>	$iPlan
    	));
	
	}

	public static function FiberSheetSave(Application $app, Request $request)
	{

		$_replace 	= 	new Config();
	
		parse_str($request->get('str'), $params);

		$iData 	=	[
			'client'	=>	$params['sh_client'],
			'name'		=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($params['sh_name'])))),
			'phone'		=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($params['sh_phone'])))),
			'date'		=>	$_replace->ChangeDate($params['sh_calendar']),
			'town'		=>	$params['sh_town'],
			'coord'		=>	utf8_encode(trim($params['sh_coord'])),
			'add'		=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($params['sh_add'])))),
			'plan'		=>	utf8_encode(strtoupper(trim($params['sh_plan_s']))),
			'ip'		=>	utf8_encode(trim($params['sh_ip_a'])),
			'ip_p'		=>	utf8_encode(trim($params['sh_ip_p'])),
			'signal'	=>	utf8_encode(trim($params['sh_signal'])),
			'transf'	=>	utf8_encode(trim($params['sh_trans'])),
			'sppedrx'	=>	utf8_encode(trim($params['sh_speedrx'])),
			'sppedtx'	=>	utf8_encode(trim($params['sh_speedtx'])),
			'ap'		=>	utf8_encode(trim($params['sh_ap'])),
			'pack'		=>	utf8_encode(trim($params['sh_pack'])),
			'fee'		=>	utf8_encode(trim($params['sh_fee'])),
			'apfib'		=>	utf8_encode(trim($params['sh_apfib'])),
			'motive'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim("Migracion a Servicio de Internet Fibra Inalambrica Paquete ".utf8_encode(strtoupper(trim($params['sh_pack']))). " de ".utf8_encode(strtoupper(trim($params['sh_plan_s']))) )))),
			'tecnical'	=>	'1',
			'operator'	=>	$app['session']->get('id')
		];
		
		$iResp 	=	ServiceSheetVe::SaveSheetFib($iData);

		if($iResp <> false)
		{
		    $info = array('client' =>  '', 'channel' => 'Hoja de Servicio', 'message' => 'Creacion de Hoja de Servicio Fibra - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Success', 
	        	'content'   =>  'Hoja de Servicio Creada Satisfactoriamente.',
	        	'dataID'	=>	$iResp
	    	));
		}else{

		    $info = array('client' =>  '', 'channel' => 'Hoja de Servicio', 'message' => 'Creacion de Hoja de Servicio Fibra - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible crear hoja de servicio, intente nuevamente."
	    	));
		}
			
	}

	public static function FiberLoad(Application $app, Request $request)
	{

		$date 	= 	date("Y-m-d");

		return $app->json(array(
	        'status'    => 	true, 
	    	'html'      => 	ServiceSheetVe::LoadSheetFib($date),
	    	'date'		=>	date("m/d/Y")
		)); 

	}

	public static function FiberLoadDate(Application $app, Request $request)
	{

		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

        $dates   =   [
            'dateIni'   =>  ($params['dateIni'] <> '') ? $_replace->ChangeDate($params['dateIni']) : '',
            'dateEnd'   =>  ($params['dateEnd'] <> '') ? $_replace->ChangeDate($params['dateEnd']) : ''
        ];

		return $app->json(array(
	        'status'    	=> 	true, 
	    	'html'      	=> 	ServiceSheetVe::LoadSheetDateFib($dates),
	    	'dateStreet'	=>	$_replace->ShowDateGen($params['dateEnd'])
		)); 

	}

	public static function FiberEdit(Application $app, Request $request)
	{

		$_replace 	= 	new Config();

		$iData 		=	['id' => $request->get('id')];
		$iSheet 	=	ServiceSheetVe::SearchSheetFibById($iData);

		if($iSheet <> false)
		{
			$iCli 	=	['client' => $iSheet['client_id'] ];
			$iMikro =	MikrowispVe::ClientSearch($iCli);

			if($iMikro <> false)
			{
				$iAp 	=	MikrowispVe::ClientAP($iCli);
				$iEmi 	=	MikrowispVe::ClientPlans($iCli);
				$iPack 	=	Internet::GetTypeVzFib("4");

				$iClient 	=	[
					'id'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['client_id'])))),
					'name'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['name'])))),
					'add'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['add_main'])))),
					'phone'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['phone_main'])))),
					'coord'	=>	utf8_encode(trim($iSheet['coordinations'])),
					'date'	=>	$_replace->ShowDate($iSheet['visit_date']),
					'town'	=>	$iSheet['town_id']
				];

				$iPlan 		=	[
					'ip'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_current'])))),
					'ip_p'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ip_public'])))),
					'contr'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'plan'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['plan'])))),
					'ap'	=>	utf8_encode(trim($iSheet['ap_current'])),
					'rx'	=>	utf8_encode(trim($iSheet['speedtest_rx'])),
					'tx'	=>	utf8_encode(trim($iSheet['speedtest_tx'])),
					'sig'	=>	utf8_encode(trim($iSheet['signal_r'])),
					'trans'	=>	utf8_encode(trim($iSheet['transmitions'])),
					'reas'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['reason'])))),
					'pack'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['pack'])))),
					'fee'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['fee'])))),
					'ap-f'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim($iSheet['ap_fiber'])))),
				];	

				$town 	=	Town::GetTownCountry('4');

				return $app->json(array(
			        'status'    => 	true, 
			    	'html'      => 	ServiceSheetVe::SheetEditFibHTML($iClient, $iPlan, $iEmi, $town, $iPack)
				)); 
			}else{

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"No se encuentra cliente en Mikrowisp, por favor intente nuevamente."
				)); 
			}
		}else{
			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
			)); 
		}

		$iMikro 	=	MikrowispVe::ClientSearch($iData);

	}

	public static function SubmitFiberEdit(Application $app, Request $request)
	{

		$_replace 	= 	new Config();
	
		parse_str($request->get('str'), $params);

		$iData 	=	[
			'id'		=>	strtoupper(trim($params['id_mikro'])),
			'date'		=>	$_replace->ChangeDate(trim($params['sh_calendar'])),
			'phone'		=>	strtoupper(trim($params['sh_phone'])),
			'name'		=>	$_replace->deleteTilde(strtoupper(trim($params['sh_name']))),
			'coord'		=>	strtoupper(trim($params['sh_coord'])),
			'town'		=>	strtoupper(trim($params['sh_town'])),
			'add'		=>	$_replace->deleteTilde(strtoupper(trim($params['sh_add']))),
			'plan'		=>	$_replace->deleteTilde(strtoupper(trim($params['sh_plan_s']))),
			'ip'		=>	strtoupper(trim($params['sh_ip_a'])),
			'ip_p'		=>	strtoupper(trim($params['sh_ip_p'])),
			'signal'	=>	strtoupper(trim($params['sh_signal'])),
			'trans'		=>	strtoupper(trim($params['sh_trans'])),
			'ap'		=>	trim($params['sh_ap']),
			'rx'		=>	strtoupper(trim($params['sh_speedrx'])),
			'tx'		=>	strtoupper(trim($params['sh_speedtx'])),
			'pack'		=>	utf8_encode(trim($params['sh_pack'])),
			'fee'		=>	utf8_encode(trim($params['sh_fee'])),
			'ap_fibra'	=>	utf8_encode(trim($params['sh_apfib'])),
			'reason'	=>	utf8_encode($_replace->deleteTilde(strtoupper(trim("Migracion a Servicio de Internet Fibra Inalambrica Paquete ".utf8_encode(strtoupper(trim($params['sh_pack']))). " de ".utf8_encode(strtoupper(trim($params['sh_plan_s']))) )))),
			'operator'	=>	$app['session']->get('id')
		];

		$iRes 	=	ServiceSheetVe::UploadSheetFib($iData);

		if($iRes <> false)
		{
		    $info = array('client' =>  '', 'channel' => 'Actualizacion Hoja de Servicio Fibra', 'message' => 'Actualizacion de Hoja de Servicio Fibra - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Success', 
	        	'content'   =>  'Hoja de Servicio Actualizada Satisfactoriamente.',
	        	'dataID'	=>	$iRes
	    	));
		}else{
		    $info = array('client' =>  '', 'channel' => 'Actualizacion Hoja de Servicio Fibra', 'message' => 'Actualizacion de Hoja de Servicio Fibra - Error - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

            $app['datalogger']->RecordLogger($info);

			return $app->json(array(
	        	'status'	=>	true,
	        	'title'     =>  'Error', 
	        	'content'   =>  'Imposible Actualizar Hoja de Servicio, Intente Nuevamente.'
	    	));
		}

	}

	public static function CancelSheetFib(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		if($params['res_can'] == "0")
		{
			return $app->json(array(
		        'status'    => 	false, 
	        	'title'		=>	'Error',
		    	'content'   => 	"Sin Ninguna Accion a Ejecutar."
			)); 
		}else{
			$iData =	['status' => $params['res_can'], 'id' => $params['mikro_id_can'] ];

			$res 	=	ServiceSheetVe::CancelSheetFib($iData);

			if($res <> false)
			{
			    $info = array('client' =>  '', 'channel' => 'Cancelacion Hoja de Servicio Fibra', 'message' => 'Cancelacion de Hoja de Servicio Fibra - Satisfactorio - Realizado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	true, 
			        'title'		=>	'Success',
			    	'content'   => 	"Datos Actualizados Correctamente"
				)); 
			}else{

			    $info = array('client' =>  '', 'channel' => 'Cancelacion Hoja de Servicio Fibra', 'message' => 'Cancelacion de Hoja de Servicio Fibra - Error - Solicitado por - '.$app['session']->get('username').'', 'time' => $app['date'], 'username' => $app['session']->get('username'));

	            $app['datalogger']->RecordLogger($info);

				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Imposible Realizar Procedimiento, Intente nuevamente."
				)); 
			}


		}

	}

	public static function Contract(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
	
		parse_str($request->get('str'), $params);

		if( ($params['sh_day'] == "") || ($params['sh_pro'] == "") || ($params['sh_month'] == "") )
		{

			return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Existen datos faltantes, por favor verificar.!"
			)); 

		}else{

			$id 	=	['id' 	=>	$params['sh_idc'] ];
			$sheet 	=	ServiceSheetVe::SearchSheetFibById($id);

			$iData 	=	[
				'ticket'		=>	"",
				'client_id'		=>	$sheet['client_id'],
				'name'			=>	$sheet['name'],
				'add_main'		=>	$sheet['add_main'],
				'add_postal'	=>	$sheet['add_main'],
				'phone_main'	=>	$sheet['phone_main'],
				'phone_alt'		=>	$sheet['phone_main'],
				'email'			=>	"",
				'nivel'			=>	Level::GetLevelById($params['sh_lev'])['name'],
				'techo'			=>	Ceiling::GetCeilingById($params['sh_cei'])['name'],
				'house'			=>	House::GetHouseById($params['sh_hou'])['name'],
				'coor_lati'		=>	"",
				'coor_long'		=>	"",
				'package'		=>	$sheet['plan'],
				'type'			=>	"RESIDENCIAL",
				'telephone'		=>	"",
				'tel_type'		=>	"",
				'tel_price'		=>	"",
				'pre_date'		=>	date("Y-m-d"),
				'last_date'		=>	date("Y-m-d",strtotime(date("Y-m-d")."+ ".$params['sh_month']." month")),
				'date_res'		=>	$params['sh_day'],
				'price'			=>	Internet::GetInternetByName(substr($sheet['pack'], 6, strlen($sheet['pack']) ))['price'],
				'prorateo'		=>	$params['sh_pro'],
				'mes'			=>	$params['sh_month'],
				'instalation'	=>	$sheet['fee'],
				'cc_last'		=>	"",
				'cc_exp'		=>	"",
				'ab_last'		=>	"",
				'ab_bank'		=>	"",
				'ab_exp'		=>	"",
				'id_last' 		=>	"",
				'id_exp'		=>	"",
				'ss_last' 		=>	"",
				'ss_exp'		=>	"",
				'contract'		=>	date("Ymdhms"),
				'country'		=>	"VENEZUELA",
				'sale'			=>	$app['session']->get('username'),
				'repre_legal'	=>	"",
				'tp'			=>	"1",
				'created_by'	=>	$app['session']->get('username'),
			];

			$contract 	=	ServiceSheetVe::SaveContractFib($iData);

			if($contract == true)
			{
				return $app->json(array(
			        'status'    => 	true, 
			        'contract'	=>	$iData['contract']
				)); 

			}else{
				return $app->json(array(
			        'status'    => 	false, 
			        'title'		=>	'Error',
			    	'content'   => 	"Error al intentar crear el contrato, si problema persiste contacte al administrador de sistemas."
				)); 

			}

			
		}

	}

	public static function SendEmailDay(Application $app, Request $request)
	{
		$_replace 	= 	new Config();
		
		parse_str($request->get('str'), $params);

		$iEmail 	=	false;

		$rout       =   '../sheetsVe/';

        $gestor     =   opendir($rout);
        $date       =   $params['send_date'].'.pdf';
        $con        =   0;

        while ($file = readdir($gestor)) 
        {
            if(!is_dir($file))
            {
                $data   =   substr($file, -14);

                if($date == $data)
                {
                    $con = $con + 1;
                    $iEmail[$con]   =   $file; 
                }
            }
        }

        if($iEmail <> false)
        {
        	foreach ($iEmail as $i => $ie) 
        	{
        		$fAdd[$i]	=	$rout.$ie;
        	}

			$emails[0]  =   'hector.904@boomsolutions.com';
			// $ccs[0]     =   'luis.924@boomsolutions.com';

			$email 		=	new SendMail();

			$sendmail   =   $email->SendWeb($emails, $ccs, $params['send_date'], $fAdd);
			
			return $app->json(array(
		        'status'    => 	true, 
		        'title'		=>	'Success',
		    	'content'   => 	"Email Enviado Correctamente"
			)); 
       
        }else{
       		return $app->json(array(
		        'status'    => 	false, 
		        'title'		=>	'Error',
		    	'content'   => 	"Intente nuevamente."
			)); 
        }
	}

}