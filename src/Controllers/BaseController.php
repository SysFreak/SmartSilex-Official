<?php
namespace App\Controllers;

use Silex\Application;
/**
* Controlador base de la aplicacion
* 
* Cualquier controlador puede extenderlo para obtener su funcionalidad, usalo
* para definir metodos que deban estar presentes en todos los controladores
*/
class BaseController 
{
    
    protected $app;
    
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function redirectToRoute($name)
    {
        return $this->app->redirect($this->app['url_generator']->generate($name));
    }

}