<?php

namespace App\Controllers;

use App\Models\User;
use App\Models\UserRol;
use App\Models\UserPermit;
use App\Models\Auth;
use App\Models\Departament;
use App\Models\Status;
use App\Models\Team;
use App\Models\Role;
use App\Lib\MultiLogger;
use App\Lib\DBSmart;
use App\Lib\SendMail;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
* 
*/
class ProfileController extends BaseController
{

    public function index(Application $app, Request $request)
    {
        return $app['twig']->render('profiles/index.html.twig',array(
              'sidebar'   =>    true,
        ));
    }
}