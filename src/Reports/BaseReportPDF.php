<?php
namespace App\Reports;

use FPDF;
use Symfony\Component\HttpFoundation\Response;

class BaseReportPDF extends FPDF
{

    public function __construct($orientation='P', $unit='mm', $size='letter')
    {
        parent::__construct($orientation, $unit, $size);
        $this->setFont('Times');
        $this->addPage();
        $this->AliasNbPages();
    }

    public function asInlineResponse($filename = 'doc.pdf', $isUTF8 = false, $aditionalHeaders = array())
    {
        return $this->asResponse(true, $filename, $isUTF8, $aditionalHeaders);
    }

    public function asFileResponse($filename = 'doc.pdf', $isUTF8 = false, $aditionalHeaders = array())
    {
        return $this->asResponse(false, $filename, $isUTF8, $aditionalHeaders);
    }

    public function asResponse($inline = true, $filename = 'doc.pdf', $isUTF8 = false, $aditionalHeaders = array())
    {
        $disposition = $inline ? 'inline;' : 'attachment;' ;
        $filename    = $this->_httpencode('filename',$filename,$isUTF8);
        $headers     = array(
            'Content-Type'        => 'application/pdf',
            'Cache-Control'       => 'private, max-age=0, must-revalidate',
            'Content-Disposition' => $disposition.' '.$filename,
        );
        
        return new Response(
            $this->output('s'),
            Response::HTTP_OK,
            array_merge($headers, $aditionalHeaders)
        );
    }
}
