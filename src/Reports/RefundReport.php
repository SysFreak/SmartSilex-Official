<?php
namespace App\Reports;

class RefundReport extends BasePaymentLotPDF
{
    public function Header()    {
        $this->Image('../web/img/boomNET.png',10,10,0,20);
        
        $this->SetFont('Calibri','I',12);
        $this->Cell(0,5,'Boom Net / CA Solutions Inc.',$this->debug,1,'R');
        
        $this->SetFontsize(10);
        $this->Cell(0,5,'PO Box 810, Fajardo, PR, 00738',$this->debug,1,'R');
        $this->Cell(0,5,'787-333-0230 / 787-333-0231',$this->debug,1,'R');
        
        $this->SetFont('','B',14);
        $this->SetTextColor(227,108,10);
        $this->Cell(0,5,'Reporte de Pago por Lote',$this->debug,1,'C');
        
        $this->Ln($this->height);
    }

    public function PaymentReport(array $data)  {
        
        $this->addPage('P');
        $this->setFillColor(0,34,96);
        $this->datos_cliente($data);

        return $this;
    }

    public function datos_cliente(array $data)  {

        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'PAGOS PROCESADOS EXITOSAMENTE',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();


        
        foreach ($data as $key => $value) {

             if ($value->status == "Exitoso"){

                $this->Cell(40,$this->height,'ID CLIENTE: '          . utf8_decode($value->idClient),$this->debug,0,'L');
                $this->Cell(50,$this->height,'CLIENTE: '             . utf8_decode($value->nameClient),$this->debug,0,'L');
                $this->Cell(50,$this->height,'USUARIO: '             . utf8_decode(strtoupper($value->username)),$this->debug,0,'L');
                $this->Cell(20,$this->height,'FECHA: '               . utf8_decode($value->created_at),$this->debug,0,'L');
                $this->Ln($this->height);        

                $this->Cell(40,$this->height,'FACTURA: '             . utf8_decode($value->transBill),$this->debug,0,'L');
                $this->Cell(50,$this->height,'MONTO: '               . utf8_decode($value->transMont),$this->debug,0,'L');
                $this->Cell(50,$this->height,'CODIGO TRANSACCION: '  . utf8_decode($value->transCode),$this->debug,0,'L');
                $this->Cell(40,$this->height,'CODIGO AUTORIZACION: ' . utf8_decode($value->transAuth),$this->debug,0,'L');
                $this->Ln($this->height);

                $this->Cell(40,$this->height,'ESTATUS: '             . utf8_decode(strtoupper($value->status)),$this->debug,0,'L');
                $this->Cell(50,$this->height,'MONTO REFUND: '        . utf8_decode($value->refundMont),$this->debug,0,'L');
                $this->Cell(50,$this->height,'CODIGO REFUND: '       . utf8_decode($value->refundAuth),$this->debug,0,'L');
                $this->Cell(40,$this->height,'TRANSACCION REFUND: '  . utf8_decode($value->refundCode),$this->debug,0,'L');
                $this->Ln($this->height);

                $this->Cell(40,$this->height,'CREDIT CARD: '         . utf8_decode($value->creditCard),$this->debug,0,'L');
                $this->Cell(100,$this->height,'MENSAJE REFUND: '     . utf8_decode(strtoupper($value->message)),$this->debug,0,'L');
                $this->Ln($this->height);
                $this->Ln($this->height);

            }
        }


        
        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }

        $this->Ln($this->height);

        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'Error en Procesamiento de Pago',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        
        foreach ($data as $key => $value) {

            if ($value->status == "Error"){

                $this->Cell(40,$this->height,'ID CLIENTE: '          . utf8_decode($value->idClient),$this->debug,0,'L');
                $this->Cell(50,$this->height,'CLIENTE: '             . utf8_decode($value->nameClient),$this->debug,0,'L');
                $this->Cell(50,$this->height,'USUARIO: '             . utf8_decode(strtoupper($value->username)),$this->debug,0,'L');
                $this->Cell(20,$this->height,'FECHA: '               . utf8_decode($value->created_at),$this->debug,0,'L');
                $this->Ln($this->height);        

                $this->Cell(40,$this->height,'FACTURA: '             . utf8_decode($value->transBill),$this->debug,0,'L');
                $this->Cell(50,$this->height,'MONTO: '               . utf8_decode($value->transMont),$this->debug,0,'L');
                $this->Cell(50,$this->height,'CODIGO TRANSACCION: '  . utf8_decode($value->transCode),$this->debug,0,'L');
                $this->Cell(40,$this->height,'CODIGO AUTORIZACION: ' . utf8_decode($value->transAuth),$this->debug,0,'L');
                $this->Ln($this->height);

                $this->Cell(40,$this->height,'ESTATUS: '             . utf8_decode(strtoupper($value->status)),$this->debug,0,'L');
                $this->Cell(50,$this->height,'MONTO REFUND: '        . utf8_decode($value->refundMont),$this->debug,0,'L');
                $this->Cell(50,$this->height,'CODIGO REFUND: '       . utf8_decode($value->refundAuth),$this->debug,0,'L');
                $this->Cell(40,$this->height,'TRANSACCION REFUND: '  . utf8_decode($value->refundCode),$this->debug,0,'L');
                $this->Ln($this->height);

                $this->Cell(40,$this->height,'CREDIT CARD: '         . utf8_decode("xxxx" .$value->creditCard),$this->debug,0,'L');
                $this->Cell(100,$this->height,'MENSAJE REFUND: '     . utf8_decode(strtoupper($value->message)),$this->debug,0,'L');
                $this->Ln($this->height);
                $this->Ln($this->height);

            }
        }

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }

        $this->Ln($this->height);
    }

    public function encabezado_tabla($value='') {
        
        $this->SetFont('','B',12);  // fuente 14 y negrita
        
        $this->SetTextColor(255);   // texto color blanco
    }

    public function cuerpo_tabla($value='') {
        
        $this->SetFont('','',8);    // fuente 11 y negrita
        
        $this->SetTextColor(0);     // texto color NEGRO
    }
}