<?php
namespace App\Reports;

class ServiceReport extends BaseServicePDF
{
    public function Header()    {
        $this->Image('../web/img/boomNET.png',10,10,0,20);
        
        $this->SetFont('Calibri','I',12);
        $this->Cell(0,5,'Boom Net / CA Solutions Inc.',$this->debug,1,'R');
        
        $this->SetFontsize(10);
        $this->Cell(0,5,'PO Box 810, Fajardo, PR, 00738',$this->debug,1,'R');
        $this->Cell(0,5,'787-333-0230 / 787-333-0231',$this->debug,1,'R');
        
        $this->SetFont('','B',14);
        $this->SetTextColor(227,108,10);
        $this->Cell(0,5,'SERVICIO TECNICO',$this->debug,1,'C');
        
        $this->Ln($this->height);
    }

    public function ServicioTecnico(array $data, array $answer, $equipament)  {
        
        $this->addPage('P');
        $this->setFillColor(0,34,96);
        $this->datos_cliente($data);

        $this->Ln(4);
        $this->falla_tecnica($answer);
        
        $this->Ln(4);
        $this->ausencia_servicio($answer);

        $this->addPage('P');
        $this->setFillColor(0,34,96);
        
        $this->Ln(4);
        $this->servicio_activo($answer);

        $this->Ln(4);
        $this->trafico_senal($answer);

        $this->Ln(4);
        $this->otros($answer);

        $this->addPage('P');
        $this->setFillColor(0,34,96);
        
        $this->Ln(4);
        $this->actualizacion_informacion($answer, $equipament);

        return $this;
    }

    public function datos_cliente(array $data)  {

        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'INFORMACION DEL CLIENTE',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 35;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        $this->Cell($uno, $this->height,'ID CLIENTE: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['idClient']),$this->debug,0,'L');

        $this->Cell($tres, $this->height,'TICKET: ',$this->debug,0,'R');
        $this->Cell($quad, $this->height,utf8_decode($data['idTicket']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'CLIENTE: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['clientName']),$this->debug,0,'L');

        $this->Cell($tres,$this->height,'SOLICITADO POR: ',$this->debug,0,'R');
        $this->Cell($quad,$this->height,utf8_decode($data['requested_by']),$this->debug,0,'L');
        $this->Ln($this->height);
        
        $this->Cell($uno,$this->height,'SUPERVISADO POR: ',$this->debug,0,'R');
        $this->Cell($dos,$this->height,utf8_decode($data['approved_by']),$this->debug);

        $this->Cell($tres, $this->height,'STATUS: ',$this->debug,0,'R');
        $estado = ($data['idStatus'] == 'Rejected') ? 'Rechazado' : 'Aprobado' ;
        $this->Cell($quad, $this->height,utf8_decode($estado),$this->debug,0,'L');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function falla_tecnica(array $answer)  {
        
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'FALLA TECNICA EN LOS 90 DIAS DE GARANTIA',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 80;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        // Cell(Ancho, Alto, Texto, Border, Posicion, Aliniacion, Color de Fondo, URL)
        $this->Ln(3);
        $this->Cell($uno,$this->height,"CLIENTE POSEE GARANTIA POR LA INSTALACION:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[0]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[0]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');       
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[0]['coment']),$this->debug);
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function ausencia_servicio(array $answer)  {
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'AUSENCIA TOTAL DEL SERVICIO',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 80;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        // Cell(Ancho, Alto, Texto, Border, Posicion, Aliniacion, Color de Fondo, URL)
        $this->Ln(3);
        $this->Cell($uno,$this->height,"CLIENTE SE ENCUENTRA DESACTIVADO POR FALTA DE PAGO:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[1]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[1]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[1]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"CLIENTE PENDIENTE POR ACTIVACION (PREVIAMENTE REALIZO EL PAGO DEL SERVICIO):",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[2]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[2]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[2]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"CONEXION DE CORRIENTE Y CALBE CHINITA DEL ROUTER Y POWER SUPPLY CORRECTAMENTE CONECTADO:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[3]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[3]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[3]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"EXISTE AVERIAS EN EL SITE (CONFIRMAR EN EL DUDE O CON EL EQUIPO DE SITE):",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[4]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[4]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[4]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"EL SITE HA PRESENTADO MODIFICACIONES RECIENTEMENTE (CONFIRMAR CON EQUIPO DE SITE):",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[5]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[5]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[5]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"EL CLIENTE SE ENCUENTRA EN EL AP CORRECTO SEGUN ULTIMO MONITOREO O CONTRATO DE INSTALACION:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[6]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[6]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[6]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"Observacion:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[7]['coment']),$this->debug);
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function servicio_activo(array $answer)    {
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'SERVICIO ACTIVO PERO CON LENTITUD',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 80;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        // Cell(Ancho, Alto, Texto, Border, Posicion, Aliniacion, Color de Fondo, URL)
        $this->Ln(3);
        $this->Cell($uno,$this->height,"SE VERIFICO IMAGEN DE LINEA DE VISION EN CONTRATO O VISITA RECIENTE?:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[8]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[8]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[8]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"CLIENTE ES EL UNICO EN EL SITE EN EL QUE SE ENCUENTRA CONECTADO QUE PRESENTA LA FALLA TECNICA:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[9]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[9]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[9]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"CLIENTE REALIZO EL SPEED TEST EN EL SERVIDOR DE BOOMNET MEDIENTE LA PAGINA WWW. SPEEDTEST.NET:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[10]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[10]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[10]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMPARAR EL SCREENSHOT DE SENAL QUE SE INGRESO AL MOMENTO DE LA INSTALACION CON MONITOREO RECIENTE:",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[11]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[11]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[11]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"INDICAR VALORES ACTUALES (ACTUALIZAR AP EN UBER DE SER NECESARIO):",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[12]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[12]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[12]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"Observacion:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[13]['coment']),$this->debug);
        $this->Ln($this->height);


        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function trafico_senal(array $answer)  {
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'TRAFICO Y SENAL OPTIMA PERO SERVICIO INTERMITENTE',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 80;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        // Cell(Ancho, Alto, Texto, Border, Posicion, Aliniacion, Color de Fondo, URL)
        $this->Ln(3);
        $this->Cell($uno,$this->height,"CLIENTE PRESENTA MONITOREO POR PARTE DEL EQUIPO DE SITE: DUDE (LAPSO DE 24 HORAS):",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[14]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[14]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[14]['coment']),$this->debug);
        $this->Ln($this->height);

        
        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function otros(array $answer)  {

        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'OTROS',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 80;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        // Cell(Ancho, Alto, Texto, Border, Posicion, Aliniacion, Color de Fondo, URL)
        $this->Ln(3);
        $this->Cell($uno,$this->height,"CLIENTE PRESENTA MONITOREO POR PARTE DEL EQUIPO DE SITE: DUDE (LAPSO DE 24 HORAS):",$this->debug,0,'L');
        $this->Cell(90,$this->height,"SI:",$this->debug,0,'R');
        $Opcion     = ($answer[15]['answers'] == 1) ? 'X' : Null;
        $Opcion2    = ($answer[15]['answers'] == 0) ? 'X' : Null;
        $this->Cell(5,$this->height, $Opcion ,$this->debug,0,'R');
        $this->Cell(10,$this->height,"NO",$this->debug,0,'R');
        $this->Cell(5,$this->height, $Opcion2 ,$this->debug,0,'R');  
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,"COMENTARIO:",$this->debug,0,'L');
        $this->Ln($this->height);
        $this->MultiCell(196,$this->height,utf8_decode($answer[15]['coment']),$this->debug);
        $this->Ln($this->height);

        
        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function actualizacion_informacion(array $answer, $equipament) {
        
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'INFORMACION DEL CLIENTE',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 80;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;


        $this->Cell($uno,$this->height,'NIVELES DE RESIDENCIA: ',$this->debug,0,'R');
        $this->Cell($dos,$this->height,utf8_decode($answer[21]['coment']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'FECHA DE DISPONIBILIDAD PARA LA VISITA: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($answer[16]['coment']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'PAUTAR HORA PARA LA VISITA TECNICA: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($answer[17]['coment']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'CONFIRMAR DIRECCION DE CLIENTE: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($answer[18]['coment']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,'CONFIRMAR TELEFONO DE CONTACTO: ',$this->debug,0,'R');
        $this->Cell($dos,$this->height,utf8_decode($answer[19]['coment']),$this->debug,0,'L');
        $this->Ln($this->height);
        
        $this->Cell($uno,$this->height,'INDICAR NUMERO TELEFONICO DE CONTACTO ALTERNO: ',$this->debug,0,'R');
        $this->Cell($dos,$this->height,utf8_decode($answer[20]['coment']),$this->debug);
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,'EQUIPAMIENTO ESPECIAL: ',$this->debug,0,'R');
        $this->Cell($dos,$this->height,utf8_decode($equipament),$this->debug,0,'L');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function encabezado_tabla($value='') {
        
        $this->SetFont('','B',12);  // fuente 14 y negrita
        
        $this->SetTextColor(255);   // texto color blanco
    }

    public function cuerpo_tabla($value='') {
        
        $this->SetFont('','',8);    // fuente 11 y negrita
        
        $this->SetTextColor(0);     // texto color NEGRO
    }
}