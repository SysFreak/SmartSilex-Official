<?php
namespace App\Reports;

use FPDF;
use Symfony\Component\HttpFoundation\Response;

define('FPDF_FONTPATH', realpath( 'fonts/' ));

class BaseServicePDF extends FPDF
{

    public $debug = 0;
    public $height = 5;
    
    public function __construct($orientation='P', $unit='mm', $size='letter')
    {
        parent::__construct($orientation, $unit, $size);
        $this->SetLineWidth(0.4);
        $this->AddFont('Calibri','', 'calibri.php');
        $this->AddFont('Calibri','B','calibrib.php');
        $this->AddFont('Calibri','I','calibrii.php');
    }
}