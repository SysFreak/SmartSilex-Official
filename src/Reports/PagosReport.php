<?php
namespace App\Reports;

class PagosReport extends BaseReportPDF
{

    public function Header()
    {
        $this->cell(0,10,'Boom Solutions C.A. - Registro de Transacciones',0,1,'C');
    }

    public function Footer()
    {
        $this->cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'R');
    }

    public function ReportePagos($logs)
    {
        $logs = $this->logsToArray($logs);

        foreach ($logs as $log) {
            $this->cell(50,10,'Fecha',1,0,'C');
            $this->cell(0,10,$log['time'],1,1);

            $this->cell(50,10,'Mensaje',1,0,'C');
            $this->cell(0,10,$log['message'],1,1);
            
            $this->cell(50,10,'Usuario',1,0,'C');
            $this->cell(0,10,$log['user'],1,1);
            
            $this->cell(50,10,'Cliente',1,0,'C');
            $this->cell(0,10,$log['request']['client']['first_name'].' '.$log['request']['client']['last_name'],1,1);
            
            $this->cell(50,10,'Exitoso?',1,0,'C');
            $this->cell(0,10,$log['exito'] ? 'SI' : 'NO', 1, 1);

            $this->cell(50,10*count($log['gatewayResponse']),'Respuesta de '.$log['gateway'],1,0,'C');    
            
            // $this->Multicell(0,10,$log['gatewayResponse'],0,1);    
            $first = true;
            foreach ($log['gatewayResponse'] as $key => $value) {
                
                $this->x += ($first) ? 0 : 50;
                $first = false;
                
                $this->cell(40,10,$key,1,0);
                $this->cell(0,10,$value,1,1);
            }
            $this->ln(10);   
        }
        
        return $this;
    }

    private function logsToArray($logs)
    {
        $mutated = array();
        foreach ($logs as $num => $log) {
            $mutated[$num] = $log->as_array();
            $mutated[$num]['request']  = json_decode($mutated[$num]['request'],true);
            $mutated[$num]['gatewayResponse'] = json_decode($mutated[$num]['gatewayResponse'],true);            
            $mutated[$num]['time'] = date('D, d M Y H:i:s',$mutated[$num]['time']);            
        }
        return $mutated;
    }



}
