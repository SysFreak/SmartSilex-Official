<?php
namespace App\Lib;

use App\Models\Transactions;

use App\Lib\DBMikroVE;
use App\Lib\DBSmart;

// require '../vendor/autoload.php';

use FPDF;

// ddd('FPDF_FONTPATH', realpath('fonts/'));
define('FPDF_FONTPATH', realpath( 'fonts/' ));

class OperatorStatics extends FPDF
{
    
    public $debug = 0;
    public $height = 7;

    function __construct($orientation='P', $unit='mm', $size='letter')
    {
        
        parent::__construct($orientation, $unit, $size);
        $this->SetLineWidth(0.4);
        $this->AddFont('Calibri','', 'calibri.php');
        $this->AddFont('Calibri','B','calibrib.php');
        $this->AddFont('Calibri','I','calibrii.php');
    }

    public function Header()
    {
        $this->Image('assets/img/logicon.png',10,5,0,10);
        
        $this->SetFont('Calibri','I',8);
        $this->Cell(0,2.5,'BOOM SOLUTIONS.',$this->debug,1,'R');
        
        $this->SetFontsize(8);
        $this->Cell(0,2.5,'AV. LARA, C.C. CHURUN MERU',$this->debug,1,'R');
        $this->Cell(0,2.5,'BARQUISIMETO 3001, LARA',$this->debug,1,'R');
        $this->Cell(0,2.5,'251-3353346',$this->debug,1,'R');
        
        $this->SetFont('','B',10);
        $this->SetTextColor(227,108,10);
        $this->Cell(0,2.5,'REPORTES DE OPERACIONES',$this->debug,1,'C');
        
        $this->Ln(2);
    }

    public function hojaServicio(array $date)
    {

        $dat = ( ($date['da001'] <> "") && ($date['da002'] <> "") ) ? ['in' => $date['da001'], 'en' => $date['da002'] ] : ['in' => date("Y-m-d"), 'en' => date("Y-m-d")];

        $ope    =   DBSmart::DBQuery('SELECT id FROM users WHERE username = "'.$date['op'].'"');

        $iData  =   [
            'dat'   =>  'created_at BETWEEN "'.$dat['in'].' 00:00:00" AND "'.$dat['en'].' 23:59:59"',
            'ope'   =>  'operator_id = "'.$ope['id'].'"',
            'sta'   =>  '(status_id = "0" OR status_id = "1")'
        ];

        $sta    =   Transactions::OpeStaticsReport($iData);

        $stat   =   ($sta['sta'] <> false) ? $sta['sta'] : [];

        $this->addPage('P');
        $this->setFillColor(0,34,96);
        // $this->setFillColor(0,176,240);
        // $this->setFillColor('#002260');
        $this->efectivo($stat);

        $this->Ln(4);
        $this->total($sta['form']);

        $this->Ln(4);
        $this->SetFont('','B',11);

        $this->Ln(6);
        $this->Cell(37, $this->height,'FIRMA DEL EJECUTIVO: ',  $this->debug,0,'C');
        $this->Cell(45, $this->height,'_______________________',$this->debug,0,'C');

        $this->Cell(38, $this->height,'RECIBIDO CONFORME: ',    $this->debug,0,'C');
        $this->Cell(75, $this->height,'_______________________ Fecha: '.date("Y-m-d").'',$this->debug,0,'C');

        return $this;
    }
    
    public function efectivo(array $data)
    {

        $this->cuerpo_tabla();
        $this->Ln($this->height/3);

        $this->Ln(3);
        $this->Cell(3);
        $this->SetFont('','',7);
        $this->Cell(96, $this->height,'CLIENTE ',1,0,'C');
        $this->Cell(12, $this->height,'FACTURA',1,0,'C');
        $this->Cell(13, $this->height,'TIPO ',1,0,'C');
        $this->Cell(18, $this->height,'FORMA',1,0,'C');
        $this->Cell(15, $this->height,'STATUS',1,0,'C');
        $this->Cell(12, $this->height,'RECIBIDO',1,0,'C');
        $this->Cell(12, $this->height,'CAMBIO',1,0,'C');
        $this->Cell(12, $this->height,'TOTAL',1,0,'C');

        if(!empty($data))
        {
            foreach ($data as $d => $dat) 
            {
                $this->Ln(7);
                $this->Cell(3);
                $this->SetFont('','', 7);
                $this->Cell(96, $this->height,  utf8_decode( (isset($dat['name'])) ? $dat['name'] : ''  ),$this->debug,0,'C');
                $this->Cell(12, $this->height,  utf8_decode( (isset($dat['bills'])) ? $dat['bills'] : ''  ),$this->debug,0,'C');
                $this->Cell(13, $this->height,  utf8_decode( (isset($dat['type'])) ? $dat['type'] : ''  ),$this->debug,0,'C');
                $this->Cell(18, $this->height,  utf8_decode( (isset($dat['form'])) ? $dat['form'] : ''  ),$this->debug,0,'C');
                $this->Cell(15, $this->height,  utf8_decode( (isset($dat['status'])) ? $dat['status'] : ''  ),$this->debug,0,'C');
                $this->Cell(12, $this->height,'$ '.utf8_decode( (isset($dat['total_dr'])) ? $dat['total_dr'] : ''  ).'',$this->debug,0,'C');
                $this->Cell(12, $this->height,'$ '.utf8_decode( (isset($dat['total_vc'])) ? $dat['total_vc'] : ''  ).'',$this->debug,0,'C');
                $this->Cell(12, $this->height,'$ '.utf8_decode( (isset($dat['total_fp'])) ? $dat['total_fp'] : ''  ).'',$this->debug,0,'C');
            
            }

        }else{
            $this->Ln(7);
            $this->Cell(3);
            $this->SetFont('','', 7);
            $this->Cell(96, $this->height,'',$this->debug,0,'C');
            $this->Cell(12, $this->height,'',$this->debug,0,'C');
            $this->Cell(13, $this->height,'',$this->debug,0,'C');
            $this->Cell(18, $this->height,'',$this->debug,0,'C');
            $this->Cell(15, $this->height,'',$this->debug,0,'C');
            $this->Cell(12, $this->height,'$ 0.00',$this->debug,0,'C');
            $this->Cell(12, $this->height,'$ 0.00',$this->debug,0,'C');
            $this->Cell(12, $this->height,'$ 0.00',$this->debug,0,'C');
        }
    
    }

    public function Total(array $dat)
    {

        $this->Ln(5);
        $this->Cell(3);
        $this->SetFont('','',7);
        $this->Cell(18, $this->height,'FORMA',  1,0,'C');
        $this->Cell(8,  $this->height,'CANT',   1,0,'C');
        $this->Cell(13, $this->height,'TOTAL',  1,0,'C');
        $this->Cell(19, $this->height,'FORMA',  1,0,'C');
        $this->Cell(8,  $this->height,'CANT',   1,0,'C');
        $this->Cell(13, $this->height,'TOTAL',  1,0,'C');
        $this->Cell(15, $this->height,'FORMA',  1,0,'C');
        $this->Cell(8,  $this->height,'CANT',   1,0,'C');
        $this->Cell(15, $this->height,'TOTAL',  1,0,'C');
        $this->Cell(15, $this->height,'FORMA',  1,0,'C');
        $this->Cell(8,  $this->height,'CANT',   1,0,'C');
        $this->Cell(15, $this->height,'TOTAL',  1,0,'C');
        $this->Cell(15, $this->height,'FORMA',  1,0,'C');
        $this->Cell(8,  $this->height,'CANT',   1,0,'C');
        $this->Cell(12, $this->height,'TOTAL',  1,0,'C');

        $this->Ln();
        $this->Cell(3);
        $this->SetFont('','', 7);
        $this->Cell(18, $this->height, 'EFECTIVO',       $this->debug,0,'C');
        $this->Cell(8,  $this->height, ( (isset($dat[1]['C'])) ? utf8_decode($dat[1]['C']) : "0"),            $this->debug,0,'C');
        $this->Cell(13, $this->height, '$ '.((isset($dat[1]['T'])) ? utf8_decode($dat[1]['T']) : "00.00").'', $this->debug,0,'C');
        $this->Cell(19, $this->height, 'TRANSFERENCIA',  $this->debug,0,'C');
        $this->Cell(8,  $this->height, ((isset($dat[2]['C'])) ? utf8_decode($dat[2]['C']) : "0"),            $this->debug,0,'C');
        $this->Cell(13, $this->height, '$ '.((isset($dat[2]['T'])) ? utf8_decode($dat[2]['T']) : "00.00"), $this->debug,0,'C');
        $this->Cell(15, $this->height, 'PAGO MOVIL',     $this->debug,0,'C');
        $this->Cell(8,  $this->height, ((isset($dat[3]['C'])) ? utf8_decode($dat[3]['C']) : "0"),            $this->debug,0,'C');
        $this->Cell(15, $this->height, '$ '.((isset($dat[3]['T'])) ? utf8_decode($dat[3]['T']) : "00.00"), $this->debug,0,'C');
        $this->Cell(15, $this->height,'PAYPAL',         $this->debug,0,'C');
        $this->Cell(8,  $this->height, ((isset($dat[4]['C'])) ? utf8_decode($dat[4]['C']) : "0"),            $this->debug,0,'C');
        $this->Cell(15, $this->height, '$ '.((isset($dat[4]['T'])) ? utf8_decode($dat[4]['T']) : "00.00"), $this->debug,0,'C');
        $this->Cell(15, $this->height,'ZELLE',          $this->debug,0,'C');
        $this->Cell(8,  $this->height, ((isset($dat[5]['C'])) ? utf8_decode($dat[5]['C']) : "0"),            $this->debug,0,'C');
        $this->Cell(12, $this->height, '$ '.((isset($dat[5]['T'])) ? utf8_decode($dat[5]['T']) : "00.00"), $this->debug,0,'C');

        $this->Ln();

    }

    public function encabezado_tabla($value='')
    {
        // fuente 14 y negrita
        $this->SetFont('','B',14);
        // texto color blanco
        $this->SetTextColor(255);
    }

    public function cuerpo_tabla($value='')
    {
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    }
}