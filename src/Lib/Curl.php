<?php

namespace App\Lib;

use \PDO;

use App\Lib\Config;

class Curl
{

	protected $host;
	protected $user;
	protected $pass;

	public function __construct($host="", $user="", $pass="")
	{
		$info = new Config();

		$this->user = $info->Curl()['user'];
		$this->pass = $info->Curl()['pass'];
		$this->host = $info->Curl()['host'];
	}

	public function execute($postfields)
    {
        $url    = $this->host;

        array_push($postfields, $this->user, $this->pass);

        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        
        $output = curl_exec($ch);

        if($output === false)
        {
            return ("cURL error: " . curl_error($ch));
        }else{

            curl_close($ch);
            
            return $output;
        }
    }

}