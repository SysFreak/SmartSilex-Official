<?php
namespace App\Lib;

use \PDO;

/**
 * 
 */
class OtherCode
{

    public static function ClientStatusLeads()
    {
        $date   =   date('Y-m-d');

        $query  =   'SELECT id, client_id, (SELECT username FROM users WHERE id = user_id) AS operador, (SELECT departament_id FROM users WHERE id = user_id) AS user_dep, (SELECT name FROM data_departament WHERE id = user_dep) AS departament, online, offline FROM cp_status_leads WHERE status_id = "1" AND online NOT LIKE "'.$date.'%" AND offline = "" ORDER BY id DESC';
        
        $info   =   DBSmart::DBQueryAll($query);

        $iData  =   $cn     =   "";

        if($info <>  false)
        {
            foreach ($info as $i => $inf) 
            {
                $query  =   'SELECT id FROM cp_status_leads WHERE id = "'.$inf['id'].'" AND offline != ""  ORDER BY id DESC';

                $res    =   DBSmart::DBQuery($query);
                
                if($res == false)
                {
                    $query  =   'SELECT name, phone_main FROM cp_leads WHERE client_id = "'.$inf['client_id'].'"';
                    $lead  =    DBSmart::DBQuery($query);

                    $iData[$cn++] = [
                        'Cliente'   =>  $inf['client_id'],
                        'Nombre'    =>  $lead['name'],
                        'Telefono'  =>  $lead['phone_main'],
                        'Operador'  =>  $inf['operador'],
                        'depart'    =>  $inf['departament'],
                        'Online'    =>  $inf['online'],
                        'Offline'   =>  $inf['offline'],

                    ];
                }
            }
        }else{
            return ['status' => false, 'iData' => 'SIN INFORMACION PARA MOSTRAR' ];
        }

        return ['status' => true, 'iData' => $iData ];
    }


    public static function ProcCierresPorEjecutivo()
    {
        $query  =   'SELECT id FROM users WHERE status_id = "1" AND departament_id = "2"';
        $users  =   DBSmart::DBQueryAll($query);
        $date   =   "2020-01";
        foreach ($users as $u => $val) 
        {
            $query  =   'SELECT (SELECT username FROM users WHERE id = "'.$val['id'].'") AS operador, (SELECT COUNT(*) FROM cp_leads WHERE created_by = "'.$val['id'].'" AND created_at BETWEEN "'.$date.'-01" AND "'.$date.'-31") AS creado, (SELECT COUNT(*) FROM cp_leads AS t1 INNER JOIN cp_appro_serv AS t2 ON (t1.client_id = t2.client_id) AND t1.created_by = "'.$val['id'].'" AND t1.created_at BETWEEN "'.$date.'-01" AND "'.$date.'-31") AS sometido, (SELECT (sometido *100)/creado) AS porc_sometido, (SELECT COUNT(*) FROM cp_leads AS t1 INNER JOIN cp_appro_serv AS t2 ON (t1.client_id = t2.client_id) INNER JOIN cp_coord_serv AS t3 ON (t2.ticket = t3.ticket) AND t1.created_by = "'.$val['id'].'" AND t1.created_at BETWEEN "'.$date.'-01" AND "'.$date.'-31") AS coordinado, (SELECT (coordinado*100)/sometido) AS porc_som_vs_coord FROM cp_leads WHERE created_by = "'.$val['id'].'" AND created_at BETWEEN "'.$date.'-01" AND "'.$date.'-31" LIMIT 1';

            $info  =   DBSmart::DBQuery($query);

            $iData[$u]  =   [
                'Operador'          =>  $info['operador'],
                'Lead_Creados'      =>  $info['creado'],
                'Lead_Sometidos'    =>  $info['sometido'],
                'Porc_Sometidos'    =>  $info['porc_sometido'],
                'Lead_Coordinados'  =>  $info['coordinado'],
                'Porc_Som_VS_Coord' =>  $info['porc_som_vs_coord']
            ];
        }
        ddd($iData);
    } 

}