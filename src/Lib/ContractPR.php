<?php
namespace App\Lib;

// require '../vendor/autoload.php';

use FPDF;

// ddd('FPDF_FONTPATH', realpath('fonts/'));
define('FPDF_FONTPATH', realpath( 'fonts/' ));

class ContractPR extends FPDF
{
    
    public $debug = 0;
    public $height = 7;

    function __construct($orientation='P', $unit='mm', $size='letter')
    {
        
        parent::__construct($orientation, $unit, $size);
        $this->SetLineWidth(0.4);
        $this->AddFont('Calibri','', 'calibri.php');
        $this->AddFont('Calibri','B','calibrib.php');
        $this->AddFont('Calibri','I','calibrii.php');
    }

    // public function Header()
    // {  

    //     $this->Image('assets/img/logo-boom-net.png',85,10,0,20);
    //     $this->Ln(20);
    //     $this->SetFont('Calibri','B',10);
    //     $this->Cell(0,5,'Boom Net / Connectivity Solutions LLC.',$this->debug,1,'C');
        
    //     $this->SetFont('Calibri','',10);
    //     $this->SetFontsize(10);
    //     $this->Cell(0,5,'P.O Box 5849',$this->debug,1,'C');
    //     $this->Cell(0,5,'Caguas P.R. 00726',$this->debug,1,'C');
    //     $this->Cell(0,5,'Tel: 787-333-0231',$this->debug,1,'C');
    //     $this->Cell(0,5,'email: servicio.cliente@boomsolutionspr.com',$this->debug,1,'C');
    // }

    public function Contract()
    {
        $this->addPage('P');

        $this->Image('assets/img/boomNET.png',85,10,0,20);
        $this->Ln(20);
        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Boom Net / Connectivity Solutions LLC.',$this->debug,1,'C');
        
        $this->SetFont('Calibri','',10);
        $this->SetFontsize(10);
        $this->Cell(0,5,'P.O Box 5849',$this->debug,1,'C');
        $this->Cell(0,5,'Caguas P.R. 00726',$this->debug,1,'C');
        $this->Cell(0,5,'Tel: 787-333-0231',$this->debug,1,'C');
        $this->Cell(0,5,'email: servicio.cliente@boomsolutionspr.com',$this->debug,1,'C');


        $this->SetFont('Calibri','',10);
        $this->Cell(35, $this->height,'Invoice Number: ',$this->debug,0,'R');
        $this->Cell(65, $this->height,'123456789',$this->debug,0,'L');
        $this->Cell(70, $this->height,'Account Number: ',$this->debug,0,'R');
        $this->Cell(30, $this->height,'987654321',$this->debug,0,'L');
        $this->Ln(3);

        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Customer Agreement',$this->debug,0,'C');
        $this->Ln($this->height);
        
        $this->SetFont('Calibri','',10);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();

        $this->Cell(22, $this->height, 'Name: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, 'WANDA IVETTE PENA TORRES',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Social Security Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(40, $this->height, 'xxx-xx-6863',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Date: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, '04/03/2020',$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Postal Add: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(136, $this->height, 'URB PASEO SAN LORENZO CALLE TOPACIO J14 SAN LORENZO', $this->debug, 0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(10, $this->height, 'Sale: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, 'LUIS.TR318', $this->debug, 0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Physical Add: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(133, $this->height, 'URB PASEO SAN LORENZO CALLE TOPACIO J14 SAN LORENZO', $this->debug, 0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(13, $this->height, 'Phone: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, '7876895514', $this->debug, 0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Bank: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, 'BANCO POPULAR',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Account Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(33, $this->height, 'xxxx-1254',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(18, $this->height, 'Phone Alt: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(10, $this->height, '7876895514',$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Email: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, 'wandagym22@yahoo.com',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Credit Card Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(35, $this->height, 'xxxx-xxxx-xxxx-6863',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(16, $this->height, 'Exp Date: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, '03/2020',$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Residencia: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, 'PROPIA CON ESCRITURA',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Tipo: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(37, $this->height, 'MADERA',$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(14, $this->height, 'Niveles: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, 'EDIFICIO',$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Service: ',1,0,'C');
        $this->Cell(50, $this->height, 'Monthly Payment',1,0,'C');
        $this->Cell(45, $this->height, 'Router Purchase: ',1,0,'C');
        $this->Cell(37, $this->height, 'Activation Fee',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '','T',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, '5MB / 1MB',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ 39.99',1,0,'C');
        $this->Cell(45, $this->height, '',1,0,'C');
        $this->Cell(37, $this->height, '$ 99.99 Fee',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '','',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Telephone',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->Cell(45, $this->height, 'Router Lease: Yes',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(37, $this->height, '',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Other',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ 0.00',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Days until 30',1,0,'C');
        $this->Cell(50, $this->height, 'Proration Payment',1,0,'C');
        $this->Cell(45, $this->height, 'Equipment',1,0,'C');
        $this->Cell(37, $this->height, 'Activation Fee Total','R',0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','',8);
        $this->Cell(22, $this->height, '26',1,0,'C');
        $this->Cell(50, $this->height, '$ 30.32',1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ 9.99',1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->setFillColor(160,160,160);
        $this->Cell(42, $this->height, '$ 99.99','T',0,'C');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }

        $this->SetFont('Calibri','',8);
        $this->Ln(1);

        $this->MultiCell(196,3,utf8_decode('Boom Net / Connectivity Solutions LLC.  Proveerá servicio de acceso a Internet utilizando antenas y equipo inalámbrico propiedad de Boom Net / Connectivity Solutions LLC. El Servicio de acceso a Internet Inalámbrico ofrecido al cliente es basado en la selección de servicio hecha por el cliente y consiste de una velocidad con tope igual al acordado según se especifica en este contrato, usted se compromete a mantener una velocidad igual y/o mayor a la que establece este contrato, de haber una reducción en la  velocidad de su servicio de internet Boom Net/ Connectivity Solutions LLC. Estará facturando un cargo de $15.00. Al firmar este contrato usted se compromete a cumplir con los siguientes términos: Este contrato tiene una vigencia  de  24_______  meses calendario a partir de la fecha de instalación y comienzo de servicio, usted se compromete a mantener este servicio sin interrupción, por falta de pago, desconexión voluntaria de la misma por el término de 2______años. Luego de cumplir los tres (2) años de contrato el servicio será facturado mes a mes sin compromiso de contrato.  El cliente habrá de pagar un cargo mensual recurrente por acceso al Internet, por mantenimiento del equipo instalado y por el costo y mantenimiento del “wireless local loop”, si aplicase, según el mismo surge de este contrato. Boom Net / Connectivity Solutions LLC. facturará al cliente de manera mensual por los servicios presentados dentro de los primeros diez (10) días del mes, por medio de su correo electrónico provisto. De  no recibir su factura dentro del primer ciclo , estará obligado a informar inmediatamente a Boom Net Connectivity Solutions LLC.  El cliente habrá de pagar dicha factura dentro de los diez (10) días siguientes al recibo de la factura. Los pagos se debitaran automáticamente de la cuenta de banco o tarjeta de crédito provista por el cliente, de ser una cuenta de banco su método de pago Connectivity Solutions LLC/Boom Net facturará un cargo mensual de $5.00 adicional en su factura ,de la misma forma usted autoriza a Boom Net / Connectivity Solutions LLC a realizar la gestión de cobro por algún tipo de cancelación de contrato, servicio técnico, mensualidad en atraso o compra de alguna parte del equipo a la tarjeta de crédito o cuenta de banco provista por usted en el contrato, sin que Boom Net / Connectivity Solutions LLC necesite su firma o autorización en el recibo de compra y usted está de acuerdo con todo lo anterior convenido. Connectivity Solutions LLC. / BoomNet le realizará un cargo de $5.00 a su cuenta por cada pago devuelto o declinado de la cuenta de banco provista. Boom Net / Connectivity Solutions LLC. le ofrece la opción de adquirir  un "Router" en un módico precio de acuerdo a su modelo, el costo del mismo deberá ser pagado al técnico en el momento de la instalación de nuestros servicios. En el caso que Boom Net / Connectivity Solutions LLC le rente un "Router" el mismo tendría un costo mensual de $5.00 adicionales en su mensualidad y este equipo será propiedad de Boom Net/ Connectivity Solutions LLC en todo momento.  De usted decidir entregar el "Router" rentado, debe hacerlo en una de nuestras oficinas mas cercanas y el mismo será evaluado y recibido solo si está en buenas condiciones físicas y de funcionamiento completo, de lo contrario se le estará facturando el valor del equipo fijado en nuestra lista de precios a la fecha de entrega. En caso de desconexión por falta de pago el cliente pagara $20.00 por reconexión una vez se realice el pago que este atrasado. El equipo utilizado para proveer los servicios de Internet será en todo momento propiedad de Boom Net / Connectivity Solutions LLC. El cliente tiene la obligación de reportar a Boom Net / Connectivity Solutions LLC cualquier tipo de interrupción o degradación al servicio tan pronto advenga en conocimiento de ello. Boom Net / Connectivity Solutions LLC garantiza que el ancho de banda y el servicio será continuo, aunque en ocasiones puede haber ciertas degradaciones del ancho de banda por razones no imputables de Boom Net / Connectivity Solutions LLC. También puede haber interrupción de servicio por causas ajenas a de Boom Net / Connectivity Solutions LLC, como lo serían las averías al cable submarino que transporta data desde Puerto Rico al extranjero. En caso de una interrupción del servicio de acceso por una falla en el sistema que opera y mantiene de Boom Net / Connectivity Solutions LLC. , y si la misma dura más de 24  horas a partir de la notificación de la avería a de Boom Net / Connectivity Solutions LLC, el cliente recibirá un crédito por cada día que este fuera de servicio. Este crédito no aplicara cuando la interrupción del servicio se deba a causas ajenas a de Boom Net / Connectivity Solutions LLC. Se aplicará un cargo a su factura mensual de $3.00 por concepto de Plan de Servicio. Este seguro  cubrirá cualquier  servicio relacionado  con  ubicación de antena (señal), mantenimiento de equipo defectuoso  o reemplazo de equipo afectado por exceso de voltaje.  Este seguro no cubrirá mudanza de equipo o algún evento de fuerza mayor y/o actos de la naturaleza como se mencionará más adelante.  En caso de que el cliente requiera que de Boom Net / Connectivity Solutions LLC rinda servicios adicionales a lo contratado, tales como servicios de configuración de equipo o mantenimiento de equipo que no sea el de Boom Net / Connectivity Solutions LLC, se le hará un cargo al cliente basado en las tarifas.'),$this->debug);
        $this->Ln($this->height);

        $this->addPage('P');
        $this->MultiCell(196,3,utf8_decode('Esos cargos aplicarán también a cualquier situación donde el cliente o cualquier otro personal de manera alguna alteren el servicio Boom Net / Connectivity Solutions LLC y requiera la asignación de personal para restaurar el servicio. Antes de realizar el trabajo Boom Net / Connectivity Solutions LLC  le notificara al cliente la naturaleza del trabajo a ser realizado, la razón por la que dicho trabajo es necesario, y el costo estimado de dicho servicio. Solo si el cliente lo autoriza se  procederá a realizar el trabajo. Boom Net / Connectivity Solutions LLC facturara al cliente inmediatamente por dichos servicios adicionales. Boom Net / Connectivity Solutions LLC facturará un cargo de $50.00 por concepto de mudanza de equipo. Boom Net / Connectivity Solutions LLC  no será responsable  por ninguna interrupción del servicio que se deba a eventos de fuerza mayor o actos  de la naturaleza, o por razones que estén fuera del control de Boom Net / Connectivity Solutions LLC.  Como resultado de culpa o negligencia de otros. Dichos actos incluirá pero no se limitara a: fuego, huracanes u otras condiciones climatológicas similares, inundaciones, terremotos, desastres naturales, motines, epidemias, actos de guerra, actos de vandalismo, robo y otros. El cliente se identificara y se establecerá como punto de contacto primario entre Boom Net / Connectivity Solutions LLC y el cliente en cuanto a cualquier aspecto del cumplimiento de este contrato y los servicios acordados. Se designa como contacto a la persona que firma este contrato y su nombre aparece escrito en él, y la información de cómo comunicarse con él es la provista en este contrato de servicio. En caso de emergencia, cualquier representante del cliente podrá contactar a Boom Net / Connectivity Solutions LLC. Si el cliente al recibo de una factura objeta uno o más cargos que en ella puedan aparecer, habrá de notificar a Boom Net / Connectivity Solutions LLC dentro de un plazo no mayor de diez (10) días a partir del recibo de dicha factura de que objeta todo o parte de la factura. En caso de que se objete solo parte de la factura, el cliente deberá remitir dentro de dicho plazo de diez (10) días el pago por aquella parte que no objeta. Boom Net / Connectivity Solutions LLC evaluara la objeción del cliente y le notificara su posición dentro de los veinte (20) días siguientes al recibo de la objeción.  El equipo de comunicación utilizado para proveer el servicio al cliente es propiedad de Boom Net / Connectivity Solutions LLC, y al final del contrato el mismo será removido por Boom Net / Connectivity Solutions LLC sin costo alguno para el cliente. Será deber del cliente no realizar ningún acto que pueda dañar el equipo o ponerlo en riesgo de pérdida. Cualquier daño sufrido por el equipo causado por la negligencia del cliente será responsabilidad del cliente. En caso de que usted (cliente) incumpla con cualquier obligación establecida en el mismo, la otra Parte (la compañía) le remitirá por transmisión facsímile, por correo electrónico o por correo regular un aviso de que se está violando o incumpliendo el contrato, con una descripción especifica de lo que constituye dicha violación. Al recibir dicha notificación tendrá la obligación de corregir la deficiencia dentro de un término no mayor de diez (10) días calendarios a partir del recibo de dicha notificación. En caso de que no se cumpla con esta solicitud, y asumiendo que le asista la razón,  entonces Boom Net / Connectivity Solutions LLC. Podrá solicitar la cancelación del contrato por justa causa. Boom Net/ Connectivity Solutions LLC le ofrece la alternativa del servicio de  telefonía. De usted adquirir este servicio, se compromete a no trasladar el dispositivo ATA a otra dirección por motivos de 911. De igual forma todo equipo instalado de telefonía pertenece  en todo momento a Boom Net / Connectivity Solutions LLC. Solo puede adquirir y mantener el servicio de telefonía en conjunto a nuestros servicios de internet. De ocurrir una cancelación del servicio de telefonía, entiéndase por falta de pago y/o voluntaria, se determinará como incumplimiento de contrato, por lo que usted autoriza a Boom Net / Connectivity Solutions LLC realizarle un único cargo por concepto de cancelación de $175.00 a su tarjeta de crédito o cuenta de banco provista. Por su parte, Boom Net / Connectivity Solutions LLC podrá cancelar el contrato por justa causa si el cliente dejase de pagar cualquier factura o incumple cualquier obligación contraída en este contrato y no subsane dicha deficiencia dentro de un periodo de diez (10) días luego de serle notificada dicha deficiencia. En caso de que el cliente cancele este contrato sin justa causa, o si Boom Net / Connectivity Solutions LLC tiene que cancelar el contrato por el incumplimiento del mismo por parte del cliente, Boom Net / Connectivity Solutions LLC interrumpirá de inmediato el servicio y removerá el equipo instalado dentro de un periodo razonable, de no ser posible el recogido o entrega de equipo por parte del cliente, se le estará facturando un cargo de $480.00 por concepto de costos de equipo. En caso de cancelación temprana por parte del cliente, este deberá pagar la mensualidad correspondiente, por cada mes que falte desde la fecha en la que manifieste su deseo de no continuar con el servicio hasta la terminación de su contrato. Para el cálculo de este pago, se tomará en cuenta el monto de la mensualidad más alta del paquete que el cliente haya disfrutado en los últimos 6 meses de servicio. "BOOMNET / Connectivity Solutions LLC" Referirá esta acción a AMER ASSITS A/R SOLUTIONS. INC. Quienes iniciaran las gestiones de cobro y reportaran a todas las agencias de crédito, afectando su historial a raíz de un incumplimiento de su parte y/o refiriendo esta cuenta a su división legal para trámites de rigor. De  ser necesario Referir su cuenta a una agencia de cobro, usted será responsable  de asumir los gastos por concepto de las gestiones de esta, así como los honorarios  de abogado y trámites legales  y/o judiciales. . Boom Net/ Connectivity Solutions LLC  acuerda salvaguardar y proteger al cliente contra cualquier reclamación, multa, imposición, penalidad o acción legal que pueda ser iniciada contra el cliente por actos u omisiones de Boom Net / Connectivity Solutions LLC que puedan violar leyes federales, estatales o municipales que regulen la instalación y/u operación del servicio que se ha acordado en este contrato o de cualquier actuación de Boom Net / Connectivity Solutions LLC en el cumplimiento de este contrato que pueda dar base a tal acción o cualquier acto que cause daños a terceros. El cliente acuerda salvaguardar y proteger a Boom Net / Connectivity Solutions LLC contra cualquier reclamación, multa, imposición, penalidad o acción legal que pueda ser iniciada contra Boom Net / Connectivity Solutions LLC por actos u omisiones del cliente  que puedan violar leyes federales, estatales o municipales que regulen la utilización del servicio que se ha acordado en este contrato o de cualquier actuación del cliente en el cumplimiento de este contrato que pueda dar base a tal acción o cualquier acto que cause daños a terceros. Boom Net / Connectivity Solutions LLC no será responsable por el mal uso del sistema o abuso del sistema por parte del cliente. Será responsabilidad del cliente el mantener las salvaguardas necesarias para proteger la integridad de su sistema en todo momento y protegerlo contra virus, hacking, spam attacks y otra conducta similar. Este contrato y sus anejos, si alguno, constituyen el acuerdo completo entre las partes y no existe ninguna representación, promesa o acuerdo que no se haya desglosado en el mismo.Este contrato solo podrá ser enmendado mediante acuerdo escrito físico, por correo eletrónico y llamadas grabadas donde se evidencie el acuerdo de las partes. Cláusula de USO LEGAL: Está prohibido el uso del servicio para transmitir, extraer, ver, recibir, o de cualquier forma manejar cualquier material, imagen, escrito o contenido que viole cualquier disposición de ley estatal, federal o tratado internacional.  La violación de lo anterior será considerada violación de este contrato y Boom Net / Connectivity Solutions LLC podrá, a su sola discreción, descontinuar el servicio al cliente, mediante notificación simultánea al efecto sin honrar ningún periodo de gracia. Cláusula de Cesión: Boom Net / Connectivity Solutions LLC se reserva el derecho de ceder el presente contrato a un tercero sin que se requiera la autorización del cliente. Este acuerdo será interpretado en acorde con las leyes del Estado Libre Asociado de Puerto Rico, y cualquier reclamación que pueda surgir a raíz del mismo se tramitara ante el Tribunal de Primera Instancia, Sala de Fajardo, o ante el Tribunal Federal de Distrito, según sea el caso. El hecho de que Boom Net / Connectivity Solutions LLC opte por no ejercitar cualquiera de las prerrogativas y derechos concedidos en este contrato no será interpretado como una renuncia al derecho de ejercer tal derecho en un futuro.  Al firmar este contrato usted reconoce  y acuerda que usted  ha recibido , leído, comprendido y aceptado Voluntariamente y está sujeto a todos los términos y condiciones establecidos a este acuerdo   y que todos los términos se especificaron  antes de la activación.'), $this->debug);
            
            $this->Ln($this->height);

        // $this->SetFont('Calibri','B',14);
        // $this->Write(5,'Boom');
        // $this->SetFont('Calibri','',14);
        // $this->Write(5,'Solutions');


        return $this;
    }
    public function encabezado_tabla($value='')
    {
        // fuente 14 y negrita
        $this->SetFont('','B',14);
        // texto color blanco
        $this->SetTextColor(255);
    }

    public function cuerpo_tabla($value='')
    {
        $this->setFillColor(160,160,160);
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    }
}