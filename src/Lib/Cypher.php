<?php
namespace App\Lib;

/**
 * Clase diseñada para cifrar y descifrar datos
 * 
 * solo requiere ser instanciada pasando como parametro una encryption key
 * usa el algoritmo rijndael en modo CBC (cipher block chaining) y genera
 * automaticamente un vector de inicializacion
 * 
 * Basado en el componente illuminate/encryption parte del framework Laravel
 * originalmente hecho por Taylor Otwell <taylor@laravel.com>
 * 
 * Dependencias
 * 1 ext-openssl (extension de PHP): para usar las funciones: openssl_encrypt() y
 *   openssl_decrypt()
 * 
 * 2 paragonie/random_compat (Paquete en packagist.org): para el polyfill de la
 *   funcion random_bytes()
 * 
 * 3 symfony/polyfill-php56 (Paquete en packagist.org): para el polyfill de la
 *   funcion hash_equals()
 * 
 *  Las dependencias 2 y 3 son necesarias para que esta clase pueda funcionar en
 *  PHP 5.3 de los repositorios de CentOS 6.8
 *  
 * @author Luis Campos <campos.luis19@gmail.com>
 */
class Cypher
{
    /**
     * llave de encripcion.
     *
     * @var string
     */
    protected $key;

    /**
     * Algoritmo usado para encriptar.
     *
     * @var string
     */
    protected $cipher;

    public function __construct($key, $cipher = 'AES-256-CBC')
    {
        $key = (string) $key;

        if (static::supported($key, $cipher)) {
            $this->key = $key;
            $this->cipher = $cipher;
        } else {
            throw new \RuntimeException('The only supported ciphers are AES-128-CBC and AES-256-CBC with the correct key lengths.');
        }
    }

    public static function supported($key, $cipher)
    {
        $length = mb_strlen($key, '8bit');

        return ($cipher === 'AES-128-CBC' && $length === 16) ||
               ($cipher === 'AES-256-CBC' && $length === 32);
    }

    /**
     * Cifra el valor dado
     *
     * @param  mixed  $value
     * @return string
     *
     * @throws \Exception
     */
    public function encrypt($value, $keygen)
    {
        $iv = random_bytes(16);

        $value = \openssl_encrypt(
            serialize($value),
            $this->cipher, $keygen, 0, $iv
        );

        if ($value === false) {
            throw new \Exception('Could not encrypt the data.');
        }

        $mac = $this->hash($iv = base64_encode($iv), $value, $keygen);

        $json = json_encode(compact('iv', 'value', 'mac'));

        if (! is_string($json)) {
            return ['status' => false, 'message' => 'Could not encrypt the data.'];
        }else{
            return ['status' => true, 'cypher' => base64_encode($json), 'keygen' => $keygen, 'json' => json_decode($json)];
        }

        // base64_encode($json);
    }

    /**
     * Decrypt the given value.
     *
     * @param  mixed  $payload
     * @return string
     *
     * @throws \Exception
     */
    public function decrypt($payload, $keygen)
    {

        $payload = $this->getJsonPayload($payload, $keygen);

        $iv = base64_decode($payload['iv']);

        $decrypted = \openssl_decrypt(
            $payload['value'], $this->cipher, $keygen, 0, $iv
        );

        if ($decrypted === false) {
            throw new \Exception('Could not decrypt the data.');
        }

        return unserialize($decrypted);
    }

    /**
     * Crea el MAC para la concatenacion del vector de inicializacion y el valor cifrado
     * 
     *
     * @param  string  $iv
     * @param  mixed  $value
     * @return string
     */
    protected function hash($iv, $value, $keygen)
    {
        return hash_hmac('sha256', $iv.$value, $keygen);
    }

    /**
     * obtener el arreglo del "payload" en formato json
     *
     * @param  string  $payload
     * @return array
     *
     * @throws \Exception
     */
    protected function getJsonPayload($payload, $keygen)
    {
        $payload = json_decode(base64_decode($payload), true);

        if (! $this->validPayload($payload, $keygen)) {
            throw new \Exception('The payload is invalid.');
        }

        if (! $this->validMac($payload, $keygen)) {
            throw new \Exception('The MAC is invalid.');
        }

        return $payload;
    }

    /**
     * Verificar que el payload es valido.
     *
     * @param  mixed  $payload
     * @return bool
     */
    protected function validPayload($payload, $keygen)
    {
        return is_array($payload) && isset(
            $payload['iv'], $payload['value'], $payload['mac']
        );
    }

    /**
     * Determinar si el MAC de este "payload" es valido.
     *
     * @param  array  $payload
     * @return bool
     */
    protected function validMac($payload, $keygen)
    {
        $calculated = $this->calculateMac($payload, $bytes = random_bytes(16), $keygen);

        return hash_equals(
            hash_hmac('sha256', $payload['mac'], $bytes, true), $calculated
        );
    }

    /**
     * Calcular el hash del "payload" dado
     *
     * @param  array  $payload
     * @param  string  $bytes
     * @return string
     */
    protected function calculateMac($payload, $bytes, $keygen)
    {
        return hash_hmac(
            'sha256', $this->hash($payload['iv'], $payload['value'], $keygen), $bytes, true
        );
    }

}