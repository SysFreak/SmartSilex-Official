<?php
namespace App\Lib;

use Silex\Application;
use App\Models\Logger;

class DataLogger
{
	public static function RecordLogger($info)
	{
		$record 	= Logger::create();
		$record->channel 	= 	$info['channel'];
		$record->client 	= 	$info['client'];
		$record->message 	=	$info['message'];
		$record->time 		=	$info['time'];
		$record->ip 		=	$_SERVER['REMOTE_ADDR'];
		$record->username 	=	$info['username'];
		$record->save();
	}
}