<?php

namespace App\Lib;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use MySQLHandler\MySQLHandler;
use Symfony\Component\HttpFoundation\Response;
use PDO;


class BaseMultiLogger extends Logger
{
    protected $users_looger;
    protected $technical_visit_looger;
    
    function __construct($pdo=null,$host='localhost',$db='boomcrm_solu',$dbuser='root',$dbpass='M1cha3l')
    {
        $this->pdo   = ($pdo != null) ? $pdo : new pdo("mysql:host=".$host.";dbname=".$db.";",$dbuser,$dbpass);

        $DbHandler   = new MySQLHandler($this->pdo, "log", array('ip','username'));
        $filehandler = new StreamHandler(__DIR__.'/system.log');

        $this->users_login = new Logger('Login');
        $this->users_login->pushHandler($filehandler);
        $this->users_login->pushHandler($DbHandler);

        $this->users_logout = new Logger('LogOut');
        $this->users_logout->pushHandler($filehandler);
        $this->users_logout->pushHandler($DbHandler);

        $this->users_change = new Logger('PassChange');
        $this->users_change->pushHandler($filehandler);
        $this->users_change->pushHandler($DbHandler);

    }
    
}