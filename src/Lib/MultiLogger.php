<?php

namespace App\Lib;

/**
 * 
 */
 class MultiLogger extends BaseMultiLogger
 {
     
    public function Login($usuario)
    {
        $this->users_login->addInfo(
            'LogIn',
            array(
                'username'  =>  $usuario, 
                'ip'        =>  $_SERVER['REMOTE_ADDR']
            )
        );
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function Logout($usuario)
    {
        $this->users_logout->addInfo(
            'LogOut',  
            array(
                'username'  =>  $usuario, 
                'ip'        =>  $_SERVER['REMOTE_ADDR']
            )
        );
    
    }

///////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function Passchange($usuario)
    {
        $this->users_change->addInfo(
            'PassChange',
            array(
                'username'  =>  $usuario, 
                'ip'        =>  $_SERVER['REMOTE_ADDR']
            )
        );
    
    }


///////////////////////////////////////////////////////////////////////////////////////////////////

 } 