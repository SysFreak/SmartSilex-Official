<?php

namespace App\Lib;

use \PDO;

class Functions
{

////////////////////////////////////////////////////////////////////////////////////////////////////
	public function AuthorizeExampled() {

		return $Authorize = array(
			'AUTHORIZE_USER'			=> 	'556KThWQ6vf2',
			'AUTHORIZE_PASSWORD'		=> 	'9ac2932kQ7kN2Wzq',
			'AUTHORIZE_TRANSACTIONTYPE'	=> 	'authCaptureTransaction'
		);
	
	}

    public function AgeClient($date)
    {
        $tiempo = strtotime($date); 
        $ahora  = time(); 
        $edad   = ($ahora-$tiempo)/(60*60*24*365.25);     
        return  floor($edad); 
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static function LeadsSmart($p, $cl)
    {
    	$age = 0;

    	if($p['birthdate'] <> '') 
    	{
    		$tiempo = strtotime($p['birthdate']); 
        	$ahora  = time(); 	
        	$edad   = ($ahora-$tiempo)/(60*60*24*365.25);     
        	$age  	= floor($edad); 

    	}else{ $age = 0 ;}

        return array(
            'id_c'                  =>  $p['id_c'],
            'client_id'             =>  $cl,
            'referred_id'           =>  '',
            'name'                  =>  ($p['name'] <> '' )                ? mb_convert_encoding($p['name'], 'UTF-8', 'UTF-8') : '',
            'birthday'              =>  ($p['birthdate'] <> '' )           ? $p['birthdate'] : '',
            'gender'                =>  'M',
            'age'                   =>  ($p['birthdate'] <> '' )           ? $age : '',
            'phone_main'            =>  ($p['phone_main'] <> '' )          ? $p['phone_main'] : '',
            'phone_main_owner'      =>  ($p['name'] <> '' )                ? mb_convert_encoding($p['name'], 'UTF-8', 'UTF-8') : '',
            'phone_main_provider'   =>  ($p['phone_main_provider'] <> '' ) ? $p['phone_main_provider'] : '1',
            'phone_alt'             =>  ($p['phone_alt'] <> '' )         ? $p['phone_alt'] : '',
            'phone_alt_owner'       =>  ($p['name'] <> '' )                ? mb_convert_encoding($p['name'], 'UTF-8', 'UTF-8') : '',
            'phone_alt_provider'    =>  ($p['phone_alt_provider'] <> '' )  ? $p['phone_alt_provider'] : '1',
            'phone_other'           =>  '',
            'phone_other_owner'     =>  '',
            'phone_other_provider'  =>  '1',
            'email_main'            =>  ($p['email_main'] <> '')           ? strtoupper($p['email_main']) : '',
            'email_other'           =>  '',
            'add_main'              =>  ($p['add_main'] <> '' )            ? mb_convert_encoding($p['add_main'], 'UTF-8', 'UTF-8') : '',
            'add_postal'            =>  ($p['add_postal'] <> '' )          ? mb_convert_encoding($p['add_postal'], 'UTF-8', 'UTF-8') : '',
            'coor_lati'             =>  '',
            'coor_long'             =>  '',
            'country_id'            =>  '1',
            'town_id'               =>  ($p['town_id'] <> '' )             ? $p['town_id']     : '',
            'zip_id'                =>  '1',
            'h_type_id'             =>  ($p['h_type_id'] <> '' )           ? $p['h_type_id']   : '',
            'h_roof_id'             =>  ($p['h_roof_id'] <> '' )           ? $p['h_roof_id']   : '',
            'h_level_id'            =>  '1',
            'ident_id'              =>  '',
            'ident_exp'             =>  '',
            'ss_id'                 =>  '',
            'ss_exp'                =>  '',
            'mkt_origen_id'         =>  '1',
            'mkt_medium_id'         =>  '1',
            'mkt_objective_id'      =>  '1',
            'mkt_post_id'           =>  '1',
            'mkt_forms_id'          =>  '1',
            'mkt_conv_id'           =>  '1',
            'mkt_service_id'        =>  '1',
            'mkt_campaign_id'       =>  '1',
            'additional'            =>  ($p['description'] <> '' )          ? mb_convert_encoding($p['description'], 'UTF-8', 'UTF-8') : '',
            'created_by'            =>  ($p['user_id'] <> '' )              ? $p['user_id'] : '134',
            'created_at'            =>  ($p['date_entered'] <> '' )         ? $p['date_entered'] :  date("Y-m-d h:m:s")
        );

    }

    public static function DelCharAll()
    {
        $char[1]    =   array('name' => 'a', 'change' => '');
        $char[2]    =   array('name' => 'b', 'change' => '');
        $char[3]    =   array('name' => 'c', 'change' => '');
        $char[4]    =   array('name' => 'd', 'change' => '');
        $char[5]    =   array('name' => 'e', 'change' => '');
        $char[6]    =   array('name' => 'f', 'change' => '');
        $char[7]    =   array('name' => 'g', 'change' => '');
        $char[8]    =   array('name' => 'h', 'change' => '');
        $char[9]    =   array('name' => 'i', 'change' => '');
        $char[10]   =   array('name' => 'j', 'change' => '');
        $char[11]   =   array('name' => 'k', 'change' => '');
        $char[12]   =   array('name' => 'l', 'change' => '');
        $char[13]   =   array('name' => 'm', 'change' => '');
        $char[14]   =   array('name' => 'n', 'change' => '');
        $char[15]   =   array('name' => 'o', 'change' => '');
        $char[16]   =   array('name' => 'p', 'change' => '');
        $char[17]   =   array('name' => 'q', 'change' => '');
        $char[18]   =   array('name' => 'r', 'change' => '');
        $char[19]   =   array('name' => 's', 'change' => '');
        $char[20]   =   array('name' => 't', 'change' => '');
        $char[21]   =   array('name' => 'u', 'change' => '');
        $char[22]   =   array('name' => 'v', 'change' => '');
        $char[23]   =   array('name' => 'w', 'change' => '');
        $char[24]   =   array('name' => 'x', 'change' => '');
        $char[25]   =   array('name' => 'y', 'change' => '');
        $char[26]   =   array('name' => 'z', 'change' => '');
        $char[27]   =   array('name' => 'A', 'change' => '');
        $char[28]   =   array('name' => 'B', 'change' => '');
        $char[29]   =   array('name' => 'C', 'change' => '');
        $char[30]   =   array('name' => 'D', 'change' => '');
        $char[31]   =   array('name' => 'E', 'change' => '');
        $char[32]   =   array('name' => 'F', 'change' => '');
        $char[33]   =   array('name' => 'G', 'change' => '');
        $char[34]   =   array('name' => 'H', 'change' => '');
        $char[35]   =   array('name' => 'I', 'change' => '');
        $char[36]   =   array('name' => 'J', 'change' => '');
        $char[37]   =   array('name' => 'K', 'change' => '');
        $char[38]   =   array('name' => 'L', 'change' => '');
        $char[39]   =   array('name' => 'M', 'change' => '');
        $char[40]   =   array('name' => 'N', 'change' => '');
        $char[41]   =   array('name' => 'O', 'change' => '');
        $char[42]   =   array('name' => 'P', 'change' => '');
        $char[43]   =   array('name' => 'Q', 'change' => '');
        $char[44]   =   array('name' => 'R', 'change' => '');
        $char[45]   =   array('name' => 'S', 'change' => '');
        $char[46]   =   array('name' => 'T', 'change' => '');
        $char[47]   =   array('name' => 'U', 'change' => '');
        $char[48]   =   array('name' => 'V', 'change' => '');
        $char[49]   =   array('name' => 'W', 'change' => '');
        $char[50]   =   array('name' => 'X', 'change' => '');
        $char[51]   =   array('name' => 'Y', 'change' => '');
        $char[52]   =   array('name' => 'Z', 'change' => '');
        $char[53]   =   array('name' => 'á', 'change' => 'A');
        $char[54]   =   array('name' => 'à', 'change' => 'A');
        $char[55]   =   array('name' => 'ä', 'change' => 'A');
        $char[56]   =   array('name' => 'â', 'change' => 'A');
        $char[57]   =   array('name' => 'ª', 'change' => 'A');
        $char[58]   =   array('name' => 'Á', 'change' => 'A');
        $char[59]   =   array('name' => 'À', 'change' => 'A');
        $char[60]   =   array('name' => 'Â', 'change' => 'A');
        $char[61]   =   array('name' => 'Ä', 'change' => 'A');
        $char[62]   =   array('name' => 'Ã', 'change' => 'A');
        $char[63]   =   array('name' => 'é', 'change' => 'E');
        $char[64]   =   array('name' => 'è', 'change' => 'E');
        $char[65]   =   array('name' => 'ë', 'change' => 'E');
        $char[66]   =   array('name' => 'ê', 'change' => 'E');
        $char[67]   =   array('name' => 'É', 'change' => 'E');
        $char[68]   =   array('name' => 'È', 'change' => 'E');
        $char[69]   =   array('name' => 'Ê', 'change' => 'E');
        $char[70]   =   array('name' => 'Ë', 'change' => 'E');
        $char[71]   =   array('name' => 'í', 'change' => 'I');
        $char[72]   =   array('name' => 'ì', 'change' => 'I');
        $char[73]   =   array('name' => 'ï', 'change' => 'I');
        $char[74]   =   array('name' => 'î', 'change' => 'I');
        $char[75]   =   array('name' => 'Í', 'change' => 'I');
        $char[76]   =   array('name' => 'Ì', 'change' => 'I');
        $char[77]   =   array('name' => 'Ï', 'change' => 'I');
        $char[78]   =   array('name' => 'Î', 'change' => 'I');
        $char[79]   =   array('name' => 'ó', 'change' => 'O');
        $char[80]   =   array('name' => 'ò', 'change' => 'O');
        $char[81]   =   array('name' => 'ö', 'change' => 'O');
        $char[82]   =   array('name' => 'ô', 'change' => 'O');
        $char[83]   =   array('name' => 'Ó', 'change' => 'O');
        $char[84]   =   array('name' => 'Ò', 'change' => 'O');
        $char[85]   =   array('name' => 'Ö', 'change' => 'O');
        $char[86]   =   array('name' => 'Ô', 'change' => 'O');
        $char[87]   =   array('name' => 'ú', 'change' => 'U');
        $char[88]   =   array('name' => 'ù', 'change' => 'U');
        $char[89]   =   array('name' => 'ü', 'change' => 'U');
        $char[90]   =   array('name' => 'û', 'change' => 'U');
        $char[91]   =   array('name' => 'Ú', 'change' => 'U');
        $char[92]   =   array('name' => 'Ù', 'change' => 'U');
        $char[93]   =   array('name' => 'Û', 'change' => 'U');
        $char[94]   =   array('name' => 'Ü', 'change' => 'U');
        $char[95]   =   array('name' => 'ñ', 'change' => 'N');
        $char[96]   =   array('name' => 'Ñ', 'change' => 'N');
        $char[97]   =   array('name' => 'ç', 'change' => '');
        $char[98]   =   array('name' => 'Ç', 'change' => '');
        $char[99]   =   array('name' => '!', 'change' => '');
        $char[100]  =   array('name' => '@', 'change' => '');
        $char[101]  =   array('name' => '#', 'change' => '');
        $char[102]  =   array('name' => '$', 'change' => '');
        $char[103]  =   array('name' => '%', 'change' => '');
        $char[104]  =   array('name' => '^', 'change' => '');
        $char[105]  =   array('name' => '&', 'change' => '');
        $char[106]  =   array('name' => '*', 'change' => '');
        $char[107]  =   array('name' => '(', 'change' => '');
        $char[108]  =   array('name' => ')', 'change' => '');
        $char[109]  =   array('name' => '+', 'change' => '');
        $char[110]  =   array('name' => '=', 'change' => '');
        $char[111]  =   array('name' => '/', 'change' => '');
        $char[112]  =   array('name' => '*', 'change' => '');
        $char[113]  =   array('name' => '+', 'change' => '');
        $char[114]  =   array('name' => '.', 'change' => '');
        $char[115]  =   array('name' => ';', 'change' => '');
        $char[116]  =   array('name' => ':', 'change' => '');
        $char[117]  =   array('name' => '-', 'change' => ' ');
        $char[118]  =   array('name' => '_', 'change' => ' ');
        $char[119]  =   array('name' => ' ', 'change' => ' ');
        $char[120]  =   array('name' => '?', 'change' => ' ');

        return $char;

    }

    public static function TownChar()
    {
        $char[1]    =   array('name' => '0', 'change' => '');
        $char[2]    =   array('name' => '1', 'change' => '');
        $char[3]    =   array('name' => '2', 'change' => '');
        $char[4]    =   array('name' => '3', 'change' => '');
        $char[5]    =   array('name' => '4', 'change' => '');
        $char[6]    =   array('name' => '5', 'change' => '');
        $char[7]    =   array('name' => '6', 'change' => '');
        $char[8]    =   array('name' => '7', 'change' => '');
        $char[9]    =   array('name' => '8', 'change' => '');
        $char[10]   =   array('name' => '9', 'change' => '');
        $char[11]   =   array('name' => 'á', 'change' => 'A');
        $char[12]   =   array('name' => 'à', 'change' => 'A');
        $char[13]   =   array('name' => 'ä', 'change' => 'A');
        $char[14]   =   array('name' => 'â', 'change' => 'A');
        $char[15]   =   array('name' => 'ª', 'change' => 'A');
        $char[16]   =   array('name' => 'Á', 'change' => 'A');
        $char[17]   =   array('name' => 'À', 'change' => 'A');
        $char[18]   =   array('name' => 'Â', 'change' => 'A');
        $char[19]   =   array('name' => 'Ä', 'change' => 'A');
        $char[20]   =   array('name' => 'Ã', 'change' => 'A');
        $char[21]   =   array('name' => 'é', 'change' => 'E');
        $char[22]   =   array('name' => 'è', 'change' => 'E');
        $char[23]   =   array('name' => 'ë', 'change' => 'E');
        $char[24]   =   array('name' => 'ê', 'change' => 'E');
        $char[25]   =   array('name' => 'É', 'change' => 'E');
        $char[26]   =   array('name' => 'È', 'change' => 'E');
        $char[27]   =   array('name' => 'Ê', 'change' => 'E');
        $char[28]   =   array('name' => 'Ë', 'change' => 'E');
        $char[29]   =   array('name' => 'í', 'change' => 'I');
        $char[30]   =   array('name' => 'ì', 'change' => 'I');
        $char[31]   =   array('name' => 'ï', 'change' => 'I');
        $char[32]   =   array('name' => 'î', 'change' => 'I');
        $char[33]   =   array('name' => 'Í', 'change' => 'I');
        $char[34]   =   array('name' => 'Ì', 'change' => 'I');
        $char[36]   =   array('name' => 'Ï', 'change' => 'I');
        $char[36]   =   array('name' => 'Î', 'change' => 'I');
        $char[37]   =   array('name' => 'ó', 'change' => 'O');
        $char[38]   =   array('name' => 'ò', 'change' => 'O');
        $char[39]   =   array('name' => 'ö', 'change' => 'O');
        $char[40]   =   array('name' => 'ô', 'change' => 'O');
        $char[41]   =   array('name' => 'Ó', 'change' => 'O');
        $char[42]   =   array('name' => 'Ò', 'change' => 'O');
        $char[43]   =   array('name' => 'Ö', 'change' => 'O');
        $char[44]   =   array('name' => 'Ô', 'change' => 'O');
        $char[45]   =   array('name' => 'ú', 'change' => 'U');
        $char[46]   =   array('name' => 'ù', 'change' => 'U');
        $char[47]   =   array('name' => 'ü', 'change' => 'U');
        $char[48]   =   array('name' => 'û', 'change' => 'U');
        $char[49]   =   array('name' => 'Ú', 'change' => 'U');
        $char[50]   =   array('name' => 'Ù', 'change' => 'U');
        $char[51]   =   array('name' => 'Û', 'change' => 'U');
        $char[52]   =   array('name' => 'Ü', 'change' => 'U');
        $char[53]   =   array('name' => 'ñ', 'change' => 'N');
        $char[54]   =   array('name' => 'Ñ', 'change' => 'N');
        $char[55]   =   array('name' => 'ç', 'change' => '');
        $char[56]   =   array('name' => 'Ç', 'change' => '');
        $char[57]   =   array('name' => '!', 'change' => '');
        $char[58]   =   array('name' => '@', 'change' => '');
        $char[59]   =   array('name' => '#', 'change' => '');
        $char[60]   =   array('name' => '$', 'change' => '');
        $char[61]   =   array('name' => '%', 'change' => '');
        $char[62]   =   array('name' => '^', 'change' => '');
        $char[63]   =   array('name' => '&', 'change' => '');
        $char[64]   =   array('name' => '*', 'change' => '');
        $char[65]   =   array('name' => '(', 'change' => '');
        $char[66]   =   array('name' => ')', 'change' => '');
        $char[67]   =   array('name' => '+', 'change' => '');
        $char[68]   =   array('name' => '=', 'change' => '');
        $char[69]   =   array('name' => '/', 'change' => '');
        $char[70]   =   array('name' => '*', 'change' => '');
        $char[71]   =   array('name' => '+', 'change' => '');
        $char[72]   =   array('name' => '.', 'change' => '');
        $char[73]   =   array('name' => ';', 'change' => '');
        $char[74]   =   array('name' => ':', 'change' => '');
        $char[75]   =   array('name' => '-', 'change' => ' ');
        $char[76]   =   array('name' => '_', 'change' => ' ');

        return $char;
    }


    public static function DelChar()
    {
        $char[1]    =   array('name' => 'a', 'change' => '');
        $char[2]    =   array('name' => 'b', 'change' => '');
        $char[3]    =   array('name' => 'c', 'change' => '');
        $char[4]    =   array('name' => 'd', 'change' => '');
        $char[5]    =   array('name' => 'e', 'change' => '');
        $char[6]    =   array('name' => 'f', 'change' => '');
        $char[7]    =   array('name' => 'g', 'change' => '');
        $char[8]    =   array('name' => 'h', 'change' => '');
        $char[9]    =   array('name' => 'i', 'change' => '');
        $char[10]   =   array('name' => 'j', 'change' => '');
        $char[11]   =   array('name' => 'k', 'change' => '');
        $char[12]   =   array('name' => 'l', 'change' => '');
        $char[13]   =   array('name' => 'm', 'change' => '');
        $char[14]   =   array('name' => 'n', 'change' => '');
        $char[15]   =   array('name' => 'o', 'change' => '');
        $char[16]   =   array('name' => 'p', 'change' => '');
        $char[17]   =   array('name' => 'q', 'change' => '');
        $char[18]   =   array('name' => 'r', 'change' => '');
        $char[19]   =   array('name' => 's', 'change' => '');
        $char[20]   =   array('name' => 't', 'change' => '');
        $char[21]   =   array('name' => 'u', 'change' => '');
        $char[22]   =   array('name' => 'v', 'change' => '');
        $char[23]   =   array('name' => 'w', 'change' => '');
        $char[24]   =   array('name' => 'x', 'change' => '');
        $char[25]   =   array('name' => 'y', 'change' => '');
        $char[26]   =   array('name' => 'z', 'change' => '');
        $char[27]   =   array('name' => 'A', 'change' => '');
        $char[28]   =   array('name' => 'B', 'change' => '');
        $char[29]   =   array('name' => 'C', 'change' => '');
        $char[30]   =   array('name' => 'D', 'change' => '');
        $char[31]   =   array('name' => 'E', 'change' => '');
        $char[32]   =   array('name' => 'F', 'change' => '');
        $char[33]   =   array('name' => 'G', 'change' => '');
        $char[34]   =   array('name' => 'H', 'change' => '');
        $char[35]   =   array('name' => 'I', 'change' => '');
        $char[36]   =   array('name' => 'J', 'change' => '');
        $char[37]   =   array('name' => 'K', 'change' => '');
        $char[38]   =   array('name' => 'L', 'change' => '');
        $char[39]   =   array('name' => 'M', 'change' => '');
        $char[40]   =   array('name' => 'N', 'change' => '');
        $char[41]   =   array('name' => 'O', 'change' => '');
        $char[42]   =   array('name' => 'P', 'change' => '');
        $char[43]   =   array('name' => 'Q', 'change' => '');
        $char[44]   =   array('name' => 'R', 'change' => '');
        $char[45]   =   array('name' => 'S', 'change' => '');
        $char[46]   =   array('name' => 'T', 'change' => '');
        $char[47]   =   array('name' => 'U', 'change' => '');
        $char[48]   =   array('name' => 'V', 'change' => '');
        $char[49]   =   array('name' => 'W', 'change' => '');
        $char[50]   =   array('name' => 'X', 'change' => '');
        $char[51]   =   array('name' => 'Y', 'change' => '');
        $char[52]   =   array('name' => 'Z', 'change' => '');
        $char[53]   =   array('name' => '1', 'change' => '');
        $char[54]   =   array('name' => '2', 'change' => '');
        $char[55]   =   array('name' => '3', 'change' => '');
        $char[56]   =   array('name' => '4', 'change' => '');
        $char[57]   =   array('name' => '5', 'change' => '');
        $char[58]   =   array('name' => '6', 'change' => '');
        $char[59]   =   array('name' => '7', 'change' => '');
        $char[60]   =   array('name' => '8', 'change' => '');
        $char[61]   =   array('name' => '9', 'change' => '');
        $char[62]   =   array('name' => '0', 'change' => '');
        return $char;
    }

    public static function IdTown()
    {
        $town[1]    =   array('town' => 'ADJUNTAS',      'id' => '1');
        $town[2]    =   array('town' => 'AGUADA',        'id' => '2');
        $town[3]    =   array('town' => 'AGUADILLA',     'id' => '3');
        $town[4]    =   array('town' => 'AGUAS BUENAS',  'id' => '4');
        $town[5]    =   array('town' => 'AIBONITO',      'id' => '5');
        $town[6]    =   array('town' => 'ARECIBO',       'id' => '6');
        $town[7]    =   array('town' => 'ARROYO',        'id' => '7');
        $town[8]    =   array('town' => 'ANASCO',        'id' => '8');
        $town[9]    =   array('town' => 'BARCELONETA',   'id' => '9');
        $town[10]   =   array('town' => 'BARRANQUITAS',  'id' => '10');
        $town[11]   =   array('town' => 'BAYAMON',       'id' => '11');
        $town[12]   =   array('town' => 'CABO ROJO',     'id' => '12');
        $town[13]   =   array('town' => 'CAGUAS',        'id' => '13');
        $town[14]   =   array('town' => 'CAMUY',         'id' => '14');
        $town[15]   =   array('town' => 'CANOVANAS',     'id' => '15');
        $town[16]   =   array('town' => 'CAROLINA',      'id' => '16');
        $town[17]   =   array('town' => 'CATANO',        'id' => '17');
        $town[18]   =   array('town' => 'CAYEY',         'id' => '18');
        $town[19]   =   array('town' => 'CEIBA',         'id' => '19');
        $town[20]   =   array('town' => 'CIALES',        'id' => '20');
        $town[21]   =   array('town' => 'CIDRA',         'id' => '21');
        $town[22]   =   array('town' => 'COAMO',         'id' => '22');
        $town[23]   =   array('town' => 'COMERIO',       'id' => '23');
        $town[24]   =   array('town' => 'COROZAL',       'id' => '24');
        $town[25]   =   array('town' => 'CULEBRA',       'id' => '25');
        $town[26]   =   array('town' => 'DORADO',        'id' => '26');
        $town[27]   =   array('town' => 'FAJARDO',       'id' => '27');
        $town[28]   =   array('town' => 'FLORIDA',       'id' => '28');
        $town[29]   =   array('town' => 'GUAYAMA',       'id' => '29');
        $town[30]   =   array('town' => 'GUAYANILLAS',   'id' => '30');
        $town[31]   =   array('town' => 'GUAYNABO',      'id' => '31');
        $town[32]   =   array('town' => 'GURABO',        'id' => '32');
        $town[33]   =   array('town' => 'GUANICA',       'id' => '33');
        $town[34]   =   array('town' => 'HATILLO',       'id' => '34');
        $town[35]   =   array('town' => 'HORMIGUEROS',   'id' => '35');
        $town[36]   =   array('town' => 'HUMACAO',       'id' => '36');
        $town[37]   =   array('town' => 'ISABELA',       'id' => '37');
        $town[38]   =   array('town' => 'JAYUYA',        'id' => '38');
        $town[39]   =   array('town' => 'JUANA DIAZ',    'id' => '39');
        $town[40]   =   array('town' => 'JUNCOS',        'id' => '40');
        $town[41]   =   array('town' => 'LAJAS',         'id' => '41');
        $town[42]   =   array('town' => 'LARES',         'id' => '42');
        $town[43]   =   array('town' => 'LAS MARIAS',    'id' => '43');
        $town[44]   =   array('town' => 'LAS PIEDRAS',   'id' => '44');
        $town[45]   =   array('town' => 'LOIZA',         'id' => '45');
        $town[46]   =   array('town' => 'LUQUILLO',      'id' => '46');
        $town[47]   =   array('town' => 'MANATI',        'id' => '47');
        $town[48]   =   array('town' => 'MARICAO',       'id' => '48');
        $town[49]   =   array('town' => 'MAUNABO',       'id' => '49');
        $town[50]   =   array('town' => 'MAYAGUEZ',      'id' => '50');
        $town[51]   =   array('town' => 'MOCA',          'id' => '51');
        $town[52]   =   array('town' => 'MOROVIS',       'id' => '52');
        $town[53]   =   array('town' => 'NAGUABO',       'id' => '53');
        $town[54]   =   array('town' => 'NARANJITO',     'id' => '54');
        $town[55]   =   array('town' => 'OROCOVIS',      'id' => '55');
        $town[56]   =   array('town' => 'PATILLAS',      'id' => '56');
        $town[57]   =   array('town' => 'PENUELAS',      'id' => '57');
        $town[58]   =   array('town' => 'PONCE',         'id' => '58');
        $town[59]   =   array('town' => 'QUEBRADILLAS',  'id' => '59');
        $town[60]   =   array('town' => 'RINCON',        'id' => '60');
        $town[61]   =   array('town' => 'RIO GRANDE',    'id' => '61');
        $town[62]   =   array('town' => 'SABANA GRANDE', 'id' => '62');
        $town[63]   =   array('town' => 'SALINAS',       'id' => '63');
        $town[64]   =   array('town' => 'SAN GERMAN',    'id' => '64');
        $town[65]   =   array('town' => 'SAN JUAN',      'id' => '65');
        $town[66]   =   array('town' => 'SAN LORENZO',   'id' => '66');
        $town[67]   =   array('town' => 'SAN SEBASTIAN', 'id' => '67');
        $town[68]   =   array('town' => 'SANTA ISABEL',  'id' => '68');
        $town[69]   =   array('town' => 'TOA ALTA',      'id' => '69');
        $town[70]   =   array('town' => 'TOA BAJA',      'id' => '70');
        $town[71]   =   array('town' => 'TRUJILLO ALTO', 'id' => '71');
        $town[72]   =   array('town' => 'UTUADO',        'id' => '72');
        $town[73]   =   array('town' => 'VEGA ALTA',     'id' => '73');
        $town[74]   =   array('town' => 'VEGA BAJA',     'id' => '74');
        $town[75]   =   array('town' => 'VIEQUES',       'id' => '75');
        $town[76]   =   array('town' => 'VILLALBA',      'id' => '76');
        $town[77]   =   array('town' => 'YABUCOA',       'id' => '77');
        $town[78]   =   array('town' => 'YAUCO',         'id' => '78');
        $town[79]   =   array('town' => 'NAUNABO',       'id' => '49');
        $town[80]   =   array('town' => 'E',             'id' => '1');
        $town[81]   =   array('town' => ' ',             'id' => '1');
        return $town;
    }

    public static function OptProvider()
    {
        $char[1]    =   array('name' => 'AT_T',                         'id' => '2');
        $char[2]    =   array('name' => 'CLARO',                        'id' => '3');
        $char[3]    =   array('name' => 'T_MOBILE',                     'id' => '4');
        $char[4]    =   array('name' => 'BOOM_SOLUTIONS',               'id' => '5');
        $char[5]    =   array('name' => '4NET',                         'id' => '6');
        $char[6]    =   array('name' => 'SPRINT_PCS',                   'id' => '7');
        $char[7]    =   array('name' => 'BOOST MOBILE',                 'id' => '8');
        $char[8]    =   array('name' => 'BOOST_MOBILE',                 'id' => '8');
        $char[9]    =   array('name' => 'LIBERTY_CABLEVISION',          'id' => '9');
        $char[10]   =   array('name' => 'METRO_PCS',                    'id' => '10');
        $char[11]   =   array('name' => 'NEWCOMM_WIRELESS',             'id' => '11');
        $char[12]   =   array('name' => 'WORLDNET_COMMUNICATION',       'id' => '12');
        $char[13]   =   array('name' => 'PUERTO_RICO_TELEPHONE',        'id' => '13');
        $char[14]   =   array('name' => 'CENTENNIAL_PR_RESIDENTIAL',    'id' => '14');
        $char[15]   =   array('name' => 'ONELINK_COMMUNICATIONS',       'id' => '15');
        $char[16]   =   array('name' => 'CHOICE_CABLE_TV',              'id' => '16');
        $char[17]   =   array('name' => 'TRACFONE_WIRELESS',            'id' => '17');
        $char[18]   =   array('name' => 'OPEN_MOBILE',                  'id' => '18');
        $char[19]   =   array('name' => 'VIRGIN_MOBILE',                'id' => '19');
        $char[20]   =   array('name' => 'AT&T',                         'id' => '2');
        $char[21]   =   array('name' => 'T-MOBILE',                     'id' => '4');

        return $char;
    }

    public static function OptType()
    {
        $char[1]   =   array('name' => 'PROPIA CON ESCRITURAS','id' => '1');
        $char[2]   =   array('name' => 'PROPIA.S.E',           'id' => '2');
        $char[3]   =   array('name' => 'PROPIA',               'id' => '1');
        $char[4]   =   array('name' => 'RENTADA',              'id' => '3');
        $char[5]   =   array('name' => 'ASIGNADA',             'id' => '4');
        $char[6]   =   array('name' => 'NOT_OWNER',            'id' => '5');

        return $char;
    }

    public static function OptRoof()
    {
        $char[1]   =   array('name' => 'CEMENTO',   'id' => '1');
        $char[3]   =   array('name' => 'UNDEFINED', 'id' => '1');
        $char[2]   =   array('name' => 'MADERA',    'id' => '2');
        $char[4]   =   array('name' => 'ZINC',      'id' => '3');

        return $char;
    }

}
