<?php
namespace App\Lib;

// require '../vendor/autoload.php';

use FPDF;

// ddd('FPDF_FONTPATH', realpath('fonts/'));
define('FPDF_FONTPATH', realpath( 'fonts/' ));

class HojaServicioPDFNew extends FPDF
{
    
    public $debug = 0;
    public $height = 7;

    function __construct($orientation='P', $unit='mm', $size='letter')
    {
        
        parent::__construct($orientation, $unit, $size);
        $this->SetLineWidth(0.4);
        $this->AddFont('Calibri','', 'calibri.php');
        $this->AddFont('Calibri','B','calibrib.php');
        $this->AddFont('Calibri','I','calibrii.php');
    }

    public function Header()
    {
        $this->Image('assets/img/logicon.png',10,5,0,10);
        
        $this->SetFont('Calibri','I',8);
        $this->Cell(0,2.5,'Boom Net / Boom Net LLC.',$this->debug,1,'R');
        
        $this->SetFontsize(8);
        $this->Cell(0,2.5,'P.O BOX 5849',$this->debug,1,'R');
        $this->Cell(0,2.5,'CAGUAS P.R. 00726',$this->debug,1,'R');
        $this->Cell(0,2.5,'787-333-0230 / 787-333-0231',$this->debug,1,'R');
        
        $this->SetFont('','B',10);
        $this->SetTextColor(227,108,10);
        $this->Cell(0,2.5,'HOJA DE SERVICIO',$this->debug,1,'C');
        
        $this->Ln(2);
    }

    public function hojaServicio(array $data)
    {
        $this->addPage('P');
        $this->setFillColor(0,34,96);
        // $this->setFillColor(0,176,240);
        // $this->setFillColor('#002260');
        $this->informacion_cliente($data['cliente']);
        
        $this->Ln(2);
        $this->informacion_servicio($data['servicio']);
        
        $this->Ln(2);
        $this->politicas_garantias($data['servicio']);
        
        $this->Ln(2);
        $this->informacion_visita($data['visita']);


        $this->Ln(2);
        $this->SetFont('','B',11);

        $uno  = 35;
        $dos  = 105-$uno;
        $tres = 20;
        $quad = 91-$tres;
        $this->Cell($uno, $this->height,'FIRMA DEL CLIENTE: ',$this->debug,0,'C');
        $this->Cell($dos, $this->height,'__________________/Fecha___________',$this->debug,0,'C');
        $this->Cell($tres, $this->height,'TECNICO: ',$this->debug,0,'C');
        $this->Cell($quad, $this->height,'_________________________________',$this->debug,0,'C');

        return $this;
    }
    
    public function informacion_cliente(array $data)
    {
        $this->encabezado_tabla();
        $this->Cell(196,5,'INFORMACION DEL CLIENTE',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        // d("Informacion CLiente", $rectCoordIni);


        $this->cuerpo_tabla();
        $uno  = 35;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        $this->Cell($uno, $this->height,'FECHA DE VISITA: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['fecha_visita']),$this->debug,0,'L');
        $this->Cell($tres,$this->height,'TECNICO: ',$this->debug,0,'R');
        $this->Cell($quad,$this->height,utf8_decode($data['tecnico_nombre']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'CLIENTE: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['nombre']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'CLIENTE ID: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['id']),$this->debug,0,'L');
        $this->Cell($tres,$this->height,'TELEFONO: ',$this->debug,0,'R');
        $this->Cell($quad,$this->height,utf8_decode($data['telefono']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno,$this->height,'PUEBLO: ',$this->debug,0,'R');
        $this->Cell($dos,$this->height,utf8_decode($data['pueblo']),$this->debug,0,'L');
        $this->Cell($tres, $this->height,'COORDENADAS: ',$this->debug,0,'R');
        $this->Cell($quad, $this->height,utf8_decode($data['coordenadas']),$this->debug,0,'L');
        $this->Ln($this->height);
        
        $this->Cell($uno,$this->height,'DIRECCION: ',$this->debug,0,'R');
        $this->MultiCell(196-$uno,$this->height,utf8_decode($data['direccion']),$this->debug);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function informacion_servicio(array $servicio)
    {
        $this->encabezado_tabla();
        $this->Cell(196,5,'INFORMACION DEL SERVICIO',1,1,'C',1);

        $this->cuerpo_tabla();
        $uno   = 34;
        $dos   = 70-$uno;
        $tres  = 30;
        $quad  = 65-$tres;
        $cinco = 22;
        $seis  = 60-$cinco;
        
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        // d("Informacion Servicio", $rectCoordIni);

        $this->Cell($uno,  $this->height,'PLAN CONTRATADO: ',$this->debug,0,'R');
        $this->Cell(196-$uno,     $this->height,utf8_decode($servicio['plan-contratado']),$this->debug,0,'L');
        $this->Ln();
        
        $this->Cell($uno, $this->height,'IP ACTUAL: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,$servicio['ip-actual'],$this->debug,0,'L');
        $this->Cell($tres,$this->height,'AP ACTUAL: ',$this->debug,0,'R');
        $this->Cell($quad, $this->height,$servicio['ap-actual'],$this->debug,0,'L');
        $this->Ln();

        $this->Cell($uno,    $this->height,'SENAL: ',$this->debug,0,'R');
        $this->Cell($dos,    $this->height,utf8_decode($servicio['senal']),$this->debug,0,'L');
        $this->Cell($tres,   $this->height,'TRANSMISION: ',$this->debug,0,'R');
        $this->Cell($quad,   $this->height,utf8_decode($servicio['transmision']),$this->debug,0,'L');
        $this->Cell($cinco,  $this->height,'SPEEDTEST: ',$this->debug,0,'R');
        $this->Cell($seis/2, $this->height,'Rx'.utf8_decode($servicio['speedtest-recibido']),$this->debug,0,'C');
        $this->Cell($seis/2, $this->height,'Tx'.utf8_decode($servicio['speedtest-transmitido']),$this->debug,0,'C');
        $this->Ln();

        $this->Cell($uno,  $this->height,'MOTIVO: ',$this->debug,0,'R');
        $this->MultiCell(196-$uno,$this->height, utf8_decode($servicio['motivo']),$this->debug);
        
        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    }

    public function politicas_garantias(array $servicio)
    {
        $this->encabezado_tabla();
        $this->Cell(196,5,'POLITICAS DE GARANTIAS DE SERVICIOS',1,1,'C',1);

        $this->cuerpo_tabla();
        $uno  = 35;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        // d("Informacion Servicio", $rectCoordIni);

        $text   =   'Boom Solutions contempla en el acuerdo comercial con el titular ya mencionado, una garantía permanente de los equipos instalados con la finalidad de que el mismo pueda disfrutar plenamente del servicio con los estándares de calidad establecidos. Ante lo expuesto si existiera algún problema técnico asociado a su servicio de Internet y el mismo no es solventado de manera remota, el cliente tiene derecho a una visita técnica por el personal especialista de Boom Solutions siempre y cuando el problema o falla presentado no sea imputable al cliente, de lo contrario deberá pagar un monto de $25, en efectivo o será cargado en su próxima factura.';

        $this->Ln(3);
        $this->MultiCell(196,3,utf8_decode($text), $this->debug);

        $this->Cell(82,  $this->height,utf8_decode('Escenarios Imputables a la Garantía de Boom Solutions'),$this->debug,0,'R');
        $this->Cell(80,  $this->height,utf8_decode('Escenarios imputables al cliente'),$this->debug,0,'R');
        $this->Ln(4);
        
        $this->Cell(40,  $this->height,utf8_decode('- Desperfecto de equipos'),$this->debug,0,'R');
        $this->Cell(120,  $this->height,utf8_decode('- Manipulación de equipos'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(36.5,  $this->height,utf8_decode('- Cableado Defectuoso'),$this->debug,0,'R');
        $this->Cell(133,  $this->height,utf8_decode('- Conexión incorrecta de equipos'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(34.5,  $this->height,utf8_decode('- Antena desalineada'),$this->debug,0,'R');
        $this->Cell(154.5,  $this->height,utf8_decode('- Daños a cablería por animales o construcción'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(34.5,  $this->height,utf8_decode(''),$this->debug,0,'R');
        $this->Cell(113.5,  $this->height,utf8_decode('- Daños a equipos'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(34.5,  $this->height,utf8_decode(''),$this->debug,0,'R');
        $this->Cell(155,  $this->height,utf8_decode('- Otros:________________________________'),$this->debug,0,'R');
        $this->Ln(6);

        $text1   =   'El Cliente debe firmar este apartado como autorización de realizar el servicio técnico donde queda notificado la política de garantía de equipos.';

        $this->MultiCell(196,3,utf8_decode($text1), $this->debug);

        $this->Cell(120, $this->height,'_______________________',$this->debug,0,'R');
        $this->Ln($this->height);

        $this->Cell(115,$this->height,utf8_decode('Firma de autorizacion'),$this->debug,0,'R');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    
    }

    public function informacion_visita(array $data)
    {
        $this->encabezado_tabla();
        $this->Cell(196,5,'INFORMACION DE LA VISITA',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x,'y' => $this->y, );
        // ddd("Informacion Visita", $rectCoordIni);

        $this->cuerpo_tabla();
        $this->Ln($this->height/3);
        $this->Cell(3);
        $this->Cell(50,  $this->height,'PROBLEMAS ENCONTRADOS: ',1,0,'C');
        
        if (array_key_exists('problemas', $data)) {
            $problemas = implode(', ', $data['problemas']);
        } else {
            $problemas = '';
        }
        
        $this->Cell(140,  $this->height, ucwords($problemas), 1,0,'L');
        // $this->Cell(42,  $this->height,' CONECTOR/CABLE',1,0,'C');
        // $this->Cell(22,  $this->height,' ROUTER',1,0,'C');
        // $this->Cell(54,  $this->height,' EQUIPOS MANIPULADOS',1,0,'C');

        $this->Ln();
        $this->Ln(3);
        $this->Cell(3);
        $this->Cell(27,  $this->height,'SOLUCION: ',1,0,'R');
        // $this->Cell(163,  $this->height,$data['solucion'],1,0,'L');
        $this->MultiCell(163,  $this->height,utf8_decode($data['solucion']) ,1);
        
        $uno    = 27;
        $dos    = 30;
        $tres   = 28;
        $quad   = 45;
        $cinco  = 25;
        $seis   = 35;
        $this->Ln(3);
        $this->Cell(3);
        $this->Cell($uno,    $this->height,'CAMBIO DE IP: ',1,0,'R');
        $this->Cell($dos,    $this->height,$data['cambio-IP'],1,0,'C');
        $this->Cell($tres,   $this->height,'CAMBIO DE AP: ',1,0,'R');
        $this->Cell($quad,   $this->height,$data['cambio-AP'],1,0,'C');
        $this->Cell($cinco,  $this->height,'SENAL: ',1,0,'R');
        $this->Cell($seis,   $this->height,'dBm',1,0,'C');

        $this->Ln();
        $this->Cell(3);
        $this->Cell($uno,    $this->height,'SPEEDTEST: ',1,0,'R');
        $this->Cell($dos,    $this->height,'Rx '.$data['speedtest-recibido'],1,0,'C');
        $this->Cell($tres,   $this->height,'Tx '.$data['speedtest-recibido'],1,0,'C');
        $this->Cell($quad,   $this->height,'',1,0,'R');
        $this->Cell($cinco,  $this->height,'TRANSMIT: ',1,0,'R');
        $this->Cell($seis,   $this->height,$data['transmit'].'%',1,0,'C');

        $uno = 40;
        $dos = (190-$uno)/4;
        $this->Ln();
        $this->Ln(3);
        $this->Cell(3);
        $this->Cell($uno, $this->height,'EQUIPOS/INVENTARIO',1,0,'C');
        $this->Cell($dos, $this->height,'ANTENA',1,0,'C');
        $this->Cell($dos, $this->height,'ROUTER',1,0,'C');
        $this->Cell($dos, $this->height,'ATA',1,0,'C');
        $this->Cell($dos, $this->height,'CABLE IN/OUT: ',1,0,'C');
        
        $this->Ln();
        $this->Cell(3);
        $this->Cell($uno, $this->height,'INSTALADOS',1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['antena']['instalado']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['router']['instalado']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['ata']['instalado']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['cable-inout']['instalado']),1,0,'C');

        $this->Ln();
        $this->Cell(3);
        $this->Cell($uno, $this->height,'RECOGIDOS',1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['antena']['recogido']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['router']['recogido']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['ata']['recogido']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['cable-inout']['recogido']),1,0,'C');

        $this->Ln();
        $this->Ln(3);
        $this->Cell(3);
        $this->MultiCell(190,$this->height,'OBSERVACIONES: '.utf8_decode($data['observaciones']),1);
        
        $this->Ln(3);
        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196, $this->y - $rectCoordIni['y']);
        }

    }

    public function encabezado_tabla($value='')
    {
        // fuente 14 y negrita
        $this->SetFont('','B',14);
        // texto color blanco
        $this->SetTextColor(255);
    }

    public function cuerpo_tabla($value='')
    {
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    }
}