<?php

namespace App\Lib;

use App\Lib\Config;

require '../vendor/autoload.php';

class ApiMWVz
{
    
    public static function Curl($fields, $url)
    {
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"
        ));


        $response = curl_exec($ch);

        if (curl_error($ch)) {
            
            $response = "Error de comunicacion Intente nuevamente mas tarde.";
            return json_decode($response, false);
        }
     
        curl_close($ch);   

        return json_decode($response, true);

        // return true;
    
    }

    public static function CurlTest($fields, $url)
    {
        
        d($url, $fields);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"
        ));

        d(json_encode($fields), curl_exec($ch));

        $response = curl_exec($ch);
        ddd($response);

        if (curl_error($ch)) {
            
            $response = "Error de comunicacion Intente nuevamente mas tarde.";
            return json_decode($response, false);
        }
     
        curl_close($ch);   

        return json_decode($response, true);

        // return true;
    
    }

}