<?php

namespace App\Lib;

use \PDO;

use App\Lib\Config;

/**
 * 
 */
class DBPbx
{
  public function Conexion() 
  {

    $datos  = New Config();
    
    $db     = $datos->DataBasePBX();

    try {

      return New PDO('mysql:host='.$db['DB_HOST'].';dbname='.$db['DB_NAME'].'', ''.$db['DB_USER'].'', ''.$db['DB_PASSWORD'].'');

    } catch (Exception $e) {
    
      return "Error!: " . $e->getMessage() . "<br/>";
    }
  }
}