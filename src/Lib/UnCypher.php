<?php

namespace App\Lib;

use App\Lib\Config;
use App\Lib\DataCypher;
use App\Models\CypherData;
use App\Models\Leads;

/**
 * 
 */
class UnCypher
{

//////////////////////////////////////////////////////////////////////////////////////////
  
  public static function GetInfoCypher($info, $type)
  {

    $infoC    =   New Config();

    $exists   =   @get_headers( $infoC->Curl()['host']);

    if(strpos($exists[0],'404') === false)
    {
      $curl = execute(array('command' => 'license'));

      if($curl == 'expired')
      { 
          return false; 
      }
      else
      {
        switch ($type) 
        {
          case 'CC':
            return uncypher($info, $type);
            break;

          case 'SS':
            return uncypher($info, $type);
            break;
          
          case 'ID':
            return uncypher($info, $type);
            break;
            
          case 'AB':
            return uncypher($info, $type);
            break;
        }
      }
    }
    else
    {
      return false; 
    }
  
  }

//////////////////////////////////////////////////////////////////////////////////////////

  public static function CypherDataInfo($infoD, $ope)
  {
      $infoC    =   New Config();

      $cifrado  =   New DataCypher($infoC->Ramdom(32));

      $exists   =   @get_headers( $infoC->Curl()['host']);
      
      if(strpos($exists[0],'404') === false)
      {
        $curl = execute(array('command' => 'license'));

        if($curl == 'expired')
        { 
            return ['status' => 'license', 'web' => '/ErrorLicense']; 
        }
        else
        {
          $keygen   =   execute(array('command' => 'keygen'));


          $cypher   =   $cifrado->Cypher($infoD, $keygen);

          if($cypher['status'] == false)
          {

            return [
              'status'    =>  false,
              'title'     =>  'Error',
              'content'   =>  $cypher['message']
            ];

          }else{

            $keygen = execute([
                'command'   =>  'savecypher',
                'cypher'    =>  $cypher['keygen'],
                'client'    =>  $ope['client'],
                'type'      =>  $ope['type']
            ]);

            if($keygen == false)
            {
              return [
                'status'    =>  false,
                'title'     =>  'Error',
                'content'   =>  'No se Pudo completa lar solicitud intente de nuevo, si el problema persiste comuniquese con su administrador de sistemas.'
              ];

            }else{

              switch ($ope['type']) 
              {
                case 'CC':

                  $infoR = [
                    'client'    =>  $ope['client'],
                    'cypher'    =>  $cypher['cypher'],
                    'key'       =>  $keygen,
                    'type'      =>  $ope['type_l'],
                    'last'      =>  mb_substr(trim(substr($infoD['card'], 0,strlen($infoD['card']))), -4),
                    'c_type'    =>  $ope['type'],
                    'country'   =>  $ope['country'],
                    'exp'       =>  $ope['exp'],
                    'type_d'    =>  $ope['type_d']
                  ];
                  break;
                
                case 'AB':

                  $infoR = [
                    'client'    =>  $ope['client'],
                    'cypher'    =>  $cypher['cypher'],
                    'key'       =>  $keygen,
                    'type'      =>  $ope['type_l'],
                    'last'      =>  mb_substr(trim(substr($infoD['account'], 0,strlen($infoD['account']))), -4),
                    'c_type'    =>  $ope['type'],
                    'country'   =>  $ope['country'],
                    'exp'       =>  $ope['exp'],
                    'type_d'    =>  $ope['type_d']
                  ];

                  break;

                case 'ID':

                  $infoR = [
                    'client'    =>  $ope['client'],
                    'cypher'    =>  $cypher['cypher'],
                    'key'       =>  $keygen,
                    'type'      =>  $ope['type_l'],
                    'last'      =>  mb_substr(trim(substr($infoD['id'], 0,strlen($infoD['id']))), -4),
                    'c_type'    =>  $ope['type'],
                    'country'   =>  $ope['country'],
                    'exp'       =>  $ope['exp'],
                    'type_d'    =>  $ope['type_d']
                  ];

                  break;

                case 'SS':

                  $infoR = [
                    'client'    =>  $ope['client'],
                    'cypher'    =>  $cypher['cypher'],
                    'key'       =>  $keygen,
                    'type'      =>  $ope['type_l'],
                    'last'      =>  mb_substr(trim(substr($infoD['ss'], 0,strlen($infoD['ss']))), -4),
                    'c_type'    =>  $ope['type'],
                    'country'   =>  $ope['country'],
                    'exp'       =>  $ope['exp'],
                    'type_d'    =>  $ope['type_d']
                  ];

                  break;
              }

              $SaveCypher   =   CypherData::SaveCypher($infoR, $ope['operator']);
              
              if($SaveCypher == true)
              {
                return [
                  'status'    =>  true,
                  'title'     =>  'Success',
                  'content'   =>  'Datos Encriptados Satisfactoriamente'
                ];

              }else{

                return [
                  'status'  => false, 
                  'title'   =>  'Error',
                  'content' =>  'No se pudo completar la solicitud intente de nuevo, si el problema persiste comuniquese con el administrador de sistemas.' 
                ];
              }
            }
          }
        }
      }
      else
      {
        return ['status' => 'license', 'web' => '/ErrorLicense']; 
      }
  
  }

//////////////////////////////////////////////////////////////////////////////////////////

  public static function GetInfoApi($command)
  {
    $infoC    =   New Config();

    $exists   =   @get_headers( $infoC->Curl()['host']);

    if(strpos($exists[0],'404') === false)
    {
      $curl = execute(array('command' => $command));

      return $curl;
    }
    else
    {
      return "No Encuentra API"; 
    }


  }

//////////////////////////////////////////////////////////////////////////////////////////

  // public static function UnCypherInfoData($info, $ope)
  // {
  //     $infoC    =   New Config();

  //     $exists   =   @get_headers( $infoC->Curl()['host']);

  //     if(strpos($exists[0],'404') === false)
  //     {
  //       $curl = execute(array('command' => 'license'));

  //       if($curl == 'expired')
  //       { 
  //           return false; 
  //       }
  //       else
  //       {

  //         $card = CypherData::GetCypherClientType($lead['client'], 'CC');

  //         return $card
  //         $keygen   =   execute(array('command' => 'keygen'));

  //         $data     =   ['id' =>  $info['client'], 'c_type' => $info['type'], 'exp' => $info['exp'] ];

  //         $cypher   =   $cifrado->Cypher($data, $keygen);

  //         if($cypher['status'] == false)
  //         {

  //           return [
  //             'status'    =>  false,
  //             'title'   =>  'Error',
  //             'content'   =>  $cypher['message']
  //           ];

  //         }else{

  //           $keygen = execute([
  //               'command'   =>  'savecypher',
  //               'cypher'    =>  $cypher['keygen'],
  //               'client'    =>  $data['id'],
  //               'type'      =>  $data['c_type']
  //           ]);

  //           if($keygen == false)
  //           {
  //             return [
  //               'status'    =>  false,
  //               'title'     =>  'Error',
  //               'content'   =>  'Could not process the request try again, if the problem persists contact the system administrator'
  //             ];

  //           }else{

  //             $infoR = [
  //               'client'    =>  $data['id'],
  //               'cypher'    =>  $cypher['cypher'],
  //               'key'       =>  $keygen,
  //               'type'      =>  $data['c_type'],
  //               'last'      =>  mb_substr(trim(substr($data['id'], 0,strlen($data['id']))), -4),
  //               'c_type'    =>  $data['c_type'],
  //               'country'   =>  $info['country'],
  //               'exp'       =>  $info['exp']
  //             ];

  //               $SaveCypher   =   CypherData::SaveCypher($infoR, $ope);
  //               $SaveLead     =   Leads::SaveID($infoR['client']);

  //               if($SaveCypher == true)
  //               {
  //                 return [
  //                   'status'    =>  true,
  //                   'title'     =>  'Success',
  //                   'content'   =>  'Identification data encrypted correctly'
  //                 ];

  //               }else{

  //                 return [
  //                   'status'  => false, 
  //                   'title'   =>  'Error',
  //                   'content' =>  'Could not process the request try again, if the problem persists contact the system administrator' 
  //                 ];
  //               }

  //           }
  //         }
  //       }
  //     }
  //     else
  //     {
  //       return false; 
  //     }
  
  // }

//////////////////////////////////////////////////////////////////////////////////////////

}

//////////////////////////////////////////////////////////////////////////////////////////

function uncypher($client, $type)
{

  $info   =   CypherData::GetCypherClientType($client, $type);


  if($info['key'] <> "")
  {
    $keygen = execute(array(
        'command'   =>  'infocard',
        'id'        =>  $info['key']
    ));

    if($keygen <> false)
    {
      $datos    = new Config();
      $app      = new DataCypher($datos->Ramdom(32));
      $infoCard = $app->Uncypher($info['cypher'], $keygen);

      return ['infoCard' => $infoCard, 'info'   =>  $info];

    }else{ return false; }
  
  }else{ return false; }

}

function cypher($client, $type)
{

  $info   =   CypherData::GetCypherClientType($client, $type);

  if($info['key'] <> "")
  {
    $keygen = execute(array(
        'command'   =>  'infocard',
        'id'        =>  $info['key']
    ));

    if($keygen <> false)
    {
      $datos    = new Config();
      $app      = new DataCypher($datos->Ramdom(32));
      $infoCard = $app->Uncypher($info['cypher'], $keygen);

      return $infoCard;

    }else{ return false; }
  
  }else{ return false; }

}

//////////////////////////////////////////////////////////////////////////////////////////

function execute($postfields)
{

  $info = new Config();

  $user = $info->Curl()['user'];
  $pass = $info->Curl()['pass'];
  $host = $info->Curl()['host'];

    $url    = $host;

    array_push($postfields, $user, $pass);

    $ch     = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);        
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    
    $output = curl_exec($ch);

    if($output === false)
    {
        return ("cURL error: " . curl_error($ch));
    }else{

        curl_close($ch);
        
        return $output;
    }

}

//////////////////////////////////////////////////////////////////////////////////////////