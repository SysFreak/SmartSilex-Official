<?php
namespace App\Lib;

class DataCypher extends Cypher
{

    public function __construct($key)
    {
        
        parent::__construct($key,'AES-256-CBC');
    }

    /**
     * Descifra los datos de una tarjeta
     *
     * Toma los primeros 16 caracteres de $input y lo usa como vector
     * de inicializacion para descifrar el resto de $input, luego
     * toma los datos en el texto descifrado, los convierte en
     * un arreglo y los retorna
     *
     * @param string $input texto a descifrar
     *
     * @return array un arreglo con numero, cvv y fecha expiracion de la tarjeta (mes y año)
     */
    
    public function Uncypher($input, $keygen)
    {
        return json_decode($this->decrypt($input, $keygen), true);
    }    

    /**
     * Cifra los datos de una tarjeta
     *
     * @param string $number    numero de la tarjeta
     * @param string $cvv       codigo de seguridad
     * @param string $expMonth  mes de expiracion
     * @param string $expYear   ano de expiracion
     *
     * @return string texto cifrado que contiene todos los datos de la tarjeta
     */
    
    public function CypherCard($info, $keygen)
    {

        $json = json_encode(array(
            'name'  => $info['name'],
            'card'  => $info['card'],
            'cvv'   => $info['cvv'],
            'month' => $info['month'],
            'year'  => $info['year']
        ));

        
        return $this->encrypt($json, $keygen);
   
    }

    public function cifrarTarjetaAuthorize($number,$cvv, $TRANSACTIONTYPE)
    {

        $json = json_encode(array(
            'number'            =>  $number,
            'cvv'               =>  $cvv,
            'TRANSACTIONTYPE'   =>  $TRANSACTIONTYPE
        ));
        
        return $this->encrypt($json);
    }

    public function CypherPass($input)
    {
        $json = json_encode($input);
        return $this->encrypt($json);
    }

    public function Cypher($params, $keygen)
    {
        return $this->encrypt(json_encode($params), $keygen);
    }

}

