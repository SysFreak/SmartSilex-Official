<?php

use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;

require __DIR__.'/prod.php';

$app['debug'] = true;

// $app->register(new MonologServiceProvider(), array(
//     'monolog.logfile' => __DIR__.'/../var/logs/silex.log',
// ));
