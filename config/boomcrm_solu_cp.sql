/*
 Navicat Premium Data Transfer

 Source Server         : SmartSilexPR
 Source Server Type    : MySQL
 Source Server Version : 50645
 Source Host           : 192.203.0.9:3306
 Source Schema         : boomcrm_solu

 Target Server Type    : MySQL
 Target Server Version : 50645
 File Encoding         : 65001

 Date: 16/11/2020 09:02:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cp_appro_resp_cancel
-- ----------------------------
DROP TABLE IF EXISTS `cp_appro_resp_cancel`;
CREATE TABLE `cp_appro_resp_cancel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `reason_id` int(11) NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1756 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_appro_score
-- ----------------------------
DROP TABLE IF EXISTS `cp_appro_score`;
CREATE TABLE `cp_appro_score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `appro_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `score` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transactions` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `value` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fico` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16632 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_appro_score_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_appro_score_tmp`;
CREATE TABLE `cp_appro_score_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `appro_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `score` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transactions` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `value` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fico` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26206 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_appro_serv
-- ----------------------------
DROP TABLE IF EXISTS `cp_appro_serv`;
CREATE TABLE `cp_appro_serv`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `score_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `score` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transactions` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `value` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fico` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_id` int(11) NULL DEFAULT NULL,
  `view` int(11) NULL DEFAULT NULL,
  `cancel` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_dep_id` int(11) NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `finish_by` int(11) NULL DEFAULT NULL,
  `finish_dep_id` int(11) NULL DEFAULT NULL,
  `finish_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  INDEX `finish_at`(`finish_at`) USING BTREE,
  INDEX `score`(`score`) USING BTREE,
  INDEX `service_id`(`service_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `cancel`(`cancel`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE,
  INDEX `finish_by`(`finish_by`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17961 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_contracts
-- ----------------------------
DROP TABLE IF EXISTS `cp_contracts`;
CREATE TABLE `cp_contracts`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_main` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_postal` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nivel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `techo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `house` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_lati` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_long` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `package` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telephone` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tel_type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tel_price` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `pre_date` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_date` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_res` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `price` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `prorateo` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mes` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instalation` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cc_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cc_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ab_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ab_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ab_bank` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contract` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sale` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '2',
  `repre_legal` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tp` int(20) NULL DEFAULT 1,
  `created_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '2',
  `created_at` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `contract`(`contract`) USING BTREE,
  INDEX `country`(`country`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5580 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_contracts2
-- ----------------------------
DROP TABLE IF EXISTS `cp_contracts2`;
CREATE TABLE `cp_contracts2`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_main` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_postal` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nivel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `techo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `house` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_lati` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_long` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `package` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telephone` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tel_type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tel_price` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `pre_date` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_date` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_res` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `price` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `prorateo` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mes` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instalation` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cc_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cc_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ab_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ab_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ab_bank` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_last` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_exp` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contract` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sale` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '2',
  `repre_legal` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tp` int(20) NULL DEFAULT 1,
  `created_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '2',
  `created_at` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `contract`(`contract`) USING BTREE,
  INDEX `country`(`country`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2181 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_cancel_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_cancel_tmp`;
CREATE TABLE `cp_coord_cancel_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `reason_id` int(11) NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `reason_id`(`reason_id`) USING BTREE,
  INDEX `service_id`(`service_id`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2703 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_comp_data
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_comp_data`;
CREATE TABLE `cp_coord_comp_data`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `signature_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `signature` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instalation_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `suscriptor` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contract` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(10) NULL DEFAULT 1,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `suscriptor`(`suscriptor`) USING BTREE,
  INDEX `contract`(`contract`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_coord_comp_data_int
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_comp_data_int`;
CREATE TABLE `cp_coord_comp_data_int`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `installer_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `installer` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contract` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT 1,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_comp_data_int_vzla
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_comp_data_int_vzla`;
CREATE TABLE `cp_coord_comp_data_int_vzla`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `installer_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `installer` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contract` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT 1,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_comp_data_sec
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_comp_data_sec`;
CREATE TABLE `cp_coord_comp_data_sec`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `installer_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `installer` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `company` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `n_order` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT 1,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_comp_data_tv
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_comp_data_tv`;
CREATE TABLE `cp_coord_comp_data_tv`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `signature_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `signature` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instalation_date` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `suscriptor` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contract` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT 1,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `suscriptor`(`suscriptor`) USING BTREE,
  INDEX `contract`(`contract`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_contact_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_contact_tmp`;
CREATE TABLE `cp_coord_contact_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `cont_id` int(11) NULL DEFAULT NULL,
  `reason_id` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 114655 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_log
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_log`;
CREATE TABLE `cp_coord_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `reason_id` int(11) NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_pre_sup
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_pre_sup`;
CREATE TABLE `cp_coord_pre_sup`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT 1,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 237 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_pre_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_pre_tmp`;
CREATE TABLE `cp_coord_pre_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coord_id` int(11) NULL DEFAULT NULL,
  `reason_id` int(11) NULL DEFAULT NULL,
  `pre_date` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pre_disp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sig_time_id` int(11) NULL DEFAULT 0,
  `sig_reason_id` int(11) NULL DEFAULT 0,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11273 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_proc_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_proc_tmp`;
CREATE TABLE `cp_coord_proc_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `resp_id` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11586 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_ref_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_ref_tmp`;
CREATE TABLE `cp_coord_ref_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `resp_id` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_serv
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_serv`;
CREATE TABLE `cp_coord_serv`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT NULL,
  `contact_id` int(20) NULL DEFAULT NULL,
  `view_id` int(20) NULL DEFAULT NULL,
  `edit_id` int(20) NULL DEFAULT NULL,
  `pre_id` int(20) NULL DEFAULT NULL,
  `status_id` int(20) NULL DEFAULT NULL,
  `coord_id` int(20) NULL DEFAULT NULL,
  `coord_ref` int(20) NULL DEFAULT NULL,
  `cancel` int(20) NULL DEFAULT NULL,
  `created_by` int(20) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `contact_id`(`contact_id`) USING BTREE,
  INDEX `view_id`(`view_id`) USING BTREE,
  INDEX `edit_id`(`edit_id`) USING BTREE,
  INDEX `pre_id`(`pre_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `coord_id`(`coord_id`) USING BTREE,
  INDEX `coord_ref`(`coord_ref`) USING BTREE,
  INDEX `cancel`(`cancel`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12877 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_serv_edit
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_serv_edit`;
CREATE TABLE `cp_coord_serv_edit`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT NULL,
  `service_id` int(20) NULL DEFAULT NULL,
  `user_id` int(20) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `service_id`(`service_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3605 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_coord_view_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_coord_view_tmp`;
CREATE TABLE `cp_coord_view_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `resp_id` int(11) NULL DEFAULT NULL,
  `created_by` int(11) NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12683 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_cypherdata
-- ----------------------------
DROP TABLE IF EXISTS `cp_cypherdata`;
CREATE TABLE `cp_cypherdata`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cypher` longblob NULL,
  `key_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `exp` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `c_type` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_d` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `user_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `key_id`(`key_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31647 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads`;
CREATE TABLE `cp_leads`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NULL DEFAULT NULL,
  `referred_id` int(20) NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `birthday` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gender` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `age` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `repre_legal` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_acc` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `phone_main` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `phone_alt` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `phone_other` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `email_main` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_other` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_main` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `add_postal` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `coor_lati` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_long` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` int(20) NULL DEFAULT NULL,
  `town_id` int(20) NULL DEFAULT NULL,
  `zip_id` int(20) NULL DEFAULT NULL,
  `h_type_id` int(20) NULL DEFAULT NULL,
  `h_roof_id` int(20) NULL DEFAULT NULL,
  `h_level_id` int(20) NULL DEFAULT NULL,
  `ident_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ident_exp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_exp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mkt_origen_id` int(20) NULL DEFAULT NULL,
  `mkt_medium_id` int(20) NULL DEFAULT NULL,
  `mkt_objective_id` int(20) NULL DEFAULT NULL,
  `mkt_post_id` int(20) NULL DEFAULT NULL,
  `mkt_forms_id` int(20) NULL DEFAULT NULL,
  `mkt_conv_id` int(20) NULL DEFAULT NULL,
  `mkt_service_id` int(20) NULL DEFAULT NULL,
  `mkt_campaign_id` int(20) NULL DEFAULT NULL,
  `additional` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_by` int(20) NULL DEFAULT NULL,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `phone_main`(`phone_main`) USING BTREE,
  INDEX `phone_alt`(`phone_alt`) USING BTREE,
  INDEX `phone_other`(`phone_other`) USING BTREE,
  INDEX `referred_id`(`referred_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167453 CHARACTER SET = latin1 COLLATE = latin1_spanish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_copy1
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_copy1`;
CREATE TABLE `cp_leads_copy1`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NULL DEFAULT NULL,
  `referred_id` int(20) NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `birthday` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gender` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `age` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `repre_legal` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_acc` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `phone_main` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `phone_alt` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `phone_other` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `email_main` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_other` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_main` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `add_postal` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `coor_lati` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_long` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT NULL,
  `town_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT NULL,
  `zip_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT NULL,
  `h_type_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT NULL,
  `h_roof_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT NULL,
  `h_level_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT NULL,
  `ident_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ident_exp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_exp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mkt_origen_id` int(20) NULL DEFAULT NULL,
  `mkt_medium_id` int(20) NULL DEFAULT NULL,
  `mkt_objective_id` int(20) NULL DEFAULT NULL,
  `mkt_post_id` int(20) NULL DEFAULT NULL,
  `mkt_forms_id` int(20) NULL DEFAULT NULL,
  `mkt_conv_id` int(20) NULL DEFAULT NULL,
  `mkt_service_id` int(20) NULL DEFAULT NULL,
  `mkt_campaign_id` int(20) NULL DEFAULT NULL,
  `additional` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_by` int(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `phone_main`(`phone_main`) USING BTREE,
  INDEX `phone_alt`(`phone_alt`) USING BTREE,
  INDEX `phone_other`(`phone_other`) USING BTREE,
  INDEX `referred_id`(`referred_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_spanish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_geographic
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_geographic`;
CREATE TABLE `cp_leads_geographic`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_id` int(10) NULL DEFAULT 1,
  `state_id` int(10) NULL DEFAULT 1,
  `township_id` int(10) NULL DEFAULT 1,
  `parish_id` int(10) NULL DEFAULT 1,
  `sector_id` int(10) NULL DEFAULT 1,
  `created_by` int(10) NULL DEFAULT 1,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `state_id`(`state_id`) USING BTREE,
  INDEX `township_id`(`township_id`) USING BTREE,
  INDEX `parish_id`(`parish_id`) USING BTREE,
  INDEX `sector_id`(`sector_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25700 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_new
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_new`;
CREATE TABLE `cp_leads_new`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NULL DEFAULT NULL,
  `referred_id` int(20) NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `birthday` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gender` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `age` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `repre_legal` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_acc` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `phone_main` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `phone_alt` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `phone_other` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL DEFAULT '1',
  `email_main` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_other` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_main` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `add_postal` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `coor_lati` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_long` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` int(20) NULL DEFAULT NULL,
  `town_id` int(20) NULL DEFAULT NULL,
  `zip_id` int(20) NULL DEFAULT NULL,
  `h_type_id` int(20) NULL DEFAULT NULL,
  `h_roof_id` int(20) NULL DEFAULT NULL,
  `h_level_id` int(20) NULL DEFAULT NULL,
  `ident_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ident_exp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ss_exp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mkt_origen_id` int(20) NULL DEFAULT NULL,
  `mkt_medium_id` int(20) NULL DEFAULT NULL,
  `mkt_objective_id` int(20) NULL DEFAULT NULL,
  `mkt_post_id` int(20) NULL DEFAULT NULL,
  `mkt_forms_id` int(20) NULL DEFAULT NULL,
  `mkt_conv_id` int(20) NULL DEFAULT NULL,
  `mkt_service_id` int(20) NULL DEFAULT NULL,
  `mkt_campaign_id` int(20) NULL DEFAULT NULL,
  `additional` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_by` int(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `phone_main`(`phone_main`) USING BTREE,
  INDEX `phone_alt`(`phone_alt`) USING BTREE,
  INDEX `phone_other`(`phone_other`) USING BTREE,
  INDEX `referred_id`(`referred_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 147534 CHARACTER SET = latin1 COLLATE = latin1_spanish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_parish
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_parish`;
CREATE TABLE `cp_leads_parish`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `township_id` int(10) NULL DEFAULT 1,
  `status_id` int(10) NULL DEFAULT 1,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `township_id`(`township_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_sector
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_sector`;
CREATE TABLE `cp_leads_sector`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `parish_id` int(10) NULL DEFAULT 1,
  `status_id` int(10) NULL DEFAULT 1,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `parish_id`(`parish_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 842 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_serv_objections
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_serv_objections`;
CREATE TABLE `cp_leads_serv_objections`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `objection_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `objection_id`(`objection_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_states
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_states`;
CREATE TABLE `cp_leads_states`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` int(10) NULL DEFAULT 1,
  `status_id` int(10) NULL DEFAULT 1,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `country_id`(`country_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_tmp`;
CREATE TABLE `cp_leads_tmp`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NULL DEFAULT NULL,
  `referred_id` int(20) NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `birthday` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gender` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `age` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `repre_legal` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_acc` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `phone_main` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_main_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `phone_alt` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_alt_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `phone_other` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_owner` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_other_provider` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `email_main` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_other` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `add_main` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `add_postal` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `coor_lati` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coor_long` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_id` int(11) NULL DEFAULT NULL,
  `town_id` int(11) NULL DEFAULT NULL,
  `zip_id` int(11) NULL DEFAULT NULL,
  `h_type_id` int(11) NULL DEFAULT NULL,
  `h_roof_id` int(11) NULL DEFAULT NULL,
  `h_level_id` int(11) NULL DEFAULT NULL,
  `type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(20) NULL DEFAULT NULL,
  `additional` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `referred_id`(`referred_id`) USING BTREE,
  INDEX `phone_main`(`phone_main`) USING BTREE,
  INDEX `phone_alt`(`phone_alt`) USING BTREE,
  INDEX `phone_other`(`phone_other`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 155557 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_tmp_mkt
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_tmp_mkt`;
CREATE TABLE `cp_leads_tmp_mkt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `origen_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `medium_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `objetive_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `post_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `forms_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `conv_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `services_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `campaign_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1574 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_leads_township
-- ----------------------------
DROP TABLE IF EXISTS `cp_leads_township`;
CREATE TABLE `cp_leads_township`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `states_id` int(10) NULL DEFAULT 1,
  `status_id` int(10) NULL DEFAULT 1,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `states_id`(`states_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mkt_assig_leads
-- ----------------------------
DROP TABLE IF EXISTS `cp_mkt_assig_leads`;
CREATE TABLE `cp_mkt_assig_leads`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_i_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `origen_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `medium_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `objective_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `post_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `form_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `conv_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `campaign_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1045 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mw_consolidate
-- ----------------------------
DROP TABLE IF EXISTS `cp_mw_consolidate`;
CREATE TABLE `cp_mw_consolidate`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idcliente` int(6) UNSIGNED ZEROFILL NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `asunto` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nfactura` int(8) UNSIGNED ZEROFILL NOT NULL,
  `total` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fdate` date NOT NULL,
  `transaccion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `amount` decimal(12, 2) NOT NULL,
  `cdate` date NOT NULL,
  `status_id` int(3) NOT NULL DEFAULT 0,
  `img` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `size` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` int(3) NOT NULL DEFAULT 1,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `idcliente`(`idcliente`) USING BTREE,
  INDEX `nfactura`(`nfactura`) USING BTREE,
  INDEX `transaccion`(`transaccion`) USING BTREE,
  INDEX `code`(`code`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4120 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mw_consolidate_copy1
-- ----------------------------
DROP TABLE IF EXISTS `cp_mw_consolidate_copy1`;
CREATE TABLE `cp_mw_consolidate_copy1`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idcliente` int(6) UNSIGNED ZEROFILL NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `asunto` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nfactura` int(8) UNSIGNED ZEROFILL NOT NULL,
  `total` decimal(12, 2) NOT NULL,
  `fdate` date NOT NULL,
  `transaccion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `amount` decimal(12, 2) NOT NULL,
  `cdate` date NOT NULL,
  `status_id` int(3) NOT NULL DEFAULT 0,
  `img` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `size` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` int(3) NOT NULL DEFAULT 1,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `idcliente`(`idcliente`) USING BTREE,
  INDEX `nfactura`(`nfactura`) USING BTREE,
  INDEX `transaccion`(`transaccion`) USING BTREE,
  INDEX `code`(`code`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1287 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mw_facturas
-- ----------------------------
DROP TABLE IF EXISTS `cp_mw_facturas`;
CREATE TABLE `cp_mw_facturas`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `factura` int(8) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `idcliente` int(6) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `emitido` date NULL DEFAULT NULL,
  `estado` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'No pagado',
  `sub_total` decimal(12, 0) NULL DEFAULT NULL,
  `total` decimal(12, 0) NULL DEFAULT NULL,
  `status_id` int(20) NULL DEFAULT 0,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `factura`(`factura`) USING BTREE,
  INDEX `idcliente`(`idcliente`) USING BTREE,
  INDEX `emitido`(`emitido`) USING BTREE,
  INDEX `estado`(`estado`) USING BTREE,
  INDEX `sub_total`(`sub_total`) USING BTREE,
  INDEX `total`(`total`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mw_manual
-- ----------------------------
DROP TABLE IF EXISTS `cp_mw_manual`;
CREATE TABLE `cp_mw_manual`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idcliente` int(6) UNSIGNED ZEROFILL NOT NULL,
  `code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `amount` decimal(12, 2) NOT NULL,
  `created_by` int(3) NOT NULL DEFAULT 1,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `idcliente`(`idcliente`) USING BTREE,
  INDEX `code`(`code`) USING BTREE,
  INDEX `amount`(`amount`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mw_payment_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_mw_payment_tmp`;
CREATE TABLE `cp_mw_payment_tmp`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `amount` decimal(12, 2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `fecha`(`fecha`) USING BTREE,
  INDEX `code`(`code`) USING BTREE,
  INDEX `amount`(`amount`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mw_payment_tmp2
-- ----------------------------
DROP TABLE IF EXISTS `cp_mw_payment_tmp2`;
CREATE TABLE `cp_mw_payment_tmp2`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `amount` decimal(12, 2) NOT NULL,
  `type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `fecha`(`fecha`) USING BTREE,
  INDEX `code`(`code`) USING BTREE,
  INDEX `amount`(`amount`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_mw_reporte
-- ----------------------------
DROP TABLE IF EXISTS `cp_mw_reporte`;
CREATE TABLE `cp_mw_reporte`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `factura` int(10) UNSIGNED ZEROFILL NOT NULL,
  `idcliente` int(6) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `factura`(`factura`) USING BTREE,
  INDEX `idcliente`(`idcliente`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_notes
-- ----------------------------
DROP TABLE IF EXISTS `cp_notes`;
CREATE TABLE `cp_notes`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NULL DEFAULT NULL,
  `note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `img` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `size` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(20) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1960110 CHARACTER SET = latin1 COLLATE = latin1_spanish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_objection_pr
-- ----------------------------
DROP TABLE IF EXISTS `cp_objection_pr`;
CREATE TABLE `cp_objection_pr`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tvs_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sec_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `int_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_ope_assig
-- ----------------------------
DROP TABLE IF EXISTS `cp_ope_assig`;
CREATE TABLE `cp_ope_assig`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `mkt_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `status_id` int(11) NULL DEFAULT NULL,
  `data_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `process_by` int(11) NULL DEFAULT NULL,
  `process_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `assigned_by` int(11) NULL DEFAULT NULL,
  `assigned_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `process_by`(`process_by`) USING BTREE,
  INDEX `assigned_by`(`assigned_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1045 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_ope_assig_resp
-- ----------------------------
DROP TABLE IF EXISTS `cp_ope_assig_resp`;
CREATE TABLE `cp_ope_assig_resp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mkt_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tv_pr_contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tv_pr_objection` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sec_pr_contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sec_pr_objection` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `int_pr_contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `int_pr_objection` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wrl_pr_contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wrl_pr_objection` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `mkt_id`(`mkt_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 769 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_ope_assig_resp_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_ope_assig_resp_tmp`;
CREATE TABLE `cp_ope_assig_resp_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tv_pr_contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tv_pr_objection` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sec_pr_contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sec_pr_objection` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `int_pr_contacted` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `int_pr_objection` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_ope_online_status
-- ----------------------------
DROP TABLE IF EXISTS `cp_ope_online_status`;
CREATE TABLE `cp_ope_online_status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `status_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 537 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_prev_tv_cancel_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_prev_tv_cancel_tmp`;
CREATE TABLE `cp_prev_tv_cancel_tmp`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT NULL,
  `reason_id` int(20) NULL DEFAULT 1,
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_prev_tv_conf_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_prev_tv_conf_tmp`;
CREATE TABLE `cp_prev_tv_conf_tmp`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT NULL,
  `resp_id` int(20) NULL DEFAULT 0,
  `reason_id` int(20) NULL DEFAULT 1,
  `date_conf` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_disp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_prev_tv_recoord_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_prev_tv_recoord_tmp`;
CREATE TABLE `cp_prev_tv_recoord_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT NULL,
  `date_conf` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_disp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_prev_tv_ret_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_prev_tv_ret_tmp`;
CREATE TABLE `cp_prev_tv_ret_tmp`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT NULL,
  `reason_id` int(20) NULL DEFAULT NULL,
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_prev_tv_serv
-- ----------------------------
DROP TABLE IF EXISTS `cp_prev_tv_serv`;
CREATE TABLE `cp_prev_tv_serv`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(20) NULL DEFAULT 0,
  `date_prev` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_disp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `conf_id` int(20) NULL DEFAULT 0,
  `recoord_id` int(20) NULL DEFAULT 0,
  `ret_id` int(20) NULL DEFAULT 0,
  `cancel_id` int(20) NULL DEFAULT 0,
  `status_id` int(20) NULL DEFAULT 0,
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_referred
-- ----------------------------
DROP TABLE IF EXISTS `cp_referred`;
CREATE TABLE `cp_referred`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `birthday` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `referred_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `referred_id`(`referred_id`) USING BTREE,
  INDEX `type_id`(`type_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1534 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_referred_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_referred_tmp`;
CREATE TABLE `cp_referred_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `birthday` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `referred_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_serv_prop
-- ----------------------------
DROP TABLE IF EXISTS `cp_serv_prop`;
CREATE TABLE `cp_serv_prop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `owen_id` int(11) NULL DEFAULT NULL,
  `assistan_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket_id`(`ticket_id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `owen_id`(`owen_id`) USING BTREE,
  INDEX `assistan_id`(`assistan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17989 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_int
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_int`;
CREATE TABLE `cp_service_pr_int`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `int_res_id` int(11) NULL DEFAULT NULL,
  `pho_res_id` int(11) NULL DEFAULT NULL,
  `int_com_id` int(11) NULL DEFAULT NULL,
  `pho_com_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2905 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_int_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_int_tmp`;
CREATE TABLE `cp_service_pr_int_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `int_res_id` int(11) NULL DEFAULT NULL,
  `pho_res_id` int(11) NULL DEFAULT NULL,
  `int_com_id` int(11) NULL DEFAULT NULL,
  `pho_com_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `owen_id` int(11) NULL DEFAULT NULL,
  `assit_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5177 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_mr
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_mr`;
CREATE TABLE `cp_service_pr_mr`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `cant` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ssid` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `red_id` int(11) NULL DEFAULT NULL,
  `password` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_mr_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_mr_tmp`;
CREATE TABLE `cp_service_pr_mr_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `cant` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ssid` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `red_id` int(11) NULL DEFAULT NULL,
  `password` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `owen_id` int(11) NULL DEFAULT NULL,
  `assit_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_sec
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_sec`;
CREATE TABLE `cp_service_pr_sec`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `previously_id` int(11) NULL DEFAULT NULL,
  `equipment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `cameras_id` int(11) NULL DEFAULT NULL,
  `comp_equipment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `dvr_id` int(11) NULL DEFAULT NULL,
  `add_equipment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `password_alarm` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `contact_1` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_1_desc` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_2_desc` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_3` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_3_desc` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 313 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_sec_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_sec_tmp`;
CREATE TABLE `cp_service_pr_sec_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `previously_id` int(11) NULL DEFAULT NULL,
  `equipment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `cameras_id` int(11) NULL DEFAULT NULL,
  `comp_equipment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `dvr_id` int(11) NULL DEFAULT NULL,
  `add_equipment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `password_alarm` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `contact_1` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_1_desc` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_2_desc` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_3` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_3_desc` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `owen_id` int(11) NULL DEFAULT NULL,
  `assit_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 563 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_tv
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_tv`;
CREATE TABLE `cp_service_pr_tv`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `tv` int(11) NULL DEFAULT NULL,
  `package_id` int(11) NULL DEFAULT NULL,
  `provider_id` int(11) NULL DEFAULT NULL,
  `decoder_id` int(11) NULL DEFAULT NULL,
  `dvr_id` int(11) NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `advertising_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `ch_hd` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_dvr` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_hbo` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_cinemax` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_starz` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_showtime` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gif_ent` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gif_choice` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gif_xtra` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6252 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_pr_tv_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_pr_tv_tmp`;
CREATE TABLE `cp_service_pr_tv_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `tv` int(11) NULL DEFAULT NULL,
  `package_id` int(11) NULL DEFAULT NULL,
  `provider_id` int(11) NULL DEFAULT NULL,
  `decoder_id` int(11) NULL DEFAULT NULL,
  `dvr_id` int(11) NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `advertising_id` int(191) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `ch_hd` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_dvr` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_hbo` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_cinemax` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_starz` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ch_showtime` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gif_ent` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gif_choice` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gif_xtra` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `owen_id` int(11) NULL DEFAULT NULL,
  `assit_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13668 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_vzla_int
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_vzla_int`;
CREATE TABLE `cp_service_vzla_int`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_id` int(11) NULL DEFAULT NULL,
  `int_res_id` int(11) NULL DEFAULT NULL,
  `pho_res_id` int(11) NULL DEFAULT NULL,
  `int_com_id` int(11) NULL DEFAULT NULL,
  `pho_com_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `bank_id` int(11) NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `amount` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transference` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `trans_date` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `additional` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `operator_id`(`operator_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8499 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_service_vzla_int_tmp
-- ----------------------------
DROP TABLE IF EXISTS `cp_service_vzla_int_tmp`;
CREATE TABLE `cp_service_vzla_int_tmp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `int_res_id` int(11) NULL DEFAULT NULL,
  `pho_res_id` int(11) NULL DEFAULT NULL,
  `int_com_id` int(11) NULL DEFAULT NULL,
  `pho_com_id` int(11) NULL DEFAULT NULL,
  `ref_id` int(11) NULL DEFAULT NULL,
  `ref_list_id` int(11) NULL DEFAULT NULL,
  `sup_id` int(11) NULL DEFAULT NULL,
  `sup_ope_id` int(11) NULL DEFAULT NULL,
  `bank_id` int(11) NULL DEFAULT NULL,
  `payment_id` int(11) NULL DEFAULT NULL,
  `amount` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transference` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `trans_date` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `service_id` int(11) NULL DEFAULT NULL,
  `type_order_id` int(11) NULL DEFAULT NULL,
  `origen_id` int(11) NULL DEFAULT 1,
  `additional` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `owen_id` int(11) NULL DEFAULT NULL,
  `assit_id` int(11) NULL DEFAULT NULL,
  `created_at` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10621 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_status_leads
-- ----------------------------
DROP TABLE IF EXISTS `cp_status_leads`;
CREATE TABLE `cp_status_leads`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `status_id` int(11) NULL DEFAULT NULL,
  `online` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `offline` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17402 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_sup_serv_sheet
-- ----------------------------
DROP TABLE IF EXISTS `cp_sup_serv_sheet`;
CREATE TABLE `cp_sup_serv_sheet`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NULL DEFAULT 0,
  `visit_date` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `technical_id` int(20) NULL DEFAULT 1,
  `phone_main` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coordinations` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `town_id` int(100) NULL DEFAULT 1,
  `add_main` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `plan` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_current` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_public` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `signal_r` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transmitions` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ap_current` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `speedtest_rx` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `speedtest_tx` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reason` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `prev_int_id` int(20) NULL DEFAULT 0,
  `status_id` int(20) NULL DEFAULT 0,
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `visit_date`(`visit_date`) USING BTREE,
  INDEX `prev_int_id`(`prev_int_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1475 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_sup_serv_sheet_ve
-- ----------------------------
DROP TABLE IF EXISTS `cp_sup_serv_sheet_ve`;
CREATE TABLE `cp_sup_serv_sheet_ve`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(20) NULL DEFAULT 0,
  `visit_date` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `technical_id` int(20) NULL DEFAULT 1,
  `phone_main` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `coordinations` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `town_id` int(20) NULL DEFAULT 1,
  `add_main` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `plan` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_current` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_public` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `signal_r` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transmitions` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ap_current` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `speedtest_rx` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `speedtest_tx` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reason` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `prev_int_id` int(20) NULL DEFAULT 0,
  `status_id` int(20) NULL DEFAULT 0,
  `created_by` int(20) NULL DEFAULT 1,
  `created_at` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `client_id`(`client_id`) USING BTREE,
  INDEX `visit_date`(`visit_date`) USING BTREE,
  INDEX `prev_int_id`(`prev_int_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `created_by`(`created_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 747 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_tickets
-- ----------------------------
DROP TABLE IF EXISTS `cp_tickets`;
CREATE TABLE `cp_tickets`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ticket_dep` int(11) NULL DEFAULT NULL,
  `dep_id` int(11) NULL DEFAULT NULL,
  `serv_id` int(11) NULL DEFAULT NULL,
  `code` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `information` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status_id` int(11) NULL DEFAULT NULL,
  `description` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `operator_id` int(11) NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `finish_by` int(11) NULL DEFAULT NULL,
  `finish_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ticket`(`ticket`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 452 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Table structure for cp_time_call_new
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_call_new`;
CREATE TABLE `cp_time_call_new`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `departament` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT 1,
  `inma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbt` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouna` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ounat` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls`;
CREATE TABLE `cp_time_calls`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_date` timestamp(0) NULL DEFAULT NULL,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team_id` int(11) NULL DEFAULT 1,
  `inb_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_no_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_no_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `t_total` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `call_date`(`call_date`) USING BTREE,
  INDEX `ext`(`ext`) USING BTREE,
  INDEX `team`(`team`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33837 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_mcs
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_mcs`;
CREATE TABLE `cp_time_calls_mcs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `departament` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT 1,
  `inma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbt` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubto` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubtot` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouna` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ounat` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cacalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ticalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_pr
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_pr`;
CREATE TABLE `cp_time_calls_pr`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_date` timestamp(0) NULL DEFAULT NULL,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team_id` int(11) NULL DEFAULT 1,
  `inb_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_no_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_no_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `t_total` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `call_date`(`call_date`) USING BTREE,
  INDEX `ext`(`ext`) USING BTREE,
  INDEX `team`(`team`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_pr_new
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_pr_new`;
CREATE TABLE `cp_time_calls_pr_new`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `departament` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT 1,
  `inma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbt` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubto` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubtot` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouna` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ounat` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cacalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ticalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7470 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_psicoemprende
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_psicoemprende`;
CREATE TABLE `cp_time_calls_psicoemprende`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `departament` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT 1,
  `inma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbt` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubto` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubtot` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouna` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ounat` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cacalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ticalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_us_new
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_us_new`;
CREATE TABLE `cp_time_calls_us_new`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `departament` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT 1,
  `inma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbt` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubto` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubtot` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouna` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ounat` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cacalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ticalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 203 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_voix
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_voix`;
CREATE TABLE `cp_time_calls_voix`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `departament` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT 1,
  `inma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbt` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubto` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubtot` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouna` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ounat` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cacalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ticalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_vz_new
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_vz_new`;
CREATE TABLE `cp_time_calls_vz_new`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `departament` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country` int(11) NULL DEFAULT 1,
  `inma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inme120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbt` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouma120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oume120t` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubto` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `oubtot` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ouna` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ounat` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cacalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ticalls` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5078 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cp_time_calls_vzla
-- ----------------------------
DROP TABLE IF EXISTS `cp_time_calls_vzla`;
CREATE TABLE `cp_time_calls_vzla`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `call_date` timestamp(0) NULL DEFAULT NULL,
  `username` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `team_id` int(11) NULL DEFAULT 1,
  `inb_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inb_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_no_ans` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out_no_time` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `t_total` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `call_date`(`call_date`) USING BTREE,
  INDEX `ext`(`ext`) USING BTREE,
  INDEX `team`(`team`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12223 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
