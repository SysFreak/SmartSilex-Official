<?php

session_start();

$connect 	=	New PDO('mysql:host=localhost;dbname=boomcrm_solu', 'root', 'B00mS3rv3rDB@2020');

$date = date("Y-d-m");

if($_POST['type'] == "events")
{
	$query = 'SELECT * FROM events WHERE operator_id = "'.$_SESSION['id'].'" AND start_event LIKE "'.$date.'%" AND process = "1" AND notifie = "0" AND deleted = "0"';

	$statement	=	$connect->prepare($query);
	$statement->execute();
	$result 	=	$statement->fetchAll(PDO::FETCH_ASSOC);

	if(!empty($result))
	{
		foreach ($result as $r => $res) 
		{
			$info[$r]  = array('status' => true, 'title' => $res['title'], 'description' => $res['description'], 'color' => $res['color'], 'id' => $res['id']);
		}
		
		echo json_encode($info);

	}else{
		echo json_encode(array('status' => false));
	}

}

if($_POST['type'] == "eventsView")
{
	$query = 'SELECT * FROM events WHERE operator_id = "'.$_SESSION['id'].'"  AND process = "1" AND notifie = "1" AND deleted = "0" AND view = "0"';

	$statement	=	$connect->prepare($query);
	$statement->execute();
	$result 	=	$statement->fetchAll(PDO::FETCH_ASSOC);
	$row 		=	$statement->rowCount();

	$html 		=	""; 

	if($row > 0)
	{
		$html.='<ul class="notification-body">';

		foreach ($result as $v => $val) 
		{

			$html.='<li>';
			$html.='<span class="padding-10 unread">';
			$html.='<em class="badge padding-5 no-border-radius bg-color-purple txt-color-white pull-left margin-right-5">';
			$html.='<i class="fa fa-calendar fa-fw fa-2x"></i>';
			$html.='</em>';
			$html.='<span>';
			$html.='<a href="javascript:void(0);" class="display-normal"><strong>Titulo</strong></a>: '.$val['title'].' ';
			$html.='<br>';
			$html.='<strong>Mensaje</strong>: '.$val['description'].' ';
			$html.='<br>';
			$html.='<strong style="margin-left: 45px;">Inicio: </strong> '.$val['start_event'].'<br>';
			$html.='<strong style="margin-left: 45px;">Fin: </strong> '.$val['end_event'].'<br>';
			$html.='</span>';
			$html.='</span>';
			$html.='</li>';
		}

		$html.='</ul>';

		$info = array('numb' => $row, 'html' => $html);
	}else{

		$info = array('numb' => 0, 'html' => '');
	}

	echo json_encode($info);

}

if($_POST['type'] == "eventsNot")
{
	if(isset($_POST['id']))
	{
		$query = " UPDATE events SET notifie=:notifie WHERE id =:id";

		$statement = $connect->prepare($query);
		
		$statement->execute(
			array(
				':notifie'	=> "1",
				':id'			=> $_POST['id'],
			)
		);
	}

}

if($_POST['type'] == "eventsHide")
{
	$query = 'UPDATE events SET view = "1" WHERE operator_id = "'.$_SESSION['id'].'" AND process = "1" AND notifie = "1"';
	$statement = $connect->prepare($query);
	$statement->execute();

}