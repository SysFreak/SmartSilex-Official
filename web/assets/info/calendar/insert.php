<?php

session_start();

require '../../../../vendor/autoload.php';

$connect 	=	New PDO('mysql:host=localhost;dbname=boomcrm_solu', 'root', 'B00mS3rv3rDB@2020');

$params     =   [];

parse_str($_POST['value'], $params);

$info =	[
	'client'		=>	"",
	'title'			=>	strtoupper(clean($params['title'])),
	'description'	=>	strtoupper(clean($params['description'])),
	'operator_id'	=>	$_SESSION['id'],
	'color'			=>	$params['priority'],
	'process'		=>	"0",
	'notifie'		=>	"0",
	'view'			=>	"0",
	'deleted'		=>	"0",
	'start_event'	=>	$params['dateEvent'].' '.$params['i_hora'].':'.$params['i_min'].':00',
	'end_event'		=>	$params['dateEvent'].' '.$params['f_hora'].':'.$params['f_min'].':00',
];

$query = " 
	INSERT INTO events
	(client_id, title, description, operator_id, color, process, notifie, view, deleted, start_event, end_event)
	VALUES (:client, :title, :description, :operator_id, :color, :process, :notifie, :view, :deleted, :start_event, :end_event)
";
$statement = $connect->prepare($query);

$statement->execute(
	array(
		':client'		=>	"",
		':title'		=>	strtoupper(clean($params['title'])),
		':description'	=>	strtoupper(clean($params['description'])),
		':operator_id'	=>	$_SESSION['id'],
		':color'		=>	$params['priority'],
		':process'		=>	"0",
		':notifie'		=>	"0",
		':view'			=>	"0",
		':deleted'		=>	"0",
		':start_event'	=>	$params['dateEvent'].' '.$params['i_hora'].':'.$params['i_min'].':00',
		':end_event'	=>	$params['dateEvent'].' '.$params['f_hora'].':'.$params['f_min'].':00',
	)
);

function clean($str) {
    $str = @trim($str);
    if(get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return $str;
}