<?php

session_start();

require '../../../../vendor/autoload.php';

$connect 	=	New PDO('mysql:host=localhost;dbname=boomcrm_solu', 'root', 'B00mS3rv3rDB@2020');

if(isset($_POST['id']))
{
	$query = "
		UPDATE events
			SET deleted=:deleted
			WHERE id =:id
	";

	$statement = $connect->prepare($query);

	$statement->execute(
		array(
			':deleted'		=> "1",
			':id'			=> $_POST['id']
		)
	);
}