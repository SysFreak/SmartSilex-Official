$(document).ready(function() 
{

	startTime();

	var d = new Date();

	$('#pro_date').datepicker({
		dateFormat: 'mm-dd-yy',
		yearRange: 'c-60:c+10',
		defaultDate: "+1w",
		changeMonth: true,
		changeYear: true,
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
	});

	$('#dolar_date').datepicker({
		dateFormat : 'yy-mm-dd',
		yearRange: 'c-60:c+10',
		defaultDate: "+1w",
		changeMonth: true,
		changeYear: true,
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
	});

	$("#ced_numb").keypress(function(e) {
		if(e.which == 13) {
			ced = $("#ced_numb").val();
			typ = $("#ced_type").val();
			$.ajax({ type: "POST", url:  "/Leads/InfoVe" ,data:{ced: ced, typ: typ},
			success: function(datos)
			{
				if(datos.status == true)
				{
					$("#ResultInfoVe").html(datos.html);
				}else{
					$("#ResultInfoVe").html(datos.html);
				}
			}
			});
		}
	});

	$( "#ivu_in" ).keypress(function() {
	  var porc 	= 	$('#ivu_porc').val();
	  var int 	=	$('#ivu_in').val();

	  var ivu 	=	Math.round(((porc * int) / 100)*100)/100;

	  $('#ivu_view_porc').val(ivu);

	  var total 	=	parseFloat(int)+ parseFloat(ivu);

	  $('#ivu_out').val(total);
	});

	$( "#pro_in" ).keypress(function() {
		var date 	=	$('#pro_date').val().substr(3,2);
		var price 	= 	$('#pro_in').val();
		var dT 		=	"30";
		var dR 		=	dT - date;
		if(dR <= 0) { var total = "0.00"; }else{ var total 	=	((price / dT) * dR).toFixed(3); }
		$('#pro_out').val(total);
	});

	$.ajax({ type: "POST", url:  "/Calendar/ListView",
	success: function(datos)
	{
		if(datos.status == true)
		{
			$("#numb2").html(datos.cant);
			$("#NotBody").html(datos.html);
		}else{
			$("#numb2").html('0');
			$("#NotBody").html('');
		}
	}
	});

	// setInterval(function(){ 

	// 	$.ajax({ type: "POST", url:  "/Calendar/Notifie",
 //        success: function(datos)
 //        {
 //            if(datos.status == true){ 
 //             	$.each(datos.events, function(index, info){        		
	//  				$.ajax({ type: "POST", url:  "/Calendar/Notifie/Change", data: {id: info.id},
	// 				success: function(datos)
	// 				{
	//               		$.bigBox({
	// 						title : info.title,
	// 						content : info.decription,
	// 						color : info.color,
	// 						icon : "fa fa-warning shake animated",
	// 						timeout : 30000
	// 					});
	// 				}
	// 				});
 //              	});
              
 //            }
 //        }
 //      	});

	// }, 45000);

	// setInterval(function(){
	// 	$.ajax({ type: "POST", url:  "/Calendar/ListView",
	// 	success: function(datos)
	// 	{
	// 		if(datos.status == true)
	// 		{
 //            	$("#numb2").html(datos.cant);
 //            	$("#NotBody").html(datos.html);
	// 		}else{
	// 			$("#numb2").html('0');
	// 			$("#NotBody").html('');
	// 		}
	// 	}
	// 	});
	// }, 45000);

	$('#inline').change(function(event) {
		var str = $("#clientStatus").serialize();
		$.ajax({ type: "POST", url:  "/Leads/ChangeStatus", data:{str: str},
		success: function(datos)
		{
			if(datos.status == 'OCCUPIED')
			{
				console.log(datos.status);
			}
		}
		});
	});

	$("#SearchCypherClient").submit(function( event ) {
		$.ajax({ type: "POST", url:  "/Search/CypherClient", data:{str: $("#search-cypher").val()},
		success: function(datos)
		{
			if(datos.status == true)
			{
				$("#InfoCypherClient").html(datos.html);
			}else{
				$("#InfoCypherClient").html(datos.html);
			}
		}
		});
	  event.preventDefault();
	});

	$("#SearchCypherCl").submit(function( event ) {
		var str = $("#SearchCypherCl").serialize();
		$.ajax({ type: "POST", url:  "/Search/CypherClientType", data:{str: str},
		success: function(datos)
		{
			if(datos.status == true)
			{
				$("#InfoCypherClient").html(datos.html);
			}else{
				$("#InfoCypherClient").html(datos.html);
			}
		}
		});
	  event.preventDefault();
	});
	
});

////////////////////////////// Referred Form //////////////////////////////
  
function CalcIvu()
{
	$('#CalcIvuModal').modal('show');

}

$( "#ivu_in" ).keypress(function() {
  var porc 	= 	$('#ivu_porc').val();
  var int 	=	$('#ivu_in').val();

  var ivu 	=	Math.round(((porc * int) / 100)*100)/100;

  $('#ivu_view_porc').val(ivu);

  var total 	=	parseFloat(int)+ parseFloat(ivu);

  $('#ivu_out').val(total);
});

function CypherInfoClient()
{
	$('#CypherDataClient').modal('show');
}

function HideActivity()
{
	$.ajax({ type: "POST", url:  "/Calendar/Notifie/ChangeView",
	success: function(datos)
	{
		setInterval(function(){
			$("#numb2").html('');
			$("#NotBody").html('');
		}, 20000);
	}
	});

}

function NewTicket()
{

	$.ajax({ type: "POST", url:  "/Ticket/Modal",
	beforeSend: function(request){
      blockUI();
    },
	success: function(datos)
	{
		if(datos.status == 	true)
		{
			$("#ticket_ope_html").html(datos.users);
			$("#ticket_dep_html").html(datos.deps);

			$('#tic_dep').change(function(event) {
				$.ajax({ type: "POST", url:  "/Ticket/Service", data:{id: $('#tic_dep').val() },
				success: function(datos)
				{
					$("#ticket_serv_html").html(datos.servs);
				}
				});
			});
		}
	$.unblockUI();
	}
	});
	$('#TicketModal').modal('show');

}

function NewTicketGen()
{
	$.ajax({ type: "POST", url:  "/tickets/getservice",
	success: function(datos)
	{
		if(datos.status == true)
		{
			$('#ticket_dep_new').html(datos.depart);
			
			$('#ticket_ope_dat').html(datos.operator);
			$('#ticket_ope_sup').html(datos.operator);
			$('#ticket_ope_pay').html(datos.operator);
			$('#ticS_serv').html(datos.support);
			$('#ticP_serv').html(datos.payment);
			$('#ticD_serv').html(datos.data);

			$('#ticket_ope_html').html(datos.operator);
			
			$('#tic_departM').change(function(event) {
				var tp 	=	$('#tic_departM').val();
				if(tp == 1)
				{
					$('#TicketSelect').modal('hide');
					$('#FormTicketSupportSubmit')[0].reset();
					$('#TicketSupport').modal('show');

				}else if (tp == 9) {
					$('#TicketSelect').modal('hide');
					$('#FormTicketPaymentSubmit')[0].reset();
					$('#TicketPayment').modal('show');

				}else if (tp == 6)
				{
					$('#FormTicketSubmit')[0].reset();
					$.ajax({ type: "POST", url:  "/Ticket/Modal",
					success: function(datos)
					{
						if(datos.status == 	true)
						{
							$("#ticket_ope_html").html(datos.users);
							$("#ticket_dep_html").html(datos.deps);

							$('#tic_dep').change(function(event) {
								$.ajax({ type: "POST", url:  "/Ticket/Service", data:{id: $('#tic_dep').val() },
								success: function(datos)
								{
									$("#ticket_serv_html").html(datos.servs);
								}
								});
							});
						}
					$.unblockUI();
					}
					});
					$('#TicketSelect').modal('hide');
					$('#TicketModal').modal('show');
				}

			});
		}

	}
	});
	$.unblockUI();
	$('#TicketSelect').modal('show');

}

function SaveTicket()
{
	var str 	= 	$("#FormTicketSubmit").serialize();
	$.ajax({ type: "POST", url:  "/Ticket/SubmitTicket", data:{str: str},
	beforeSend: function(request){
      blockUI();
    },
	success: function(datos)
	{
		if(datos.status == true)
		{
			$('#TicketModal').modal('hide');
			$("#TicketContent").html('');
			$("#TicketContent").html(datos.html);
			$('#TicketModalResp').modal('show');
		}else{
			$.smallBox({
			    title : datos.title,
			    content : datos.content,
			    color : "#C46A69",
			    iconSmall : "fa fa-warning shake animated",
				timeout : 10000
			});
		}
		$.unblockUI();
	}
	});

}

function SaveTicketSupport()
{
	var str = 	$("#FormTicketSupportSubmit").serialize();

	$.ajax({ type: "POST", url:  "/tickets/submitticketsup", data:{str: str},
	beforeSend: function(request){
      $('#AlertTicketSupport').html('<div class="alert alert-warning fade in"><i class="fa-fw fa fa-warning"></i><strong> Procesando </strong> Por Favor Espere...</div>');
    },
	success: function(datos)
	{
		if(datos.status == true)
		{
			$('#TicketSupport').modal('hide');
			$("#TicketContent").html('');
			$("#TicketContent").html(datos.html);
			$('#TicketModalResp').modal('show');
			$('#FormTicketSupportSubmit')[0].reset();
			$('#AlertTicketData').html('');

		}else{
			$.smallBox({
			    title : datos.title,
			    content : datos.content,
			    color : "#C46A69",
			    iconSmall : "fa fa-warning shake animated",
				timeout : 10000
			});
			$('#AlertTicketData').html('');
		}
		$.unblockUI();
	}
	});

}

function SaveTicketPayment()
{
	var str = 	$("#FormTicketPaymentSubmit").serialize();

	$.ajax({ type: "POST", url:  "/tickets/submitticketpay", data:{str: str},
	beforeSend: function(request){
      $('#AlertTicketPayment').html('<div class="alert alert-warning fade in"><i class="fa-fw fa fa-warning"></i><strong> Procesando </strong> Por Favor Espere...</div>');
    },
	success: function(datos)
	{
		if(datos.status == true)
		{
			$('#TicketPayment').modal('hide');
			$("#TicketContent").html('');
			$("#TicketContent").html(datos.html);
			$('#TicketModalResp').modal('show');
			$('#AlertTicketData').html('');
		}else{
			$.smallBox({
			    title : datos.title,
			    content : datos.content,
			    color : "#C46A69",
			    iconSmall : "fa fa-warning shake animated",
				timeout : 10000
			});
			$('#AlertTicketData').html('');
		}
		$.unblockUI();
	}
	});

}

function SaveTicketData()
{
	var str = 	$("#FormTicketPaymentSubmit").serialize();

	$.ajax({ type: "POST", url:  "/tickets/submitticketdat", data:{str: str},
	beforeSend: function(request){
      $('#AlertTicketData').html('<div class="alert alert-warning fade in"><i class="fa-fw fa fa-warning"></i><strong> Procesando </strong> Por Favor Espere...</div>');
    },
	success: function(datos)
	{
		if(datos.status == true)
		{
			$('#TicketPayment').modal('hide');
			$("#TicketContent").html('');
			$("#TicketContent").html(datos.html);
			$('#TicketModalResp').modal('show');
			$('#AlertTicketData').html('');
		}else{
			$.smallBox({
			    title : datos.title,
			    content : datos.content,
			    color : "#C46A69",
			    iconSmall : "fa fa-warning shake animated",
				timeout : 10000
			});
			$('#AlertTicketData').html('');
		}
		$.unblockUI();
	}
	});

}

function ChangePasswordID()
{
	var str 	= 	$("#ChangePasswordSub").serialize();
	$.ajax({ type: "POST", url:  "/ChangePasswordSubmit", data:{str: str},
	beforeSend: function(request){
      $('#PasswordAlert').html('<div class="alert alert-info fade in"><i class="fa-fw fa fa-times"></i><strong>Procesando!</strong> Por Favor Espere.</div>');
    },
	success: function(datos)
	{
		if(datos.status == true)
		{
 			$('#ChangePasswordSub')[0].reset();
 			$('#ChangePassword').modal('hide');

		}else{
			$('#PasswordAlert').html('<div class="alert alert-danger fade in"><i class="fa-fw fa fa-times"></i><strong>Error!</strong> Password <strong>NO</strong> Coinciden.</div>');
		}
	}
	});

}

function blockUI()
{
  	$.blockUI({ css: 
  	{
		border: 'none',
		padding: '15px',
		backgroundColor: '#000',
		'-webkit-border-radius': '10px',
		'-moz-border-radius': '10px',
		opacity: .5,
		color: '#fff'
	}});

}

function startTime() 
{
    var today = new Date();
    var hr = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();
    ap = (hr < 12) ? '<span style="margin-left: 10px;">AM</span>' : '<span>PM</span>';
    hr = (hr == 0) ? 12 : hr;
    hr = (hr > 12) ? hr : hr;
    //Add a zero in front of numbers<10
    hr = checkTime(hr);
    min = checkTime(min);
    sec = checkTime(sec);
    document.getElementById("clock").innerHTML = hr + ":" + min + ":" + sec;
    
    var months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    var curWeekDay = days[today.getDay()];
    var curDay = today.getDate();
    var curMonth = months[today.getMonth()];
    var curYear = today.getFullYear();
    var date = curMonth+" "+curDay+" "+curYear;
    document.getElementById("date").innerHTML = date;
    
    var time = setTimeout(function(){ startTime() }, 500);

}

function checkTime(i) 
{
    if (i < 10) {
        i = "0" + i;
    }
    return i;

}

function InfoDollar()
{

	$.ajax({ type: "POST", url:  "/collection/ve/dollarview",
	success: function(datos)
	{
		if(datos.status == true)
		{ 
		  $('#DolarTable').html(datos.html);
		  $('#TableDataDollar').DataTable({
		    aLengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		    pageLength: 5
		  });
		  $.unblockUI();
		}else{ $.unblockUI(); ErrorNotified(datos); }
	}
	});

	$('#DollarModal').modal('show');
	}

	$('#DollarForms').parsley().on('field:validated', function() {
	  var ok = $('.parsley-error').length === 0;
	}).on('form:submit', function() {
	  blockUI(); 
	  var str = $("#DollarForms").serialize();
	  $.ajax({ type: "POST", url:  "/collection/ve/dollar", data: {str: str},
	  success: function(datos)
	  {
	      if(datos.status == true){ 
	        $('#DollarForms')[0].reset();
	        $('#DolarTable').html(datos.html);
	        $('#TableDataDollar').DataTable({
	          aLengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
	          pageLength: 5
	        });
	        $.unblockUI();
	      }else{ $.unblockUI() }
	  }
	  });
	  return false;
	});