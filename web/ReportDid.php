<?php
require '../vendor/autoload.php';
require '../src/Lib/DBSmart.php';
require '../src/Lib/DBPbx.php';
require '../src/Lib/DBPbxVzla.php';

$user 		=	$_REQUEST['user'];
$dateIn 	= 	$_REQUEST['dateIn'];
$dateFn 	= 	$_REQUEST['dateFn'];
$country 	= 	$_REQUEST['country'];

$cont 		=	$contf 	=	0;

$Smart      =   new \App\Lib\DBsmart();
$PBX 		=	new \App\Lib\DBPbx();
$PBXVZ 		=	new \App\Lib\DBPbxVzla();


if($country == 1)
{
	$cou 		=	'PR';
	$query 		=	'SELECT DISTINCT(did) FROM cdr WHERE calldate BETWEEN "'.$dateIn.' 00:00:00" AND "'.$dateFn.' 23:59:59" AND did <> "" GROUP BY did';
	$dids 		=	$PBX->DBQueryAll($query);

	if($dids <> false)
	{
	    foreach ($dids as $d => $did) 
	    {
	        if( substr($did['did'], 0, 3) == "787" )
	        {
	            $di[$d] =   substr($did['did'], -10);
	        }
	    }

	    foreach ($di as $dInb) 
	    {

			$query  =   'SELECT DISTINCT(src) FROM cdr WHERE did = "'.$dInb.'" AND calldate BETWEEN "'.$dateIn.' 00:00:00" AND "'.$dateFn.' 23:59:59" GROUP BY src ORDER BY calldate ASC';
			$res    =   $PBX->DBQueryAll($query);

			if($res <> false)
			{
				foreach ($res as $re) 
				{
                    $nData[$cont++] =   (strlen($re['src']) > 10) ? (substr($re['src'],-10)) : $re['src'];
				}

			}
		}
	
	}

	$nTotal     =   array_unique($nData);

	foreach ($nTotal as $nIn) 
	{
        $query  =   'SELECT calldate, src, did FROM cdr_copy WHERE src LIKE "%'.$nIn.'%" AND lastapp = "Queue" AND did <> "" AND calldate BETWEEN "'.$dateIn.' 00:00:00" AND "'.$dateFn.' 23:59:59" ORDER BY calldate ASC LIMIT 1';
        $nRes   =    $PBX->DBQuery($query);

		$numb   =   (strlen($nRes['src']) > 10) ? (substr($nRes['src'],-10)) : $nRes['src'];

        $query  =   'SELECT client_id FROM cp_leads WHERE phone_main LIKE "%'.$numb.'%" OR phone_alt LIKE "%'.$numb.'%"';
        $lead   =    $Smart->DBQuery($query);

        $smart  =   ($lead) ? "true" : "false";

        if($sel == false)
        {

        	$rows[$contf++]	=	[
        		'calldate'	=>	$nRes['calldate'],
        		'phone'		=>	$numb,
        		'did'		=>	$nRes['did'],
        		'smart'		=>	$smart
        	];
        }


        $rRes = $numb = $lead = $smart = $sel = $cdr = "";
	
	}

}
elseif ($country == 4) 
{
	$cou 		=	'VE';
	$query 		=	'SELECT DISTINCT(did) FROM cdr WHERE calldate BETWEEN "'.$dateIn.' 00:00:00" AND "'.$dateFn.' 23:59:59" AND did <> "" GROUP BY did';
	$dids 		=	$PBXVZ->DBQueryAll($query);

	if($dids <> false)
	{
	    foreach ($dids as $d => $did) 
	    {
	        if( substr($did['did'], 0, 3) == "787" )
	        {
	            $di[$d] =   substr($did['did'], -10);
	        }
	    }

	    foreach ($di as $dInb) 
	    {

			$query  =   'SELECT DISTINCT(src) FROM cdr WHERE did = "'.$dInb.'" AND calldate BETWEEN "'.$dateIn.' 00:00:00" AND "'.$dateFn.' 23:59:59" GROUP BY src ORDER BY calldate ASC';
			$res    =   $PBXVZ->DBQueryAll($query);

			if($res <> false)
			{
				foreach ($res as $re) 
				{
                    $nData[$cont++] =   (strlen($re['src']) > 10) ? (substr($re['src'],-10)) : $re['src'];
				}

			}
		}
	
	}

	$nTotal     =   array_unique($nData);

	foreach ($nTotal as $nIn) 
	{
        $query  =   'SELECT calldate, src, did FROM cdr_copy WHERE src LIKE "%'.$nIn.'%" AND lastapp = "Queue" AND did <> "" AND calldate BETWEEN "'.$dateIn.' 00:00:00" AND "'.$dateFn.' 23:59:59" ORDER BY calldate ASC LIMIT 1';
        $nRes   =    $PBXVZ->DBQuery($query);

		$numb   =   (strlen($nRes['src']) > 10) ? (substr($nRes['src'],-10)) : $nRes['src'];

        $query  =   'SELECT client_id FROM cp_leads WHERE phone_main LIKE "%'.$numb.'%" OR phone_alt LIKE "%'.$numb.'%"';
        $lead   =    $Smart->DBQuery($query);

        $smart  =   ($lead) ? "true" : "false";

        if($sel == false)
        {

        	$rows[$contf++]	=	[
        		'calldate'	=>	$nRes['calldate'],
        		'phone'		=>	$numb,
        		'did'		=>	$nRes['did'],
        		'smart'		=>	$smart
        	];
        }


        $rRes = $numb = $lead = $smart = $sel = $cdr = "";
	
	}

}

$columnNames = array();

if(!empty($rows))
{
    $firstRow = $rows[0];
    foreach($firstRow as $colName => $val){
        $columnNames[] = $colName;
    }
}

$fileName = 'Reporting-DID-'.$cou.'-'.date("Y-d-m").'.csv';

header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="' . $fileName . '"');
 
$fp = fopen('php://output', 'w');
 
fputcsv($fp, $columnNames);

foreach ($rows as $row) { fputcsv($fp, $row);   }
 
fclose($fp);

exit;

?>