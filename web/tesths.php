<?php

require '../vendor/autoload.php';

require '../src/Lib/DBSmart.php';

require '../src/Lib/Config.php';

$objPHPExcel    =   new \PHPExcel();
$Smart          =   new \App\Lib\DBsmart();
$_replace       =   new \App\Lib\Config(); 

$dateI  =   ($_REQUEST['datos'] == "") ? date("m/d/Y") : $_REQUEST['datos'];
$date   =   substr($dateI,6,4).'-'.substr($dateI,0,2).'-'.substr($dateI,3,2);

$BStyle =   [
                'borders'   =>  [   'allborders'    =>  ['style' =>  PHPExcel_Style_Border::BORDER_THIN] ],
                'alignment' =>  [   'horizontal'    =>  PHPExcel_Style_Alignment::HORIZONTAL_CENTER ]
            ];
$center =   [
                'alignment' =>  [   'horizontal'    =>  PHPExcel_Style_Alignment::HORIZONTAL_CENTER ]
            ];

$cel =  0;

/**
* CONFIGURACION DE MARGENES Y CELDAS
*/
$objPHPExcel->getActiveSheet()->getPageMargins()
    ->setTop(0.2)
    ->setRight(0.2)
    ->setLeft(0.2)
    ->setBottom(0.2);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(1);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);

$objPHPExcel->getActiveSheet()->getStyle("A1:J3")->getFont()->setSize(8);

/**
 * TITULO DEL ARCHIVO
 */

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTES DE OPERACIONES');
$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($BStyle);

/**
 * SUBTITULOS
 */

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'CLIENTE');
$objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
$objPHPExcel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C3', 'FACTURA');
$objPHPExcel->getActiveSheet()->mergeCells('C3:C3');
$objPHPExcel->getActiveSheet()->getStyle('C3:C3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D3', 'OFICINA');
$objPHPExcel->getActiveSheet()->mergeCells('D3:D3');
$objPHPExcel->getActiveSheet()->getStyle('D3:D3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E3', 'CAJA');
$objPHPExcel->getActiveSheet()->mergeCells('E3:E3');
$objPHPExcel->getActiveSheet()->getStyle('E3:E3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F3', 'FORMA');
$objPHPExcel->getActiveSheet()->mergeCells('F3:F3');
$objPHPExcel->getActiveSheet()->getStyle('F3:F3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G3', 'STATUS');
$objPHPExcel->getActiveSheet()->mergeCells('G3:G3');
$objPHPExcel->getActiveSheet()->getStyle('G3:G3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H3', 'RECIBIDO');
$objPHPExcel->getActiveSheet()->mergeCells('H3:H3');
$objPHPExcel->getActiveSheet()->getStyle('H3:H3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I3', 'CAMBIO');
$objPHPExcel->getActiveSheet()->mergeCells('I3:I3');
$objPHPExcel->getActiveSheet()->getStyle('I3:I3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J3', 'TOTAL');
$objPHPExcel->getActiveSheet()->mergeCells('J3:J3');
$objPHPExcel->getActiveSheet()->getStyle('J3:J3')->applyFromArray($BStyle);

$objPHPExcel->getActiveSheet()->getStyle("A3:J100")->getFont()->setSize(6);
$objPHPExcel->getActiveSheet()->getStyle('A3:J100')->applyFromArray($center);


$cel+=4;
for ($il=1; $il <= 10; $il++)
{
    $a1     =   "";
    $b1     =   "DEVELOPMENT - CLIENTE DE PRUEBAS";
    $c1     =   "92455";
    $d1     =   "BARQUISIMETO";
    $e1     =   "EJECUTIVO";
    $f1     =   "TRANSFERENCIA";
    $g1     =   "SUSPENDIDO";
    $h1     =   "1234";
    $i1     =   $il;
    $j1     =   $il;

    // $a      =   "A".$cel;
    $b      =   "B".$cel;
    $c      =   "C".$cel;
    $d      =   "D".$cel;
    $e      =   "E".$cel;
    $f      =   "F".$cel;
    $g      =   "G".$cel;
    $h      =   "H".$cel;
    $i      =   "I".$cel;
    $j      =   "J".$cel;

    // $objPHPExcel->getActiveSheet()->getFont()->setSize(6);
    $objPHPExcel->setActiveSheetIndex(0)
        // ->setCellValue($a, $a1)
        ->setCellValue($b, $b1)
        ->setCellValue($c, $c1)
        ->setCellValue($d, $d1)
        ->setCellValue($e, $e1)
        ->setCellValue($f, $f1)
        ->setCellValue($g, $g1)
        ->setCellValue($h, $h1)
        ->setCellValue($i, $i1)
        ->setCellValue($j, $j1);
    $cel+=1;
}

$cel+=1;


$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'FORMA');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', 'CANTIDAD');
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', 'TOTAL');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'EFECTIVO');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', '5');
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', '5');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'TRANSFERENCIA');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', '4');
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', '4');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'PAGO MOVIL');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', '3');
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', '3');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'PAYPAL');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', '2');
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', '2');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'ZELLE');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', '1');
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', '1');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'TOTAL GENERAL');
$objPHPExcel->getActiveSheet()->mergeCells('G'.$cel.':H'.$cel.'');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', '15');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'TOTAL EFECTIVO');
$objPHPExcel->getActiveSheet()->mergeCells('G'.$cel.':H'.$cel.'');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', '5');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

    $objPHPExcel->getActiveSheet()->setTitle('Reporte-StreetSmart');

    $objPHPExcel->setActiveSheetIndex(0);

    header('Content-Type: application/vnd.ms-excel');

    header('Content-Disposition: attachment;filename="Reporte-StreetSmart-"'.date("YmdHms").'".xls"');

    header('Cache-Control: max-age=0');

    $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

    $writer->save('php://output');

    exit;