<?php
require '../vendor/autoload.php';
require('fpdf.php');

require '../src/Emailer/class.phpmailer.php';
require '../src/Emailer/class.smtp.php';

require '../src/Lib/DBSmart.php';
require '../src/Lib/Config.php';

$date       =   ['da001' => $_REQUEST['d1'], 'da002'  =>  $_REQUEST['d2'], 'op' =>  $_REQUEST['op'] ];

$Smart      =   new \App\Lib\DBsmart();
$_replace   =   new \App\Lib\Config(); 

$pdf        = new \App\Lib\OperatorStatics();
$pdf->hojaServicio($date)->output('D', 'OperatorStatics-'.date("YmdHms").'.pdf');