<?php
require_once __DIR__.'/../vendor/autoload.php';


$app = require __DIR__.'/../src/app.php';

$upload = __DIR__.'/../upload/';	

if ($app['env']['APP_DEBUG']) {

    ini_set('display_errors', 0);
    Symfony\Component\Debug\Debug::enable();
    require __DIR__.'/../config/dev.php';

} else {

    require __DIR__.'/../config/prod.php'; 

}

define("PROJECTPATH", dirname(__DIR__). '/web/');

define("UPLOADPATH", dirname(__DIR__).'/web/assets/upload/');

require __DIR__.'/../src/controllers.php';
require __DIR__.'/../src/middlewares.php';
require __DIR__.'/../src/routes.php';
$app->run();
