<?php

require '../vendor/autoload.php';

require '../src/Lib/DBSmart.php';

require '../src/Lib/Config.php';

$objPHPExcel 	= 	new \PHPExcel();
$Smart      	=   new \App\Lib\DBsmart();
$_replace   	=   new \App\Lib\Config(); 

$dateI 	=	($_REQUEST['datos'] == "") ? date("m/d/Y") : $_REQUEST['datos'];
$date 	=	substr($dateI,6,4).'-'.substr($dateI,0,2).'-'.substr($dateI,3,2);

$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'Job Name')
	->setCellValue('B1', 'Job ID')
	->setCellValue('C1', 'Job Type')
	->setCellValue('D1', 'Description')
	->setCellValue('E1', 'Always available')
	->setCellValue('F1', 'TimeZone')
	->setCellValue('G1', 'Scheduled Start Date/Time')
	->setCellValue('H1', 'Send Job to Mobile Device')
	->setCellValue('I1', 'End Always Available Jobs')
	->setCellValue('J1', 'Duration')
	->setCellValue('K1', 'Priority')
	->setCellValue('L1', 'Assigned To')
	->setCellValue('M1', 'Location Reference Number')
	->setCellValue('N1', 'Location Name')
	->setCellValue('O1', 'Street Address')
	->setCellValue('P1', 'Apt_Suite')
	->setCellValue('Q1', 'City')
	->setCellValue('R1', 'State')
	->setCellValue('S1', 'Zip')
	->setCellValue('T1', 'Country')
	->setCellValue('U1', 'Create Location?')
	->setCellValue('V1', 'Latitude')
	->setCellValue('W1', 'Longitude')
	->setCellValue('X1', 'Contact name')
	->setCellValue('Y1', 'Contact phone')
	->setCellValue('Z1', 'Cliente ID')
	->setCellValue('AA1', 'Plan Contratado')
	->setCellValue('AB1', 'IP Actual')
	->setCellValue('AC1', 'AP Actual')
	->setCellValue('AD1', 'Signal')
	->setCellValue('AE1', 'Transmision')
	->setCellValue('AF1', 'Speed Test')
	->setCellValue('AG1', 'Motivo');

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(25);
	
	$cel=2;

	$query  =   'SELECT *, (SELECT name FROM data_town WHERE id = town_id) AS town FROM cp_sup_serv_sheet_ve where visit_date = "'.$date.'" AND status_id = "0"';
	$sheet  =   $Smart->DBQueryAll($query);

	foreach ($sheet as $s => $sh) 
	{
		if($sh['coordinations'] == "")
		{
			$data[0] 	=	$data[1] 	=	"";
		}else{
			if(strpos($sh['coordinations'],","))
			{
				$data 	=	explode(",", $sh['coordinations']);

			}else{
				$data[0]	=	$sh['coordinations'];
				$data[1]	=	"";
			}
		}

		if(strlen($sh['phone_main']) > 10)
		{
			$ppp1 	=	"(".substr($sh['phone_main'],0,3).")".substr($sh['phone_main'],3,6). " (" .substr($sh['phone_main'],12,3).")".substr($sh['phone_main'],14);
		}else{
			$ppp1 	=	"(".substr($sh['phone_main'],0,3).")".substr($sh['phone_main'],3);
		}

		$a1		=	$sh['name'] .' - '. $sh['town'] .' - '.$ppp1;
		$b1		=	"";
		$c1		=	"Technical Service";
		$d1		=	"";
		$e1		=	"";
		$f1		=	"";
		$g1		=	$_replace->ShowDateStreet($sh['visit_date']) . ' 12:00 AM';
		$h1		=	"";
		$i1		=	"";
		$j1		=	"1 d";
		$k1		=	"Low";
		$l1		=	"";
		$m1		=	"";
		$n1		=	"";
		$o1		=	$sh['add_main'];
		$p1		=	"";
		$q1		=	$sh['town'];
		$r1		=	"PR";
		$s1		=	"";
		$t1		=	"";
		$u1		=	"";
		$v1		=	$data[0];
		$w1		=	$data[1];
		$x1		=	$sh['name'];
		$y1		=	$ppp1;
		$z1		=	$sh['client_id'];
		$aa1	=	$sh['plan'];
		$ab1	=	$sh['ip_current'];
		$ac1	=	$sh['ap_current'];
		$ad1	=	$sh['signal_r'];
		$ae1	=	$sh['transmitions'];
		$af1	=	"";
		$ag1	=	$sh['reason'];

		$a		=	"A".$cel;
		$b		=	"B".$cel;
		$c		=	"C".$cel;
		$d		=	"D".$cel;
		$e		=	"E".$cel;
		$f		=	"F".$cel;
		$g		=	"G".$cel;
		$h		=	"H".$cel;
		$i		=	"I".$cel;
		$j		=	"J".$cel;
		$k		=	"K".$cel;
		$l		=	"L".$cel;
		$m		=	"M".$cel;
		$n		=	"N".$cel;
		$o		=	"O".$cel;
		$p		=	"P".$cel;
		$q		=	"Q".$cel;
		$r		=	"R".$cel;
		$s		=	"S".$cel;
		$t   	=	"T".$cel;
		$u		=	"U".$cel;
		$v		=	"V".$cel;
		$w		=	"W".$cel;
		$x		=	"X".$cel;
		$y		=	"Y".$cel;
		$z		=	"Z".$cel;
		$aa		=	"AA".$cel;
		$ab		=	"AB".$cel;
		$ac		=	"AC".$cel;
		$ad		=	"AD".$cel;
		$ae		=	"AE".$cel;
		$af		=	"AF".$cel;
		$ag		=	"AG".$cel;

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue($a, $a1)
			->setCellValue($b, $b1)
			->setCellValue($c, $c1)
			->setCellValue($d, $d1)
			->setCellValue($e, $e1)
			->setCellValue($f, $f1)
			->setCellValue($g, $g1)
			->setCellValue($h, $h1)
			->setCellValue($i, $i1)
			->setCellValue($j, $j1)
			->setCellValue($k, $k1)
			->setCellValue($l, $l1)
			->setCellValue($m, $m1)
			->setCellValue($n, $n1)
			->setCellValue($o, $o1)
			->setCellValue($p, $p1)
			->setCellValue($q, $q1)
			->setCellValue($r, $r1)
			->setCellValue($s, $s1)
			->setCellValue($t, $t1)
			->setCellValue($u, $u1)
			->setCellValue($v, $v1)
			->setCellValue($w, $w1)
			->setCellValue($x, $x1)
			->setCellValue($y, $y1)
			->setCellValue($z, $z1)
			->setCellValue($aa, $aa1)
			->setCellValue($ab, $ab1)
			->setCellValue($ac, $ac1)
			->setCellValue($ad, $ad1)
			->setCellValue($ae, $ae1)
			->setCellValue($af, $af1)
			->setCellValue($ag, $ag1);
		$cel+=1;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Reporte-StreetSmart-VE');

	$objPHPExcel->setActiveSheetIndex(0);

	header('Content-Type: application/vnd.ms-excel');

	header('Content-Disposition: attachment;filename="Reporte-StreetSmart-VE-"'.$date.'".xls"');

	header('Cache-Control: max-age=0');

	$writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

	$writer->save('php://output');

	exit;