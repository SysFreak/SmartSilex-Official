<?php

require '../vendor/autoload.php';

require '../src/Lib/DBSmart.php';

require '../src/Lib/Config.php';

require '../src/Models/Transactions.php';

?>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <style type="text/css">
            .pos{
                width: 100%;
                width: 80mm;
                padding: 2px;
                display: block;
                word-wrap: break-word;
                font-family:tahoma;
                font-size:10px;
                font-weight:bold;
            }
            .table td{
                word-wrap: break-word;
                font-family:tahoma;
                font-size:10px;
                font-weight:bold;
            }
            .pos p,
            .pos h4{ 
                margin: 3px 0;
                font-weight:bold; 
            }
            .center{
                text-align: center; 
            }   
            .mayu{
                text-transform: uppercase;
            }
        </style>
        <div class="pos">

            <?php 
                $id     =   $_GET['id'];

                $Smart  =   new \App\Lib\DBsmart();
                
                $info   =   $Smart->DBQuery('SELECT id, (SELECT name FROM cp_mw_clients WHERE mw =  (SELECT mw FROM cp_mw_invoices WHERE f_id = bills)) name, bills, (SELECT username FROM cp_mw_clients WHERE mw =  (SELECT mw FROM cp_mw_invoices WHERE f_id = bills)) ced, (SELECT address FROM cp_mw_clients WHERE mw =  (SELECT mw FROM cp_mw_invoices WHERE f_id = bills)) address, (CASE WHEN pay_form = 1 THEN "Pago Parcial - Efectivo" WHEN pay_form = 2 THEN "Pago Parcial - Transferencia - Bancaria" WHEN pay_form = 3 THEN "Pago Parcial - Transferencia - Pago Movil" WHEN pay_form = 4 THEN "Pago Parcial - Transferencia - PayPal" WHEN pay_form = 5 THEN "Pago Parcial - Transferencia - Zelle" END) form, (CASE WHEN pay_office = 1 THEN "BARQUISIMETO" WHEN pay_office = 2 THEN "SAN FELIPE" WHEN pay_office = 3 THEN "CALL CENTER" END) office, pay_amount, total_fp, total_df, (SELECT username FROM users WHERE id = operator_id) operator, created_at FROM cp_mw_payments WHERE id = "'.$id.'"');


            ?>
            <h4 class="center mayu">BOOM SOLUTIONS C.A</h4>
            <h4 class="center">RIF: J-40537521-3</h4>
            <h4 class="center">Av Lara, C.C Churun Meru,Nivel Sótano Local S-1 Barquisimeto, Lara, 003001</h4>
            <h4 class="center">0251-3353330</h4>
            <p>&nbsp; RECIBO:&nbsp;<?php echo $info['bills']; ?></p>
            <p>---------------------------------------------------------------------</p>
            <p class="center mayu">Descripci&oacute;n</p>
            <p>---------------------------------------------------------------------</p>
            <p>&nbsp; <h3>Sucursal: <?php echo $info['office']; ?></h3></p>
            <p><h3>Concepto: <?php echo $info['form']; ?></h3></p>

            <p><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);">---------------------------------------------------------------------</span></span></span></p>
            <table border="0" cellpadding="2" cellspacing="2" class="table" style="width: 100%;">
                <tbody><tr><td style="text-align: right;">TOTAL FACTURA:</td><td style="text-align: left;"><?php echo $info['pay_amount']; ?></td></tr><tr><td style="text-align: right;">MONTO ABONADO:&nbsp;</td><td style="text-align: left;"><?php echo $info['total_fp']; ?></td></tr><tr><td style="text-align: right;">SALDO PENDIENTE:&nbsp;</td><td style="text-align: left;"><?php echo $info['total_df']; ?></td></tr>
            </tbody>
        </table>
        <p><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);">---------------------------------------------------------------------</span></span></span></p>
        <p class="mayu center" style="padding-top: 6px;margin-bottom: -5px;">cliente</p>
        <p><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);">---------------------------------------------------------------------</span></span></span></p>
        <h4><?php echo $info['name']; ?></h4>
        <h4><?php echo $info['address']; ?></h4>
        <h4><?php echo $info['ced']; ?></h4>
        <p><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);"><span style="color: rgb(51, 51, 51); font-family: tahoma; font-size: 10px; font-weight: 700; background-color: rgb(255, 255, 255);">---------------------------------------------------------------------</span></span></span></p>
        <p>EJECUTIVO: <?php echo $info['operator']; ?></p>
        <p>FECHA: <?php echo date("Y-m-d H:m:s"); ?></p>
        <p style="text-align: center;"><span style="font-size:6px;">RECIBO SIN VALOR FISCAL</span></p>
    </div>
</body>

<script src="/assets/js/libs/jquery-3.5.1.min.js"></script>
<script type="text/javascript">
    
$(function() 
{
   // window.print();
});

</script>

</html>