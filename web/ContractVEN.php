<?php

require '../vendor/autoload.php';
require '../src/Lib/DBSmart.php';
require '../src/Lib/Config.php';
require('fpdf.php');

define('FPDF_FONTPATH', realpath( 'fonts/' ));


class PDF extends FPDF
{
    
    public $debug = 0;
    public $height = 7;

    function __construct($orientation='P', $unit='mm', $size='letter')
    {
        
        parent::__construct($orientation, $unit, $size);
        $this->SetLineWidth(0.4);
        $this->AddFont('Calibri','', 'calibri.php');
        $this->AddFont('Calibri','B','calibrib.php');
        $this->AddFont('Calibri','I','calibrii.php');
    }

    public function ContractRes($iData, $text, $text1, $text2, $text3)
    {
        $this->addPage('P');

        $this->Image('assets/img/logicon.png',85,10,0,20);
        $this->Ln(20);
        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Boom Solutions CA.',$this->debug,1,'C');
        
        $this->SetFont('Calibri','',10);
        $this->SetFontsize(10);
        $this->Cell(0,5,'RIF J-405375213',$this->debug,1,'C');
        $this->Cell(0,5,'Tel: 0251-3353330',$this->debug,1,'C');
        $this->Cell(0,5,'email: servicio.cliente@boomsolutions.com',$this->debug,1,'C');


        $this->SetFont('Calibri','',10);
        $this->Cell(35, $this->height,'Contract Number: ',$this->debug,0,'R');
        $this->Cell(65, $this->height,$iData['contract'],$this->debug,0,'L');
        $this->Cell(70, $this->height,'Account Number: ',$this->debug,0,'R');
        $this->Cell(30, $this->height, $iData['client_id'],$this->debug,0,'L');
        $this->Ln(3);

        $this->Ln(5);
        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'TERMINOS Y CONDICIONES GENERALES DEL CONTRATO DE SERVICIOS DE INTERNET INALAMBRICO PREPAGADO',$this->debug,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',8.5);
        $this->MultiCell(196,3,utf8_decode($text),$this->debug);
        
        $this->Ln(3);
        $this->SetFont('Calibri','',10);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();

        $this->Cell(22, $this->height, 'Nombre: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['name'], $this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Cedula: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(40, $this->height, $iData['id_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Desde: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['pre_date'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Direccion: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(135, $this->height, $iData['add_main'], $this->debug, 0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Hasta: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(10, $this->height, $iData['last_date'], $this->debug, 0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Tel. Principal:', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['phone_main'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Tel. Alterno:',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(36, $this->height, $iData['phone_alt'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(15, $this->height, 'Adicional: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['phone_other'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Email: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['email'] ,$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Residencia: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(36, $this->height, $iData['house'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(15, $this->height, 'Contrato: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['mes'].' Meses',$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Niveles: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['nivel'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Tipo: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(38, $this->height, $iData['techo'],$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(13, $this->height, 'Vendedor: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['sale'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, 'Servicio: ',1,0,'C');
        $this->Cell(50, $this->height, 'Pago Mensual',1,0,'C');
        $this->Cell(50, $this->height, 'Activacion',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '','T',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, $iData['package'],1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ '.$iData['price'],1,0,'C');
        $this->Cell(50, $this->height, '$ '.$iData['instalation'].'',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '','',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, 'Otros',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, 'Dias hasta el 30',1,0,'C');
        $this->Cell(50, $this->height, 'Prorateo',1,0,'C');
        $this->Cell(50, $this->height, 'Pago de Activacion Total:',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, (($iData['date_res'] <= 0) ? 0 : $iData['date_res']),1,0,'C');
        $this->Cell(50, $this->height, '$ '. $iData['prorateo'],1,0,'C');
        $this->Cell(50, $this->height, '$ '.$iData['instalation'],1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->setFillColor(160,160,160);
        $this->Cell(46, $this->height, '$ '.($iData['instalation']+$iData['prorateo']),'T',0,'C');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }

        $this->SetFont('Calibri','', 8);
        $this->Ln(3);

        $this->SetFont('Calibri','', 7.5);
        $this->MultiCell(196,3,utf8_decode($text1),$this->debug);
        $this->Ln($this->height);


        $this->addPage('P');
        $this->MultiCell(196,3,utf8_decode($text2), $this->debug);

        $this->Ln(3);
        $this->SetFont('Calibri','',8);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $this->SetFont('Calibri','B',8);
        $this->Cell(25, $this->height, 'Servicio Internet',1,0,'C');
        $this->Cell(20, $this->height, 'Costos',1,0,'C');
        $this->Cell(55, $this->height, 'Servicios',1,0,'C');
        $this->Cell(20, $this->height, 'Costos',1,0,'C');
        $this->Cell(56, $this->height, 'Servicios y/o Equipos',1,0,'C');
        $this->Cell(20, $this->height, 'Costos',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '1Mb / 1Mb',1,0,'C');
        $this->Cell(20, $this->height, '$ 29.99',1,0,'C');
        $this->MultiCell(55,3.5,'Activacion del Servicio de Internet (Incluye hasta 30 metros de cable)',1,'C');
        $this->SetXY(110, 26);
        $this->Cell(20, $this->height, '$ '.$iData['instalation'].'',1,0,'C');
        $this->MultiCell(56,3.5,'Reubicacion de equipos en el mismo domicilio (Incluye solo 10 mts de cable) ',1,'C');
        $this->SetXY(186, 26);
        $this->Cell(20, $this->height, '$ 25',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '2Mb / 1Mb',1,0,'C');
        $this->Cell(20, $this->height, '$ 39.99',1,0,'C');
        $this->MultiCell(55,7,'Reactivacion por pago extemporaneo',1,'C');
        $this->SetXY(110, 33);
        $this->Cell(20, $this->height, '$ 10',1,0,'C');
        $this->MultiCell(56,7,'Metro adicional de cable',1,'C');
        $this->SetXY(186, 33);
        $this->Cell(20, $this->height, '$ 0.25',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '3Mb / 2Mb',1,0,'C');
        $this->Cell(20, $this->height, '$ 49.99',1,0,'C');
        $this->MultiCell(55,7,'Reubicacion de equipos por mudanza',1,'C');
        $this->SetXY(110, 40);
        $this->Cell(20, $this->height, '$ 49.99',1,0,'C');
        $this->MultiCell(56,7,'Antena',1,'C');
        $this->SetXY(186, 40);
        $this->Cell(20, $this->height, '$ 299.99',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '5Mb / 2Mb',1,0,'C');
        $this->Cell(20, $this->height, '$ 59.99',1,0,'C');
        $this->MultiCell(55,7,'Visita tecnica con costo',1,'C');
        $this->SetXY(110, 47);
        $this->Cell(20, $this->height, '$ 25',1,0,'C');
        $this->MultiCell(56,7,'Router',1,'C');
        $this->SetXY(186, 47);
        $this->Cell(20, $this->height, '$ 49.99',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '10Mb / 2Mb',1,0,'C');
        $this->Cell(20, $this->height, '$ 69.99',1,0,'C');
        $this->MultiCell(55,3.5,'Reconfiguracion de equipos (Router y/o Antena)',1,'C');
        $this->SetXY(110, 54);
        $this->Cell(20, $this->height, '$ 25',1,0,'C');
        $this->MultiCell(56,3.5,'Router adicional para la venta por activacion de servicio',1,'C');
        $this->SetXY(186, 54);
        $this->Cell(20, $this->height, '$ 49.99',1,0,'C');
        $this->Ln($this->height);

        $this->Ln(2);
        $this->SetFont('Calibri','',8);
        $this->MultiCell(196,3,utf8_decode($text3), $this->debug);
            
        $this->Ln($this->height);
        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Coordenada Longitud: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Coordenada Latitud',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Cableado al Entrar: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Cableado al Salir:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'IP: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Asignada Por:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Site: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'ATA:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Signal: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, '',0,0,'C');
        $this->Cell(50, $this->height, '',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Cliente: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Tecnico: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);


        return $this;
    
    }

    public function ContractCom($iData, $text1, $text2, $text3)
    {
        $this->addPage('P');

        $this->Image('assets/img/logicon.png',85,10,0,20);
        $this->Ln(20);
        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Boom Solutions CA.',$this->debug,1,'C');
        
        $this->SetFont('Calibri','',10);
        $this->SetFontsize(10);
        $this->Cell(0,5,'RIF J-405375213',$this->debug,1,'C');
        $this->Cell(0,5,'Tel: 0251-3353330',$this->debug,1,'C');
        $this->Cell(0,5,'email: servicio.cliente@boomsolutions.com',$this->debug,1,'C');


        $this->SetFont('Calibri','',10);
        $this->Cell(35, $this->height,'Contract Number: ',$this->debug,0,'R');
        $this->Cell(65, $this->height,$iData['contract'],$this->debug,0,'L');
        $this->Cell(70, $this->height,'Account Number: ',$this->debug,0,'R');
        $this->Cell(30, $this->height, $iData['client_id'],$this->debug,0,'L');
        $this->Ln(3);

        $this->Ln(5);
        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'TERMINOS Y CONDICIONES GENERALES DEL CONTRATO DE SERVICIOS DE INTERNET INALAMBRICO PREPAGADO',$this->debug,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',8.5);
        $this->MultiCell(196,3,utf8_decode('Las cláusulas y términos del presente documento regulan las condiciones del contrato de servicio de internet inalámbrico prepagado entre BOOM SOLUTIONS, C.A., identificada con el RIF J-405375213 y para los efectos de este contrato se denominará BOOM SOLUTIONS, por una parte y por la otra el solicitante denominado a los efectos de este contrato EL CLIENTE (cuyos datos, especificaciones del servicio contratado, dirección de instalación se encuentran descritas en la siguiente cláusula: PRIMERA: DE LOS DATOS Y ESPECIFICACIONES PARTICULARES DE EL CLIENTE. El contenido de la información arrojada en cada uno de los recuadros de la presente cláusula fue debidamente validados y aceptados por el cliente, y gozan de plena validez no importando si para el llenado de los mismos fue utilizado lapicero o/u bolígrafo.'),$this->debug);
        
        $this->Ln(3);
        $this->SetFont('Calibri','',10);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();

        $this->Cell(22, $this->height, 'Rep. Legal: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['repre_legal'], $this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Cedula: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(40, $this->height, $iData['id_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Desde: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['pre_date'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Razon Social: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['name'], $this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'RIF: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(40, $this->height, $iData['ss_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Hasta: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['last_date'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Direccion: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(190, $this->height, $iData['add_main'], $this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Tel. Principal:', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['phone_main'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Tel. Alterno:',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(36, $this->height, $iData['phone_alt'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(15, $this->height, 'Adicional: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['phone_other'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Email: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['email'] ,$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Residencia: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(36, $this->height, $iData['house'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(15, $this->height, 'Contrato: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['mes'].' Meses',$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Niveles: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['nivel'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Tipo: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(38, $this->height, $iData['techo'],$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(13, $this->height, 'Vendedor: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['sale'],$this->debug,0,'L');
        $this->Ln($this->height);
        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, 'Servicio: ',1,0,'C');
        $this->Cell(50, $this->height, 'Pago Mensual',1,0,'C');
        $this->Cell(50, $this->height, 'Activacion',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '','T',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, $iData['package'],1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ '.$iData['price'],1,0,'C');
        $this->Cell(50, $this->height, '$ '.$iData['instalation'].'',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '','',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, 'Otros',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(50, $this->height, 'Dias hasta el 30',1,0,'C');
        $this->Cell(50, $this->height, 'Prorateo',1,0,'C');
        $this->Cell(50, $this->height, 'Pago de Activacion Total:',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(46, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, (($iData['date_res'] <= 0) ? 0 : $iData['date_res']),1,0,'C');
        $this->Cell(50, $this->height, '$ '. $iData['prorateo'],1,0,'C');
        $this->Cell(50, $this->height, '$ '.$iData['instalation'],1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->setFillColor(160,160,160);
        $this->Cell(46, $this->height, '$ '.($iData['instalation']+$iData['prorateo']),'T',0,'C');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }

        $this->SetFont('Calibri','', 8);
        $this->Ln(3);

        $this->SetFont('Calibri','', 7.5);
        $this->MultiCell(196,3,utf8_decode($text1),$this->debug);
        $this->Ln($this->height);


        $this->addPage('P');
        $this->MultiCell(196,3,utf8_decode($text2), $this->debug);

        $this->Ln(3);
        $this->SetFont('Calibri','',8);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $this->SetFont('Calibri','B',8);
        $this->Cell(25, $this->height, 'Servicio Internet',1,0,'C');
        $this->Cell(20, $this->height, 'Costos',1,0,'C');
        $this->Cell(55, $this->height, 'Servicios',1,0,'C');
        $this->Cell(20, $this->height, 'Costos',1,0,'C');
        $this->Cell(56, $this->height, 'Servicios y/o Equipos',1,0,'C');
        $this->Cell(20, $this->height, 'Costos',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '1MB / 1MB',1,0,'C');
        $this->Cell(20, $this->height, '$ 39.99',1,0,'C');
        $this->MultiCell(55,3.5,'Activacion del Servicio de Internet (Incluye hasta 30 metros de cable)',1,'C');
        $this->SetXY(110, 26);
        $this->Cell(20, $this->height, '$ '.$iData['instalation'].'',1,0,'C');
        $this->MultiCell(56,3.5,'Reubicacion de equipos en el mismo domicilio (Incluye solo 10 mts de cable) ',1,'C');
        $this->SetXY(186, 26);
        $this->Cell(20, $this->height, '$ 25',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '2MB / 2MB',1,0,'C');
        $this->Cell(20, $this->height, '$ 59.99',1,0,'C');
        $this->MultiCell(55,7,'Reactivacion por pago extemporaneo',1,'C');
        $this->SetXY(110, 33);
        $this->Cell(20, $this->height, '$ 20',1,0,'C');
        $this->MultiCell(56,7,'Metro adicional de cable',1,'C');
        $this->SetXY(186, 33);
        $this->Cell(20, $this->height, '$ 0.25',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '3MB / 3MB',1,0,'C');
        $this->Cell(20, $this->height, '$ 84.99',1,0,'C');
        $this->MultiCell(55,7,'Reubicacion de equipos por mudanza',1,'C');
        $this->SetXY(110, 40);
        $this->Cell(20, $this->height, '$ 49.99',1,0,'C');
        $this->MultiCell(56,7,'Antena',1,'C');
        $this->SetXY(186, 40);
        $this->Cell(20, $this->height, '$ 299.99',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '5MB / MB',1,0,'C');
        $this->Cell(20, $this->height, '$ 109.99',1,0,'C');
        $this->MultiCell(55,7,'Visita tecnica con costo',1,'C');
        $this->SetXY(110, 47);
        $this->Cell(20, $this->height, '$ 25',1,0,'C');
        $this->MultiCell(56,7,'Router',1,'C');
        $this->SetXY(186, 47);
        $this->Cell(20, $this->height, '$ 49.99',1,0,'C');
        $this->Ln($this->height);

        $this->Cell(25, $this->height, '10MB / 10MB',1,0,'C');
        $this->Cell(20, $this->height, '$ 159.99',1,0,'C');
        $this->MultiCell(55,3.5,'Reconfiguracion de equipos (Router y/o Antena)',1,'C');
        $this->SetXY(110, 54);
        $this->Cell(20, $this->height, '$ 25',1,0,'C');
        $this->MultiCell(56,3.5,'Router adicional para la venta por activacion de servicio',1,'C');
        $this->SetXY(186, 54);
        $this->Cell(20, $this->height, '$ 49.99',1,0,'C');
        $this->Ln($this->height);

        $this->Ln(2);
        $this->SetFont('Calibri','',8);
        $this->MultiCell(196,3,utf8_decode($text3), $this->debug);
            
        $this->Ln($this->height);
        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Coordenada Longitud: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Coordenada Latitud',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Cableado al Entrar: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Cableado al Salir:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'IP: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Asignada Por:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Site: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'ATA:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Signal: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, '',0,0,'C');
        $this->Cell(50, $this->height, '',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Cliente: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Tecnico: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);


        return $this;

    }
    public function encabezado_tabla($value='')
    {
        // fuente 14 y negrita
        $this->SetFont('','B',14);
        // texto color blanco
        $this->SetTextColor(255);
    
    }

    public function cuerpo_tabla($value='')
    {
        $this->setFillColor(160,160,160);
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    
    }
    
    public function cuerpo_tabla2($value='')
    {
        $this->setFillColor(100,100,100);
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    
    }

}


$Smart      =   new \App\Lib\DBsmart();
$_replace   =   new \App\Lib\Config(); 
$pdf        =   new PDF();

$query  =   'SELECT id, ticket, client_id, name, add_main, add_postal, phone_main, phone_alt, phone_other, email, nivel, techo, house, coor_lati, coor_long, package, type, pre_date, last_date, date_res, price, prorateo, mes, instalation, cc_last, cc_exp, ab_last, ab_exp, ab_bank, id_last, id_exp, ss_last, ss_exp, contract, country, repre_legal, created_by, created_at, sale FROM cp_contracts WHERE contract = "'.$_REQUEST['contract'].'"';

$sheet  =   $Smart->DBQuery($query);

if($sheet <> false)
{
    if($sheet['type'] == "RESIDENCIAL")
    {
        if($sheet['mes'] == "24"){ $year = '2'; }elseif($sheet['mes'] == "36"){ $year = '3'; }else{ $year = '2'; }

        $text   =   'Las cláusulas y términos del presente documento regulan las condiciones del contrato de servicio de internet inalámbrico prepagado entre BOOM SOLUTIONS, C.A., identificada con el RIF J-405375213 y para los efectos de este contrato se denominará BOOM SOLUTIONS, por una parte y por la otra el solicitante denominado a los efectos de este contrato EL CLIENTE (cuyos datos, especificaciones del servicio contratado, dirección de instalación se encuentran descritas en la siguiente cláusula: PRIMERA: DE LOS DATOS Y ESPECIFICACIONES PARTICULARES DE EL CLIENTE. El contenido de la información arrojada en cada uno de los recuadros de la presente cláusula fue debidamente validados y aceptados por el cliente, y gozan de plena validez no importando si para el llenado de los mismos fue utilizado lapicero o/u bolígrafo.';

        $text1  =   'SEGUNDA: BOOM SOLUTIONS conviene en brindarle a EL CLIENTE el servicio de suministro de internet prepagado vía inalámbrica con un ancho de banda ilimitado con la velocidad indicada en la cláusula anterior; servicio que se prestará durante todo el día todos los días que comprende la duración de este contrato; la asistencia técnica vía telefónica o si es necesario mediante la visita de alguno de los miembros del personal técnico de BOOM SOLUTIONS se efectuará en el horario comprendido de 8:00 AM hasta las 5:00 pm. TERCERA: TIEMPO DE DURACIÓN DEL CONTRATO DE SERVICIO DE INTERNET INALÁMBRICO. Las partes de común acuerdo establecen que el presente contrato de servicios tendrá una duración de '.$sheet['mes'].' meses contados desde el día en que se efectuó la instalación de los equipos para la activación del servicio de internet lo cual es el día indicado en la cláusula primera del presente contrato. CUARTA: GARANTÍA DE LA CONTINUIDAD DEL SERVICIO A PESAR DE LAS FALLAS ELÉCTRICAS. BOOM SOLUTIONS garantizará a EL CLIENTE la continuidad del servicio de internet inalámbrico aun cuando se encuentra suspendido el servicio eléctrico, si y solo si el cliente posee algún generador eléctrico que le permita mantener activos sus equipos en casa. QUINTA: SERVICIO INTUITO PERSONAE. Las partes declaran conocer que el presente contrato es celebrado intuito personae en el sentido de que EL CLIENTE no podrá subarrendar, alquilar, vender permutar, prestar, ni compartir la señal del servicio de internet inalámbrico suministrado por BOOM SOLUTIONS aquí contratado a terceras personas, en el entendido que este sistema es perfectamente auditable y se puede determinar si se está compartiendo o no la señal de internet inalámbrico, por lo cual la empresa BOOM SOLUTIONS se reserva el derecho de rescindir de manera unilateral el presente contrato en caso de que se violará la presente cláusula. SEXTA: COSTO MENSUAL DEL SERVICIO. EL CLIENTE se compromete a pagar de forma prepagada a BOOM SOLUTIONS por el servicio de internet fibra  inalámbrico descrito en la cláusula PRIMERA del contrato el cual (no incluye IVA), y el mismo debe ser pagado dentro de los cinco (05) primeros días del mes mediante transferencia vía ZELLE cuyo titular es BOOM SOLUTIONS y el correo electrónico asociado a la cuenta es pagos.net@boomsolutions.com; debiendo EL CLIENTE enviar su soporte de pago a el Portal Web http://facturacion.boomsolutionsve.com. Una vez sea verificada como efectiva y disponible en la cuenta de BOOM SOLUTIONS, se pueda adjudicar el pago a favor de EL CLIENTE; teniendo como segunda opción pago en nuestra oficina ubicada en Av. Lara , carrera 1 con calle 8 Nueva segovia,  C.C Churun meru - Planta alta, Local B-08, además de tener la opción de pagos por transferencia vía Paypal cuyo titular es BOOM SOLUTIONS y el correo electrónico asociado a la cuenta es pagos.net@boomsolutions.com asumiendo el cliente el debido porcentaje indicado por dicha plataforma; en tal sentido EL CLIENTE deberá ser lo suficientemente previsivo para efectuar los pagos oportunamente con la finalidad que los mismos estén disponibles en las cuentas de BOOM SOLUTIONS dentro del lapso de los cinco (05) primeros días del mes. En caso de poseer alguna duda con su facturación escribir a la dirección electrónica cobro.net@boomsolutions.com o comunicarse al (0251) 33503340 PARÁGRAFO PRIMERO: BOOM SOLUTIONS se compromete durante la vigencia del presente contrato a mantener las tarifas fijas del servicio de internet fibra inalámbrico contratado. SÉPTIMA: DEL PAGO PRORRATEADO DEL PRIMER MES DEL SERVICIO. A los fines de que los cortes de cuenta de la facturación del servicio se puedan hacer por mes calendario, la partes acuerdan que, desde el momento de la ACTIVACIÓN DEL SERVICIO DE INTERNET, hasta el fin del mes serán calculados de manera prorrateada, tomando en consideración la cantidad de días que EL CLIENTE disfruto efectivamente el servicio; continuando normalmente la facturación prepagada de los meses subsiguientes. OCTAVA: DEL RECIBO DE COBRO. Las partes acuerdan que BOOM SOLUTIONS emitirá el recibo de cobro del servicio de internet inalámbrico prepagado el día primero (1ro) de cada mes, por lo cual dicho recibo de cobro será enviado vía correo electrónico a la dirección electrónica que suministro EL CLIENTE la cual esta descrita en la cláusula PRIMERA del presente contrato. NOVENA: RECLAMO DE COBROS DE RECIBOS. Cada recibo mensual se considerará aceptado por EL CLIENTE si éste no la objeta en la Oficina de Atención al Cliente de BOOM SOLUTIONS o a través de los mecanismos que a tales efectos implemente BOOM SOLUTIONS, dentro de los cinco (05) primeros días continuos del mes, conforme a lo previsto en legislación venezolana vigente. Solo se dará curso a la reclamación extemporánea previo pago de la factura.';

        $text2 = 'DÉCIMA: DE LOS COSTOS DE LOS SERVICIOS, PAQUETES, EQUIPOS. EL CLIENTE declara conocer y aceptar mediante la firma del presente contrato los siguientes costos de los distintos servicios, paquetes y equipos descritos en el cuadro anexo a los cuales pudieran aplicar el presente contrato.';

        $text3 = 'DÉCIMA PRIMERA: Los precios establecidos en el presente contrato no incluyen ningún impuesto o tasa con la cual se grave EL SERVICIO, por lo cual dicho monto será agregado a las cantidades que deba pagar EL CLIENTE A BOOM SOLUTIONS por los servicios en recibo que entregará BOOM SOLUTIONS a EL CLIENTE se indicará en forma separada el monto correspondiente a el servicio y la cantidad que corresponde al impuesto o tasa de que se trate, aplicable a el servicio. DÉCIMA SEGUNDA: DE LA PROPIEDAD DE LOS EQUIPOS INSTALADOS SU CONCESIÓN EN COMODATO. EL CLIENTE manifiesta que los equipos descritos en la cláusula PRIMERA del presente contrato los recibe en calidad de comodato lo que se constituye como un préstamo de uso de los mismos con la finalidad de que EL CLIENTE pueda disfrutar del servicio de internet inalámbrico contratado; en tal sentido EL CLIENTE será responsable de los mismos tal y como lo establece el artículo 1193 del código civil venezolano así como también deberán ser devueltos en las mismas buenas condiciones que fueron recibidos a la empresa BOOM SOLUTIONS, una vez se rescinda el presente contrato o culmine por cualquiera de las formas o motivos posibles, EL CLIENTE se compromete a devolver los equipos en un lapso no mayor a diez (10) días hábiles, contados desde la notificación vía correo electrónico de la culminación del contrato de servicios, en las instalaciones de la empresa BOOM SOLUTIONS descrita en la cláusula VIGÉSIMA SEXTA del presente contrato; asi como también permitirá a que el personal técnico proceda a retirar los equipos instalados en el lugar donde se prestaba el servicio. PARÁGRAFO ÚNICO En caso de que transcurra el lapso de diez (10) días sin que EL CLIENTE haga la devolución de los equipos descritos en la cláusula PRIMERA o que el habiéndose trasladado el técnico de la empresa al lugar donde se prestaba el servicio al mismo se le negare el acceso, o se le negare la entrega de los equipos, o habiéndose entregado los equipos se encuentre en u notable grado de deterioro o dañados, EL CLIENTE deberá cancelar el costo de los equipos los cuales se encuentras descritos en la cláusula DÉCIMA del presente contrato DÉCIMA TERCERA: DE LA PÉRDIDA, DETERIORO DE LOS EQUIPOS DADOS EN COMODATO. EL CLIENTE es el único responsable de los equipos instalados en su domicilio y dados en comodato para la efectiva prestación del servicio de internet inalámbrico, en tal sentido, en caso de pérdida, robo, hurto, deterioro total o parcial o cualquier cosa que le ocurra a los equipos descritos en la cláusula DÉCIMA PRIMERA deberá pagar a BOOM SOLUTIONS el costo de los mismos equipos los cuales se encuentran plenamente descritos en la cláusula DÉCIMA. DÉCIMA CUARTA: PROHIBICIÓN DE ALTERACIÓN DE EQUIPOS. Es contra la ley alterar cualquier equipo perteneciente a BOOM SOLUTIONS, con el fin de recibir, interceptar o ayudar a recibir o interceptar cualquier servicio de comunicación proporcionado a través del sistema propiedad de BOOM SOLUTIONS o de un tercero. La violación de este artículo supondrá que se rescinde el presente contrato con los daños y perjuicios a que haya lugar, así como el ejercicio de las vías legales pertinentes, en defensa de los derechos e intereses de BOOM SOLUTIONS. DÉCIMA QUINTA: DE LA SUSPENSIÓN DEL SERVICIO POR FALTA DE PAGO. Si llegado el sexto (6to) día del mes en curso y EL CLIENTE no canceló de manera oportuna y como lo establece la cláusula SEXTA, el servicio de internet inalámbrico contratado, se suspenderá el servicio a las 12:01 am del Sexto (6to) día del mes en curso, y no se restituirá el servicio hasta tanto EL CLIENTE no pague el recibo del mes en curso y el costo por REACTIVACIÓN POR PAGO EXTEMPORANEO establecido en la cláusula DÉCIMA del presente contrato. PARÁGRAFO PRIMERO: el pago extemporáneo del recibo del mes en curso que tuviera como consecuencia la suspensión temporal del servicio de internet inalámbrico, no le dará derecho a EL CLIENTE a solicitar el pago prorrateado por los días del mes que solamente pudo disfrutar del servicio de internet inalámbrico. Por lo cual el recibo del mes en curso sigue transcurriendo íntegramente y con la obligatoriedad de EL CLIENTE de cancelarlo de manera completa. PARÁGRAFO SEGUNDO: si llegado el día ultimo del mes y EL CLIENTE no cancelo el mes en curso del servicio de internet inalámbrico, BOOM SOLUTIONS procederá a retirar del domicilio del cliente los equipos entregados en comodato descritos en la cláusula PRIMERA PARÁGRAFO TERCERO: si posterior al mes vencido sin que EL CLIENTE haya efectuado el pago del servicio, y el mismo desea continuar disfrutando del servicio de internet inalámbrico, EL CLIENTE deberá pagar nuevamente el costo de la ACTIVACIÓN DEL SERVICIO DE INTERNET descrito en la cláusula DÉCIMA del presente contrato; no importando si BOOM SOLUTIONS pudo retirar o no los equipos dados en comodato antes de que EL CLIENTE decidiera reactivar el servicio. DÉCIMA SEXTA: DEL LAPSO DE RECONEXIÓN DEL SERVICIO POR PAGO EXTEMPORÁNEO. Una vez que BOOM SOLUTIONS, logre corroborar un pago de EL CLIENTE el cual se hizo efectivo posterior al día CINCO (05) del mes en curso y antes del último día del mismo mes en curso, el servicio de internet inalámbrico será restituido por BOOM SOLUTIONS, en un lapso de tiempo no mayor de 24 horas contadas desde el momento en que se hizo efectiva la transferencia del cliente siempre y cuando haya notificado vía correo electrónico. DÉCIMA SÉPTIMA: GARANTIA POR ACTIVACIÓN DEL SERVICIO. Una vez efectuada la activación del servicio de internet, BOOM SOLUTIONS, otorgará una garantía de treinta (30) días contínuos a EL CLIENTE con la finalidad que el mismo pueda disfrutar plenamente del servicio contratado, en tal sentido durante ese lapso si existiere algún problema con el servicio de internet que no se pudiere resolver técnicamente por vía remota, tendrá derecho a una visita sin costo del personal técnico de BOOM SOLUTIONS, siempre y cuando el problema o falla no sea imputable a EL CLIENTE, porque de lo contrario deberá cancelar el servicio de visita técnica con costo cuyas tarifas están especificadas en clausula DÉCIMA del presente contrato. PARÁGRAFO ÚNICO: como parte de la garantía establecida en esta cláusula si la falla del servicio de internet inalámbrico contratado es imputable a BOOM SOLUTIONS, la empresa no le cobrará a EL CLIENTE los días en que no se prestó efectivamente el servicio, generándose una nota de crédito a su favor que será restada del recibo de pago del mes inmediatamente siguiente .DÉCIMA OCTAVA: DE LA NOTAS DE CRÉDITO A FAVOR DEL CLIENTE. Cuando exista una falla en la prestación del servicio que sea imputable a BOOM SOLUTIONS, se generará a favor de EL CLIENTE una nota de crédito por el tiempo que estuvo sin servicio de internet inalámbrico, si este alcanza las 24 horas continuas, calculado de manera prorrateada y será restada de recibo de pago del mes inmediatamente siguiente. DÉCIMA NOVENA: DISPONIBILIDAD DEL SERVICIO. En el caso que la prestación del servicio por parte de BOOM SOLUTIONS resulte afectada o sufra interferencia por causas ajenas a su control, incluyendo, sin que constituya limitación, casos fortuitos, incendio, explosión, vandalismo, o producto de la modificación o implementación de cualquier ley, norma, reglamento o por actos de gobierno de la República Bolivariana de Venezuela, gobiernos locales o autoridades militares, emergencias, motines, guerras o huelgas, BOOM SOLUTIONS estará exonerada de prestar el servicio por todo el tiempo que dure el hecho que afecte o interfiera con la prestación del servicio. A tales efectos, BOOM SOLUTIONS deberá notificar al ente competente sobre la interrupción de EL SERVICIO, en cuyo caso la facturación será suspendida hasta que se reanude la prestación el servicio de y al final del mes se presentará una factura prorrateada. BOOM SOLUTIONS podrá suspender el servicio, parcial o totalmente, cuando sea necesario a los efectos de instalar, reparar o cambiar equipos, o de realizar actividades similares requeridas para el correcto o mejor funcionamiento del servicio, previa notificación a EL CLIENTE vía correo electrónico oficial. VIGÉSIMA: DEL MODO PARA EL REPORTE DE FALLAS EN EL SERVICIO. EL CLIENTE deberá reportar cualquier falla o avería que presente en el servicio bien sea vía correo electrónico usando para tales fines las direcciones electrónicas oficiales de las partes contratante descritas en la cláusula VIGÉSIMA SEXTA del presente contrato; o mediante el número telefónico 0251- 3350911, en lo cual un operador va a canalizar vía telefónica el tipo de avería que tiene el servicio, emitiendo BOOM SOLUTIONS un comprobante de la solicitud de EL CLIENTE el cual será enviado vía correo electrónico a las dirección oficial de EL CLIENTE a los fines de que el mismo posea su comprobante del reporte. Cabe destacar que todas las llamadas telefónicas son grabadas y auditables a los fines de garantizar un mejor servicio. El operador procurará dar solución a EL CLIENTE, vía remota del problema que pueda presentar sin ningún costo; sin embargo en caso de que el operado no pueda solventar de manera inmediata la falla del sistema, procederá a canalizar la solución del problema mediante los distintos medios bien sea a través de una visita técnica con costo o sin costo dependiendo del tipo de falla y que o quien lo originó; en caso que la falla seo por causas imputables a EL CLIENTE el mismo deberá cancelar el servicio que requiera sin menoscabo de la obligación de cancelar si fuere necesario el costo de los equipos en caso de que el daño sea inminente irremediable. VIGÉSIMA PRIMERA: DEL COSTO DE LA ACTIVACION DEL SERVICIO DE INTERNET. EL CLIENTE se obliga a efectuar un pago a BOOM SOLUTIONS por concepto de ACTIVACIÓN DEL SERVICIO DE INTERNET, cuyas tarifas se encuentran descritas en la cláusula DECIMA del presente contrato; dicho pago no está sujeto a devolución alguna, ni mucho menos comprende el pago de los equipos instalados; este pago se podrá efectuar en dólares en efectivo, o trasferencias bancarias en la cuentas descritas en la clausula SEXTA, pagaderas al momento de realizar la instalación en el domicilio de EL CLIENTE. VIGESIMA SEGUNDA: DE LAS VISITAS TECNICA CON COSTO. Se entienden por visitas técnicas con costo, aquella solicitud que efectúa EL CLIENTE en aras de solventar algún inconveniente presentado con el servicio de internet inalámbrico, que no pueda ser resuelto por la empresa BOOM SOLUTIONS de manera remota y que sea imputable a EL CLIENTE, por lo cual dicho servicio genera un pago por parte de EL CLIENTE, cuyas tarifas están descritas en la cláusula DECIMA del presente contrato: 1) Reconfiguración de equipos (router y alineación de antena. 2) Reubicación de equipos en el mismo domicilio. 3) Reubicación de equipos por mudanza. 4) Sustitución de cables y conectores. VIGESIMA TERCERA: DEFINICION DE AVERIAS COMUNES. Se entienden por averías comunes las posibles fallas que se pueden generar en el servicio de internet inalámbrico por el uso normal y cotidiano de los equipos pero que sin embargo sufren algún tipo de deterioro por el transcurso del tiempo y que no son imputables a EL CLIENTE, por lo que no generan costos para su solución; Por lo cual las averías más comunes son: 1) Por necesidad de Sustitución de equipos por daño no imputable a EL CLIENTE 2) Reseteo del Router a valores de fábrica LO ELIMINARIA 3) Desalineación de la antena receptora.4) Deterioro de cables.5) Deterioro de conectores. PARÁGRAFO ÚNICO: en caso de alguna avería común que no sea directamente imputable a EL CLIENTE, la empresa BOOM SOLUTIONS se obliga a efectuar la reparación de la misma e inclusive la sustitución de los equipos en un lapso no mayor de 72 horas posteriores a la visita del técnico. VIGESIMA CUARTA: LEGALIDAD Y VALIDEZ. En caso de que se entienda que alguna de las disposiciones de este Contrato es de tal amplitud que no puede ser exigible en toda su extensión, la misma será válida hasta el punto máximo en que sea legalmente procedente. De igual manera si cualquier disposición del presente contrato se considerara prohibida o inejecutable por ser contraria a la ley, la normativa aplicable o al orden público, o es calificada de tal por los Tribunales de la República o por cualquier organismo de la administración pública, BOOM SOLUTIONS y EL CLIENTE acuerdan que el resto del contrato mantendrá plenamente su vigencia. VIGESIMA QUINTA: ÚNICO CONTRATO. EL CLIENTE queda sometido a las cláusulas del presente contrato. En caso de que BOOM SOLUTIONS realice modificaciones o incorpore anexos en el futuro, Dichas modificaciones o anexos serán notificados por BOOM SOLUTIONS a EL CLIENTE, a través de los medios que BOOM SOLUTIONS considere adecuados, a fin de que EL CLIENTE manifieste su voluntad de acogerse a las mismas. VIGESIMA SEXTA: NOTIFICACIONES. Cualquier notificación u otra forma de comunicación que sea necesaria dar o pueda darse de acuerdo con este contrato, la partes establecen Por parte de BOOM SOLUTIONS: DIRECCION FISICA: Carrera 24 entre calles 10 y 11 local número 10-88, de la ciudad de Barquisimeto municipio Iribarren del estado Lara. TELÉFONO: 0251-3353330 e mail: servicio.cliente@boomsolutions.com. Por parte de EL CLIENTE: las descritas en la cláusula primera del presente contrato. VIGESIMA SEPTIMA: LEGISLACIÓN APLICABLE. Este contrato se regirá por la legislación aplicable de la República Bolivariana de Venezuela. VIGESIMA OCTAVA: VALIDEZ DEL CONTRATO. Este contrato se entenderá suscrito por las partes con la sola firma de EL CLIENTE Inclusive. DISPOSICIÓN FINAL: Lo no previsto en este instrumento será regido por las normas contempladas la Legislación aplicable de la República Bolivariana de Venezuela, la equidad y la buena fe. Las partes eligen como domicilio especial único y excluyente de cualquier otro a la a ciudad de Barquisimeto, Estado Lara, a la Jurisdicción de cuyos Tribunales acuerdan someterse. Se hacen dos (02) ejemplares de un mismo tenor y a un solo efecto.';
        
        $pdf->ContractRes($sheet, $text, $text1, $text2, $text3)->output('I', 'Contracto-'.$sheet['contract'].'.pdf');

    }
    elseif($sheet['type'] == "COMERCIAL")
    {
        if($sheet['mes'] == "24"){ $year = '2'; }elseif($sheet['mes'] == "36"){ $year = '3'; }else{ $year = '2'; }


        $text1  =   'SEGUNDA: BOOM SOLUTIONS conviene en brindarle a EL CLIENTE el servicio de suministro de internet prepagado vía inalámbrica con un ancho de banda ilimitado con la velocidad indicada en la cláusula anterior; servicio que se prestará durante todo el día todos los días que comprende la duración de este contrato; la asistencia técnica vía telefónica o si es necesario mediante la visita de alguno de los miembros del personal técnico de BOOM SOLUTIONS se efectuará en el horario comprendido de 8:00 AM hasta las 5:00 pm. TERCERA: TIEMPO DE DURACIÓN DEL CONTRATO DE SERVICIO DE INTERNET INALÁMBRICO. Las partes de común acuerdo establecen que el presente contrato de servicios tendrá una duración de '.$sheet['mes'].' meses contados desde el día en que se efectuó la instalación de los equipos para la activación del servicio de internet lo cual es el día indicado en la cláusula primera del presente contrato. CUARTA: GARANTÍA DE LA CONTINUIDAD DEL SERVICIO A PESAR DE LAS FALLAS ELÉCTRICAS. BOOM SOLUTIONS garantizará a EL CLIENTE la continuidad del servicio de internet inalámbrico aun cuando se encuentra suspendido el servicio eléctrico, si y solo si el cliente posee algún generador eléctrico que le permita mantener activos sus equipos en casa. QUINTA: SERVICIO INTUITO PERSONAE. Las partes declaran conocer que el presente contrato es celebrado intuito personae en el sentido de que EL CLIENTE no podrá subarrendar, alquilar, vender permutar, prestar, ni compartir la señal del servicio de internet inalámbrico suministrado por BOOM SOLUTIONS aquí contratado a terceras personas, en el entendido que este sistema es perfectamente auditable y se puede determinar si se está compartiendo o no la señal de internet inalámbrico, por lo cual la empresa BOOM SOLUTIONS se reserva el derecho de rescindir de manera unilateral el presente contrato en caso de que se violará la presente cláusula. SEXTA: COSTO MENSUAL DEL SERVICIO. EL CLIENTE se compromete a pagar de forma prepagada a BOOM SOLUTIONS por el servicio de internet fibra  inalámbrico descrito en la cláusula PRIMERA del contrato el cual (no incluye IVA), y el mismo debe ser pagado dentro de los cinco (05) primeros días del mes mediante transferencia vía ZELLE cuyo titular es BOOM SOLUTIONS y el correo electrónico asociado a la cuenta es pagos.net@boomsolutions.com; debiendo EL CLIENTE enviar su soporte de pago a el Portal Web http://facturacion.boomsolutionsve.com. Una vez sea verificada como efectiva y disponible en la cuenta de BOOM SOLUTIONS, se pueda adjudicar el pago a favor de EL CLIENTE; teniendo como segunda opción pago en nuestra oficina ubicada en Av. Lara , carrera 1 con calle 8 Nueva segovia,  C.C Churun meru - Planta alta, Local B-08, además de tener la opción de pagos por transferencia vía Paypal cuyo titular es BOOM SOLUTIONS y el correo electrónico asociado a la cuenta es pagos.net@boomsolutions.com asumiendo el cliente el debido porcentaje indicado por dicha plataforma; en tal sentido EL CLIENTE deberá ser lo suficientemente previsivo para efectuar los pagos oportunamente con la finalidad que los mismos estén disponibles en las cuentas de BOOM SOLUTIONS dentro del lapso de los cinco (05) primeros días del mes. En caso de poseer alguna duda con su facturación escribir a la dirección electrónica cobro.net@boomsolutions.com o comunicarse al (0251) 33503340 PARÁGRAFO PRIMERO: BOOM SOLUTIONS se compromete durante la vigencia del presente contrato a mantener las tarifas fijas del servicio de internet fibra inalámbrico contratado. SÉPTIMA: DEL PAGO PRORRATEADO DEL PRIMER MES DEL SERVICIO. A los fines de que los cortes de cuenta de la facturación del servicio se puedan hacer por mes calendario, la partes acuerdan que, desde el momento de la ACTIVACIÓN DEL SERVICIO DE INTERNET, hasta el fin del mes serán calculados de manera prorrateada, tomando en consideración la cantidad de días que EL CLIENTE disfruto efectivamente el servicio; continuando normalmente la facturación prepagada de los meses subsiguientes. OCTAVA: DEL RECIBO DE COBRO. Las partes acuerdan que BOOM SOLUTIONS emitirá el recibo de cobro del servicio de internet inalámbrico prepagado el día primero (1ro) de cada mes, por lo cual dicho recibo de cobro será enviado vía correo electrónico a la dirección electrónica que suministro EL CLIENTE la cual esta descrita en la cláusula PRIMERA del presente contrato. NOVENA: RECLAMO DE COBROS DE RECIBOS. Cada recibo mensual se considerará aceptado por EL CLIENTE si éste no la objeta en la Oficina de Atención al Cliente de BOOM SOLUTIONS o a través de los mecanismos que a tales efectos implemente BOOM SOLUTIONS, dentro de los cinco (05) primeros días continuos del mes, conforme a lo previsto en legislación venezolana vigente. Solo se dará curso a la reclamación extemporánea previo pago de la factura.';

        $text2  =   'DÉCIMA: DE LOS COSTOS DE LOS SERVICIOS, PAQUETES, EQUIPOS. EL CLIENTE declara conocer y aceptar mediante la firma del presente contrato los siguientes costos de los distintos servicios, paquetes y equipos descritos en el cuadro anexo a los cuales pudieran aplicar el presente contrato.';

        $text3  =   'DÉCIMA PRIMERA: Los precios establecidos en el presente contrato no incluyen ningún impuesto o tasa con la cual se grave EL SERVICIO, por lo cual dicho monto será agregado a las cantidades que deba pagar EL CLIENTE A BOOM SOLUTIONS por los servicios en recibo que entregará BOOM SOLUTIONS a EL CLIENTE se indicará en forma separada el monto correspondiente a el servicio y la cantidad que corresponde al impuesto o tasa de que se trate, aplicable a el servicio. DÉCIMA SEGUNDA: DE LA PROPIEDAD DE LOS EQUIPOS INSTALADOS SU CONCESIÓN EN COMODATO. EL CLIENTE manifiesta que los equipos descritos en la cláusula PRIMERA del presente contrato los recibe en calidad de comodato lo que se constituye como un préstamo de uso de los mismos con la finalidad de que EL CLIENTE pueda disfrutar del servicio de internet inalámbrico contratado; en tal sentido EL CLIENTE será responsable de los mismos tal y como lo establece el artículo 1193 del código civil venezolano así como también deberán ser devueltos en las mismas buenas condiciones que fueron recibidos a la empresa BOOM SOLUTIONS, una vez se rescinda el presente contrato o culmine por cualquiera de las formas o motivos posibles, EL CLIENTE se compromete a devolver los equipos en un lapso no mayor a diez(10) días hábiles, contados desde la notificación vía correo electrónico de la culminación del contrato de servicios, en las instalaciones de la empresa BOOMSOLUTIONS descrita en la cláusula VIGÉSIMA SEXTA del presente contrato; así como también permitirá a que el personal técnico proceda a retirar los equipos instalados en el lugar donde se prestaba el servicio. PARÁGRAFO ÚNICO En caso de que transcurra el lapso de diez (10) días sin que EL CLIENTE haga la devolución de los equipos descritos en la cláusula PRIMERA o que el habiéndose trasladado el técnico de la empresa al lugar donde se prestaba el servicio al mismo se le negare el acceso, o se le negare la entrega de los equipos, o habiéndose entregado los equipos se encuentre en u notable grado de deterioro o dañados, EL CLIENTE deberá cancelar el costo de los equipos los cuales se encuentras descritos en la cláusula DÉCIMA del presente contrato DÉCIMA TERCERA: DE LA PÉRDIDA, DETERIORO DE LOS EQUIPOS DADOS EN COMODATO. EL CLIENTE es el único responsable de los equipos instalados en su domicilio y dados en comodato para la efectiva prestación del servicio de internet inalámbrico, en tal sentido, en caso de pérdida, robo, hurto, deterioro total o parcial o cualquier cosa que le ocurra a los equipos descritos en la cláusula DÉCIMA PRIMERA deberá pagara BOOM SOLUTIONS el costo de los mismos equipos los cuales se encuentran plenamente descritos en la cláusula DÉCIMA. DÉCIMA CUARTA:  PROHIBICIÓN DE ALTERACIÓN DE EQUIPOS. Es contra la ley alterar cualquier equipo perteneciente a BOOM SOLUTIONS, con el fin de recibir, interceptar o ayudar a recibir o interceptar cualquier servicio de comunicación proporcionado a través del sistema propiedad de BOOM SOLUTIONS o de un tercero. La violación de este artículo supondrá que se rescinde el presente contrato con los daños y perjuicios a que haya lugar, así como el ejercicio de las vías legales pertinentes, en defensa de los derechos e intereses de BOOM SOLUTIONS. DÉCIMA QUINTA: DE LA SUSPENSIÓN DEL SERVICIO POR FALTA DE PAGO. Si llegado el sexto (6to) día del mes en curso y EL CLIENTE no canceló de manera oportuna y como lo establece la cláusula SEXTA, el servicio de internet inalámbrico contratado, se suspenderá el servicio a las 12:01 am del Sexto (6to) día del mes en curso, y no se restituirá el servicio hasta tanto EL CLIENTE no pague el recibo del mes en curso y el costo por REACTIVACIÓN POR PAGO EXTEMPORANEO establecido en la cláusula DÉCIMA del presente contrato. PARÁGRAFO PRIMERO: el pago extemporáneo del recibo del mes en curso que tuviera como consecuencia la suspensión temporal del servicio de internet inalámbrico, no le dará derecho a EL CLIENTE a solicitar el pago prorrateado por los días del mes que solamente pudo disfrutar del servicio de internet inalámbrico. Por lo cual el recibo del mes en curso sigue transcurriendo íntegramente y con la obligatoriedad de EL CLIENTE de cancelarlo de manera completa. PARÁGRAFO SEGUNDO: si llegado el día ultimo del mes y EL CLIENTE no cancelo el mes en curso del servicio de internet inalámbrico, BOOM SOLUTIONS procederá a retirar del domicilio del cliente los equipos entregados en comodato descritos en la cláusula PRIMERA PARÁGRAFO TERCERO: si posterior al mes vencido sin que EL CLIENTE haya efectuado el pago del servicio, y el mismo desea continuar disfrutando del servicio de internet inalámbrico, EL CLIENTE deberá pagar nuevamente el costo de la ACTIVACIÓN DEL SERVICIO DE INTERNET descrito en la cláusula DÉCIMA del presente contrato; no importando si BOOM SOLUTIONS pudo retirar o no los equipos dados en comodato antes de que EL CLIENTE decidiera reactivar el servicio. DÉCIMA SEXTA: DEL LAPSO DE RECONEXIÓN DEL SERVICIO POR PAGO EXTEMPORÁNEO. Una vez que BOOM SOLUTIONS, logre corroborar un pago de EL CLIENTE el cual se hizo efectivo posterior al día CINCO (05) del mes en curso y antes del último día del mismo mes en curso, el servicio de internet inalámbrico será restituido por BOOM SOLUTIONS, en un lapso de tiempo no mayor de 24 horas contadas desde el momento en que se hizo efectiva la transferencia del cliente siempre y cuando haya notificado vía correo electrónico. DÉCIMA SÉPTIMA: GARANTIA POR ACTIVACIÓN DEL SERVICIO. Una vez efectuada la activación del servicio de internet, BOOM SOLUTIONS, otorgará una garantía de treinta (30) días continuos a EL CLIENTE con la finalidad que el mismo pueda disfrutar plenamente del servicio contratado, en tal sentido duran te ese lapso si existiere algún problema con el servicio de internet que no se pudiere resolver técnicamente por vía remota, tendrá derecho a una visita sin costo del personal técnico de BOOM SOLUTIONS, siempre y cuando el problema o falla no sea imputable a EL CLIENTE, porque de lo contrario deberá cancelar el servicio de visita técnica con costo cuyas tarifas están especificadas en clausula DÉCIMA del presente contrato. PARÁGRAFO ÚNICO: como parte de la garantía establecida en esta cláusula si la falla del servicio de internet inalámbrico contratado es imputable a BOOM SOLUTIONS, la empresa no le cobrará a EL CLIENTE los días en que no se prestó efectivamente el servicio, generándose una nota de crédito a su favor que será restada del recibo de pago del mes inmediatamente siguiente. DÉCIMA OCTAVA: DE LA NOTAS DE CRÉDITO A FAVOR DEL CLIENTE. Cuando exista una falla en la prestación del servicio que sea imputable a BOOM SOLUTIONS, se generará a favor de EL CLIENTE una nota de crédito por el tiempo que estuvo sin servicio de internet inalámbrico, si este alcanza las 24 horas continuas, calculado de manera prorrateada y será restada de recibo de pago del mes inmediatamente siguiente. DÉCIMA NOVENA: DISPONIBILIDAD DEL SERVICIO. En el caso que la prestación del servicio por parte de BOOM SOLUTIONS resulte afectada o sufra interferencia por causas ajenas a su control, incluyendo, sin que constituya limitación, casos fortuitos, incendio, explosión, vandalismo, o producto de la modificación o implementación de cualquier ley, norma, reglamento o por actos de gobierno de la República Bolivariana de Venezuela, gobiernos locales o autoridades militares, emergencias, motines, guerras o huelgas, BOOM SOLUTIONS estará exonerada de prestar el servicio por todo el tiempo que dure el hecho que afecte o interfiera con la prestación del servicio. A tales efectos, BOOM SOLUTIONS deberá notificar al ente competente sobre la interrupción de EL SERVICIO, en cuyo caso la facturación será suspendida hasta que se reanude la prestación el servicio de y al final del mes se presentará una factura prorrateada. BOOM SOLUTIONS podrá suspender el servicio, parcial o totalmente, cuando sea necesario a los efectos de instalar, reparar o cambiar equipos, o de realizar actividades similares requeridas para el correcto o mejor funcionamiento del servicio, previa notificación a EL CLIENTE vía correo electrónico oficial. VIGÉSIMA: DEL MODO PARA EL REPORTE DE FALLAS EN EL SERVICIO. EL CLIENTE deberá reportar cualquier falla o avería que presente en el servicio bien sea vía correo electrónico usando para tales fines las direcciones electrónicas oficiales de las partes contratante descritas en la cláusula VIGÉSIMA SEXTA del presente contrato; o mediante el número telefónico 0251-3353330, en lo cual un operador va a canalizar vía telefónica el tipo de avería que tiene el servicio, emitiendo BOOM SOLUTIONS un comprobante de la solicitud de EL CLIENTE el cual será enviado vía correo electrónico a las dirección oficial de EL CLIENTE a los fines de que el mismo posea su comprobante del reporte. Cabe destacar que todas las llamadas telefónicas son grabadas y auditables a los fines de garantizar un mejor servicio. El operador procurará dar solución a EL CLIENTE, vía remota del problema que pueda presentar sin ningún costo; sin embargo en caso de que el operador no pueda solventar de manera inmediata la falla del sistema, procederá a canalizar la solución del problema mediante los distintos medios bien sea a través de una visita técnica con costo o sin costo dependiendo del tipo de falla y que o quien lo originó; en caso que la falla seo por causas imputables a EL CLIENTE el mismo deberá cancelar el servicio que requiera sin menoscabo de la obligación de cancelar si fuere necesario el costo de los equipos en caso de que el daño sea inminente irremediable. VIGÉSIMA PRIMERA: DEL COSTO DE LA ACTIVACION DEL SERVICIO DE INTERNET. EL CLIENTE se obliga a efectuar un pago a BOOM SOLUTIONS por concepto de ACTIVACIÓN DEL SERVICIO DE INTERNET, cuyas tarifas se encuentran descritas en la cláusula DECIMA del presente contrato; dicho pago no está sujeto a devolución alguna, ni mucho menos comprende el pago de los equipos instalados; este pago se podrá efectuar en dólares en efectivo, o trasferencias bancarias en la cuentas descritas en la cláusula SEXTA, pagaderas al momento de realizar la instalación en el domicilio de EL CLIENTE. VIGESIMA SEGUNDA: DE LAS VISITAS TECNICA CON COSTO. Se entienden por visitas técnicas con costo, aquella solicitud que efectúa EL CLIENTE en aras de solventar algún inconveniente presentado con el servicio de internet inalámbrico, que no pueda ser resuelto por la empresa BOOM SOLUTIONS de manera remota y que sea imputable a EL CLIENTE, por lo cual dicho servicio genera un pago por parte de EL CLIENTE, cuyas tarifas están descritas en la cláusula DECIMA del presente contrato: 1) Reconfiguración de equipos (router y alineación de antena. 2) Reubicación de equipos en el mismo domicilio. 3) Reubicación de equipos por mudanza. 4) Sustitución de cables y conectores. VIGESIMA TERCERA: DEFINICION DE AVERIAS COMUNES. Se entienden por averías comunes las posibles fallas que se pueden generar en el servicio de internet inalámbrico por el uso normal y cotidiano de los equipos pero que sin embargo sufren algún tipo de deterioro por el transcurso del tiempo y que no son imputables a EL CLIENTE, por lo que no generan costos para su solución; Por lo cual las averías más comunes son: 1) Por necesidad de sustitución de equipos por daño no imputable a EL CLIENTE. 2) Reseteo del router a valores de fábrica LO ELIMINARIA. 3) Desalineación de la antena receptora. 4) Deterioro de cables. 5) Deterioro de conectores. PARÁGRAFO ÚNICO: en caso de alguna avería común que no sea directamente imputable a EL CLIENTE, la empresa BOOM SOLUTIONS se obliga a efectuar la reparación de la misma e inclusive la sustitución de los equipos en un lapso no mayor de 72 horas posteriores a la visita del técnico. VIGESIMA CUARTA: LEGALIDAD Y VALIDEZ. En caso de que se entienda que alguna de las disposiciones de este Contrato es de tal amplitud que no puede ser exigible en toda su extensión, la misma será válida hasta el punto máximo en que sea legalmente procedente. De igual manera si cualquier disposición del presente contrato se considerara prohibida o inejecutable por ser contraria a la ley, la normativa aplicable o al orden público, o es calificada de tal por los Tribunales de la República o por cualquier organismo de la administración pública, BOOM SOLUTIONS y EL CLIENTE acuerdan que el resto del contrato mantendrá plenamente su vigencia. VIGESIMA QUINTA: ÚNICO CONTRATO. EL CLIENTE queda sometido a las cláusulas del presente contrato. En caso de que BOOM SOLUTIONS realice modificaciones o incorpore anexos en el futuro, Dichas modificaciones o anexos serán notificados por BOOM SOLUTIONS a EL CLIENTE, a través de los medios que BOOM SOLUTIONS considere adecuados, a fin de que EL CLIENTE manifieste su voluntad de acogerse a las mismas. VIGESIMA SEXTA: NOTIFICACIONES. Cualquier notificación u otra forma de comunicación que sea necesaria dar o pueda darse de acuerdo con este contrato, la partes establecen Por parte de BOOM SOLUTIONS: DIRECCION FISICA: Carrera24 entre calles 10 y 11 local número 10-88, de la ciudad de Barquisimeto municipio Iribarren del estado Lara. TELÉFONO: 0251-3353330 e mail: servicio.cliente@boomsolutions.com. Por parte de EL CLIENTE: las descritas en la cláusula primera del presente contrato. VIGESIMA SEPTIMA: LEGISLACIÓN APLICABLE. Este contrato se regirá por la legislación aplicable de la República Bolivariana de Venezuela. VIGESIMA OCTAVA: VALIDEZ DEL CONTRATO. Este contrato se entenderá suscrito por las partes con la sola firma de EL CLIENTE Inclusive. DISPOSICIÓN FINAL: Lo no previsto en este instrumento será regido por las normas contempladas la Legislación aplicable de la República Bolivariana de Venezuela, la equidad y la buena fe. Las partes eligen como domicilio especial único y excluyente de cualquier otro a la ciudad de Barquisimeto, Estado Lara, a la Jurisdicción de cuyos Tribunales acuerdan someterse. Se hacen dos (02) ejemplares de un mismo tenor y aun solo efecto.';

        $pdf->ContractCom($sheet, $text1, $text2, $text3)->output('I', 'Contracto-'.$sheet['contract'].'.pdf');
    }

    
}else{
    echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
}
exit;