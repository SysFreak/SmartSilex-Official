<?php
require '../../vendor/autoload.php';
require('fpdf.php');

require '../../src/Emailer/class.phpmailer.php';
require '../../src/Emailer/class.smtp.php';

require '../../src/Lib/DBSmart.php';
require '../../src/Lib/Config.php';


$id         =   strip_tags($_REQUEST['datos']);
 // $id         =   strip_tags("1");
$Smart      =   new \App\Lib\DBsmart();
$_replace   =   new \App\Lib\Config(); 

// $query      =   'SELECT client_id, visit_date, phone_main, name, coordinations, add_main, plan, ip_current, signal_r, ip_public, transmitions, ap_current, speedtest_rx, speedtest_tx, reason, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT username FROM users WHERE id = created_by) AS operator FROM cp_sup_serv_sheet WHERE id = "'.$id.'" LIMIT 1';

// $sheet      =   $Smart->DBQuery($query);

$_REQUEST['cliente']['fecha_visita']            =   '';
$_REQUEST['cliente']['tecnico_nombre']          =   '';
$_REQUEST['cliente']['id']                      =   '';
$_REQUEST['cliente']['telefono']                =   '';
$_REQUEST['cliente']['nombre']                  =   '';
$_REQUEST['cliente']['coordenadas']             =   '';
$_REQUEST['cliente']['pueblo']                  =   '';
$_REQUEST['cliente']['direccion']               =   '';
$_REQUEST['cliente']['plan']                    =   '';

$_REQUEST['servicio']['plan-contratado']        =   '';
$_REQUEST['servicio']['ip-actual']              =   '';
$_REQUEST['servicio']['senal']                  =   '';
$_REQUEST['servicio']['ip-publica']             =   '';
$_REQUEST['servicio']['transmision']            =   '';
$_REQUEST['servicio']['ap-actual']              =   '';
$_REQUEST['servicio']['speedtest-recibido']     =   '';
$_REQUEST['servicio']['speedtest-transmitido']  =   '';
$_REQUEST['servicio']['motivo']                 =   '';

$_REQUEST['visita']['solucion']                 =   '';
$_REQUEST['visita']['senal']                    =   '';
$_REQUEST['visita']['transmit']                 =   '';
$_REQUEST['visita']['cambio-IP']                =   '';
$_REQUEST['visita']['cambio-AP']                =   '';
$_REQUEST['visita']['speedtest-recibido']       =   '';
$_REQUEST['visita']['speedtest-transmitido']    =   '';
$_REQUEST['visita']['antena']['instalado']      =   '';
$_REQUEST['visita']['antena']['recogido']       =   '';
$_REQUEST['visita']['router']['instalado']      =   '';
$_REQUEST['visita']['router']['recogido']       =   '';
$_REQUEST['visita']['ata']['instalado']         =   '';
$_REQUEST['visita']['ata']['recogido']          =   '';
$_REQUEST['visita']['cable-inout']['instalado'] =   '';
$_REQUEST['visita']['cable-inout']['recogido']  =   '';
$_REQUEST['visita']['observaciones']            =   '';


$pdf        = new \App\Lib\OperatorStatics();
// // ddd($pdf);
$pdf->hojaServicio($_REQUEST)->output('I', 'OperatorStatics.pdf');