<?php

require '../vendor/autoload.php';

require '../src/Lib/DBSmart.php';

require '../src/Lib/Config.php';

require '../src/Models/Transactions.php';

$objPHPExcel    =   new \PHPExcel();
$Smart          =   new \App\Lib\DBsmart();
$_replace       =   new \App\Lib\Config(); 
$transactions   =   new \App\Models\Transactions();

$date       =   ['da001' => $_REQUEST['d1'], 'da002'  =>  $_REQUEST['d2'], 'op' =>  $_REQUEST['op'] ];

$dat = ( ($date['da001'] <> "") && ($date['da002'] <> "") ) ? ['in' => $date['da001'], 'en' => $date['da002'] ] : ['in' => date("Y-m-d"), 'en' => date("Y-m-d")];

$ope    =   $Smart->DBQuery('SELECT id FROM users WHERE username = "'.$date['op'].'"');

$iData  =   [
    'dat'   =>  'created_at BETWEEN "'.$dat['in'].' 00:00:00" AND "'.$dat['en'].' 23:59:59"',
    'ope'   =>  'operator_id = "'.$ope['id'].'"',
    'sta'   =>  '(status_id = "0" OR status_id = "1")'
];

$sta    =   $transactions->OpeStaticsReport($iData);

$BStyle =   [
                'borders'   =>  [   'allborders'    =>  ['style' =>  PHPExcel_Style_Border::BORDER_THIN] ],
                'alignment' =>  [   'horizontal'    =>  PHPExcel_Style_Alignment::HORIZONTAL_CENTER ]
            ];
$center =   [
                'alignment' =>  [   'horizontal'    =>  PHPExcel_Style_Alignment::HORIZONTAL_CENTER ]
            ];

$cel =  0;

/**
* CONFIGURACION DE MARGENES Y CELDAS
*/
$objPHPExcel->getActiveSheet()->getPageMargins()
    ->setTop(0.2)
    ->setRight(0.2)
    ->setLeft(0.2)
    ->setBottom(0.2);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(1);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);

$objPHPExcel->getActiveSheet()->getStyle("A1:J3")->getFont()->setSize(8);

/**
 * TITULO DEL ARCHIVO
 */

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTES DE OPERACIONES');
$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($BStyle);

/**
 * SUBTITULOS
 */

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'CLIENTE');
$objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
$objPHPExcel->getActiveSheet()->getStyle('A3:B3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C3', 'FACTURA');
$objPHPExcel->getActiveSheet()->mergeCells('C3:C3');
$objPHPExcel->getActiveSheet()->getStyle('C3:C3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D3', 'OFICINA');
$objPHPExcel->getActiveSheet()->mergeCells('D3:D3');
$objPHPExcel->getActiveSheet()->getStyle('D3:D3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E3', 'CAJA');
$objPHPExcel->getActiveSheet()->mergeCells('E3:E3');
$objPHPExcel->getActiveSheet()->getStyle('E3:E3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F3', 'FORMA');
$objPHPExcel->getActiveSheet()->mergeCells('F3:F3');
$objPHPExcel->getActiveSheet()->getStyle('F3:F3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G3', 'STATUS');
$objPHPExcel->getActiveSheet()->mergeCells('G3:G3');
$objPHPExcel->getActiveSheet()->getStyle('G3:G3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H3', 'RECIBIDO');
$objPHPExcel->getActiveSheet()->mergeCells('H3:H3');
$objPHPExcel->getActiveSheet()->getStyle('H3:H3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I3', 'CAMBIO');
$objPHPExcel->getActiveSheet()->mergeCells('I3:I3');
$objPHPExcel->getActiveSheet()->getStyle('I3:I3')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J3', 'TOTAL');
$objPHPExcel->getActiveSheet()->mergeCells('J3:J3');
$objPHPExcel->getActiveSheet()->getStyle('J3:J3')->applyFromArray($BStyle);

$objPHPExcel->getActiveSheet()->getStyle("A3:J100")->getFont()->setSize(6);
$objPHPExcel->getActiveSheet()->getStyle('A3:J100')->applyFromArray($center);


$cel+=4;

foreach ($sta['sta'] as $s => $st) 
{
    $a1     =   "";
    $b1     =   $st['name'];
    $c1     =   $st['bills'];
    $d1     =   $st['office'];
    $e1     =   $st['box'];
    $f1     =   $st['form'];
    $g1     =   $st['status'];
    $h1     =   $st['total_dr'];
    $i1     =   $st['total_vc'];
    $j1     =   $st['total_fp'];

    // $a      =   "A".$cel;
    $b      =   "B".$cel;
    $c      =   "C".$cel;
    $d      =   "D".$cel;
    $e      =   "E".$cel;
    $f      =   "F".$cel;
    $g      =   "G".$cel;
    $h      =   "H".$cel;
    $i      =   "I".$cel;
    $j      =   "J".$cel;

    // $objPHPExcel->getActiveSheet()->getFont()->setSize(6);
    $objPHPExcel->setActiveSheetIndex(0)
        // ->setCellValue($a, $a1)
        ->setCellValue($b, $b1)
        ->setCellValue($c, $c1)
        ->setCellValue($d, $d1)
        ->setCellValue($e, $e1)
        ->setCellValue($f, $f1)
        ->setCellValue($g, $g1)
        ->setCellValue($h, $h1)
        ->setCellValue($i, $i1)
        ->setCellValue($j, $j1);
    $cel+=1;
}


$cel+=1;

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'FORMA');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', 'CANTIDAD');
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', 'TOTAL');
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'EFECTIVO');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', (isset($sta['form'][1]['C']) ? $sta['form'][1]['C'] : '0' ) );
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', (isset($sta['form'][1]['T']) ? $sta['form'][1]['T'] : '0.00' ));
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'TRANSFERENCIA');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', (isset($sta['form'][2]['C']) ? $sta['form'][2]['C'] : '0' ) );
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', (isset($sta['form'][2]['T']) ? $sta['form'][2]['T'] : '0.00' ));
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'PAGO MOVIL');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', (isset($sta['form'][3]['C']) ? $sta['form'][3]['C'] : '0' ) );
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', (isset($sta['form'][3]['T']) ? $sta['form'][3]['T'] : '0.00' ));
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'PAYPAL');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', (isset($sta['form'][4]['C']) ? $sta['form'][4]['C'] : '0' ));
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', (isset($sta['form'][4]['T']) ? $sta['form'][4]['T'] : '0.00' ));
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'ZELLE');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':G'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$cel.'', (isset($sta['form'][5]['C']) ? $sta['form'][5]['C'] : '0' ));
$objPHPExcel->getActiveSheet()->getStyle('H'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', (isset($sta['form'][5]['T']) ? $sta['form'][5]['T'] : '0.00' ));
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$tGen   =   ($sta['form'][1]['T'] + $sta['form'][2]['T'] + $sta['form'][3]['T'] + $sta['form'][4]['T'] + $sta['form'][5]['T']);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'TOTAL GENERAL');
$objPHPExcel->getActiveSheet()->mergeCells('G'.$cel.':H'.$cel.'');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', $tGen);
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

$cel+=1;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$cel.'', 'TOTAL EFECTIVO');
$objPHPExcel->getActiveSheet()->mergeCells('G'.$cel.':H'.$cel.'');
$objPHPExcel->getActiveSheet()->getStyle('G'.$cel.':H'.$cel.'')->applyFromArray($BStyle);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$cel.'', $sta['form'][1]['T']);
$objPHPExcel->getActiveSheet()->getStyle('I'.$cel.':I'.$cel.'')->applyFromArray($BStyle);

    $objPHPExcel->getActiveSheet()->setTitle('Reporte-StreetSmart');

    $objPHPExcel->setActiveSheetIndex(0);

    header('Content-Type: application/vnd.ms-excel');

    header('Content-Disposition: attachment;filename="Reporte-StreetSmart-"'.date("YmdHms").'".xls"');

    header('Cache-Control: max-age=0');

    $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

    $writer->save('php://output');

    exit;