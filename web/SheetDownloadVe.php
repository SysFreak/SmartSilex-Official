<?php
require '../vendor/autoload.php';
require('fpdf.php');

require '../src/Emailer/class.phpmailer.php';
require '../src/Emailer/class.smtp.php';

require '../src/Lib/DBSmart.php';
require '../src/Lib/Config.php';

// use FPDF;

define('FPDF_FONTPATH', realpath( 'fonts/' ));

class PDF extends FPDF
{
    public $debug = 0;
    public $height = 6;

    function __construct($orientation='P', $unit='mm', $size='letter')
    {
        
        parent::__construct($orientation, $unit, $size);
        $this->SetLineWidth(0.4);
        $this->AddFont('Calibri','', 'calibri.php');
        $this->AddFont('Calibri','B','calibrib.php');
        $this->AddFont('Calibri','I','calibrii.php');
    
    }

    function Header()
    {
        $this->Image('assets/img/logicon.png',10,5,0,10);
        $this->SetFont('Calibri','I',12);
        $this->Ln(10);
        // $this->Cell(0,5,'Boom Solutions CA.',$this->debug,1,'R');
        
        // $this->SetFontsize(10);
        // $this->Cell(0,5,'RIF J-405375213',$this->debug,1,'R');
        // $this->Cell(0,5,'Tel: 0251-3350911',$this->debug,1,'R');
        
        $this->SetFont('','B',14);
        $this->SetTextColor(227,108,10);
        $this->Cell(0,5,'HOJA DE SERVICIO',$this->debug,1,'C');
        
        $this->Ln($this->height);
    
    }

    function hojaServicio(array $data)
    {

        $this->addPage('P');
        $this->setFillColor(0,34,96);
        $this->informacion_cliente($data['cliente']);
        
        $this->Ln(2);
        $this->informacion_servicio($data['servicio']);

        $this->Ln(2);
        $this->politicas_garantias($data['servicio']);
        
        $this->Ln(2);
        $this->informacion_visita($data['visita']);

        $this->Ln(2);
        $this->SetFont('','B',11);

        $uno  = 35;
        $dos  = 105-$uno;
        $tres = 20;
        $quad = 91-$tres;
        $this->Cell($uno, $this->height,'FIRMA DEL CLIENTE: ',$this->debug,0,'C');
        $this->Cell($dos, $this->height,'__________________/Fecha___________',$this->debug,0,'C');
        $this->Cell($tres, $this->height,'TECNICO: ',$this->debug,0,'C');
        $this->Cell($quad, $this->height,'_________________________________',$this->debug,0,'C');

        $info = $_REQUEST['cliente']['id'] .' - '.str_replace("/", "-", $_REQUEST['cliente']['fecha_visita']);
        
        $this->Output('F', '../sheetsVe/HojaServicio - ID: '.$data['cliente']['id'].' - '.$data['cliente']['fecha_visita'].'.pdf');
        return;
    
    }
    
    function informacion_cliente(array $data)
    {
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'INFORMACION DEL CLIENTE',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();
        $uno  = 35;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        $this->Cell($uno, $this->height,'FECHA DE VISITA: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['fecha_visita']),$this->debug,0,'L');
        $this->Cell($tres,$this->height,'TECNICO: ',$this->debug,0,'R');
        $this->Cell($quad,$this->height,utf8_decode($data['tecnico_nombre']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'CLIENTE: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['nombre']),$this->debug,0,'L');
        $this->Ln($this->height);

        $this->Cell($uno, $this->height,'CLIENTE ID: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,utf8_decode($data['id']),$this->debug,0,'L');
        $this->Cell($tres,$this->height,'TELEFONO: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell($quad,$this->height,utf8_decode($data['telefono']),$this->debug,0,'L');
        $this->Ln($this->height);
        
        $this->SetFont('Calibri','',10);
        $this->Cell($uno,$this->height,'PUEBLO: ',$this->debug,0,'R');
        $this->Cell($dos,$this->height,utf8_decode($data['pueblo']),$this->debug,0,'L');
        $this->Cell($tres, $this->height,'COORDENADAS: ',$this->debug,0,'R');
        $this->Cell($quad, $this->height,utf8_decode($data['coordenadas']),$this->debug,0,'L');
        $this->Ln($this->height);
        
        $this->Cell($uno,$this->height,'DIRECCION: ',$this->debug,0,'R');
        $this->MultiCell(196-$uno,$this->height,utf8_decode($data['direccion']),$this->debug);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    
    }

    function informacion_servicio(array $servicio)
    {

        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'INFORMACION DEL SERVICIO',1,1,'C',1);

        $this->cuerpo_tabla();
        $uno   = 34;
        $dos   = 70-$uno;
        $tres  = 30;
        $quad  = 65-$tres;
        $cinco = 22;
        $seis  = 60-$cinco;
        
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);

        $this->Cell($uno,  $this->height,'PLAN CONTRATADO: ',$this->debug,0,'R');
        $this->Cell(196-$uno,     $this->height,utf8_decode($servicio['plan-contratado']),$this->debug,0,'L');
        $this->Ln();
        
        $this->Cell($uno, $this->height,'IP ACTUAL: ',$this->debug,0,'R');
        $this->Cell($dos, $this->height,$servicio['ip-actual'],$this->debug,0,'L');
        $this->Cell($tres,$this->height,'AP ACTUAL: ',$this->debug,0,'R');
        $this->Cell($quad, $this->height,$servicio['ap-actual'],$this->debug,0,'L');
        $this->Ln();

        $this->Cell($uno,    $this->height,'SENAL: ',$this->debug,0,'R');
        $this->Cell($dos,    $this->height,utf8_decode($servicio['senal']),$this->debug,0,'L');
        $this->Cell($tres,   $this->height,'TRANSMISION: ',$this->debug,0,'R');
        $this->Cell($quad,   $this->height,utf8_decode($servicio['transmision']),$this->debug,0,'L');
        $this->Cell($cinco,  $this->height,'SPEEDTEST: ',$this->debug,0,'R');
        $this->Cell($seis/2, $this->height,'Rx '.utf8_decode($servicio['speedtest-recibido']),$this->debug,0,'C');
        $this->Cell($seis/2, $this->height,'Tx '.utf8_decode($servicio['speedtest-transmitido']),$this->debug,0,'C');
        $this->Ln();

        $this->Cell($uno,  $this->height,'MOTIVO: ',$this->debug,0,'R');
        $this->MultiCell(196-$uno,$this->height, utf8_decode($servicio['motivo']),$this->debug);
        
        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    
    }

    public function politicas_garantias(array $servicio)
    {
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'POLITICAS DE GARANTIAS DE SERVICIOS',1,1,'C',1);

        $this->cuerpo_tabla();
        $uno  = 35;
        $dos  = 98-$uno;
        $tres = 20;
        $quad = 98-$tres;

        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        // d("Informacion Servicio", $rectCoordIni);

        $text   =   'Boom Solutions contempla en el acuerdo comercial con el titular ya mencionado, una garantía permanente de los equipos instalados con la finalidad de que el mismo pueda disfrutar plenamente del servicio con los estándares de calidad establecidos. Ante lo expuesto si existiera algún problema técnico asociado a su servicio de Internet y el mismo no es solventado de manera remota, el cliente tiene derecho a una visita técnica por el personal especialista de Boom Solutions siempre y cuando el problema o falla presentado no sea imputable al cliente, de lo contrario deberá pagar un monto de $25, en efectivo o será cargado en su próxima factura.';

        $this->Ln(3);
        $this->MultiCell(196,3,utf8_decode($text), $this->debug);

        $this->Cell(82,  $this->height,utf8_decode('Escenarios Imputables a la Garantía de Boom Solutions'),$this->debug,0,'R');
        $this->Cell(80,  $this->height,utf8_decode('Escenarios imputables al cliente'),$this->debug,0,'R');
        $this->Ln(4);
        
        $this->Cell(40,  $this->height,utf8_decode('- Desperfecto de equipos'),$this->debug,0,'R');
        $this->Cell(120,  $this->height,utf8_decode('- Manipulación de equipos'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(36.5,  $this->height,utf8_decode('- Cableado Defectuoso'),$this->debug,0,'R');
        $this->Cell(133,  $this->height,utf8_decode('- Conexión incorrecta de equipos'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(34.5,  $this->height,utf8_decode('- Antena desalineada'),$this->debug,0,'R');
        $this->Cell(154.5,  $this->height,utf8_decode('- Daños a cablería por animales o construcción'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(34.5,  $this->height,utf8_decode(''),$this->debug,0,'R');
        $this->Cell(113.5,  $this->height,utf8_decode('- Daños a equipos'),$this->debug,0,'R');
        $this->Ln(3);
        $this->Cell(34.5,  $this->height,utf8_decode(''),$this->debug,0,'R');
        $this->Cell(155,  $this->height,utf8_decode('- Otros:________________________________'),$this->debug,0,'R');
        $this->Ln(6);

        $text1   =   'El Cliente debe firmar este apartado como autorización de realizar el servicio técnico donde queda notificado la política de garantía de equipos.';

        $this->MultiCell(196,3,utf8_decode($text1), $this->debug);

        $this->Cell(120, $this->height,'_______________________',$this->debug,0,'R');
        $this->Ln($this->height);

        $this->Cell(115,$this->height,utf8_decode('Firma de autorizacion'),$this->debug,0,'R');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }
    
    }

    function informacion_visita(array $data)
    {
        $this->encabezado_tabla();
        $this->Cell(196,$this->height,'INFORMACION DE LA VISITA',1,1,'C',1);
        $rectCoordIni = array('x' => $this->x,'y' => $this->y, );

        $this->cuerpo_tabla();
        $this->Ln($this->height/3);
        $this->Cell(3);
        $this->Cell(50,  $this->height,'PROBLEMAS ENCONTRADOS: ',1,0,'C');
        
        if (array_key_exists('problemas', $data)) {
            $problemas = implode(', ', $data['problemas']);
        } else {
            $problemas = '';
        }
        
        $this->Cell(140,  $this->height, ucwords($problemas), 1,0,'L');
        // $this->Cell(42,  $this->height,' CONECTOR/CABLE',1,0,'C');
        // $this->Cell(22,  $this->height,' ROUTER',1,0,'C');
        // $this->Cell(54,  $this->height,' EQUIPOS MANIPULADOS',1,0,'C');

        $this->Ln();
        $this->Ln(3);
        $this->Cell(3);
        $this->Cell(27,  $this->height,'SOLUCION: ',1,0,'R');
        // $this->Cell(163,  $this->height,$data['solucion'],1,0,'L');
        $this->MultiCell(163,  $this->height,utf8_decode($data['solucion']) ,1);
        
        $uno    = 27;
        $dos    = 30;
        $tres   = 28;
        $quad   = 45;
        $cinco  = 25;
        $seis   = 35;
        $this->Ln(3);
        $this->Cell(3);
        $this->Cell($uno,    $this->height,'CAMBIO DE IP: ',1,0,'R');
        $this->Cell($dos,    $this->height,$data['cambio-IP'],1,0,'C');
        $this->Cell($tres,   $this->height,'CAMBIO DE AP: ',1,0,'R');
        $this->Cell($quad,   $this->height,$data['cambio-AP'],1,0,'C');
        $this->Cell($cinco,  $this->height,'SENAL: ',1,0,'R');
        $this->Cell($seis,   $this->height,'dBm',1,0,'C');

        $this->Ln();
        $this->Cell(3);
        $this->Cell($uno,    $this->height,'SPEEDTEST: ',1,0,'R');
        $this->Cell($dos,    $this->height,'Rx '.$data['speedtest-recibido'],1,0,'C');
        $this->Cell($tres,   $this->height,'Tx '.$data['speedtest-recibido'],1,0,'C');
        $this->Cell($quad,   $this->height,'',1,0,'R');
        $this->Cell($cinco,  $this->height,'TRANSMIT: ',1,0,'R');
        $this->Cell($seis,   $this->height,$data['transmit'].'%',1,0,'C');

        $uno = 40;
        $dos = (190-$uno)/4;
        $this->Ln();
        $this->Ln(3);
        $this->Cell(3);
        $this->Cell($uno, $this->height,'EQUIPOS/INVENTARIO',1,0,'C');
        $this->Cell($dos, $this->height,'ANTENA',1,0,'C');
        $this->Cell($dos, $this->height,'ROUTER',1,0,'C');
        $this->Cell($dos, $this->height,'ATA',1,0,'C');
        $this->Cell($dos, $this->height,'CABLE IN/OUT: ',1,0,'C');
        $this->Ln();
        $this->Cell(3);
        $this->Cell($uno, $this->height,'INSTALADOS',1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['antena']['instalado']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['router']['instalado']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['ata']['instalado']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['cable-inout']['instalado']),1,0,'C');

        $this->Ln();
        $this->Cell(3);
        $this->Cell($uno, $this->height,'RECOGIDOS',1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['antena']['recogido']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['router']['recogido']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['ata']['recogido']),1,0,'C');
        $this->Cell($dos, $this->height,utf8_decode($data['cable-inout']['recogido']),1,0,'C');

        $this->Ln();
        $this->Ln(3);
        $this->Cell(3);
        $this->MultiCell(190,$this->height,'OBSERVACIONES: '.utf8_decode($data['observaciones']),1);
        
        $this->Ln(3);
        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196, $this->y - $rectCoordIni['y']);
        }

    }

    function encabezado_tabla($value='')
    {
        // fuente 14 y negrita
        $this->SetFont('','B',14);
        // texto color blanco
        $this->SetTextColor(255);
    
    }

    function cuerpo_tabla($value='')
    {
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    
    }

}

$id         =   strip_tags($_REQUEST['datos']);
 // $id         =   strip_tags("1");
$Smart      =   new \App\Lib\DBsmart();
$_replace   =   new \App\Lib\Config(); 
$pdf        =   new PDF();

$query  =   'SELECT client_id, visit_date, phone_main, name, coordinations, add_main, plan, ip_current, signal_r, ip_public, transmitions, ap_current, speedtest_rx, speedtest_tx, reason, (SELECT name FROM data_town WHERE id = town_id) AS town, (SELECT username FROM users WHERE id = created_by) AS operator FROM cp_sup_serv_sheet_ve WHERE id = "'.$id.'" LIMIT 1';

$sheet  =   $Smart->DBQuery($query);

$_REQUEST['cliente']['fecha_visita']            =   $_replace->ShowDate($sheet['visit_date']);
$_REQUEST['cliente']['tecnico_nombre']          =   '';
$_REQUEST['cliente']['id']                      =   $sheet['client_id'];
$_REQUEST['cliente']['telefono']                =   $sheet['phone_main'];
$_REQUEST['cliente']['nombre']                  =   $sheet['name'];
$_REQUEST['cliente']['coordenadas']             =   $sheet['coordinations'];
$_REQUEST['cliente']['pueblo']                  =   $sheet['town'];
$_REQUEST['cliente']['direccion']               =   $sheet['add_main'];
$_REQUEST['cliente']['plan']                    =   $sheet['plan'];

$_REQUEST['servicio']['plan-contratado']        =   $sheet['plan'];
$_REQUEST['servicio']['ip-actual']              =   $sheet['ip_current'];
$_REQUEST['servicio']['senal']                  =   $sheet['signal_r'];
$_REQUEST['servicio']['ip-publica']             =   $sheet['ip_public'];
$_REQUEST['servicio']['transmision']            =   $sheet['transmitions'];
$_REQUEST['servicio']['ap-actual']              =   $sheet['ap_current'];
$_REQUEST['servicio']['speedtest-recibido']     =   $sheet['speedtest_rx'];
$_REQUEST['servicio']['speedtest-transmitido']  =   $sheet['speedtest_tx'];
$_REQUEST['servicio']['motivo']                 =   $sheet['reason'];

$_REQUEST['visita']['solucion']                 =   '';
$_REQUEST['visita']['senal']                    =   '';
$_REQUEST['visita']['transmit']                 =   '';
$_REQUEST['visita']['cambio-IP']                =   '';
$_REQUEST['visita']['cambio-AP']                =   '';
$_REQUEST['visita']['speedtest-recibido']       =   '';
$_REQUEST['visita']['speedtest-transmitido']    =   '';
$_REQUEST['visita']['antena']['instalado']      =   '';
$_REQUEST['visita']['antena']['recogido']       =   '';
$_REQUEST['visita']['router']['instalado']      =   '';
$_REQUEST['visita']['router']['recogido']       =   '';
$_REQUEST['visita']['ata']['instalado']         =   '';
$_REQUEST['visita']['ata']['recogido']          =   '';
$_REQUEST['visita']['cable-inout']['instalado'] =   '';
$_REQUEST['visita']['cable-inout']['recogido']  =   '';
$_REQUEST['visita']['observaciones']            =   '';

$pdf->hojaServicio($_REQUEST);

$dataMail = [
    'id'        =>  $_REQUEST['cliente']['id'] ,
    'name'      =>  $_REQUEST['cliente']['nombre'],
    'city'      =>  $_REQUEST['cliente']['pueblo'],
    'date'      =>  $_REQUEST['cliente']['fecha_visita'],
    'reason'    =>  $_REQUEST['servicio']['motivo'],
    'operator'  =>  $sheet['operator'],
];

// sleep(3);

$rout       =   '../sheetsVe/HojaServicio - ID: '.$_REQUEST['cliente']['id'].' - '.$_REQUEST['cliente']['fecha_visita'].'.pdf';

$emails     =   $ccs    =   '';

// $emails[0]  =   'luis.924@boomsolutions.com';
// $css[0]     =   '';

$emails[0]  =   'servicio.cliente@boomsolutions.com';
$ccs[1]     =   'support@boomsolutionspr.com';
$ccs[2]     =   'instalaciones.bqto@boomsolutionspr.com';
$ccs[3]     =   'servicio.cliente@boomsolutions.com';
$ccs[4]     =   'fernando.instalaciones@boomsolutions.com';
$ccs[5]     =   'cobro@boomsolutions.com';



$sendmail   =   SendWeb($emails, $ccs, $dataMail, $rout);


// var_dump($sendmail);exit;

echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

exit;


function SendWeb($emails, $ccs, $cuerpo, $direct)  
{

    $mail = new \PHPMailer();                              // Passing `true` enables exceptions

    try {

        $mail->isSMTP();                                // Set mailer to use SMTP
        $mail->Host         =   'smtp.gmail.com';       // Specify main and backup SMTP servers
        $mail->Port         =   465;                    // TCP port to connect to
        $mail->SMTPAuth     =   true;                   // Enable SMTP authentication
        $mail->Username     =   'boomnetphp@gmail.com'; // SMTP username
        $mail->Password     =   'b00mN3tPHP';           // SMTP password
        $mail->SMTPSecure   =   'ssl';                  // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPDebug    =   0;                      // Enable verbose debug output


        $mail->setFrom('boomnetphp@gmail.com', 'BoomSolutions');


        foreach ($emails as $e => $em)  {   $mail->addAddress($em); }

        foreach ($ccs as $c => $cc)     {   $mail->AddCC($cc);      }

        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Visita Tecnica VE ID # '.$cuerpo["id"].' - '.$cuerpo["name"].' - '.$cuerpo["city"].' - '.$cuerpo['date'].'';
        $mail->Body    = '          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <!-- If you delete this tag, the sky will fall on your head -->
        <meta name="viewport" content="width=device-width" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title>BoomSolutions</title>
        <link rel="icon" href="https://boomsolutionspr.com/wp-content/uploads/2019/05/cropped-favicon-32x32.png" sizes="32x32" />

        <style>
        * { 
            margin:0;
            padding:0;
        }
        * { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

        img { 
            max-width: 100%; 
        }
        .collapse {
            margin:0;
            padding:0;
        }
        body {
            -webkit-font-smoothing:antialiased; 
            -webkit-text-size-adjust:none; 
            width: 100%!important; 
            height: 100%;
        }

        a { color: #2BA6CB;}

        .btn {
            text-decoration:none;
            color: #FFF;
            background-color: #666;
            padding:10px 16px;
            font-weight:bold;
            margin-right:10px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
        }

        p.callout {
            padding:15px;
            background-color:#ECF8FF;
            margin-bottom: 15px;
        }
        .callout a {
            font-weight:bold;
            color: #2BA6CB;
        }

        table.social {
        /*  padding:15px; */
            background-color: #ebebeb;
            
        }
        .social .soc-btn {
            padding: 3px 7px;
            font-size:12px;
            margin-bottom:10px;
            text-decoration:none;
            color: #FFF;font-weight:bold;
            display:block;
            text-align:center;
        }
        a.fb { background-color: #3B5998!important; }
        a.tw { background-color: #1daced!important; }
        a.gp { background-color: #002260!important; }
        a.ms { background-color: #000!important; }

        .sidebar .soc-btn { 
            display:block;
            width:100%;
        }

        table.head-wrap { width: 100%;}

        .header.container table td.logo { padding: 15px; }
        .header.container table td.label { padding: 15px; padding-left:0px;}

        table.body-wrap { width: 100%;}

        table.footer-wrap { width: 100%;    clear:both!important;
        }
        .footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
        .footer-wrap .container td.content p {
            font-size:10px;
            font-weight: bold;
            
        }

        h1,h2,h3,h4,h5,h6 {
        font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
        }
        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

        h1 { font-weight:200; font-size: 44px;}
        h2 { font-weight:200; font-size: 37px;}
        h3 { font-weight:500; font-size: 27px;}
        h4 { font-weight:500; font-size: 23px;}
        h5 { font-weight:900; font-size: 17px;}
        h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

        .collapse { margin:0!important;}

        p, ul { 
            margin-bottom: 10px; 
            font-weight: normal; 
            font-size:14px; 
            line-height:1.6;
        }
        p.lead { font-size:17px; }
        p.last { margin-bottom:0px;}

        ul li {
            margin-left:5px;
            list-style-position: inside;
        }

        ul.sidebar {
            background:#ebebeb;
            display:block;
            list-style-type: none;
        }
        ul.sidebar li { display: block; margin:0;}
        ul.sidebar li a {
            text-decoration:none;
            color: #666;
            padding:10px 16px;
        /*  font-weight:bold; */
            margin-right:10px;
        /*  text-align:center; */
            cursor:pointer;
            border-bottom: 1px solid #777777;
            border-top: 1px solid #FFFFFF;
            display:block;
            margin:0;
        }
        ul.sidebar li a.last { border-bottom-width:0px;}
        ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}

        .container {
            display:block!important;
            max-width:1000px!important;
            margin:0 auto!important; /* makes it centered */
            clear:both!important;
        }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            padding:15px;
            max-width:1000px;
            margin:0 auto;
            display:block; 
        }

        .content table { width: 100%; }


        /* Odds and ends */
        .column {
            width: 300px;
            float:left;
        }
        .column tr td { padding: 15px; }
        .column-wrap { 
            padding:0!important; 
            margin:0 auto; 
            max-width:600px!important;
        }
        .column table { width:100%;}
        .social .column {
            width: 280px;
            min-width: 279px;
            float:left;
        }

        .clear { display: block; clear: both; }

        @media only screen and (max-width: 600px) {
            
            a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

            div[class="column"] { width: auto!important; float:none!important;}
            
            table.social div[class="column"] {
                width:auto!important;
            }

        }
        </style>

        </head>
         
        <body bgcolor="#FFFFFF">

        <table class="head-wrap" bgcolor="#002260">
            <tr>
                <td class="header container">
                    <div class="content">
                        <table bgcolor="#002260">
                            <tr style="text-align: center; width: 75px;">
                                <td><img src="https://i.ibb.co/Mg8JtcR/LOGOTIPOS-BOOM-SOLUTIONS-blanco-1.png" style="width: 100px;"/></td>
                            </tr>
                        </table>
                    </div>          
                </td>
            </tr>
        </table>

        <table class="body-wrap">
            <tr>
                <td></td>
                <td class="container" bgcolor="#FFFFFF">

                    <div class="content">
                        <table>
                            <tr>
                                <td>
                                    <p class="lead">Se agenda visita tecnica bajo los siguientes parametros.</p>
                                    
                                    <table class="head-wrap" style="text-align: center;">
                                        <tr>
                                            <td class="header container">
                                                <div class="content">
                                                    <table>
                                                        <tr style="text-align: center;">
                                                            <img src="https://boomsolutionspr.com/wp-content/uploads/2019/03/boom-net-icon.png" height="73" width="110">
                                                        </tr>
                                                        <tr><h4 style="margin-top: 10px;">Visita Tecnica ID # '.$cuerpo["id"].' - '.$cuerpo["name"].' - '.$cuerpo["city"].'</h4></tr>
                                                    </table>
                                                </div>          
                                            </td>
                                        </tr>
                                    </table>

                                    <br>

                                    <table class="head-wrap" style="background: #ecf8ff">
                                        <tbody>
                                            <tr>
                                                <td>Visita Tecnica</td>
                                                <td>'.$cuerpo["date"].'</td>
                                            </tr>
                                            <tr>
                                                <td>Operador</td>
                                                <td>'.$cuerpo["operator"].'</td>
                                            </tr>
                                            <tr>
                                                <td>Motivo</td>
                                                <td>
                                                    <div>
                                                        <p>'.$cuerpo["reason"].'</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <br>
                                    
                                    <table class="social" width="100%">
                                        <tr>
                                            <td>
                                                <table align="left" class="column">
                                                    <tr>
                                                        <td>
                                                            
                                                            <h5 class="">Contactanos:</h5>
                                                            <p class=""><a href="https://www.boomsolutionspr.com" class="soc-btn fb">Web Page</a>
                                                            <p class=""><a href="https://www.facebook.com/BoomSolutionsPR/" class="soc-btn fb">Facebook</a>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table align="left" class="column">
                                                    <tr>
                                                        <td>
                                                            <h5 class="">Contact Info:</h5>
                                                            <p>Phone:    <strong>(787) 710-2400</strong><br/>
                                                            <p>Whatsapp: <strong>(787) 222-4128</strong><br/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                            
                                                <span class="clear"></span>
                                                            
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </div>
                                            
                </td>
                <td></td>
            </tr>
        </table>

        </body>
        </html>';

        $mail->IsHTML(true);

        $mail->addAttachment($direct);

        $mail->send();

        return array(
            'error'         =>  false, 
            'Message'       =>  "Solicitud enviada satisfactoriamente."   
        );

    } catch (Exception $e) {

        return array(
            'error'         =>  true, 
            'Message'       =>  "Error al enviar solicitud intente nuevamente, si el error persiste contacte a su programador web."   
        );

    }

        return array(
            'error'         =>  false, 
            'Message'       =>  "Solicitud enviada satisfactoriamente."   
        );

}