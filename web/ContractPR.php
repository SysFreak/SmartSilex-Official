<?php

require '../vendor/autoload.php';
require '../src/Lib/DBSmart.php';
require '../src/Lib/Config.php';
require('fpdf.php');

define('FPDF_FONTPATH', realpath( 'fonts/' ));


class PDF extends FPDF
{
    
    public $debug = 0;
    public $height = 7;

    function __construct($orientation='P', $unit='mm', $size='letter')
    {
        
        parent::__construct($orientation, $unit, $size);
        $this->SetLineWidth(0.4);
        $this->AddFont('Calibri','', 'calibri.php');
        $this->AddFont('Calibri','B','calibrib.php');
        $this->AddFont('Calibri','I','calibrii.php');
    }

    public function ContractRes($iData, $text1, $text2)
    {
        $tel = "0.00";

        if(isset($iData['tel_price']) == "0"){ $tel = "0.00"; }else{ $tel = $iData['tel_price']; }

        $this->addPage('P');

        $this->Image('assets/img/logicon.png',85,10,0,20);
        $this->Ln(20);
        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Connectivity Solutions LLC.',$this->debug,1,'C');
        
        $this->SetFont('Calibri','',10);
        $this->SetFontsize(10);
        $this->Cell(0,5,'P.O Box 5849',$this->debug,1,'C');
        $this->Cell(0,5,'Caguas P.R. 00726',$this->debug,1,'C');
        $this->Cell(0,5,'Tel: 787-333-0231',$this->debug,1,'C');
        $this->Cell(0,5,'email: servicio.cliente@boomsolutions.com',$this->debug,1,'C');


        $this->SetFont('Calibri','',10);
        $this->Cell(35, $this->height,'Contract Number: ',$this->debug,0,'R');
        $this->Cell(65, $this->height,$iData['contract'],$this->debug,0,'L');
        $this->Cell(70, $this->height,'Account Number: ',$this->debug,0,'R');
        $this->Cell(30, $this->height, $iData['client_id'],$this->debug,0,'L');
        $this->Ln(3);

        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Customer Agreement',$this->debug,0,'C');
        $this->Ln($this->height);
        
        $this->SetFont('Calibri','',10);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();

        $this->Cell(22, $this->height, 'Name: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['name'], $this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Social Security Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(40, $this->height, $iData['ss_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Date: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['pre_date'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Postal Add: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(136, $this->height, $iData['add_postal'], $this->debug, 0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(10, $this->height, 'Sale: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['sale'], $this->debug, 0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Physical Add: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(133, $this->height, $iData['add_main'], $this->debug, 0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(13, $this->height, 'Phone: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['phone_main'], $this->debug, 0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Bank: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['ab_bank'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Account Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(33, $this->height, $iData['ab_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(18, $this->height, 'Phone Alt: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(10, $this->height, $iData['phone_alt'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Email: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['email'] ,$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Credit Card Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(35, $this->height, $iData['cc_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(16, $this->height, 'Exp Date: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['cc_exp'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Residence: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['house'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Tipo: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(37, $this->height, $iData['techo'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(14, $this->height, 'Niveles: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['nivel'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Service: ',1,0,'C');
        $this->Cell(50, $this->height, 'Monthly Payment',1,0,'C');
        $this->Cell(45, $this->height, 'Router Purchase: No',1,0,'C');
        $this->Cell(37, $this->height, 'Activation Fee',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '','T',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, $iData['package'],1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ '.$iData['price'],1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ '.$iData['instalation'].' Fee',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '','',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Telephone',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ ' .$tel ,1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->Cell(45, $this->height, (($iData['telephone'] == 'NONE') ? 'None' : $iData['telephone']),1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->Cell(37, $this->height, 'Router Lease: Yes',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Other',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ 0.00',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Days until 30',1,0,'C');
        $this->Cell(50, $this->height, 'Proration Payment',1,0,'C');
        $this->Cell(45, $this->height, 'Equipment',1,0,'C');
        $this->Cell(37, $this->height, 'Activation Fee Total','R',0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','',8);
        $this->Cell(22, $this->height, (($iData['date_res'] <= 0) ? 0 : $iData['date_res']) ,1,0,'C');
        $this->Cell(50, $this->height, '$ '. $iData['prorateo'],1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ '.$iData['instalation'],1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->setFillColor(160,160,160);
        $this->Cell(42, $this->height, '$ '.($iData['instalation']+$tel+$iData['prorateo']),'T',0,'C');
        $this->Ln($this->height);

        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }

        $this->SetFont('Calibri','',8);
        $this->Ln(1);

        $this->MultiCell(196,3,utf8_decode($text1),$this->debug);
        $this->Ln($this->height);

        $this->addPage('P');
        $this->MultiCell(196,3,utf8_decode($text2), $this->debug);
            
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Coordenada Longitud: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Coordenada Latitud',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Cableado al Entrar: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Cableado al Salir:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'IP: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Asignada Por:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Site: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'ATA:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Signal: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, '',0,0,'C');
        $this->Cell(50, $this->height, '',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Cliente: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Tecnico: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);


        return $this;

    }

    public function ContractCom($iData, $text1, $text2)
    {
        
        $tel = "0.00";

        if(isset($iData['tel_price']) == "0"){ $tel = "0.00"; }else{ $tel = $iData['tel_price']; }

        $this->addPage('P');

        $this->Image('assets/img/logicon.png',85,10,0,20);
        $this->Ln(20);
        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Connectivity Solutions LLC.',$this->debug,1,'C');
        
        $this->SetFont('Calibri','',10);
        $this->SetFontsize(10);
        $this->Cell(0,5,'P.O Box 5849',$this->debug,1,'C');
        $this->Cell(0,5,'Caguas P.R. 00726',$this->debug,1,'C');
        $this->Cell(0,5,'Tel: 787-333-0231',$this->debug,1,'C');
        $this->Cell(0,5,'email: servicio.cliente@boomsolutions.com',$this->debug,1,'C');


        $this->SetFont('Calibri','',10);
        $this->Cell(35, $this->height,'Contract Number: ',$this->debug,0,'R');
        $this->Cell(65, $this->height,$iData['contract'],$this->debug,0,'L');
        $this->Cell(70, $this->height,'Account Number: ',$this->debug,0,'R');
        $this->Cell(30, $this->height, $iData['client_id'],$this->debug,0,'L');
        $this->Ln(3);

        $this->SetFont('Calibri','B',10);
        $this->Cell(0,5,'Customer Agreement',$this->debug,0,'C');
        $this->Ln($this->height);
        
        $this->SetFont('Calibri','',10);
        $rectCoordIni = array('x' => $this->x, 'y' => $this->y);
        
        $this->cuerpo_tabla();

        $this->Cell(22, $this->height, 'Legal Rep.: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['repre_legal'], $this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Social Security Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(40, $this->height, $iData['ss_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Start: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['pre_date'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Business: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['name'], $this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Social Security Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(40, $this->height, $iData['id_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(11, $this->height, 'Finish: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['last_date'],$this->debug,0,'L');
        $this->Ln($this->height);


        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Postal Add: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(136, $this->height, $iData['add_postal'], $this->debug, 0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(10, $this->height, 'Sale: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['sale'], $this->debug, 0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Physical Add: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(133, $this->height, $iData['add_main'], $this->debug, 0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(13, $this->height, 'Phone: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['phone_main'], $this->debug, 0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Bank: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['ab_bank'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Account Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(33, $this->height, $iData['ab_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(18, $this->height, 'Phone Alt: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(10, $this->height, $iData['phone_alt'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Email: ', $this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['email'] ,$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Credit Card Number: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(35, $this->height, $iData['cc_last'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(16, $this->height, 'Exp Date: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['cc_exp'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','',10);
        $this->Cell(22, $this->height, 'Residencia: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, $iData['house'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(45, $this->height, 'Tipo: ',$this->debug,0,'R');
        $this->SetFont('Calibri','',8);
        $this->Cell(37, $this->height, $iData['techo'],$this->debug,0,'L');
        $this->SetFont('Calibri','',10);
        $this->Cell(14, $this->height, 'Niveles: ',$this->debug,0,'L');
        $this->SetFont('Calibri','',8);
        $this->Cell(20, $this->height, $iData['nivel'],$this->debug,0,'L');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Service: ',1,0,'C');
        $this->Cell(50, $this->height, 'Monthly Payment',1,0,'C');
        $this->Cell(45, $this->height, 'Router Purchase: No',1,0,'C');
        $this->Cell(37, $this->height, 'Activation Fee',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '','T',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, $iData['package'],1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ '.$iData['price'],1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ '.$iData['instalation'].' Fee',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '','',0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Telephone',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ ' .$tel,1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->Cell(45, $this->height, (($iData['telephone'] == 'NONE') ? 'None' : $iData['telephone']),1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->Cell(37, $this->height, 'Router Lease: Yes',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Other',1,0,'C');
        $this->SetFont('Calibri','',8);
        $this->Cell(50, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ 0.00',1,0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(22, $this->height, 'Days until 30',1,0,'C');
        $this->Cell(50, $this->height, 'Proration Payment',1,0,'C');
        $this->Cell(45, $this->height, 'Equipment',1,0,'C');
        $this->Cell(37, $this->height, 'Activation Fee Total','R',0,'C');
        $this->setFillColor(160,160,160);
        $this->cell(0.2);
        $this->Cell(42, $this->height, '',0,0,'C',1);
        $this->Ln($this->height);

        $this->SetFont('Calibri','',8);
        $this->Cell(22, $this->height, (($iData['date_res'] <= 0) ? 0 : $iData['date_res']),1,0,'C');
        $this->Cell(50, $this->height, '$ '. $iData['prorateo'],1,0,'C');
        $this->Cell(45, $this->height, '$ 0.00',1,0,'C');
        $this->Cell(37, $this->height, '$ '.$iData['instalation'],1,0,'C');
        $this->SetFont('Calibri','B',8);
        $this->setFillColor(160,160,160);
        $this->Cell(42, $this->height, '$ '.($iData['instalation']+$tel+$iData['prorateo']),'T',0,'C');
        $this->Ln($this->height);


        if ($this->y > $rectCoordIni['y']){
            $this->rect($rectCoordIni['x'],$rectCoordIni['y'],196,$this->y-$rectCoordIni['y']);
        }

        $this->SetFont('Calibri','',8);
        $this->Ln(1);

        $this->MultiCell(196,3,utf8_decode($text1),$this->debug);
        $this->Ln($this->height);

        $this->addPage('P');
        $this->MultiCell(196,3,utf8_decode($text2), $this->debug);
            
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Coordenada Longitud: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Coordenada Latitud',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Cableado al Entrar: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Cableado al Salir:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'IP: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Asignada Por:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Site: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'ATA:',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Signal: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, '',0,0,'C');
        $this->Cell(50, $this->height, '',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Cliente: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        $this->SetFont('Calibri','B',8);
        $this->Cell(30, $this->height, 'Nombre del Tecnico: ',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Cell(30, $this->height, 'Firma',0,0,'R');
        $this->Cell(50, $this->height, '___________________________________',0,0,'C');
        $this->Ln($this->height);

        return $this;

    }
    public function encabezado_tabla($value='')
    {
        // fuente 14 y negrita
        $this->SetFont('','B',14);
        // texto color blanco
        $this->SetTextColor(255);
    
    }

    public function cuerpo_tabla($value='')
    {
        $this->setFillColor(160,160,160);
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    
    }
    
    public function cuerpo_tabla2($value='')
    {
        $this->setFillColor(100,100,100);
        // fuente 11 y negrita
        $this->SetFont('','',10);
        // texto color NEGRO
        $this->SetTextColor(0);
    
    }

}


$Smart      =   new \App\Lib\DBsmart();
$_replace   =   new \App\Lib\Config(); 
$pdf        =   new PDF();

$query  =   'SELECT id, ticket, client_id, name, add_main, add_postal, phone_main, phone_alt, email, nivel, techo, house, coor_lati, coor_long, package, type, telephone, tel_type, tel_price, pre_date, last_date, date_res, price, prorateo, mes, instalation, cc_last, cc_exp, ab_last, ab_exp, ab_bank, id_last, id_exp, ss_last, ss_exp, contract, country, repre_legal, created_by, created_at, sale FROM cp_contracts WHERE contract = "'.$_REQUEST['contract'].'"';

$sheet  =   $Smart->DBQuery($query);

if($sheet <> false)
{
    if($sheet['type'] == "RESIDENCIAL")
    {
        if($sheet['mes'] == "24"){ $year = '2'; $mes = "dos"; }elseif($sheet['mes'] == "36"){ $year = '3'; $mes = "tres"; }else{ $year = '2'; $mes = "dos";}

        $text1  =   'Connectivity Solutions LLC. Proveerá servicio de acceso a Internet utilizando antenas y equipo inalámbrico propiedad de Connectivity Solutions LLC. El Servicio de acceso a Internet Inalámbrico ofrecido al cliente es basado en la selección de servicio hecha por el cliente y consiste de una velocidad con tope igual al acordado según se especifica en este contrato, usted se compromete a mantener una velocidad igual y/o mayor a la que establece este contrato, de haber una reducción en la velocidad de su servicio de internet Connectivity Solutions LLC. Estará facturando un cargo de $15.00. Al firmar este contrato usted se compromete a cumplir con los siguientes términos: Este contrato tiene una vigencia de '.$sheet['mes'].' meses calendario a partir de la fecha de instalación y comienzo de servicio, usted se compromete a mantener este servicio sin interrupción, por falta de pago, desconexión voluntaria de la misma por el término de '.$year.' años. Luego de cumplir los '.$mes.' ('.$year.') años de contrato el servicio será facturado mes a mes sin compromiso de contrato.  El cliente habrá de pagar un cargo mensual recurrente por acceso al Internet, por mantenimiento del equipo instalado y por el costo y mantenimiento del wireless local loop, si aplicase, según el mismo surge de este contrato. Connectivity Solutions LLC. facturará al cliente de manera mensual por los servicios presentados dentro de los primeros diez (10) días del mes, por medio de su correo electrónico provisto. De no recibir su factura dentro del primer ciclo, estará obligado a informar inmediatamente a Connectivity Solutions LLC.  El cliente habrá de pagar dicha factura dentro de los diez (10) días siguientes al recibo de la factura. Los pagos se debitaran automáticamente de la cuenta de banco o tarjeta de crédito provista por el cliente, de ser una cuenta de banco su método de pago Connectivity Solutions LLC facturará un cargo mensual de $5.00 adicional en su factura ,de la misma forma usted autoriza a Connectivity Solutions LLC a realizar la gestión de cobro por algún tipo de cancelación de contrato, servicio técnico, mensualidad en atraso o compra de alguna parte del equipo a la tarjeta de crédito o cuenta de banco provista por usted en el contrato, sin que Connectivity Solutions LLC necesite su firma o autorización en el recibo de compra y usted está de acuerdo con todo lo anterior convenido. Connectivity Solutions LLC. le realizará un cargo de $5.00 a su cuenta por cada pago devuelto o declinado de la cuenta de banco provista. Connectivity Solutions LLC. le ofrece la opción de adquirir un "Router" en un módico precio de acuerdo a su modelo, el costo del mismo deberá ser pagado al técnico en el momento de la instalación de nuestros servicios. En el caso que Connectivity Solutions LLC le rente un "Router" el mismo tendría un costo mensual de $5.00 adicionales en su mensualidad y este equipo será propiedad de Connectivity Solutions LLC en todo momento. De usted decidir entregar el "Router" rentado, debe hacerlo en una de nuestras oficinas mas cercanas y el mismo será evaluado y recibido solo si está en buenas condiciones físicas y de funcionamiento completo, de lo contrario se le estará facturando el valor del equipo fijado en nuestra lista de precios a la fecha de entrega. En caso de desconexión por falta de pago el cliente pagara $20.00 por reconexión una vez se realice el pago que este atrasado. El equipo utilizado para proveer los servicios de Internet será en todo momento propiedad de Connectivity Solutions LLC. El cliente tiene la obligación de reportar a Connectivity Solutions LLC cualquier tipo de interrupción o degradación al servicio tan pronto advenga en conocimiento de ello. 
        Connectivity Solutions LLC garantiza que el ancho de banda y el servicio será continuo, aunque en ocasiones puede haber ciertas degradaciones del ancho de banda por razones no imputables de Connectivity Solutions LLC. También puede haber interrupción de servicio por causas ajenas a de Connectivity Solutions LLC, como lo serían las averías al cable submarino que transporta data desde Puerto Rico al extranjero. En caso de una interrupción del servicio de acceso por una falla en el sistema que opera y mantiene de Connectivity Solutions LLC., y si la misma dura más de 24 horas a partir de la notificación de la avería a de Connectivity Solutions LLC, el cliente recibirá un crédito por cada día que este fuera de servicio. Este crédito no aplicara cuando la interrupción del servicio se deba a causas ajenas a de Connectivity Solutions LLC. Se aplicará un cargo a su factura mensual de $3.00 por concepto de Plan de Servicio. Este seguro cubrirá cualquier servicio relacionado con ubicación de antena (señal), mantenimiento de equipo defectuoso o reemplazo de equipo afectado por exceso de voltaje.  Este seguro no cubrirá mudanza de equipo o algún evento de fuerza mayor y/o actos de la naturaleza como se mencionará más adelante.  En caso de que el cliente requiera que de Connectivity Solutions LLC rinda servicios adicionales a lo contratado, tales como servicios de configuración de equipo o mantenimiento de equipo que no sea el de Connectivity Solutions LLC, se le hará un cargo al cliente basado en las tarifas.';

        $text2 = 'Esos cargos aplicarán también a cualquier situación donde el cliente o cualquier otro personal de manera alguna alteren el servicio Connectivity Solutions LLC y requiera la asignación de personal para restaurar el servicio. Antes de realizar el trabajo Connectivity Solutions LLC le notificara al cliente la naturaleza del trabajo a ser realizado, la razón por la que dicho trabajo es necesario, y el costo estimado de dicho servicio. Solo si el cliente lo autoriza se procederá a realizar el trabajo. Connectivity Solutions LLC facturara al cliente inmediatamente por dichos servicios adicionales. Connectivity Solutions LLC facturará un cargo de $50.00 por concepto de mudanza de equipo. Connectivity Solutions LLC no será responsable por ninguna interrupción del servicio que se deba a eventos de fuerza mayor o actos de la naturaleza, o por razones que estén fuera del control de Connectivity Solutions LLC.  Como resultado de culpa o negligencia de otros. Dicho acto incluirá, pero no se limitará a: fuego, huracanes u otras condiciones climatológicas similares, inundaciones, terremotos, desastres naturales, motines, epidemias, actos de guerra, actos de vandalismo, robo y otros. El cliente se identificará y se establecerá como punto de contacto primario entre Connectivity Solutions LLC y el cliente en cuanto a cualquier aspecto del cumplimiento de este contrato y los servicios acordados. Se designa como contacto a la persona que firma este contrato y su nombre aparece escrito en él, y la información de cómo comunicarse con él es la provista en este contrato de servicio. En caso de emergencia, cualquier representante del cliente podrá contactar a Connectivity Solutions LLC. Si el cliente al recibo de una factura objeta uno o más cargos que en ella puedan aparecer, habrá de notificar a Connectivity Solutions LLC dentro de un plazo no mayor de diez (10) días a partir del recibo de dicha factura de que objeta todo o parte de la factura. En caso de que se objete solo parte de la factura, el cliente deberá remitir dentro de dicho plazo de diez (10) días el pago por aquella parte que no objeta. Connectivity Solutions LLC evaluara la objeción del cliente y le notificara su posición dentro de los veinte (20) días siguientes al recibo de la objeción.  El equipo de comunicación utilizado para proveer el servicio al cliente es propiedad de Connectivity Solutions LLC, y al final del contrato el mismo será removido por Connectivity Solutions LLC sin costo alguno para el cliente. Será deber del cliente no realizar ningún acto que pueda dañar el equipo o ponerlo en riesgo de pérdida. Cualquier daño sufrido por el equipo causado por la negligencia del cliente será responsabilidad del cliente. En caso de que usted (cliente) incumpla con cualquier obligación establecida en el mismo, la otra Parte (la compañía) le remitirá por transmisión facsímile, por correo electrónico o por correo regular un aviso de que se está violando o incumpliendo el contrato, con una descripción especifica de lo que constituye dicha violación. Al recibir dicha notificación tendrá la obligación de corregir la deficiencia dentro de un término no mayor de diez (10) días calendarios a partir del recibo de dicha notificación. En caso de que no se cumpla con esta solicitud, y asumiendo que le asista la razón, entonces Connectivity Solutions LLC. Podrá solicitar la cancelación del contrato por justa causa. Connectivity Solutions LLC le ofrece la alternativa del servicio de telefonía. De usted adquirir este servicio, se compromete a no trasladar el dispositivo ATA a otra dirección por motivos de 911. De igual forma todo equipo instalado de telefonía pertenece en todo momento a Connectivity Solutions LLC. Solo puede adquirir y mantener el servicio de telefonía en conjunto a nuestros servicios de internet. De ocurrir una cancelación del servicio de telefonía, entiéndase por falta de pago y/o voluntaria, se determinará como incumplimiento de contrato, por lo que usted autoriza a Connectivity Solutions LLC realizarle un único cargo por concepto de cancelación de $175.00 a su tarjeta de crédito o cuenta de banco provista.

Por su parte, Connectivity Solutions LLC podrá cancelar el contrato por justa causa si el cliente dejase de pagar cualquier factura o incumple cualquier obligación contraída en este contrato y no subsane dicha deficiencia dentro de un periodo de diez (10) días luego de serle notificada dicha deficiencia. En caso de que el cliente cancele este contrato sin justa causa, o si Connectivity Solutions LLC tiene que cancelar el contrato por el incumplimiento del mismo por parte del cliente, Connectivity Solutions LLC interrumpirá de inmediato el servicio y removerá el equipo instalado dentro de un periodo razonable, de no ser posible el recogido o entrega de equipo por parte del cliente, se le estará facturando un cargo de $480.00 por concepto de costos de equipo. En caso de cancelación temprana por parte del cliente, este deberá pagar la mensualidad correspondiente, por cada mes que falte desde la fecha en la que manifieste su deseo de no continuar con el servicio hasta la terminación de su contrato. Para el cálculo de este pago, se tomará en cuenta el monto de la mensualidad más alta del paquete que el cliente haya disfrutado en los últimos 6 meses de servicio. "Connectivity Solutions LLC" Referirá esta acción a AMER ASSITS A/R SOLUTIONS. INC. Quienes iniciaran las gestiones de cobro y reportaran a todas las agencias de crédito, afectando su historial a raíz de un incumplimiento de su parte y/o refiriendo esta cuenta a su división legal para trámites de rigor. De ser necesario Referir su cuenta a una agencia de cobro, usted será responsable de asumir los gastos por concepto de las gestiones de esta, así como los honorarios de abogado y trámites legales y/o judiciales. Connectivity Solutions LLC  acuerda salvaguardar y proteger al cliente contra cualquier reclamación, multa, imposición, penalidad o acción legal que pueda ser iniciada contra el cliente por actos u omisiones de Connectivity Solutions LLC que puedan violar leyes federales, estatales o municipales que regulen la instalación y/u operación del servicio que se ha acordado en este contrato o de cualquier actuación de Connectivity Solutions LLC en el cumplimiento de este contrato que pueda dar base a tal acción o cualquier acto que cause daños a terceros. El cliente acuerda salvaguardar y proteger a Connectivity Solutions LLC contra cualquier reclamación, multa, imposición, penalidad o acción legal que pueda ser iniciada contra Connectivity Solutions LLC por actos u omisiones del cliente  que puedan violar leyes federales, estatales o municipales que regulen la utilización del servicio que se ha acordado en este contrato o de cualquier actuación del cliente en el cumplimiento de este contrato que pueda dar base a tal acción o cualquier acto que cause daños a terceros. Connectivity Solutions LLC no será responsable por el mal uso del sistema o abuso del sistema por parte del cliente. Será responsabilidad del cliente el mantener las salvaguardas necesarias para proteger la integridad de su sistema en todo momento y protegerlo contra virus, hacking, spam attacks y otra conducta similar. Este contrato y sus anejos, si alguno, constituyen el acuerdo completo entre las partes y no existe ninguna representación, promesa o acuerdo que no se haya desglosado en el mismo.

Este contrato solo podrá ser enmendado mediante acuerdo escrito físico, por correo electrónico y llamadas grabadas donde se evidencie el acuerdo de las partes. Cláusula de USO LEGAL: Está prohibido el uso del servicio para transmitir, extraer, ver, recibir, o de cualquier forma manejar cualquier material, imagen, escrito o contenido que viole cualquier disposición de ley estatal, federal o tratado internacional.  La violación de lo anterior será considerada violación de este contrato y Connectivity Solutions LLC podrá, a su sola discreción, descontinuar el servicio al cliente, mediante notificación simultánea al efecto sin honrar ningún periodo de gracia. Cláusula de Cesión: Connectivity Solutions LLC se reserva el derecho de ceder el presente contrato a un tercero sin que se requiera la autorización del cliente. 

Este acuerdo será interpretado en acorde con las leyes del Estado Libre Asociado de Puerto Rico, y cualquier reclamación que pueda surgir a raíz del mismo se tramitará ante el Tribunal de Primera Instancia, Sala de Fajardo, o ante el Tribunal Federal de Distrito, según sea el caso. El hecho de que Connectivity Solutions LLC opte por no ejercitar cualquiera de las prerrogativas y derechos concedidos en este contrato no será interpretado como una renuncia al derecho de ejercer tal derecho en un futuro.  Al firmar este contrato usted reconoce y acuerda que usted ha recibido, leído, comprendido y aceptado Voluntariamente y está sujeto a todos los términos y condiciones establecidos a este acuerdo   y que todos los términos se especificaron antes de la activación.
';
        
        $pdf->ContractRes($sheet, $text1, $text2)->output('I', 'Contracto-'.$sheet['contract'].'.pdf');

    }
    elseif($sheet['type'] == "COMERCIAL")
    {
        if($sheet['mes'] == "24"){ $year = '2'; $mes = "dos"; }elseif($sheet['mes'] == "36"){ $year = '3'; $mes = "tres"; }else{ $year = '2'; $mes = "dos";}

        $text1  =   'Connectivity Solutions LLC.  Proveerá servicio de acceso a Internet utilizando antenas y equipo inalámbrico propiedad de Connectivity Solutions LLC. El Servicio de acceso a Internet Inalámbrico ofrecido al cliente es basado en la selección de servicio hecha por el cliente y consiste de una velocidad con tope igual al acordado según se especifica en este contrato, usted se compromete a mantener una velocidad igual y/o mayor a la que establece este contrato, de haber una reducción en la velocidad de su servicio de internet Connectivity Solutions LLC. Estará facturando un cargo de $15.00. Al firmar este contrato usted se compromete a cumplir con los siguientes términos: Este contrato tiene una vigencia de '.$sheet['mes'].' meses calendario a partir de la fecha de instalación y comienzo de servicio, usted se compromete a mantener este servicio sin interrupción, por falta de pago, desconexión voluntaria de la misma por el término de '.$year.' años. Luego de cumplir los '.$mes.' ('.$year.') años de contrato el servicio será facturado mes a mes sin compromiso de contrato.  El cliente habrá de pagar un cargo mensual recurrente por acceso al Internet, por mantenimiento del equipo instalado y por el costo y mantenimiento del wireless local loop, si aplicase, según el mismo surge de este contrato. Connectivity Solutions LLC. facturará al cliente de manera mensual por los servicios presentados dentro de los primeros diez (10) días del mes, por medio de su correo electrónico provisto. De no recibir su factura dentro del primer ciclo, estará obligado a informar inmediatamente a Connectivity Solutions LLC.  El cliente habrá de pagar dicha factura dentro de los diez (10) días siguientes al recibo de la factura. Los pagos se debitaran automáticamente de la cuenta de banco o tarjeta de crédito provista por el cliente, de ser una cuenta de banco su método de pago Connectivity Solutions LLC facturará un cargo mensual de $5.00 adicional en su factura ,de la misma forma usted autoriza a Connectivity Solutions LLC a realizar la gestión de cobro por algún tipo de cancelación de contrato, servicio técnico, mensualidad en atraso o compra de alguna parte del equipo a la tarjeta de crédito o cuenta de banco provista por usted en el contrato, sin que Connectivity Solutions LLC necesite su firma o autorización en el recibo de compra y usted está de acuerdo con todo lo anterior convenido. Connectivity Solutions LLC. le realizará un cargo de $5.00 a su cuenta por cada pago devuelto o declinado de la cuenta de banco provista. Connectivity Solutions LLC. le ofrece la opción de adquirir un "Router" en un módico precio de acuerdo a su modelo, el costo del mismo deberá ser pagado al técnico en el momento de la instalación de nuestros servicios. En el caso que Connectivity Solutions LLC le rente un "Router" el mismo tendría un costo mensual de $5.00 adicionales en su mensualidad y este equipo será propiedad de Connectivity Solutions LLC en todo momento.  De usted decidir entregar el "Router" rentado, debe hacerlo en una de nuestras oficinas más cercanas y el mismo será evaluado y recibido solo si está en buenas condiciones físicas y de funcionamiento completo, de lo contrario se le estará facturando el valor del equipo fijado en nuestra lista de precios a la fecha de entrega. En caso de desconexión por falta de pago el cliente pagara $20.00 por reconexión una vez se realice el pago que este atrasado. El equipo utilizado para proveer los servicios de Internet será en todo momento propiedad de Connectivity Solutions LLC. El cliente tiene la obligación de reportar a Connectivity Solutions LLC cualquier tipo de interrupción o degradación al servicio tan pronto advenga en conocimiento de ello. 

        Connectivity Solutions LLC garantiza que el ancho de banda y el servicio será continuo, aunque en ocasiones puede haber ciertas degradaciones del ancho de banda por razones no imputables de Connectivity Solutions LLC. También puede haber interrupción de servicio por causas ajenas a de Connectivity Solutions LLC, como lo serían las averías al cable submarino que transporta data desde Puerto Rico al extranjero. En caso de una interrupción del servicio de acceso por una falla en el sistema que opera y mantiene de Connectivity Solutions LLC. , y si la misma dura más de 24  horas a partir de la notificación de la avería a de Connectivity Solutions LLC, el cliente recibirá un crédito por cada día que este fuera de servicio. Este crédito no aplicara cuando la interrupción del servicio se deba a causas ajenas a de Connectivity Solutions LLC. Se aplicará un cargo a su factura mensual de $3.00 por concepto de Plan de Servicio. Este seguro cubrirá cualquier servicio relacionado con ubicación de antena (señal), mantenimiento de equipo defectuoso o reemplazo de equipo afectado por exceso de voltaje.  Este seguro no cubrirá mudanza de equipo o algún evento de fuerza mayor y/o actos de la naturaleza como se mencionará más adelante.  En caso de que el cliente requiera que de Connectivity Solutions LLC rinda servicios adicionales a lo contratado, tales como servicios de configuración de equipo o mantenimiento de equipo que no sea el de Connectivity Solutions LLC, se le hará un cargo al cliente basado en las tarifas.';

        $text2  =   'Esos cargos aplicarán también a cualquier situación donde el cliente o cualquier otro personal de manera alguna alteren el servicio Connectivity Solutions LLC y requiera la asignación de personal para restaurar el servicio. Antes de realizar el trabajo Connectivity Solutions LLC le notificara al cliente la naturaleza del trabajo a ser realizado, la razón por la que dicho trabajo es necesario, y el costo estimado de dicho servicio. Solo si el cliente lo autoriza se procederá a realizar el trabajo. Connectivity Solutions LLC facturara al cliente inmediatamente por dichos servicios adicionales. Connectivity Solutions LLC facturará un cargo de $100.00 por concepto de mudanza de equipo. Connectivity Solutions LLC no será responsable por ninguna interrupción del servicio que se deba a eventos de fuerza mayor o actos de la naturaleza, o por razones que estén fuera del control de Connectivity Solutions LLC.  Como resultado de culpa o negligencia de otros. Dicho acto incluirá, pero no se limitará a: fuego, huracanes u otras condiciones climatológicas similares, inundaciones, terremotos, desastres naturales, motines, epidemias, actos de guerra, actos de vandalismo, robo y otros. El cliente se identificará y se establecerá como punto de contacto primario entre Connectivity Solutions LLC y el cliente en cuanto a cualquier aspecto del cumplimiento de este contrato y los servicios acordados. Se designa como contacto a la persona que firma este contrato y su nombre aparece escrito en él, y la información de cómo comunicarse con él es la provista en este contrato de servicio. En caso de emergencia, cualquier representante del cliente podrá contactar a Connectivity Solutions LLC. Si el cliente al recibo de una factura objeta uno o más cargos que en ella puedan aparecer, habrá de notificar a Connectivity Solutions LLC dentro de un plazo no mayor de diez (10) días a partir del recibo de dicha factura de que objeta todo o parte de la factura. En caso de que se objete solo parte de la factura, el cliente deberá remitir dentro de dicho plazo de diez (10) días el pago por aquella parte que no objeta. Connectivity Solutions LLC evaluara la objeción del cliente y le notificara su posición dentro de los veinte (20) días siguientes al recibo de la objeción.  El equipo de comunicación utilizado para proveer el servicio al cliente es propiedad de Connectivity Solutions LLC, y al final del contrato el mismo será removido por Connectivity Solutions LLC sin costo alguno para el cliente. Será deber del cliente no realizar ningún acto que pueda dañar el equipo o ponerlo en riesgo de pérdida. Cualquier daño sufrido por el equipo causado por la negligencia del cliente será responsabilidad del cliente. En caso de que usted (cliente) incumpla con cualquier obligación establecida en el mismo, la otra Parte (la compañía) le remitirá por transmisión facsímile, por correo electrónico o por correo regular un aviso de que se está violando o incumpliendo el contrato, con una descripción especifica de lo que constituye dicha violación. Al recibir dicha notificación tendrá la obligación de corregir la deficiencia dentro de un término no mayor de diez (10) días calendarios a partir del recibo de dicha notificación. En caso de que no se cumpla con esta solicitud, y asumiendo que le asista la razón, entonces Connectivity Solutions LLC. Podrá solicitar la cancelación del contrato por justa causa. Connectivity Solutions LLC le ofrece la alternativa del servicio de telefonía. De usted adquirir este servicio, se compromete a no trasladar el dispositivo ATA a otra dirección por motivos de 911. De igual forma todo equipo instalado de telefonía pertenece en todo momento a Connectivity Solutions LLC. Solo puede adquirir y mantener el servicio de telefonía en conjunto a nuestros servicios de internet. De ocurrir una cancelación del servicio de telefonía, entiéndase por falta de pago y/o voluntaria, se determinará como incumplimiento de contrato, por lo que usted autoriza a Connectivity Solutions LLC realizarle un único cargo por concepto de cancelación de $175.00 a su tarjeta de crédito o cuenta de banco provista.

Por su parte, Connectivity Solutions LLC podrá cancelar el contrato por justa causa si el cliente dejase de pagar cualquier factura o incumple cualquier obligación contraída en este contrato y no subsane dicha deficiencia dentro de un periodo de diez (10) días luego de serle notificada dicha deficiencia. En caso de que el cliente cancele este contrato sin justa causa, o si Connectivity Solutions LLC tiene que cancelar el contrato por el incumplimiento del mismo por parte del cliente, Connectivity Solutions LLC interrumpirá de inmediato el servicio y removerá el equipo instalado dentro de un periodo razonable, de no ser posible el recogido o entrega de equipo por parte del cliente, se le estará facturando un cargo de $480.00 por concepto de costos de equipo. En caso de cancelación temprana por parte del cliente, este deberá pagar la mensualidad correspondiente, por cada mes que falte desde la fecha en la que manifieste su deseo de no continuar con el servicio hasta la terminación de su contrato. Para el cálculo de este pago, se tomará en cuenta el monto de la mensualidad más alta del paquete que el cliente haya disfrutado en los últimos 6 meses de servicio. "Connectivity Solutions LLC" Referirá esta acción a AMER ASSITS A/R SOLUTIONS. INC. Quienes iniciaran las gestiones de cobro y reportaran a todas las agencias de crédito, afectando su historial a raíz de un incumplimiento de su parte y/o refiriendo esta cuenta a su división legal para trámites de rigor. De ser necesario Referir su cuenta a una agencia de cobro, usted será responsable de asumir los gastos por concepto de las gestiones de esta, así como los honorarios de abogado y trámites legales y/o judiciales. Connectivity Solutions LLC  acuerda salvaguardar y proteger al cliente contra cualquier reclamación, multa, imposición, penalidad o acción legal que pueda ser iniciada contra el cliente por actos u omisiones de Connectivity Solutions LLC que puedan violar leyes federales, estatales o municipales que regulen la instalación y/u operación del servicio que se ha acordado en este contrato o de cualquier actuación de Connectivity Solutions LLC en el cumplimiento de este contrato que pueda dar base a tal acción o cualquier acto que cause daños a terceros. El cliente acuerda salvaguardar y proteger a Connectivity Solutions LLC contra cualquier reclamación, multa, imposición, penalidad o acción legal que pueda ser iniciada contra Connectivity Solutions LLC por actos u omisiones del cliente  que puedan violar leyes federales, estatales o municipales que regulen la utilización del servicio que se ha acordado en este contrato o de cualquier actuación del cliente en el cumplimiento de este contrato que pueda dar base a tal acción o cualquier acto que cause daños a terceros. Connectivity Solutions LLC no será responsable por el mal uso del sistema o abuso del sistema por parte del cliente. Será responsabilidad del cliente el mantener las salvaguardas necesarias para proteger la integridad de su sistema en todo momento y protegerlo contra virus, hacking, spam attacks y otra conducta similar. Este contrato y sus anejos, si alguno, constituyen el acuerdo completo entre las partes y no existe ninguna representación, promesa o acuerdo que no se haya desglosado en el mismo.

Este contrato solo podrá ser enmendado mediante acuerdo escrito físico, por correo electrónico y llamadas grabadas donde se evidencie el acuerdo de las partes. Cláusula de USO LEGAL: Está prohibido el uso del servicio para transmitir, extraer, ver, recibir, o de cualquier forma manejar cualquier material, imagen, escrito o contenido que viole cualquier disposición de ley estatal, federal o tratado internacional.  La violación de lo anterior será considerada violación de este contrato y Connectivity Solutions LLC podrá, a su sola discreción, descontinuar el servicio al cliente, mediante notificación simultánea al efecto sin honrar ningún periodo de gracia. Cláusula de Cesión: Connectivity Solutions LLC se reserva el derecho de ceder el presente contrato a un tercero sin que se requiera la autorización del cliente. 

Este acuerdo será interpretado en acorde con las leyes del Estado Libre Asociado de Puerto Rico, y cualquier reclamación que pueda surgir a raíz del mismo se tramitará ante el Tribunal de Primera Instancia, Sala de Fajardo, o ante el Tribunal Federal de Distrito, según sea el caso. El hecho de que Connectivity Solutions LLC opte por no ejercitar cualquiera de las prerrogativas y derechos concedidos en este contrato no será interpretado como una renuncia al derecho de ejercer tal derecho en un futuro.  Al firmar este contrato usted reconoce y acuerda que usted ha recibido, leído, comprendido y aceptado Voluntariamente y está sujeto a todos los términos y condiciones establecidos a este acuerdo   y que todos los términos se especificaron antes de la activación.
';
        $pdf->ContractCom($sheet, $text1, $text2)->output('I', 'Contracto-'.$sheet['contract'].'.pdf');
    }

    
}else{
    echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
}
exit;