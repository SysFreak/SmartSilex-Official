<?php

define('CLIENT_ID', 	'local.5f5f8b2452ffc6.77417618');
define('CLIENT_SECRET', 'dgQOTLS71U1AW6CzlEM5nvoL4rx53itzHa1YYSjdnnpkMM9PEY');
define('REDIRECT_URI', 	'http://b24-wlwa3w.bitrix24.es/');
define('DOMAIN', 		'b24-wlwa3w.bitrix24.es');
define('SCOPE', 		'application_permissions');
define('PROTOCOL', 		"https");

/**
 * Funcion Para redireccionar URL
 *
 * @param string $url
 */
function redirect($url)
{
	Header("HTTP 302 Found");
	Header("Location: ".$url);
	die();

}

/**
 * Realiza una solicitud con los datos proporcionados en la URL, Espera Resp. JSON
 *
 * @param string $method GET|POST
 * @param string $url 
 * @param array|null $data POST
 *
 * @return array
 */
function query($method, $url, $data = null)
{
	$query_data 	= "";

	$curlOptions 	= array(
		CURLOPT_RETURNTRANSFER => true
	);

	if($method == "POST")
	{
		$url .= strpos($url, "?") > 0 ? "&" : "?";
		$url .= http_build_query($data);
		$curlOptions[CURLOPT_POST] 			= 	true;
		$curlOptions[CURLOPT_POSTFIELDS] 	= 	http_build_query($data);
	}
	elseif(!empty($data))
	{
		$url .= strpos($url, "?") > 0 ? "&" : "?";
		$url .= http_build_query($data);
	}
	return $url;

	$curl 	= 	curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER 	=> 0,
		CURLOPT_POST 			=> 1,
		CURLOPT_HEADER 			=> 0,
		CURLOPT_RETURNTRANSFER 	=> 1,
		CURLOPT_URL 			=> $url,
	));

  	$result = 	curl_exec($curl);
 	curl_close($curl);

	return json_decode($result, 1);

}

function GetString($url, $data = null)
{
	$url .= strpos($url, "?") > 0 ? "&" : "?";
	$url .= http_build_query($data);

	return $url;
}