/*
 Navicat Premium Data Transfer

 Source Server         : BoomWeb
 Source Server Type    : MySQL
 Source Server Version : 50651
 Source Host           : 107.180.4.96:3306
 Source Schema         : testbasedatos

 Target Server Type    : MySQL
 Target Server Version : 50651
 File Encoding         : 65001

 Date: 06/07/2021 15:57:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ws_api
-- ----------------------------
DROP TABLE IF EXISTS `ws_api`;
CREATE TABLE `ws_api`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `body` tinytext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `from_me` int(10) NULL DEFAULT 0,
  `author` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `time` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `chat_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sender_name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `chat_name` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_id` int(10) NULL DEFAULT 0,
  `pivot_id` int(10) NULL DEFAULT 0,
  `created_at` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `author`(`author`) USING BTREE,
  INDEX `time`(`time`) USING BTREE,
  INDEX `chat_id`(`chat_id`) USING BTREE,
  INDEX `chat_name`(`chat_name`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `pivot_id`(`pivot_id`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ws_stages
-- ----------------------------
DROP TABLE IF EXISTS `ws_stages`;
CREATE TABLE `ws_stages`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pivot_id` int(10) NOT NULL DEFAULT 0,
  `chat_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `time` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `stages_id` int(10) NOT NULL,
  `status_id` int(10) NOT NULL DEFAULT 0,
  `created_at` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `pivot_id`(`pivot_id`) USING BTREE,
  INDEX `chat_id`(`chat_id`) USING BTREE,
  INDEX `stages_id`(`stages_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `created_at`(`created_at`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
